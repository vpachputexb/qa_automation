@Cargo
Feature: Cargo Print Sticker

  @CargoLogin
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |
	

  @CargoPrintSticker
  Scenario Outline: <TC>: Cargo Print Sticker - Happy flow for AWB Sticker and Carton Number
    Given Navigate to Cargo Print Sticker option
    When User selects radio button "<StickerOption>" and enters "<AWBorCartonNo>" and clicks on Search button.
    Then Validate MPS details are shown
    When User Prints first MPS or Carton
    Then Validate error message as "<ErrMsg_NoPrinterInstalled>". User clicks on OK button.
    When User Prints all MPS or Carton
    Then Validate error message as "<ErrMsg_NoPrinterInstalled>". User clicks on OK button.

    Examples: 
      | StickerOption    | AWBorCartonNo | ErrMsg_NoPrinterInstalled             |
      | Cargo AWB Number |  946789899898 | Error: Please Install Printing Device |
      | Cargo AWB Number | 9422952100010 | Error: Please Install Printing Device |
      | Cargo Carton No  |    9016501556 | Error: Please Install Printing Device |
      | Cargo Carton No  |    9016493057 | Error: Please Install Printing Device1 |
