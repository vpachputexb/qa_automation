package starter.stepdefinitions;

//import com.xb.cafe.core.baseStep.BaseStep;
//import com.xb.cafe.ui.pages.login;

import com.xb.cafe.ui.pages.*;

//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;
import net.thucydides.core.webdriver.SupportedWebDriver;
import io.cucumber.java.en.*;

public class login_stepDef  {
	
	//cargo cargo = new cargo();
	
	@Steps
	Cargo cargo;
	
	@Managed
	SupportedWebDriver browser;

	
	@Given("Open the Cargo application")
	public void open_the_cargo_application() throws InterruptedException {
	    cargo.openApplication();
	}
	
	@When("User Enters {string} and {string}")
	public void cargo_Login(String Username, String Password) throws InterruptedException {
		
		cargo.cargoLogin(Username, Password);
	}
	
	@When("Click on Login button")
	public void clickLogin() throws InterruptedException
	{
		cargo.clickLogin();
	}
		
	
	@Then("Validate {string} Email address in welcome message on landing page")
	public void ValidateLoggedInUser(String Username) throws InterruptedException {
		cargo.validateLandingPage(Username);
	}
	
	
	@Given("Navigate to Cargo Print Sticker option")
	public void navigate_to_cargo_Print_Sticker_option() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	    cargo.Navigate_PrintAWBMenu();
	}

	@When("User selects radio button {string} and enters {string} and clicks on Search button.")
	public void user_selects_radio_button_and_enters_and_clicks_on_Search_button(String StickerOption, String AWBorCartonNo) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	    cargo.printAWBorCarton_Search(StickerOption, AWBorCartonNo);
	}
	
	@Then("Validate MPS details are shown")
	public void Validate_MPS_details_are_shown() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	    cargo.validateMPSDetailsShown();
	}
	
	

	@When("User Prints first MPS or Carton")
	public void user_Prints_first_MPS_or_Carton() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	    cargo.printFirstMPSorCarton();
	}

	@Then("Validate error message as {string}. User clicks on OK button.")
	public void validate_error_message_as_User_clicks_on_OK_button(String ErrMsg_NoPrinterInstalled) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		cargo.errorMsg_Print(ErrMsg_NoPrinterInstalled);
	}

	@When("User Prints all MPS or Carton")
	public void user_Prints_all_MPS_or_Carton() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		cargo.printAllMPSorCarton();
	}
	
	@Then("Validate error message as {string} for wrong AWBOrCartoonNo . User clicks on OK button.")
	public void printSticker_ErrMsg_InvalidAWBorCartoonNo(String ErrMsg_WrongAWBorCartoonNo) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	   cargo.printSticker_ErrMsg_InvalidAWBorCartoonNo(ErrMsg_WrongAWBorCartoonNo);
	}
	
	@Then("Validate error message as {string} for blank AWBOrCartoonNo.")
	public void errorMsg_BlankAWBorCartonNo(String ErrMsg_BlankAWBorCartoonNo) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		cargo.errorMsg_BlankAWBorCartonNo(ErrMsg_BlankAWBorCartoonNo);
	}

	@When("User clicks on Reset button.")
	public void user_clicks_on_Reset_button() throws InterruptedException {
		cargo.print_ResetBtn();
	}

	@Then("Validate textbox is null")
	public void validate_textbox_is_null() throws InterruptedException {
	   cargo.validate_blankAWBTOrCartonxtbox();
	}
	
	@Then("Validate {string} in MPS or Carton details")
	public void validate_AWBOrCartonData(String ClientName) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	    cargo.validate_AWBOrCartonData(ClientName);
	}
	
}