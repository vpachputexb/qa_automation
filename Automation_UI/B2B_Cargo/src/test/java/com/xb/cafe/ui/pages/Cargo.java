package com.xb.cafe.ui.pages;

import org.joda.time.DateTime;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.asserts.SoftAssert;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Managed;



//@DefaultUrl("http://cargoautoallocation-appstage.xbees.in/#/login")
//@RunWith(SerenityRunner.class)
	public class Cargo extends PageObject{
	
	 String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	 String username ;
	 String TagName1;
	 String TagComment1;
	 String Tag_MappedEnvironment;
	
	/*public String getUserName() {
        return this.username;
    }
	
	 public void setUserName(String username) {
	        this.username = username;
	 }*/
	        
	@Managed()
	WebDriver driver;
	
	
	public  final By input_UN = By.xpath("//input[@name='email']");
	public  final By input_PW = By.xpath("//input[@id='typepass']");
	public  final By btn_Login = By.xpath("//button[contains(text(),'Login')]");
	public  final By welcome_Msg = By.xpath("//a[@class=\"nav-link dropdown-toggle\"]/span");
	

	public  final By menu_CargoPrintSticker = By.xpath("//span[contains(text(),'Cargo Print Sticker')]");
	public  final By RadioBtn_AWB = By.xpath("//label[contains(text(),'Cargo AWB Number')]");
	public  final By RadioBtn_Carton = By.xpath("//label[contains(text(),'Cargo Carton No')]");
	public  final By Txt_AWB = By.xpath("//input[@id='AwbNo']");
	public  final By Txt_Catron = By.xpath("//input[@id='CartonNo']");
	public  final By Btn_Search = By.xpath("//button[@title='Search']");
	public  final By Btn_Reset = By.xpath("//button[@title='Reset']");
	public  final By Lbl_TotalMPS = By.xpath("//label[contains(text(),'Total MPS Count :')]");
	public  final By Btn_PrintMPS1 = By.xpath("//tbody/tr[1]/td[5]/button[1]");
	public  final By Btn_PrintAWB = By.xpath("//button[contains(text(),'Print AWB')]");
	public  final By Btn_PrintMPS = By.xpath("//button[contains(text(),'Print MPS')]");
	public  final By Msg_NoPrinter = By.xpath("//div[@id='swal2-content']");
	public  final By Btn_OK = By.xpath("//button[contains(text(),'OK')]");
	public  final By ErrMsg_BlankAWBorCartonNo = By.xpath("//span[contains(text(),'StickerType required')]");
	public  final By lbl_ClientName = By.xpath("//div[@class='form-group-row']/legend/div");
	
	
//	public  final By Test = By.name("email");
	public  final By errMsg_InvalidAWBorCartoonNo = By.id("swal2-content");
	public  String StickerOptionType=null;
	

	
	
			
	
	 SoftAssert softAssert= new SoftAssert();
	
	public void openApplication() throws InterruptedException
	{
		open();
	}
	
	public  void cargoLogin(String Username, String Password) throws InterruptedException
	{
		
		getDriver().findElement(input_UN).sendKeys(Username);
		getDriver().findElement(input_PW).sendKeys(Password);
		
		
	}
	
	public void clickLogin() throws InterruptedException
	{
		/*
		 * WebElement ele = getDriver().findElement(btn_Login); clickOn(ele);
		 */
		getDriver().findElement(btn_Login).click();		
		System.out.println("============> Clicked on login button");
		Thread.sleep(3000);
	}
	
	public void validateLandingPage(String Username) throws InterruptedException
	{
		Thread.sleep(2000);
		getDriver().findElement(welcome_Msg).getText(); 
		softAssert.assertTrue(getDriver().findElement(welcome_Msg).isDisplayed());
		softAssert.assertAll();

	}
	
	
	public  void Navigate_PrintAWBMenu() throws InterruptedException 
	{
		Thread.sleep(5000);
		getDriver().findElement(menu_CargoPrintSticker).click();
		Thread.sleep(3000);
		getDriver().findElement(Btn_Reset).click();
		
		
	}
	
	private void selectFromDropdown(String string, String stickerOptionType2) {
		// TODO Auto-generated method stub
		
	}

	public  void printAWBorCarton_Search(String StickerOption,String AWBorCartonNo) throws InterruptedException 
	{
		if (StickerOption.trim().equals("Cargo AWB Number"))
		{
			getDriver().findElement(RadioBtn_AWB).click();
			getDriver().findElement(Txt_AWB).sendKeys(AWBorCartonNo);
			Thread.sleep(2000);
		}
		else if (StickerOption.trim().equals("Cargo Carton No"))
		{
			getDriver().findElement(RadioBtn_Carton).click();
			getDriver().findElement(Txt_Catron).sendKeys(AWBorCartonNo);
			
		}
		getDriver().findElement(Btn_Search).click();
		StickerOptionType=StickerOption;
		Thread.sleep(3000);
		
	}
	
	public  void validateMPSDetailsShown() throws InterruptedException 
	{
		//Thread.sleep(2000);
		softAssert.assertTrue(getDriver().findElement(Lbl_TotalMPS).isDisplayed(), "Expected MPS details are not shown");
		softAssert.assertAll();
	}
	
	public  void printFirstMPSorCarton() throws InterruptedException 
	{
		getDriver().findElement(Btn_PrintMPS1).click();
		Thread.sleep(1000);
		selectFromDropdown(TagComment1, StickerOptionType);
		
	}
	
	public  void printAllMPSorCarton() throws InterruptedException 
	{
	
		if (StickerOptionType.equals("Cargo AWB Number"))
		{
			getDriver().findElement(Btn_PrintAWB).click();
		}
		else if(StickerOptionType.equals("Cargo Carton No"))
		{
			getDriver().findElement(Btn_PrintMPS).click();
		}
		
		Thread.sleep(1000);
		
	}
	
	public  void errorMsg_Print(String ErrMsg_NoPrinterInstalled) throws InterruptedException 
	{
		softAssert.assertEquals(getDriver().findElement(Msg_NoPrinter).getText().trim(), ErrMsg_NoPrinterInstalled);
		Thread.sleep(1000);
		getDriver().findElement(Btn_OK).click();
		softAssert.assertAll();
		
	}
	
	public  void printSticker_ErrMsg_InvalidAWBorCartoonNo(String ErrMsg_WrongAWBorCartoonNo) throws InterruptedException 
	{
		softAssert.assertEquals(getDriver().findElement(errMsg_InvalidAWBorCartoonNo).getText().trim(), ErrMsg_WrongAWBorCartoonNo, "Expected error condition not found");
		Thread.sleep(1000);
		getDriver().findElement(Btn_OK).click();
		softAssert.assertAll();
		
	}
	
	public  void errorMsg_BlankAWBorCartonNo(String ErrMsg_BlankAWBorCartoonNo) throws InterruptedException 
	{
		Thread.sleep(2000);
		String str = "//span[contains(text(),'Test')]".replace("Test", ErrMsg_BlankAWBorCartoonNo);
		System.out.print("++++++++" +str);
		softAssert.assertTrue(getDriver().findElement(By.xpath((str))).isDisplayed(),"Expected error message is not shown");
		softAssert.assertAll();
		
	}
	
	public  void print_ResetBtn() throws InterruptedException 
	{
		getDriver().findElement(Btn_Reset).click();
		Thread.sleep(500);
	}
	
	public  void validate_blankAWBTOrCartonxtbox() throws InterruptedException 
	{
		if (StickerOptionType.equals("Cargo AWB Number"))
		{
			softAssert.assertTrue(getDriver().findElement(((Txt_AWB))).getAttribute("value").isEmpty(),"AWB Textbox is not empty");
		}
		else if(StickerOptionType.equals("Cargo Carton No"))
		{
			softAssert.assertTrue(getDriver().findElement(((Txt_Catron))).getAttribute("value").isEmpty(),"Carton Textbox is not empty");
		}
		softAssert.assertAll();
		
		
	}
	
	public  void validate_AWBOrCartonData(String ClientName) throws InterruptedException 
	{
		if (getDriver().findElement((lbl_ClientName)).getText().endsWith(ClientName))
		{
			softAssert.assertTrue(true,"Expected client name is there");
		}
		else 
			softAssert.assertTrue(false,"Expected client name not fond");
			softAssert.assertAll();

		
	}
	
	

	
	
	
}
	

