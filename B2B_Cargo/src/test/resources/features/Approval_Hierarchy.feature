@ApprovalHierarchyFeature
Feature: Approval Hierarchy feature

  @ApprovalHierarchy
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

  @ApprovalHierarchy_EmailCheckbox
  Scenario Outline: <TC>: Email check box
    Given Navigate to Master data and click Approval hierarchy link
    When User clicked on email checkbox
    Then Checkbox should be shown as selected

  @ApprovalHierarchy_Validations
  Scenario Outline: <TC>: Validation on blank Role and User
    Given Navigate to Master data and click Approval hierarchy link
    When Approval object is selected as Client
    When User clicked on plus button
    When user clicked on Save button
    Then It should show "<ErrorOnBlankRole>","<ErroronBlankUser>"

    Examples: 
      | ErrorOnBlankRole    | ErroronBlankUser    |
      | Please select Role. | Please select User. |

  @ApprovalHierarchy_DeleteButton
  Scenario Outline: <TC>: Delete button
    Given Navigate to Master data and click Approval hierarchy link
    When Approval object is selected as Client
    When User clicked on plus button
    When User clicked on Delete button
    Then It should show confirmation message "<DeleteConfirmationmsg>" as well as Ok and cancel button

    Examples: 
      | DeleteConfirmationmsg            |
      | Remove selected Approval Levels. |

  @ApprovalHierarchy_DeleteValidationsOKButton
  Scenario: <TC>: OK button validations
    Given Navigate to Master data and click Approval hierarchy link
    When Approval object is selected as Client
    When User clicked on plus button
    When User clicked on Delete button
    When Confirmation message is displayed
    When Clicked on OK button
    Then Popup should get closed and record should get deleted

  @ApprovalHierarchy_Happyflow
  Scenario Outline: <TC>: Approval Hierarchy-Happy Flow
    Given Navigate to Master data and click Approval hierarchy link
    When Approval object is selected as Client
    When User clicked on plus button
    Then it should add new row
    When user selects "<Role>" and "<User>"
    When user clicked on Save button
    Then Success message "<Successmsg>" should displayed

    Examples: 
      | Role                  | User            | Successmsg                 |
      | Client On-board Admin | Snehal Takawale | Record saved successfully. |
