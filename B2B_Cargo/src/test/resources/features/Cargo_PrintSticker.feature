@Cargo_PrintSticker_Feature
Feature: Cargo Print Sticker

  @CargoLoginPrintSticker
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

  @CargoPrintSticker
  Scenario Outline: <TC>: Cargo Print Sticker - Happy flow for AWB Sticker and Carton Number
    Given Navigate to Cargo Print Sticker option
    When User selects radio button "<StickerOption>" and enters "<AWBorCartonNo>" and clicks on Search button.
    Then Validate MPS details are shown
    When User Prints first MPS or Carton
    Then Validate error message as "<ErrMsg_NoPrinterInstalled>". User clicks on OK button.
    When User Prints all MPS or Carton
    Then Validate error message as "<ErrMsg_NoPrinterInstalled>". User clicks on OK button.

    Examples: 
      | StickerOption    | AWBorCartonNo | ErrMsg_NoPrinterInstalled              |
      | Cargo AWB Number |  946789899898 | Error: Please Install Printing Device  |
      | Cargo AWB Number | 9422952100010 | Error: Please Install Printing Device  |
      | Cargo Carton No  |    9016501556 | Error: Please Install Printing Device  |
      | Cargo Carton No  |    9016493057 | Error: Please Install Printing Device1 |

  @CargoPrintSticker_NegativeScenarios
  Scenario Outline: <TC>: Cargo Print Sticker - Negative Scenarios i.e Non existing or invalid AWB or Cartoon No
    Given Navigate to Cargo Print Sticker option
    When User selects radio button "<StickerOption>" and enters "<AWBorCartonNo>" and clicks on Search button.
    Then Validate error message as "<ErrMsg_WrongAWBorCartoonNo>" for wrong AWBOrCartoonNo . User clicks on OK button.

    Examples: 
      | StickerOption    | AWBorCartonNo | ErrMsg_WrongAWBorCartoonNo |
      | Cargo AWB Number |         22222 | Record Not Found           |
      | Cargo AWB Number | abc123        | Record Not Found           |
      | Cargo AWB Number | abcd          | Record Not Found           |
      | Cargo AWB Number | ^&$rg         | Record Not Found           |
      | Cargo Carton No  |         22222 | Record Not Found           |
      | Cargo Carton No  | abc123        | Record Not Found           |
      | Cargo Carton No  | abcd          | Record Not Found           |
      | Cargo Carton No  | ^&$rg         | Record Not Found           |

  @CargoPrintSticker_BlankFields
  Scenario Outline: <TC>: Cargo Print Sticker - Blank AWB or Cartoon No
    Given Navigate to Cargo Print Sticker option
    When User selects radio button "<StickerOption>" and enters "<AWBorCartonNo>" and clicks on Search button.
    Then Validate error message as "<ErrMsg_BlankAWBorCartoonNo>" for blank AWBOrCartoonNo.

    Examples: 
      | StickerOption    | AWBorCartonNo | ErrMsg_BlankAWBorCartoonNo    |
      | Cargo AWB Number |               | The AWB Number is required    |
      | Cargo Carton No  |               | The Carton Number is required |

  @CargoPrintSticker_ResetBtn
  Scenario Outline: <TC>: Cargo Print Sticker - Validate functionality of Search and Reset button.
    Given Navigate to Cargo Print Sticker option
    When User selects radio button "<StickerOption>" and enters "<AWBorCartonNo>" and clicks on Search button.
    And User clicks on Reset button.
    Then Validate textbox is null

    Examples: 
      | StickerOption    | AWBorCartonNo  |
      | Cargo AWB Number | 94185221110253 |
      | Cargo Carton No  |     9016492980 |

  @CargoPrintSticker_ValidateData
  Scenario Outline: <TC>: Cargo Print Sticker - Happy flow Validate data for specific AWB or Carton No
    Given Navigate to Cargo Print Sticker option
    When User selects radio button "<StickerOption>" and enters "<AWBorCartonNo>" and clicks on Search button.
    Then Validate "<ClientName>" in MPS or Carton details

    Examples: 
      | StickerOption    | AWBorCartonNo  | ClientName    |
      | Cargo AWB Number | 94185221110253 | MotoG Pvt Ltd |
