@ServiceCreation
Feature: Cargo Service Creation

  @CargoLogin1
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

    @servicelevelcreation
    Scenario Outline: <TC>: Happy flow for service level creation
    Given Navigate to Master template
    When select tab Service Level Template
    Then Click on add new service level button 
    And Provide Service level template name "<Template_Name>"
    Then Select route mode "<Route_Mode>" and upload file and verify success message
    Then click on client dash board
    

   Examples: 
   | Route_Mode    | Template_Name  |Error_Message                                                                                                                  | Success_message             |
   | Surface       |  SLT1          |File headers are not matched. Please download OPS TAT csv file and use that file to upload data to avoid header mismatch issue.| Record created successfully.|
   | Air           |  SLT2          |File headers are not matched. Please download OPS TAT csv file and use that file to upload data to avoid header mismatch issue.| Record created successfully.|


   @FindTheTemplate
   Scenario Outline: <TC>: Happy flow for service level search
    Given Navigate to Master template
    When select tab Service Level Template
    And click on Filter button 
    Then enter the non existing template "<Invalid_Template_Name>" 
    And validate message "<No_Record>"
    Then enter the "<Template_Name>" and Status "<Status>"
    And click on reset and verify both fields are empty
    Then enter the "<Template_Name>" and Status "<Status>"
    And Click on Search
    Then Verfy entered template "<Template_Name>" is present in the result
    Then click on client dash board
     
  Examples: 
   |  Template_Name | Status   | No_Record        | Invalid_Template_Name |
   |  SL 21 feb 1   | Active   | No Records Found | abcdef                |
   |  SL 21 feb 1   | Inactive | No Records Found | abcdef                |
   
   
  
	   @InvalidTemplateCheck
    Scenario Outline: <TC>: Invalid file upload flow for error check 
    Given Navigate to Master template
    When select tab Service Level Template
    Then Click on add new service level button 
    And Provide Service level template name "<Template_Name>"
    Then Upload file with No mandatory data "<No_mandatory_data>" and E2E TAT is "<alpha_numeric>" and E2E TAT is "<negative_integer>" and E2E TAT is "<Balnk>" and incorrect "<Origin_Destinations>"
    Then click on client dash board
    
    Examples: 
    |  Template_Name | No_mandatory_data                                                                                                     | alpha_numeric                      | negative_integer                 | Balnk                              | Origin_Destinations                                                                   |
    | SL 21 feb 1    | Mandatory Fields Missing: Origin Region,Origin State,Origin City,Destination State,Destination City,Route Mode,E2E TAT| E2E TAT should be an integer value | E2E TAT should be greater than 0 | Mandatory Fields Missing: E2E TAT  | Origin/Destinations Region, City, State, Route Mode combination not exists in OPS TAT |
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  