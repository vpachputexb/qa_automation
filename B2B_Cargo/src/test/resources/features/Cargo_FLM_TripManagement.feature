@Cargo_FLM_TripManagement
Feature: Cargo FLM_TripManagement feature

  @CargoLogin
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

  #@Cargo_FLM_TripManagement_TripStartedFLowAdhoc
  #Scenario Outline: <Tc> : Happy flow for Trip Management-Trip Started
  #	Given User Navigates to Trip Management
  #	Given User Navigates to Create Booking Trip Tab
  #	When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #	When User Enters Details In AddShipment Tab One AWB "<AWB Number>"
  #	When User Assigns AWBNumber To Trip
  #	Then User Enters ODO Reading "<ODO Reading>"
  #	Then User Scans AWBNumber & ValidateSucessMessage "<AWB Number>","<Message>"
  #	Then Validate Trip created details "<AWB Number>","<Assigned To>","<ODO Reading>"
  #
  #
  #Examples:
  #| Assigned To | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | AWB Number | ODO Reading | Message |
  #| Prashik Mankar | Mauti Van |  MH 12 XX 8819 | 4W | 9169F023724051 | 212 | Trip Started Successfully |
  #
  #@Cargo_FLM_TripManagement_TripStartedFLowContracted
  #Scenario Outline: <Tc> : Happy flow for Trip Management-Trip Started
  #	Given User Navigates to Trip Management
  #	Given User Navigates to Create Booking Trip Tab
  #	When User Enters Assignment Person and contracted Vehicle Details "<Assigned To>","<Contracted Vehicle Number>"
  #	When User Enters Details In AddShipment Tab One AWB "<AWB Number>"
  #	When User Assigns AWBNumber To Trip
  #	Then User Enters ODO Reading "<ODO Reading>"
  #	Then User Scans AWBNumber & ValidateSucessMessage "<AWB Number>","<Message>"
  #	Then Validate Trip created details "<AWB Number>","<Assigned To>","<ODO Reading>"
  #
  #
  #
  #Examples:
  #| Assigned To | Contracted Vehicle Number | AWB Number | ODO Reading | Message |
  #| Jagdeep 14830 | MH96SP5656| 941923212201902 | 212 | Trip Started Successfully |
  #
  #@Cargo_FLM_TripManagement_TripStartedFLowThreeAWBs
  #Scenario Outline: <Tc> : Happy flow for Trip Management-Trip Started
  #Given User Navigates to Trip Management
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #When User Enters Details In AddShipment Tab Three AWB "<AWB Number1>","<AWB Number2>","<AWB Number3>"
  #When User Assigns AWBNumber To Trip
  #Then User Enters ODO Reading "<ODO Reading>"
  #Then User Scans Multiple AWBNumber & ValidateSucessMessage "<AWB Number1>","<AWB Number2>","<Message>"
  #Then Validate Trip created details Multiple "<AWB Number1>","<AWB Number2>","<Assigned To>","<ODO Reading>"
  #
  #Examples:
  #| Assigned To    | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | AWB Number1    | AWB Number2    | AWB Number3     | ODO Reading | Message                   |
  #| Prashik Mankar | Mauti Van            | MH 12 XX 8819         | 4W                  | 	9422072100288 | 9422072100289 | 9422072100290 |         212 | Trip Started Successfully |
  #
  #
  #@Cargo_FLM_TripManagement_TripStartedFLowAdhoc
  #Scenario Outline: <Tc> : Happy flow for Trip Management-Trip Started
  #	Given Navigate to Cargo Booking option
  #When User navigates to Create booking option
  #When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
  #When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
  #When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
  #Then Verify the AWB genarated and verify the status "<Status>"
  #Then Capture the AWBNumber Generated
  #	Given User Navigates to Trip Management
  #	Given User Navigates to Create Booking Trip Tab
  #	When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #	When User Enters Details In AddShipment Tab Booking Way
  #	When User Assigns AWBNumber To Trip
  #	Then User Enters ODO Reading "<ODO Reading>"
  #	Then User Scans AWBNumber & ValidateSucessMessage Booking Way"<Message>"
  #	Then Validate Trip created details Booking Way "<Assigned To>","<ODO Reading>"
  #
  #Examples:
  #| clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email        | Address            | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | Assigned To | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | AWB Number | ODO Reading | Message |
  #| lenovo     | Surface    | Credit       |        411054 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com     | local Address      | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | Prashik Mankar | Mauti Van |  MH 12 XX 8819 | 4W | 9169F023724051 | 212 | Trip Started Successfully |
  #
  @Cargo_FLM_TripManagement_CloseTrip
  Scenario Outline: <Tc> : Happy flow for Trip Management-Trip close 
    Given User Navigates to Trip Management
    Given User opens the Trip details "<TripStatus>","<BA/SR>"
    When User Validates Trip Details "<AWB Number>","<MPS Number>"
    When User Navigates To Close Trip and Scans AWBs "<AWB Number>","<Number of physical invoices>"
    Then user Enters closure ODO reading "<ClosureODOReading>"
    Then User closes trip and validates trip closure message"<message>"

    Examples: 
      | TripStatus | BA/SR                | AWB Number     | MPS Number     | Number of physical invoices | ClosureODOReading | message                  |
      | Started    | 	Prashik Mankar (24422) |94192321420131|MPS37292240327|                           1 |               300 | Trip Closed Successfully |

  #@Cargo_FLM_TripManagement_CloseTrip_MultipleAWBs
  #Scenario Outline: <Tc> : Happy flow for Trip Management-Trip Started
    #Given User Navigates to Trip Management
    #Given User opens the Trip details "<TripStatus>","<BA/SR>"
    #When User Validates Trip Details Multiple "<AWB Number1>","<AWB Number2>","<AWB Number3>","<MPS Number1>","<MPS Number2>","<MPS Number3>"
    #When User Navigates To Close Trip and Scans AWBs Multiple "<AWB Number1>","<MPS Number1>","<Number of physical invoices1>","<AWB Number2>","<MPS Number2>","<Number of physical invoices2>","<AWB Number3>","<MPS Number3>","<Number of physical invoices3>"
    #Then user Enters closure ODO reading "<ClosureODOReading>"
    #Then User closes trip and validates trip closure message"<message>"
#
    #Examples: 
      #| TripStatus | BA/SR                | AWB Number1     | MPS Number1     | Number of physical invoices1 | AWB Number2    | MPS Number2    | Number of physical invoices2 | AWB Number3   | MPS Number3    | Number of physical invoices3 | ClosureODOReading | message                  |
     #| Started    | Ganesh Vanve (13645) | 94192321420123 | MPS37292240317 |                           1 | 94192321420124 | MPS37292240318 |                            1 | 94192321420125 | MPS37292240319 |                            1 |               300 | Trip Closed Successfully |
     #
     #
     #
     #
     #
     #
     
     
      