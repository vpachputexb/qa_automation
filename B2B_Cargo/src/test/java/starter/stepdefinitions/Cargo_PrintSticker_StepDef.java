package starter.stepdefinitions;


import com.xb.cafe.ui.pages.Cargo;
import com.xb.cafe.ui.pages.Cargo_Booking;
import com.xb.cafe.ui.pages.Cargo_PrintSticker;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Cargo_PrintSticker_StepDef {
	
	
	@Steps
	Cargo_PrintSticker cargo_PrintSticker;
	
	
	@Given("Navigate to Cargo Print Sticker option")
	public void navigate_to_Cargo_Print_Sticker_option() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	    cargo_PrintSticker.Navigate_PrintAWBMenu();
	}

	@When("User selects radio button {string} and enters {string} and clicks on Search button.")
	public void user_selects_radio_button_and_enters_and_clicks_on_Search_button(String StickerOption, String AWBorCartonNo) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	    cargo_PrintSticker.printAWBorCarton_Search(StickerOption, AWBorCartonNo);
	}
	
	@Then("Validate MPS details are shown")
	public void Validate_MPS_details_are_shown() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	    cargo_PrintSticker.validateMPSDetailsShown();
	}
	
	

	@When("User Prints first MPS or Carton")
	public void user_Prints_first_MPS_or_Carton() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	    cargo_PrintSticker.printFirstMPSorCarton();
	}

	@Then("Validate error message as {string}. User clicks on OK button.")
	public void validate_error_message_as_User_clicks_on_OK_button(String ErrMsg_NoPrinterInstalled) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		cargo_PrintSticker.errorMsg_Print(ErrMsg_NoPrinterInstalled);
	}

	@When("User Prints all MPS or Carton")
	public void user_Prints_all_MPS_or_Carton() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		cargo_PrintSticker.printAllMPSorCarton();
	}
	
	@Then("Validate error message as {string} for wrong AWBOrCartoonNo . User clicks on OK button.")
	public void printSticker_ErrMsg_InvalidAWBorCartoonNo(String ErrMsg_WrongAWBorCartoonNo) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	   cargo_PrintSticker.printSticker_ErrMsg_InvalidAWBorCartoonNo(ErrMsg_WrongAWBorCartoonNo);
	}
	
	@Then("Validate error message as {string} for blank AWBOrCartoonNo.")
	public void errorMsg_BlankAWBorCartonNo(String ErrMsg_BlankAWBorCartoonNo) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		cargo_PrintSticker.errorMsg_BlankAWBorCartonNo(ErrMsg_BlankAWBorCartoonNo);
	}

	@When("User clicks on Reset button.")
	public void user_clicks_on_Reset_button() throws InterruptedException {
		cargo_PrintSticker.print_ResetBtn();
	}

	@Then("Validate textbox is null")
	public void validate_textbox_is_null() throws InterruptedException {
	   cargo_PrintSticker.validate_blankAWBTOrCartonxtbox();
	}
	
	@Then("Validate {string} in MPS or Carton details")
	public void validate_AWBOrCartonData(String ClientName) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	    cargo_PrintSticker.validate_AWBOrCartonData(ClientName);
	}
	
}