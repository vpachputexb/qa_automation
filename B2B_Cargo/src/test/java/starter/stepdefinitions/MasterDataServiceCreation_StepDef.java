package starter.stepdefinitions;

//import com.xb.cafe.core.baseStep.BaseStep;
//import com.xb.cafe.ui.pages.login;

import com.xb.cafe.ui.pages.*;

//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;
import net.thucydides.core.webdriver.SupportedWebDriver;
import io.cucumber.java.en.*;

public class MasterDataServiceCreation_StepDef  {
	
	//cargo cargo = new cargo();
	
	@Steps
	Cargo Cargo;
	MasterDataServiceCreation MasterDataServiceCreation;
	
	@Managed
	SupportedWebDriver browser;
	
	@Given("Navigate to Master template")
	public void NavigatetoMasterTemplate() {
		MasterDataServiceCreation.NavigatetoMasterTemplate();
	}
	@When("select tab Service Level Template")
	public void SelectTabServiceLevelTemplate() throws InterruptedException {
		MasterDataServiceCreation.SelectTabServiceLevelTemplate();
	}
	@Then("Click on add new service level button")
		public void ClickonAddNewServiceLevelButton() throws InterruptedException {
		MasterDataServiceCreation.ClickonAddNewServiceLevelButton();
	}
	@And("Provide Service level template name {string}")
	public void ProvideServiceLevelTemplateName(String Template_Name) throws InterruptedException {
		MasterDataServiceCreation.ProvideServiceLevelTemplateName(Template_Name);
	}		
	
	@Then("Select route mode {string} and upload file and verify success message")
	public void SelectRouteMode(String Route_Mode) throws InterruptedException {
		MasterDataServiceCreation.SelectRouteMode(Route_Mode);
	}
	
	@And("Upload invalid file and validate the Error message{string}")
	public void UploadInvalidFileAndValidateTheErrorMessage(String Error_Message) throws InterruptedException {
		MasterDataServiceCreation.UploadInvalidFileAndValidateTheErrorMessage(Error_Message);
	}	
	
	@Then("Select route mode {string} and upload file to edit and verify success message")
	public void UploadFiletoverifySuccessMessage(String Route_Mode) throws InterruptedException {
		MasterDataServiceCreation.UploadFiletoverifySuccessMessage(Route_Mode);
	}
	
	
	
	@Then("Upload correct file and Validate file uploaded successfully")
	public void ValidateSuccessMessageasUserClicksonOkButton() throws InterruptedException {
		MasterDataServiceCreation.ValidateSuccessMessageasUserClicksonOkButton();
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
	@And("click on Filter button")
	public void ClickOnFilterButton() {
		MasterDataServiceCreation.ClickOnFilterButton();
		
	}
	@Then("enter the {string} and Status {string}")
	public void EnterTheAndStatus(String Template_Name, String Status) {
		MasterDataServiceCreation.EnterTheAndStatus(Template_Name, Status);
	}
	
	@Then("click on reset and verify both fields are empty")
	public void clickOnResetAndVerifyBothFieldsAreEmpty() {
		MasterDataServiceCreation.clickOnResetAndVerifyBothFieldsAreEmpty();
	}

	@And("Click on Search")
	public void ClickOnSearch() {
		MasterDataServiceCreation.ClickOnSearch();
	}
	
	 @Then("Verfy entered template {string} is present in the result")
	public void VerfyEnteredTemplateisPresentInTheResult(String Template_Name) {
		MasterDataServiceCreation.VerfyEnteredTemplateisPresentInTheResult( Template_Name);
		
	}
	 
	 @Then("enter the non existing template {string}")
	 public void enterTheNonExistingTemplate(String Invalid_Template_Name) {
		 MasterDataServiceCreation.enterTheNonExistingTemplate( Invalid_Template_Name);
	 }

	 @Then("validate message {string}")
	 public void validateMssage1(String No_Record) {
		 MasterDataServiceCreation.validateMssage(No_Record);
	 }

	 @Then("click on Edit")
	 public void ClickOnEdit() throws InterruptedException {
		 MasterDataServiceCreation.ClickOnEdit();
	 }
/////////////////////////////////////////////////////////////////////////////
	 
	 @Then("click on client dash board")
	 public void ClickonClientDashboard() {
		 MasterDataServiceCreation.ClickonClientDashboard();
	 }
	 @Then("Upload file with No mandatory data {string} and E2E TAT is {string} and E2E TAT is {string} and E2E TAT is {string} and incorrect {string}")
	 public void upload_file_with_no_mandatory_data_and_e2e_tat_is_and_e2e_tat_is_and_e2e_tat_is_and_incorrect(String No_mandatory_data, String alpha_numeric, String negative_integer, String Balnk, String Origin_Destinations) throws InterruptedException {
		 MasterDataServiceCreation.UploadInvalidFileAndValidateErrorMessage(No_mandatory_data, alpha_numeric, negative_integer, Balnk, Origin_Destinations);
	 }
	 
	
	
}