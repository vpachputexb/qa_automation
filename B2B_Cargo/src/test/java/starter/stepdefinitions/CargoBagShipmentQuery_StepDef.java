package starter.stepdefinitions;

import com.xb.cafe.ui.pages.Cargo;
import com.xb.cafe.ui.pages.CargoBagShipmentQuery;
import com.xb.cafe.ui.pages.Cargo_Booking;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

@SuppressWarnings("deprecation")
public class CargoBagShipmentQuery_StepDef {

//	@Given("I login in Cargo Web UI with following credentials")
//	public void cargo_Login(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
//		Cargo.cargoLogin(dataTable);
//	}
	@Steps
	CargoBagShipmentQuery cargoBagShipmentQuery;
	
	
	@Given("User selects report menu and clicks on bag shipment Query")
	public void Navigate_on_Bag_shipmentQuery() throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		cargoBagShipmentQuery.Navigate_on_Bag_shipmentQuery();
	}

	@When("User enteres AWB {string} and click on show Shipment Log button")
	public void UserenteresAWB_and_clickon_showShipmentLog_button(String AWBNo) throws InterruptedException {
		cargoBagShipmentQuery.searchAWB(AWBNo);

	}

	@Then("Validate {string} and validate AWB tracking sections displayed")
	public void verify_result_dispayed_and_validate_success(String successmessage) throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		cargoBagShipmentQuery.verifyValidAWB(successmessage);
		// cargoBagShipmentQuery.verifyAWBTracking(ParentAWB, MPSCount);
	}

	@When("user enters awb {string} and clicks on show shipment Log button")
	public void user_enters_blank_awb_and_clicks_on_show_shipment_Log_button(String AWBNo) throws InterruptedException {
		cargoBagShipmentQuery.verifyResultOnBlankAWB(AWBNo);
	}

	@Then("Verify error displayed {string},{string}")
	public void Verify_error_displayed(String erroronblankawb, String AWBNo) throws InterruptedException {
		cargoBagShipmentQuery.verifyerroronblankAWB(erroronblankawb, AWBNo);
	}

	@When("user enteres invalid AWB {string} and clicks on show shipment Log button")

	public void user_enteres_invalid_AWB_and_clicks_on_show_shipment_Log_button(String AWBNo)
			throws InterruptedException {
		cargoBagShipmentQuery.verifyerroronInvaidAWB(AWBNo);
	}

	@When("User enters AWB number and click on show shipment log button")
	public void User_enters_AWB_number_and_click_on_show_shipment_log_button(String AWBNo) throws InterruptedException {
		cargoBagShipmentQuery.resetBtnClick(AWBNo);
	}

	@When("User enters {string} and clicked on reset button")
	public void User_clicked_on_Reset_button(String AwbNo) throws InterruptedException {
		cargoBagShipmentQuery.resetBtnClick(AwbNo);
	}

	
	@Then("Verify AWB Tracking table columns {string},{string},{string} displayed")
	public void Verify_AWBTracking_Coumns(String ParentAWB, String MPSCount, String ClientName)
			throws InterruptedException {
		cargoBagShipmentQuery.verifyAWBTracking(ParentAWB, MPSCount, ClientName);
	}
	
	@Then("It should display result {string},{string}")
	public void resultonBagNo(String SealNo,String OriginHubname) throws InterruptedException {
		cargoBagShipmentQuery.resultonBagNo(SealNo, OriginHubname);
		
	}

	@Then("Verify MPS Tracking table columns {string} displayed")
	public void Verify_MPS_Tracking_table_columns(String ShippingID) {
		cargoBagShipmentQuery.VerifyMPSTrackingSection(ShippingID);
	}

	@When("User clicks on {string}")
	public void User_clicks_on_shippingIDNumber(String ShippingIDNumber) throws InterruptedException {
		cargoBagShipmentQuery.clickonMPSandVerifyTransactionLog(ShippingIDNumber);

	}

	@Then("Transaction log page should get opened")
	public void Transaction_log_page_should_get_opened() throws InterruptedException {
		cargoBagShipmentQuery.checkTransactionLog();
	}
@When("User enters {string} and clicked on show bag log button")
public void EnterBagNo_ClickonShowBagLog(String BagNo) throws InterruptedException {
	cargoBagShipmentQuery.checkinvalidBagNo(BagNo);
}

@Then("It should display valid result {string},{string}")
public void bagValidation(String BagNo,String Errorbagno) {
	cargoBagShipmentQuery.bagNoValidation(BagNo,Errorbagno);
}

@Then("It should data as {string},{string},{string},{string}")
public void bagnodataVerification(String SealNo, String OriginHubname, String CurrentHubname, String DestinationHubname) throws InterruptedException {
	cargoBagShipmentQuery.bagNoDetailDataVerification(SealNo,OriginHubname,CurrentHubname,DestinationHubname);
}

@Then("Verify data displyed for AWB {string},{string}")
public void  awbdataverification(String ClientName, String ConsingeeNameAdd) throws InterruptedException {
	cargoBagShipmentQuery.awddataVerification(ClientName,ConsingeeNameAdd);
}

@Then("Verify TransactionLog data {string}")
public void transactionlogdataverification(String Process) throws InterruptedException {
	cargoBagShipmentQuery.transactionLogDataVerification(Process);
}























}
