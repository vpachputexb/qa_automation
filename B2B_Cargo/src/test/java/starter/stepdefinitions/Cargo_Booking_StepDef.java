package starter.stepdefinitions;



import java.io.IOException;

import com.xb.cafe.ui.pages.Cargo_Booking;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Cargo_Booking_StepDef {
	
	@Steps
	Cargo_Booking cargo_Booking;
	
	@Given("Navigate to Cargo Booking option")
	public void navigateToCargoBookingOption() throws InterruptedException {
		cargo_Booking.navigateToCargoBookingOption();
		
	}
	
	@When("User navigates to Create booking option")
	public void navigateToCreateBookingOption() throws InterruptedException {
		cargo_Booking.navigateToCreateBookingOption();
	}

	@When("Enter client name {string} and enters serviceablity information {string},{string},{string} as well as {string},{string}")
	public void enterClientNameAndBasicInformation(String clientName, String route_mode, String booking_mode, String pickupPincode, String dropPincode,String serviceability) throws InterruptedException {
		cargo_Booking.enterClientNameAndBasicInformation(clientName, route_mode, booking_mode, pickupPincode, dropPincode,serviceability);
	}
	
	@When("Enter client name {string} and enters serviceablity information for ODA validation {string},{string},{string} as well as {string},{string},{string}")
	public void enterClientNameAndBasicInformationForODAValidation(String clientName, String route_mode, String booking_mode, String pickupPincode, String dropPincode,String serviceability,String message) throws InterruptedException {
		cargo_Booking.enterClientNameAndBasicInformationForODAValidation(clientName, route_mode, booking_mode, pickupPincode, dropPincode,serviceability,message);
	}
	
	@When("User enters pickup location details {string},{string},{string},{string} and {string},{string}")
	public void userEntersPickupLocationDetails(String locationName, String mobile, String email, String address, String gstNo,String telephone) throws InterruptedException {
		cargo_Booking.userEntersPickupLocationDetails(locationName, mobile, email, address, gstNo, telephone);
		
	}
	@When("User enters shipment details {string},{string},{string},{string} and box details {string},{string},{string},{string}")
	public void userEntersShipmentDetails(String numberOfInvoices, String totalShipmentDeclaredValue, String numberMPS, String totalShipmentDeclaredWeight, String lenghth, String breadth, String height, String noOfBoxes) throws InterruptedException {
		cargo_Booking.userEntersShipmentDetails(numberOfInvoices, totalShipmentDeclaredValue, numberMPS, totalShipmentDeclaredWeight, lenghth, breadth, height, noOfBoxes);
	}
	
	@Then("Verify the AWB genarated and verify the status {string}")
	public void verifyAWBGeneratedAndStatus(String status) throws InterruptedException {
		cargo_Booking.verifyAWBGeneratedAndStatus(status);
	}
	
	@Then("Verify the details saved with AWB Number and other details info {string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string}")
	public void verifyTheDetailsDisplayedWithAWBNumbersAndOtherDetails(String clientName, String route_mode, String booking_mode, String pickupPincode, String dropPincode,String locationName, String mobile, String email, String address, String gstNo,String numberOfInvoices, String totalShipmentDeclaredValue, String numberMPS, String totalShipmentDeclaredWeight, String lenghth, String breadth, String height, String noOfBoxes) throws InterruptedException {
		cargo_Booking.verifyTheDetailsDisplayedWithAWBNumbersAndOtherDetails(clientName, route_mode, booking_mode, pickupPincode, dropPincode, locationName, mobile, email, address, gstNo, numberOfInvoices, totalShipmentDeclaredValue, numberMPS, totalShipmentDeclaredWeight, lenghth, breadth, height, noOfBoxes);
	}
	@When("User Enters serviceablity information {string},{string},{string} as well as {string}")
	public void userEntersServiceabilityInformation(String route_mode, String booking_mode, String pickupPincode, String dropPincode) throws InterruptedException {
		cargo_Booking.userEntersServiceabilityInformation(route_mode, booking_mode, pickupPincode, dropPincode);
	}
	
	@Then("User Clicks on refresh button and then validate error messages {string},{string},{string}")
	public void userClicksOnRefreshButtonAndThenValidateErrorMessages(String errorMsgClientName, String messagePickupPincode, String messageDestinationPincode) throws InterruptedException {
		cargo_Booking.userClicksOnRefreshButtonAndThenValidateErrorMessages(errorMsgClientName, messagePickupPincode, messageDestinationPincode);
		
	}
	
	@When("User Enters client name {string}")
	public void userEntersClientName(String clientName) throws InterruptedException {
		cargo_Booking.userEntersClientName(clientName);
	}
	@Then("User clicks on Check Serviceabilty button and validate {string}")
	public void userClicksOnCheckServiceabiltyBttnAndValidateErrorMessage(String errorMessage) throws InterruptedException {
		cargo_Booking.userClicksOnCheckServiceabiltyBttnAndValidateErrorMessage(errorMessage);
		
	}
	
	@Then("User clicks on Check Serviceabilty button and validate pincode error message {string} and {string}")
	public void userClicksOnCheckServiceabiltyButtonAndValidatePincodeErrorMessage(String pickupError, String dropError) throws InterruptedException {
		cargo_Booking.userClicksOnCheckServiceabiltyButtonAndValidatePincodeErrorMessage(pickupError, dropError);
	}
	
	@When("User enters additional box details {string},{string},{string},{string}")
	public void userEntersAdditionalBoxDetails(String length1, String breadth1, String height1, String noOfBoxes1) {
		cargo_Booking.userEntersAdditionalBoxDetails(length1, breadth1, height1, noOfBoxes1);
	}
	
	@Then("validate_origin_hub {string} and destination_hub {string} values")
	public void validateOriginHubAndDestinationHub(String origin_hub, String destination_hub) throws InterruptedException {
		cargo_Booking.validateOriginHubAndDestinationHub(origin_hub, destination_hub);
	}
	
	 @When("Functional error -Enter client name {string} and enters serviceablity information {string},{string},{string} as well as {string}")
	 public void functionalErrorValidationEnterClientNameAndBasicInformation(String clientName, String route_mode, String booking_mode, String pickupPincode, String dropPincode) throws InterruptedException {
		 cargo_Booking.functionalErrorValidationEnterClientNameAndBasicInformation(clientName, route_mode, booking_mode, pickupPincode, dropPincode);
	 }
	 
	 @Then("Validate the error message displayed functional Errors {string}")
	 public void validateTheErrorMessageDisplayedFunctionalError(String message) {
		 cargo_Booking.validateTheErrorMessageDisplayedFunctionalError(message);
	 }
	
	@Then("validate error messages displayed after clicking on reset button {string},{string},{string},{string}")
	public void validateErrorMessagesDisplayedAfterClickingOnReserButton(String messagePickpincode, String messageDestPincode, String messageOriginHub, String messageDestHub) throws InterruptedException {
		cargo_Booking.validateErrorMessagesDisplayedAfterClickingOnReserButton(messagePickpincode, messageDestPincode, messageOriginHub, messageDestHub);
	}
	@Then("Validate pickup location details {string},{string},{string}")
	public void validatePickupLocationDetails(String pickupPincode, String city, String state) throws InterruptedException {
		cargo_Booking.validatePickupLocationDetails(pickupPincode, city, state);
		
	}
	
	@Then("Validate blank error messages {string},{string},{string},{string},{string}")
	public void validateBlankErrorMessagesPickupLocation(String ErrorLocationMessage, String ErrorMobileMessage, String ErrorEmailMessage, String ErrorAddressMessage, String ErrorGSTNumberMessage) throws InterruptedException {
		cargo_Booking.validateBlankErrorMessagesPickupLocation(ErrorLocationMessage, ErrorMobileMessage, ErrorEmailMessage, ErrorAddressMessage, ErrorGSTNumberMessage);
	}
	@When("User enters pickup location details format validation {string},{string},{string},{string} and {string},{string}")
	public void userEntersPickupLocationDetailsFormatValidation(String locationName, String mobile, String email, String address, String gstNo,String telephone) throws InterruptedException {
		cargo_Booking.userEntersPickupLocationDetailsFormatValidation(locationName, mobile, email, address, gstNo, telephone);
	}
	
	@Then("Validate format error messages {string},{string},{string},{string}")
	public void validateFormatErrorMessagesOfPickupLocationDetails(String errorMobileFormatString, String errorEmailFormat, String errorGSTFormat,String errorTelephoneFormat) throws InterruptedException {
		cargo_Booking.validateFormatErrorMessagesOfPickupLocationDetails(errorMobileFormatString, errorEmailFormat, errorGSTFormat, errorTelephoneFormat);
	}
	
	@Then("User navigates back to Pickup Location tab and validate details saved {string},{string},{string},{string} and {string} and click on AddNewLocation button")
	public void userNavigatesBackToPickUpLocationTabAndValidateDetailsSaved(String locationName,String mobile,String email,String address,String gstNo) throws InterruptedException {
		cargo_Booking.userNavigatesBackToPickUpLocationTabAndValidateDetailsSaved(locationName, mobile, email, address, gstNo);
	}
	
	@When("User clicks on save button in shipment details tab")
	public void userClicksOnSaveButtonInShipmentDetailsTab() {
		cargo_Booking.userClicksOnSaveButtonInShipmentDetailsTab();
	}
	@Then("Validate error messages for blank inputs {string},{string},{string},{string},{string},{string},{string},{string}")
	public void validateErrorMessagesForBlankInputsOfShipmentDetails(String errorblankNumberofInvoices, String errorblankTotalDeclaredValue, String errorblankNumberMPS, String errorblankTotalDeclredWeight, String errorblanklength, String errorblankBreadth, String errorblankHeight, String errorblankNoBoxes) throws InterruptedException {
		cargo_Booking.validateErrorMessagesForBlankInputsOfShipmentDetails(errorblankNumberofInvoices, errorblankTotalDeclaredValue, errorblankNumberMPS, errorblankTotalDeclredWeight, errorblanklength, errorblankBreadth, errorblankHeight, errorblankNoBoxes);
	}
	
	@Then("Validate error message total shipment value error {string}, total shipment weight {string}")
	public void validateErrorMessageTotalShipmentValueErrorAndTotalShipmentWeightError(String messageValue, String messageWeight) {
		cargo_Booking.validateErrorMessageTotalShipmentValueErrorAndTotalShipmentWeightError(messageValue, messageWeight);
	}
	
	@Then("Validate Todays date is auto populated in shipment details tab")
	public void validateTodaysDateInShipmentDetailsTab() throws InterruptedException {
		 cargo_Booking.ValidateTodaysDateIsAutoPopulatedInShipmentDetailsTab();
		 
		 
		 
	}
	
	@When("User enters shipment details for error validation {string},{string},{string},{string} and box details {string},{string},{string},{string}, {string},{string},{string},{string}")
	public void userEntersShipmentDetailsForErrorValidationOfBoxAndMPSMismatch(String numberOfInvoices, String totalShipmentDeclaredValue, String numberMPS, String totalShipmentDeclaredWeight, String lenghth1, String breadth1, String height1, String noOfBoxes1,String lenghth2,String breadth2, String height2, String noOfBoxes2) throws InterruptedException {
		cargo_Booking.userEntersShipmentDetailsForErrorValidationOfBoxAndMPSMismatch(numberOfInvoices, totalShipmentDeclaredValue, numberMPS, totalShipmentDeclaredWeight, lenghth1, breadth1, height1, noOfBoxes1, lenghth2, breadth2, height2, noOfBoxes2);
	}
	
	@Then("Validate error message of MPS and boxes mismatch {string}")
	public void validateErrorMessageOfMPSAndBoxesMismatch(String errorMismatch) throws InterruptedException {
		cargo_Booking.validateErrorMessageOfMPSAndBoxesMismatch(errorMismatch);
	}
	
	@When("Validate Booking UI present {string}")
	public void validateBookingUIPresent(String bookingEntryList) throws InterruptedException {
		cargo_Booking.validateBookingUIPresent(bookingEntryList);
	}
	
	@Then("Validate table details {string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string}, {string}")
	public void validateTableDetails(String srNo, String aWB, String mps, String clientRefNo, String tlType, String routeMode, String bookingMode, String clientName, String origin, String destination, String invoiceValue, String phyWt, String volWt, String pickupDate, String bookingDate,String bookingMethod, String bookingStatus, String bookingType, String Action) throws InterruptedException {
		cargo_Booking.validateTableDetails(srNo, aWB, mps, clientRefNo, tlType, routeMode, bookingMode, clientName, origin, destination, invoiceValue, phyWt, volWt, pickupDate, bookingDate, bookingMethod, bookingStatus, bookingType, Action);
		
	}
	
	@When("User navigates to Convert Inches to centimeter tab.")
	public void userNavigatesToConvertInchesToCentimeterTab() {
		cargo_Booking.userNavigatesToConvertInchesToCentimeterTab();
	}
	
	@Then("Enter Inches value {string} and validate centimeter values {string}")
	public void enterInchesValueAndValidateCentimeterValues(String inches, String centimeter) throws InterruptedException {
		cargo_Booking.enterInchesValueAndValidateCentimeterValues(inches, centimeter);
	}
	
	
	@When("User verifies number of total records and number of records displayed on the screen")
	public void userVerifiesNumberOfTotalRecordsAndRecordsDisplayed() throws InterruptedException {
		cargo_Booking.userVerifiesNumberOfTotalRecordsAndRecordsDisplayed();
	}
	@Then("Validate number of pages displayed and validate records displayed per page")
	public void validateNumberOfPagesDisplayedAndValidateRecordsDisplayedPerPage() throws InterruptedException {
		cargo_Booking.validateNumberOfPagesDisplayedAndValidateRecordsDisplayedPerPage();
	}
	

	@When("User navigates to view button in actions class")
	public void userNavigatesToViewButton() throws InterruptedException {
		cargo_Booking.userNavigatesToViewButton();
		
		
	}
	
	@Then("Validate details displayed in view mode {string},{string},{string}")
	public void validateDetailsDisplayedInViewMode(String serviceability, String pickuplocationView, String shipmentDetails) {
		cargo_Booking.validateDetailsDisplayedInViewMode(serviceability, pickuplocationView, shipmentDetails);
		
	}
	
	
	@Then("Validate details displayed in edit mode {string},{string},{string}")
	public void validateDetailsDisplayedInEditMode(String createBooking, String basicInformation, String deatiledInformation) throws InterruptedException {
		cargo_Booking.validateDetailsDisplayedInEditMode(createBooking, basicInformation, deatiledInformation);
	}
	
	@When("User navigates to filter option")
	public void userNavigatesToFilterOption() throws InterruptedException {
		cargo_Booking.userNavigatesToFilterOption();
		
	}
	
	@When("User enter filter inputs {string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string}")
	public void userEntersFilterInputs(String companyName, String aWB, String bookingStatus, String truckLoadType, String routeMode, String bookingMode, String bookingMethod, String origin, String destination, String fromDateBooking, String fromDatePickup) throws InterruptedException {
		cargo_Booking.userEntersFilterInputs(companyName, aWB, bookingStatus, truckLoadType, routeMode, bookingMode, bookingMethod, origin, destination, fromDateBooking, fromDatePickup);
	}
	
	@Then("Validate details displayed after search click in booking entry list {string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string}")
	public void validateDetailsDispalyedAfterSearchClickInFilterTab(String companyName, String aWB, String bookingStatus, String truckLoadType, String routeMode, String bookingMode, String bookingMethod, String origin, String destination, String fromDateBooking, String fromDatePickup) throws InterruptedException {
		cargo_Booking.validateDetailsDispalyedAfterSearchClickInFilterTab(companyName, aWB, bookingStatus, truckLoadType, routeMode, bookingMode, bookingMethod, origin, destination, fromDateBooking, fromDatePickup);
	}
	
	@When("User enters wrong AWB input {string}")
	public void userEntersWrongAWBInput(String awbNumber) throws InterruptedException {
		cargo_Booking.userEntersWrongAWBInput(awbNumber);
	}
	
	@Then("validate no records found message {string}")
	public void validateNoRecordFoundMessage(String message) throws InterruptedException {
		cargo_Booking.validateNoRecordFoundMessage(message);
	}
	
	@When("User enter filter inputs partial inputs {string},{string},{string},{string},{string},{string},{string}")
	public void userEntersFilterInputPartialInputs(String companyName, String aWB, String bookingStatus, String truckLoadType, String routeMode, String bookingMode, String bookingMethod) throws InterruptedException{
		cargo_Booking.userEntersFilterInputPartialInputs(companyName, aWB, bookingStatus, truckLoadType, routeMode, bookingMode, bookingMethod);
	}
	
	@Then("Validate details displayed after search click in booking entry list partial input {string},{string},{string},{string},{string},{string},{string}")
	public void validateDetailsDisplayedAfterSearchClickinBookingEntryListPartialInputValidation(String companyNameDashboard, String aWBDashboard, String bookingStatusDashboard, String truckLoadTypeDashboard, String routeModeDashboard, String bookingModeDashboard, String bookingMethodDashboard) throws InterruptedException {
		cargo_Booking.validateDetailsDisplayedAfterSearchClickinBookingEntryListPartialInputValidation(companyNameDashboard, aWBDashboard, bookingStatusDashboard, truckLoadTypeDashboard, routeModeDashboard, bookingModeDashboard, bookingMethodDashboard);
	}
	
	@When("User clicks on download xlsx button")
	public void userClicksOnDownloadXlsxButton() throws InterruptedException {
		cargo_Booking.userClicksOnDownloadXlsxButton();
	}
	
	@Then("Verify file downloaded at location")
	public void verifyFileDownloadedAtLocation() {
		cargo_Booking.verifyFileDownloadedAtLocation();
	}
	
	@Then("Verify details in the file downloaded {string},{string},{string},{string},{string},{string},{string},{string},{string}")
	public void verifyDetailsInTheFileDownloaded(String awb, String mps, String clientRefNo, String tlType, String routeMode, String bookingMode, String clientName, String origin, String destination) throws IOException {
		cargo_Booking.verifyDetailsInTheFileDownloaded(awb, mps, clientRefNo, tlType, routeMode, bookingMode, clientName, origin, destination);
	}
	
	
	
	@Then("User clicks on consignee details tab")
	public void userClicksOnConsigneeDetailsTab() throws InterruptedException {
		cargo_Booking.userClicksOnConsigneeDetailsTab();
	}
	
	@When("User enters Consignee details {string},{string},{string},{string},{string},{string},{string}")
	public void userEntersConsigneeDetails(String Consigneecode, String companyname, String Mobile, String consigneepoc,String Email,String gstno,String address) throws InterruptedException {
		cargo_Booking.userEntersConsigneeDetails(Consigneecode,companyname,Mobile,consigneepoc,Email,gstno,address);
	}
	
	@Then("User clicks on Save and Next button and Validate record entered successful")
	public void userClicksOnSaveAndNextButtonAndValidateRecordEnteredSuccessful() throws InterruptedException {
		cargo_Booking.userClicksOnSaveAndNextButtonAndValidateRecordEnteredSuccessful();
		
	}
	
	@Then("Validate the error messages consignee details {string},{string},{string},{string},{string},{string},{string}")
	public void validateTheErrorMessages(String ErrorConsigneeCodeMessage, String ErrorCompanyNameMessage, String ErrorMobileMessage, String ErrorConsigneePoc, String ErrorEmailMessage, String ErrorGSTNumberMessage, String ErrorAddressMessage) throws InterruptedException {
		cargo_Booking.validateTheErrorMessages(ErrorConsigneeCodeMessage, ErrorCompanyNameMessage, ErrorMobileMessage, ErrorConsigneePoc, ErrorEmailMessage, ErrorGSTNumberMessage, ErrorAddressMessage);
	}

	
	@Then("User clicks on Save and Next button and Validate for consignee details tab format validation")
	public void userClicksOnSaveAndNextButtonAndValidateForConsigneeDetailsTabFormatValidation() throws InterruptedException {
		cargo_Booking.userClicksOnSaveAndNextButtonAndValidateForConsigneeDetailsTabFormatValidation();
	}
	
	@Then("Validate the error messages {string},{string},{string},{string},{string},{string},{string}")
	public void validateTheErrorMessagesForInvalidInputs(String ErrorConsigneeCodeMessage, String ErrorCompanyNameMessage, String ErrorMobileMessage, String ErrorConsigneePoc, String ErrorEmailMessage, String ErrorGSTNumberMessage, String ErrorAddressMessage) throws InterruptedException {
		cargo_Booking.validateTheErrorMessages(ErrorConsigneeCodeMessage, ErrorCompanyNameMessage, ErrorMobileMessage, ErrorConsigneePoc, ErrorEmailMessage, ErrorGSTNumberMessage, ErrorAddressMessage);
	}
	
	@When("User clicks on Save and Next button")
	public void User_clicks_on_Save_and_Next_button() throws InterruptedException {
		cargo_Booking.userClicksOnSaveAndNextButton();
	}
	@Then("User clicks on consignee details tab and validate the result")
	public void userClicksOnConsigneeDetailsTabAndValidateTheResult()throws InterruptedException {
		cargo_Booking.userClicksOnConsigneeDetailsTabAndValidateTheResult();
	}
	@Then("User clicks on Add new consignee and validate")
	public void userClicksOnAddNewConsigneeAndValidate()throws InterruptedException {
		cargo_Booking.userClicksOnAddNewConsigneeAndValidate();

	}
	@When("User entters following shipment details {string},{string}")
	public void userEnttersFollowingShipmentDetails( String Refno, String Clientrefno)throws InterruptedException {
		cargo_Booking.userEnttersFollowingShipmentDetails(Refno,Clientrefno);	
	}
	@When("User clicks on Invoice and GST details tab")
	public void userClicksOnInvoiceAndGSTDetailsTab()throws InterruptedException {
		cargo_Booking.userClicksOnInvoiceAndGSTDetailsTab();
	}
	@When("User enters Invoice and GST details {string},{string},{string}")
	public void userEntersInvoiceAndGSTDetails(String invoiceNumber, String invoiceValue, String ewbNumber)throws InterruptedException {
		cargo_Booking.userEntersInvoiceAndGSTDetails(invoiceNumber,invoiceValue,ewbNumber);
	}
	
	@Then("User clicks on Save and Next button and Validate the Invoice and GST details result")
	public void userClicksOnSaveAndNextButtonAndValidateTheInvoiceAndGSTDetailsResult()throws InterruptedException {
		cargo_Booking.userClicksOnSaveAndNextButtonAndValidateTheInvoiceAndGSTDetailsResult();
		
	}
	
	@When("user clicks on innformation tab")
	public void userClicksOnInnformationTab()throws InterruptedException {
		cargo_Booking.clickInformationButton();
	}
	
	@Then("validate the information error message {string}")
	public void validateTheInformationErrorMessage(String informationerrormesssage)throws InterruptedException {
		cargo_Booking.validateTheInformationErrorMessage(informationerrormesssage);
	}
	
	@When("User clicks on Value Added Services tab")
	public void userClicksOnValueAddedServicesTab()throws InterruptedException {
		cargo_Booking.userClicksOnValueAddedServicesTab();
	}
	
	@When("user enters the Hold days {string}")
	public void user_enters_the_Hold_days(String Holddays)throws InterruptedException {
		cargo_Booking.holdDaysentry(Holddays);
	}
	@When("User enters delivery time {string},{string}")
	public void userEntersDeliveryTime(String RequestedFromTime, String RequestedToTime)throws InterruptedException {
		cargo_Booking.userEntersDeliveryTime(RequestedFromTime,RequestedToTime);
	}
	@Then("User clicks on save button and validate")
	public void userClicksOnSaveButtonAndValidate()throws InterruptedException {
		cargo_Booking.userClicksOnSaveButtonAndValidate();
		
	}
	
	@Then("validate thee error messages {string},{string},{string}")
	public void validateTheeErrorMessages(String ErrorInvoiceNumber,String ErrorInvoiceValue,String ErrorEWBNumber)throws InterruptedException {
		cargo_Booking.errorMessageValidation(ErrorInvoiceNumber,ErrorInvoiceValue,ErrorEWBNumber);
	}
	
	@When("user clicks on Expand all button and collapse All button")
	public void userClicksOnExpandAllButtonAndCollapseAllButton()throws InterruptedException {
		cargo_Booking.userClicksOnExpandAllButtonAndCollapseAllButton();
	}

	@When("Userr enters Invoice and GST details {string},{string}")
	public void userEnterInvoiceDetails(String invoiceNumber, String invoiceValue)throws InterruptedException {
		cargo_Booking.userEnterInvoiceDetails(invoiceNumber,invoiceValue);	
	}

	@Then("User validates the Invoice and GST details result {string}")
	public void userValidatesInvoiceResult(String invoicegeneratedText)throws InterruptedException {
		cargo_Booking.userValidatesInvoiceResult(invoicegeneratedText);
	}
	
	@Then("validate ewb error message {string}")
	public void validateEwbErrorMessage(String ewbErrorMessage)throws InterruptedException {
		cargo_Booking.validateEwbErrorMessage(ewbErrorMessage);
	}
	@When("User enters multiple Invoice and GST details {string},{string},{string},{string},{string},{string}")
	public void userEntersMultipleInvoiceandGSTDetails(String invoiceNumber, String invoiceValue, String ewbNumber, String invoiceNumber1, String invoiceValue1, String ewbNumber1)throws InterruptedException {
		cargo_Booking.userEntersMultipleInvoiceandGSTDetails(invoiceNumber,invoiceValue, ewbNumber, invoiceNumber1, invoiceValue1, ewbNumber1);
	}
	@Then("Validate the invoice message {string}")
	public void validateTheInvoiceMessage(String errormessageinvoice)throws InterruptedException {
		cargo_Booking.validateTheInvoiceMessage(errormessageinvoice);
	}
	@When("Usser enters following invoice and gst details {string},{string},{string}")
	public void usserEntersFollowingInvoiceAndGstDetails(String invoiceNumber, String invoiceValue, String ewbNumber)throws InterruptedException {
		cargo_Booking.usserEntersFollowingInvoiceAndGstDetails(invoiceNumber,invoiceValue, ewbNumber);
	}
	
	
	
}


	
	













	
	
	
	

