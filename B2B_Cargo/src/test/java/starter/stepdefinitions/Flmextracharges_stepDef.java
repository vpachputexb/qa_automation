package starter.stepdefinitions;

import com.xb.cafe.ui.pages.Cargo;
import com.xb.cafe.ui.pages.Cargo_FLM_AddExtraCharges;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.webdriver.SupportedWebDriver;

public class Flmextracharges_stepDef {
	
	@Steps
	Cargo_FLM_AddExtraCharges cargoflmaddextracharges;

	@Managed
	SupportedWebDriver browser;
		
		@Given("User Navigaates to Trip Management")
		public void userNavigaatesToTripManagement() throws InterruptedException {
			cargoflmaddextracharges.userNavigaatesToTripManagement();
		}
		@When("User Enters the Add Extra Charges Details {string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string}")
		public void userEntersTheAddExtraChargesDetails( String AllocationType2, String allocationType,String shippingId,String fromDate,String toDate, String shipmentSizedCharges,String varaiCharges,String portgagesCharges,String specialDeliveryCharges,String loadingUnloadingCharges,String odaCharges,String fullTruckLoad)throws InterruptedException {
			
			cargoflmaddextracharges.userEntersTheAddExtraChargesDetails( AllocationType2,allocationType,shippingId,fromDate,toDate,shipmentSizedCharges,varaiCharges,portgagesCharges,specialDeliveryCharges,loadingUnloadingCharges,odaCharges,fullTruckLoad);
		}

		@Then("Click on Save button and validate the result {string}")
		public void clickOnSaveButtonAndValidateTheResult( String successmessage)throws InterruptedException {
			cargoflmaddextracharges.clickOnSaveButtonAndValidateTheResult( successmessage);
			
		}
		@When("User Enters the following Add Extra Charges Details {string},{string},{string},{string},{string}")
		public void userEntersTheFollowingAddExtraChargesDetails( String AllocationType2,String allocationType,String shippingId,String fromDate,String toDate)throws InterruptedException {
			cargoflmaddextracharges.userEntersTheFollowingAddExtraChargesDetails( AllocationType2,allocationType,shippingId,fromDate,toDate);
		}
		@Then("click on clear button and Validate the button functionality")
		public void clickOnClearButtonAndValidateTheButtonFunctionality()throws InterruptedException {
			cargoflmaddextracharges.clickOnClearButtonAndValidateTheButtonFunctionality();
		}
		@When("User Enters the Add Extra Charges Details as of following {string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string}")
		public void userEntersTheAddExtraChargesDetailsAsOfFollowing( String AllocationType2,String allocationType,String shippingId,String fromDate,String toDate, String shipmentSizedCharges,String varaiCharges,String portgagesCharges,String specialDeliveryCharges,String loadingUnloadingCharges,String odaCharges,String fullTruckLoad)throws InterruptedException {
			cargoflmaddextracharges.userEntersTheAddExtraChargesDetailsAsOfFollowing( AllocationType2,allocationType,shippingId,fromDate,toDate,shipmentSizedCharges,varaiCharges,portgagesCharges,specialDeliveryCharges,loadingUnloadingCharges,odaCharges,fullTruckLoad);
		}
		@Then("click on final clear button and Validate the button functionality")
		public void clickOnFinalClearButtonAndValidateTheButtonFunctionality()throws InterruptedException {
			cargoflmaddextracharges.clickOnFinalClearButtonAndValidateTheButtonFunctionality();
		}
		@Then("click on export button and validate the button functionality")
		public void clickOnExportButtonAndValidateTheButtonFunctionality()throws InterruptedException {
			cargoflmaddextracharges.clickOnExportButtonAndValidateTheButtonFunctionality();
			
		}
		@Then("Click on Save button and validate the  error message {string}")
		public void clickOnSaveButtonAndValidateTheErrorMessage( String errorMessageExtracharge)throws InterruptedException {
			cargoflmaddextracharges.clickOnSaveButtonAndValidateTheErrorMessage( errorMessageExtracharge);
		}
		@Then("user clicks on upload button and validate upload functionality {string},{string},{string},{string},{string},{string},{string},{string}")
		public void userClicksOnUploadButton(String shipmentSizedUpload,String mathadiChargesUpload,String portgageChargesUpload,String specialDeliveryChargesUpload,String loadingChargesUpload,String odaChargesUpload,String fullTruckLoadUpload, String successfulUploadMessage)throws InterruptedException {
			cargoflmaddextracharges.userClicksOnUploadButton(shipmentSizedUpload,mathadiChargesUpload,portgageChargesUpload,specialDeliveryChargesUpload,loadingChargesUpload,odaChargesUpload,fullTruckLoadUpload,successfulUploadMessage);
}
		@When("user clicks on upload button and uploads the invalid file {string},{string},{string},{string},{string},{string},{string}")
		public void userClicksOnUploadButtonAndUploadsTheInvalidFile(String shipmentSizedUpload,String mathadiChargesUpload,String portgageChargesUpload,String specialDeliveryChargesUpload,String loadingChargesUpload,String odaChargesUpload,String fullTruckLoadUpload)throws InterruptedException {
			cargoflmaddextracharges.userClicksOnUploadButtonAndUploadsTheInvalidFile(shipmentSizedUpload,mathadiChargesUpload,portgageChargesUpload,specialDeliveryChargesUpload,loadingChargesUpload,odaChargesUpload,fullTruckLoadUpload);
		}
		@Then("validate the invalid upload error message {string}")
		public void validateTheInvalidUploadErrorMessage(String invalidUploadErrorMessage)throws InterruptedException {
			cargoflmaddextracharges.validateTheInvalidUploadErrorMessage(invalidUploadErrorMessage);
		}
		@When("user uploads large file {string},{string},{string},{string},{string},{string},{string}")
		public void userUploadsLargeFile(String shipmentSizedUpload,String mathadiChargesUpload,String portgageChargesUpload,String specialDeliveryChargesUpload,String loadingChargesUpload,String odaChargesUpload,String fullTruckLoadUpload)throws InterruptedException {
			cargoflmaddextracharges.userUploadsLargeFile(shipmentSizedUpload,mathadiChargesUpload,portgageChargesUpload,specialDeliveryChargesUpload,loadingChargesUpload,odaChargesUpload,fullTruckLoadUpload);
		}
		@Then("validate the large file size error message {string}")
		public void validateTheLargeFileSizeErrorMessage(String largeFileSizeErrorMessage)throws InterruptedException {
			cargoflmaddextracharges.validateTheLargeFileSizeErrorMessage(largeFileSizeErrorMessage);
		}
		@Then("validate the from date alert message {string}")
		public void validateTheFromDateAlertMessage(String fromDateAlertMessage)throws InterruptedException {
			cargoflmaddextracharges.validateTheFromDateAlertMessage(fromDateAlertMessage);
		}
		@When("user enters the allocation type and from date details {string},{string},{string}")
		public void userEntersTheAllocationTypeAndfromDateDetails(String AllocationType2,String allocationType,String fromDate)throws InterruptedException {
			cargoflmaddextracharges.userEntersTheAllocationTypeAndfromDateDetails(AllocationType2,allocationType,fromDate);
		}
		@Then("validate the to date alert message {string}")
		public void validateTheToDateAlertMessage(String toDateAlertMessage)throws InterruptedException {
			cargoflmaddextracharges.validateTheToDateAlertMessage(toDateAlertMessage);
		}
		@When("user enters the allocation type and date details {string},{string},{string},{string}")
		public void userEntersTheAllocationTypeAndDateDetails(String AllocationType2,String allocationType,String fromDate,String toDate)throws InterruptedException {
			cargoflmaddextracharges.userEntersTheAllocationTypeAndDateDetails(AllocationType2,allocationType,fromDate,toDate);
		}
		@Then("validate the no records found {string}")
		public void validateTheNoRecordsFound(String noRecordsFoundAlertMessage)throws InterruptedException {
			cargoflmaddextracharges.validateTheNoRecordsFound(noRecordsFoundAlertMessage);
		}
		@Then("validate the less to date input value {string}")
		public void validateTheLessToDateInputValue(String lowerToDateAlertMessage)throws InterruptedException {
			cargoflmaddextracharges.validateTheLessToDateInputValue(lowerToDateAlertMessage);
		}
		@When("user enters allocation type and shipping and date details {string},{string},{string}")
		public void userEntersAllocationTypeAndShippingAndDateDetails(String AllocationType2,String allocationType,String shippingId)throws InterruptedException {
			cargoflmaddextracharges.userEntersAllocationTypeAndShippingAndDateDetails(AllocationType2,allocationType,shippingId);
		}
		@When("user hits the upload button {string}")
		public void userHitsTheUploadButton(String shipmentSizedUpload)throws InterruptedException {
			cargoflmaddextracharges.userHitsTheUploadButton(shipmentSizedUpload);
		}
		@Then("user clicks on cancel button and validate the upload file cancel button functionality {string}")
		public void userClicksOnCancelButtonAndValidateTheUploadFileCancelButtonFunctionality(String cancelButtonAlertMessage)throws InterruptedException {
			cargoflmaddextracharges.userClicksOnCancelButtonAndValidateTheUploadFileCancelButtonFunctionality(cancelButtonAlertMessage);
		}
		@Then("user clicks on view button and validate the upload file view button functionality")
		public void userClicksOnViewButtonAndValidateTheUploadFileViewButtonFunctinality()throws InterruptedException {
			cargoflmaddextracharges.userClicksOnViewButtonAndValidateTheUploadFileViewButtonFunctinality();
			
		}
		@Then("user clicks on close button of the file opened and validate click functionality")
		public void userClicksOnCloseButtonOfTheFileOpenedAndValidateClickFunctionality()throws InterruptedException {
			cargoflmaddextracharges.userClicksOnCloseButtonOfTheFileOpenedAndValidateClickFunctionality();
		}
	@Then("user clicks on search button and validate the awb error message {string}")
	public void userClicksOnSearchButtonAndValidateTheAwbErrorMessage(String AwbErrorMessage)throws InterruptedException {
		cargoflmaddextracharges.userClicksOnSearchButtonAndValidateTheAwbErrorMessage(AwbErrorMessage);
		
	}
	@When("Enters the Add Extra Charges Details {string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string}")
    public void enterTheAddExtraChargesDetails(String AllocationType2, String allocationType,String shippingId,String fromDate,String toDate, String shipmentSizedCharges,String varaiCharges,String portgagesCharges,String specialDeliveryCharges,String loadingUnloadingCharges,String odaCharges,String fullTruckLoad)throws InterruptedException {
            cargoflmaddextracharges.enterTheAddExtraChargesDetails(AllocationType2,allocationType,shippingId,fromDate,toDate,shipmentSizedCharges,varaiCharges,portgagesCharges,specialDeliveryCharges,loadingUnloadingCharges,odaCharges,fullTruckLoad);
    }
}
