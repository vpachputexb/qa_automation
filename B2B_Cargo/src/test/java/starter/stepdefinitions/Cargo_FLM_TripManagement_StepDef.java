package starter.stepdefinitions;


import com.xb.cafe.ui.pages.Cargo_FLM_TripManagement;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.webdriver.SupportedWebDriver;

public class Cargo_FLM_TripManagement_StepDef  {
	
	@Steps
	Cargo_FLM_TripManagement cargo_FLM_TripManagement;
	
	@Managed
	SupportedWebDriver browser;
	

	@Given("User Navigates to Trip Management")
	public void userNavigatesToTripManagement() throws InterruptedException {
		cargo_FLM_TripManagement.userNavigatesToTripManagement();
	}
	
	@Given("User Navigates to Create Booking Trip Tab")
	public void userNavigatesToCreateBookingTripTab() throws InterruptedException {
		cargo_FLM_TripManagement.userNavigatesToCreateBookingTripTab();
	}
	
	@Given("User Enters Assignment Person and AD hoc Vehicle Details {string},{string},{string},{string}")
	public void userEntersAssignmentPersonAndAdHocVehicleDetails(String assignedTo, String adHocVehicleModel, String adHocVehicleNumber, String adHocVehicleType) throws InterruptedException {
		cargo_FLM_TripManagement.userEntersAssignmentPersonAndAdHocVehicleDetails(assignedTo, adHocVehicleModel, adHocVehicleNumber, adHocVehicleType);
	}
	
	@When("User Enters Details In AddShipment Tab One AWB {string}")
	public void userEntersDetailsInAddShipmentTabOneAWB(String aWBNumber) throws InterruptedException {
		cargo_FLM_TripManagement.userEntersDetailsInAddShipmentTabOneAWB(aWBNumber);
	}
	
	@When("User Assigns AWBNumber To Trip")
	public void userAssignsAWBNumberToTrip() throws InterruptedException {
		cargo_FLM_TripManagement.userAssignsAWBNumberToTrip();
	}
	
	@Then("User Enters ODO Reading {string}")
	public void userEntersODOReading(String oDOReading) throws InterruptedException {
		cargo_FLM_TripManagement.userEntersODOReading(oDOReading);
	}
	
	@Then("User Scans AWBNumber & ValidateSucessMessage {string},{string}")
	public void userScansAWBNumberAndValidateSucessMessage(String awbNumber, String message) throws InterruptedException {
		cargo_FLM_TripManagement.userScansAWBNumberAndValidateSucessMessage(awbNumber, message);
	}
	@Then("Validate Trip created details {string},{string},{string}")
	public void validateTripCreatedDetails(String aWBNumber, String assignedTo,String oDOReading) throws InterruptedException {
		cargo_FLM_TripManagement.validateTripCreatedDetails(aWBNumber, assignedTo, oDOReading);
	}
	
	@When("User Enters Assignment Person and contracted Vehicle Details {string},{string}")
	public void userEntersAssignementPersonAndContractedVehicleDetails(String assignedTo,String contractedVehicleNumber) throws InterruptedException {
		cargo_FLM_TripManagement.userEntersAssignementPersonAndContractedVehicleDetails(assignedTo, contractedVehicleNumber);
	}
	
	@When("User Enters Details In AddShipment Tab Three AWB {string},{string},{string}")
	public void userEntersDetailsInAddShipmentTabThreeAWB(String aWBNumber1,String aWBNumber2,String aWBNumber3) throws InterruptedException {
		cargo_FLM_TripManagement.userEntersDetailsInAddShipmentTabThreeAWB(aWBNumber1, aWBNumber2, aWBNumber3);
	}
	
	@Then("User Scans Multiple AWBNumber & ValidateSucessMessage {string},{string},{string}")
	public void userScansMultipleAWBNumberAndValidateSuccessMessage(String aWBNumber1,String aWBNumber2, String message) throws InterruptedException {
		cargo_FLM_TripManagement.userScansMultipleAWBNumberAndValidateSuccessMessage(aWBNumber1, aWBNumber2, message);
	}
	@Then("Capture the AWBNumber Generated")
	public void captureTheAWBNumberGenerated() {
		cargo_FLM_TripManagement.captureTheAWBNumberGenerated();
	}
	@When("User Enters Details In AddShipment Tab Booking Way")
	public void userEntersDetailsInShipmentTabBookingWay() throws InterruptedException {
		cargo_FLM_TripManagement.userEntersDetailsInShipmentTabBookingWay();
	}
	@Then("User Scans AWBNumber & ValidateSucessMessage Booking Way{string}")
	public void userScansAWBNumberAndValidateSuccessMessageBookingWay(String message) throws InterruptedException {
		cargo_FLM_TripManagement.userScansAWBNumberAndValidateSuccessMessageBookingWay(message);
	}
	@Then("Validate Trip created details Booking Way {string},{string}")
	public void validateTripCreatedDetailsBookingWay(String assignedTo,String oDOReading) throws InterruptedException {
		cargo_FLM_TripManagement.validateTripCreatedDetailsBookingWay(assignedTo, oDOReading);
	}
	@Then("Validate Trip created details Multiple {string},{string},{string},{string}")
	public void validateTripCreatedDetailsMultiple(String aWBNumber1,String aWBNumber2,String assignedTo,String oDOReading) throws InterruptedException {
		cargo_FLM_TripManagement.validateTripCreatedDetailsMultiple(aWBNumber1, aWBNumber2, assignedTo, oDOReading);
	}
	
	@Given("User opens the Trip details {string},{string}")
	public void userOpensTheTripDetails(String tripStatus, String baSR) throws InterruptedException {
		cargo_FLM_TripManagement.userOpensTheTripDetails(tripStatus, baSR);
	}
	@When("User Validates Trip Details {string},{string}")
	public void userValidatesTripDetails(String AWBNumber,String mpsNumber) {
		cargo_FLM_TripManagement.userValidatesTripDetails(AWBNumber, mpsNumber);
	
	}
	
    @When("User Navigates To Close Trip and Scans AWBs {string},{string}")
	public void userNavigatesToCloseTripButtonAndScansAWB(String AWBNumber, String numberOfPhysicalINvoiceInput) throws InterruptedException {
    	cargo_FLM_TripManagement.userNavigatesToCloseTripButtonAndScansAWB(AWBNumber, numberOfPhysicalINvoiceInput);
	}
    @Then("user Enters closure ODO reading {string}")
    public void userEntersClosureODOReading(String vehicleCloseODOReading) throws InterruptedException{
    	cargo_FLM_TripManagement.userEntersClosureODOReading(vehicleCloseODOReading);
    }
    @Then("User closes trip and validates trip closure message{string}")
    public void userClosesTripAndValidateTripClosureMessage(String message) throws InterruptedException {
    	cargo_FLM_TripManagement.userClosesTripAndValidateTripClosureMessage(message);
    }
    @When("User Validates Trip Details Multiple {string},{string},{string},{string},{string},{string}")
	public void userValidatesTripDetailsMultiple(String AWBNumber1,String mpsNumber1,String AWBNumber2,String mpsNumber2,String AWBNumber3,String mpsNumber3) {
		cargo_FLM_TripManagement.userValidatesTripDetailsMultiple(AWBNumber1, mpsNumber1, AWBNumber2, mpsNumber2, AWBNumber3, mpsNumber3);
	
	}
    @When("User Navigates To Close Trip and Scans AWBs Multiple {string},{string},{string},{string},{string},{string},{string},{string},{string}")
    public void userNavigatesToCloseTripButtonAndScansAWBMultiple(String AWBNumber1,String mpsNumber1, String numberOfPhysicalINvoiceInput1,String AWBNumber2,String mpsNumber2,String numberOfPhysicalINvoiceInput2,String AWBNumber3, String mpsNumber3,String numberOfPhysicalINvoiceInput3) throws InterruptedException {
    	cargo_FLM_TripManagement.userNavigatesToCloseTripButtonAndScansAWBMultiple(AWBNumber1, mpsNumber1, numberOfPhysicalINvoiceInput1, AWBNumber2, mpsNumber2, numberOfPhysicalINvoiceInput2, AWBNumber3, mpsNumber3, numberOfPhysicalINvoiceInput3);
	}
    
    
}
