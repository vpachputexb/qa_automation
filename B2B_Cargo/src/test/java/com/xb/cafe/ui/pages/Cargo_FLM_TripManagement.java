package com.xb.cafe.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;



import net.serenitybdd.core.pages.PageObject;

public class Cargo_FLM_TripManagement extends PageObject {
	
	public final By firstMileLastMileLabelLocator=By.xpath("//span[contains(text(),'First & Last Mile')]");
	public final By tripManagementLabelLocator=By.xpath("//span[contains(text(),'Trip Management')]");
	
	public final By createBookingBttn=By.xpath("//i[@class='fa fa-plus-square-o fa-2x']");
	public final By expandAllBttn=By.xpath("//button[@title='Expand All']"); 
	public  final By selectUserLocator=By.xpath("//input[@class='vue-treeselect__input']");
	public  final By vehicleModelLocator=By.xpath("(//input[@class='vue-treeselect__input'])[2]");
	public  final By vehicleNumberLocator=By.xpath("//input[@id='adHocVehicleNumber']");
	public  final By assignBAVehicleSavebttn=By.xpath("//button[@title='Save']");
	public  final By Btn_OKBA = By.xpath("//button[@class='swal2-confirm swal2-styled']");
	public  final By expandAllSecondButon=By.xpath("//label[contains(text(),'Not added to the trip')]");
	public  final By filteredRecordsLabelLocator=By.xpath("(//b[contains(text(),'Filtered Records:')])[1]");
	public  final By selectedRecordsLabelLocator=By.xpath("(//b[contains(text(),'Selected Records:')])[1]");
	public  final By assignToTheTripLocator=By.xpath("//button[@title='Assign To The Trip']");
	public  final By odoReadingInputLocator=By.xpath("//input[@id='vehicleODOReading']");
	public  final By startTripButtonLocator=By.xpath("//button[contains(text(),'Start Trip')]");
	public  final By scanDocumentLocator=By.xpath("(//input[@name='Scandocument'])[1]");
	public  final By assignToTheTripButtonLocator=By.xpath("//button[@title='Assign To The Trip']");
	public final By tripIDLocator=By.xpath("(//th[contains(text(),'Trip ID:')]/following-sibling::td[1])[1]");
	public final By tripIDFilterInput=By.xpath("//input[@id='tripid']");
	public final By filterButton=By.xpath("//i[@title='Search Filter']");
	public final By resetButton=By.xpath("//button[@type='reset']");
	public final By searchButton=By.xpath("//button[@type='button']");
	public final By viewModeButton=By.xpath("//span[@title='View']");
	public  final By contractedVehiclelLocator=By.xpath("(//input[@class='vue-treeselect__input'])[2]");
	public  final By aWBNumberGeneratedLocator=By.xpath("//input[@id='awbNo']");
	
	public final By closeTripButton=By.xpath("//button[contains(text(),'Close Trip')]");
	public final By scanAWBInput=By.xpath("//input[@id='scanAWB']");
	public final By numberOfPhysicalInput=By.xpath("//input[@id='physicalInvoices']");
	public final By scanMPSInput=By.xpath("//input[@id='scanMPS']");
	public final By vehicleCloseODOReadingInput=By.xpath("//input[@id='VehicleCloseODOreading']");
	public final By saveButton=By.xpath("//button[contains(text(),'Save')]");
	public final By okButton=By.xpath("(//button[contains(text(),'OK')])[3]");
	
	
	
	SoftAssert softAssert = new SoftAssert();
	public  String awbBookingGenerated;
	
	public  void userNavigatesToTripManagement() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		
		
		try {
			getDriver().navigate().refresh();
			Thread.sleep(4000);
			wait.until(ExpectedConditions.elementToBeClickable(tripManagementLabelLocator));
			getDriver().findElement(tripManagementLabelLocator).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			Thread.sleep(4000);
			wait.until(ExpectedConditions.elementToBeClickable(firstMileLastMileLabelLocator));
			getDriver().findElement(firstMileLastMileLabelLocator).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(tripManagementLabelLocator));
			getDriver().findElement(tripManagementLabelLocator).click();
			Thread.sleep(3000);
		}
	}
	
	public  void userNavigatesToCreateBookingTripTab() throws InterruptedException {
		Thread.sleep(5000);
		getDriver().findElement(createBookingBttn).click();
	
	}
	public  void userEntersAssignmentPersonAndAdHocVehicleDetails(String assignedTo, String adHocVehicleModel, String adHocVehicleNumber, String adHocVehicleType) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(6000);
		getDriver().findElement(expandAllBttn).click();
		Thread.sleep(3000);
		WebElement userTxtbox=getDriver().findElement(selectUserLocator);
		userTxtbox.sendKeys(assignedTo);
		Thread.sleep(3000);
		userTxtbox.sendKeys(Keys.ENTER);
		WebElement vehicleTxtbox=getDriver().findElement(vehicleModelLocator);
		vehicleTxtbox.sendKeys(adHocVehicleModel);
		Thread.sleep(3000);
		vehicleTxtbox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("//label[contains(text(),'ABC')]".replace("ABC", adHocVehicleType))).click();
		getDriver().findElement(vehicleNumberLocator).sendKeys(adHocVehicleNumber);
		Thread.sleep(1000);
		getDriver().findElement(assignBAVehicleSavebttn).click();
	
		wait.until(ExpectedConditions.visibilityOfElementLocated(Btn_OKBA));
		Thread.sleep(3000);
		getDriver().findElement(Btn_OKBA).click();
	}
	public void userEntersDetailsInAddShipmentTabOneAWB(String aWBNumber) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
//		Thread.sleep(30000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(expandAllSecondButon));
//		Thread.sleep(7000);
		getDriver().findElement(expandAllSecondButon).click();
		Thread.sleep(2000);
		String filteredRecordsTxt=getDriver().findElement(filteredRecordsLabelLocator).getText();
		System.out.println(filteredRecordsTxt+"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		String numberOnly = filteredRecordsTxt.replaceAll("\\D+", "");
		int totalfilteredRecordrecordsNumerical=Integer.parseInt(numberOnly);
		System.out.println(getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr[1]/td[2]/span")).getText()+">>>>>>>>>>>>>>>>>>>>>>>>");
		awbSelectionForTrip(aWBNumber);
		
	}
	
	public void awbSelectionForTrip(String awbNumber) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		Thread.sleep(20000);
		Boolean flag=false;
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		do {
		for(int i=1;i<=10;i++) {
			if(getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr["+i+"]/td[2]/span")).getText().equals(awbNumber)) 
					{
					flag=true;
					Thread.sleep(5000);
					System.out.println("11111111111111");
					je.executeScript("arguments[0].scrollIntoView(true);",getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr["+i+"]/td[1]")));
					getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr["+i+"]/td[1]")).click();
					System.out.println("22222");
					break;
					}
			}
			if(flag==false) {
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@aria-label='Go to next page']")));
					System.out.println("33333");
					getDriver().findElement(By.xpath("//button[@aria-label='Go to next page']")).click();
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@aria-label='Go to next page']")));
					System.out.println("44444");
				}
			
	} while(!flag);
	
	System.out.println("555555");
	Thread.sleep(5000);
	}
	
	public void userAssignsAWBNumberToTrip() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
//		Thread.sleep(30000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(expandAllSecondButon));
		Thread.sleep(2000);
		getDriver().findElement(assignToTheTripButtonLocator).click();
		
		Thread.sleep(15000);
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		je.executeScript("arguments[0].scrollIntoView(true);",getDriver().findElement(startTripButtonLocator));
		getDriver().findElement(startTripButtonLocator).click();
		Thread.sleep(5000);
	}
	
	public void userEntersODOReading(String oDOReading) throws InterruptedException {
	 getDriver().findElement(odoReadingInputLocator).sendKeys(oDOReading);
	 Thread.sleep(2000);
	}
	
	public void userScansAWBNumberAndValidateSucessMessage(String awbNumber, String message) throws InterruptedException {
	 getDriver().findElement(scanDocumentLocator).sendKeys(awbNumber);
	 Thread.sleep(5000);
	 getDriver().findElement(startTripButtonLocator).click();
	 Thread.sleep(5000);
	 
	 softAssert.assertEquals(getDriver().findElement(By.xpath("//div[contains(text(),'ABC')]".replace("ABC", message))).getText(), message);
	 softAssert.assertAll();
	 Thread.sleep(2000);
	 
	 getDriver().findElement(Btn_OKBA).click();
	 
	 String tripID=getDriver().findElement(tripIDLocator).getText();
	 
	 
	}
	
	public void validateTripCreatedDetails(String aWBNumber, String assignedTo,String oDOReading) throws InterruptedException {
		String tripID=getDriver().findElement(tripIDLocator).getText();
		getDriver().navigate().refresh();
		Thread.sleep(2000);
		getDriver().findElement(filterButton).click();
		Thread.sleep(2000);
		getDriver().findElement(resetButton).click();
		Thread.sleep(2000);
		getDriver().findElement(tripIDFilterInput).sendKeys(tripID);
		Thread.sleep(2000);
		getDriver().findElement(searchButton).click();
		Thread.sleep(2000);
		Thread.sleep(2000);
		getDriver().findElement(viewModeButton).click();
		Thread.sleep(2000);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", aWBNumber))).getText(), aWBNumber);
		softAssert.assertAll();
		
	}
	
	public void userEntersAssignementPersonAndContractedVehicleDetails(String assignedTo,String contractedVehicleNumber) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(6000);
		getDriver().findElement(expandAllBttn).click();
		Thread.sleep(3000);
		WebElement userTxtbox=getDriver().findElement(selectUserLocator);
		userTxtbox.sendKeys(assignedTo);
		Thread.sleep(3000);
		userTxtbox.sendKeys(Keys.ENTER);
		WebElement contractedVehicleTxtbox=getDriver().findElement(contractedVehiclelLocator);
		contractedVehicleTxtbox.sendKeys(contractedVehicleNumber);
		Thread.sleep(3000);
		contractedVehicleTxtbox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		getDriver().findElement(assignBAVehicleSavebttn).click();
	
		wait.until(ExpectedConditions.visibilityOfElementLocated(Btn_OKBA));
		Thread.sleep(3000);
		getDriver().findElement(Btn_OKBA).click();
		
	}
	
	public void userEntersDetailsInAddShipmentTabThreeAWB(String aWBNumber1,String aWBNumber2,String aWBNumber3) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(20000);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(expandAllSecondButon));
//		Thread.sleep(7000);
		getDriver().findElement(expandAllSecondButon).click();
		Thread.sleep(2000);
		String filteredRecordsTxt=getDriver().findElement(filteredRecordsLabelLocator).getText();
		System.out.println(filteredRecordsTxt+"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		String numberOnly = filteredRecordsTxt.replaceAll("\\D+", "");
		int totalfilteredRecordrecordsNumerical=Integer.parseInt(numberOnly);
		System.out.println(getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr[1]/td[2]/span")).getText()+">>>>>>>>>>>>>>>>>>>>>>>>");
		awbSelectionForTrip(aWBNumber1);
		Thread.sleep(3000);
		awbSelectionForTrip(aWBNumber2);
		Thread.sleep(3000);
		awbSelectionForTrip(aWBNumber3);
		Thread.sleep(3000);
	}
	
	public void userScansMultipleAWBNumberAndValidateSuccessMessage(String aWBNumber1,String aWBNumber2, String message) throws InterruptedException {
		 getDriver().findElement(scanDocumentLocator).sendKeys(aWBNumber1);
		 Thread.sleep(5000);
		 getDriver().findElement(scanDocumentLocator).sendKeys(aWBNumber2);
		 Thread.sleep(5000);
		 getDriver().findElement(startTripButtonLocator).click();
		 Thread.sleep(5000);
		 
		 softAssert.assertEquals(getDriver().findElement(By.xpath("//div[contains(text(),'ABC')]".replace("ABC", message))).getText(), message);
		 softAssert.assertAll();
		 Thread.sleep(2000);
		 
		 getDriver().findElement(Btn_OKBA).click();
	
	}
	public void captureTheAWBNumberGenerated() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(aWBNumberGeneratedLocator));
		String awbBookingGenerated=getDriver().findElement(aWBNumberGeneratedLocator).getAttribute("value");
	}
	
	public void userEntersDetailsInShipmentTabBookingWay() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(20000);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(expandAllSecondButon));
//		Thread.sleep(7000);
		getDriver().findElement(expandAllSecondButon).click();
		Thread.sleep(2000);
		String filteredRecordsTxt=getDriver().findElement(filteredRecordsLabelLocator).getText();
		System.out.println(filteredRecordsTxt+"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		String numberOnly = filteredRecordsTxt.replaceAll("\\D+", "");
		int totalfilteredRecordrecordsNumerical=Integer.parseInt(numberOnly);
		System.out.println(getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr[1]/td[2]/span")).getText()+">>>>>>>>>>>>>>>>>>>>>>>>");
		awbSelectionForTrip(awbBookingGenerated);
		
	
	}
	public void userScansAWBNumberAndValidateSuccessMessageBookingWay(String message) throws InterruptedException {
		getDriver().findElement(scanDocumentLocator).sendKeys(awbBookingGenerated);
		 Thread.sleep(5000);
		 getDriver().findElement(startTripButtonLocator).click();
		 Thread.sleep(5000);
		 
		 softAssert.assertEquals(getDriver().findElement(By.xpath("//div[contains(text(),'ABC')]".replace("ABC", message))).getText(), message);
		 softAssert.assertAll();
		 Thread.sleep(2000);
		 
		 getDriver().findElement(Btn_OKBA).click();
		 
		 
		 
	}
	public void validateTripCreatedDetailsBookingWay(String assignedTo,String oDOReading) throws InterruptedException {
		String tripID=getDriver().findElement(tripIDLocator).getText();
		getDriver().navigate().refresh();
		Thread.sleep(2000);
		getDriver().findElement(filterButton).click();
		Thread.sleep(2000);
		getDriver().findElement(resetButton).click();
		Thread.sleep(2000);
		getDriver().findElement(tripIDFilterInput).sendKeys(tripID);
		Thread.sleep(2000);
		getDriver().findElement(searchButton).click();
		Thread.sleep(2000);
		Thread.sleep(2000);
		getDriver().findElement(viewModeButton).click();
		Thread.sleep(2000);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", awbBookingGenerated))).getText(), awbBookingGenerated);
		softAssert.assertAll();
		
	}
	
	public void validateTripCreatedDetailsMultiple(String aWBNumber1,String aWBNumber2,String assignedTo,String oDOReading) throws InterruptedException {
		String tripID=getDriver().findElement(tripIDLocator).getText();
		getDriver().navigate().refresh();
		Thread.sleep(2000);
		getDriver().findElement(filterButton).click();
		Thread.sleep(2000);
		getDriver().findElement(resetButton).click();
		Thread.sleep(2000);
		getDriver().findElement(tripIDFilterInput).sendKeys(tripID);
		Thread.sleep(2000);
		getDriver().findElement(searchButton).click();
		Thread.sleep(2000);
		Thread.sleep(2000);
		getDriver().findElement(viewModeButton).click();
		Thread.sleep(2000);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", aWBNumber1))).getText(), aWBNumber1);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", aWBNumber2))).getText(), aWBNumber2);
		
		softAssert.assertAll();
		

	}

	public void userOpensTheTripDetails(String tripStatus, String baSR) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Boolean flag=false;
		do {
			for(int i=1;i<=10;i++) {
				Thread.sleep(3000);
				if(getDriver().findElement(By.xpath("//table[@class='styled-table widthsize']//tr["+i+"]//td[4]")).getText().equals(tripStatus) && getDriver().findElement(By.xpath("//table[@class='styled-table widthsize']//tr["+i+"]//td[6]")).getText().equals(baSR) ) 
						{
						flag=true;
						Thread.sleep(1000);
						getDriver().findElement(By.xpath("//table[@class='styled-table widthsize']//tr["+i+"]//td[8]/span[2]/img[1]")).click();
						break;
						}
				}
				if(flag==false) {
						
					System.out.println("33333");
					getDriver().findElement(By.xpath("//button[contains(text(),'Next')]")).click();	
					}
				
		} while(!flag);
		
	}
		
		public void userValidatesTripDetails(String AWBNumber,String mpsNumber) {
		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", AWBNumber))).getText(), AWBNumber);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", mpsNumber))).getText(), mpsNumber);
		softAssert.assertAll();
		}
		
		public void userNavigatesToCloseTripButtonAndScansAWB(String AWBNumber, String numberOfPhysicalINvoiceInput) throws InterruptedException {
		getDriver().findElement(closeTripButton).click();
		
		Thread.sleep(3000);
		
		getDriver().findElement(expandAllBttn).click();
		Thread.sleep(2000);
		getDriver().findElement(scanAWBInput).clear();
		Thread.sleep(2000);
		getDriver().findElement(scanAWBInput).sendKeys(AWBNumber);
		Thread.sleep(2000);
		
		getDriver().findElement(numberOfPhysicalInput).sendKeys(numberOfPhysicalINvoiceInput);
		Thread.sleep(2000);
		getDriver().findElement(saveButton).click();
		Thread.sleep(2000);
		
		getDriver().findElement(scanMPSInput).sendKeys(AWBNumber);
		Thread.sleep(500);
		
		}
		public void userEntersClosureODOReading(String vehicleCloseODOReading) throws InterruptedException{
			getDriver().findElement(vehicleCloseODOReadingInput).sendKeys(vehicleCloseODOReading);
			Thread.sleep(1000);
		}
		
		public void userClosesTripAndValidateTripClosureMessage(String message) throws InterruptedException {
		getDriver().findElement(closeTripButton).click();
		Thread.sleep(3000);
		getDriver().findElement(okButton).click();
//		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", message))).getText(), message);
		Thread.sleep(5000);
		getDriver().findElement(okButton).click();
		
		}

		public void userValidatesTripDetailsMultiple(String AWBNumber1,String mpsNumber1,String AWBNumber2,String mpsNumber2,String AWBNumber3,String mpsNumber3) {
			softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", AWBNumber1))).getText(), AWBNumber1);
			softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", mpsNumber1))).getText(), mpsNumber1);
			softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", AWBNumber2))).getText(), AWBNumber2);
			softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", mpsNumber2))).getText(), mpsNumber2);
			softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", AWBNumber3))).getText(), AWBNumber3);
			softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", mpsNumber3))).getText(), mpsNumber3);
			
			softAssert.assertAll();
			
		}
		
		public void userNavigatesToCloseTripButtonAndScansAWBMultiple(String AWBNumber1,String mpsNumber1, String numberOfPhysicalINvoiceInput1,String AWBNumber2,String mpsNumber2,String numberOfPhysicalINvoiceInput2,String AWBNumber3, String mpsNumber3,String numberOfPhysicalINvoiceInput3) throws InterruptedException {
			getDriver().findElement(closeTripButton).click();
			
			Thread.sleep(3000);
			
			getDriver().findElement(expandAllBttn).click();
			Thread.sleep(1200);
			getDriver().findElement(scanAWBInput).sendKeys(AWBNumber1);
			Thread.sleep(2000);
			getDriver().findElement(numberOfPhysicalInput).sendKeys(numberOfPhysicalINvoiceInput1);
			Thread.sleep(2000);
			getDriver().findElement(saveButton).click();
			Thread.sleep(3000);
			getDriver().findElement(scanAWBInput).clear();
			Thread.sleep(2000);
			getDriver().findElement(scanAWBInput).sendKeys(AWBNumber2);
			Thread.sleep(2000);
			getDriver().findElement(numberOfPhysicalInput).clear();
			Thread.sleep(2000);
			getDriver().findElement(numberOfPhysicalInput).sendKeys(numberOfPhysicalINvoiceInput2);
			Thread.sleep(3000);
			getDriver().findElement(saveButton).click();
			Thread.sleep(2000);
			getDriver().findElement(scanAWBInput).clear();
			Thread.sleep(2000);
			getDriver().findElement(scanAWBInput).sendKeys(AWBNumber3);
			Thread.sleep(2000);
			getDriver().findElement(numberOfPhysicalInput).clear();
			Thread.sleep(2000);
			getDriver().findElement(numberOfPhysicalInput).sendKeys(numberOfPhysicalINvoiceInput3);
			Thread.sleep(2000);
			getDriver().findElement(saveButton).click();
			Thread.sleep(2000);
			getDriver().findElement(scanMPSInput).sendKeys(mpsNumber1);
			Thread.sleep(2000);
			getDriver().findElement(scanMPSInput).clear();
			Thread.sleep(2000);
			getDriver().findElement(scanMPSInput).sendKeys(mpsNumber2);
			Thread.sleep(2000);
			getDriver().findElement(scanMPSInput).clear();
			Thread.sleep(2000);
			getDriver().findElement(scanMPSInput).sendKeys(mpsNumber3);
			Thread.sleep(4000);
			
			
		}
		
	}
	

	



