package com.xb.cafe.ui.pages;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;

import org.joda.time.DateTime;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.ibm.icu.impl.duration.TimeUnit;

import ch.qos.logback.core.net.SyslogOutputStream;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Managed;
import java.io.File;

import org.joda.time.DateTime;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.ibm.icu.impl.duration.TimeUnit;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Managed;
import java.io.File;

import org.joda.time.DateTime;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.ibm.icu.impl.duration.TimeUnit;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Managed;
import java.io.File;

import org.joda.time.DateTime;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.ibm.icu.impl.duration.TimeUnit;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Managed;

public class MasterDataServiceCreation extends PageObject{

	String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	String username ;
	String TagName1;
	String TagComment1;
	String Tag_MappedEnvironment;

	@Managed()
	WebDriver driver;
	

	public  final By Master_Tab = By.xpath("//span[normalize-space()='Master Data']");
	public  final By ServiceLevel_Tab = By.xpath("//span[normalize-space()='Service Level Template']");
	public  final By Add_Service_Btn = By.xpath("//i[@class='fa fa-plus-square-o fa-2x']");
	public  final By Temp_Name_inp = By.xpath("//input[@id='TemplateName']");
	public  final By Temp_Name_Des = By.xpath("//textarea[@id='Description']");
	public  final By Surface_Rbtn = By.xpath("//label[@for='Surface']");
	public  final By Air_Rbtn = By.xpath("//label[@for='Air']");
	public  final By Upload_btn = By.xpath("//input[@id='FileUpload']");
	public  final By Ok_btn = By.xpath("//button[normalize-space()='OK']");
	public  final By Service_lbl = By.xpath("//h4[@class='header']");
	public  final By SLT_lbl = By.xpath("//label[normalize-space()='Service Level Template']");
	public  final By Client_dash = By.xpath("//span[normalize-space()='Client Dashboard']");
	
/////////////////////////////////////////////////////////////////////////////////	
	public  final By Filter_btn = By.xpath("//i[@title='Search Filter']");
	public  final By Slt_Inp = By.xpath("//input[@id='ServiceLevel']");
	public  final By Slt_Status = By.xpath("//input[@id='status']");
	public  final By Seacrh_Btn = By.xpath("//button[normalize-space()='Search']");
	public  final By Edit_Btn = By.xpath("//span[@title='Edit']");
	public  final By Invalid_File = By.xpath("//div[@id='swal2-content']");
	public  final By File_Success = By.xpath("//span[normalize-space()='Chosen File']");
	public  final By Reset_Btn = By.xpath("//button[@type='reset']");
	//public  final By NoRecords = By.xpath("//td[@class='text-center']");
	public  final By NoRecords = By.xpath("//td[contains(text(),'No Records Found')]");
	public  final By Validate_Btn = By.xpath("//button[normalize-space()='Validate File']");
	//public  final By Validation_Result = By.xpath("//label[contains(text(),'Total Records: 8, Valid Records: 0, Invalid Record')]");
	public  final By Validation_Result = By.xpath("//div[@class='col-md-8']");
	public  final By Submit_Btn = By.xpath("//button[@title='Submit']");
	public  final By OK_Btn1 = By.xpath("//button[normalize-space()='OK']");
	public  final By Sucess_Msg = By.xpath("//div[contains(text(),'Service Level Template Name Already Exists.')]");
	public  final By Download_Btn = By.xpath("//button[normalize-space()='Download (CSV)']");
	public  final By Download_Ops = By.xpath("//button[normalize-space()='Download Ops TAT master']");
	public  final By Download_Pin = By.xpath("//button[normalize-space()='Download pin code master']");
	
	SoftAssert softAssert = new SoftAssert();
	WebDriverWait wait = new WebDriverWait(getDriver(), 10);

	public void NavigatetoMasterTemplate() {
		getDriver().findElement(Master_Tab).click();		
	}

	public void SelectTabServiceLevelTemplate() throws InterruptedException {
		
		
		getDriver().findElement(ServiceLevel_Tab).click();
		
		//WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Service_lbl));
		//Thread.sleep(500);
		softAssert.assertTrue(getDriver().findElement(Service_lbl).isDisplayed(), "Expected MPS details are not shown");
		softAssert.assertAll();
	}

	public void ClickonAddNewServiceLevelButton() throws InterruptedException {
		getDriver().findElement(Add_Service_Btn).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(SLT_lbl));
		softAssert.assertTrue(getDriver().findElement(SLT_lbl).isDisplayed(), " Expected Service level Template is displayed");
		softAssert.assertAll();

	}

	public void ProvideServiceLevelTemplateName(String template_Name) throws InterruptedException {
		
		String pattern = "yyyy-MM-dd HH:mm:ss.SSS";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date(0));
		
		getDriver().findElement(Temp_Name_inp).sendKeys(template_Name + date );
				//+ TimeStamp);
		softAssert.assertTrue(getDriver().findElement(Download_Ops).isDisplayed());
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(Download_Pin).isDisplayed());
		softAssert.assertAll();
	}
	
	public void SelectRouteMode(String route_Mode) throws InterruptedException {
		//Thread.sleep(100);
        getDriver().findElement(By.xpath("//label[contains(text(),'test')]".trim().replace("test", route_Mode))).click();
        Thread.sleep(500);
		if (route_Mode.equals("Air"))  {
			
			getDriver().findElement(Upload_btn).sendKeys( "/home/sachinnagtilak/Automation Data/serenity-cucumber-starter-master_Vishal/serenity-cucumber-starter-master/OPS_TAT_Master_Air.csv");
			Thread.sleep(500);
			softAssert.assertEquals(getDriver().findElement(File_Success).getText().trim(), "Chosen File");
			softAssert.assertAll();
			getDriver().findElement(Validate_Btn).click();
			Thread.sleep(500);
			softAssert.assertEquals(getDriver().findElement(Validation_Result).getText().trim(), "Total Records: 8, Valid Records: 8, Invalid Records: 0");
			softAssert.assertAll();
			Thread.sleep(500);
	        getDriver().findElement(Submit_Btn).click();
			//softAssert.assertEquals(getDriver().findElement(Sucess_Msg).getText().trim(), "Record created successfully.");
			softAssert.assertAll();
			Thread.sleep(500);
			getDriver().findElement(OK_Btn1).click();
			Thread.sleep(500);
		}
		else{
			Thread.sleep(100);
			getDriver().findElement(Upload_btn).sendKeys( "/home/sachinnagtilak/Automation Data/serenity-cucumber-starter-master_Vishal/serenity-cucumber-starter-master/OPS_TAT_Master_Surface.csv");
			Thread.sleep(500);
			softAssert.assertEquals(getDriver().findElement(File_Success).getText().trim(), "Chosen File");
			softAssert.assertAll();
			getDriver().findElement(Validate_Btn).click();
			Thread.sleep(500);
			softAssert.assertEquals(getDriver().findElement(Validation_Result).getText().trim(), "Total Records: 7, Valid Records: 7, Invalid Records: 0");
			softAssert.assertAll();
			Thread.sleep(500);
	        getDriver().findElement(Submit_Btn).click();
			//softAssert.assertEquals(getDriver().findElement(Sucess_Msg).getText().trim(), "Record created successfully.");
			softAssert.assertAll();
			Thread.sleep(500);
			getDriver().findElement(OK_Btn1).click();
			Thread.sleep(500);
			
		}
	}
	
	public void SelectRouteModeForEdit(String route_Mode) throws InterruptedException {
		Thread.sleep(100);
        getDriver().findElement(By.xpath("//label[contains(text(),'test')]".trim().replace("test", route_Mode))).click();
       	
		if (route_Mode.equals("Air"))  {
			
			getDriver().findElement(Upload_btn).sendKeys( "/home/sachinnagtilak/Automation Data/serenity-cucumber-starter-master_Vishal/serenity-cucumber-starter-master/OPS_TAT_Master_Air.csv");
			softAssert.assertEquals(getDriver().findElement(File_Success).getText().trim(), "Chosen File");
			softAssert.assertAll();
			getDriver().findElement(Validate_Btn).click();
			Thread.sleep(500);
			softAssert.assertEquals(getDriver().findElement(Validation_Result).getText().trim(), "Total Records: 8, Valid Records: 8, Invalid Records: 0");
			softAssert.assertAll();
	        getDriver().findElement(Submit_Btn).click();
			//softAssert.assertEquals(getDriver().findElement(Sucess_Msg).getText().trim(), "Record created successfully.");
			softAssert.assertAll();
			getDriver().findElement(OK_Btn1).click();
			
		}
		else{
			Thread.sleep(100);
			getDriver().findElement(Upload_btn).sendKeys( "/home/sachinnagtilak/Automation Data/serenity-cucumber-starter-master_Vishal/serenity-cucumber-starter-master/OPS_TAT_Master_Surface.csv");	
			softAssert.assertEquals(getDriver().findElement(File_Success).getText().trim(), "Chosen File");
			softAssert.assertAll();
			getDriver().findElement(Validate_Btn).click();
			Thread.sleep(500);
			softAssert.assertEquals(getDriver().findElement(Validation_Result).getText().trim(), "Total Records: 7, Valid Records: 7, Invalid Records: 0");
			softAssert.assertAll();
	        getDriver().findElement(Submit_Btn).click();
			//softAssert.assertEquals(getDriver().findElement(Sucess_Msg).getText().trim(), "Record created successfully.");
			softAssert.assertAll();
			getDriver().findElement(OK_Btn1).click();
			
		}
	}


	public void UploadInvalidFileAndValidateTheErrorMessage(String error_Message) throws InterruptedException {
		getDriver().findElement(Upload_btn).sendKeys( "/home/sachinnagtilak/Automation Data/serenity-cucumber-starter-master_Vishal/serenity-cucumber-starter-master/Invalid_OPS.csv");
		wait.until(ExpectedConditions.visibilityOfElementLocated(Invalid_File));
		softAssert.assertEquals(getDriver().findElement(Invalid_File).getText().trim(), error_Message, "Error Message Matched");
		softAssert.assertAll();
		getDriver().findElement(Ok_btn).click();
	}
	
	public void ValidateSuccessMessageasUserClicksonOkButton() throws InterruptedException {
		
		getDriver().findElement(Upload_btn).sendKeys( "/home/sachinnagtilak/Automation Data/serenity-cucumber-starter-master_Vishal/serenity-cucumber-starter-master/OPS_TAT_Master_Air.csv");
		wait.until(ExpectedConditions.visibilityOfElementLocated(File_Success));
		softAssert.assertEquals(getDriver().findElement(File_Success).getText().trim(), "Chosen File");
		softAssert.assertAll();
		getDriver().findElement(Validate_Btn).click();
		Thread.sleep(500);
		softAssert.assertEquals(getDriver().findElement(Validation_Result).getText().trim(), "Total Records: 8, Valid Records: 8, Invalid Records: 0");
		softAssert.assertAll();
        getDriver().findElement(Submit_Btn).click();
		softAssert.assertEquals(getDriver().findElement(Sucess_Msg).getText().trim(), "Service Level Template Name Already Exists.");
		softAssert.assertAll();
		getDriver().findElement(OK_Btn1).click();
		
	}
///////////////////////////////////////////////////////////////////////////////////////////////
	public void ClickOnFilterButton() {
		getDriver().findElement(Filter_btn).click();
		
	}

	public void EnterTheAndStatus(String template_Name, String status) {
		
		getDriver().findElement(Slt_Inp).sendKeys(template_Name);
		getDriver().findElement(Slt_Status).sendKeys(template_Name);
		
	}

	public void ClickOnSearch() {
		getDriver().findElement(Seacrh_Btn).click();
		
	}

	public void VerfyEnteredTemplateisPresentInTheResult(String template_Name) {
		
		try {
			
		softAssert.assertTrue(getDriver().findElement(By.xpath("//td[contains(text(),'"+template_Name+"')]")).getText().contentEquals(template_Name));
		
		}
		catch(AssertionError e){
			
			System.out.println("Template Name is already present change the example in feature file ");
			
		}
		
		getDriver().findElement(Client_dash).click();
		
		
	}

	public void ClickOnEdit() throws InterruptedException {
		
		getDriver().findElement(Edit_Btn).click();
	}

	public void clickOnResetAndVerifyBothFieldsAreEmpty() {
		getDriver().findElement(Reset_Btn).click();
		softAssert.assertTrue(getDriver().findElement(Slt_Inp).getText().isEmpty());
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(Slt_Status).getText().isEmpty());
		softAssert.assertAll();
		
	}

	public void enterTheNonExistingTemplate(String invalid_Template_Name) {
		getDriver().findElement(Slt_Inp).sendKeys(invalid_Template_Name);
		getDriver().findElement(Seacrh_Btn).click();
	}

	
	public void validateMssage(String no_Record) {
		softAssert.assertTrue(getDriver().findElement(NoRecords).getText().contentEquals(no_Record));
		softAssert.assertEquals(getDriver().findElement(NoRecords).getText().trim(), "No Records Found");
		softAssert.assertAll();		
	}

	public void ClickonClientDashboard() {
		getDriver().findElement(Client_dash).click();
		
	}

	public void UploadInvalidFileAndValidateErrorMessage(String No_mandatory_data, String alpha_numeric, String negative_integer, String Balnk, String Origin_Destinations) throws InterruptedException {
		
		
		getDriver().findElement(Upload_btn).sendKeys( "/home/sachinnagtilak/Automation Data/serenity-cucumber-starter-master_Vishal/serenity-cucumber-starter-master/InvalidError.csv");
		wait.until(ExpectedConditions.visibilityOfElementLocated(File_Success));
		softAssert.assertEquals(getDriver().findElement(File_Success).getText().trim(), "Chosen File");
		softAssert.assertAll();
		Thread.sleep(500);
		getDriver().findElement(Validate_Btn).click();
		softAssert.assertTrue(getDriver().findElement(Download_Btn).isEnabled());
		softAssert.assertAll();
		Thread.sleep(500);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'Mandatory Fields Missing: Origin Region,Origin State,Origin City,Destination State,Destination City,Route Mode,E2E TAT')]")).getText().trim(),No_mandatory_data);
		softAssert.assertAll();
		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'Origin/Destinations Region, City, State, Route Mode combination not exists in OPS TAT')]")).getText().trim(),Origin_Destinations);
		softAssert.assertAll();
		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'Mandatory Fields Missing: E2E TAT')]")).getText().trim(),Balnk);
		softAssert.assertAll();
		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'E2E TAT should be greater than 0')]")).getText().trim(),negative_integer);
		softAssert.assertAll();
		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'E2E TAT should be an integer value')]")).getText().trim(),alpha_numeric);
		softAssert.assertAll();
	}

	public void UploadFiletoverifySuccessMessage(String route_Mode) throws InterruptedException {
		Thread.sleep(100);
        getDriver().findElement(By.xpath("//label[contains(text(),'test')]".trim().replace("test", route_Mode))).click();
       	
		if (route_Mode.equals("Air"))  {
			
			getDriver().findElement(Upload_btn).sendKeys( "/home/sachinnagtilak/Automation Data/serenity-cucumber-starter-master_Vishal/serenity-cucumber-starter-master/OPS_TAT_Master_Air.csv");
			softAssert.assertEquals(getDriver().findElement(File_Success).getText().trim(), "Chosen File");
			softAssert.assertAll();
			getDriver().findElement(Validate_Btn).click();
			Thread.sleep(500);
			softAssert.assertEquals(getDriver().findElement(Validation_Result).getText().trim(), "Total Records: 8, Valid Records: 8, Invalid Records: 0");
			softAssert.assertAll();
	        getDriver().findElement(Submit_Btn).click();
			
			getDriver().findElement(OK_Btn1).click();
			getDriver().findElement(OK_Btn1).click();
			
		}
		else{
			Thread.sleep(100);
			getDriver().findElement(Upload_btn).sendKeys( "/home/sachinnagtilak/Automation Data/serenity-cucumber-starter-master_Vishal/serenity-cucumber-starter-master/OPS_TAT_Master_Surface.csv");	
			softAssert.assertEquals(getDriver().findElement(File_Success).getText().trim(), "Chosen File");
			softAssert.assertAll();
			getDriver().findElement(Validate_Btn).click();
			Thread.sleep(500);
			softAssert.assertEquals(getDriver().findElement(Validation_Result).getText().trim(), "Total Records: 7, Valid Records: 7, Invalid Records: 0");
			softAssert.assertAll();
	        getDriver().findElement(Submit_Btn).click();
			
			getDriver().findElement(OK_Btn1).click();
			getDriver().findElement(OK_Btn1).click();
			
		}
	}

		
	}

	




