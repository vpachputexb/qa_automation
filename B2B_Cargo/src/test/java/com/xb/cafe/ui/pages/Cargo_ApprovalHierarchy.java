package com.xb.cafe.ui.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.google.inject.Key;

import net.serenitybdd.core.pages.PageObject;

public class Cargo_ApprovalHierarchy extends PageObject {

	public final By cargoMasterLink = By.xpath("//span[contains(text(),'Master Data')]");
	public final By cargoApprocalHierarchy = By.xpath("//span[contains(text(),'Approval Hierarchy')]");
	public final By approvalobjectCleint = By.xpath("//input[@value='client']");
	public final By add = By.xpath("//i[@title='Add']");
	public final By role = By.xpath("//tbody/tr[2]/td[2]/div[1]/select[1]");
	public final By user = By.xpath("(//div[@class='multiselect__tags'])[2]");
	public final By savebtn = By.xpath("//button[@type='submit']");
	public final By successmsg = By.xpath("//div[@id='swal2-content']");
	public final By errBlankRole = By.xpath("//span[contains(text(),'Please select Role.')]");
	public final By errBlankUser = By.xpath("//span[contains(text(),'Please select User.')]");
	public final By deletebtn = By.xpath("(//i[@title='Delete'])[2]");
	public final By confirmationonmsgondeletebtn = By.xpath("//div[@id='swal2-content']");
	public final By okbtn = By.xpath("//button[contains(text(),'OK')]");
	public final By cancelbtn = By.xpath("//button[contains(text(),'Cancel')]");
	public final By emailcheck = By.xpath("(//label//span[@class='cr'])[4]//i");

	SoftAssert softAssert = new SoftAssert();

	public void NavigateToMaster() throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(3000);
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(cargoApprocalHierarchy));
			getDriver().findElement(cargoApprocalHierarchy).click();

		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(cargoMasterLink).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(cargoApprocalHierarchy));
			getDriver().findElement(cargoApprocalHierarchy).click();
			Thread.sleep(3000);
		}

	}

	public void checkApprovalObjectSelection() throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(approvalobjectCleint).isSelected(),
				"Client is selected default as approval object");
		softAssert.assertAll();

	}

	public void addRow() {

		getDriver().findElement(add).click();
	}

	public void addnewRow() throws InterruptedException {
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(role).isDisplayed(), "Row added successfully");

	}

	public void selectRole(String Role, String User) throws InterruptedException {

		Select roledrpdown = new Select(getDriver().findElement(role));
		roledrpdown.selectByVisibleText(Role);
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(3000);
		getDriver().findElement(user).click();
		Thread.sleep(3000);
List<WebElement> listuser=getDriver().findElements(By.xpath("//div[@class='multiselect__content-wrapper']//span/span"));
Thread.sleep(3000);
		
		for(int i=0;i<=listuser.size()-1;i++) {
			System.out.println(listuser.get(i).getText());
			if(listuser.get(i).getText().equalsIgnoreCase(User)) {
				listuser.get(i).click();
				break;
			}
		
		}
	}

	public void clickOnSaveButton() throws InterruptedException {
		getDriver().findElement(savebtn).click();
		Thread.sleep(3000);

	}

	public void checkSuccessMessage(String Successmsg) throws InterruptedException {
		Thread.sleep(3000);
		String msg = getDriver().findElement(successmsg).getText();
		softAssert.assertEquals(msg, Successmsg, "Approval hierarchy saved");
		softAssert.assertAll();

	}

	public void ErrorMessageonBlankInput(String errorOnBlankRole, String erroronBlankUser) throws InterruptedException {
		Thread.sleep(3000);
		String errblankrole = getDriver().findElement(errBlankRole).getText();
		softAssert.assertEquals(errblankrole, errorOnBlankRole, "Error on blank Role is matched");
		softAssert.assertAll();
		String errblankuser = getDriver().findElement(errBlankUser).getText();
		softAssert.assertEquals(errblankuser, erroronBlankUser, "Error on blank user is matched");
		softAssert.assertAll();
	}

	public void ClickOnDeleteButton() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(deletebtn).click();
	}

	public void ConfirmationMessageOnDelete(String deleteConfirmationmsg) {
		String cnfrmondeletmsg = getDriver().findElement(confirmationonmsgondeletebtn).getText();
		softAssert.assertEquals(cnfrmondeletmsg, deleteConfirmationmsg, "Confirmation message on delete is matched");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(okbtn).isDisplayed(), "Ok button is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(cancelbtn).isDisplayed(), "Cancel button is displayed");
		softAssert.assertAll();
	}

	public void ShowConfirmationMessageonclickedDelete() throws InterruptedException {
		softAssert.assertTrue(getDriver().findElement(confirmationonmsgondeletebtn).isDisplayed(),
				"Confirmation message on delete is displayed");
		softAssert.assertAll();
		Thread.sleep(3000);

	}

	public void ClickOKbutton() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(okbtn).click();
	}

	public void ResultwhenClickedOK() {
		softAssert.assertTrue(getDriver().findElement(confirmationonmsgondeletebtn).isDisplayed(),
				"Confirmation popup is closed");
		softAssert.assertAll();
	}

	public void ClickonEmailCheckbox() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(emailcheck).click();
	}

	public void CheckboxSelected() throws InterruptedException {
		Thread.sleep(3000);
		softAssert.assertFalse(getDriver().findElement(emailcheck).isSelected(), "Email check box is selected");
		softAssert.assertAll();

	}

}
