package com.xb.cafe.ui.pages;

import java.util.List;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;



import net.serenitybdd.core.pages.PageObject;

public class CargoBagShipmentQuery extends PageObject {

	 String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	 String username;
	 String TagName1;
	 String TagComment1;
	 String Tag_MappedEnvironment;
	//  WebDriver driver = null;

	public  final By input_UN = By.xpath("//input[@name='email']");
	public  final By input_PW = By.xpath("//input[@id='typepass']");
	public  final By btn_Login = By.xpath("//button[contains(text(),'Login')]");
	public  final By Lnk_report = By.xpath("//span[contains(text(),'Reports')]");
	public  final By lnk_BagShipmentQuery = By.xpath("//span[contains(text(),'Bag Shipment Query')]");
	public  final By input_awbNo = By.xpath("//input[@id='ParentMPS']");
	public  final By btn_showShipmentLog = By.xpath("//button[contains(text(),'Show Shipment Log')]");
	public  final By lbl_shipmentQuery = By.xpath(" //label[contains(text(),'Cargo Shipment Summary')]");
	public  final By msg_success = By.xpath("//div[contains(text(),'Success.')]");
	public  final By err_blnkawb = By.xpath("//*[@class=\"text-danger\"]");
	public  final By btn_reset = By.xpath("//button[contains(text(),'Reset')]");
	public  final By err_invalidstatusAWB = By.xpath("//div[contains(text(),'UFA:Shipments not Found')]");
	public  final By err_invalidMPS = By.xpath("//div[contains(text(),'UFA:Data not Found')]");
	public  final By lbl_parentMPs = By.xpath("//th[contains(text(),'ParentMPS')]");
	public  final By lbl_AWBTracking = By.xpath("//legend[contains(text(),'AWB Tracking')]");
	public  final By tblHdr_ParentAWB = By.xpath("//th[contains(text(),'Parent AWB')]");
	public  final By tblHdr_MPSCount = By.xpath("//th[contains(text(),'MPS Count')]");
	public  final By tblHdr_ClientName = By.xpath("//th[contains(text(),'Client Name')]");
	public  final By tblHdr_ShippingID = By.xpath("//th[contains(text(),'Shipping Id')]");
	public  final By shippingID_Number = By.xpath("//td[contains(text(),'MPS3729211910608')]");
	public  final By hdr_TransactionLog = By.xpath("//h4[contains(text(),'Transactions Log')]");
	public  final By bagNo = By.xpath("//input[@id='bagNo']");
	public  final By btn_showbaglog = By.xpath("//button[contains(text(),'Show Bag Log')]");
	public  final By hdr_sealNo = By.xpath("//th[contains(text(),'Seal No')]");
	public  final By err_blnkbagNo = By.xpath("//span[contains(text(),'Enter Bag No')]");
	public  final By err_invalidbagNo = By.xpath("//div[contains(text(),'No record found')]");
	public  final By hdr_originhub = By.xpath("//th[contains(text(),'Origin Hubname')]");
	public  final By data_sealNo = By.xpath("//td[contains(text(),'nxb-11test80')]");
	public  final By data_originhubname = By.xpath("//td[contains(text(),'PNQ/BAN')]");
	public  final By data_currenthubname = By.xpath("//tbody/tr[1]/td[4]");
	public  final By data_destinationhubname = By.xpath("//tbody/tr[1]/td[5]");
	public  final By data_clientnm = By.xpath("//td[contains(text(),'MotoG Pvt Ltd (2041)')]");
	public  final By data_consigneeadd = By
			.xpath("//td[contains(text(),'Brainbees solutions private limited 12 (Wakad pune')]");
	public  final By data_trnsctnlogProcess = By.xpath("//td[contains(text(),'Shipment InScan from Manifest')]");

	public  final By lbl_MPSTracking = By.xpath("//legend[contains(text(),'MPS Tracking')]");

	 SoftAssert softAssert = new SoftAssert();

	public  void LoginSelfServicepage(String URL) throws InterruptedException {
		getDriver().get(URL);
	}

	public  void Navigate_on_Bag_shipmentQuery() throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(3000);

		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			getDriver().findElement(lnk_BagShipmentQuery).click();

			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(Lnk_report).click();
			Thread.sleep(3000);
			getDriver().findElement(lnk_BagShipmentQuery).click();
			Thread.sleep(3000);
		}

	}

	public  void searchAWB(String AWBNo) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(input_awbNo).sendKeys(AWBNo);
		getDriver().findElement(btn_showShipmentLog).click();

	}

	public  void verifyValidAWB(String successmessage) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(msg_success));

		String succesmsg = getDriver().findElement(msg_success).getText();
		softAssert.assertEquals(succesmsg, successmessage, "Entered AWB is valid and found on Bag shipment Query page");
		softAssert.assertAll();

	}

	public  void verifyResultOnBlankAWB(String AWBNo) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(btn_reset).click();
		getDriver().findElement(input_awbNo).sendKeys(AWBNo);
		getDriver().findElement(btn_showShipmentLog).click();

	}

	public  void verifyerroronblankAWB(String erroronblankawb, String AWBNo) throws InterruptedException {
		Thread.sleep(3000);
		if (AWBNo.isEmpty()) {
			String errmsg = getDriver().findElement(err_blnkawb).getText();
			softAssert.assertEquals(errmsg, erroronblankawb, "AWB is blank");
			softAssert.assertAll();
		} else {

			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(err_invalidstatusAWB));
			String invalidAWBerr = getDriver().findElement(err_invalidstatusAWB).getText();
			softAssert.assertEquals(invalidAWBerr, erroronblankawb, "AWB entered is invalid");
			softAssert.assertAll();

		}
	}

	public  void verifyerroronInvaidAWB(String AWBNo) throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(3000);
		getDriver().findElement(input_awbNo).sendKeys(AWBNo);

	}

	public  void resetBtnClick(String AwbNo) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(input_awbNo).sendKeys(AwbNo);
		getDriver().findElement(btn_reset).click();
		Thread.sleep(3000);
	}

	public  void verifyAWBTracking(String ParentAWB, String MPSCount, String ClientName)
			throws InterruptedException {
		Thread.sleep(3000);
		String lbl = getDriver().findElement(tblHdr_ParentAWB).getText();
		softAssert.assertEquals(lbl, ParentAWB, "Lable is Parent AWB");
		softAssert.assertAll();
		String lblMPS = getDriver().findElement(tblHdr_MPSCount).getText();
		softAssert.assertEquals(lblMPS, MPSCount, "Label is MPS Count");
		softAssert.assertAll();
		String lblclientNAme = getDriver().findElement(tblHdr_ClientName).getText();
		softAssert.assertEquals(lblclientNAme, ClientName, "Label is Client Name");
		softAssert.assertAll();

	}

	public  void VerifyMPSTrackingSection(String ShippingID) {
		String lblShippingID = getDriver().findElement(tblHdr_ShippingID).getText();
		softAssert.assertEquals(lblShippingID, ShippingID, "Label is ShippingID");
		softAssert.assertAll();

	}

	public  void clickonMPSandVerifyTransactionLog(String ShippingID) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(shippingID_Number).click();
		

	}

	public  void checkTransactionLog() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(hdr_TransactionLog).isDisplayed();
	}

	public  void checkinvalidBagNo(String BagNo) throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(3000);
		getDriver().findElement(bagNo).sendKeys(BagNo);
		getDriver().findElement(btn_showbaglog).click();

	}

	public  void bagNoValidation(String BagNo, String Errorbagno) {
		if (BagNo.isEmpty()) {
			String errmsg = getDriver().findElement(err_blnkbagNo).getText();
			softAssert.assertEquals(errmsg, Errorbagno, "Bag no is balnk");
			softAssert.assertAll();

		} else {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(err_invalidbagNo));
			String invalidbagerr = getDriver().findElement(err_invalidbagNo).getText();
			softAssert.assertEquals(invalidbagerr, Errorbagno, "Bag No entered is invalid");
			softAssert.assertAll();
		}

	}

	public  void resultonBagNo(String SealNo, String OriginHubname) throws InterruptedException {
		Thread.sleep(3000);
		String hdr_sealno = getDriver().findElement(hdr_sealNo).getText();
		softAssert.assertEquals(hdr_sealno, SealNo, "Seal No header found");
		softAssert.assertAll();
		String hdr_orginhub = getDriver().findElement(hdr_originhub).getText();
		softAssert.assertEquals(hdr_orginhub, OriginHubname, "Origin hubname header found");
		softAssert.assertAll();

	}

	public  void bagNoDetailDataVerification(String sealNo, String originHubname, String currentHubname,
			String destinationHubname) throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(3000);
		String dtsealNo = getDriver().findElement(data_sealNo).getText();
		softAssert.assertEquals(dtsealNo, sealNo, "Seal no is Nxb-11test80");
		softAssert.assertAll();
		String dtoriginhub = getDriver().findElement(data_originhubname).getText();
		softAssert.assertEquals(dtoriginhub, originHubname, "Origin hubname is PNQ/BAN");
		softAssert.assertAll();
		String dtcurrenthub = getDriver().findElement(data_currenthubname).getText();
		softAssert.assertEquals(dtcurrenthub, currentHubname, "Current hub name is PNQ/HUB");
		softAssert.assertAll();
		String dtdestinationhub = getDriver().findElement(data_destinationhubname).getText();
		softAssert.assertEquals(dtdestinationhub, destinationHubname, "Destination hub name is PNQ/HUB");
		softAssert.assertAll();

	}

	public  void awddataVerification(String clientName, String consingeeNameAdd) throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(4000);
		String dtclientnm = getDriver().findElement(data_clientnm).getText();
		softAssert.assertEquals(dtclientnm, clientName, "Cleint Name is MotoG");
		softAssert.assertAll();
		String dtconsigneeadd = getDriver().findElement(data_consigneeadd).getText();
		softAssert.assertEquals(dtconsigneeadd, consingeeNameAdd, "Consignee address is correct");
		softAssert.assertAll();

	}

	public  void transactionLogDataVerification(String Process) throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(3000);
		String dtprocess = getDriver().findElement(data_trnsctnlogProcess).getText();
		softAssert.assertEquals(dtprocess, Process, "Process name is displayed");
		softAssert.assertAll();

	}

}

