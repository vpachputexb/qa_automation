@CargoBooking_FileUpload
Feature: Cargo Booking : Using file upload

  @CargoLogin
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

  @Cargo_FlipkartSoftDataUpload_HappyFlow
  Scenario Outline: <TC> :FlipkartSoftDataUpload - Happy Flow
    Given Navigate to Cargo Booking Menu and Clicks on Flipkart Soft Data Upload tab
    When User enters "<clientfiletype>" information
    When User selectss client "<ClientName>"
    When Select "<FileName>" which want to upload. User click on Submit button
    Then User clicks submit button. Validate success message as "<Message_FileUpload>" Click on OK button

    Examples: 
      | clientfiletype    | ClientName      | FileName                    | Message_FileUpload      |
      | With Seller ID    | Snehal_5thApril | SampleFileWithSeller.csv    | File Upload Successful. |
      | Without Seller ID | MotoG           | SampleFileWithoutSeller.csv | File Upload Successful. |

  @Cargo_FlipkartSoftDataUpload_error_messages_blank
  Scenario: FlipkartSoftDataUpload - Validation of blank error message
    Given Navigate to Cargo Booking Menu and Clicks on Flipkart Soft Data Upload tab
    Then User clicks on Submit button and validate the error message

  @Cargo_FlipkartSoftDataUpload_ClientTextSearchAndPartialTextSearch
  Scenario Outline: <TC> :FlipkartSoftDataUpload - Happy Flow
    Given Navigate to Cargo Booking Menu and Clicks on Flipkart Soft Data Upload tab
    Then User searches client name and Validate Search "<ClientName>"

    Examples: 
      | ClientName      |
      | Nikita 7 April  |
      | Snehal_5thApril |
      | Niki            |

  @Cargo_FlipkartSoftDataUpload_ClientTextSearch_Negative_Scenarios
  Scenario Outline: <TC> :FlipkartSoftDataUpload - Happy Flow
    Given Navigate to Cargo Booking Menu and Clicks on Flipkart Soft Data Upload tab
    Then User searches invalid client name and Validate Search "<ClientName>"

    Examples: 
      | ClientName |
      | MotoG      |
      | Mo         |

  @Cargo_FlipkartSoftDataUpload_WithSellerID
  Scenario Outline: <TC> :FlipkartSoftDataUpload - Happy Flow
    Given Navigate to Cargo Booking Menu and Clicks on Flipkart Soft Data Upload tab
    Then User searches client name and Validate Search "<ClientName>"
    When Select "<FileName>" which want to upload. User click on Submit button
    Then User clicks submit button. Validate success message as "<Message_FileUpload>" Click on OK button

    Examples: 
      | ClientName     | FileName                 | Message_FileUpload      |
      | Nikita 7 April | SampleFileWithSeller.csv | File Upload Successful. |

  @Cargo_FlipkartSoftDataUpload_Resetfunctionality
  Scenario Outline: <TC> :FlipkartSoftDataUpload - Happy Flow for reset functionality
    Given Navigate to Cargo Booking Menu and Clicks on Flipkart Soft Data Upload tab
    Then User searches client name and Validate Search "<ClientName>"
    When Select "<FileName>" which want to upload. User click on Submit button
    Then User clicks submit button. Validate success message as "<Message_FileUpload>" Click on OK button
    Then User clicks on reset button

    Examples: 
      | clientfiletype | ClientName      | FileName                 | Message_FileUpload      |
      | With Seller ID | Snehal_5thApril | SampleFileWithSeller.csv | File Upload Successful. |

  @Cargo_FlipkartSoftDataUpload_InvalidfileValidation
  Scenario Outline: <TC> :FlipkartSoftDataUpload - Invalid file Validation
    Given Navigate to Cargo Booking Menu and Clicks on Flipkart Soft Data Upload tab
    Then User searches client name and Validate Search "<ClientName>"
    When user select file "<filename>" and clicks on submit button

    Examples: 
      | clientfiletype | ClientName     | FileName                 | Message_FileUpload      |
      | With Seller ID | Nikita 7 April | Sample_Booking_Data.xlsx | File Upload Successful. |
