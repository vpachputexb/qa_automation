@DEPS
Feature: Damage MArking for MPS

 @CargoDEPSLoging
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | summit.sakhre@xpressbees.com   | Summ@1080      |
      
  @Cargo_CompleteBooking
  Scenario Outline: <TC>: Cargo Booking - Happy flow for Cargo Booking for MotoGClient
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    #When User enters additional box details "<Length2>","<Breadth2>","<Height2>","<NoOfBoxes2>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then Capture the AWBNumber Generated
    Then Capture the AWBNumber from Rapid Booking
    Then Capture the MPS No from cargo Booking
    #Then Verify the details saved with AWB Number and other details info "<clientName>","<Route Mode>","<Booking Mode>","<pickupPincode>","<dropPincode>","<LocationName>","<Mobile>","<Email>","<Address>","<GSTNo>","<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>","<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then User clicks on consignee details tab
    When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
    Then User clicks on Save and Next button and Validate record entered successful
    When User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
    |Electrical Goods| 
    When User clicks on Save and Next button
    When User enters Invoice and GST details less then 50K "<InvoiceValue>"
    Then User clicks on Save and Next button and Validate the Invoice and GST details result
    Then User clicks on Save and Next button to complete Booking
    
    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Length2 | Breadth2 | Height2 | NoOfBoxes2 | Status        | consigneeCode  | companyName  | Mobile1    | consigneePOC | Email1   											| GSTNo1          | Address1      | Productcategories | Referenceno | Clientreferenceno  | InvoiceValue |
      | MotoG      | Surface    | Credit   		 |        411045 |      411045 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 18AABCU9603R1ZM |   0201254 |                1 |                    1000.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 |      50 |       50 |      50 |          3 | AWB Generated | Summit         | Summit       | 8275195563 | Summit       | summit.sakhre1@xpressbees.com | 00XXXXX0000X1ZZ | local Address | 									| auto1       | auto           		 |        1000  |
    
    
 @Allocateing_Booked_shipment_for_Pickup
  Scenario Outline: <TC>: Allocating shipment for FirstMile Pickup
   	Given User Navigates to Trip Management
  	Given User Navigates to Create Booking Trip Tab
  	When User Enters Assignment Person and contracted Vehicle Details "<Assigned To>","<Contracted Vehicle Number>"
  	When User Enters Details In AddShipment Tab Booking Way
  	When User Assigns AWBNumber To Trip
  	Then User Enters ODO Reading "<ODO Reading>"
  	Then User Scans AWBNumber & ValidateSucessMessage Booking Way"<Message>"
  	Then Validate Trip created details Booking Way "<Assigned To>","<ODO Reading>"
  	
  	Examples:
  		| Assigned To           | Contracted Vehicle Number | ODO Reading | Message                   |
  		|PRAMOD BIHARI (22113)  | JU 21 N 0002 						  | 10	        | Trip Started Successfully |
  		
  
  @MarkDamageforMPSWithoutShippedStatus
  Scenario Outline: <TC> Marking the product as Damage prior to picked status
  	Given User will navigate to Cargo Mark Shipments Damage		
  	When User will add the record for Damage MPS
  	Then User enters the not shipped MPS details for Damage Products "<Damage Reason>","<Description>","<MPS Status>"
  	
  	Examples:
  	|	Damage Reason																					|	Description										|	MPS Status								|
  	|	Shipment received in Outer Packaging Damage Condition	|	Not in good condition to use	|	MPS Status is not Shipped	|
  	
  		
  @ShipmentPickupAPI
  	Scenario Outline: <TC>: SR Pickup the shipment
  		Then User will pick up the mps from Hub
  		

 @CloseTrip
 	Scenario Outline: <TC>: Close the SR/BA Trip
 		#Then validate the status "<Trip Status>"
    Given User Navigates to Trip Management
    Given User opens the Trip details "<TripStatusCloseTrip>","<Assigned To>"
    When User Navigates To Close Trip and Scans AWBs Booking way "<NumberOfInvoices>"
    Then user Enters closure ODO reading "<ClosureODOReading>"
    Then User closes trip and validates trip closure message"<MessageCloseTrip>"
    
    Examples:
    	| Trip Status | TripStatusCloseTrip | NumberOfInvoices | ClosureODOReading | MessageCloseTrip 				 | Assigned To						|           
    	| picked			| Started						  | 1								 | 11								 | Trip Closed Successfully  | PRAMOD BIHARI (22113)  |
    	
    	
 @MarkDamageforMPS
  Scenario Outline: <TC> Marking the product as Damage
  	Given User will navigate to Cargo Mark Shipments Damage		
  	When User will add the record for Damage MPS
  	Then User enters the MPS details for Damage Products "<Damage Reason>","<Description>"
  	
  	Examples:
  	|	Damage Reason																					|	Description										|
  	|	Shipment received in Outer Packaging Damage Condition	|	Not in good condition to use	|
  	
  	
 @ValidateTheAddedRecordsforDamageMPS
 	Scenario Outline: <TC> Verify the record added is correct or not
 		Given If user wanted to update the Damage Condition "<Damage Reason 1>"
 		When User wanted to View Damage Details "<Previous Reason>","<Description1>"
 		Then User can view the uploaded image
 		
 		Examples:
 		|	Damage Reason 1													|	Previous Reason																				|	Description1									|
 		|	Shipment received in leakage Condition	|	Shipment received in Outer Packaging Damage Condition	|	Not in good condition to use	|
 		
 		
 @ValidateTheFilterRecord
 	Scenario Outline: <TC> Verify the output when incorrect & then correct data provided
		Given User clicks on Filter Option
  	When User enters the invalid inputs "<MPS No>","<Mark Damage Hub/SVC Name>"
  	And User searches for the provided input "<Output>"
  	
  	Examples:
  	|	MPS No			|	Mark Damage Hub/SVC Name	|	Output						|
  	|	9123456789	|	PNQ/BAN										|	No Records Found	|
  	
  	
 @MarkingMPSasDamageOfOtherHub
 	Scenario Outline: <TC> Verify other then current hubs MPS cannot be marked as damage
		When User will add the record for Damage MPS
 		Then User enters the MPS details of other hubs for Damage Products "<Damage Reason>","<Description>","<MPS No>","<Status Validation>"
  	
  	Examples:
  	|	Damage Reason																					|	Description										|	MPS No					|	Status Validation																							|
  	|	Shipment received in Outer Packaging Damage Condition	|	Not in good condition to use	|	MPS372922280002	|	Unable to Mark Damage: MPS does not belong to the Current Hub	|
  	
  	
 @UploadingImageMoreThen6000KB
 	Scenario Outline: <TC> Verify More Then 6000 KB image should not be uploaded
  	When User will add the record for Damage MPS
 		Then User tries to upload image more then 6000 KB "<Damage Reason>","<MPS No>","<Image Error>"
  	
  	Examples:
  	|	Damage Reason																					|	MPS No					|	Image Error																|
  	|	Shipment received in Outer Packaging Damage Condition	|	MPS372922280002	|	Uploaded file size is greater than 6000KB	|
  	
  
 @AlreadyMarkedMPSTryAgain
 	Scenario Outline: <TC> Already Marked MPS trying to upload again
 	When User will add the record for Damage MPS
 	Then User enters the already marked MPS details again "<Damage Reason>","<Description>","<MPS Status>"
  	
  	Examples:
  	|	Damage Reason																					|	Description										|	MPS Status								|
  	|	Shipment received in Outer Packaging Damage Condition	|	Not in good condition to use	|	MPS Already Marked Damage	|
  	 	