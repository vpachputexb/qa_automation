@Cargo_TariffCard
Feature: Slabs

  @CargoLoginTariffCard
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page
    
     Examples: 
      | Username                       | Password       |
      | admin.atul.deokar@xpressbees.com | Xpress@123 |
      
      
       @TariffCardcreation
  Scenario Outline: <TC>: Happy flow for Tariff Card creation
    Given User Navigates to Traiff card option
   When Click on add new Tariff card button
    Then Provide Tariff card name "<TemplateName>" and select Route mode Air
    Then select service level template "<Serviceleveltemplate>" from drop down and select rate basis Quantity "<ratebasis>"
    Then Select Tariff card rate type flat rate "<Tariffcardratetype>"
    Then Upload file "<fileName>" and validate it
    Then click on Save button and validates successmsg "<Successmsg>"
    
    Examples:
    | TemplateName | Serviceleveltemplate 		| ratebasis | Tariffcardratetype | fileName | Successmsg |
    | Tariffcard66 | 	Air Template Automation | Quantity 	| Flat Rate  				 | TariffAutomation.csv |  Record created successfully. |
    
    
    
@TariffCardcreationwithslab
  Scenario Outline: <TC>: Happy flow for Tariff Card creation with slabs
    Given User Navigates to Traiff card option
   When Click on add new Tariff card button
    Then Provide Tariff card name "<TemplateName>" and select Route mode Air
    Then select service level template "<Serviceleveltemplate>" from drop down and select rate basis Quantity "<ratebasis>"
    Then Select Tariff card rate type slab rate
    Then User selects slab type Quantity
    Then User selects slab template "<Slabtemplate>"
    Then User clicks on Break 1
    Then Upload file 1 "<fileName1>" and validate it
    Then User clicks on Break 2
    Then Upload file 2 "<fileName2>" and validate it
    Then click on Save button and validates successmsg "<Successmsg>"
    
    Examples:
    | TemplateName | Serviceleveltemplate | ratebasis | Slabtemplate | fileName1 | fileName2 | Successmsg |
    | Tariffcard67 | 	Air Template Automation | Quantity | Slab Automation | SlabAutomation1.csv | SlabAutomation2.csv | Record created successfully. |


    @TariffCardcreationwithslabwithoutvalidating2ndslab
  Scenario Outline: <TC>: Happy flow for Tariff Card creation with slabs without validating 2nd slab
    Given User Navigates to Traiff card option
   When Click on add new Tariff card button
    Then Provide Tariff card name "<TemplateName>" and select Route mode Air
    Then select service level template "<Serviceleveltemplate>" from drop down and select rate basis Quantity "<ratebasis>"
    Then Select Tariff card rate type slab rate
    Then User selects slab type Quantity
    Then User selects slab template "<Slabtemplate>"
    Then User clicks on Break 1
    Then Upload file 1 "<fileName1>" and validate it
    Then User clicks on Break 2
    Then Upload file 2 "<fileName2>" 
    Then click on Save button and validates Errormsg "<Errormsg>"
    
    Examples:
    | TemplateName | Serviceleveltemplate | ratebasis | Slabtemplate | fileName1 | fileName2 | Errormsg |
    | Tariffcard68 | 	Air Template Automation | Quantity | Slab Automation | SlabAutomation1.csv | SlabAutomation2.csv | Please Validate Rate for Slab Break 2 |

    @DownloadInvalidRecords
  Scenario Outline: <TC>: Download Invalid data
    Given User Navigates to Traiff card option
    When Click on add new Tariff card button
    Then Provide Tariff card name "<TemplateName>" and select Route mode Air
    Then select service level template "<Serviceleveltemplate>" from drop down and select rate basis Quantity "<ratebasis>"
    Then Select Tariff card rate type flat rate "<Tariffcardratetype>"
    Then Upload Invalid file "<InvalidfileName>" and validate it
    Then User clicks on Download CSV button
    
    Examples:
    | TemplateName | Serviceleveltemplate 		| ratebasis | Tariffcardratetype | InvalidfileName | 
    | Tariffcard69 | 	Air Template Automation | Quantity 	| Flat Rate  				 | TariffAutomationInvaliddata.csv |  
    
    
    
    @Reuploadfile
  Scenario Outline: <TC>: Reupload rate file
    Given User Navigates to Traiff card option
    When User clicks on filter option1
    Then User enters tariffcard name "<Tariffcardname>" in Tariff card name filter
    Then User clicks on Search button1
    Then User clicks on Edit option
    Then Upload file "<fileName>" and validate it
    Then User clicks on save button
    Then User validates the alert msg "<Alertmsg>" and clicks on Ok
     
    Examples:
    | Tariffcardname | fileName | Alertmsg |
    | TC2            | TariffAutomation.csv | The old data will be overwritten by new data.Are you sure? |
    
    
    @Downloadingvaliddata
    Scenario Outline: <TC>: Reupload rate file
    Given User Navigates to Traiff card option
    When User clicks on filter option1
    Then User enters tariffcard name "<Tariffcardname>" in Tariff card name filter
    Then User clicks on Search button1
    Then User clicks on Edit option
    Then download valid data
    
    Examples:
    | Tariffcardname |
    | TC2 |
    
@TariffCardcreationwithslabwithwrongfile
  Scenario Outline: <TC>: Tariff Card creation with slabs with flat rate file
    Given User Navigates to Traiff card option
    When Click on add new Tariff card button
    Then Provide Tariff card name "<TemplateName>" and select Route mode Air
    Then select service level template "<Serviceleveltemplate>" from drop down and select rate basis Quantity "<ratebasis>"
    Then Select Tariff card rate type slab rate
    Then User selects slab type Quantity
    Then User selects slab template "<Slabtemplate>"
    Then User clicks on Break 1
    Then Upload file 3 "<fileName3>" 
    Then verify Header mismatch error "<Headermismatcherror>"
    
    
    Examples:
    | TemplateName | Serviceleveltemplate | ratebasis | Slabtemplate | fileName3 | Headermismatcherror |
    | Tariffcard64 | 	Air Template Automation | Quantity | Slab Automation | TariffAutomation.csv |  File headers are not matched. Please download Tariff card csv file and use that file to upload data to avoid header mismatch issue. |
    
    
    @Nonselectionofquantity
  Scenario Outline: <TC>: Non selection of slab type quantity
    Given User Navigates to Traiff card option
    When Click on add new Tariff card button
    Then Provide Tariff card name "<TemplateName>" and select Route mode Air
    Then select service level template "<Serviceleveltemplate>" from drop down and select rate basis Weight "<ratebasis>"
    Then Select Tariff card rate type slab rate
    Then User tries to select disabled slab type Quantity
    
    Examples:
    | TemplateName | Serviceleveltemplate | ratebasis |
    | Tariffcard65 | Air Template Automation | Weight |
    
  
    
    
    
    
    
    