@Cargo_Slabs
Feature: Slabs

  @CargoLoginSlabs
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page
     Examples: 
      | Username                       | Password       |
      | admin.atul.deokar@xpressbees.com | Xpress@123 |
      
 
  @Cargo_HappyFlow
  Scenario Outline: <TC>: Slabs - Happy flow for Slabs
    Given User Navigates to Master data option
    When User Navigates to create new slab option
    Then User enters "<SlabName>" in the text field and user selects Slab Type Invoice amount
    Then User selects higher break limit type "<BreakHigherLimitType>" and enter higher break limit value "<HigherBreakLimitValue>"
    Then User clicks on Save button
    Then User validates successful msg and clicks on Ok "<Successmsg>"
    
    Examples: 
    | SlabName    | BreakHigherLimitType | HigherBreakLimitValue | Successmsg |
    | TestSlab240   | <= (less than or equal to) | 100 | Record created successfully. |
    
    
    @Cargo_Multipleslabs
    Scenario Outline: <TC>: Slabs - Multiple Slabs
    Given User Navigates to Master data option
    When User Navigates to create new slab option
    Then User enters "<SlabName>" in the text field and user selects Slab Type Invoice amount
    Then User selects higher break limit type "<BreakHigherLimitType>" and enter higher break limit value "<HigherBreakLimitValue>"
    Then User clicks on plus sign
    Then User selects higher break limit type "<BreakHigherLimitType1>" for second row and enter higher break limit value "<HigherBreakLimitValue1>"
    Then User clicks on Save button
    Then User validates successful msg and clicks on Ok "<Successmsg>"
    
     Examples: 
    | SlabName   | BreakHigherLimitType | HigherBreakLimitValue | BreakHigherLimitType1 | HigherBreakLimitValue1 |Successmsg |
    | TestSlab241 | <= (less than or equal to) | 100 | < (less than) | 300 | Record created successfully. |
    
    
     @Cargo_MakingStatusActive
    Scenario Outline: <TC>: Slabs - Making Slab Status Active
    Given User Navigates to Master data option
    When User clicks on filter option
    Then User enters "<slabsname>" in text field and clicks on Search button
    Then User clicks on Inactive status button and clicks on Ok button
    Then User validates activation successful msg and clicks on Ok "<ActivationSuccessmsg>"
    
     Examples: 
    | slabsname   | ActivationSuccessmsg |
    | TestSlab241 | Slab Activated Successfully!! |
    
    
    @Cargo_ForWeightSlab
    Scenario Outline: <TC>: Slabs - WeightSlab And Its SubTypes
    Given User Navigates to Master data option
    When User Navigates to create new slab option
    Then User enters "<SlabName1>" in the text field and user selects Slab Type Weight and Weight Type Billable Weight
    Then User selects higher break limit type "<BreakHigherLimitType>" and enter higher break limit value "<HigherBreakLimitValue>"
    Then User clicks on plus sign
    Then User selects higher break limit type "<BreakHigherLimitType1>" for second row and enter higher break limit value "<HigherBreakLimitValue1>"
    Then User clicks on Save button
    Then User validates successful msg and clicks on Ok "<Successmsg>"
    Then User clicks on Inactive status button and clicks on Ok button
    Then User validates activation successful msg and clicks on Ok "<ActivationSuccessmsg>"

   Examples: 
    | SlabName1   | BreakHigherLimitType | HigherBreakLimitValue | BreakHigherLimitType1 | HigherBreakLimitValue1 |Successmsg | ActivationSuccessmsg |
    | TestSlab242 | <= (less than or equal to) | 100 | < (less than) | 300 | Record created successfully. | Slab Activated Successfully!! |
    
    @Cargo_CLickonviewbutton
    Scenario Outline: <TC>: Slabs - Click On view Button
    Given User Navigates to Master data option
    When User clicks on filter option
    Then User enters "<slabsname>" in text field and clicks on Search button
    Then User clicks on View button
    

Examples:
| slabsname   | SlabName |  
| TestSlab23 | TestSlab23 |   

 @Cargo_Verifyerrormsg
 Scenario Outline: <TC>: Slabs - Verify Error msg
    Given User Navigates to Master data option
    When User Navigates to create new slab option
    Then User clicks on Save button
    Then User verifies Blank Slab error msg "<BlankSlabErrorMsg>"
    Then User verifies Blank Slab Type error msg "<BlankSlabTypeErrorMsg>"
    
 Examples: 
  | BlankSlabErrorMsg | BlankSlabTypeErrorMsg |
  | Please enter Slab Name. | Please select Slab Type. |
    
    
    @Cargo_FilterforNoRecordsFound
Scenario Outline: <TC>: Slabs - Filter for No Records Found
    Given User Navigates to Master data option
    When User clicks on filter option
    Then User enters "<Slabsname1>" in text field
    Then User clicks on Search button
    Then User verifies msg "<msg>"
    
    Examples:
    | Slabsname1 | msg |
    | TestSlab000 | No Records Found |
    
 @Cargo_ForQuantitySlab
    Scenario Outline: <TC>: Slabs - Slab Type Quantity
    Given User Navigates to Master data option
    When User Navigates to create new slab option
    Then User enters "<SlabName2>" in the text field and user selects Slab Type Quantity
    Then User selects higher break limit type "<BreakHigherLimitType>" and enter higher break limit value "<HigherBreakLimitValue>"
    Then User clicks on plus sign
    Then User selects higher break limit type "<BreakHigherLimitType1>" for second row and enter higher break limit value "<HigherBreakLimitValue1>"
    Then User clicks on Save button
    Then User validates successful msg and clicks on Ok "<Successmsg>"
    Then User clicks on Inactive status button and clicks on Ok button
    Then User validates activation successful msg and clicks on Ok "<ActivationSuccessmsg>"
    
    Examples: 
    | SlabName2   | BreakHigherLimitType | HigherBreakLimitValue | BreakHigherLimitType1 | HigherBreakLimitValue1 |Successmsg | ActivationSuccessmsg |
    | TestSlab243 | <= (less than or equal to) | 100 | < (less than) | 300 | Record created successfully. | Slab Activated Successfully!! |
    
    
    @Cargo_ForSameSlabBreakValues
    Scenario Outline: <TC>: Slabs - For Same Slab Break Values
    Given User Navigates to Master data option
    When User Navigates to create new slab option
    Then User enters "<SlabName2>" in the text field and user selects Slab Type Quantity
    Then User selects higher break limit type "<BreakHigherLimitType>" and enter higher break limit value "<HigherBreakLimitValue>"
    Then User clicks on plus sign
    Then User selects higher break limit type "<BreakHigherLimitType1>" for second row and enter higher break limit value "<HigherBreakLimitValue1>"
    Then User clicks on Save button
    Then User clicks on Ok button
    
    Examples:
    | SlabName2   | BreakHigherLimitType | HigherBreakLimitValue | BreakHigherLimitType1 | HigherBreakLimitValue1 | ErrorMsg1 |
    | TestSlab244 | <= (less than or equal to) | 100 | < (less than) | 100 | Invalid Slab breakup |
    
    
    
    @Cargo_ForSlabDelete
    Scenario Outline: <TC>: Slabs - For Slab Break Delete
    Given User Navigates to Master data option
    When User Navigates to create new slab option
    Then User enters "<SlabName2>" in the text field and user selects Slab Type Quantity
    Then User selects higher break limit type "<BreakHigherLimitType>" and enter higher break limit value "<HigherBreakLimitValue>"
    Then User clicks on plus sign
    Then User selects higher break limit type "<BreakHigherLimitType1>" for second row and enter higher break limit value "<HigherBreakLimitValue1>"
    Then User clicks on slab delete button and verifies msg "<Msg1>"
    Then User clicks on Ok button
    Then User clicks on Save button
    Then User clicks on Ok button
    
    Examples:
    | SlabName2   | BreakHigherLimitType | HigherBreakLimitValue | BreakHigherLimitType1 | HigherBreakLimitValue1 | Msg1 |
    | TestSlab245 | <= (less than or equal to) | 100 | < (less than) | 200 | Remove selected slab breakup. |
    
    @Cargo_ForDistanceSlab
    Scenario Outline: <TC>: Slabs - Slab Type Distance
    Given User Navigates to Master data option
    When User Navigates to create new slab option
    Then User enters "<SlabName3>" in the text field and user selects Slab Type Distance
    Then User selects higher break limit type "<BreakHigherLimitType>" and enter higher break limit value "<HigherBreakLimitValue>"
    Then User clicks on plus sign
    Then User selects higher break limit type "<BreakHigherLimitType1>" for second row and enter higher break limit value "<HigherBreakLimitValue1>"
    Then User clicks on Save button
    Then User validates successful msg and clicks on Ok "<Successmsg>"
    Then User clicks on Inactive status button and clicks on Ok button
    Then User validates activation successful msg and clicks on Ok "<ActivationSuccessmsg>"
    
    Examples: 
    | SlabName3   | BreakHigherLimitType | HigherBreakLimitValue | BreakHigherLimitType1 | HigherBreakLimitValue1 |Successmsg | ActivationSuccessmsg |
    | TestSlab246 | <= (less than or equal to) | 100 | < (less than) | 300 | Record created successfully. | Slab Activated Successfully!! |
    
    
    
    @Cargo_ForSlabDeleteAndCancel
    Scenario Outline: <TC>: Slabs - For Slab Break Delete And Cancel Functionality
    Given User Navigates to Master data option
    When User Navigates to create new slab option
    Then User enters "<SlabName2>" in the text field and user selects Slab Type Quantity
    Then User selects higher break limit type "<BreakHigherLimitType>" and enter higher break limit value "<HigherBreakLimitValue>"
    Then User clicks on plus sign
    Then User selects higher break limit type "<BreakHigherLimitType1>" for second row and enter higher break limit value "<HigherBreakLimitValue1>"
    Then User clicks on slab delete button and verifies msg "<Msg1>"
    Then User clicks on Cancel button
    Then User clicks on Save button
    Then User validates successful msg and clicks on Ok "<Successmsg>"
    
   Examples:
   
     | SlabName2   | BreakHigherLimitType | HigherBreakLimitValue | BreakHigherLimitType1 | HigherBreakLimitValue1 | Msg1 |Successmsg | 
    | TestSlab247 | <= (less than or equal to) | 100 | < (less than) | 200 | Remove selected slab breakup. | Record created successfully. | 
    
    
    
    
    
      