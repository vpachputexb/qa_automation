@SanityStageFeature
Feature: Sanity Suuite for Stage

  @CargoLoginSanityStage
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                   | Password   |
      | atul.deokar@xpressbees.com | Xpress@123 |

  @Cargo_FLM_TripManagement_MultipleMPS_SameHub_Sanity
  Scenario Outline: <Tc> : Complete Flow from Booking to Delivery for Different Hub
    Given User changes hub to Pickup hub "<PickUpHub>"
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    When User enters additional box details "<Length2>","<Breadth2>","<Height2>","<NoOfBoxes2>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then Capture the AWBNumber Generated
    Then User clicks on consignee details tab
    When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
    When User clicks on Save and Next button
    Then User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
      | Electrical Goods |
      | Liquid           |
    When Userr enters Invoice and GST details Dynamic FLM "<InvoiceValue>"
    Then User clicks on Save and Next button and Validate the Invoice and GST details result
    When User enters delivery date
    Then User clicks on save button and validate
    Given User Navigates to Trip Management
    Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
    Given User Navigates to Create Booking Trip Tab
    When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
    When User Enters Details In AddShipment Tab Booking Way through filter
    When User Assigns AWBNumber To Trip
    Then User Enters ODO Reading "<ODO Reading>"
    Then User Scans AWBNumber & ValidateSucessMessage Booking Way"<Message>"
    Then Validate Trip created details Booking Way "<Assigned To>","<ODO Reading>"
    Then User will pick up the mps from Hub
    Then validate the status "<Trip Status>"
    Given User Navigates to Trip Management
    Given User opens the Trip details "<TripStatusCloseTrip>","<Assigned To>"
    When User Navigates To Close Trip and Scans AWBs Booking way "<NumberOfInvoices>"
    Then user Enters closure ODO reading "<ClosureODOReading>"
    Then User closes trip and validates trip closure message"<MessageCloseTrip>"
    Given User hits awb verify api
    Given User changes hub to Pickup hub "<DeliveryHub>"
    Given User Navigates to Trip Management
    Given User closes trip from DB if trip is started already "<BA/SR Dropdown Delivery>"
    Given User Navigates to Create Booking Trip Tab
    When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
    When User Navigates to create trip and Starts the Trip
    When User hits delivery apis
    Then user navigates to close trip
    Given User changes hub to Pickup hub "<PickUpHub>"

    Examples: 
      | BA/SR Dropdown                  | BA/SR Dropdown Delivery         | PickUpHub | DeliveryHub | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Length2 | Breadth2 | Height2 | NoOfBoxes2 | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | Assigned To            | Assigned ToDelivery    | Contracted Vehicle Number | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | ODO Reading | Message                   | Trip Status | TripStatusCloseTrip | MessageCloseTrip         | ClosureODOReading | Productcategories | Referenceno | Clientreferenceno | InvoiceNumber  | InvoiceValue | invoicegeneratedText                                        |
      | Prashik Mankar - TSP BA (24422) | Prashik Mankar - TSP BA (24422) | PNQ/WKD   | PNQ/WKD     | MotoG      | Surface    | Credit       |        411054 |      411054 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         2 |                       10.00 |     50 |      50 |     50 |         1 |      50 |       50 |      50 |          1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | Prashik Mankar (24422) | Prashik Mankar (24422) | MH12DT9218 (TATA MAGIC)   | Mauti Van            | MH 12 XX 8819         | 4W                  |         212 | Trip Started Successfully | picked      | Started             | Trip Closed Successfully |               300 |                   | sdf1        | ssad1             | test00445121a7 |          100 | Total Invoice Value should be equal to Total Shipment Value |

  @Cargo_PrintAWB_Sanity
  Scenario Outline: <TC>: Sanity Print AWB - Happy flow for PrintAWB
    Given Navigate to Print AWB option
    When User enters "<AWBNumber>" in the text field. User clicks on Validate AWB button
    Then Validate Details displayed. Also check entered "<AWBNumber>" is validated
    When User clicks on Print Button. User cliks escape button for printing to cancel

    Examples: 
      | AWBNumber      |
      | 94185221110023 |
      |  9422952100010 |

  @Cargo_FileUpload_WithoutSellerId_Sanity
  Scenario Outline: <TC> : Sanity FileUpload - WithoutSellerId
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    When Select the "<filename>" and click on submit button
    Then Validate the upload details section "<Totalrecordsmessage>","<Validrecordsmessage>"

    Examples: 
      | ClientName | filename                    | Totalrecordsmessage | Validrecordsmessage |
      | MotoG      | WithoutSellerfileupload.csv | Total Records       | Valid Records       |

  @CargoPrintStickerHappyFlow_Sanity
  Scenario Outline: <TC>:Sanity Cargo Print Sticker - Happy flow for AWB Sticker and Carton Number
    Given Navigate to Cargo Print Sticker option
    When User selects radio button "<StickerOption>" and enters "<AWBorCartonNo>" and clicks on Search button.
    Then Validate MPS details are shown
    When User Prints first MPS or Carton
    Then Validate error message as "<ErrMsg_NoPrinterInstalled>". User clicks on OK button.
    When User Prints all MPS or Carton
    Then Validate error message as "<ErrMsg_NoPrinterInstalled>". User clicks on OK button.

    Examples: 
      | StickerOption    | AWBorCartonNo  | ErrMsg_NoPrinterInstalled             |
      | Cargo AWB Number | 94296420217089 | Error: Please Install Printing Device |
      | Cargo AWB Number | 94296420217088 | Error: Please Install Printing Device |

  @CargoBagShipmentQuery_Sanity
  Scenario Outline: <TC>: Sanity Bagshipment Query -Happy Flow
    Given User selects report menu and clicks on bag shipment Query
    When User enteres AWB "<AWBNo>" and click on show Shipment Log button
    Then Validate "<successMessage>" and validate AWB tracking sections displayed

    Examples: 
      | AWBNo           | successMessage |
      |  93672727192628 | Success.       |
      | MPS372922210132 | Success.       |

  @Cargo_ChangeInvoiceQuantity_Sanity
  Scenario Outline: <TC>: Sanity Change Invoice Quantity - Happy flow for Change Invoice Quantity
    Given User Navigates to Support option
    When User enters "<AWBNumber>" in the text field. User clicks on Search button
    Then User clicks on Update button
    Then User validates the successful msg and User clicks on Ok button "<Successfulmsg>"

    Examples: 
      | AWBNumber     | Successfulmsg                       |
      | 9422072100758 | Invoice Count Updated Successfully. |

  @ShiftShipmentDestinationHubHappyFLow_Sanity
  Scenario Outline: <TC>:Sanity  HappyFLow Shift Shipment Destination Hub
    Given Navigate to shift shipment destinationhub module
    When User enters "<AWB Number>"
    When User selects change destination hub "<Changed Hub>"
    When User enters comments "<Comments>"
    Then User Clicks on update button
    Then Validate successmessage "<SuccessMessage>"

    Examples: 
      | AWB Number    | Changed Hub | Comments | SuccessMessage                       |
      | 9422072100787 | Ajmer       | Test     | Destination Hub Successfully Updated |

  @CargoSlabs_HappyFlow_Sanity
  Scenario Outline: <TC>: Sanity Slabs - Happy flow for Slabs
    Given User Navigates to Master data option
    When User Navigates to create new slab option
    Then User enters "<SlabName>" in the text field and user selects Slab Type Invoice amount
    Then User selects higher break limit type "<BreakHigherLimitType>" and enter higher break limit value "<HigherBreakLimitValue>"
    Then User clicks on Save button
    Then User validates successful msg and clicks on Ok "<Successmsg>"

    Examples: 
      | SlabName     | BreakHigherLimitType       | HigherBreakLimitValue | Successmsg                   |
      | TestSlab0045 | <= (less than or equal to) |                   100 | Record created successfully. |

  @TariffCardcreation_Sanity
  Scenario Outline: <TC>: Sanity Happy flow for Tariff Card creation
    Given User Navigates to Traiff card option
    When Click on add new Tariff card button
    Then Provide Tariff card name "<TemplateName>" and select Route mode Air
    Then select service level template "<Serviceleveltemplate>" from drop down and select rate basis Quantity "<ratebasis>"
    Then Select Tariff card rate type flat rate "<Tariffcardratetype>"
    Then Upload file "<fileName>" and validate it
    Then click on Save button and validates successmsg "<Successmsg>"

    Examples: 
      | TemplateName   | Serviceleveltemplate    | ratebasis | Tariffcardratetype | fileName             | Successmsg                   |
      | Tariffcard0172 | Air Template Automation | Quantity  | Flat Rate          | TariffAutomation.csv | Record created successfully. |
