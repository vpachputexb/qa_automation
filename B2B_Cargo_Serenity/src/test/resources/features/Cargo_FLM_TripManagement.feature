@Cargo_FLM_TripManagement
Feature: Cargo FLM_TripManagement feature

  @CargoFLMLogin
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                   | Password   |
      | atul.deokar@xpressbees.com | Xpress@123 |

  #@Cargo_FLM_TripManagement_TripStartedFLowAdhoc
  #Scenario Outline: <Tc> : Happy flow for Trip Management-Trip Started
  #Given User Navigates to Trip Management
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #When User Enters Details In AddShipment Tab One AWB "<AWB Number>"
  #When User Assigns AWBNumber To Trip
  #Then User Enters ODO Reading "<ODO Reading>"
  #Then User Scans AWBNumber & ValidateSucessMessage "<AWB Number>","<Message>"
  #Then Validate Trip created details "<AWB Number>","<Assigned To>","<ODO Reading>"
  #Then User will pick up the mps from Hub
  #
  #Examples:
  #| Assigned To | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | AWB Number | ODO Reading | Message |
  #| Prashik Mankar | Mauti Van |  MH 12 XX 8819 | 4W | 9422072100371 | 212 | Trip Started Successfully |
  #@Cargo_FLM_TripManagement_TripStartedFLowContracted
  #Scenario Outline: <Tc> : Happy flow for Trip Management-Trip Started
  #	Given User Navigates to Trip Management
  #	Given User Navigates to Create Booking Trip Tab
  #	When User Enters Assignment Person and contracted Vehicle Details "<Assigned To>","<Contracted Vehicle Number>"
  #	When User Enters Details In AddShipment Tab One AWB "<AWB Number>"
  #	When User Assigns AWBNumber To Trip
  #	Then User Enters ODO Reading "<ODO Reading>"
  #	Then User Scans AWBNumber & ValidateSucessMessage "<AWB Number>","<Message>"
  #	Then Validate Trip created details "<AWB Number>","<Assigned To>","<ODO Reading>"
  #
  #
  #
  #Examples:
  #| Assigned To | Contracted Vehicle Number | AWB Number | ODO Reading | Message |
  #| Jagdeep 14830 | MH96SP5656| 941923212201902 | 212 | Trip Started Successfully |
  #
  #@Cargo_FLM_TripManagement_TripStartedFLowThreeAWBs
  #Scenario Outline: <Tc> : Happy flow for Trip Management-Trip Started
  #Given User Navigates to Trip Management
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #When User Enters Details In AddShipment Tab Three AWB "<AWB Number1>","<AWB Number2>","<AWB Number3>"
  #When User Assigns AWBNumber To Trip
  #Then User Enters ODO Reading "<ODO Reading>"
  #Then User Scans Multiple AWBNumber & ValidateSucessMessage "<AWB Number1>","<AWB Number2>","<Message>"
  #Then Validate Trip created details Multiple "<AWB Number1>","<AWB Number2>","<Assigned To>","<ODO Reading>"
  #
  #Examples:
  #| Assigned To    | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | AWB Number1    | AWB Number2    | AWB Number3     | ODO Reading | Message                   |
  #| Prashik Mankar | Mauti Van            | MH 12 XX 8819         | 4W                  | 	9422072100288 | 9422072100289 | 9422072100290 |         212 | Trip Started Successfully |
  #
  #
  #@Cargo_FLM_TripManagement_TripStartedFLowAdhoc
  #Scenario Outline: <Tc> : Happy flow for Trip Management-First Mile
  #Given Navigate to Cargo Booking option
  #When User navigates to Create booking option
  #When Enter client name "<clientName>" and enters serviceablity information for ODA validation "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>","<MessageODA>"
  #When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
  #When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
  #Then Verify the AWB genarated and verify the status "<Status>"
  #Then Capture the AWBNumber Generated
  #Then User clicks on consignee details tab
  #When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
  #When User clicks on Save and Next button
  #Then User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
  #| Electrical Goods |
  #| Liquid           |
  #When Userr enters Invoice and GST details Dynamic FLM "<InvoiceValue>"
  #Then User clicks on Save and Next button and Validate the Invoice and GST details result
  #When User enters delivery date
  #Then User clicks on save button and validate
  #Given User Navigates to Trip Management
  #Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #When User Enters Details In AddShipment Tab Booking Way through filter
  #When User Assigns AWBNumber To Trip
  #Then User Enters ODO Reading "<ODO Reading>"
  #Then User Scans AWBNumber & ValidateSucessMessage Booking Way"<Message>"
  #Then Validate Trip created details Booking Way "<Assigned To>","<ODO Reading>"
  #Then User will pick up the mps from Hub
  #Then validate the status "<Trip Status>"
  #Given User Navigates to Trip Management
  #Given User opens the Trip details "<TripStatusCloseTrip>","<Assigned To>"
  #When User Navigates To Close Trip and Scans AWBs Booking way "<NumberOfInvoices>"
  #Then user Enters closure ODO reading "<ClosureODOReading>"
  #Then User closes trip and validates trip closure message"<MessageCloseTrip>"
  #
  #Examples:
  #| BA/SR Dropdown                  | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | MessageODA                                                              | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | Assigned To            | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | ODO Reading | Message                   | Trip Status | TripStatusCloseTrip | MessageCloseTrip         | ClosureODOReading | Productcategories | Referenceno | Clientreferenceno | InvoiceNumber  | InvoiceValue | invoicegeneratedText                                        |
  #| Prashik Mankar - TSP BA (24422) | lenovo     | Surface    | Credit       |        411054 |      411054 | Serviceable   | Consignee pincode is ODA, please click on OK if you still want to book. | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | Prashik Mankar (24422) | Mauti Van            | MH 12 XX 8819         | 4W                  |         212 | Trip Started Successfully | picked      | Started             | Trip Closed Successfully |               300 |                   | sdf1        | ssad1             | test00445121a7 |          100 | Total Invoice Value should be equal to Total Shipment Value |
  #@Cargo_FLM_TripManagement_CloseTrip
  #Scenario Outline: <Tc> : Happy flow for Trip Management-Trip close
  #Given User Navigates to Trip Management
  #Given User opens the Trip details "<TripStatus>","<Assigned To>"
  #When User Validates Trip Details "<AWB Number>","<MPS Number>"
  #When User Navigates To Close Trip and Scans AWBs "<AWB Number>","<Number of physical invoices>"
  #Then user Enters closure ODO reading "<ClosureODOReading>"
  #Then User closes trip and validates trip closure message"<message>"
  #
  #Examples:
  #| TripStatus | BA/SR                  | AWB Number     | MPS Number     | Number of physical invoices | ClosureODOReading | message                  |
  #| Started    | Prashik Mankar (24422) | 94192321420131 | MPS37292240327 |                           1 |               300 | Trip Closed Successfully |
  #
  #@Cargo_FLM_TripManagement_TripStartedDelivery
  #Scenario Outline: <Tc> : Trip Management Delivery
  #Given Navigate to Cargo Booking option
  #When User navigates to Create booking option
  #When Enter client name "<clientName>" and enters serviceablity information for ODA validation "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>","<MessageODA>"
  #When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
  #When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
  #Then Verify the AWB genarated and verify the status "<Status>"
  #Then Capture the AWBNumber Generated
  #Then User clicks on consignee details tab
  #When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
  #When User clicks on Save and Next button
  #Then User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
  #| Electrical Goods |
  #| Liquid           |
  #When Userr enters Invoice and GST details Dynamic FLM "<InvoiceValue>"
  #Then User clicks on Save and Next button and Validate the Invoice and GST details result
  #When User enters delivery date
  #Then User clicks on save button and validate
  #Given User Navigates to Trip Management
  #Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #When User Enters Details In AddShipment Tab Booking Way through filter
  #When User Assigns AWBNumber To Trip
  #Then User Enters ODO Reading "<ODO Reading>"
  #Then User Scans AWBNumber & ValidateSucessMessage Booking Way"<Message>"
  #Then Validate Trip created details Booking Way "<Assigned To>","<ODO Reading>"
  #Then User will pick up the mps from Hub
  #Then validate the status "<Trip Status>"
  #Given User Navigates to Trip Management
  #Given User opens the Trip details "<TripStatusCloseTrip>","<Assigned To>"
  #When User Navigates To Close Trip and Scans AWBs Booking way "<NumberOfInvoices>"
  #Then user Enters closure ODO reading "<ClosureODOReading>"
  #Then User closes trip and validates trip closure message"<MessageCloseTrip>"
  #Given User Navigates to Trip Management
  #Given User hits awb verify api
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #When User Navigates to create trip and Starts the Trip
  #When User hits delivery apis
  #Given User hits awb verify api
  #Then user navigates to close trip
  #
  #Examples:
  #| BA/SR Dropdown                  | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | MessageODA                                                              | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | Assigned To            | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | ODO Reading | Message                   | Trip Status | TripStatusCloseTrip | MessageCloseTrip         | ClosureODOReading | Productcategories | Referenceno | Clientreferenceno | InvoiceNumber  | InvoiceValue | invoicegeneratedText                                        |
  #| Prashik Mankar - TSP BA (24422) | lenovo     | Surface    | Credit       |        411054 |      411054 | Serviceable   | Consignee pincode is ODA, please click on OK if you still want to book. | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | Prashik Mankar (24422) | Mauti Van            | MH 12 XX 8819         | 4W                  |         212 | Trip Started Successfully | picked      | Started             | Trip Closed Successfully |               300 |                   | sdf1        | ssad1             | test00445121a7 |          100 | Total Invoice Value should be equal to Total Shipment Value |
  #
  #
  @Cargo_FLM_TripManagement_Different_Hub
  Scenario Outline: <Tc> : Complete Flow from Booking to Delivery for Different Hub
    Given User changes hub to Pickup hub "<PickUpHub>"
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then Capture the AWBNumber Generated
    Then User clicks on consignee details tab
    When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
    When User clicks on Save and Next button
    Then User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
      | Electrical Goods |
      | Liquid           |
    When Userr enters Invoice and GST details Dynamic FLM "<InvoiceValue>"
    Then User clicks on Save and Next button and Validate the Invoice and GST details result
    When User enters delivery date
    Then User clicks on save button and validate
    Given User Navigates to Trip Management
    Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
    Given User Navigates to Create Booking Trip Tab
    When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
    When User Enters Details In AddShipment Tab Booking Way through filter
    When User Assigns AWBNumber To Trip
    Then User Enters ODO Reading "<ODO Reading>"
    Then User Scans AWBNumber & ValidateSucessMessage Booking Way"<Message>"
    Then Validate Trip created details Booking Way "<Assigned To>","<ODO Reading>"
    Then User will pick up the mps from Hub
    Then validate the status "<Trip Status>"
    Given User Navigates to Trip Management
    Given User opens the Trip details "<TripStatusCloseTrip>","<Assigned To>"
    When User Navigates To Close Trip and Scans AWBs Booking way "<NumberOfInvoices>"
    Then user Enters closure ODO reading "<ClosureODOReading>"
    Then User closes trip and validates trip closure message"<MessageCloseTrip>"
    Given User hits awb verify api
    When User hits Hubops API
    Given User changes hub to Pickup hub "<DeliveryHub>"
    Given User Navigates to Trip Management
    Given User closes trip from DB if trip is started already "<BA/SR Dropdown Delivery>"
    Given User Navigates to Create Booking Trip Tab
    When User Enters Assignment Person and contracted Vehicle Details "<Assigned ToDelivery>","<Contracted Vehicle Number>"
    When User Navigates to create trip and Starts the Trip
    When User hits delivery apis
    Then user navigates to close trip
    Given User changes hub to Pickup hub "<PickUpHub>"

    Examples: 
      | BA/SR Dropdown                  | BA/SR Dropdown Delivery  | PickUpHub | DeliveryHub | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | Assigned To            | Assigned ToDelivery | Contracted Vehicle Number | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | ODO Reading | Message                   | Trip Status | TripStatusCloseTrip | MessageCloseTrip         | ClosureODOReading | Productcategories | Referenceno | Clientreferenceno | InvoiceNumber  | InvoiceValue | invoicegeneratedText                                        |  |
      | Prashik Mankar - TSP BA (24422) | Sagar Yadav - SR (15838) | PNQ/WKD   | PNQ/BAN     | lenovo     | Surface    | Credit       |        411054 |      411045 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | Prashik Mankar (24422) | Sagar Yadav (15838) | MH12DT9218 (TATA MAGIC)   | Mauti Van            | MH 12 XX 8819         | 4W                  |         212 | Trip Started Successfully | picked      | Started             | Trip Closed Successfully |               300 |                   | sdf1        | ssad1             | test00445121a7 |          100 | Total Invoice Value should be equal to Total Shipment Value |  |

  @Cargo_FLM_TripManagement_MultipleMPS_SameHub
  Scenario Outline: <Tc> : Complete Flow from Booking to Delivery for Different Hub
    Given User changes hub to Pickup hub "<PickUpHub>"
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    When User enters additional box details "<Length2>","<Breadth2>","<Height2>","<NoOfBoxes2>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then Capture the AWBNumber Generated
    Then User clicks on consignee details tab
    When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
    When User clicks on Save and Next button
    Then User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
      | Electrical Goods |
      | Liquid           |
    When Userr enters Invoice and GST details Dynamic FLM "<InvoiceValue>"
    Then User clicks on Save and Next button and Validate the Invoice and GST details result
    When User enters delivery date
    Then User clicks on save button and validate
    Given User Navigates to Trip Management
    Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
    Given User Navigates to Create Booking Trip Tab
    When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
    When User Enters Details In AddShipment Tab Booking Way through filter
    When User Assigns AWBNumber To Trip
    Then User Enters ODO Reading "<ODO Reading>"
    Then User Scans AWBNumber & ValidateSucessMessage Booking Way"<Message>"
    Then Validate Trip created details Booking Way "<Assigned To>","<ODO Reading>"
    Then User will pick up the mps from Hub
    Then validate the status "<Trip Status>"
    Given User Navigates to Trip Management
    Given User opens the Trip details "<TripStatusCloseTrip>","<Assigned To>"
    When User Navigates To Close Trip and Scans AWBs Booking way "<NumberOfInvoices>"
    Then user Enters closure ODO reading "<ClosureODOReading>"
    Then User closes trip and validates trip closure message"<MessageCloseTrip>"
    Given User hits awb verify api
    Given User changes hub to Pickup hub "<DeliveryHub>"
    Given User Navigates to Trip Management
    Given User closes trip from DB if trip is started already "<BA/SR Dropdown Delivery>"
    Given User Navigates to Create Booking Trip Tab
    When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
    When User Navigates to create trip and Starts the Trip
    When User hits delivery apis
    Then user navigates to close trip
    Given User changes hub to Pickup hub "<PickUpHub>"

    Examples: 
      | BA/SR Dropdown                  | BA/SR Dropdown Delivery         | PickUpHub | DeliveryHub | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Length2 | Breadth2 | Height2 | NoOfBoxes2 | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | Assigned To            | Assigned ToDelivery    | Contracted Vehicle Number | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | ODO Reading | Message                   | Trip Status | TripStatusCloseTrip | MessageCloseTrip         | ClosureODOReading | Productcategories | Referenceno | Clientreferenceno | InvoiceNumber  | InvoiceValue | invoicegeneratedText                                        |
      | Prashik Mankar - TSP BA (24422) | Prashik Mankar - TSP BA (24422) | PNQ/WKD   | PNQ/WKD     | MotoG      | Surface    | Credit       |        411054 |      411054 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         2 |                       10.00 |     50 |      50 |     50 |         1 |      50 |       50 |      50 |          1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | Prashik Mankar (24422) | Prashik Mankar (24422) | MH12DT9218 (TATA MAGIC)   | Mauti Van            | MH 12 XX 8819         | 4W                  |         212 | Trip Started Successfully | picked      | Started             | Trip Closed Successfully |               300 |                   | sdf1        | ssad1             | test00445121a7 |          100 | Total Invoice Value should be equal to Total Shipment Value |

  #@Cargo_FLM_TripManagement_Trip_CreateTripBlankerror
  #Scenario Outline: <Tc> : Trip Management Create Trip Blank error
  #Given User Navigates to Trip Management
  #Given User Navigates to Create Booking Trip Tab
  #When User clicks on save&next button
  #Then validate the error messages "<errorAssignedTo>", "<errorContractedVehicle>"
  #
  #Examples:
  #| errorAssignedTo       | errorContractedVehicle |
  #| Please Select User/SR | Please Select Vehicle  |
  #
  #@Cargo_FLM_TripManagement_Trip_AddShipments_AssignAWBBlank
  #Scenario Outline: <Tc> : Trip Management Trip_AddShipments Assign AWBBlank
  #Given User Navigates to Trip Management
  #Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #When user Clicks on assignAWB and RemoveAWB buttons
  #Then validate the error message "<errorMessage>"
  #
  #Examples:
  #| BA/SR Dropdown                  | Assigned To            | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | errorMessage                  |
  #| Prashik Mankar - TSP BA (24422) | Prashik Mankar (24422) | Mauti Van            | MH 12 XX 8819         | 4W                  | Please Select Atleast One AWB |
  #
  #@Cargo_FLM_TripManagement_FM_BlankODO
  #Scenario Outline: <Tc> : Trip Management FM_BlankODO
  #Given Navigate to Cargo Booking option
  #When User navigates to Create booking option
  #When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
  #When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
  #When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
  #Then Verify the AWB genarated and verify the status "<Status>"
  #Then Capture the AWBNumber Generated
  #Then User clicks on consignee details tab
  #When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
  #When User clicks on Save and Next button
  #Then User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
  #| Electrical Goods |
  #| Liquid           |
  #When Userr enters Invoice and GST details Dynamic FLM "<InvoiceValue>"
  #Then User clicks on Save and Next button and Validate the Invoice and GST details result
  #Then User clicks on save button and validate
  #Given User Navigates to Trip Management
  #Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #When User Enters Details In AddShipment Tab Booking Way through filter
  #When User Assigns AWBNumber To Trip
  #When User clicks on Start button without enterung odo reading
  #Then Validate error message ODO reading "<ErrorMessage>"
  #
  #Examples:
  #| BA/SR Dropdown                  | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | MessageODA                                                              | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | Assigned To            | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | ODO Reading | Message                   | ErrorMessage                 | InvoiceValue |
  #| Prashik Mankar - TSP BA (24422) | lenovo     | Surface    | Credit       |        411054 |      110505 | Serviceable   | Consignee pincode is ODA, please click on OK if you still want to book. | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | Prashik Mankar (24422) | Mauti Van            | MH 12 XX 8819         | 4W                  |         212 | Trip Started Successfully | Please enter ODO reading(km) |          100 |
  #
  #@Cargo_FLM_TripManagement_FM_BlankODO1
  #Scenario Outline: <Tc> : Happy flow for Trip Management-Trip Started
  #Given Navigate to Cargo Booking option
  #When User navigates to Create booking option
  #When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
  #When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
  #When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
  #Then Verify the AWB genarated and verify the status "<Status>"
  #Then Capture the AWBNumber Generated
  #Then User clicks on consignee details tab
  #When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
  #When User clicks on Save and Next button
  #Then User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
  #| Electrical Goods |
  #| Liquid           |
  #When Userr enters Invoice and GST details Dynamic FLM "<InvoiceValue>"
  #Then User clicks on Save and Next button and Validate the Invoice and GST details result
  #Then User clicks on save button and validate
  #Given User Navigates to Trip Management
  #Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #When User Enters Details In AddShipment Tab Booking Way through filter
  #When User Assigns AWBNumber To Trip
  #When User clicks on Start button without enterung odo reading
  #Then Validate error message ODO reading "<ErrorMessage>"
  #
  #Examples:
  #| BA/SR Dropdown                  | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | MessageODA                                                              | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | Assigned To            | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | ODO Reading | Message                   | ErrorMessage                 | InvoiceValue |
  #| Prashik Mankar - TSP BA (24422) | lenovo     | Surface    | Credit       |        411054 |      110505 | Serviceable   | Consignee pincode is ODA, please click on OK if you still want to book. | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | Prashik Mankar (24422) | Mauti Van            | MH 12 XX 8819         | 4W                  |         212 | Trip Started Successfully | Please enter ODO reading(km) |          100 |
  #
  #
  #
  #@Cargo_FLM_TripManagement_FirstMileClosureODO
  #Scenario Outline: <Tc> : Trip Management Delivery
  #Given Navigate to Cargo Booking option
  #When User navigates to Create booking option
  #When Enter client name "<clientName>" and enters serviceablity information for ODA validation "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>","<MessageODA>"
  #When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
  #When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
  #Then Verify the AWB genarated and verify the status "<Status>"
  #Then Capture the AWBNumber Generated
  #Then User clicks on consignee details tab
  #When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
  #When User clicks on Save and Next button
  #Then User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
  #| Electrical Goods |
  #| Liquid           |
  #When Userr enters Invoice and GST details Dynamic FLM "<InvoiceValue>"
  #Then User clicks on Save and Next button and Validate the Invoice and GST details result
  #When User enters delivery date
  #Then User clicks on save button and validate
  #Given User Navigates to Trip Management
  #Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #When User Enters Details In AddShipment Tab Booking Way through filter
  #When User Assigns AWBNumber To Trip
  #Then User Enters ODO Reading "<ODO Reading>"
  #Then User Scans AWBNumber & ValidateSucessMessage Booking Way"<Message>"
  #Then Validate Trip created details Booking Way "<Assigned To>","<ODO Reading>"
  #Then User will pick up the mps from Hub
  #Then validate the status "<Trip Status>"
  #Given User Navigates to Trip Management
  #Given User opens the Trip details "<TripStatusCloseTrip>","<Assigned To>"
  #When User Navigates To Close Trip and Scans AWBs Booking way "<NumberOfInvoices>"
  #Then user Enters closure ODO reading "<ClosureODOReading>"
  #Then User closes trip and validates trip closure message"<MessageCloseTrip>"
  #Examples:
  #| BA/SR Dropdown                  | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | MessageODA                                                              | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | Assigned To            | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | ODO Reading | Message                   | Trip Status | TripStatusCloseTrip | MessageCloseTrip         | ClosureODOReading | Productcategories | Referenceno | Clientreferenceno | InvoiceNumber  | InvoiceValue | invoicegeneratedText                                        |
  #| Prashik Mankar - TSP BA (24422) | lenovo     | Surface    | Credit       |        411054 |      411054 | Serviceable   | Consignee pincode is ODA, please click on OK if you still want to book. | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | Prashik Mankar (24422) | Mauti Van            | MH 12 XX 8819         | 4W                  |         212 | Trip Started Successfully | picked      | Started             | Please enter valid close trip ODO reading |               100 |                   | sdf1        | ssad1             | test00445121a7 |          100 | Total Invoice Value should be equal to Total Shipment Value |
  #
  #@Cargo_FLM_TripManagement_VehicleCapacity
  #Scenario Outline: <Tc> : Trip Management Delivery
  #Given Navigate to Cargo Booking option
  #When User navigates to Create booking option
  #When Enter client name "<clientName>" and enters serviceablity information for ODA validation "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>","<MessageODA>"
  #When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
  #When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
  #Then Verify the AWB genarated and verify the status "<Status>"
  #Then Capture the AWBNumber Generated
  #Then User clicks on consignee details tab
  #When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
  #When User clicks on Save and Next button
  #Then User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
  #| Electrical Goods |
  #| Liquid           |
  #When Userr enters Invoice and GST details Dynamic FLM "<InvoiceValue>"
  #Then User clicks on Save and Next button and Validate the Invoice and GST details result
  #When User enters delivery date
  #Then User clicks on save button and validate
  #Given User Navigates to Trip Management
  #Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #Then Validate the Vehicle capacity "<Vehicle Capacity>"
  #
  #Examples:
  #| BA/SR Dropdown                  | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | MessageODA                                                              | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | Assigned To            | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | ODO Reading | Message                   | Trip Status | TripStatusCloseTrip | MessageCloseTrip         | ClosureODOReading | Productcategories | Referenceno | Clientreferenceno | InvoiceNumber  | InvoiceValue | invoicegeneratedText                                        | Vehicle Capacity |
  #| Prashik Mankar - TSP BA (24422) | lenovo     | Surface    | Credit       |        411054 |      411054 | Serviceable   | Consignee pincode is ODA, please click on OK if you still want to book. | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | Prashik Mankar (24422) | Mauti Van            | MH 12 XX 8819         | 4W                  |         212 | Trip Started Successfully | picked      | Started             | Please enter valid close trip ODO reading |               100 |                   | sdf1        | ssad1             | test00445121a7 |          100 | Total Invoice Value should be equal to Total Shipment Value | Available Capacity (kg): 10000000.00 |
  #
  #
  #
  #@Cargo_FLM_TripManagement_VehicleCapacity_overload
  #Scenario Outline: <Tc> : Trip Management Delivery
  #Given Navigate to Cargo Booking option
  #When User navigates to Create booking option
  #When Enter client name "<clientName>" and enters serviceablity information for ODA validation "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>","<MessageODA>"
  #When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
  #When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
  #Then Verify the AWB genarated and verify the status "<Status>"
  #Then Capture the AWBNumber Generated
  #Then User clicks on consignee details tab
  #When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
  #When User clicks on Save and Next button
  #Then User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
  #| Electrical Goods |
  #| Liquid           |
  #When Userr enters Invoice and GST details Dynamic FLM "<InvoiceValue>"
  #Then User clicks on Save and Next button and Validate the Invoice and GST details result
  #When User enters delivery date
  #Then User clicks on save button and validate
  #Given User Navigates to Trip Management
  #Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #When User Enters Details In AddShipment Tab Booking Way through filter
  #When User Assigns AWBNumber To Trip Overload
  #Then Validate Error message Overload "<ErrorOverload>"
  #Examples:
  #| BA/SR Dropdown                  | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | MessageODA                                                              | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | Assigned To            | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | ODO Reading | Message                   | Trip Status | TripStatusCloseTrip | MessageCloseTrip         | ClosureODOReading | Productcategories | Referenceno | Clientreferenceno | InvoiceNumber  | InvoiceValue | invoicegeneratedText                                        | ErrorOverload |
  #| Prashik Mankar - TSP BA (24422) | lenovo     | Surface    | Credit       |        411054 |      411054 | Serviceable   | Consignee pincode is ODA, please click on OK if you still want to book. | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | Prashik Mankar (24422) | Auto           | MH 12 XX 8819         | 4W                  |         212 | Trip Started Successfully | picked      | Started             | Please enter valid close trip ODO reading |               100 |                   | sdf1        | ssad1             | test00445121a7 |          100 | Total Invoice Value should be equal to Total Shipment Value | Selected AWBs weighs more than vehicle capacity by 10.00 KGS |
  #
  #@Cargo_FLM_TripManagement_Sanity_Different_Hub
  #Scenario Outline: <Tc> : Sanity Complete Flow from Booking to Delivery for Different Hub
  #Given User changes hub to Pickup hub "<PickUpHub>"
  #Given Navigate to Cargo Booking option
  #When User navigates to Create booking option
  #When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
  #When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
  #When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
  #Then Verify the AWB genarated and verify the status "<Status>"
  #Then Capture the AWBNumber Generated
  #Then User clicks on consignee details tab
  #When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
  #When User clicks on Save and Next button
  #Then User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
  #| Electrical Goods |
  #| Liquid           |
  #When Userr enters Invoice and GST details Dynamic FLM "<InvoiceValue>"
  #Then User clicks on Save and Next button and Validate the Invoice and GST details result
  #When User enters delivery date
  #Then User clicks on save button and validate
  #Given User Navigates to Trip Management
  #Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
  #When User Enters Details In AddShipment Tab Booking Way through filter
  #When User Assigns AWBNumber To Trip
  #Then User Enters ODO Reading "<ODO Reading>"
  #Then User Scans AWBNumber & ValidateSucessMessage Booking Way"<Message>"
  #Then Validate Trip created details Booking Way "<Assigned To>","<ODO Reading>"
  #Then User will pick up the mps from Hub
  #Then validate the status "<Trip Status>"
  #Given User Navigates to Trip Management
  #Given User opens the Trip details "<TripStatusCloseTrip>","<Assigned To>"
  #When User Navigates To Close Trip and Scans AWBs Booking way "<NumberOfInvoices>"
  #Then user Enters closure ODO reading "<ClosureODOReading>"
  #Then User closes trip and validates trip closure message"<MessageCloseTrip>"
  #Given User hits awb verify api
  #When User hits Hubops API
  #Given User changes hub to Pickup hub "<DeliveryHub>"
  #Given User Navigates to Trip Management
  #Given User closes trip from DB if trip is started already "<BA/SR Dropdown Delivery>"
  #Given User Navigates to Create Booking Trip Tab
  #When User Enters Assignment Person and contracted Vehicle Details "<Assigned ToDelivery>","<Contracted Vehicle Number>"
  #When User Navigates to create trip and Starts the Trip
  #When User hits delivery apis
  #Then user navigates to close trip
  #Given User changes hub to Pickup hub "<PickUpHub>"
  #
  #Examples:
  #| BA/SR Dropdown                  | BA/SR Dropdown Delivery  | PickUpHub | DeliveryHub | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | Assigned To            | Assigned ToDelivery | Contracted Vehicle Number | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | ODO Reading | Message                   | Trip Status | TripStatusCloseTrip | MessageCloseTrip         | ClosureODOReading | Productcategories | Referenceno | Clientreferenceno | InvoiceNumber  | InvoiceValue | invoicegeneratedText                                        |  |
  #| Prashik Mankar - TSP BA (24422) | Sagar Yadav - SR (15838) | PNQ/WKD   | PNQ/BAN     | lenovo     | Surface    | Credit       |        411054 |      411045 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | Prashik Mankar (24422) | Sagar Yadav (15838) | MH12DT9218 (TATA MAGIC)   | Mauti Van            | MH 12 XX 8819         | 4W                  |         212 | Trip Started Successfully | picked      | Started             | Trip Closed Successfully |               300 |                   | sdf1        | ssad1             | test00445121a7 |          100 | Total Invoice Value should be equal to Total Shipment Value |  |
  #
  @CreateAwbAssignFirstMileTripMultipleAWBs
  Scenario Outline: <Tc> : Assign Trip From Web
    Given User Navigates to Trip Management
    When User Navigates to Create Booking Trip Tab
    When User generates number of AWB <NoOfAWB> for pincode "<Pincode>"
    Then User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
    When User Enters Details In AddShipment Tab AWB
    Then User Assigns AWBNumber To Trip
    When User Enters ODO Reading "<ODO Reading>"
    And User Scans all AWBNumber & ValidateSucessMessage "<Message>"
    When User will pick up the mps from Hub MultipleAWb
    Then search for and close the first mile trip
    Given User hits awb verify api Multiple
    Given User Navigates to Trip Management
    When User Navigates to Create Booking Trip Tab
    Then User Enters Assignment Person and AD hoc Vehicle Details LM "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
    And user Click on Last mile tab and enter multiple AWB and start the Last mile trip
    Then User hits delivery APIs MultipleAWBs
    Then search for assigned trip and click on view trip then click on close trip button

    Examples: 
      | Assigned To         | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | AWB Number    | ODO Reading | Message                   | Username                       | Password       | NoOfAWB | Pincode |
      | Sagar Yadav (15838) | Maruti Van           | MH 12 XX 8819         | 4W                  | 9169145910980 |           1 | Trip Started Successfully | vishal.pachpute@xpressbees.com | XpressBees@123 |      50 |  411045 |

  @CreateAwbAssignFirstMileTripMultipleAWBs_DataCreation
  Scenario Outline: <Tc> : Assign Trip From Web
    Given User Navigates to Trip Management
    When User Navigates to Create Booking Trip Tab
    When User generates number of AWB <NoOfAWB> for pincode "<Pincode>"
    When User Enters Assignment Person and contracted Vehicle Details "<Assigned ToDelivery>","<Contracted Vehicle Number>"
    When User Enters Details In AddShipment Tab AWB
    Then User Assigns AWBNumber To Trip
    When User Enters ODO Reading "<ODO Reading>"
    And User Scans all AWBNumber & ValidateSucessMessage "<Message>"
    When User will pick up the mps from Hub MultipleAWb
    Then search for and close the first mile trip
    #Given User hits awb verify api Multiple
    #Given User Navigates to Trip Management
    #When User Navigates to Create Booking Trip Tab
    #Then User Enters Assignment Person and AD hoc Vehicle Details LM "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
    #And user Click on Last mile tab and enter multiple AWB and start the Last mile trip
    #Then User hits delivery APIs MultipleAWBs
    #Then search for assigned trip and click on view trip then click on close trip button

    Examples: 
      | Assigned To         | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | AWB Number    | ODO Reading | Message                   | Username                       | Password       | NoOfAWB | Pincode | Assigned ToDelivery | Contracted Vehicle Number |
      | Sagar Yadav (15838) | Maruti Van           | MH 12 XX 8819         | 4W                  | 9169145910980 |           1 | Trip Started Successfully | vishal.pachpute@xpressbees.com | XpressBees@123 |      50 |  411045 | Sagar Yadav (15838) | MH12DT9218 (TATA MAGIC)   |
