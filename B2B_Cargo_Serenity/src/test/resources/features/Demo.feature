@CargoDemo
Feature: Cargo Demo

  @CargoLoginDemo
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

  @CargoPrintStickerDemo
  Scenario Outline: <TC>: Demo Cargo Print Sticker - Happy flow for AWB Sticker and Carton Number
    Given Navigate to Cargo Print Sticker option
    When User selects radio button "<StickerOption>" and enters "<AWBorCartonNo>" and clicks on Search button.
    Then Validate MPS details are shown
    When User Prints first MPS or Carton
    Then Validate error message as "<ErrMsg_NoPrinterInstalled>". User clicks on OK button.
    When User Prints all MPS or Carton
    Then Validate error message as "<ErrMsg_NoPrinterInstalled>". User clicks on OK button.

    Examples: 
      | StickerOption    | AWBorCartonNo  | ErrMsg_NoPrinterInstalled             |
      | Cargo AWB Number | 94296420217089 | Error: Please Install Printing Device |
      | Cargo AWB Number | 94296420217088 | Error: Please Install Printing Device |

  @Cargo_SoftBooking_lenovoClient_Demo
  Scenario Outline: <TC>: Demo Cargo Booking - Happy flow for Cargo Booking for lenovo client
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then Verify the details saved with AWB Number and other details info "<clientName>","<Route Mode>","<Booking Mode>","<pickupPincode>","<dropPincode>","<LocationName>","<Mobile>","<Email>","<Address>","<GSTNo>","<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>","<Length>","<Breadth>","<Height>","<NoOfBoxes>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        |
      | MotoG      | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated |
