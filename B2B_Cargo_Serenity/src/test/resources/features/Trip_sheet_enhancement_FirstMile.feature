@Trip_sheet_enhancement_FirstMile
Feature: Trip Sheet Enhancement feature

 @CargoLoginBooking
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | summit.sakhre@xpressbees.com   | Summ@1080       |
     
     
   @Cargo_CompleteBooking_HappyFlow_And_Validating_TripSheet
  Scenario Outline: <TC>: Cargo Booking - Happy flow for Cargo Booking for MotoGClient
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    When User enters additional box details "<Length2>","<Breadth2>","<Height2>","<NoOfBoxes2>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then Capture the AWBNumber Generated
    Then Capture the AWBNumber from Rapid Booking
    #Then Verify the details saved with AWB Number and other details info "<clientName>","<Route Mode>","<Booking Mode>","<pickupPincode>","<dropPincode>","<LocationName>","<Mobile>","<Email>","<Address>","<GSTNo>","<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>","<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then User clicks on consignee details tab
    When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
    Then User clicks on Save and Next button and Validate record entered successful
    When User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
    |Electrical Goods|
    |Liquid|
    |Metal| 
    When User clicks on Save and Next button
    When User enters Invoice and GST details less then 50K "<InvoiceValue>"
    Then User clicks on Save and Next button and Validate the Invoice and GST details result
    Then User clicks on Save and Next button to complete Booking
    Given User Navigates to Trip Management
    Given User closes trip from DB if trip is started already "<BA/SR Dropdown>"
  	Given User Navigates to Create Booking Trip Tab
  	When User Enters Assignment Person and contracted Vehicle Details "<Assigned To>","<Contracted Vehicle Number>"
  	When User Enters Details In AddShipment Tab Booking Way
  	When User Assigns AWBNumber To Trip
  	Then User Enters ODO Reading "<ODO Reading>"
  	Then User Scans AWBNumber & ValidateSucessMessage Booking Way"<Message>"
  	Then Validate Trip created details Booking Way "<Assigned To>","<ODO Reading>"
  	Then User clicks on Trip Sheet Button
  	And User validate the assigned records to SR "<Contracted Vehicle Number>","<Assigned To>","<ODO Reading>","<clientName>","<Address>","<pickupPincode>","<NumberMPS>","<TotalShipmentDeclaredWeight>"
  	
    
    Examples: 
     | BA/SR Dropdown | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Length2 | Breadth2 | Height2 | NoOfBoxes2 | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | Productcategories | Referenceno | Clientreferenceno  | InvoiceValue | Assigned To           | Contracted Vehicle Number | ODO Reading | Message                   | 
     | PRAMOD BIHARI - SR (22113) | MotoG      | Surface    | Credit       |        411045 |      411045 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 18AABCU9603R1ZM |   0201254 |                1 |                    1000.00 |         4 |                       10.00 |     50 |      50 |     50 |         1 |      50 |       50 |      50 |          3 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | 									 | auto1       | rauto1             |        1000  |PRAMOD BIHARI (22113)  | JU 21 N 0002 						 | 10	         | Trip Started Successfully |
      
      
   

    