@ChangePhysicalWeight
Feature: Change Physical Weight feature

  @ChangePhysicalWeightLogin
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

  @ChangePhysicalWeight_HappyFlow
  Scenario Outline: <TC>: Change Physical Weight Happy Flow
    Given Navigate to Support page
    When Clicked on Change Physical weight link
    When Shipment is id "<ShipemntID>" and clicked on Search button
    Then It should display update button
    When Enter "<physicalwt>" and clicked on update
    Then Proper "<Successmsg>" should displayed

    Examples: 
      | ShipemntID     | physicalweight | Successmsg           |
      | 94192321420091 |            120 | Updated Successfully |

  @ChangePhysicalWeight_Navigation
  Scenario Outline: <TC>: Change Physical Weight navigation
    Given Navigate to Support page
    When Clicked on Change Physical weight link
    Then User should navigate on "<ChangePhysicalWeight>" page

    Examples: 
      | ChangePhysicalWeight   |
      | Change Physical Weight |

  @ChangePhysicalWeight_Shipment_id_validations
  Scenario Outline: <TC>: Change Physical Weight Shipment id field validations
    Given Navigate to Support page
    When Clicked on Change Physical weight link
    When Shipment is id "<ShipemntID>" and clicked on Search button
    Then Proper validation message should displayed "<ErrormsgonInvalidShipmentID>"

    Examples: 
      | ShipemntID | ErrormsgonInvalidShipmentID       |
      |            | The Shipment Id field is required |
      |            | The Shipment Id field is required |

  @ChangePhysicalWeight_DeliveredAWB
  Scenario Outline: <TC>: Change Physical Weight for delivered AWB
    Given Navigate to Support page
    When Clicked on Change Physical weight link
    When Shipment is id "<ShipemntID>" and clicked on Search button
    When Enter "<physicalwt>" and clicked on update
    Then It should show message "<DeliveredAWBMsg>"

    Examples: 
      | ShipemntID     | physicalwt | DeliveredAWBMsg                       |
      | 94192321420101 |        120 | Invalid! Shipment Status is Delivered |

  @ChangePhysicalWeight_ResetButton
  Scenario Outline: <TC>: Change Physical Weight Reset button functionality
    Given Navigate to Support page
    When Clicked on Change Physical weight link
    When Shipment is id "<ShipemntID>" and clicked on Search button
    When click on reset button
    Then "<shipmentid>" should get blank

    Examples: 
      | ShipemntID     | shipmentid |
      | 94192321420091 |            |
