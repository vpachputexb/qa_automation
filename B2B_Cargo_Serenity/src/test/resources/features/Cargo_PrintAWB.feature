@Cargo_PrintAWBFeature
Feature: Cargo Print AWB

  @CargoLoginPrintAWB
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page
     Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |
      
      
  @Cargo_PrintAWB
  Scenario Outline: <TC>: Print AWB - Happy flow for PrintAWB
    Given Navigate to Print AWB option
    When User enters "<AWBNumber>" in the text field. User clicks on Validate AWB button
    Then Validate Details displayed. Also check entered "<AWBNumber>" is validated
    When User clicks on Print Button. User cliks escape button for printing to cancel

    Examples: 
      | AWBNumber      |
      | 94185221110023 |
      |  9422952100010 |

  @Cargo_PrintAWB_MultipleAWBs
  Scenario Outline: <TC>: Print AWB - Happy flow for multiple AWBs
    Given Navigate to Print AWB option
    When User enters "<AWBNumber>" in the text field. User clicks on Validate AWB button
    Then Validate Details displayed. Also check entered "<AWBNumber>" is validated
    When User clicks on Print Button. User cliks escape button for printing to cancel

    Examples: 
      | AWBNumber                                                                                                                                                                                                                                                                                     |
      |                                                                                                                                                                                                                       94185221110023,9422362130368,9421772130019,94185221110024,9421772130018 |
      | 94185221110023,9422362130368,9421772130019,94185221110024,9421772130018,94185221110230,94185221110224,9421772130078,941852211101781,94185221110229,948425687656,933452100008,9421772130077,941852211101779,933452100007,933452100006,933452100005,9421772130076,941852211101780,9421772130075 |
      |               94185221110023,9422362130368,9421772130019,94185221110024,9421772130018,94185221110230,94185221110224,9421772130078,941852211101781,94185221110229,948425687656,933452100008,9421772130077,941852211101779,933452100007,933452100006,933452100005,9421772130076,941852211101780 |

  @Cargo_PrintAWB_NegativeScenarios_AWB
  Scenario Outline: <TC>: Print AWB - Flow for blank and max entered value
    Given Navigate to Print AWB option
    When User enters inavlid AWBs "<AWBNumber>" in the text field. User clicks on Validate AWB button
    Then Validate Error message displayed "<AWBNumber>" and check error message "<ErrorMessage>"

    Examples: 
      | AWBNumber                                                                                                                                                                                                                                                                                      | ErrorMessage | 
      |                                                                                                                                                                                                                                                                                                | The AWB Number field is required |
      | 94185221110023,9422362130368,9421772130019,94185221110024,9421772130018,94185221110230,94185221110224,9421772130078,941852211101781,94185221110229,948425687656,933452100008,9421772130077,941852211101779,933452100007,933452100006,933452100005,9421772130076,941852211101780,9421772130075, | Maximum of 20 AWB(s) you can enter. |

  @Cargo_PrintAWB_NegativeScenarios_InvalidAWBs
  Scenario Outline: <TC>: Print AWB - Flow for invalid AWB validation
    Given Navigate to Print AWB option
    When User enters "<AWBNumber>" in the text field. User clicks on Validate AWB button
    Then Validate Details displayed for invalid AWBs "<Message>"

    Examples: 
      | AWBNumber     | Message |
      | aaaaaa12      | No Valid AWB(s) details found |
      | 12#2@         | No Valid AWB(s) details found |
      | 9418522111002 | No Valid AWB(s) details found |

  @Cargo_PrintAWB_NegativeScenario_Valid_Invalid_CombineInputAWBs
  Scenario Outline: <TC>: Print AWB - Happy flow for PrintAWB
    Given Navigate to Print AWB option
    When User enters "<AWBNumber>" in the text field. User clicks on Validate AWB button
    Then Validate number of AWB Details displayed

    Examples: 
      | AWBNumber                 |
      | 94185221110023,9422952100 |
