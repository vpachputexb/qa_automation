@ShiftShipmentDestinationHubFeature
Feature: ShiftShipment Destination Hub Feature

  @ShiftShipmentDestinationHubLogin
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                   | Password   |
      | atul.deokar@xpressbees.com | Xpress@123 |

  @ShiftShipmentDestinationHubHappyFLow
  Scenario Outline: <TC>: HappyFLow
    Given Navigate to shift shipment destinationhub module
    When User enters "<AWB Number>"
    When User selects change destination hub "<Changed Hub>"
    When User enters comments "<Comments>"
    Then User Clicks on update button
    Then Validate successmessage "<SuccessMessage>"

    Examples: 
      | AWB Number    | Changed Hub | Comments | SuccessMessage                       |
      | 9422072100787 | Aurangabad      | Test     | Destination Hub Successfully Updated |

  @ShiftShipmentDestinationHubHappyFLow_Multiple
  Scenario Outline: <TC>: HappyFLow
    Given Navigate to shift shipment destinationhub module
    When User enters "<AWB Number>"
    When User selects change destination hub "<Changed Hub>"
    When User enters comments "<Comments>"
    Then User Clicks on update button
    Then Validate successmessage "<SuccessMessage>"

    Examples: 
      | AWB Number    | Changed Hub | Comments | SuccessMessage                       |
      | 9422072100788 | Aurangabad      | Test     | Destination Hub Successfully Updated |

  @ShiftShipmentDestinationHubHappyFLow_BlankErrorMessage
  Scenario Outline: <TC>: HappyFLow
    Given Navigate to shift shipment destinationhub module
    Then User Clicks on update button
    Then Validate error messages "<Name error Message>", "<final hub error message>", "<error comments message>"

    Examples: 
      | AWB Number                  | Changed Hub | Comments | SuccessMessage                       | Name error Message          | final hub error message       | error comments message |
      | 9422072100787,9422072100789 | Agra    | Test     | Destination Hub Successfully Updated | Please Enter Shipment ID(s) | Please select Destination hub | Please enter comments  |

  @ShiftShipmentDestinationHub_SameHubValidationError
  Scenario Outline: <TC>: HappyFLow
    Given Navigate to shift shipment destinationhub module
    When User enters "<AWB Number>"
    When User selects change destination hub "<Changed Hub>"
    When User enters comments "<Comments>"
    Then User Clicks on update button
    Then Validate successmessage samehub "<ErrorMessage>","<AWB Number>"

    Examples: 
      | AWB Number      | Changed Hub | Comments | ErrorMessage                                         |
      |   9422072100787 | Aurangabad      | Test     | Invalid! Previous and new final destination are same |
      |  94192321260003 | NASHIK      | Test     | Invalid! Shipment Status is Delivered                |
      | 941923214203825 | Nashik      | Test     | Shipment not Found                                   |

  @ShiftShipmentDestinationHub_FormatValidation
  Scenario Outline: <TC>: HappyFLow
    Given Navigate to shift shipment destinationhub module
    When User enters "<AWB Number>"
    When User selects change destination hub "<Changed Hub>"
    When User enters comments "<Comments>"
    Then User Clicks on update button
    Then Validate errorMessage format "<ErrorMessage>"

    Examples: 
      | AWB Number                   | Changed Hub | Comments | ErrorMessage                     |
      | 9422072100488 94192321420382 | NASHIK      | Test     | Please follow the correct format |
