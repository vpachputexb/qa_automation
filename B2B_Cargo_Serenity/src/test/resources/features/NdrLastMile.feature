@AssignTripForLastMile
Feature: Assign Trip For LastMile

  @AssignLastmileTripFromWeb
  Scenario Outline: <Tc> : Assign Lastmile trip From web
    Given User is no Cargo application page
    When User enter username "vishal.pachpute@xpressbees.com" and password "XpressBees@123"
    Then User login WMS application successfully
    Given User Navigates to Trip Management
    When User Navigates to Create Booking Trip Tab
    Then User Enters Assignment Person and AD hoc Vehicle Details "<Assigned To>","<Ad-hoc Vehicle Model>","<Ad-hoc Vehicle Number>","<Ad Hoc Vehicle Type>"
    And user Click on Last mile tab and enter AWB "<AWB>"
    And LogOut Web App
    
    Examples: 
      | Assigned To     | Ad-hoc Vehicle Model | Ad-hoc Vehicle Number | Ad Hoc Vehicle Type | AWB              | ODO Reading | Message                   | Username                       | Password       |
      | Akshay Galphade | PIAGGO               | MH 12 XX 8811         | 4W                  | 94192321420537	 	|           1 | Trip Started Successfully | vishal.pachpute@xpressbees.com | XpressBees@123 |


  @CargoAndroidLogin
  Scenario Outline:Login Cargo Application on mobile
    Given User Open Cargo Mobile Application
    When User enter login creadentials on Mobile Application of Cargo
    Then Cargo Mobile application login successfully
  Then Mark Ndr reason "<NdrReason>" and select "<NdrDate>" and submit non dilevered reason
  
  Examples:
  | NdrReason      | NdrDate    |
  | Self collect   | 2022-04-21 |
   


  @closeTheAssignedTripFromWeb
  Scenario Outline: <Tc> : Login to Web and close the assigned trip From Web
    #//While running Change AWB and Ndr Date accordingly
    Given User is no Cargo application page
    When User enter username "vishal.pachpute@xpressbees.com" and password "XpressBees@123"
    Then User login WMS application successfully
    Given User Navigates to Trip Management
    Then search for "<AWB>" and click on view trip then click on close trip button
    #//Unable to close the trip "Close button" is disabled 
   # Then Search for closed trip "<NdrAWB>" and validate NDR date "<NdrDate>" is there
    #And Search for Value Added Services Awb "<VasAwb>" and validate date is there "<VasDate>"
  

    Examples: 
       | NdrAWB        | Invoices | OdoReading | AWB              |NdrDate       | VasAwb         | VasDate    |
       | 94192321420510|        1 |          2 | 94192321420377	  | 	20-04-2022 | 948888888888   | 18-04-2022 |
        
      






 
 #############################################################################################
 #@HitValidateAWBaPI 
  #Scenario Outline: Hit To Validate AWB API
    #Given Api User need to hit the AWB verification end point with access token
      #| baseUri     | http://api.staging.shipmentupdates.xbees.in/ |
      #| accessToken | c423ed60f34c0021016fc65cb53a9ad4             |
    #When Api User need to verify AWB and MPS no with API data <AWB> <MPSno>
    #When Api user need to hit Post Request for verification <Post>
    #Then Api User need to validate the shipment status code <status code>
    #And User need to validate the message <msg> of the body
#
    #Examples: 
      #| status code | Post                  | AWB           | MPSno           | msg                        |
      #|         103 | updateawbverification | 9422072100404 | MPS372922170173 | shipment status is invalid |
 #
 
  
  
  
  
  
  
  
  
  
  
  
  
  
    
    
