@Cargo_TnC
Feature: Slabs

  @CargoLoginTnC
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                         | Password   |
      | admin.atul.deokar@xpressbees.com | Xpress@123 |

  @Cargo_HappyFlowTnC
  Scenario Outline: <TC>: T&C- Happy flow for T&C
  Given User Navigates to Master data1 option
  When User Navigates to create new TnC option
  Then User enters "<TermsandConditionsTemplateName>" in the text field
  Then User enters "<TermsandConditionsTemplateDescription>" in the text field and clicks on Expand All button
  Then User selects Billing Cycle "<BillingCycle>" and select POD Hard Copy Required value and enter date "<date>"
  Then User enters Billing terms "<CreditPeriod>","<Creditlimit>","<Advancepayment>","<Creditlimitalert>" and user selects Payment mode Cheque
  Then User enters Other terms and conditions "<Maxliabilityamount>"
  Then User clicks on Save button
  Then User validates successful msg and clicks on Ok "<Successmsg>"
  
  Examples:
  | TermsandConditionsTemplateName | TermsandConditionsTemplateDescription | BillingCycle | date | CreditPeriod | Creditlimit | Advancepayment | Creditlimitalert | Maxliabilityamount | Successmsg                   |
  | T&C135                          | Test                                  | Monthly      |    7 |            5 |         100 |             10 |                5 |              10000 | Record created successfully. |
  
  @Cargo_SelectionofFortnightlycycle
  Scenario Outline: <TC>: T&C- Selection of Fortnightly cycle
  Given User Navigates to Master data1 option
  When User Navigates to create new TnC option
  Then User enters "<TermsandConditionsTemplateName>" in the text field
  Then User enters "<TermsandConditionsTemplateDescription>" in the text field and clicks on Expand All button
  Then User selects Billing Cycle "<BillingCycle>" and select POD Hard Copy Required value and enter dates "<date1>" "<date2>"
  Then User enters Billing terms "<CreditPeriod>","<Creditlimit>","<Advancepayment>","<Creditlimitalert>" and user selects Payment mode Cheque
  Then User enters Other terms and conditions "<Maxliabilityamount>"
  Then User clicks on Save button
  Then User validates successful msg and clicks on Ok "<Successmsg>"
  
  Examples:
  | TermsandConditionsTemplateName | TermsandConditionsTemplateDescription | BillingCycle | date1 | date2 | CreditPeriod | Creditlimit | Advancepayment | Creditlimitalert | Maxliabilityamount | Successmsg                   |
  | T&C136                          | Test                                  | Fortnightly  |     7 |    14 |            5 |         100 |             10 |                5 |              10000 | Record created successfully. |
  
  @Cargo_SelectionofWeeklycycle
  Scenario Outline: <TC>: T&C- Selection of Weekly cycle
  Given User Navigates to Master data1 option
  When User Navigates to create new TnC option
  Then User enters "<TermsandConditionsTemplateName>" in the text field
  Then User enters "<TermsandConditionsTemplateDescription>" in the text field and clicks on Expand All button
  Then User selects Billing Cycle "<BillingCycle>" and select POD Hard Copy Required value
  Then User enters Billing terms "<CreditPeriod>","<Creditlimit>","<Advancepayment>","<Creditlimitalert>" and user selects Payment mode Cheque
  Then User enters Other terms and conditions "<Maxliabilityamount>"
  Then User clicks on Save button
  Then User validates successful msg and clicks on Ok "<Successmsg>"
  
  Examples:
  | TermsandConditionsTemplateName | TermsandConditionsTemplateDescription | BillingCycle | CreditPeriod | Creditlimit | Advancepayment | Creditlimitalert | Maxliabilityamount | Successmsg                   |
  | T&C137                          | Test                                  | Weekly       |            5 |         100 |             10 |                5 |              10000 | Record created successfully. |
  
  @Cargo_Validateerrormsgformandatoryfields
  Scenario Outline: <TC>: T&C- Validate error msg for mandatory fields
  Given User Navigates to Master data1 option
  When User Navigates to create new TnC option
  Then User clicks on Save button
  Then User validates error msgs "<TermsandConditionsTemplateNameerrormsg>","<TermsandConditionsTemplateDescriptionerrormsg>","<Billingcycleerrormsg>","<PODHardCopyrequirederrormsg>","<Creditdayserrormsg>","<Maxliabilityerrormsg>"
  
  Examples:
  | TermsandConditionsTemplateNameerrormsg | TermsandConditionsTemplateDescriptionerrormsg | Billingcycleerrormsg        | PODHardCopyrequirederrormsg | Creditdayserrormsg        | Maxliabilityerrormsg             |
  | The TemplateName field is required     | The TemplateDescription field is required     | Please Select Billing Cycle | Please Select POD Hard Copy | Credit Period is required | Max Liability Amount is required |
  
  @Cargo_PODHardCopyYes
  Scenario Outline: <TC>: T&C- User selectes POD hard Copy Yes
  Given User Navigates to Master data1 option
  When User Navigates to create new TnC option
  Then User enters "<TermsandConditionsTemplateName>" in the text field
  Then User enters "<TermsandConditionsTemplateDescription>" in the text field and clicks on Expand All button
  Then User selects Billing Cycle "<BillingCycle>" and select POD Hard Copy Required value Yes
  Then User enters POD hard copy charges "<PODCharges>"
  Then User enters Billing terms "<CreditPeriod>","<Creditlimit>","<Advancepayment>","<Creditlimitalert>" and user selects Payment mode Cheque
  Then User enters Other terms and conditions "<Maxliabilityamount>"
  Then User clicks on Save button
  Then User validates successful msg and clicks on Ok "<Successmsg>"
  
  Examples:
  | TermsandConditionsTemplateName | TermsandConditionsTemplateDescription | PODCharges | BillingCycle | CreditPeriod | Creditlimit | Advancepayment | Creditlimitalert | Maxliabilityamount | Successmsg                   |
  | T&C138                          | Test                                  |         10 | Weekly       |            5 |         100 |             10 |                5 |              10000 | Record created successfully. |
  
  @Cargo_MakeStatusActive
  Scenario Outline: <TC>: T&C- User makes TnC status active
  Given User Navigates to Master data1 option
  When User clicks on filter option
  Then User enters "<TermsandConditionsTemplateName>" in the text field
  Then User clicks on Search button
  Then User clicks on Inactive status button and clicks on Ok button
  Then User validates activation successful msg and clicks on Ok "<ActivationSuccessmsg>"
  
  Examples:
  | TermsandConditionsTemplateName | ActivationSuccessmsg         |
  | T&C138                          | T&C Activated Successfully!! |
  
  @Cargo_DuplicateTnCTemplateName
  Scenario Outline: <TC>: T&C- Duplicate TnC template Name
  Given User Navigates to Master data1 option
  When User Navigates to create new TnC option
  Then User enters "<TermsandConditionsTemplateName>" in the text field
  Then User enters "<TermsandConditionsTemplateDescription>" in the text field and clicks on Expand All button
  Then User selects Billing Cycle "<BillingCycle>" and select POD Hard Copy Required value
  Then User enters Billing terms "<CreditPeriod>","<Creditlimit>","<Advancepayment>","<Creditlimitalert>" and user selects Payment mode Cheque
  Then User enters Other terms and conditions "<Maxliabilityamount>"
  Then User clicks on Save button
  Then User validates error msg and clicks on Ok "<Errormsg>"
  
  Examples:
  | TermsandConditionsTemplateName | TermsandConditionsTemplateDescription | BillingCycle | CreditPeriod | Creditlimit | Advancepayment | Creditlimitalert | Maxliabilityamount | Errormsg                                      |
  | T&C138                          | Test                                  | Weekly       |            5 |         100 |             10 |                5 |              10000 | Terms Condition Template Name Already Exists. |
  
  @Cargo_EditFunctionality
  Scenario Outline: <TC>: T&C- Edit Functionality
  Given User Navigates to Master data1 option
  When User clicks on filter option
  Then User enters "<TermsandConditionsTemplateName>" in the text field
  Then User clicks on Search button
  Then User clicks on edit button
  Then User clicks on Expand All button
  Then User clears max liability amount
  Then User enters new max liability amount "<Maxliabilityamount>"
  Then User clicks on Save button
  Then User validates successful msg and clicks on Ok "<Successmsg>"
  
  Examples:
  | TermsandConditionsTemplateName | Maxliabilityamount | Successmsg                 |
  | T&C138                          |              20000 | Record saved successfully. |
  
 
 
  @Cargo_VerifyElementsOnListingPage
  Scenario Outline: <TC>: T&C- Verify the elements on listing page
    Given User Navigates to Master data1 option
    When User clicks on filter option
    Then User enters "<TermsandConditionsTemplateName>" in the text field
    Then User clicks on Search button
    Then User validates listing elements "<TemplateName>","<TemplateDescription>","<CreatedBy>","<CreatedDate>","<LastModifiedBy>","<LastModifiedDate>","<Status>"

    Examples: 
      | TermsandConditionsTemplateName | TemplateName | TemplateDescription | CreatedBy                        | CreatedDate      | LastModifiedBy                   | LastModifiedDate | Status |
      | T&C37                          | T&C37        | Test                | admin.atul.deokar@xpressbees.com | 05-05-2022 15:53 | admin.atul.deokar@xpressbees.com | 06-05-2022 13:53 | Active |
