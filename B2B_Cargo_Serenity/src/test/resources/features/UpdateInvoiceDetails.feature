@Cargo_Support_UpdateInvoiceDetails
Feature: Cargo Update Invoice Details Feature

  @CargoLoginUpdateInvoiceDetails
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                     | Password   |
      | haribhau.tupe@xpressbees.com | Xpress@123 |

  @Cargo_AwbSearch
  Scenario Outline: <TC>: Update Invoice Details - Search button functionality
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    Then Click on search button and and Validate the button functionality

    Examples: 
      | Awbsearch      |
      | 94192321420076 |

  @Cargo_ResetButtonFunctionality
  Scenario Outline: <TC>: Update Invoice Details - Reset button functionality
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    Then Click on reset button and validate the error message "<awberrormessage>"

    Examples: 
      | Awbsearch      | awberrormessage     |
      | 94192321420075 | Please enter AWB No |

  @Cargo_BlankAwbSearchValidation
  Scenario Outline: <TC>: Update Invoice Details - Blank Awb Search button functionality
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    Then User clicks on search button and validate the error message "<awberrormessage>"

    Examples: 
      | Awbsearch | awberrormessage     |
      |           | Please enter AWB No |

  @Cargo_BlankEwbInputValidation
  Scenario Outline: <TC>: Update Invoice Details - Negative scenario for blank EWB input
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    Then User clicks on validate ewb button and validate the blank ewb input "<ewberrormessage>"

    Examples: 
      | Awbsearch     | ewberrormessage         |
      | 9422362130494 | Please enter EWB Number |

  @Cargo_InvalidAWBInputValidation
  Scenario Outline: <TC>:Update Invoice Details - Invalid AWB search validation
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    Then User clicks on search button and validate the data error message "<awbdataerrormessage>"

    Examples: 
      | Awbsearch      | awbdataerrormessage |
      | 94345435645645 | Data not found      |

  @Cargo_InvalidInvoiceDetailsValidation
  Scenario Outline: <TC>:Update Invoice Details - Invalid Invoice Details validation
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    When User Clicks on validate ewb button and clicks on information icon
    Then User Clicks on save button and validates the invalid invoice details "<invalidinvoiceerrormessage>"

    Examples: 
      | Awbsearch      | invalidinvoiceerrormessage    |
      | 94192321420076 | Invoice Details are not valid |

  @Cargo_DuplicateEWBValidation
  Scenario Outline: <TC>:Update Invoice Details - Happy flow for duplicate EWB Validation
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    When User Clicks on plus button and enters the invoice details "<Invoicenumber>","<Invoicevalue>","<EwbNumber>"
    Then validate the duplicate invoice details "<duplicateinvoiceerrormessage>"

    Examples: 
      | Awbsearch      | Invoicenumber    | Invoicevalue | EwbNumber    | duplicateinvoiceerrormessage          |
      | 94192321420075 | UPD/TAX212206878 |   7007810.00 | 471228234308 | Duplicates Invoice Numbers are exists |

  @Cargo_InvoiceDetailRowDeletion
  Scenario Outline: <TC>:Update Invoice Details - Deletion of one of the Invoice Details row
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    When User clicks on plus button and delete button
    Then validate the invoice value and Invoice number "<invoicenumbererrormessage>","<invoicevalueerrormessage>"

    Examples: 
      | Awbsearch      | invoicenumbererrormessage  | invoicevalueerrormessage  |
      | 94192321420076 | Invoice Number is required | Invoice Value is required |

  @Cargo_MultipleRowValidateEWBButtonClick
  Scenario Outline: <TC>:Update Invoice Details - Multiple Row Validate EWB  Click Validation
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    When User clicks on validate ewb button
    When User enters the invoice details "<Invoicenumber>","<Invoicevalue>","<EwbNumber>"
    Then User clicks on validate ewb button and validate the click functionality

    Examples: 
      | Awbsearch      | Invoicenumber    | Invoicevalue | EwbNumber    |
      | 94192321420076 | MSST210600182642 |      1794.56 | 381423073104 |

  @Cargo_InvalidEwbNumberValidation
  Scenario Outline: <TC>:Update Invoice Details - Less than 12 characters EWB number validation
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    Then User enters the invalid ewb number and validates the ewb number "<EwbNumberone>","<invalidewbnumbermessage>"

    Examples: 
      | Awbsearch      | EwbNumberone | invalidewbnumbermessage                          |
      | 94192321420139 |     34343434 | EWB Number is required and must be min 12 digits |

  @Cargo_ExpiredEWBValidation
  Scenario Outline: <TC>:Update Invoice Details - Expired EWB Validation in information popup
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    Then User clicks on information icon and validate EWB expiration message "<ewbexpirationmessage>"

    Examples: 
      | Awbsearch      | ewbexpirationmessage |
      | 94192321420076 | EWB Expired          |

  @Cargo_InvalidInvoiceNumberValueAndPartB
  Scenario Outline: <TC>:Update Invoice Details - invalid invoice number,value and partB Validation in information popup
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    When User enters ewb number "<EwbNumber>"
    Then User clicks on information icon and validate following error message "<errormessageinvoicenumber>","<errormessageinvoicevalue>","<partberrormessage>"

    Examples: 
      | Awbsearch      | EwbNumber     | errormessageinvoicenumber | errormessageinvoicevalue  | partberrormessage       |
      | 94192321420137 | 4322352345235 | Invalid Invoice Number    | Invoice Value not matched | Part B is not generated |

  @Cargo_AWBValidation
  Scenario Outline: <TC>:Update Invoice Details - Happy flow for AWB validation
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    Then click on validate ewb button and click on save button "<successfulmessage>"

    Examples: 
      | Awbsearch      | successfulmessage    |
      | 94192321420156 | Updated successfully |

  @Cargo_OldEwbReplacementWithNewEwb
  Scenario Outline: <TC>:Update Invoice Details - Old ewb replacement with new ewb
    Given User Navigates to Update invoice details
    When User Enters awb number "<Awbsearch>"
    When User clicks on search button
    When User enters the invoice details "<Invoicenumber>","<Invoicevalue>","<EwbNumber>"
    When User clicks on delete button1
    Then validate and click on save button "<successfulmessage>"

    Examples: 
      | Awbsearch      | Invoicenumber | Invoicevalue | EwbNumber    | successfulmessage    |
      | 94192321420153 | SB316         |      35400.0 | 371423290416 | Updated successfully |
