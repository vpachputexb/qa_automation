@CargoBooking_NewUploadfile
Feature: Cargo Booking : upload file

  @CargoLogin
  Scenario Outline: <TC> : Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

  @Cargo_FileUpload_WithoutSellerId
  Scenario Outline: <TC> : FileUpload - WithoutSellerId
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    When Select the "<filename>" and click on submit button
    Then Validate the upload details section "<Totalrecordsmessage>","<Validrecordsmessage>"

    Examples: 
      | ClientName | filename                    | Totalrecordsmessage | Validrecordsmessage |
      | MotoG      | WithoutSellerfileupload.csv | Total Records       | Valid Records       |

  @Cargo_FileUpload_DownloadTemplate
  Scenario Outline: <TC> : FileUpload - Validation of on click functionality of download template button
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    Then click on download template button and validate

    Examples: 
      | ClientName |
      | MotoG      |

  @Cargo_FileUpload_ResetFunctionality
  Scenario Outline: <TC> : FileUpload - Validation of on click functionality of reset button
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    Then click on reset button and validate

    Examples: 
      | ClientName |
      | lenovo     |

  @Cargo_FileUpload_ElementVerification
  Scenario Outline: <TC> : FileUpload - Verification Of Elements present in File Upload Page
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    Then Validate the elements present in data upload page "<Clientmessage>","<ChooseFileMessage>","<SubmitMessage>","<ResetMessage>"

    Examples: 
      | Clientmessage | ChooseFileMessage | SubmitMessage | ResetMessage |
      | Client        | Choose File       | Submit        | Reset        |

  @Cargo_FileUpload_InvalidFileUpload
  Scenario Outline: <TC> : FileUpload - Validation of invalid file Upload
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    When User selects invalid file format "<File>" and click on submit button
    Then Validate the error message "<UploadFormatErrorMessage>"

    Examples: 
      | ClientName | File        | UploadFormatErrorMessage                                                    |
      | MotoG      | Pdffile.pdf | The upload format is incorrect. Please download template for correct format |

  @Cargo_FileUpload_ResetFunctionalityForValidUpload
  Scenario Outline: <TC> : FileUpload - Validation of on functionality of reset button on valid file upload
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    When Select the "<filename>" and click on submit button
    Then click on reset button and validate

    Examples: 
      | ClientName | filename                    |
      | MotoG      | WithoutSellerfileupload.csv |

  @Cargo_FileUpload_UploadDetailsGridElementsVerification
  Scenario Outline: <TC> : FileUpload - Verification of Upload Details Grid Elements
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    When Select the "<filename>" and click on submit button
    Then Validate the upload details grid elements "<Totalrecordsmessage>","<Validrecordsmessage>","<Invalidrecordsmessage>"

    Examples: 
      | ClientName | filename                    | Totalrecordsmessage | Validrecordsmessage | Invalidrecordsmessage |
      | MotoG      | WithoutSellerfileupload.csv | Total Records       | Valid Records       | Invalid Records       |

  @Cargo_FileUpload_BlankfieldsValidation
  Scenario Outline: <TC> : FileUpload - Blank field Validation
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Click on submit button
    Then Validate the error messages "<Clienterrormessage>","<Fileerrormessage>"

    Examples: 
      | Clienterrormessage           | Fileerrormessage      |
      | The Client field is required | Choose file to upload |

  @Cargo_FileUpload_InvalidFileLinkVerification
  Scenario Outline: <TC> : FileUpload - Invalid File Link Verification
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    When User selects invalid file format "<File>" and click on submit button
    Then User clicks on invalid file link and validate the message "<InvalidFileErrorMessage>"

    Examples: 
      | ClientName | File               | InvalidFileErrorMessage |
      | MotoG      | InvalidRecords.csv | Message                 |

  @Cargo_FileUpload_WithSellerId
  Scenario Outline: <TC> : FileUpload - WithSellerId
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    When Select the "<filename>" and click on submit button
    Then Validate the upload details section "<Totalrecordsmessage>","<Validrecordsmessage>"

    Examples: 
      | ClientName     | filename                 | Totalrecordsmessage | Validrecordsmessage |
      | Nikita 7 April | WithSellerfileupload.csv | Total Records       | Valid Records       |

  @Cargo_FileUpload_CsvToXlsx
  Scenario Outline: <TC> : FileUpload - WithoutSellerId Csv to Xlsx extension
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    When Select the excel file "<excelFile>" and click on submit button
    Then Validate the error message "<UploadFormatErrorMessage>"

    Examples: 
      | ClientName | excelFile                 | UploadFormatErrorMessage                                                    |
      | MotoG      | WithSellerfileupload.xlsx | The upload format is incorrect. Please download template for correct format |

  @Cargo_FileUpload_NegativeScenarioWithoutSellerId
  Scenario Outline: <TC> : FileUpload - Negative Scenario for Motog client to upload with Seller Id
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    When Select the excel file "<excelFile>" and click on submit button
    Then Validate the file header error message "<FileHeaderErrorMessage>"

    Examples: 
      | ClientName | excelFile                | FileHeaderErrorMessage                                                                 |
      | MotoG      | WithoutSellerfileupload.csv | File headers not matched. Kindly use Download Template option for correct file format. |

  @Cargo_FileUpload_NegativeScenarioWithSellerId
  Scenario Outline: <TC> : FileUpload - Negative Scenario for Nikita 7 April client to upload with Seller Id
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    When Select the without seller id file "<withoutSellerIdFile>" and click on submit button
    Then Validate the file header error message "<FileHeaderErrorMessage>"

    Examples: 
      | ClientName     | withoutSellerIdFile         | FileHeaderErrorMessage                                                                 |
      | Nikita 7 April | WithSellerfileupload.csv | File headers not matched. Kindly use Download Template option for correct file format. |

  @Cargo_FileUpload_InvalidFileLinkVerification
  Scenario Outline: <TC> : FileUpload - Invalid File Link Verification
    Given Navigate to Cargo Booking Menu
    When User navigates to Create booking option and click on data upload button
    When Select the client "<ClientName>"
    When User selects invalid file format "<File>" and click on submit button
    When Click on the import records button and click on ok button
    Then Validate the Po error message "<POErrorMessage>"

    Examples: 
      | ClientName | File               | POErrorMessage                           |
      | MotoG      | InvalidRecords.csv | Import Failed : PO Number already exists |
