@Cargo_TnCClient
Feature: TnCClient

  @CargoLoginTnCClient
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                         | Password   |
      | admin.atul.deokar@xpressbees.com | Xpress@123 |

  @Cargo_HappyFlowTnCClient
  Scenario Outline: <TC>: T&C- Happy flow for TnC
    Given User Navigates to Client management option TnC option
    When User clicks on create new TnC option
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User validates msg and clicks on Ok "<Msg>"
    Then User clicks on save button
    Then User validates SuccessmsgTnC and clicks on Ok "<SuccessmsgTnC>"

    Examples: 
      | Companyname | Contractid     | Msg                                                                                                       | SuccessmsgTnC              |
      | lenovo      | CR-2196-2022-1 | T&C Template already exists for selected contract id CR-2196-2022-1. You can update details for the same. | Record saved successfully. |

  @Cargo_HappyFlowTnCClient
  Scenario Outline: <TC>: T&C- Happy flow change TnC template
    Given User Navigates to Client management option TnC option
    When User clicks on create new TnC option
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User validates msg1 and clicks on Ok "<Msg1>"
    Then User selects new TnC template "<TnCtemplate>"
    Then User clicks on save button
    Then User validates SuccessmsgTnC and clicks on Ok "<SuccessmsgTnC>"

    Examples: 
      | Companyname  | Contractid  | Msg1                                                                                                   | TnCtemplate | SuccessmsgTnC              |
      | lenovo | CR-2196-2022-1 | T&C Template already exists for selected contract id CR-2196-2022-1. You can update details for the same. | Harry TC    | Record saved successfully. |

  @Cargo_HappyFlowTnCClient
  Scenario Outline: <TC>: T&C- Edit pre filled details
    Given User Navigates to Client management option TnC option
    When User clicks on create new TnC option
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User validates msg1 and clicks on Ok "<Msg1>"
    Then User clicks on Expand All button
    Then User clears max liability amount
    Then User enters new max liability amount "<Maxliabilityamount>"
    Then User clicks on Save button
    Then User validates SuccessmsgTnC and clicks on Ok "<SuccessmsgTnC>"

    Examples: 
      | Companyname  | Contractid  | Msg1                                                                                                   | TnCtemplate | Maxliabilityamount | SuccessmsgTnC              |
      | Firstcry.com | CR-1-2021-1 | T&C Template already exists for selected contract id CR-1-2021-1. You can update details for the same. | Harry TC    |               5000 | Record saved successfully. |

  #@Cargo_HappyFlowTnCClient
  #Scenario Outline: <TC>: T&C- User adds new TnC as no TnC template is present for client contract combo
    #Given User Navigates to Client management option TnC option
    #When User clicks on create new TnC option
    #Then User selects company name "<Companyname>"
    #Then User selects contract id "<Contractid>"
    #Then User validates msg2 and clicks on Ok "<Msg2>"
    #Then User selects TnC template "<NewTnCTemplate>"
    #Then User clicks on Save button
    #Then User validates SuccessmsgTnC and clicks on Ok "<SuccessmsgTnC>"
#
    #Examples: 
      #| Companyname  | Contractid  | Msg2                                                                                                   | NewTnCTemplate | SuccessmsgTnC              |
      #| Firstcry.com | CR-1-2021-2 | T&C Template already exists for selected contract id CR-1-2021-2. You can update details for the same. | Harry TC       | Record saved successfully. |

  @TnCClientfiltersearchandedit
  Scenario Outline: <TC>: T&C- User searches a Tnc file and edit it
    Given User Navigates to Client management option TnC option
    When User clicks on filter option1
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User clicks on Search button1
    Then User clicks on Edit button1
    Then User clicks on save button
    Then User validates SuccessmsgTnC and clicks on Ok "<SuccessmsgTnC>"

    Examples: 
      | Companyname  | Contractid  | SuccessmsgTnC              |
      | Firstcry.com | CR-1-2021-1 | Record saved successfully. |

  @TnCClientfiltersearchandedit
  Scenario Outline: <TC>: T&C- User searches a Tnc file and edit it
    Given User Navigates to Client management option TnC option
    When User clicks on filter option1
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User clicks on Search button1
    Then User clicks on Reset button

    Examples: 
      | Companyname  | Contractid  |
      | Firstcry.com | CR-1-2021-1 |

  @TnCClientCheckmandatoryfields
  Scenario Outline: <TC>: T&C-Check mandatory fields
    Given User Navigates to Client management option TnC option
    When User clicks on create new TnC option
    Then User clicks on save button
    Then User validates TnC error msgs "<Companynameerrormsg1>", "<Contractiderrormsg1>", "<TnCTemplateerrormsg>"

    Examples: 
      | Companynameerrormsg1        | Contractiderrormsg1        | TnCTemplateerrormsg            |
      | Please select company name. | Please select contract id. | The template field is required |
