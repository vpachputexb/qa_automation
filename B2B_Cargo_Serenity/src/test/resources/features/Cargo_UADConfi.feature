@CargoUADConfig_Feature
Feature: CargoUADConfig

  @UADConfigurationLogin
   Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |
  @UADConfigUAD_menu_Navigation
  Scenario Outline: <TC>: UAD menu Visibiity under FLM menu
  Given Navigate to FLM Menu
  Then UAD menu should be displayed
  
  @UADConfigUADConfiguration_menu_Navigation
  Scenario Outline: <TC>: UAD Configuration menu Visibiity under UAD menu
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  Then UAD configuration menu should displayed
  
  @UADConfigUADConfiguration_subTabs_Displayed
  Scenario Outline: <TC>: UAD Configuration To check sub tabs displayed for UAD configuration menu
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  Then It should show sub tabs "<Tab1>","<Tab2>","<Tab3>"
  
  Examples:
  | Tab1         | Tab2             | Tab3                |
  | UAD Cut Offs | UAD Cut Offs Log | Other Configuration |
  
  @UADConfig_UAD_Cut_Off_tab_Navigation
  Scenario Outline: <TC>: UAD Configuration To check UAD cut offs tab navigation
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  Then UAD Cut off tab should displayed
  
  @UADConfig_UAD_Cut_Off_Log_tab_Navigation
  Scenario Outline: <TC>: UAD Configuration To check UAD cut offs log tab navigation
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  Then Click on UAD Cut off and check UAD Cut off log tab should displayed
  
  @UADConfig_UAD_OtherConfig_tab_Navigation
  Scenario Outline: <TC>: UAD Configuration To check Other configuration tab navigation
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  Then Click on other config atb and check Other configuration tab should displayed
  
  @UADConfig_UAD_OtherConfig_tab_fields
  Scenario Outline: <TC>: UAD Configuration To check fields displayed on Other configuration tab
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  Then Click on other config atb and check fields displayed
  
  @UADConfig_UAD_OtherConfig_tab_Max_days_validations
  Scenario Outline: <TC>: Other Configuration Max days field validations
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  When Click on Other config tab
  Then "<Maxdays>" is and clicked save then it should display "<ErrorMsg>"
  
  Examples:
  | Maxdays | ErrorMsg                                                              |
  | jjjj    | Max days for UAD re-schedule should be a Number and greater than zero |
  
  @UADConfig_UAD_OtherConfig_tab_Max_days_validations_on_Blank
  Scenario Outline: <TC>: Other Configuration Max days field validations on blank
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  When Click on Other config tab
  Then "<Maxdays1>" entered and clicked save then it should display "<ErrorMsg1>"
  
  Examples:
  | Maxdays1 | ErrorMsg1                               |
  |          | Please enter value in atleast one field |
  
  @UADConfig_UAD_OtherConfig_tab_Max_days_HsppyFlow
  Scenario Outline: <TC>: Other Configuration Max days field happy flow
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  When Click on Other config tab
  Then "<Maxdays2>" clicked save then it should display "<SuccessMsg>"
  
  Examples:
  | Maxdays2 | SuccessMsg                         |
  |        7 | Configuration Uploaded Succesfully |
  
  @UADConfig_UAD_OtherConfig_tab_consigneeReason_validations
  Scenario Outline: <TC>: Other Configuration Consignee Reason field validations
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  When Click on Other config tab
  Then Enter "<ConsigneeReason>"  and click save then it should display "<ConsigneeReasonErrorMsg>"
  
  Examples:
  | ConsigneeReason | ConsigneeReasonErrorMsg                                  |
  | kjkkk           | Max UAD Attempt should be a Number and greater than zero |
  
  @UADConfig_UAD_OtherConfig_tab_consigneeReason_validations_Black
  Scenario Outline: <TC>: Other Configuration Consignee Reason field Blank
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  When Click on Other config tab
  Then Enter blank "<ConsigneeReason1>" then click save then it should show "<ConsigneeReasonErrorMsg1>"
  
  Examples:
  | ConsigneeReason1 | ConsigneeReasonErrorMsg1                |
  |                  | Please enter value in atleast one field |
  
  @UADConfig_UAD_OtherConfig_tab_consigneeReason_happyFlow
  Scenario Outline: <TC>: Other Configuration Consignee Reason field Happy flow
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  When Click on Other config tab
  Then Enter valid "<ConsigneeReasoninp>" click save then it should show "<SuccessConsigneeReason>"
  
  Examples:
  | ConsigneeReasoninp | SuccessConsigneeReason             |
  |                  4 | Configuration Uploaded Succesfully |
  
  @UADConfig_UAD_OtherConfig_tab_OperationReason_validations
  Scenario Outline: <TC>: Other Configuration Operation Reason field validations
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  When Click on Other config tab
  Then Enter invalid "<OperationReason>"  and click save then it should display "<OperationReasonErrorMsg>"
  
  Examples:
  | OperationReason | OperationReasonErrorMsg                                  |
  | kjkkk           | Max UAD Attempt should be a Number and greater than zero |
  
  @UADConfig_UAD_OtherConfig_tab_OperationReason_validations_Blank
  Scenario Outline: <TC>: Other Configuration Operation Reason field Blank
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  When Click on Other config tab
  Then Blank "<OperationReason1>" then click save then it should show "<OperationReasonErrorMsg1>"
  
  Examples:
  | OperationReason1 | OperationReasonErrorMsg1                |
  |                  | Please enter value in atleast one field |
  
  
  @UADConfig_UAD_OtherConfig_tab_OperationReason_happyFlow
  Scenario Outline: <TC>: Other Configuration Operation Reason field Happy flow
  Given Navigate to FLM Menu
  When Navigate to UAD menu
  When Navigate to UAD Config tab
  When Click on Other config tab
  Then Enter valid input "<OperationReasoninp>" click save should show "<SuccessOperationReason>"
  
  Examples:
  | OperationReasoninp | SuccessOperationReason             |
  |                  4 | Configuration Uploaded Succesfully |
  
  @UADConfig_OtherConfig_HappyFlow
  Scenario Outline: <TC> Happy flow to save other configuration
    Given Navigate to FLM Menu
    When Navigate to UAD menu
    When Navigate to UAD Config tab
    When Click on Other config tab
    When Enter "<MAXdays>","<CONSIGNEEReason>","<OPERATIONReason>"
    Then click Save it should show "<SUCMsg>"

    Examples: 
      | MAXdays | CONSIGNEEReason | OPERATIONReason | SUCMsg                             |
      |       7 |               3 |               1 | Configuration Uploaded Succesfully |

  @UADConfig_UAD_Cut_Off_Logs_Table_verification
  Scenario Outline: <TC> Navigate to UAD Cutoff Logs table
    Given Navigate to FLM Menu
    When Navigate to UAD menu
    When Navigate to UAD Config tab
    When Navigate to UAD cut off log
    Then It should show table with heading "<SrNo>","<ModificationDate>","<RADCutoff>","<UADCutoff>","<UserName>"

    Examples: 
      | SrNo  | ModificationDate  | RADCutoff   | UADCutoff   | UserName  |
      | Sr.No | Modification Date | RAD Cut-off | UAD Cut-off | User name |
      
      
      @UADConfig_UAD_Cut_Off_Table_verification
  Scenario Outline: <TC> Navigate to UAD Cutoff table
    Given Navigate to FLM Menu
    When Navigate to UAD menu
    When Navigate to UAD Config tab
    Then It should display table with heading "<SrNo1>","<ModificationDate1>","<RADCutoff1>","<UADCutoff1>","<UserName1>"

    Examples: 
      | SrNo1  | ModificationDate1  | RADCutoff1   | UADCutoff1   | UserName1  |
      | Sr.No | Modification Date | RAD Cut-off | UAD Cut-off | User name |
      
       @UADConfig_UAD_Cut_Off_CreateCutOff
  Scenario Outline: <TC> Create UAD cut off
    Given Navigate to FLM Menu
    When Navigate to UAD menu
    When Navigate to UAD Config tab
    When Clicked on Create cut off button
    Then It should show search and reset button
             
    @UADConfig_UAD_Cut_Off_CreateCutOff_FieldsDisplayed
  Scenario Outline: <TC> Create UAD cut off page fields displayed
    Given Navigate to FLM Menu
    When Navigate to UAD menu
    When Navigate to UAD Config tab
    When Clicked on Create cut off button
    Then It should show all expected fields
 
      
      @UADConfig_UAD_Cut_Off_CreateCutOff_cutoffCreationFuncitonality
  Scenario Outline: <TC> Create UAD cut off Create cutoff functionality
    Given Navigate to FLM Menu
    When Navigate to UAD menu
    When Navigate to UAD Config tab
    When Clicked on Create cut off button
    When selected "<Hubname>","<RADInscanTime>","<UADDialyCutOffTime>"
    When clicked Save button 
   Then It should show sucess message "<Successmsg>"
    
    Examples:
    | Hubname | RADInscanTime | UADDialyCutOffTime | Successmsg |
    | PNQ/BAN | 04:00 | 05:30 | Cut-Off Time Uploaded Successfully |
