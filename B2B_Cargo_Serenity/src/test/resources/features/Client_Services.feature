@Cargo_ClientServices
Feature: ClientServices

  @CargoLoginClientServices
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                         | Password   |
      | admin.atul.deokar@xpressbees.com | Xpress@123 |

  @ClientServicesedit
  Scenario Outline: <TC>: Edit Service
  Given User Navigates to Services option
  When User clicks on filter option1
  Then User selects company name "<Companyname>"
  Then User selects contract id "<Contractid>"
  Then User clicks on Search button1
  Then Click on edit services
  Then User clicks on save button
  Then User validates SuccessmsgServices and clicks on Ok "<SuccessmsgServices>"
  Examples:
  | Companyname | Contractid      | SuccessmsgServices         |
  | test14June  | CR-1467-2022-14 | Record saved successfully. |
  
  @ClientServiceseditandchange
  Scenario Outline: <TC>: Edit Service and change SL template
  Given User Navigates to Services option
  When User clicks on filter option1
  Then User selects company name "<Companyname>"
  Then User selects contract id "<Contractid>"
  Then User clicks on Search button1
  Then Click on edit services
  Then click on PTL tab
  Then Click on edit services
  Then select service level template from drop down "<SLTemplate>"
  Then User clicks on save button
  Then User validates SuccessmsgServices and clicks on Ok "<SuccessmsgServices>"
 
  Examples:
  | Companyname | Contractid     | SLTemplate  | SuccessmsgServices         |
  | Nikita 12 April   | CR-2238-2022-9 | 15th Dec 21 | Record saved successfully. |
 
  @ClientServiceseditandchange
  Scenario Outline: <TC>: Edit Service and change SL template
  Given User Navigates to Services option
  When User clicks on filter option1
  Then User selects company name "<Companyname>"
  Then User selects contract id "<Contractid>"
  Then User clicks on Search button1
  Then Click on edit services
  Then click on PTL tab
  Then Click on edit services
  Then Select Upload file option
  Then choose a SL file "<SLfile>"
  Then click on validate file button
  Then User clicks on save button
  Then User validates SuccessmsgServices and clicks on Ok "<SuccessmsgServices>"
  Examples:
  | Companyname                       | Contractid     | SLfile   | SuccessmsgServices         |
  | FLIPKART INTERNET PRIVATE LIMITED | CR-2295-2022-1 | ClientSL | Record saved successfully. |
 
  @ClientServiceseditandaddnewservices
  Scenario Outline: <TC>: Edit Service and add new Services
  Given User Navigates to Services option
  When User clicks on filter option1
  Then User selects company name "<Companyname>"
  Then User selects contract id "<Contractid>"
  Then User clicks on Search button1
  Then Click on edit services
  Then select Route mode and Booking type
  Then Click on edit services
  Then select service level template from drop down "<SLTemplate>"
  Then User clicks on save button
  Then User validates SuccessmsgServices and clicks on Ok "<SuccessmsgServices>"
  Examples:
  | Companyname    | Contractid     | SLTemplate  | SuccessmsgServices         |
  | Nikita 7 April | CR-2235-2021-1 | 15th Dec 21 | Record saved successfully. |
 
  @ClientServicesuploadinvalidfileandvalidateerrormsgs
  Scenario Outline: <TC>: Upload Invalid file and validate error msgs
  Given User Navigates to Services option
  When User clicks on filter option1
  Then User selects company name "<Companyname>"
  Then User selects contract id "<Contractid>"
  Then User clicks on Search button1
  Then Click on edit services
  Then click on PTL tab
  Then Click on edit services
  Then Select Upload file option
  Then choose a WrongSL file "<WrongSLfile>"
  Then click on validate file button
  Then validate SL error msgs "<Mandatoryfields>", "<Greaterthan0>", "<Integer>", "<TAT>"
  Examples:
  | Companyname    | Contractid     | WrongSLfile  | Mandatoryfields                   | Greaterthan0                     | Integer                          | TAT                                            |
  | Nikita 7 April | CR-2235-2021-3 | SalesTAT.csv | Mandatory Fields Missing: E2E TAT | E2E TAT should be greater than 0 | E2E TAT should be greater than 0 | E2E TAT should not be less than OPS TAT master |
  
  @ClientServicesaddsurfacedata
  Scenario Outline: <TC>: Add surface data in upload format
  Given User Navigates to Services option
  When User clicks on filter option1
  Then User selects company name "<Companyname>"
  Then User selects contract id "<Contractid>"
  Then User clicks on Search button1
  Then Click on edit services
  Then select Route mode and Booking type
  Then Click on edit services
  Then Select Upload file option
  Then choose a Surface SL file "<SurfaceSLfile>"
  Then click on validate file button
  Then User clicks on save button
  Then User validates SuccessmsgServices and clicks on Ok "<SuccessmsgServices>"
  Examples:
  | Companyname    | Contractid     | SurfaceSLfile     | SuccessmsgServices         |
  | Nikita 7 April | CR-2235-2021-2 | SalesTATSurface.csv | Record saved successfully. |
  
  @ClientServiceswithmultiplebookingtypes
  Scenario Outline: <TC>: Add surface data with multiple booking types
  Given User Navigates to Services option
  When User clicks on filter option1
  Then User selects company name "<Companyname>"
  Then User selects contract id "<Contractid>"
  Then User clicks on Search button1
  Then Click on edit services
  Then click on PTL tab
  Then select Route mode and multiple Booking types
  Then Click on edit services
  Then select service level template from drop down "<SLTemplate>"
  Then User clicks on save button
  Then User validates SuccessmsgServices and clicks on Ok "<SuccessmsgServices>"
  Examples:
  | Companyname     | Contractid      | SLTemplate  | SuccessmsgServices         |
  | Nikita 12 April | CR-2238-2022-17 | 26th Nov SB | Record saved successfully. |
 
  @ClientServiceseditanddownloadsalesTAT
  Scenario Outline: <TC>: Edit Service and download sales TAT
  Given User Navigates to Services option
  When User clicks on filter option1
  Then User selects company name "<Companyname>"
  Then User selects contract id "<Contractid>"
  Then User clicks on Search button1
  Then Click on edit services
  Then click on PTL tab
  Then Click on edit services
  Then click on Download Sales TAT
  Then User clicks on save button
  Then User validates SuccessmsgServices and clicks on Ok "<SuccessmsgServices>"
  Examples:
  | Companyname | Contractid      | SuccessmsgServices         |
  | test14June  | CR-1467-2022-14 | Record saved successfully. |
 
  @ClientServicesvalidationerror
  Scenario Outline: <TC>: Validation error
    Given User Navigates to Services option
    When User clicks on filter option1
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User clicks on Search button1
    Then Click on edit services
    Then click on PTL tab
    Then select Route mode and multiple Booking types
    Then User clicks on save button
    Then validate validation error "<Validationerror>"

    Examples: 
      | Companyname     | Contractid      | Validationerror  |
      | Nikita 12 April | CR-2238-2022-16 | Validation Error |
