@BoxTypeFeature
Feature: Box Type feature

#Update the Box Type parameter at line no. 100,114 and 129.
  @BoxType
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

  @BoxType_Menu_visibility
  Scenario: <TC>: Check Box type menu visibility in Master menu
  Given Navigate to Master menu
  Then Check Box type menu displayed
  
  @BoxType_Menu_navigation
  Scenario: <TC>: Check Box type menu navigation
  Given Navigate to Master menu
  When click on Box type menu
  Then USer should land on Box type page
  
  @BoxType_Menu_Listing
  Scenario Outline: <TC>: Check Box type menu listing
  Given Navigate to Master menu
  When click on Box type menu
  Then Verify listing fields "<BoxType>","<BoxTypeDescription>","<StackingLevel>","<BoxStrength>","<MinWeight>","<MaxWeight>","<CreatedBy>","<CreatedDate>","<LastModifiedBy>","<LastModifiedDate>","<Status>","<Action>"
  
  Examples:
  | BoxType  | BoxTypeDescription   | StackingLevel  | BoxStrength  | MinWeight      | MaxWeight      | CreatedBy  | CreatedDate  | LastModifiedBy   | LastModifiedDate   | Status | Action |
  | Box Type | Box Type Description | Stacking Level | Box Strength | Min Weight(kg) | Max Weight(kg) | Created By | Created Date | Last Modified By | Last Modified Date | Status | Action |
  
  @BoxType_CreateBoxTypePageNavigation
  Scenario Outline: <TC>:     To check the click of Add new box type icon
  Given Navigate to Master menu
  When click on Box type menu
  When click on create button
  Then It should navigate to create box type page with heading "<CreateBoxtypeheading>"
  
  Examples:
  | CreateBoxtypeheading |
  | Box Type             |
  
  @BoxType_CreateBoXTypeElements
  Scenario: <TC>: To check the elements present on Create new box type page
  Given Navigate to Master menu
  When click on Box type menu
  When click on create button
  Then It should show all elements
  
  @BoxType_ResetButton
  Scenario Outline: <TC>: To check reset button functionality
  Given Navigate to Master menu
  When click on Box type menu
  When click on search filter
  When Enter box type "<BoxType>"
  When clicked on reset button
  Then It should clear data from boxtype "<Value>"
  
  Examples:
  | BoxType | Value |
  | Elastic |       |
  
  @BoxType_SearchFunctionality
  Scenario Outline: <TC>: To check Search  functionality
  Given Navigate to Master menu
  When click on Box type menu
  When click on search filter
  When Enter box type "<BoxType1>"
  When clicked on Search button
  Then It should show result "<Result>"
  
  Examples:
  | BoxType1      | Result           |
  | manifest_Test | Total Records: 1 |
  
  @BoxType_SearchFilter_Elements
  Scenario: <TC>: To check Search filter elements
  Given Navigate to Master menu
  When click on Box type menu
  When click on search filter
  Then It should show elements
  
  @BoxType_CreateBoXType_HappyFlow
  Scenario Outline: <TC>: To check happy flow of box type creation
  Given Navigate to Master menu
  When click on Box type menu
  When click on create button
  When Data is entered into "<BoxType>","<StackingLevel>","<BoxStrength>","<Description>","<Minimumwt>","<Maxwt>"
  When clicked on Save button
  Then It should show success message "<SuccMsg>"
 
  
  Examples:
  | BoxType | StackingLevel | BoxStrength | Description      | Minimumwt | Maxwt | SuccMsg                      |
  | Test52  | Normal        | Standard    | Test Description |        10 |    20 | Record created successfully. |
  | Test11  | Normal        | Standard    | Test             |         5 |     6 | BoxType Already Exists.      |
  
  @BoxType_BoxType_StatusCheck_oncreation
  Scenario Outline: <TC>: To check status after boxtype creation
  Given Navigate to Master menu
  When click on Box type menu
  When click on search filter
  When Enter "<Boxtype3>"
  When clicked on Search button
  Then check status of boxtype "<Status>"
  
  Examples:
  | Boxtype3          | Status   |
  | Test52 | Inactive |
  
  @BoxType_BoxType_EditBoxType
  Scenario Outline: <TC>: To check edit boxtype creation
  Given Navigate to Master menu
  When click on Box type menu
  When click on search filter
  When Enter "<Boxtype3>"
  When clicked on Search button
  When click on Edit button
  When clicked on Save button
  Then It should show record updated msg "<UpdatedMsg>"
  
  Examples:
  | Boxtype3          | Status   | UpdatedMsg                   |
  | Test52 | Inactive | Record updated successfully. |
  @BoxType_BoxType_InactivetoActive
  Scenario Outline: <TC>: To check box type activation
    Given Navigate to Master menu
    When click on Box type menu
    When click on search filter
    When select inactive status "<Status2>"
    When clicked on Search button
    When clicked on inactive status
    Then It should show popup for confirmation message "<ConfirmationMsg>"
    When clicked on OK button It should show message "<MSG>"

    Examples: 
      | Status2  | ConfirmationMsg                       | MSG                               |
      | Inactive | Are you sure to make Active/Inactive? | Box Type Activated Successfully!! |


 @BoxType_CreateBoxType_BoxTypeValidation
  Scenario Outline: <TC>:To check box type validation
  Given Navigate to Master menu
  When click on Box type menu
  When click on create button
  When Box type is entered as blank "<Boxtype3>"
  When clicked on Save button
  Then It should show validation message "<Validationmsg>"
  
  Examples:
  | Boxtype3 | Validationmsg |
  |       | The BoxType field is required |
  
  
  @BoxType_CreateBoxType_MAndaotryFieldValidations
  Scenario Outline: <TC>:To check mandatory fields validations
  Given Navigate to Master menu
  When click on Box type menu
  When click on create button
  When clicked on Save button
  Then It should show validation message "<ValidationMsgStackingLevel>","<ValidationMsgBoxStrength>","<ValidationMsgBoxTypeDesc>","<ValidationMsgminwt>","<ValidationMsgmaxwt>"
  
  Examples:
 | ValidationMsgStackingLevel | ValidationMsgBoxStrength | ValidationMsgBoxTypeDesc | ValidationMsgminwt | ValidationMsgmaxwt |
 | The StackingLevel field is required | The BoxStrength field is required | The Description field is required | The MinimumWeight field is required | The MaximumWeight field is required |
      