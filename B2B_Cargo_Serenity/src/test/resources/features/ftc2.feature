@Cargo_FTC_New_Feature
Feature: Cargo FTC feature

  @CargoLogin
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

  @Cargo_FTCTabClick
  Scenario Outline: <TC>: FTC: Validation FTC RAD shipment on-click functionality
    Given User navigates to FTC
    Then User clicks on the calculate FTC and validate the click functionality "<CalculateFTCMessage>"

    Examples: 
      | CalculateFTCMessage |
      | Calculate FTC       |

  @Cargo_FTC_SendPaymentLink
  Scenario Outline: <TC>: FTC: Validation of Send Payment Link functionality
    Given User navigates to FTC
    Then User clicks on the calculate FTC and validate the click functionality "<CalculateFTCMessage>"
    Then User clicks on the send payment link button and validate the send button functionality

    Examples: 
      | CalculateFTCMessage |
      | Calculate FTC       |

  #@Cargo_FTC_ViewPrintCashMemo
  #Scenario Outline: <TC>: FTC: Validation of View/Print cash memo functionality
    #Given User navigates to FTC
    #Then User clicks on the calculate FTC and validate the click functionality "<CalculateFTCMessage>"
    #Then User clicks on the print cash memo button and validate the cash memo "<CashMemoMessage>"
#
    #Examples: 
      #| CalculateFTCMessage | CashMemoMessage      |
      #| Calculate FTC       | View/Print Cash Memo |
#
  #@Cargo_FTC_PrintButtonFunctionality
  #Scenario: <TC>: FTC: Validation of On-click functionality of print button
    #Given User navigates to FTC
    #When User clicks on calculate FTC
    #Then User clicks on the print cash memo button and clicks on print button

  @Cargo_FTC_ValidateCalculateFTCPage
  Scenario Outline: <TC>: FTC: Validation of Elements of the Calculate FTC page
    Given User navigates to FTC
    When User clicks on calculate FTC
    Then Validate the fields "<AwbMessage>","<TotalNoOfMps>","<OriginCity>","<DestinationCity>","<PickupDate>","<Raddate>","<ConsignorName>","<ConsigneeName>"

    Examples: 
      | AwbMessage | TotalNoOfMps         | OriginCity         | DestinationCity         | PickupDate     | Raddate   | ConsignorName              | ConsigneeName              |
      | AWB:       | Total number of MPS: | Origin City (Hub): | Destination City (Hub): | Pick Up Date : | RAD Date: | Consignor Name and Address | Consignee Name and Address |

  @Cargo_FTC_SearchFunctionality
  Scenario Outline: <TC>: FTC: Search functionality validation
    Given User navigates to FTC
    When User Enters "<AwbNumber>","<ClientName>","<RadFromDate>","<RadToDate>","<CashMemo>"
    Then Click on search and Validate the button functionality

    Examples: 
      | AwbNumber      | ClientName    | RadFromDate | RadToDate | CashMemo |
      | 94192321420301 | MotoG Pvt Ltd |    05042022 |  05042022 | Yes      |

  #@Cargo_FTC_ConfirmFtcButtonValidation
  #Scenario Outline: <TC>: FTC: Validation of Confirm Ftc Validation
    #Given User navigates to FTC
    #When User clicks on recalculate button
    #Then Click on confirm FTC button and validate the confirmation message "<ActionConfirmation>"
#
    #Examples: 
      #| ActionConfirmation                      |
      #| Action Confirmation: Confirm FTC Amount |

  #@Cargo_FTC_AddExtraChargesButtonValidation
  #Scenario Outline: <TC>: FTC: Validation of Add Extra Charges Validation
    #Given User navigates to FTC
    #When User clicks on recalculate button
    #Then User clicks on add extra charges and enters the Add Extra Charges Details "<Shipment Sized Charges>","<Mathadi/Varai Charges>","<Portgages Charges>","<Special Delivery Charges>","<Loading/Unloading Charges>","<ODA Charges>","<Full Truck Load>","<success message>"
#
    #Examples: 
      #| Shipment Sized Charges | Mathadi/Varai Charges | Portgages Charges | Special Delivery Charges | Loading/Unloading Charges | ODA Charges | Full Truck Load | success message |
      #|                     10 |                    20 |                10 |                       10 |                        10 |          10 |              10 | successfull.    |

  @Cargo_FTC_FreightChargesValidation
  Scenario Outline: <TC>: FTC: Freight Charges Validation
    Given User navigates to FTC
    When User clicks on calculate FTC
    Then User clicks on freight charges tab and validate "<Weightsmessage>","<ChargeCalculationmessage>"

    Examples: 
      | Weightsmessage | ChargeCalculationmessage |
      | Weights        | Charge Calculation       |

  @Cargo_FTC_InsuranceChargesValidation
  Scenario Outline: <TC>: FTC: Insurance Charges Validation
    Given User navigates to FTC
    When User clicks on calculate FTC
    Then User clicks on insurance charges tab and validate "<applicableChargeMessage>"

    Examples: 
      | applicableChargeMessage |
      | Applicable charge:      |

  @Cargo_FTC_AccessorialChargesValidation
  Scenario Outline: <TC>: FTC: Accessorial Charges Validation
    Given User navigates to FTC
    When User clicks on calculate FTC
    Then User clicks on accessorial charges tab and validate "<waybillChargesMessage>","<dcChargesMessage>","<ftcChargesMessage>","<fuelSurchargeMessage>","<deliveryAttemptChargesMessage>","<demurrageChargesMessage>","<appointmentVehicleChargesMessage>","<loadingChargesMessage>","<pickupDeliveryChargesMessage>","<handlingChargesMessage>","<groundFloorChargesMessage>","<greenTaxMessage>","<rtoChargesMessage>","<odaChargesMessage>","<holidayDeliveryChargesMessage>"

    Examples: 
      | waybillChargesMessage | dcChargesMessage | ftcChargesMessage     | fuelSurchargeMessage | deliveryAttemptChargesMessage | demurrageChargesMessage | appointmentVehicleChargesMessage        | loadingChargesMessage      | pickupDeliveryChargesMessage  | handlingChargesMessage | groundFloorChargesMessage            | greenTaxMessage | rtoChargesMessage | odaChargesMessage | holidayDeliveryChargesMessage |
      | Waybill Charges:      | DC Charges:      | FTC (To Pay) Charges: | Fuel Surcharge:      | Delivery Attempt Charges:     | Demurrage Charges:      | Appointment/ Different Vehicle Charges: | Loading/Unloading Charges: | Open pickup/delivery charges: | Handling Charges:      | Delivery above ground floor charges: | Green Tax:      | RTO Charges:      | ODA charges:      | Holiday delivery charges:     |

  @Cargo_FTC_DashBoard_UI_Validation
  Scenario: <TC>: FTC: - Entry List Validation
    Given User navigates to FTC
    When validate FTC entry UI "FTC RAD Shipments"
    Then validate the table entry details "Sr.No","AWB","MPS Co","Client Name","Consignee Name","Consignee Address","RAD Date","Cash Memo Generate","Payment Status","Actions"
