@Cargo_Annexure
Feature: Annexure

  @CargoLoginAnnexure
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                         | Password   |
      | admin.atul.deokar@xpressbees.com | Xpress@123 |

  @Annexurecreationwithpngfile
  Scenario Outline: <TC>: Happy flow for Annexure creation
    Given User Navigates to Annexure option
    When Click on add new Annexure button
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User provides Annexure name "<Annexurename>"
    Then User uploads Annexure file "<Annexurefilename>"
    Then User validates successful msg1 and clicks on Ok "<Successfulmsg>"
    Then User clicks on save button1 validates successmsg and clicks on Ok "<Successmsg>"

    Examples: 
      | Companyname | Contractid     | Annexurename | Annexurefilename | Successfulmsg                 | Successmsg                   |
      | test14June  | CR-1467-2021-1 | Annexure2    | Annexure.png     | Document Upload successfully. | Record created successfully. |

  @Annexurecreationwithjpegfile
  Scenario Outline: <TC>: Happy flow for Annexure creation
    Given User Navigates to Annexure option
    When Click on add new Annexure button
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User provides Annexure name "<Annexurename>"
    Then User uploads jpeg Annexure file "<Annexurefilename>"
    Then User validates successful msg1 and clicks on Ok "<Successfulmsg>"
    Then User clicks on save button1 validates successmsg and clicks on Ok "<Successmsg>"

    Examples: 
      | Companyname | Contractid     | Annexurename | Annexurefilename | Successfulmsg                 | Successmsg                   |
      | test14June  | CR-1467-2021-1 | Annexure3    | Annexure1.jpeg   | Document Upload successfully. | Record created successfully. |

  @Annexurecreationwithpdffile
  Scenario Outline: <TC>: Happy flow for Annexure creation
    Given User Navigates to Annexure option
    When Click on add new Annexure button
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User provides Annexure name "<Annexurename>"
    Then User uploads pdf Annexure file "<Annexurefilename>"
    Then User validates successful msg1 and clicks on Ok "<Successfulmsg>"
    Then User clicks on save button1 validates successmsg and clicks on Ok "<Successmsg>"

    Examples: 
      | Companyname | Contractid     | Annexurename | Annexurefilename | Successfulmsg                 | Successmsg                   |
      | test14June  | CR-1467-2021-1 | Annexure4    | Annexure2.pdf    | Document Upload successfully. | Record created successfully. |

  @Annexurecreationwithjpgfile
  Scenario Outline: <TC>: Happy flow for Annexure creation
    Given User Navigates to Annexure option
    When Click on add new Annexure button
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User provides Annexure name "<Annexurename>"
    Then User uploads jpg Annexure file "<Annexurefilename>"
    Then User validates successful msg1 and clicks on Ok "<Successfulmsg>"
    Then User clicks on save button1 validates successmsg and clicks on Ok "<Successmsg>"

    Examples: 
      | Companyname | Contractid     | Annexurename | Annexurefilename | Successfulmsg                 | Successmsg                   |
      | test14June  | CR-1467-2021-1 | Annexure5    | Annexure3.jpg    | Document Upload successfully. | Record created successfully. |

  @Annexurecreationwithgreaterthan5MB
  Scenario Outline: <TC>: Happy flow for Annexure creation
    Given User Navigates to Annexure option
    When Click on add new Annexure button
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User provides Annexure name "<Annexurename>"
    Then User uploads Annexure file of exceeded limit "<Annexurefilename>"
    Then User validates error msg1 and clicks on Ok "<Errormsg>"

    Examples: 
      | Companyname | Contractid     | Annexurename | Annexurefilename           | Errormsg                              |
      | test14June  | CR-1467-2021-1 | Annexure6    | Annexuregreaterthan5mb.png | Please upload document less than 5MB. |

  @Annexurefiltersearchanddownloadfile
  Scenario Outline: <TC>: Filter search and download file
    Given User Navigates to Annexure option
    When User clicks on filter option1
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User clicks on Search button1
    Then User clicks on download button

    Examples: 
      | Companyname | Contractid     |
      | test14June  | CR-1467-2021-1 |

  @Annexurefiltersearchanddeletefile
  Scenario Outline: <TC>: Filter search and delete file
    Given User Navigates to Annexure option
    When User clicks on filter option1
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User clicks on Search button1
    Then User clicks on delete button
    Then User validates confirmation msg and clicks on Ok "<Confirmationmsg>"

    Examples: 
      | Companyname | Contractid     | Confirmationmsg                  |
      | test14June  | CR-1467-2021-1 | Are you sure you want to delete? |

  @Annexurecreationwithnonallowedfileformat
  Scenario Outline: <TC>: Negative flow for Annexure creation
    Given User Navigates to Annexure option
    When Click on add new Annexure button
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User provides Annexure name "<Annexurename>"
    Then User uploads non allowed file format "<Annexurefilename>"
    Then User validates error msg2 and clicks on Ok "<Errormsg2>"

    Examples: 
      | Companyname | Contractid     | Annexurename | Annexurefilename | Errormsg2                              |
      | test14June  | CR-1467-2021-1 | Annexure7    | Annexure4.docs   | Please upload only allowed file format |

  @Checkmandatoryfields
  Scenario Outline: <TC>: Check mandatory fields
    Given User Navigates to Annexure option
    When Click on add new Annexure button
    Then User clicks on Save button
    Then User validates error msgs "<Companynameerrormsg>", "<Contractiderrormsg>", "<Annexurenameerrormsg>", "<Fileuploaderrormsg>"

    Examples: 
      | Companynameerrormsg         | Contractiderrormsg         | Annexurenameerrormsg        | Fileuploaderrormsg  |
      | Please select company name. | Please select contract id. | Please enter annexure name. | Please upload file. |

  @AnnexurefilterReset
  Scenario Outline: <TC>: Filter Reeset
    Given User Navigates to Annexure option
    When User clicks on filter option1
    Then User selects company name "<Companyname>"
    Then User selects contract id "<Contractid>"
    Then User clicks on Reset button

    Examples: 
      | Companyname | Contractid     |
      | test14June  | CR-1467-2021-1 |
