@CargoUADShipmentSearch_Feature
Feature: CargoShipmentSearch

  #144 :Please enter future date
  #160:Please enter future date
  #201:Please enter todays date
  @UADShipmentSearchLogin
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |
  @UADShipmentSearch_menu_Navigation
  Scenario Outline: <TC>: UAD Shipment menu Visibiity under UAD menu
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    Then UAD Shipment Search menu should displayed

  @UADShipmentSearch_PageTable
  Scenario Outline: <TC>: UAD Shipment search table listing
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    Then Sipment search listing should display with columns "<ShippingID>","<ConsigneePhone>","<ConsigneeEmail>","<DeliveryPincode>","<DeliveryAddress>","<DeliveryDate>","<RADInscan>","<RescheduleDate>","<UADMarkedStatus>","<UADMarkedCount>","<Action>"

    Examples: 
      | ShippingID  | ConsigneePhone  | ConsigneeEmail   | DeliveryPincode  | DeliveryAddress  | DeliveryDate  | RADInscan  | RescheduleDate   | UADMarkedStatus   | UADMarkedCount   | Action |
      | Shipping ID | Consignee phone | Consignee e-mail | Delivery Pincode | Delivery Address | Delivery Date | RAD Inscan | Re-Schedule Date | UAD Marked Status | UAD Marked Count | Action |

  @UADShipmentSearch_DeliveredAWB
  Scenario Outline: <TC>: Search for delivered AWB on shipment search page
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When click on Search filter
    When Entered "<DeliveredAWB>" and clicked Search button
    Then It should show text "<TotalRecords>"

    Examples: 
      | DeliveredAWB   | TotalRecords     |
      | 94192321420090 | Total Records: 0 |

  @UADShipmentSearch_MarkUADButtonVisibility
  Scenario Outline: <TC>: UAD Shipment Mark UAD button visibility
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    Then Check Mark UAD button is displayed

  @UADShipmentSearch_MarkUADPopup_Elements
  Scenario Outline: <TC>: Mard UAD popup elements
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When clicked on Mark UAD button
    Then It should show Mark UAD popup with elements "<Reason>","<Subreason>","<RescheduleDate>" and Submit Cancel button

    Examples: 
      | Reason | Subreason  | RescheduleDate                  |
      | Reason | Sub Reason | Re-schedule Date(If applicable) |

  @UADShipmentSearch_Rason_fieldValidation
  Scenario Outline: <TC>: Mard UAD popup Reason field validation
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When clicked on Mark UAD button
    When click on submit button
    Then It should show error message "<ReasonValidationMsg>"

    Examples: 
      | ReasonValidationMsg  |
      | Please select reason |

  @UADShipmentSearch_CancelBtn_Functionality
  Scenario Outline: <TC>: Mard UAD popup Cancel button functionality
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When clicked on Mark UAD button
    When click on cancel button
    Then It should close popup and should show Search filter

  @UADShipmentSearch_RescheduleDateValidation
  Scenario Outline: <TC>: Mard UAD Reschedule date validations
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When clicked on Mark UAD button
    When Selected Reason "<UADReason>"
    When click on submit button
    Then It should validation message on blank reschedule date "<RescheduleDateerrMsg>"

    Examples: 
      | UADReason                                    | RescheduleDateerrMsg                                     |
      | Consignee wants future delivery (Re-attempt) | Re-Schedule date is mandatory for this Reason/Sub-reason |

  @UADShipmentSearch_ValidationOnOperationalReasonSelection
  Scenario Outline: <TC>: Mard UAD validation on operation reason selection
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When clicked on Mark UAD button
    When Selected operational Reason "<OperationReason>"
    When click on submit button
    Then It should validation message on blank sub reason "<Subreason>" reschedule date "<RescheduleDateerrorMsg>"

    Examples: 
      | OperationReason  | Subreason                | RescheduleDateerrorMsg                                   |
      | Operational DEPS | Please select Sub-Reason | Re-Schedule date is mandatory for this Reason/Sub-reason |

  @UADShipmentSearch_UADReasons
  Scenario Outline: <TC>: Check UAD reasons displayed
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When clicked on Mark UAD button
    When clicked on Reason drop down
    Then It should show list of reasons "<UADReasons>"

    Examples: 
      | UADReasons                                    |
      | Consignee premises closed                     |
      | Consignee wants delivery at different address |
      | Consignee wants future delivery (Re-attempt)  |
      | Operational DEPS                              |

  @UADShipmentSearch_HappyFlow_WithConsigneeReason
  Scenario Outline: <TC>: Mard UAD with consignee Reason
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When clicked on Mark UAD button
    When Selected Consignee Reason "<UADReason1>"
    When Selected Reschduledate "<ReschdeuleDate>"
    When click on submit button
    Then It should show sucess message of UAD marked "<UADMarkedSucessMsg>"

    Examples: 
      | UADReason1                                   | ReschdeuleDate | UADMarkedSucessMsg      |
      | Consignee wants future delivery (Re-attempt) | 30/05/2022     | UAD Marked Successfully |

  @UADShipmentSearch_HappyFlow_WithOperationReason
  Scenario Outline: <TC>: Mard UAD with Operation Reason
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When clicked on Mark UAD button
    When Selected operation Reason "<OpsReason>" and "<Subreason>"
    When Select Reschduledate "<ReschdeuleDate1>"
    When click on submit button
    Then It should show  message of UAD marked "<UADMarkedSucessMsg1>"

    Examples: 
      | OpsReason        | Subreason      | ReschdeuleDate1 | UADMarkedSucessMsg1     |
      | Operational DEPS | Material short | 30/05/2022      | UAD Marked Successfully |

  @UADShipmentSearch_OperationalReasonDisabled
  Scenario Outline: <TC>: Mard UAD when operational reason UAD is reached its max limit
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When clicked on Mark UAD button
    Then It should not allow to select reason "<OpsReason1>"

    Examples: 
      | OpsReason1       |
      | Operational DEPS |

  @UADShipmentSearch_UADFlowFor_RapidBookingPickdoneAWB
  Scenario Outline: <TC>: To check mark UAD for Rapid Pick done AWB
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When click on Search filter
    When Enter Rapid booking AWB "<RapidAWB>" and click Search button
    Then It should show message on mouse hover on "<RapidBookingMsg>"

    Examples: 
      | RapidAWB       | RapidBookingMsg                  |
      | 9422072100879 | Please perform complete booking. |

  @UADShipmentSearch_MArkUADONCurrentDate
  Scenario Outline: <TC>: Mard UAD for AWB whoes RAD isncan time is
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When clicked on Mark UAD button
    When Selected operation Reason "<OpsReason1>" and "<Subreason1>"
    When Select Reschduledate "<ReschdeuleDate2>"
    When click on submit button
    Then It should show  message of date "<UADMarkedSucessMsg2>"

    Examples: 
      | OpsReason1       | Subreason1     | ReschdeuleDate2 | UADMarkedSucessMsg2                                  |
      | Operational DEPS | Material short | 04/05/2022      | Re-Schedule date should be greater than today's date |

  @UADShipmentSearch_Morethan_MAxRescheduledateSelection
  Scenario Outline: <TC>: Mard UAD with with more than max reschedule date
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When clicked on Mark UAD button
    When Select Consignee Reason "<UADReason4>"
    When Select more than max Reschduledate "<ReschdeuleDate4>"
    When click on submit button
    Then It should show message of UAD marked "<MaxRescheduledatereachedMsg>"

    Examples: 
      | UADReason4                                   | ReschdeuleDate4 | MaxRescheduledatereachedMsg    |
      | Consignee wants future delivery (Re-attempt) | 30/05/2022      | Re-Schedule date limit reached |

  @UADShipmentSearch_UADFlowFor_cutoffPassed
  Scenario Outline: <TC>: To check mark UAD when cut off is passed and for AWB whoes consignee and operaiton UAD mark attemps are done
    Given Navigate on FLM Menu
    When Navigate on UAD menu
    When Navigate on UAD shipment search menu
    When click on Search filter
    When Enter AWB "<AWB>" and click Search button
    Then It shows message on mouse hover on "<AWBMSG>"

    Examples: 
      | AWB            | AWBMSG                                                                      |
      | 94192321420478 | Unable to mark UAD for this shipment. UAD cut-off passed or not in premises |
    #| 948888888888 | Operations and Consignee UAD marking limit reached. |
#
#
#
  #@UADShipmentSearch_UADFlowFor_UndeliveredAWB
  #Scenario Outline: <TC>: To check mark UAD Undelivered AWB
    #Given Navigate on FLM Menu
    #When Navigate on UAD menu
    #When Navigate on UAD shipment search menu
     #When click on Search filter
    #When Enter AWB "<UndeliveredAWB>" and click Search button
    #Then It should show Mark uad button enabled
 #
 #Examples:   
    #| UndeliveredAWB |
    #| 94192321420348 |
    #
    #
 #@UADShipmentSearch_UADFlowFor_WhenUADMarkedStatuscolumnisNO_UADMArkedCountShouldAlways0
  #Scenario Outline: <TC>: To check UAD marked status and Uad marked count columns of listing
    #Given Navigate on FLM Menu
    #When Navigate on UAD menu
    #When Navigate on UAD shipment search menu
     #When click on Search filter
    #When Enter AWB "<UndeliveredAWB>" and click Search button
    #When UADMArked status is "<Status>"
    #Then It should show count as "<Count>"
 #
 #Examples:   
    #| UndeliveredAWB | Status | Count |
    #| 94192321420348 | No | 0 |
    #
    #
#
#
