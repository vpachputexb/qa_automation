@CargoBagShipmentQuery_Feature
Feature: CargoBagShipmentQuery

 @CargoLoginBagShipmentQuery
   Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |
      
      
  @CargoBagShipmentQuery
  Scenario Outline: <TC>: Bagshipment Query -Happy Flow
    Given User selects report menu and clicks on bag shipment Query
    When User enteres AWB "<AWBNo>" and click on show Shipment Log button
    Then Validate "<successMessage>" and validate AWB tracking sections displayed

    Examples: 
      | AWBNo            | successMessage |
      |   93672727192628 | Success.       |
      | MPS372922210132 | Success.       |

  @CargoBagShipmentQuery_Negative_scenario_AWBNO
  Scenario Outline: <TC>:AWB validations
    Given User selects report menu and clicks on bag shipment Query
    When User enteres AWB "<AWBNo>" and click on show Shipment Log button
    Then Verify error displayed "<erroronblankawb>","<AWBNo>"

    Examples: 
      | AWBNo          | erroronblankawb         |
      |                | Enter Shipping ID       |
      | 88667888888888 | UFA:Shipments not Found |
      | jfjjfjf55555   | UFA:Shipments not Found |

  @CargoBagShipmentQuery_AWBTrackingSection
  Scenario Outline: AWB Tracking table
    Given User selects report menu and clicks on bag shipment Query
    When User enteres AWB "<AWBNo>" and click on show Shipment Log button
    Then Verify AWB Tracking table columns "<ParentAWB>","<MPSCount>","<ClientName>" displayed
    Then Verify MPS Tracking table columns "<ShippingID>" displayed
    When User clicks on "<ShippingIDNumber>"
    Then Transaction log page should get opened

    Examples: 
      | AWBNo          | ParentAWB  | MPSCount  | ClientName  | ShippingID  | ShippingIDNumber |
      | 93672727192628 | Parent AWB | MPS Count | Client Name | Shipping Id | MPS372922210132 |

  @CargoBagShipmentQuery_AWBTrackingSection_DataVerification
  Scenario Outline: AWB Tracking data verification
    Given User selects report menu and clicks on bag shipment Query
    When User enteres AWB "<AWBNo>" and click on show Shipment Log button
    Then Verify data displyed for AWB "<ClientName>","<ConsingeeNameAdd>"
		
		Examples: 
      | AWBNo          | ClientName  | ConsingeeNameAdd  |
      | 93672727192628 | MotoG Pvt Ltd (2041) | ABC Test (Address Test) |

		
		
  @CargoBagShipmentQuery_TransactionLog_DataVerification
  Scenario Outline: BTransaction Log data verification
    Given User selects report menu and clicks on bag shipment Query
    When User enteres AWB "<AwbNo>" and click on show Shipment Log button
    When User clicks on "<ShippingIDNumber>"
    Then Verify TransactionLog data "<Process>"

    Examples: 
      | AwbNo          | ShippingIDNumber | Process |
      | 93672727192628 | MPS372922210132 | Shipment InScan from Manifest |


  @CargoBagShipmentQuery_BagNo_HappyFlow
  Scenario Outline: BagNo happy flow
    Given User selects report menu and clicks on bag shipment Query
    When User enters "<BagNo>" and clicked on show bag log button
    Then It should display result "<SealNo>","<OriginHubname>"

    Examples: 
      | BagNo        | SealNo  | OriginHubname  |
      | Nxb-11test80 | Seal No | Origin Hubname |

  @CargoBagShipmentQuery_BagNo_VerifyData
  Scenario Outline: BagNo data verification
    Given User selects report menu and clicks on bag shipment Query
    When User enters "<BagNo>" and clicked on show bag log button
    Then It should data as "<SealNo>","<OriginHubname>","<CurrentHubname>","<DestinationHubname>"

    Examples: 
      | BagNo        | SealNo       | OriginHubname | CurrentHubname | DestinationHubname |
      | Nxb-11test80 | nxb-11test80 | PNQ/BAN       | PNQ/HUB        | PNQ/HUB            |

  @CargoBagShipmentQuery_BagNo
  Scenario Outline: BagNo Validation
    Given User selects report menu and clicks on bag shipment Query
    When User enters "<BagNo>" and clicked on show bag log button
    Then It should display valid result "<BagNo>","<Errorbagno>"

    Examples: 
      | BagNo | Errorbagno      |
      |       | Enter Bag No    |
      | jfhfj | No record found |

  @CargoBagShipmentQuery_ResetButton
  Scenario Outline: Reset button functionality
    Given User selects report menu and clicks on bag shipment Query
    When User enters "<AwbNo>" and clicked on reset button

    Examples: 
      | AwbNo          |
      | 94185221110403 |


