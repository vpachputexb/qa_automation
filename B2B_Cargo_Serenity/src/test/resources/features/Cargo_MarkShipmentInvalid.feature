@MarkShipmentInvalidFeature
Feature: Mark Shipment Invalid feature

  @MarkShipmentInvalidLogin
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

  
  @MarkShipmentInvalid_menuvisibility
  Scenario Outline: <TC>: Mark Shipment Invalid menu visibility
  Given Navigate  Support page
  Then Mark shipment invalid menu should be displayed
  
  @MarkShipmentInvalid_Navigation
  Scenario Outline: <TC>: Mark Shipment Invalid navigation
  Given Navigate  Support page
  When Clicked on Mark Shipment Invalid link
  Then User  navigate on "<MarkShipmentInvalid>" page
  
  Examples:
  | MarkShipmentInvalid   |
  | Mark Shipment Invalid |
  
  @MarkShipmentInvalid_CheckElements
  Scenario: <TC>: Mark Shipment Invalid page elements
  Given Navigate  Support page
  When Clicked on Mark Shipment Invalid link
  Then User check elements present on page
  
  @MarkShipmentInvalid_Validation
  Scenario Outline: <TC>: Mark Shipment Invalid Shipemnt id validations
  Given Navigate  Support page
  When Clicked on Mark Shipment Invalid link
  When Entered "<ShipmentId>"
  When Clicked update button
  Then It should show validation message "<ShipmentIdMsg>","<commentsmsg>"
  
  Examples:
  | ShipmentId     | ShipmentIdMsg               | commentsmsg                    |
  |                | Please enter Shipment ID(s) | The comments field is required |
  | 94192321420142 |                             | The comments field is required |
  
  @MarkShipmentInvalid_HappyFlow
  Scenario Outline: <TC>: Mark Shipment Invalid Happy Flow
  Given Navigate  Support page
  When Clicked on Mark Shipment Invalid link
  When Entered "<ShipmentId>"
  When Enter comment "<Comment>"
  When Clicked update button
  Then It should show sucess popup with heading "<Popupheading>" and message "<SuccessMsg>"
  
  Examples:
  | ShipmentId     | Comment      | Popupheading          | SuccessMsg                  |
  | 94192321430065 | Test comment | Mark shipment invalid | Record Updated Successfully |
  
  @MarkShipmentInvalid_InvalidAWB
  Scenario Outline: <TC>: Mark Shipment Invalid Invalid AWB number
  Given Navigate  Support page
  When Clicked on Mark Shipment Invalid link
  When Entered "<ShipmentId>"
  When Enter comment "<Comment>"
  When Clicked update button
  Then It should show "<InvalidAWBMessage>"
  
  Examples:
  | ShipmentId | Comment      | InvalidAWBMessage                |
  |          9 | Test comment | Please follow the correct format |
  
  @MarkShipmentInvalid_CheckMaximumTenAWBAllowed
  Scenario Outline: <TC>: Mark Shipment Invalid Max 10 AWB allowed
  Given Navigate  Support page
  When Clicked on Mark Shipment Invalid link
  When Entered "<ShipmentId>" more than Ten
  When Enter comment "<Comment>"
  When Clicked update button
  Then It should show max allowed awb "<MaxTenAWBMsg>"
  
  Examples:
  | ShipmentId                                                                                                                                                                          | Comment      | MaxTenAWBMsg            |
  | 94192321420140,94192321420140,94192321420140,94192321420140,94192321420140,94192321420140,94192321420140,94192321420140,94192321420140,94192321420140,94192321420140,94192321420140 | Test comment | Maximum 10 AWBs allowed |
