@Cargo_Dashboard
Feature: Dashboard

  @CargoLoginDashboard
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page
    
     Examples: 
      | Username                       | Password       |
      | admin.atul.deokar@xpressbees.com | Xpress@123 |
      
      
       @Dashboarddatacompare
  Scenario Outline: <TC>: Dashboard data compare
    
    Given User reads Total client value
    When User navigates to client listing
    Then User reads Total records value
    Then User compares Total client value and Total records value 
  
    @Dashboardclickonactive
    Scenario Outline: <TC>: User clicks on active chart and compare active client values
    Given User Navigates to Client dashboard option
    When User clicks on active chart
    Then User compares Total active client value and Total active records value 
    
     @Dashboardclickoninactive
    Scenario Outline: <TC>: User clicks on inactive chart and compare inactive client values
    Given User Navigates to Client dashboard option
    When User clicks on inactive chart
    Then User compares Total inactive client value and Total inactive records value
    
    
     @Dashboardclickonactivebarchart
    Scenario Outline: <TC>: User clicks on active bar chart
    Given User Navigates to Client dashboard option
    When User clicks on active bar chart
    
     @Dashboardclickoninactivebarchart
    Scenario Outline: <TC>: User clicks on inactive bar chart
    Given User Navigates to Client dashboard option
    When User clicks on inactive bar chart
    
    