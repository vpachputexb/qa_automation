@Cargo_BookingFeature
Feature: Cargo Booking feature

  @CargoLoginBooking
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

  @Cargo_SoftBooking_MotoGClient
  Scenario Outline: <TC>: Cargo Booking - Happy flow for Cargo Booking for MotoGClient
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    When User enters additional box details "<Length2>","<Breadth2>","<Height2>","<NoOfBoxes2>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then Verify the details saved with AWB Number and other details info "<clientName>","<Route Mode>","<Booking Mode>","<pickupPincode>","<dropPincode>","<LocationName>","<Mobile>","<Email>","<Address>","<GSTNo>","<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>","<Length>","<Breadth>","<Height>","<NoOfBoxes>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Length2 | Breadth2 | Height2 | NoOfBoxes2 | Status        |
      | MotoG      | Surface    | Credit       |        301001 |      534006 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 18AABCU9603R1ZM |   0201254 |                1 |                     100.00 |         4 |                       10.00 |     50 |      50 |     50 |         1 |      50 |       50 |      50 |          3 | AWB Generated |

  @Cargo_SoftBooking_lenovoClient
  Scenario Outline: <TC>: Cargo Booking - Happy flow for Cargo Booking for lenovo client
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then Verify the details saved with AWB Number and other details info "<clientName>","<Route Mode>","<Booking Mode>","<pickupPincode>","<dropPincode>","<LocationName>","<Mobile>","<Email>","<Address>","<GSTNo>","<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>","<Length>","<Breadth>","<Height>","<NoOfBoxes>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email        | Address            | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com     | local Address      | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated |
      | lenovo     | Surface    | FTC          |        110505 |      132117 | Serviceable   | RetailShop   | 6987654123 | ss@gmail.com | Behind Post office | 07AABCU9603R1ZP |           |              200 |                     150.00 |         2 |                       30.00 |    100 |     120 |    105 |         2 | AWB Generated |

  @Cargo_SoftBooking_FLIPKART_INTERNET_PRIVATE_LIMITEDClient
  Scenario Outline: <TC>: Cargo Booking - Happy flow for Cargo Booking for FLIPKART INTERNET PRIVATE LIMITED
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then Verify the details saved with AWB Number and other details info "<clientName>","<Route Mode>","<Booking Mode>","<pickupPincode>","<dropPincode>","<LocationName>","<Mobile>","<Email>","<Address>","<GSTNo>","<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>","<Length>","<Breadth>","<Height>","<NoOfBoxes>"

    Examples: 
      | clientName                        | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email        | Address            | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        |
      | FLIPKART INTERNET PRIVATE LIMITED | Surface    | Credit       |        380003 |      400055 | Serviceable   | Local        | 9876543210 | ss@g.com     | local Address      | 30AABCU9603R1Z0 |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated |
      | FLIPKART INTERNET PRIVATE LIMITED | Surface    | FTC          |        249407 |      635601 | Serviceable   | RetailShop   | 6987654123 | ss@gmail.com | Behind Post office | 24AABCU9603R1ZT |           |              200 |                     150.00 |         2 |                       30.00 |    100 |     120 |    105 |         2 | AWB Generated |

  @Cargo_SoftBooking_Create_Booking_Refresh_Button
  Scenario Outline: <TC>: Cargo Booking - Create Booking Refresh
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When User Enters client name "<clientName>"
    When User Enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>"
    Then User Clicks on refresh button and then validate error messages "<errorMsgClientName>","<MessagePickupPincode>","<MessageDestinationPincode>"

    Examples: 
      | clientName                        | Route Mode | Booking Mode | pickupPincode | dropPincode | errorMsgClientName           | MessagePickupPincode                 | MessageDestinationPincode                 |
      | FLIPKART INTERNET PRIVATE LIMITED | Surface    | Credit       |        380003 |      400055 | The client field is required | The PickUp PinCode field is required | The Destination PinCode field is required |

  @Cargo_Booking_Create_Booking_Serviceability_Negative_Scenarios_Blank_Pincodes
  Scenario Outline: <TC>: Cargo Booking- Create Booking -Basic Info- Serviceability-Blank Pincodes
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When User Enters client name "<clientName>"
    Then User clicks on Check Serviceabilty button and validate "<errorMessage>"

    Examples: 
      | clientName | errorMessage                               |
      | MotoG      | Please enter Pickup & Destination Pincodes |

  @Cargo_Booking_Create_Booking_Serviceability_Invalid_Pincodes_and_serviceability_check
  Scenario Outline: <TC>: Cargo Booking- Create Booking -Basic Info- Serviceability- Invalid pincodes
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    Then User clicks on Check Serviceabilty button and validate pincode error message "<MessagePickupPincode>" and "<MessageDestinationPincode>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | MessagePickupPincode                 | MessageDestinationPincode                 |
      | MotoG      | Air        | Credit       |           111 |         222 | The PickUp PinCode field is required | The Destination PinCode field is required |
      | MotoG      | Air        | Credit       |        111222 |      222111 | Non Serviceable                      | Non Serviceable                           |

  @Cargo_Booking_Create_Booking_Serviceability_Origin_Hub_Destination_hub_validation
  Scenario Outline: <TC>: Cargo Booking- Create Booking -Basic Info- Serviceability- Origin Hub Destination Hub Validation
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    Then validate_origin_hub "<origin_hub>" and destination_hub "<destination_hub>" values

    Examples: 
      | clientName                        | Route Mode | Booking Mode | pickupPincode | dropPincode | origin_hub | destination_hub | Servicability |
      | FLIPKART INTERNET PRIVATE LIMITED | Surface    | Credit       |        380003 |      400055 | AMD/PC1    | BOM/BND         | Serviceable   |
      | FLIPKART INTERNET PRIVATE LIMITED | Surface    | FTC          |        249407 |      635601 | HARIDWAR   | AMBUR           | Serviceable   |
      | lenovo                            | Surface    | Credit       |        411051 |      411051 | RSC/Dummy    | PNQ/SGR        | Serviceable   |

  @Cargo_SoftBooking_MotoGClient_Contract_Functional_Error
  Scenario Outline: <TC>: Cargo Booking - Happy flow for Cargo Booking for MotoGClient functional Errors
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Functional error -Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>"
    Then Validate the error message displayed functional Errors "<message>"

    Examples: 
      | clientName         | Route Mode | Booking Mode | pickupPincode | dropPincode | message                                 |
      | Taralas Enterprise | Air        | Credit       |        411051 |      411052 | As per contract, invalid route mode air |

#	 @Cargo_SoftBooking_lenovoClient_ODA_Pincodes
  #Scenario Outline: <TC>: Cargo Booking - Happy flow for Cargo Booking for lenovo client
    #Given Navigate to Cargo Booking option
    #When User navigates to Create booking option
    #When Enter client name "<clientName>" and enters serviceablity information for ODA validation "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>","<Message>"
    #When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    #When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    #Then Verify the AWB genarated and verify the status "<Status>"
    #Then Verify the details saved with AWB Number and other details info "<clientName>","<Route Mode>","<Booking Mode>","<pickupPincode>","<dropPincode>","<LocationName>","<Mobile>","<Email>","<Address>","<GSTNo>","<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>","<Length>","<Breadth>","<Height>","<NoOfBoxes>"
#
    #Examples: 
      #| clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email        | Address            | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | Message |
      #| lenovo     | Surface    | Credit       |        411000 |      411054 | Serviceable   | Local        | 9876543210 | ss@g.com     | local Address      | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | Consignee pincode is ODA, please click on OK if you still want to book. |
      #

  @Cargo_Booking_Create_Booking_Serviceability_Reset_Button
  Scenario Outline: <TC>: Cargo Booking- Create Booking -Basic Info- Serviceability- Origin Hub Destination Hub Validation
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    Then validate error messages displayed after clicking on reset button "<MessagePickupPincode>","<MessageDestinationPincode>","<MessageOriginHub>","<MessageDestiantionHub>"

    Examples: 
      | clientName                        | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | MessagePickupPincode                 | MessageDestinationPincode                 | MessageOriginHub                 | MessageDestiantionHub                 |
      | FLIPKART INTERNET PRIVATE LIMITED | Surface    | Credit       |        380003 |      400055 | Serviceable   | The PickUp PinCode field is required | The Destination PinCode field is required | The Origin Hub field is required | The Destination Hub field is required |

  @Cargo_Booking_Create_Booking_PickUpLocations_Validation
  Scenario Outline: <TC>: Cargo Booking- Create Booking -Basic Info- PickUpLocation Validation
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    Then Validate pickup location details "<pickupPincode>","<city>","<State>"

    Examples: 
      | clientName                        | Route Mode | Booking Mode | pickupPincode | dropPincode | city      | State       | Servicability |
      | MotoG                             | Surface    | Credit       |        411051 |      411052 | PUNE      | MAHARASHTRA | Serviceable   |
      | MotoG                             | Surface    | Credit       |        413512 |      411051 | Latur     | MAHARASHTRA | Serviceable   |
      | FLIPKART INTERNET PRIVATE LIMITED | Surface    | Credit       |        380003 |      400055 | Ahmedabad | GUJARAT     | Serviceable   |
      | FLIPKART INTERNET PRIVATE LIMITED | Surface    | FTC          |        249407 |      635601 | Haridwar  | UTTARAKHAND | Serviceable   |

  @Cargo_Booking_Create_Booking_PickUpLocations_Validation_error_messages_blank
  Scenario Outline: <TC>: Print AWB - Cargo Booking- Create Booking -Basic Info- PickUpLocation -Blank inputs
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    Then Validate blank error messages "<ErrorLocationMessage>","<ErrorMobileMessage>","<ErrorEmailMessage>","<ErrorAddressMessage>","<ErrorGSTNumberMessage>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile | Email | Address | GSTNo | Telephone | ErrorLocationMessage                | ErrorMobileMessage                 | ErrorEmailMessage           | ErrorAddressMessage           | ErrorGSTNumberMessage       |
      | MotoG      | Surface    | Credit       |        411041 |      411052 | Serviceable   |              |        |       |         |       |           | The Location Name field is required | The Mobile field format is invalid | The Email field is required | The Address field is required | The GstNo field is required |

  @Cargo_Booking_Create_Booking_PickUpLocations_Validation_error_messages_format
  Scenario Outline: <TC>: Cargo Booking- Create Booking -Basic Info- PickUpLocation Invalid Format
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details format validation "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    Then Validate format error messages "<ErrorMobileFormat>","<ErrorEmailFormat>","<ErrorGSTFormat>","<ErrorTelephoneFormat>"

    Examples: 
      | clientName                        | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email | Address | GSTNo           | Telephone | ErrorMobileFormat                  | ErrorEmailFormat                      | ErrorGSTFormat                    | ErrorTelephoneFormat                              |
      | FLIPKART INTERNET PRIVATE LIMITED | Surface    | Credit       |        380003 |      400055 | Serviceable   | local        | 4876543210 | ss    | aaa     | asrt            |       333 | The Mobile field format is invalid | The Email field must be a valid email | The GstNo field format is invalid | The Telephone field must be at least 6 characters |
      | FLIPKART INTERNET PRIVATE LIMITED | Surface    | Credit       |        380003 |      400055 | Serviceable   | local        | 9876543210 | ss    | aaa     | 90XXXXX0000X1ZZ |           |                                    | The Email field must be a valid email | The GstNo field format is invalid |                                                   |

  @Cargo_Booking_Create_Booking_PickUpLocations_AddNewLocation_field_Validation
  Scenario Outline: <TC>: Cargo Booking- Create Booking -Basic Info- PickUpLocation Invalid Format
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    Then User navigates back to Pickup Location tab and validate details saved "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>" and click on AddNewLocation button
    Then Validate blank error messages "<ErrorLocationMessage>","<ErrorMobileMessage>","<ErrorEmailMessage>","<ErrorAddressMessage>","<ErrorGSTNumberMessage>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | ErrorLocationMessage                | ErrorMobileMessage                 | ErrorEmailMessage           | ErrorAddressMessage           | ErrorGSTNumberMessage       |
      | lenovo     | Surface    | Credit       |        411052 |      411051 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 00XXXXX0000X1ZZ |       223 | The Location Name field is required | The Mobile field format is invalid | The Email field is required | The Address field is required | The GstNo field is required |

  @Cargo_Booking_Create_Booking_ShipmentDetails_Blank
  Scenario Outline: <TC>: Cargo Booking- Create Booking -Basic Info -Shipment Details- Blank
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User clicks on save button in shipment details tab
    Then Validate error messages for blank inputs "<errorblankNumberofInvoices>","<errorblankTotalDeclaredValue>","<errorblankNumberMPS>","<errorblankTotalDeclredWeight>","<errorblanklength>","<errorblankBreadth>","<errorblankHeight>","<errorblankNoBoxes>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | errorblankNumberofInvoices              | errorblankTotalDeclaredValue                        | errorblankNumberMPS                 | errorblankTotalDeclredWeight                         | errorblanklength    | errorblankBreadth    | errorblankHeight    | errorblankNoBoxes         |
      | lenovo     | Surface    | Credit       |        411052 |      411051 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 00XXXXX0000X1ZZ |           | The Number Of Invoice field is required | The Total Shipment Declared Value field is required | The Number Of MPS field is required | The Total Shipment Declared Weight field is required | Please enter length | Please enter breadth | Please enter height | Please enter no. of boxes |

  @Cargo_SoftBooking_CreateBooking_ShipmentDetails_Total_Shipment_Value_Null_Error
  Scenario Outline: <TC>: Cargo Booking - Create Booking -Basic Info -Shipment Details - Total Shipment Value Null Error
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Validate error message total shipment value error "<messageValue>", total shipment weight "<messageWeight>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | messageValue                                              | messageWeight                                             |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                          0 |         1 |                           0 |     50 |      50 |     50 |         1 | The Total Shipment Declared Value field must be 1 or more | The Total Shipment Declared Value field must be 1 or more |

  @Cargo_Booking_Create_Booking_ShipmentDetails_Date
  Scenario Outline: <TC>: Cargo Booking- Create Booking -Basic Info- Shipment Details- Date
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    Then Validate Todays date is auto populated in shipment details tab

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone |
      | lenovo     | Surface    | Credit       |        411052 |      411051 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 00XXXXX0000X1ZZ |           |

  @Cargo_Booking_Create_Booking_ShipmentDetails_MPS_and_NoOfBoxes_Mismatch_Error
  Scenario Outline: <TC>: Cargo Booking- Create Booking -Basic Info- Shipment Details- Mismatch No of Boxes and MPS
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details for error validation "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length1>","<Breadth1>","<Height1>","<NoOfBoxes1>", "<Length2>","<Breadth2>","<Height2>","<NoOfBoxes2>"
    Then Validate error message of MPS and boxes mismatch "<errorMessageMismatch>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length1 | Breadth1 | Height1 | NoOfBoxes1 | Length2 | Breadth2 | Height2 | NoOfBoxes2 | errorMessageMismatch                                   |
      | lenovo     | Surface    | Credit       |        411052 |      411051 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 00XXXXX0000X1ZZ |           |                4 |                        100 |         3 |                          10 |      50 |       50 |      50 |          2 |      50 |       50 |      50 |          3 | Number of MPS & Total Number of boxes are not matched. |

  @Cargo_Booking_Create_Booking_ShipmentDetails_inches_to_cms_conversion_validation
  Scenario Outline: <TC>: Cargo Booking- Create Booking -Basic Info- Shipment Details- inches to cms conversion validation
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User navigates to Convert Inches to centimeter tab.
    Then Enter Inches value "<Inches>" and validate centimeter values "<Centimeter>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | Inches | Centimeter |
      | lenovo     | Surface    | Credit       |        411052 |      411051 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 00XXXXX0000X1ZZ |           |      2 |       5.08 |

  @Cargo_Booking_DashBoard_UI_Validation
  Scenario: <TC>: Cargo Booking- Booking Entry List
    Given Navigate to Cargo Booking option
    When Validate Booking UI present "Booking Entry List"
    Then Validate table details "Sr.No","AWB","#MPS","Client Ref. No","TL Type","Route Mode","Booking Mode","Client Name","Origin","Destination","Invoice Value (INR)","Phy Wt(KG)","Vol Wt(KG)","PickUp Date","Booking Date","Booking Method","Booking Status","Booking Type", "Action"

  @Cargo_Booking_Dashboard_Table_Validation_Pagination
  Scenario: <TC>: Cargo Booking- Booking Entry List- Pagination
    Given Navigate to Cargo Booking option
    When User verifies number of total records and number of records displayed on the screen
    Then Validate number of pages displayed and validate records displayed per page

  @Cargo_Booking_Dashboard_View_Booking_functionality
  Scenario Outline: <TC>: Cargo Booking- Booking Entry List- View Functionality
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then Verify the details saved with AWB Number and other details info "<clientName>","<Route Mode>","<Booking Mode>","<pickupPincode>","<dropPincode>","<LocationName>","<Mobile>","<Email>","<Address>","<GSTNo>","<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>","<Length>","<Breadth>","<Height>","<NoOfBoxes>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        |
      | MotoG      | Surface    | Credit       |        411051 |      411052 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 00XXXXX0000X1ZZ |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated |

  @Cargo_Booking_Dashboard_Table_Validation_Edit_Functionality
  Scenario: <TC>: Cargo Booking- Booking Entry List- Edit Functionality
    Given Navigate to Cargo Booking option
    When Validate Booking UI present "Booking Entry List"
    Then Validate details displayed in edit mode "Create Booking","Basic Information","Detailed Information"

  @Cargo_Booking_filter_input_validation
  Scenario Outline: <TC>: Cargo Booking- Booking Entry List- Filter inputs Validation
    Given Navigate to Cargo Booking option
    When User navigates to filter option
    When User enter filter inputs "<CompanyName>","<AWB>","<BookingStatus>","<TruckLoadType>","<RouteMode>","<BookingMode>","<BookingMethod>","<Origin>","<Destination>","<fromDateBooking>","<toDatePickup>"
    Then Validate details displayed after search click in booking entry list "<CompanyName>","<AWB>","<BookingStatus>","<TruckLoadType>","<RouteMode>","<BookingMode>","<BookingMethod>","<Origin>","<Destination>","<fromDateBooking>","<toDatePickup>"

    Examples: 
      | CompanyName  | AWB            | BookingStatus | TruckLoadType | RouteMode | BookingMode | BookingMethod | Origin | Destination | fromDateBooking | toDatePickup |
      | MotoG        | 94185221110710 | Booked        | PTL           | Surface   | FTC         | Web           |        |             | 07-12-2021      | 07-12-2021   |
      | FirstCry.com |   937292100009 | Booked        | PTL           | Surface   | Credit      | Web           |        |             | 30-12-2021      | 30-12-2021   |

  @Cargo_Booking_filter_input_validation_NoRecordsFound
  Scenario Outline: <TC>: Cargo Booking- Booking Entry List- Filter inputs Validation- No records found
    Given Navigate to Cargo Booking option
    When User navigates to filter option
    When User enters wrong AWB input "<awb>"
    Then validate no records found message "<message>"

    Examples: 
      | awb      | message          |
      | ahagstvh | No Records Found |

  @Cargo_Booking_filter_input_validation_partial_input
  Scenario Outline: <TC>: Cargo Booking- Booking Entry List- Filter inputs Validation partial_input
    Given Navigate to Cargo Booking option
    When User navigates to filter option
    When User enter filter inputs partial inputs "<CompanyName>","<AWB>","<BookingStatus>","<TruckLoadType>","<RouteMode>","<BookingMode>","<BookingMethod>"
    Then Validate details displayed after search click in booking entry list partial input "<CompanyNameDashboard>","<AWBDashboard>","<BookingStatusDashboard>","<TruckLoadTypeDashboard>","<RouteModeDashboard>","<BookingModeDashboard>","<BookingMethodDashboard>"

    Examples: 
      | CompanyName | AWB            | BookingStatus | TruckLoadType | RouteMode | BookingMode | BookingMethod | CompanyNameDashboard | AWBDashboard   | BookingStatusDashboard | TruckLoadTypeDashboard | RouteModeDashboard | BookingModeDashboard | BookingMethodDashboard |
      | Mot         | 94185221110710 | Book          | PT            | Sur       | FT          | We            | MotoG                | 94185221110710 | Booked                 | PTL                    | Surface            | FTC                  | Web                    |

  @Cargo_Booking_Download_Xlsx_file
  Scenario: <TC>: Cargo Booking- Booking Entry List- Download XLSX file validation
    Given Navigate to Cargo Booking option
    When User clicks on download xlsx button
    Then Verify file downloaded at location
    Then Verify details in the file downloaded "AWB","#MPS","Client Ref. No","TL Type","Route Mode","Booking Mode","Client Name","Origin","Destination"


@Cargo_DetailedInformation_HappyFlow   
      Scenario Outline: <TC>: Cargo Booking - Happy flow for Cargo Booking detailed information
       Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then Capture the AWBNumber Generated Booking
    Then User clicks on consignee details tab
    When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
    When User clicks on Save and Next button 
    Then User entters following shipment details "<Productcategories>","<Referenceno>","<Clientreferenceno>"
    |Electrical Goods|
    |Liquid|  
    When Userr enters Invoice and GST details Booking "<InvoiceValue>"
    Then User clicks on Save and Next button and Validate the Invoice and GST details result
    When User enters delivery date
    Then User clicks on save button and validate
   
 Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email        | Address            | GSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | ConsigneeCode | CompanyName | Mobile1    | ConsigneePOC | Email1   | GSTNo1          | Address1      | Productcategories | Referenceno | Clientreferenceno | InvoiceNumber | InvoiceValue | invoicegeneratedText |
      | lenovo     | Surface    | Credit       |        411052 |       411054 | Serviceable   | Local        | 9876543210 | ss@g.com     | local Address      | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address |                  | sdf1        | ssad1            | sssas11123sd         |        100 | Total Invoice Value should be equal to Total Shipment Value | 

  @Cargo_ConsigneeDetails
  Scenario Outline: <TC>: Consignee details - Happy flow for consignee details tab
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then User clicks on consignee details tab
    When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
    Then User clicks on Save and Next button and Validate record entered successful

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | gSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |   0202011 |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address |

  @Cargo_Consignee_NegativeBlankInputs
  Scenario Outline: <TC>: Consignee details - Happy flow for consignee details blank inputs
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then User clicks on consignee details tab
    When User enters Consignee details "<consigneeCode>","<companyName>","<Mobile1>","<consigneePOC>","<Email1>","<GSTNo1>","<Address1>"
    Then Validate the error messages consignee details "<ErrorConsigneeCodeMessage>","<ErrorCompanyNameMessage>","<ErrorMobileMessage>","<ErrorConsigneePoc>","<ErrorEmailMessage>","<ErrorGSTNumberMessage>","<ErrorAddressMessage>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | gSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1 | consigneePOC | Email1 | GSTNo1 | Address1 | ErrorConsigneeCodeMessage            | ErrorCompanyNameMessage            | ErrorMobileMessage                 | ErrorConsigneePoc                   | ErrorEmailMessage           | ErrorGSTNumberMessage                   | ErrorAddressMessage           |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated |               |             |         |              |        |        |          | The Consignee Code field is required | The Company Name field is required | The Mobile field format is invalid | The Consignee POC field is required | The Email field is required | Please enter valid format of GST Number | The Address field is required |

  @Cargo_Consigneecode_Validation_Negative_Scenarios_invalid_inputs
  Scenario Outline: <TC>:Consignee details - Happy flow for consignee details with Invalid inputs
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then User clicks on consignee details tab
    When User enters Consignee details "<Consigneecode>","<Companyname>","<Mobile1>","<ConsigneePOC>","<Email1>","<GSTNo1>","<Address1>"
    Then User clicks on Save and Next button and Validate for consignee details tab format validation
    Then Validate the error messages "<ErrorConsigneeCodeMessage>","<ErrorCompanyNameMessage>","<ErrorMobileMessage>","<ErrorConsigneePoc>","<ErrorEmailMessage>","<ErrorGSTNumberMessage>","<ErrorAddressMessage>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | gSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | Consigneecode | Companyname | Mobile1 | ConsigneePOC | Email1 | GSTNo1 | Address1 | ErrorConsigneeCodeMessage            | ErrorCompanyNameMessage            | ErrorMobileMessage                 | ErrorConsigneePoc                   | ErrorEmailMessage           | ErrorGSTNumberMessage                   | ErrorAddressMessage           |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                        100 |         1 |                          10 |     10 |      50 |     60 |         1 | AWB Generated |               |             | globe@  |              |        |        |          | The Consignee Code field is required | The Company Name field is required | The Mobile field format is invalid | The Consignee POC field is required | The Email field is required | Please enter valid format of GST Number | The Address field is required |

  @Cargo_Validate_Reverse_Flow_from_Shipment_details_to_Consignee_details
  Scenario Outline: <TC>: Consignee details - Reverse flow from shipment details tab to consignee details tab
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then User clicks on consignee details tab
    When User enters Consignee details "<Consigneecode>","<Companyname>","<Mobile1>","<ConsigneePOC>","<Email1>","<GSTNo1>","<Address1>"
    When User clicks on Save and Next button
    Then User clicks on consignee details tab and validate the result

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | gSTNo           | Telephone | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |           |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address |

  @Cargo_ConsigneeDetails_Validate_Add_consignee_for_Reverse_Flow_from_Shipment_details_to_Consignee_details
  Scenario Outline: <TC>: Consignee details - Reverse flow from shipment details tab to consignee details tab with click of add new consignee
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then User clicks on consignee details tab
    When User enters Consignee details "<Consigneecode>","<Companyname>","<Mobile1>","<ConsigneePOC>","<Email1>","<GSTNo1>","<Address1>"
    When User clicks on Save and Next button
    When User clicks on consignee details tab
    Then User clicks on Add new consignee and validate
    Then Validate the error messages "<ErrorConsigneeCodeMessage>","<ErrorCompanyNameMessage>","<ErrorMobileMessage>","<ErrorConsigneePoc>","<ErrorEmailMessage>","<ErrorGSTNumberMessage>","<ErrorAddressMessage>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | consigneeCode | companyName | Mobile1    | consigneePOC | Email1   | GSTNo1          | Address1      | ErrorConsigneeCodeMessage            | ErrorCompanyNameMessage            | ErrorMobileMessage                 | ErrorConsigneePoc                   | ErrorEmailMessage           | ErrorGSTNumberMessage                   | ErrorAddressMessage           |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address | The Consignee Code field is required | The Company Name field is required | The Mobile field format is invalid | The Consignee POC field is required | The Email field is required | Please enter valid format of GST Number | The Address field is required |

  @Cargo_ShipmentDetails_referencenoinputAndClientReferencenoinput
  Scenario Outline: <TC>: Shipment details - Happy flow for shipment details for referencenoinputAndClientReferencenoinput
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then User clicks on consignee details tab
    When User enters Consignee details "<ConsigneeCode>","<CompanyName>","<Mobile1>","<ConsigneePOC>","<Email1>","<GSTNo1>","<Address1>"
    When User clicks on Save and Next button
    When User entters following shipment details "<Referenceno>","<Clientreferenceno>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email        | Address            | gSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | ConsigneeCode | CompanyName | Mobile1    | ConsigneePOC | Email1   | GSTNo1          | Address1      | Productcategories | Referenceno | Clientreferenceno |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com     | local Address      | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | GLOBE         | GLOBE       | 9232323321 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address |                   | sdf1        | ssad1             |
      | lenovo     | Surface    | FTC          |        110505 |      132117 | Serviceable   | RetailShop   | 6987654123 | ss@gmail.com | Behind Post office | 07AABCU9603R1ZP |              200 |                     150.00 |         2 |                       30.00 |    100 |     120 |    105 |         2 | AWB Generated | GLOBE         | GLOBE       | 9229292929 | GLOBE123     | ss@g.com | 00XXXXX0000X1ZZ | local Address |                   | sfd1        | dsd1              |

  @Cargo_InvoiceGST_tab
  Scenario Outline: <TC>: Invoice and GST details - Happy flow for InvoiceGST details
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    When User clicks on Invoice and GST details tab
    When User enters Invoice and GST details "<InvoiceNumber>","<InvoiceValue>","<EWBNumber>"
    Then User clicks on Save and Next button and Validate the Invoice and GST details result

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | InvoiceNumber | InvoiceValue | EWBNumber    |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | kss11         |        51000 | 122121222112 |

  #| lenovo     | Surface    | Credit       |        411100 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | CAS2          |           23 | 122121222112 |
  #@Cargo_InvoiceGST_tab_informationbuttonclick
  #Scenario Outline: <TC>: Invoice and GST details - Happy flow for InvoiceGST details with informationbuttonclick
  #Given Navigate to Cargo Booking option
  #When User navigates to Create booking option
   #When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
  #When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
  #When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
  #Then Verify the AWB genarated and verify the status "<Status>"
  #When User clicks on Invoice and GST details tab
  #When User enters Invoice and GST details "<InvoiceNumber>","<InvoiceValue>","<EWBNumber>"
  #When user clicks on innformation tab
  #Then validate the information error message "<informationerrormesssage>"
  #Examples:
  #| clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | InvoiceNumber | InvoiceValue | EWBNumber    | informationerrormesssage |
  #| lenovo     | Surface    | Credit       |        411100 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | kss11         |           33 | 122121222112 | EWB Number is not valid  |
  @Cargo_ValueAddedservices_tab
  Scenario Outline: <TC>: Value added Services details - Happy flow for Value added details tab
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    When User clicks on Value Added Services tab
    When User enters delivery date
    When User enters delivery time "<RequestedFromTime>","<RequestedToTime>"
    When user enters the Hold days "<HoldDays>"
    Then User clicks on save button and validate

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | RequestedFromTime | RequestedToTime | HoldDays |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated |              2100 |            2359 |        0 |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated |              2100 |            2359 |    10.00 |

  @Cargo_InvoiceGST_tab_Blank_input_Validation
  Scenario Outline: <TC>: Invoice and GST details - Happy flow for InvoiceGST details blank input validation
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    When User clicks on Invoice and GST details tab
    When Usser enters following invoice and gst details "<InvoiceNumber>","<InvoiceValue>","<EWBNumber>"
    Then User clicks on Save and Next button and Validate the Invoice and GST details result
    Then validate thee error messages "<ErrorInvoiceNumber>","<ErrorInvoiceValue>","<ErrorEWBNumber>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | InvoiceNumber | InvoiceValue | EWBNumber | ErrorInvoiceNumber         | ErrorInvoiceValue         | ErrorEWBNumber                                   |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated |               |              |       112 | Invoice Number is required | Invoice Value is required | EWB Number is required and must be min 12 digits |

  @Cargo_detailedinformation_ExpandAll
  Scenario Outline: <TC>: Shipment details - Happy flow for ExpandAll functionality
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    When user clicks on Expand all button and collapse All button

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated |

  @Cargo_detailed_information_ExpandAllOpenTab
  Scenario Outline: <TC>: Shipment details - Happy flow for ExpandAll functionality one tab opened
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    Then User clicks on consignee details tab
    When user clicks on Expand all button and collapse All button

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated |

  @Cargo_InvoiceGST_tab_Error_Validation_TotalInvoiceValue
  Scenario Outline: <TC>: Invoice and GST details - Happy flow for InvoiceGST details with total invoice error message validation
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    When User clicks on Invoice and GST details tab
    When Userr enters Invoice and GST details "<InvoiceNumber>","<InvoiceValue>"
    Then User validates the Invoice and GST details result "<invoicegeneratedText>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | InvoiceNumber | InvoiceValue | invoicegeneratedText                                        |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | kss11         |         1000 | Total Invoice Value should be equal to Total Shipment Value |

  @Cargo_InvoiceGST_tab_EWB_ErrorMessageValidation
  Scenario Outline: <TC>: Invoice and GST details - Happy flow for InvoiceGST details with EWB error message validation
    Given Navigate to Cargo Booking option
    When User navigates to Create booking option
    When Enter client name "<clientName>" and enters serviceablity information "<Route Mode>","<Booking Mode>","<pickupPincode>" as well as "<dropPincode>","<Servicability>"
    When User enters pickup location details "<LocationName>","<Mobile>","<Email>","<Address>" and "<GSTNo>","<Telephone>"
    When User enters shipment details "<NumberOfInvoices>","<TotalShipmentDeclaredValue>","<NumberMPS>","<TotalShipmentDeclaredWeight>" and box details "<Length>","<Breadth>","<Height>","<NoOfBoxes>"
    Then Verify the AWB genarated and verify the status "<Status>"
    When User clicks on Invoice and GST details tab
    When User enters Invoice and GST details "<InvoiceNumber>","<InvoiceValue>","<EWBNumber>"
    Then validate ewb error message "<ewbErrorMessage>"

    Examples: 
      | clientName | Route Mode | Booking Mode | pickupPincode | dropPincode | Servicability | LocationName | Mobile     | Email    | Address       | GSTNo           | NumberOfInvoices | TotalShipmentDeclaredValue | NumberMPS | TotalShipmentDeclaredWeight | Length | Breadth | Height | NoOfBoxes | Status        | InvoiceNumber | InvoiceValue | EWBNumber | ewbErrorMessage         |
      | lenovo     | Surface    | Credit       |        411052 |      110505 | Serviceable   | Local        | 9876543210 | ss@g.com | local Address | 22AABCU9603R1ZX |                1 |                     100.00 |         1 |                       10.00 |     50 |      50 |     50 |         1 | AWB Generated | kss11         |        51000 |           | Please enter EWB Number |
