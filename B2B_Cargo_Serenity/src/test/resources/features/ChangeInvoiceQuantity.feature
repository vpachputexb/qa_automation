@Cargo_ChangeInvoiceQuantity
Feature: Cargo Print AWB

  @CargoLoginChangeInvoiceQuantity
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page
     Examples: 
      | Username                       | Password       |
      | Haribhau.tupe@xpressbees.com | Xpress@123 |
      
      
  @Cargo_ChangeInvoiceQuantity
  Scenario Outline: <TC>: Change Invoice Quantity - Happy flow for Change Invoice Quantity
    Given User Navigates to Support option
    When User enters "<AWBNumber>" in the text field. User clicks on Search button	
	  Then User clicks on Update button
    Then User validates the successful msg and User clicks on Ok button "<Successfulmsg>"
  
    
    Examples: 
      | AWBNumber      | Successfulmsg|
      | 9422072100758   |Invoice Count Updated Successfully.|
      
 
      @Cargo_InvalidAWBNo
  Scenario Outline: <TC>: Change Invoice Quantity - Negative flow for Change Invoice Quantity
    Given User Navigates to Support option 
    When User enters invalid "<AWBNumber>" in the text field and User clicks on Search button
    Then User validates the error msg and User clicks on Ok button "<errormsg>"
    
    Examples: 
    |AWBNumber   | errormsg |
    |989899898   | Data not found |
    
    #@Cargo_Resetfunctionality
    #Scenario Outline: <TC>: Change Invoice Quantity - Reset Functioanlity for Change Invoice Quantity
    #Given User Navigates to Support option
    #When User enters "<AWBNumber>" in the text field. User clicks on Search button	
    #Then User clicks on Reset button
    #Then User enters "<AWBNumber1>" in the text field. User clicks on Search button
    #Then User clicks on Update button
    #Then User validates the successful msg and User clicks on Ok button "<Successfulmsg>"
    #
    #Examples:
    #
    #| AWBNumber      | AWBNumber1 | Successfulmsg |
    #| 933452100070 | 933452100104 | Invoice Count Updated Successfully. |
    #
   
      @Cargo_UpdateInvoiceQuantityforCompleteBooking
    Scenario Outline: <TC>: Change Invoice Quantity - Update Invoice Quantity for Complete Booking
    Given User Navigates to Support option
    When User enters "<AWBNumber>" in the text field. User clicks on Search button
    Then User validates the Error msg and User clicks on Ok button "<Errormsg>"
    
       Examples:
    | AWBNumber      | Errormsg |
    | 94192321420183 | Cannot Update Invoice For Complete Booking |
    
    
    @Cargo_ChnageinInvoiceQuantity
    Scenario Outline: <TC>: Change Invoice Quantity - Change in Invoice Quantity
    Given User Navigates to Support option
    When User enters "<AWBNumber>" in the text field. User clicks on Search button
    Then User removes pre filled invoice quantity and enters a new quantity "<InvoiceQuantityvalue>"
    Then User clicks on Update button
    Then User validates the successful msg and User clicks on Ok button "<Successfulmsg>"
    
    Examples:
    | AWBNumber      | InvoiceQuantityvalue | Successfulmsg |
    | 933452100070 | 3 | Invoice Count Updated Successfully. |
    
   @Cargo_ChnageinInvoiceQuantity
    Scenario Outline: <TC>: Change Invoice Quantity - Click on search without entering shipping id
    Given User Navigates to Support option
    When User clicks on Search button without entering shipping id
    Then User validates an error msg and User clicks on Ok button "<errormsg1>"
    
    Examples:
    |errormsg1|
    |Shipment Id is required|
    
    
    
    
    
      
     