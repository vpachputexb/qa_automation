@Cargo_FLM_AddExtraCharges
Feature: Cargo Trip management add extra charges feature

  @CargoLoginAddExtraCharges
  Scenario Outline: <TC>: Login in Cargo
    Given Open the Cargo application
    When User Enters "<Username>" and "<Password>"
    And Click on Login button
    Then Validate "<Username>" Email address in welcome message on landing page

    Examples: 
      | Username                       | Password       |
      | vishal.pachpute@xpressbees.com | XpressBees@123 |

  @Cargo_FLM_pickupAllocationType
  Scenario Outline: <TC>: Happy flow for Trip Management add extra charges - pickup Allocation type
    Given User Navigaates to Trip Management
    When User Enters the Add Extra Charges Details "<Allocation Type2>","<Allocation Type>","<Shipping id>","<From date>","<To date>","<Shipment Sized Charges>","<Mathadi/Varai Charges>","<Portgages Charges>","<Special Delivery Charges>","<Loading/Unloading Charges>","<ODA Charges>","<Full Truck Load>"
    Then Click on Save button and validate the result "<success message>"

    Examples: 
      | Allocation Type2 | Allocation Type | Shipping id    | From date | To date  | Shipment Sized Charges | Mathadi/Varai Charges | Portgages Charges | Special Delivery Charges | Loading/Unloading Charges | ODA Charges | Full Truck Load | success message |
      | Delivery         | Pickup          | 94192321220443 |  16022022 | 17022022 |                     10 |                    20 |                10 |                       10 |                        10 |          10 |              10 | successfull.    |

  @Cargo_FLM_DeliveryAllocationType
  Scenario Outline: <TC>: Happy flow for Trip Management add extra charges - delivery Allocation type
    Given User Navigaates to Trip Management
    When User Enters the Add Extra Charges Details "<Allocation Type2>","<Allocation Type>","<Shipping id>","<From date>","<To date>","<Shipment Sized Charges>","<Mathadi/Varai Charges>","<Portgages Charges>","<Special Delivery Charges>","<Loading/Unloading Charges>","<ODA Charges>","<Full Truck Load>"
    Then Click on Save button and validate the result "<success message>"

    Examples: 
      | Allocation Type2 | Allocation Type | Shipping id   | From date | To date  | Shipment Sized Charges | Mathadi/Varai Charges | Portgages Charges | Special Delivery Charges | Loading/Unloading Charges | ODA Charges | Full Truck Load | success message |
      | Pickup           | Delivery        | 9422072100221 |  16022022 | 17022022 |                     10 |                    20 |                10 |                       10 |                        10 |          10 |              10 | successfull.    |

  @Cargo_FLM_Initialclearbuttonfunctionality
  Scenario Outline: <TC>: Trip Management add extra charges - Top clear button functionality validation
    Given User Navigaates to Trip Management
    When User Enters the following Add Extra Charges Details "<Allocation Type2>","<Allocation Type>","<Shipping id>","<From date>","<To date>"
    Then click on clear button and Validate the button functionality

    Examples: 
      | Allocation Type2 | Allocation Type | Shipping id   | From date | To date  |
      | Pickup           | Delivery        | 9422072100293 |  16022022 | 17022022 |

  @Cargo_FLM_Lastclearbuttonfunctionality
  Scenario Outline: <TC>: Trip Management add extra charges - last clear button functionality validation
    Given User Navigaates to Trip Management
    When User Enters the Add Extra Charges Details as of following "<Allocation Type2>","<Allocation Type>","<Shipping id>","<From date>","<To date>","<Shipment Sized Charges>","<Mathadi/Varai Charges>","<Portgages Charges>","<Special Delivery Charges>","<Loading/Unloading Charges>","<ODA Charges>","<Full Truck Load>"
    Then click on final clear button and Validate the button functionality

    Examples: 
      | Allocation Type2 | Allocation Type | Shipping id   | From date | To date  | Shipment Sized Charges | Mathadi/Varai Charges | Portgages Charges | Special Delivery Charges | Loading/Unloading Charges | ODA Charges | Full Truck Load |
      | Pickup           | Delivery        | 9422072100292 |  16022022 | 17022022 |                     10 |                    20 |                10 |                       10 |                        10 |          10 |              10 |

  @Cargo_FLM_Exportbuttonfunctionality
  Scenario Outline: <TC>: Trip Management add extra charges - export button functionality validation
    Given User Navigaates to Trip Management
    When User Enters the following Add Extra Charges Details "<Allocation Type2>","<Allocation Type>","<Shipping id>","<From date>","<To date>"
    Then click on export button and validate the button functionality

    Examples: 
      | Allocation Type2 | Allocation Type | Shipping id   | From date | To date  |
      | Pickup           | Delivery        | 9422072100293 |  16022022 | 17022022 |

  @Cargo_FLM_BlankInputValidation
  Scenario Outline: <TC>: Trip Management add extra charges - blank input field validation
    Given User Navigaates to Trip Management
    When Enters the Add Extra Charges Details "<Allocation Type2>","<Allocation Type>","<Shipping id>","<From date>","<To date>","<Shipment Sized Charges>","<Mathadi/Varai Charges>","<Portgages Charges>","<Special Delivery Charges>","<Loading/Unloading Charges>","<ODA Charges>","<Full Truck Load>"
    Then Click on Save button and validate the  error message "<errormessageextracharge>"

    Examples: 
      | Allocation Type2 | Allocation Type | Shipping id   | From date | To date | Shipment Sized Charges | Mathadi/Varai Charges | Portgages Charges | Special Delivery Charges | Loading/Unloading Charges | ODA Charges | Full Truck Load | errormessageextracharge |
      | Delivery         | Delivery        | 9422072100293 |           |         |                        |                       |                   |                          |                           |             |                 | Please Enter Values     |

  @Cargo_FLM_UploadButtonFunctionality
  Scenario Outline: <TC>: Trip Management add extra charges - upload button functionality
    Given User Navigaates to Trip Management
    When User Enters the Add Extra Charges Details "<Allocation Type2>","<Allocation Type>","<Shipping id>","<From date>","<To date>","<Shipment Sized Charges>","<Mathadi/Varai Charges>","<Portgages Charges>","<Special Delivery Charges>","<Loading/Unloading Charges>","<ODA Charges>","<Full Truck Load>"
    When user clicks on upload button and validate upload functionality "<shipmentsizedupload>","<mathadichargesupload>","<portgagechargesupload>","<specialdeliverychargesupload>","<loadingchargesupload>","<odachargesupload>","<fulltruckloadupload>","<successfuluploadmessage>"

    Examples: 
      | Allocation Type2 | Allocation Type | Shipping id    | From date | To date  | Shipment Sized Charges | Mathadi/Varai Charges | Portgages Charges | Special Delivery Charges | Loading/Unloading Charges | ODA Charges | Full Truck Load | shipmentsizedupload | mathadichargesupload | portgagechargesupload | specialdeliverychargesupload | loadingchargesupload | odachargesupload | fulltruckloadupload | successfuluploadmessage                |
      | Delivery         | Delivery        | 94192321220443 |  16022022 | 17022022 |                     10 |                    20 |                10 |                       10 |                        10 |          10 |              10 | serenity1.png       | serenity2.png        | serenity3.png         | serenity4.png                | serenity5.png        | serenity6.png    | sample.docx         |  successfull. |

  @Cargo_FLM_InvalidFileFormatuploadValidation
  Scenario Outline: <TC>: Trip Management add extra charges - Invalid File Format Upload Validation
    Given User Navigaates to Trip Management
    When User Enters the Add Extra Charges Details "<Allocation Type2>","<Allocation Type>","<Shipping id>","<From date>","<To date>","<Shipment Sized Charges>","<Mathadi/Varai Charges>","<Portgages Charges>","<Special Delivery Charges>","<Loading/Unloading Charges>","<ODA Charges>","<Full Truck Load>"
    When user clicks on upload button and uploads the invalid file "<shipmentsizedupload>","<mathadichargesupload>","<portgagechargesupload>","<specialdeliverychargesupload>","<loadingchargesupload>","<odachargesupload>","<fulltruckloadupload>"
    Then validate the invalid upload error message "<invaliduploaderrormessage>"

    Examples: 
      | Allocation Type2 | Allocation Type | Shipping id    | From date | To date  | Shipment Sized Charges | Mathadi/Varai Charges | Portgages Charges | Special Delivery Charges | Loading/Unloading Charges | ODA Charges | Full Truck Load | shipmentsizedupload | mathadichargesupload | portgagechargesupload | specialdeliverychargesupload | loadingchargesupload | odachargesupload | fulltruckloadupload  | invaliduploaderrormessage              |
      | Delivery         | Delivery        | 94192321220443 |  16022022 | 17022022 |                     10 |                    20 |                10 |                       10 |                        10 |          10 |              10 | serenity1.png       | serenity2.png        | serenity3.png         | serenity4.png                | serenity5.png        | serenity6.png    | awbExtraCharges.xlsx | Please upload only Allowed file format |

  @Cargo_FLM_LargeSizeFileValidation
  Scenario Outline: <TC>: Trip Management add extra charges - Large File Size Validation
    Given User Navigaates to Trip Management
    When User Enters the Add Extra Charges Details "<Allocation Type2>","<Allocation Type>","<Shipping id>","<From date>","<To date>","<Shipment Sized Charges>","<Mathadi/Varai Charges>","<Portgages Charges>","<Special Delivery Charges>","<Loading/Unloading Charges>","<ODA Charges>","<Full Truck Load>"
    When user uploads large file "<shipmentsizedupload>","<mathadichargesupload>","<portgagechargesupload>","<specialdeliverychargesupload>","<loadingchargesupload>","<odachargesupload>","<fulltruckloadupload>"
    Then validate the large file size error message "<largefilesizeerrormessage>"

    Examples: 
      | Allocation Type2 | Allocation Type | Shipping id    | From date | To date  | Shipment Sized Charges | Mathadi/Varai Charges | Portgages Charges | Special Delivery Charges | Loading/Unloading Charges | ODA Charges | Full Truck Load | shipmentsizedupload | mathadichargesupload | portgagechargesupload | specialdeliverychargesupload | loadingchargesupload | odachargesupload | fulltruckloadupload | largefilesizeerrormessage             |
      | Delivery         | Delivery        | 94192321220443 |  16022022 | 17022022 |                     10 |                    20 |                10 |                       10 |                        10 |          10 |              10 | serenity1.png       | serenity2.png        | serenity3.png         | serenity4.png                | serenity5.png        | serenity6.png    | largefile.jpg       | Please Upload Document Less Than 5MB. |

  @Cargo_FLM_FromdateValidation
  Scenario Outline: <TC>: Trip Management add extra charges - From date Validation
    Given User Navigaates to Trip Management
    Then click on export button and validate the button functionality
    Then validate the from date alert message "<fromdatealertmessage>"

    Examples: 
      | fromdatealertmessage    |
      | Please select From date |

  @Cargo_FLM_blankTodateValidation
  Scenario Outline: <TC>: Trip Management add extra charges - To date Validation
    Given User Navigaates to Trip Management
    When user enters the allocation type and from date details "<Allocation Type2>","<Allocation Type>","<From date>"
    Then click on export button and validate the button functionality
    Then validate the to date alert message "<todatealertmessage>"

    Examples: 
      | Allocation Type2 | Allocation Type | From date | todatealertmessage   |
      | Delivery         | Delivery        |  16022022 | Please enter To date |

  @Cargo_FLM_NoRecordsFoundValidation
  Scenario Outline: <TC>: Trip Management add extra charges - No records found validation
    Given User Navigaates to Trip Management
    When user enters the allocation type and date details "<Allocation Type2>","<Allocation Type>","<From date>","<To date>"
    Then click on export button and validate the button functionality
    Then validate the no records found "<norecordsfoundalertmessage>"

    Examples: 
      | Allocation Type2 | Allocation Type | From date | To date  | norecordsfoundalertmessage |
      | Delivery         | Delivery        |  12012022 | 13012022 | No records found           |

  @Cargo_FLM_LesserToDateValidation
  Scenario Outline: <TC>:Trip Management add extra charges - validate to date value less than from date
    Given User Navigaates to Trip Management
    When user enters the allocation type and date details "<Allocation Type2>","<Allocation Type>","<From date>","<To date>"
    Then click on export button and validate the button functionality
    Then validate the less to date input value "<lowertodatealertmessage>"

    Examples: 
      | Allocation Type2 | Allocation Type | From date | To date  | lowertodatealertmessage        |
      | Pickup           | Delivery        |  12012022 | 11012022 | To date is Less than From date |

  @Cargo_FLM_UploadFileCancelButtonValidation
  Scenario Outline: <TC>:Trip Management add extra charges - validate the upload file cancel button
    Given User Navigaates to Trip Management
    When user enters allocation type and shipping and date details "<Allocation Type2>","<Allocation Type>","<Shipping id>"
    When user hits the upload button "<shipmentsizedupload>"
    Then user clicks on cancel button and validate the upload file cancel button functionality "<cancelbuttonalertmessage>"

    Examples: 
      | Allocation Type2 | Allocation Type | Shipping id    | shipmentsizedupload | cancelbuttonalertmessage |
      | Delivery         | Delivery        | 94192321220443 | serenity1.png       | FAILURE.                 |

  @Cargo_FLM_UploadFileViewButtonValidation
  Scenario Outline: <TC>:Trip Management add extra charges - validate the upload file view button
    Given User Navigaates to Trip Management
    When user enters allocation type and shipping and date details "<Allocation Type2>","<Allocation Type>","<Shipping id>"
    When user hits the upload button "<shipmentsizedupload>"
    Then user clicks on view button and validate the upload file view button functionality

    Examples: 
      | Allocation Type2 | Allocation Type | Shipping id    | shipmentsizedupload |
      | Pickup           | Delivery        | 94192321220443 | serenity1.png       |
     
     
  @Cargo_FLM_UploadFileCloseButtonValidationInViewMode
  Scenario Outline: <TC>:Trip Management add extra charges - validate the opened uploaded file in the view mode
    Given User Navigaates to Trip Management
    When user enters allocation type and shipping and date details "<Allocation Type2>","<Allocation Type>","<Shipping id>"
    When user hits the upload button "<shipmentsizedupload>"
    Then user clicks on close button of the file opened and validate click functionality

    Examples: 
      | Allocation Type2 | Allocation Type | Shipping id    | shipmentsizedupload |
      | Pickup           | Delivery        | 94192321220443 | serenity1.png       |
      
      
   @Cargo_FLM_AwbNotFoundValidation
    Scenario Outline: <TC>:Trip Management add extra charges - validate the AWB Not Found alert message
    Given User Navigaates to Trip Management
    When user enters allocation type and shipping and date details "<Allocation Type2>","<Allocation Type>","<Shipping id>"
    Then user clicks on search button and validate the awb error message "<awberrormessage>"
    
   Examples: 
    | Allocation Type2 | Allocation Type | Shipping id    | awberrormessage |
    | Pickup          | Delivery       | 94192321220442  | AWB Not Found.  |