package starter.stepdefinitions;


import com.xb.cafe.ui.pages.Cargo_ApprovalHierarchy;
import com.xb.cafe.ui.pages.Cargo_ChangePhysicalWeight;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Cargo_ChangePhysicalWeight_StepDef {

	@Steps
	Cargo_ChangePhysicalWeight change_physicalweight;

	@Given("Navigate to Support page")

	public void navigatetoSupport() throws InterruptedException {
		change_physicalweight.NavigatetoSupportLink();
	}

	@When("Clicked on Change Physical weight link")
	public void navigatetoChangephysicalwt() throws InterruptedException {
		change_physicalweight.NavigateToChangePhysicalWeight();
	}

	@Then("User should navigate on {string} page")
	public void landedonChangePhysicalWeightPage(String ChangePhysicalWeight) {
		change_physicalweight.LandedOnChangePhysicalWeightPage(ChangePhysicalWeight);
	}

	@When("Shipment is id {string} and clicked on Search button")
	public void enterShipmentidandclickSearch(String ShipemntID) throws InterruptedException {
		change_physicalweight.EnterShipmentIDAndSearch(ShipemntID);
	}

	@Then("Proper validation message should displayed {string}")
	public void erroroninvalidShipemtID(String ErrormsgonInvalidShipmentID) throws InterruptedException {
		change_physicalweight.ErrorOnInvalidShipmentID(ErrormsgonInvalidShipmentID);

	}

	@When("click on reset button")
	public void clickresetbtn() throws InterruptedException {
		change_physicalweight.ClickOnResetButton();
	}

	@Then("{string} should get blank")
	public void resetshipmentid(String shipmentid) throws InterruptedException {
		change_physicalweight.ResetShipmentID(shipmentid);
	}

	@Then("It should display update button")
	public void updatebtndisplayed() throws InterruptedException {
		change_physicalweight.UpdatebtnDisplayed();
	}

	@When("Enter {string} and clicked on update")
	public void enterPhysicalwt(String physicalweight) throws InterruptedException {
		change_physicalweight.EnterPhysicalWt(physicalweight);
	}

	@Then("Proper {string} should displayed")
	public void succesonwtUpdate(String Successmsg) throws InterruptedException {
		change_physicalweight.SuccessMsgOnUpdate(Successmsg);
	}

	@Then("It should show message {string}")
	public void msgonupdatingwtDeliveredAWB(String DeliveredAWBMsg) throws InterruptedException {
		change_physicalweight.MsgonUpdatingWTDeliveredAWB(DeliveredAWBMsg);
	}

}
