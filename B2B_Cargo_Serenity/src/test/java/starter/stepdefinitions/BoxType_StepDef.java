package starter.stepdefinitions;

import com.xb.cafe.ui.pages.BoxType;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class BoxType_StepDef {

	@Steps
	BoxType boxtype;

	@Given("Navigate to Master menu")
	public void navigatetomastermenu() throws InterruptedException {
		boxtype.navigateToMasterMenu();
	}

	@Then("Check Box type menu displayed")
	public void boxmenuvisibility() throws InterruptedException {
		boxtype.boxTypeMenuVisibility();
	}

	@When("click on Box type menu")
	public void clickboxtype() throws InterruptedException {
		boxtype.clickBoxType();
	}

	@Then("USer should land on Box type page")
	public void landedonboxtype() throws InterruptedException {
		boxtype.landedOnBoxType();
	}

	@Then("Verify listing fields {string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string}")
	public void listing(String BoxType, String BoxTypeDescription, String StackingLevel, String BoxStrength,
			String MinWeight, String MaxWeight, String CreatedBy, String CreatedDate, String LastModifiedBy,
			String LastModifiedDate, String Status, String Action) throws InterruptedException {
		boxtype.listingElements(BoxType, BoxTypeDescription, StackingLevel, BoxStrength, MinWeight, MaxWeight,
				CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate, Status, Action);
	}

	@When("click on create button")
	public void clickcreatebtn() throws InterruptedException {
		boxtype.clickOnCreateBtn();
	}

	@Then("It should navigate to create box type page with heading {string}")
	public void createboxtypepage(String CreateBoxtypeheading) throws InterruptedException {
		boxtype.createBoxTypePage(CreateBoxtypeheading);
	}

	@Then("It should show all elements")
	public void createboxtypePageElemnets() throws InterruptedException {
		boxtype.createBoxTypePageElements();
	}

	@When("click on search filter")
	public void clickonsearch() throws InterruptedException {
		boxtype.clickonSearchFilterIcon();
	}

	@When("Enter box type {string}")
	public void enteBoxType(String BoxType) {
		boxtype.enterBoxTypeIntoSearchFilter(BoxType);
	}

	@When("clicked on reset button")
	public void clickonreset() {
		boxtype.clickOnResetBtn();
	}

	@Then("It should clear data from boxtype {string}")
	public void checkBoxTypefieldData(String Value) {
		boxtype.checkBoxTypeValue(Value);
	}

	@When("clicked on Search button")
	public void clicksearchbtn() throws InterruptedException {
		boxtype.clickOnSearchBtn();
	}

	@Then("It should show result {string}")
	public void searchresult(String Result) {
		boxtype.searchResult(Result);
	}

	@Then("It should show elements")
	public void searchelements() throws InterruptedException {
		boxtype.searchElements();
	}

	@When("Data is entered into {string},{string},{string},{string},{string},{string}")
	public void enterdatatocreateboxtype(String BoxType, String StackingLevel, String BoxStrength, String Description,
			String Minimumwt, String Maxwt) throws InterruptedException {
		boxtype.enterDataToCreateBoxType(BoxType, StackingLevel, BoxStrength, Description, Minimumwt, Maxwt);
	}

	@When("clicked on Save button")
	public void clickonsavebtn() throws InterruptedException {
		boxtype.clickOnSaveBtn();

	}

	@Then("It should show success message {string}")
	public void successmsg(String SuccMsg) {
		boxtype.successMSG(SuccMsg);
	}

	@When("Enter {string}")
	public void enterboxtype(String Boxtype3) {
		boxtype.enterBxType3(Boxtype3);
	}

	@Then("check status of boxtype {string}")
	public void checkstatus(String Status) {
		boxtype.checkStatus(Status);
	}

	@When("click on Edit button")
	public void clickeitbtn() throws InterruptedException {
		boxtype.clickOnEditBtn();
	}

	@Then("It should show record updated msg {string}")
	public void msgonupdation(String UpdatedMsg) throws InterruptedException {
		boxtype.messageOnUpdatedBoxType(UpdatedMsg);
	}

	@When("select inactive status {string}")
	public void selectinactiveboxtype(String Status2) throws InterruptedException {
		boxtype.selectInactiveBoxType(Status2);
	}

	@When("clicked on inactive status")
	public void clickoninactivestatus() {
		boxtype.clickOnInactiveStatusBoxType();
	}

	@Then("It should show popup for confirmation message {string}")
	public void confirmationmsg(String ConfirmationMsg) throws InterruptedException {
		boxtype.confirmationMsgPopup(ConfirmationMsg);
	}

	@When("clicked on OK button It should show message {string}")
	public void statuschnagedcheck(String MSG) {
		boxtype.statusChangedCheck(MSG);

	}

	@When("Box type is entered as blank {string}")
	public void enterblankboxtype(String Boxtype3) {
		boxtype.enterBlankBoxType(Boxtype3);
	}
	  @Then("It should show validation message {string}")
	  public void  validationmsg(String Validationmsg) {
		  boxtype.validationMsg(Validationmsg);
	  }
	  @Then("It should show validation message {string},{string},{string},{string},{string}")
	  public void validationsmsgs(String ValidationMsgStackingLevel,String ValidationMsgBoxStrength,String ValidationMsgBoxTypeDesc,String ValidationMsgminwt,String ValidationMsgmaxwt) {
		  boxtype.validationMsg(ValidationMsgStackingLevel,ValidationMsgBoxStrength,ValidationMsgBoxTypeDesc,ValidationMsgminwt,ValidationMsgmaxwt);
	  }
	
}
