package starter.stepdefinitions;

import com.xb.cafe.ui.pages.Cargo_UADShipmentSearch;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Cargo_UADShipmentSearch_StepDef {

	@Steps
	Cargo_UADShipmentSearch uadshipmentsearch;

	@Given("Navigate on FLM Menu")
	public void navigateFLM() throws InterruptedException {
		uadshipmentsearch.NavigateFLM();
	}

	@When("Navigate on UAD menu")
	public void navigateUAD() throws InterruptedException {
		uadshipmentsearch.NavigateUAD();
	}

	@Then("UAD Shipment Search menu should displayed")
	public void uadShipmentSearchvisibility() throws InterruptedException {
		uadshipmentsearch.UADShipmentSearchMenu();
	}

	@When("Navigate on UAD shipment search menu")
	public void navigateUAdShipmentSearch() throws InterruptedException {
		uadshipmentsearch.NaviagteUADShipmentSearch();
	}

	@Then("Sipment search listing should display with columns {string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string}")
	public void shipmentSearchtablelisting(String ShippingID, String ConsigneePhone, String ConsigneeEmail,
			String DeliveryPincode, String DeliveryAddress, String DeliveryDate, String RADInscan,
			String RescheduleDate, String UADMarkedStatus, String UADMarkedCount, String Action)
			throws InterruptedException {
		uadshipmentsearch.ShipmentSearchListing(ShippingID, ConsigneePhone, ConsigneeEmail, DeliveryPincode,
				DeliveryAddress, DeliveryDate, RADInscan, RescheduleDate, UADMarkedStatus, UADMarkedCount, Action);
	}

	@Then("Check Mark UAD button is displayed")
	public void markUADbtn() throws InterruptedException {
		uadshipmentsearch.markUADBtn();
	}

	@When("clicked on Mark UAD button")
	public void markuadbtnclick() throws InterruptedException {
		uadshipmentsearch.clickMarkUADBtn();
	}

	@Then("It should show Mark UAD popup {string}")
	public void markuadpopup(String PopupHEading) throws InterruptedException {
		uadshipmentsearch.markUadPopupDispolayed(PopupHEading);
	}

	@Then("It should show Mark UAD popup with elements {string},{string},{string} and Submit Cancel button")
	public void checkElements(String Reason, String Subreason, String RescheduleDate) throws InterruptedException {
		uadshipmentsearch.CheckMarkUADPopupElements(Reason, Subreason, RescheduleDate);
	}

	@When("click on submit button")
	public void clicksubmitbtn() throws InterruptedException {
		uadshipmentsearch.clickOnSubmitBtn();
	}

	@Then("It should show error message {string}")
	public void showErroronblankreason(String ReasonValidationMsg) throws InterruptedException {
		uadshipmentsearch.errorMessageBlankReason(ReasonValidationMsg);

	}

	@When("click on cancel button")
	public void clickcancel() throws InterruptedException {
		uadshipmentsearch.clickCancelBtn();
	}

	@Then("It should close popup and should show Search filter")
	public void closebtnFunctionality() throws InterruptedException {
		uadshipmentsearch.ClosebtnFunctionality();
	}

	@When("Selected Reason {string}")
	public void selectreason(String UADReason) throws InterruptedException {
		uadshipmentsearch.selectUADReason(UADReason);
	}

	@Then("It should validation message on blank reschedule date {string}")
	public void erroronblankrescheduleDate(String RescheduleDateerrMsg) throws InterruptedException {
		uadshipmentsearch.errorOnBlankRescheduleDate(RescheduleDateerrMsg);
	}
	@Then("It should validation message on blank sub reason {string} reschedule date {string}")
	public void validationonoperationalReason(String Subreason,String RescheduleDateerrorMsg) throws InterruptedException {
		uadshipmentsearch.validationMessageOnBlankdata(Subreason,RescheduleDateerrorMsg);
		
	}
	@When("Selected operational Reason {string}")
	public void selectoperationalreason(String OperationReason) throws InterruptedException {
		uadshipmentsearch.selectOperationalReason(OperationReason);
	}
	
	@When("Selected Consignee Reason {string}")
	public void selectconsigneereason(String UADReason1) throws InterruptedException {
		uadshipmentsearch.selectConsigneeReason(UADReason1);
	}
    @When("Selected Reschduledate {string}")
    public void selectrescheduledate(String ReschdeuleDate) throws InterruptedException {
    	uadshipmentsearch.selectRescheduleDate(ReschdeuleDate);
    }
    @Then("It should show sucess message of UAD marked {string}")
    public void successonMasrkuad(String UADMarkedSucessMsg) {
    	uadshipmentsearch.successOnMarkUAD(UADMarkedSucessMsg);
    }
    @When("clicked on Reason drop down")
    public void clickonreasondropdown() throws InterruptedException {
    	uadshipmentsearch.clickOnReasonDropDown();
    }
    @Then("It should show list of reasons {string}")
    public void listofreasons(String UADReasons) throws InterruptedException {
    	uadshipmentsearch.listOfUADReasons(UADReasons);
    }

    @When("Selected operation Reason {string} and {string}" )
    public void selectOPReason(String OpsReason,String Subreason) throws InterruptedException {
    	uadshipmentsearch.selectOpsReason(OpsReason,Subreason);
    }
    @When("Select Reschduledate {string}")
    public void selectRSdate(String ReschdeuleDate1) throws InterruptedException {
    	uadshipmentsearch.selectReshDate(ReschdeuleDate1);
    }
    @Then("It should show  message of UAD marked {string}")
    public void successmsg(String UADMarkedSucessMsg1) throws InterruptedException {
    	uadshipmentsearch.showSuccessMSG(UADMarkedSucessMsg1);
    }
    
    @Then("It should not allow to select reason {string}")
    public void  opsreasonDisabled(String OpsReason1) throws InterruptedException {
    	uadshipmentsearch.opsReasondisabled(OpsReason1);
}
    
    
    @When("click on Search filter")
    public void clickonsearch() throws InterruptedException {
    	uadshipmentsearch.clickOnSearchFilter();
    }
    @When("Entered {string} and clicked Search button")
    public void enterdeliveredAWB(String DeliveredAWB) throws InterruptedException {
    	uadshipmentsearch.enterDeliveredAWB(DeliveredAWB);
    }
    @Then("It should show text {string}")
    public void textdisplayed(String TotalRecords){
    	uadshipmentsearch.textDisplayed(TotalRecords);
    }
    
    @When("Enter Rapid booking AWB {string} and click Search button")
    public void enterRapidbookingAWB(String RapidAWB) throws InterruptedException {
    	uadshipmentsearch.enterRapidBookingAWB(RapidAWB);
    }
    @Then("It should show message on mouse hover on {string}")
    public void msgonmousehover(String RapidBookingMsg) throws InterruptedException {
    	uadshipmentsearch.msgOnMouseHover(RapidBookingMsg);
    }
    
    @Then("It should show  message of date {string}")
    public void datemsg(String UADMarkedSucessMsg2) throws InterruptedException {
    	uadshipmentsearch.msgOncurrentDateselection(UADMarkedSucessMsg2);
    }
    
    
    @When("Select Consignee Reason {string}")
    public void selectcongnreason(String UADReason4) throws InterruptedException {
    	uadshipmentsearch.slctUADReason(UADReason4);
    }
    @When("Select more than max Reschduledate {string}")
    public void slctreshcdate(String ReschdeuleDate4) throws InterruptedException {
    	uadshipmentsearch.slctRESHdate(ReschdeuleDate4);
    }
    @Then("It should show message of UAD marked {string}")
    public void maxrescheduledatelimit(String MaxRescheduledatereachedMsg) throws InterruptedException {
    	uadshipmentsearch.maxRESHDATELimitReached(MaxRescheduledatereachedMsg);
    }
    
    @When("Enter AWB {string} and click Search button")
    public void enterOFDawb(String AWB) throws InterruptedException {
    	uadshipmentsearch.enterOFDAWB(AWB);
    	
    }
    
    @Then("It shows message on mouse hover on {string}")
    public void checkmsgdisplayed(String AWBMSG) throws InterruptedException {
    uadshipmentsearch.checkMEssageDisplayed(AWBMSG);
}
    
    @Then("It should show Mark uad button enabled")
    public void undeliveredAWB() {
    	uadshipmentsearch.undeliveredAWBMarkUADBtnEnabled();
    }
    @When("UADMArked status is {string}")
    public void status(String Status) {
    	uadshipmentsearch.checkStatus(Status);
    }
    
    @Then("It should show count as {string}")
    public void count(String Count) {
    	uadshipmentsearch.checkCount(Count);
    }
}

