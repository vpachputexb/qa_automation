package starter.stepdefinitions;

import com.xb.cafe.ui.pages.Slabs;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Slabs_StepDef {

//	@Given("I login in Cargo Web UI with following credentials")
//	public void cargo_Login(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
//		Cargo.cargoLogin(dataTable);
//	}
	@Steps
	Slabs Slabs;
	
	
	@Given("User Navigates to Master data option")
	public void userNavigatesToMasterDataOption() throws InterruptedException {
		 Slabs.userNavigatesToMasterDataOption();
	}
	
	@When("User Navigates to create new slab option")
	public void userNavigatesToCreateNewSlabOption() throws InterruptedException {
		Slabs.userNavigatesToCreateNewSlabOption();
	   }

	@Then("User enters {string} in the text field and user selects Slab Type Invoice amount")
	public void userEntersInTheTextFieldAndUserSelectsSlabTypeInvoiceAmount(String SlabName) throws InterruptedException {
	Slabs.userEntersInTheTextFieldAndUserSelectsSlabTypeInvoiceAmount(SlabName);
}
	
	@Then ("User selects higher break limit type {string} and enter higher break limit value {string}")
    public void userSelectsHigherBreakLimitTypeAndEnterHigherBreakLimitValue(String BreakHigherLimitType,String HigherBreakLimitValue)  throws InterruptedException {
    Slabs.userSelectsHigherBreakLimitTypeAndEnterHigherBreakLimitValue(BreakHigherLimitType,HigherBreakLimitValue);
	}
	
	@Then("User clicks on Save button")
	public void userClicksOnSaveButton() throws InterruptedException {
	Slabs.userClicksOnSaveButton();
	
	}
	
	@Then ("User validates successful msg and clicks on Ok {string}")
	public void userValidatesSuccessfulMsgAndClicksOnOk(String Successmsg) throws InterruptedException {
    Slabs.userValidatesSuccessfulMsgAndClicksOnOk(Successmsg);
	
	}
	
	@Then ("User clicks on plus sign")
    public void userClicksOnPlusSign() throws InterruptedException {
    Slabs.userClicksOnPlusSign();
    
    }	
    @Then ("User selects higher break limit type {string} for second row and enter higher break limit value {string}")
    public void userSelectsHigherBreakLimitTypeForSecondRowAndEnterHigherBreakLimitValue(String BreakHigherLimitType1,String HigherBreakLimitValue1) throws InterruptedException {
    Slabs.userSelectsHigherBreakLimitTypeForSecondRowAndEnterHigherBreakLimitValue(BreakHigherLimitType1,HigherBreakLimitValue1);

}
   @Then ("User clicks on Inactive status button and clicks on Ok button")
   public void userClicksOnInactiveStatusButtonAndClicksOnOkButton() throws InterruptedException {
	Slabs.userClicksOnInactiveStatusButtonAndClicksOnOkButton();

}
   
   @Then ("User validates activation successful msg and clicks on Ok {string}")
	public void userValidatesActivationSuccessfulMsgAndClickOnOk(String ActivationSuccessmsg) throws InterruptedException {
	   Slabs.userValidatesActivationSuccessfulMsgAndClickOnOk(ActivationSuccessmsg);

   }

   @Then ("User enters {string} in the text field and user selects Slab Type Weight and Weight Type Billable Weight")
   public void userEntersInTheTextFieldAndUserSelectsSlabTypeWeightAndWeightTypeBillableWeight(String SlabName1) throws InterruptedException {
   Slabs.userEntersInTheTextFieldAndUserSelectsSlabTypeWeightAndWeightTypeBillableWeight(SlabName1);

}
   @Then ("User clicks on filter option")
   public void userClicksOnFilterOption() throws InterruptedException {
   Slabs.userClicksOnFilterOption();
   }
  @Then ("User enters {string} in text field and clicks on Search button")
  public void userEntersInTextFieldAndClicksOnSearchButton(String slabsname) throws InterruptedException {
	  Slabs.userEntersInTextFieldAndClicksOnSearchButton(slabsname);
  }
  @Then ("User clicks on View button")
  public void userClicksOnViewButton() throws InterruptedException {
	  Slabs.userClicksOnViewButton();
  }
  
 @Then ("User verifies Blank Slab error msg {string}")
  public void userVerifiesBlankSlabErrorMsg(String BlankSlabErrorMsg) throws InterruptedException {
	  Slabs.userVerifiesBlankSlabErrorMsg(BlankSlabErrorMsg); 
  }
  
  @Then ("User verifies Blank Slab Type error msg {string}")
  public void userVerifiesBlankSlabTypeErrorMsg(String BlankSlabTypeErrorMsg) throws InterruptedException {
	  Slabs.userVerifiesBlankSlabTypeErrorMsg(BlankSlabTypeErrorMsg);
  }
   @Then ("User enters {string} in text field")
   public void userEntersInTextField(String Slabsname1) throws InterruptedException {
	   Slabs.userEntersInTextField(Slabsname1);
  
}
 
  @Then ("User clicks on Search button")
public void userClicksOnSearchButton() throws InterruptedException {
	  Slabs.userClicksOnSearchButton();
} 
  
  @Then ("User verifies msg {string}")
  public void userVerifiesMsg(String msg) throws InterruptedException {
	  Slabs.userVerifiesMsg(msg);
  }
  @Then ("User enters {string} in the text field and user selects Slab Type Quantity")
  public void userEntersInTheTextFieldAndUserSelectsSlabTypeQuantity(String SlabName2) throws InterruptedException {
  Slabs.userEntersInTheTextFieldAndUserSelectsSlabTypeQuantity(SlabName2);
  
  
} 
  
  @Then ("User clicks on Ok button")
  public void userClicksOnOkButton() throws InterruptedException {
	  Slabs.userClicksOnOkButton();
  
}
  @Then ("User clicks on slab delete button and verifies msg {string}")
  public void userClicksOnSlabDeleteButtonAndVerifiesMsg(String Msg1) throws InterruptedException {
	  Slabs.userClicksOnSlabDeleteButtonAndVerifiesMsg(Msg1);
  }
  @Then ("User enters {string} in the text field and user selects Slab Type Distance")
  public void userEntersInTheTextFieldAndUserSelectsSlabTypeDistance(String SlabName3) throws InterruptedException {
  Slabs.userEntersInTheTextFieldAndUserSelectsSlabTypeDistance(SlabName3);
  
}
 @Then ("User clicks on Cancel button") 
 public void userClicksOnCancelButton() throws InterruptedException {
	 Slabs.userClicksOnCancelButton();
 
 }
 
}