package starter.stepdefinitions;

import com.xb.cafe.ui.pages.Trip_sheet_enhancement_FirstMile;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.webdriver.SupportedWebDriver;

public class Trip_sheet_enhancement_FirstMile_StepDef {
	
	@Steps
	Trip_sheet_enhancement_FirstMile trip_sheet_enhancement_FirstMile;
	
	@Then("Capture the AWBNumber from Rapid Booking")
	public void capturetheAWBNumberfromRapidBooking() throws InterruptedException{
		trip_sheet_enhancement_FirstMile.capturetheAWBNumberfromRapidBooking();
	}
	
	@When("User enters Invoice and GST details less then 50K {string}")
	public void userentersInvoiceandGSTdetailslessthen50K(String invoiceValue) throws InterruptedException{
		trip_sheet_enhancement_FirstMile.userentersInvoiceandGSTdetailslessthen50K(invoiceValue);
	}
	
	@Then("User clicks on Save and Next button to complete Booking")
	public void userclicksonSaveandNextbuttontocompleteBooking() throws InterruptedException{
		trip_sheet_enhancement_FirstMile.userclicksonSaveandNextbuttontocompleteBooking();
	}
	
	@Then("User clicks on Trip Sheet Button")
	public void userclicksonTripSheetButton() throws InterruptedException{
		trip_sheet_enhancement_FirstMile.userclicksonTripSheetButton();
	}
	
	@And("User validate the assigned records to SR {string},{string},{string},{string},{string},{string},{string},{string}") 
		public void UservalidatetheassignedrecordstoSR(String vehicleNumber,String Assignedto,String ODOReading,String clientName,String Address,String pickupPincode,String NumberMPS,String TotalShipmentDeclaredWeight) throws InterruptedException{
			trip_sheet_enhancement_FirstMile.UservalidatetheassignedrecordstoSR(vehicleNumber,Assignedto,ODOReading,clientName,Address,pickupPincode,NumberMPS,TotalShipmentDeclaredWeight);
	}
}
