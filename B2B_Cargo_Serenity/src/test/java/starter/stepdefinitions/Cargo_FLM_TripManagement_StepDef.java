package starter.stepdefinitions;

import java.sql.SQLException;

import org.testng.asserts.SoftAssert;

import com.apis.BookingAPI;
import com.apis.Hubops_APIs;
import com.apis.MobileDeliveryAPI;
import com.apis.MobileDeliveryAPI_Multiple;
import com.apis.MobileDeliveryAPIs_MultipleAWBs;
import com.apis.MobilePickupAPIs_MultipleAWBs;
import com.apis.MobilePickupApis;
import com.xb.cafe.ui.pages.Cargo_FLM_TripManagement;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.webdriver.SupportedWebDriver;

public class Cargo_FLM_TripManagement_StepDef  {
	
	@Managed
	SupportedWebDriver browser;
	
	
	@Steps
	Cargo_FLM_TripManagement cargo;
	
	@Steps
	MobilePickupApis mobilePickupApi;
	
	@Steps
	MobilePickupAPIs_MultipleAWBs mobilePickupAPIs_MultipleAWBs;
	
	@Steps
	MobileDeliveryAPIs_MultipleAWBs mobileDeliveryAPIs_MultipleAWBs;
	
	
//	@Steps
//	MobileDeliveryAPI mobileDeliveryAPI;
	@Steps
	MobileDeliveryAPI_Multiple mobileDeliveryAPI;
	SoftAssert softassert = new SoftAssert();
	
	@Steps
	Hubops_APIs hubops_APIs;
	
	@Given("User changes hub to Pickup hub {string}")
	public void userChangesHubToPickUpHub(String pickUpHub) throws InterruptedException {
		cargo.userChangesHubToPickUpHub(pickUpHub);
	}
	@Given("User Navigates to Trip Management")
	public void userNavigatesToTripManagement() throws InterruptedException {
		cargo.userNavigatesToTripManagement();
	}
	
	@Given("User Navigates to Create Booking Trip Tab")
	public void userNavigatesToCreateBookingTripTab() throws InterruptedException {
		cargo.userNavigatesToCreateBookingTripTab();
	}
	
	@Given("User Enters Assignment Person and AD hoc Vehicle Details {string},{string},{string},{string}")
	public void userEntersAssignmentPersonAndAdHocVehicleDetails(String assignedTo, String adHocVehicleModel, String adHocVehicleNumber, String adHocVehicleType) throws InterruptedException {
		cargo.userEntersAssignmentPersonAndAdHocVehicleDetails(assignedTo, adHocVehicleModel, adHocVehicleNumber, adHocVehicleType);
	}
	@Given("User Enters Assignment Person and AD hoc Vehicle Details LM {string},{string},{string},{string}")
	public void userEntersAssignmentPersonAndAdHocVehicleDetailsLastMile(String assignedTo, String adHocVehicleModel, String adHocVehicleNumber, String adHocVehicleType) throws InterruptedException {
		cargo.userEntersAssignmentPersonAndAdHocVehicleDetailsLastMile(assignedTo, adHocVehicleModel, adHocVehicleNumber, adHocVehicleType);
	}
	
	@When("User Enters Details In AddShipment Tab One AWB {string}")
	public void userEntersDetailsInAddShipmentTabOneAWB(String aWBNumber) throws InterruptedException {
		cargo.userEntersDetailsInAddShipmentTabOneAWB(aWBNumber);
	}
	
	@When("User Assigns AWBNumber To Trip")
	public void userAssignsAWBNumberToTrip() throws InterruptedException {
		cargo.userAssignsAWBNumberToTrip();
	}
	
	@Then("User Enters ODO Reading {string}")
	public void userEntersODOReading(String oDOReading) throws InterruptedException {
		cargo.userEntersODOReading(oDOReading);
	}
	
	@Then("User Scans AWBNumber & ValidateSucessMessage {string},{string}")
	public void userScansAWBNumberAndValidateSucessMessage(String awbNumber, String message) throws InterruptedException {
		cargo.userScansAWBNumberAndValidateSucessMessage(awbNumber, message);
	}
	@Then("Validate Trip created details {string},{string},{string}")
	public void validateTripCreatedDetails(String aWBNumber, String assignedTo,String oDOReading) throws InterruptedException {
		cargo.validateTripCreatedDetails(aWBNumber, assignedTo, oDOReading);
	}
	
	@When("User Enters Assignment Person and contracted Vehicle Details {string},{string}")
	public void userEntersAssignementPersonAndContractedVehicleDetails(String assignedTo,String contractedVehicleNumber) throws InterruptedException {
		cargo.userEntersAssignementPersonAndContractedVehicleDetails(assignedTo, contractedVehicleNumber);
	}
	
	@When("User Enters Details In AddShipment Tab Three AWB {string},{string},{string}")
	public void userEntersDetailsInAddShipmentTabThreeAWB(String aWBNumber1,String aWBNumber2,String aWBNumber3) throws InterruptedException {
		cargo.userEntersDetailsInAddShipmentTabThreeAWB(aWBNumber1, aWBNumber2, aWBNumber3);
	}
	
	@Then("User Scans Multiple AWBNumber & ValidateSucessMessage {string},{string},{string}")
	public void userScansMultipleAWBNumberAndValidateSuccessMessage(String aWBNumber1,String aWBNumber2, String message) throws InterruptedException {
		cargo.userScansMultipleAWBNumberAndValidateSuccessMessage(aWBNumber1, aWBNumber2, message);
	}
	@Then("Capture the AWBNumber Generated")
	public void captureTheAWBNumberGenerated() {
		cargo.captureTheAWBNumberGenerated();
	}
	
	@When("Userr enters Invoice and GST details Dynamic FLM {string}")
	public void userEnterInvoiceDetailsDynamic( String invoiceValue)throws InterruptedException {
		cargo.userEntersInvoiceAndGSTDetailsBookingDynamic(invoiceValue);	
	}
	@When("User Enters Details In AddShipment Tab Booking Way")
	public void userEntersDetailsInShipmentTabBookingWay() throws InterruptedException {
		cargo.userEntersDetailsInShipmentTabBookingWay();
	}
	@Then("User Scans AWBNumber & ValidateSucessMessage Booking Way{string}")
	public void userScansAWBNumberAndValidateSuccessMessageBookingWay(String message) throws InterruptedException {
		cargo.userScansAWBNumberAndValidateSuccessMessageBookingWay(message);
	}
	@Then("Validate Trip created details Booking Way {string},{string}")
	public void validateTripCreatedDetailsBookingWay(String assignedTo,String oDOReading) throws InterruptedException {
		cargo.validateTripCreatedDetailsBookingWay(assignedTo, oDOReading);
	}
	@Then("Validate Trip created details Multiple {string},{string},{string},{string}")
	public void validateTripCreatedDetailsMultiple(String aWBNumber1,String aWBNumber2,String assignedTo,String oDOReading) throws InterruptedException {
		cargo.validateTripCreatedDetailsMultiple(aWBNumber1, aWBNumber2, assignedTo, oDOReading);
	}
	
	@Given("User opens the Trip details {string},{string}")
	public void userOpensTheTripDetails(String tripStatus, String baSR) throws InterruptedException {
		cargo.userOpensTheTripDetails(tripStatus, baSR);
	}
	@When("User Validates Trip Details {string},{string}")
	public void userValidatesTripDetails(String AWBNumber,String mpsNumber) {
		cargo.userValidatesTripDetails(AWBNumber, mpsNumber);
	
	}
	
    @When("User Navigates To Close Trip and Scans AWBs {string},{string}")
	public void userNavigatesToCloseTripButtonAndScansAWB(String AWBNumber, String numberOfPhysicalINvoiceInput) throws InterruptedException {
    	cargo.userNavigatesToCloseTripButtonAndScansAWB(AWBNumber, numberOfPhysicalINvoiceInput);
	}
    @Then("user Enters closure ODO reading {string}")
    public void userEntersClosureODOReading(String vehicleCloseODOReading) throws InterruptedException{
    	cargo.userEntersClosureODOReading(vehicleCloseODOReading);
    }
    @Then("User closes trip and validates trip closure message{string}")
    public void userClosesTripAndValidateTripClosureMessage(String message) throws InterruptedException {
    	cargo.userClosesTripAndValidateTripClosureMessage(message);
    }
    @When("User Validates Trip Details Multiple {string},{string},{string},{string},{string},{string}")
	public void userValidatesTripDetailsMultiple(String AWBNumber1,String mpsNumber1,String AWBNumber2,String mpsNumber2,String AWBNumber3,String mpsNumber3) {
		cargo.userValidatesTripDetailsMultiple(AWBNumber1, mpsNumber1, AWBNumber2, mpsNumber2, AWBNumber3, mpsNumber3);
	
	}
    @When("User Navigates To Close Trip and Scans AWBs Multiple {string},{string},{string},{string},{string},{string},{string},{string},{string}")
    public void userNavigatesToCloseTripButtonAndScansAWBMultiple(String AWBNumber1,String mpsNumber1, String numberOfPhysicalINvoiceInput1,String AWBNumber2,String mpsNumber2,String numberOfPhysicalINvoiceInput2,String AWBNumber3, String mpsNumber3,String numberOfPhysicalINvoiceInput3) throws InterruptedException {
    	cargo.userNavigatesToCloseTripButtonAndScansAWBMultiple(AWBNumber1, mpsNumber1, numberOfPhysicalINvoiceInput1, AWBNumber2, mpsNumber2, numberOfPhysicalINvoiceInput2, AWBNumber3, mpsNumber3, numberOfPhysicalINvoiceInput3);
	}
    
    @Then("User will pick up the mps from Hub")
    public void userWillPickUp() {
    	mobilePickupApi.mobileApis();
    	
    }
    
    @Then("validate the status {string}")
    public void validateTheStatus(String tripStatus) throws InterruptedException {
    	cargo.validateTripStatus(tripStatus);
    	
    }
    
    @Then("User Navigates To Close Trip and Scans AWBs Booking way {string}")
    public void userNavigatesToCloseTripButtonAndScansAWBBookingWay(String numberOfPhysicalINvoiceInput) throws InterruptedException {
    	cargo.userNavigatesToCloseTripButtonAndScansAWBBookingWay(numberOfPhysicalINvoiceInput);
	}
    
    @When("User Enters Details In AddShipment Tab Booking Way through filter")
    public void UserEntersDetailsInAddShipmentTabBookingWaythroughfilter() throws InterruptedException {
    	cargo.userAddsShipmentDetailsFilter();
    }
    
    @When("User clicks on save&next button")
    public void userClicksOnSaveButtonCreateTripTab() throws InterruptedException {
    	cargo.userClicksOnSaveButtonCreateTripTab();
    }
    @Then("validate the error messages {string}, {string}")
    public  void validateErrorMessages(String errorAssignedTo, String errorContractedError) {
    	cargo.validateErrorMessages(errorAssignedTo, errorContractedError);
    }
    @Given("User closes trip from DB if trip is started already {string}")
    public void userClosesTripFormDB(String baSR) throws InterruptedException, SQLException, ClassNotFoundException {
    	cargo.userClosesTripFormDB(baSR);
    }
    
    @When("user Clicks on assignAWB and RemoveAWB buttons")
    public void userClicksOnAssignAWBandRemoveAWBButton() {
    	cargo.userClicksOnAssignAWBandRemoveAWBButton();
    }
    @Then("validate the error message {string}")
    public void validateErrorDetailsAssignAndRemoveTrip(String errorMessage) {
    	cargo.validateErrorDetailsAssignAndRemoveTrip(errorMessage);
    }
    @When("User clicks on Start button without enterung odo reading")
    public void userClicksOnStartButtonWithoutEnteringODO() {
    	cargo.userClicksOnStartButtonWithoutEnteringODO();
    }
    @Then("Validate error message ODO reading {string}")
    public void validateErrorMessage(String errorMessage) {
    	cargo.validateErrorMessage(errorMessage);
    }
    @Given("User hits awb verify api")
    public void userHitsAWBVerifyAPI() {
    	mobileDeliveryAPI.awbVerifyAPIHit();
    }
    @Given("User hits awb verify api Multiple")
    public void userHitsAWBVerifyAPIMultiple() {
    	mobileDeliveryAPIs_MultipleAWBs.awbverifyAPIMultiple();
    }
    @When("User Navigates to create trip and Starts the Trip")
    public void userNavigatesToCreateTripAndStartsTheTrip() throws InterruptedException {
    	cargo.userNavigatesToCreateTripAndStartsTheTrip();
    }
    @When("User hits delivery apis")
    public void userHitsDeliveryApis() {
    	mobileDeliveryAPI.deliveryAPIshit();
    }
    @Then("user navigates to close trip")
    public void userNavigatesToCloseTrip() throws InterruptedException {
    	cargo.userNavigatesToCloseTrip();
    }
    @Then("Validate the Vehicle capacity {string}")
    public void validateTheVehicleCapacity(String vehicleCapacity) {
    	cargo.validateTheVehicleCapacity(vehicleCapacity);
    }
    
    @When("User Assigns AWBNumber To Trip Overload")
    public void userAssignsAWBNumberToTripOverload() throws InterruptedException {
    	cargo.userAssignsAWBNumberToTripOverload();
    }
    @Then("Validate Error message Overload {string}")
    public void validateErrorMessageOverload(String errorMessage) {
    	cargo.validateErrorMessages(errorMessage, errorMessage);
    }
    @When("User hits Hubops API")
    public void userHitsHubOpsAPI() {
    	hubops_APIs.hubOpsAPIsHit();
    }
    
    @When("User generates number of AWB {int} for pincode {string}")
	public void user_generates_number_of_awb_for_pincode(Integer noofawb, String pincode) {
		softassert.assertEquals(BookingAPI.generateAwbNumbersOnStage(noofawb,pincode), true);
	}
    
	@When("^User Enters Details In AddShipment Tab AWB$")
    public void user_enters_details_in_addshipment_tab_awb() throws Throwable {
		cargo.userEntersDetailsInAddShipmentTabAWB();
    }
	
	@And("User Scans all AWBNumber & ValidateSucessMessage {string}")
    public void userScansAllAWBNumberAndValidateSucessMessage(String message) throws Throwable {
		cargo.userScansAllAWBNumberAndValidateSucessMessage(message);
    }
	 @Then("search for and close the first mile trip")
		public void search_for_and_close_the_first_mile_trip() throws InterruptedException {
		 cargo.CloseTheAssignedTrips();
	}
	 
	 @And("user Click on Last mile tab and enter multiple AWB and start the Last mile trip")
	    public void user_click_on_last_mile_tab_and_enter_multiple_awb_and_start_the_last_mile_trip() throws Throwable {
		 cargo.userClickOnLastMileTabAndEnterMultipleAwbStartTheLastMileTrip();
	    }

	 @Then("search for assigned trip and click on view trip then click on close trip button")
	    public void search_for_assigned_trip_and_click_on_view_trip_then_click_on_close_trip_button() throws Throwable {
		 cargo.searchForAssignedTripAndClickOnViewTripThenClickOnCloseTripButton();
	    }
	 @When("User will pick up the mps from Hub MultipleAWb")
	 public void userWillPickUPTheMPSFromHubMultipleAWBS() {
		 mobilePickupAPIs_MultipleAWBs.mobileApis_Multiple();
	 }
	 @Then("User hits delivery APIs MultipleAWBs")
	 public void userhitsDeliveryAPIsMultipleAWBs() {
		 mobileDeliveryAPIs_MultipleAWBs.deliveryAPIshitMultiple();
	 }
}
    
    
    
    

