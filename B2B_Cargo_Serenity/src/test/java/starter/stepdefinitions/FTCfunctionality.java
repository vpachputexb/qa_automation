package starter.stepdefinitions;


import com.xb.cafe.ui.pages.FTCcargo;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.webdriver.SupportedWebDriver;

public class FTCfunctionality {
	@Steps
	FTCcargo ftccargo;

	@Managed
	SupportedWebDriver browser;
	
	@Given("User navigates to FTC")
	public void userNavigatesToFTC() throws InterruptedException {
		ftccargo.userNavigatesToFTC();
	}
	@Then("User clicks on the calculate FTC and validate the click functionality {string}")
	public void userClicksOnTheCalculateFTCAndValidateTheClickFunctionality(String calculateFTC)throws InterruptedException {
		ftccargo.userClicksOnTheCalculateFTCAndValidateTheClickFunctionality(calculateFTC);
	}
	@Then("User clicks on the send payment link button and validate the send button functionality")
	public void userClicksOnTheSendPaymentLinkButtonAndValidateTheSendButtonFunctionality()throws InterruptedException {
		ftccargo.userClicksOnTheSendPaymentLinkButtonAndValidateTheSendButtonFunctionality();
	}
	@Then("User clicks on the print cash memo button and validate the cash memo {string}")
	public void userClicksOnThePrintCashMemoButtonAndValidateTheCashMemo(String cashMemoMessage)throws InterruptedException {
		ftccargo.userClicksOnThePrintCashMemoButtonAndValidateTheCashMemo(cashMemoMessage);
	}
	@When("User clicks on calculate FTC")
	public void userClicksOnCalculateFTC()throws InterruptedException {
		ftccargo.userClicksOnCalculateFTC();
	}
	@Then("User clicks on the print cash memo button and clicks on print button")
	public void userClicksOnThePrintCashMemoButtonAndClicksOnPrintButton()throws InterruptedException {
		ftccargo.userClicksOnThePrintCashMemoButtonAndClicksOnPrintButton();
	}
	@Then("Validate the fields {string},{string},{string},{string},{string},{string},{string},{string}")
	public void validateTheFields(String awbMessage, String totalNoOfMps, String originCity, String destinationCity, String pickupDate, String radDate,String consignorName, String consigneeName)throws InterruptedException {
		ftccargo.validateTheFields(awbMessage,totalNoOfMps,originCity,destinationCity,pickupDate,radDate,consignorName,consigneeName);
	}
	@When("User Enters {string},{string},{string},{string},{string}")
	public void userEnters(String awbNumber, String clientName, String radFromDate, String radToDate, String cashMemo)throws InterruptedException {
		ftccargo.userEnters(awbNumber, clientName, radFromDate, radToDate, cashMemo);

	}
	@Then("Click on search and Validate the button functionality")
	public void clickOnSearchAndValidateTheButtonFunctionality()throws InterruptedException {
		ftccargo.clickOnSearchAndValidateTheButtonFunctionality();
	}
	@When("User clicks on recalculate button")
	public void userClicksOnRecalculateButton()throws InterruptedException {
		ftccargo.userClicksOnRecalculateButton();
	}
@Then("Click on confirm FTC button and validate the confirmation message {string}")
public void clickOnConfirmFTCButtonAndValidateTheConfirmationMessage(String actionConfirmation)throws InterruptedException {
	ftccargo.clickOnConfirmFTCButtonAndValidateTheConfirmationMessage(actionConfirmation);
	
}
@Then("User clicks on add extra charges and enters the Add Extra Charges Details {string},{string},{string},{string},{string},{string},{string},{string}")
public void userClicksOnAddExtraCharges(String shipmentSizedCharges,String varaiCharges,String portgagesCharges,String specialDeliveryCharges,String loadingUnloadingCharges,String odaCharges,String fullTruckLoad,String successmessage)throws InterruptedException {
	ftccargo.userClicksOnAddExtraCharges(shipmentSizedCharges,varaiCharges,portgagesCharges,specialDeliveryCharges,loadingUnloadingCharges,odaCharges,fullTruckLoad,successmessage);
}
@Then("User clicks on freight charges tab and validate {string},{string}")
public void userClicksOnFreightChargesTabAndValidate(String weightsMessage,String chargeCalculationMessage)throws InterruptedException {
	ftccargo.userClicksOnFreightChargesTabAndValidate(weightsMessage,chargeCalculationMessage);
}
@Then("User clicks on insurance charges tab and validate {string}")
public void userClicksOnInsuranceChargesTabAndValidate(String applicableChargemessage)throws InterruptedException {
	ftccargo.userClicksOnInsuranceChargesTabAndValidate(applicableChargemessage);
}
@Then("User clicks on accessorial charges tab and validate {string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string},{string}")
public void userClicksOnAccessorialChargesTabAndValidate(String waybillChargesMessage,String dcChargesMessage,String ftcChargesMessage,String fuelSurchargeMessage,String deliveryAttemptChargesMessage,String demurrageChargesMessage,String appointmentVehicleChargesMessage,String loadingChargesMessage,String pickupDeliveryChargesMessage,String handlingChargesMessage,String groundFloorChargesMessage,String greenTaxMessage,String rtoChargesMessage,String odaChargesMessage,String holidayDeliveryChargesMessage)throws InterruptedException {
	ftccargo.userClicksOnAccessorialChargesTabAndValidate(waybillChargesMessage,dcChargesMessage,ftcChargesMessage,fuelSurchargeMessage,deliveryAttemptChargesMessage,demurrageChargesMessage,appointmentVehicleChargesMessage,loadingChargesMessage,pickupDeliveryChargesMessage,handlingChargesMessage,groundFloorChargesMessage,greenTaxMessage,rtoChargesMessage,odaChargesMessage,holidayDeliveryChargesMessage);
}
@When("validate FTC entry UI {string}")
public void validateFTCEntryUI(String ftcRadShipments)throws InterruptedException {
	ftccargo.validateFTCEntryUI(ftcRadShipments);
	
}
@Then("validate the table entry details {string},{string},{string},{string},{string},{string},{string},{string},{string},{string}")
public void validateTheTableEntryDetails(String srNo,String awb,String mpsCo,String clientName,String consigneeName,String consigneeAddress,String radDate,String cashMemoGenerate,String paymentStatus,String actions)throws InterruptedException {
	ftccargo.validateTheTableEntryDetails(srNo,awb,mpsCo,clientName,consigneeName,consigneeAddress,radDate,cashMemoGenerate,paymentStatus,actions);
}
}
