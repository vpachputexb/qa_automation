package starter.stepdefinitions;

import com.xb.cafe.ui.pages.FileUploadNew;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.webdriver.SupportedWebDriver;

public class NewfileUpload {
	@Steps
	FileUploadNew fileuploadnew;
	
	@Managed
	SupportedWebDriver browser;
	
	@Given("Navigate to Cargo Booking Menu")
	public void navigateToCargoBookingMenu()throws InterruptedException {
		fileuploadnew.navigateToCargoBookingMenu();
	}
	@When("User navigates to Create booking option and click on data upload button")
	public void userNavigatesToCreateBoolingOptionAndClickOnDataUploadButton()throws InterruptedException {
		fileuploadnew.userNavigatesToCreateBoolingOptionAndClickOnDataUploadButton();
		
	}
	@When("Select the client {string}")
	public void SelectTheClient(String clientName)throws InterruptedException {
		fileuploadnew.SelectTheClient(clientName);
	}
	@When("Select the {string} and click on submit button")
	public void SelectTheAndClickOnSubmitButton(String filename)throws InterruptedException {
		fileuploadnew.SelectTheAndClickOnSubmitButton(filename);
	}
	@Then("Validate the upload details section {string},{string}")
	public void validateTheUploadDetailsSection(String totalRecords,String validRecords)throws InterruptedException {
		fileuploadnew.validateTheUploadDetailsSection(totalRecords,validRecords);
	}
	@Then("click on download template button and validate")
	public void clickOnDownloadTemplateButtonAndValidate()throws InterruptedException {
		fileuploadnew.clickOnDownloadTemplateButtonAndValidate();
	}
	@Then("click on reset button and validate")
	public void clickOnResetButtonAndValidate()throws InterruptedException {
		fileuploadnew.clickOnResetButtonAndValidate();
	}
	@Then("Validate the elements present in data upload page {string},{string},{string},{string}")
	public void validateTheElementsPresent(String client,String chooseFile,String submit,String reset)throws InterruptedException {
		fileuploadnew.validateTheElementsPresent(client,chooseFile,submit,reset);
	}
	@When("User selects invalid file format {string} and click on submit button")
	public void userSelectsInvalidFileFormatAndClickOnSubmitButton(String file)throws InterruptedException {
		fileuploadnew.userSelectsInvalidFileFormatAndClickOnSubmitButton(file);
		
	}
	@Then("Validate the error message {string}")
	public void validateTheErrorMessage(String uploadFormatErrorMessage)throws InterruptedException {
		fileuploadnew.validateTheErrorMessage(uploadFormatErrorMessage);
	}
	@Then("Validate the upload details grid elements {string},{string},{string}")
	public void validateTheUploadDetailsSection(String totalRecords,String validRecords,String invalidRecords)throws InterruptedException {
		fileuploadnew.validateTheUploadDetailsSection(totalRecords,validRecords,invalidRecords);
	}
	@When("Click on submit button")
	public void clickOnSubmitButton()throws InterruptedException {
		fileuploadnew.clickOnSubmitButton();
	}
	@Then("Validate the error messages {string},{string}")
	public void validateTheErrorMessages(String clientError,String fileError)throws InterruptedException {
		fileuploadnew.validateTheErrorMessages(clientError,fileError);
	}
	@Then("User clicks on invalid file link and validate the message {string}")
	public void userClicksOnInvalidFileLinkAndValidateTheMessage(String invalidFileErrorMessage)throws InterruptedException {
		fileuploadnew.userClicksOnInvalidFileLinkAndValidateTheMessage(invalidFileErrorMessage);
	}
	@Then("Select the excel file {string} and click on submit button")
	public void selectTheExcelFileAndClickOnSubmitButton(String excelFile)throws InterruptedException {
		fileuploadnew.selectTheExcelFileAndClickOnSubmitButton(excelFile);
		
	}
	@Then("Validate the file header error message {string}")
	public void validateTheFileHeaderErrorMessage(String fileHeaderMessage)throws InterruptedException {
		fileuploadnew.validateTheFileHeaderErrorMessage(fileHeaderMessage);
	}
	@When("Select the without seller id file {string} and click on submit button")
	public void selectTheWithoutSellerIdFileAndClickOnSubmitButton(String withoutSellerIdFile)throws InterruptedException {
		fileuploadnew.selectTheWithoutSellerIdFileAndClickOnSubmitButton(withoutSellerIdFile);
	}
	@When("Click on the import records button and click on ok button")
	public void clickOnTheImportRecordsButtonAndClickOnOkButton()throws InterruptedException {
		fileuploadnew.clickOnTheImportRecordsButtonAndClickOnOkButton();
		}
	@Then("Validate the Po error message {string}")
	public void validateThePoErrorMessage(String poErrorMessage)throws InterruptedException {
		fileuploadnew.validateThePoErrorMessage(poErrorMessage);
	}
}
	
