package starter.stepdefinitions;



import java.io.IOException;

import com.xb.cafe.ui.pages.TnCClient;
import com.xb.cafe.ui.pages.TnCClient;
import com.xb.cafe.ui.pages.TnCClient;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class TnCClient_StepDef {

//	@Given("I login in Cargo Web UI with following credentials")
//	public void cargo_Login(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
//		Cargo.cargoLogin(dataTable);
//	}
	@Steps
	TnCClient TnCClient;
	
	@Given ("User Navigates to Client management option TnC option")
	public void userNavigatesToClientManagementOptionTnCOption() throws InterruptedException {
		TnCClient.userNavigatesToClientManagementOptionTnCOption();
	}
    @When ("User clicks on create new TnC option")
    public void userClicksOnCreateNewTnCOption() throws InterruptedException {
    	TnCClient.userClicksOnCreateNewTnCOption();
    }
  @Then ("User validates msg and clicks on Ok {string}")
  public void userValidatesMsgAndClicksOnOk(String Msg) throws InterruptedException {
	  TnCClient.userValidatesMsgAndClicksOnOk(Msg);
  }
  
  @Then ("User validates SuccessmsgTnC and clicks on Ok {string}")
  public void userValidatesSuccessmsgTnCAndClicksOnOk(String SuccessmsgTnC) throws InterruptedException {
	  TnCClient.userValidatesSuccessmsgTnCAndClicksOnOk(SuccessmsgTnC);
  }
  @Then ("User selects new TnC template {string}")
  public void userSelectsNewTnCTemplate(String TnCtemplate) throws InterruptedException {
	  TnCClient.userSelectsNewTnCTemplate(TnCtemplate);
  }
  @Then ("User validates msg1 and clicks on Ok {string}")
  public void userValidatesMsg1AndClicksOnOk(String Msg1) throws InterruptedException {
	  TnCClient.userValidatesMsg1AndClicksOnOk(Msg1);
  }
  
  @Then ("User validates msg2 and clicks on Ok {string}")
  public void userValidatesMsg2AndClicksOnOk(String Msg2) throws InterruptedException {
	  TnCClient.userValidatesMsg2AndClicksOnOk(Msg2);
  }
  @Then ("User selects TnC template {string}")
  public void userSelectsTnCTemplate(String NewTnCTemplate) throws InterruptedException {
	  TnCClient.userSelectsTnCTemplate(NewTnCTemplate);
   }
  @Then ("User clicks on Edit button1")
  public void userClicksOnEditButton1() throws InterruptedException {
	  TnCClient.userClicksOnEditButton1();
	  }
  @Then ("User validates TnC error msgs {string}, {string}, {string}")
  public void userValidatesTnCErrorMsgs(String Companynameerrormsg1, String Contractiderrormsg1, String TnCTemplateerrormsg) throws InterruptedException {
	  TnCClient.userValidatesTnCErrorMsgs(Companynameerrormsg1, Contractiderrormsg1, TnCTemplateerrormsg);
  }
}
   