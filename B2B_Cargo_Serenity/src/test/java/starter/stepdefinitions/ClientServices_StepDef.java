package starter.stepdefinitions;



import java.io.IOException;

import com.xb.cafe.ui.pages.ClientServices;
import com.xb.cafe.ui.pages.Tariffcard;
import com.xb.cafe.ui.pages.TnC;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ClientServices_StepDef {

//	@Given("I login in Cargo Web UI with following credentials")
//	public void cargo_Login(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
//		Cargo.cargoLogin(dataTable);
//	}
	@Steps
	ClientServices ClientServices;
	
	@Given("User Navigates to Services option")
	public void userNavigatesToServicesOption() throws InterruptedException {
		ClientServices.userNavigatesToServicesOption();
	}
	@Then("Click on edit services")
	public void clickOnEditServices() throws InterruptedException {
		ClientServices.clickOnEditServices();
	}
	@Then("User validates SuccessmsgServices and clicks on Ok {string}")
	public void userValidatesSuccessmsgServicesAndClicksOnOk(String SuccessmsgServices) throws InterruptedException {
		ClientServices.userValidatesSuccessmsgServicesAndClicksOnOk(SuccessmsgServices);
	}
	
	@Then("click on PTL tab")
	public void clickOnPTLTab() throws InterruptedException {
		ClientServices.clickOnPTLTab();
	}
	
	@Then("select service level template from drop down {string}")
	public void selectServiceLevelTemplateFromDropDown(String SLTemplate) throws InterruptedException {
		ClientServices.selectServiceLevelTemplateFromDropDown(SLTemplate);
	}
	
	@Then("Select Upload file option")
	public void selectUploadFileOption() throws InterruptedException {
		ClientServices.selectUploadFileOption();
	}
	
	@Then("choose a SL file {string}")
	public void chooseASLFile(String SLfile) throws InterruptedException {
		ClientServices.chooseASLFile(SLfile);
	}
	@Then("click on validate file button")
	public void clickOnValidateFileButton() throws InterruptedException {
		ClientServices.clickOnValidateFileButton();		
		}
		
	@Then("select Route mode and Booking type")
	public void selectRouteModeAndBookingType()	throws InterruptedException {
		ClientServices.selectRouteModeAndBookingType();
	}
	@Then("validate SL error msgs {string}, {string}, {string}, {string}")
	public void validateSLErrorMsgs(String Mandatoryfields, String Greaterthan0, String Integer, String TAT) throws InterruptedException {
		ClientServices.validateSLErrorMsgs(Mandatoryfields, Greaterthan0, Integer, TAT);
	}
	
	@Then("choose a WrongSL file {string}")
	public void chooseAWrongSLFile(String WrongSLfile) throws InterruptedException {
		ClientServices.chooseAWrongSLFile(WrongSLfile);
		
	}
	@Then("choose a Surface SL file {string}")
	public void chooseASurfaceSLFile(String SurfaceSLfile) throws InterruptedException {
		ClientServices.chooseASurfaceSLFile(SurfaceSLfile);
	}
	
	@Then("select Route mode and multiple Booking types")
	public void selectRouteModeAndMultipleBookingTypes() throws InterruptedException {
		ClientServices.selectRouteModeAndMultipleBookingTypes();
	}
	@Then("click on Download Sales TAT")
	public void clickOnDownloadSalesTAT() throws InterruptedException {
		ClientServices.clickOnDownloadSalesTAT();
		
		}
	@Then("validate validation error {string}")
	public void validateValidationError(String Validationerror) throws InterruptedException {
		ClientServices.validateValidationError(Validationerror);
		
		}
	
	
	
	}