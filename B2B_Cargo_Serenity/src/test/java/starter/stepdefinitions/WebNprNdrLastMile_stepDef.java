package starter.stepdefinitions;

import org.openqa.selenium.WebDriver;

import com.xb.cafe.ui.pages.WebNprNdrLastMile;

import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;
import net.thucydides.core.webdriver.SupportedWebDriver;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
import io.cucumber.java.en.*;

public class WebNprNdrLastMile_stepDef {

	@Steps
//	WebNprNdrLastMile LastMile = new WebNprNdrLastMile() ; 
//	//AndroidLogin Alogin = new AndroidLogin();
	
	WebNprNdrLastMile lastMile =new WebNprNdrLastMile();

	@Managed
	SupportedWebDriver browser;

	@Managed(driver = "chrome")
	WebDriver driver;

	
	
	@Given("user Click on Last mile tab and enter AWB {string}")
	public void UserclickLastMile(String aWB) throws InterruptedException {
		lastMile.clickLastMile(aWB);
	}
	
	@Then("Search for closed trip {string} and validate NDR date {string} is there")
    public void validateNDRdate(String awb, String ndrdate) throws Throwable {
		lastMile.ValidareNdrIsMarked(awb, ndrdate);
    }
	
	@And("Search for Value Added Services Awb {string} and validate date is there {string}")
    public void searchForValueAddedServicesAwb(String vasawb, String vasdate) throws Throwable {
		lastMile.searchForValueAddedServicesAwb(vasawb, vasdate);
    }


		
		
		
	
}