package starter.stepdefinitions;

import org.openqa.selenium.WebDriver;

import com.xb.cafe.ui.pages.AndroidWebNprNdr;


import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;
import net.thucydides.core.webdriver.SupportedWebDriver;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
import io.cucumber.java.en.*;

public class AndroidWebNprNdr_stepDef {

	@Steps
//	WebFirstMile  WebFirstMile;
	AndroidWebNprNdr WebNprNdr; 

	@Managed
	SupportedWebDriver browser;

	@Managed(driver = "chrome")
	WebDriver driver;

	
//	
//	@Then("search for {string} and click on view trip then click on close trip button")
//	public void searchAndClickOnView(String Awb) throws InterruptedException {
//		WebNprNdr.searchAndClickOnView(WebFirstMile.tripIdNumber, Awb);
//	}
//	
	
//	@Then("search for {string} and click on view trip then click on close trip button")
//	public void searchAndClickOnView(String Awb2) throws InterruptedException {
//		WebNprNdr.searchAndClickOnView(AndroidWebFirstMile.tripIdNumber, Awb2);
//	}

	@When("clicked on expand all scan {string} and no of {string} and click on save")
	public void clickedExpandScanAnsSave(String AWB, String Invoices) throws InterruptedException {
		WebNprNdr.clickedExpandScanAnsSave(AWB,Invoices);
	    
	}

	@Then("scan AWB-MPS and add {string} then click on close trip")
	public void addOddAndCloseTrip(String OdoReading) throws InterruptedException {
		WebNprNdr.addOddAndCloseTrip(OdoReading);
		
	}
	
	@Then ("Validate AWB RAD date and time column is present")
	public void Click() throws InterruptedException {
		WebNprNdr.userNavigatesToTripManagement();
		
	}
	
	@Then("Search for closed trip {string} and validate reschedule date {string} is there")
	public void searchRescheduleColumnIsThere(String AWB, String NprDate) throws InterruptedException {
		WebNprNdr.searchRescheduleColumnIsThere(AWB, NprDate);
	}
	
	@Then("Validate VAS column is present")
	public void VasClmnIsPresent() throws InterruptedException {
		WebNprNdr.VasClmnIsPresent();
	}
	
	@Then("Search for closed trip {string} and validate vas date {string} is there")
	public void searchTripToValidateVas(String AWB, String VASdate) throws InterruptedException {
		WebNprNdr.searchTripToValidateVas(AWB,VASdate );
	}
	
	@Given("Click on LastMile tab")
	public void clickLastMile() {
		WebNprNdr.clickLastMile();
	}
	
	 @And("LogOut Web App")
	    public void logoutWebApp() throws Throwable {
		 WebNprNdr.LogoutWeb();

	 }
	 
//	 @Then("search for undelivered {string} and click on view trip then click on close trip button")
//	    public void searchForUndeliveredSomethingAndClickOnViewTripThenClickOnCloseTripButton(String awb) throws Throwable {
//		 WebNprNdr.searchForUndeliveredSomethingAndClickOnViewTripThenClickOnCloseTripButton(WebFirstMile.tripIdNumber, awb);
//
//	 }
		
		
	
}