package starter.stepdefinitions;


import com.xb.cafe.ui.pages.Cargo;
import com.xb.cafe.ui.pages.Cargo_Booking;
import com.xb.cafe.ui.pages.Cargo_PrintAWB;

import java.awt.AWTException;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.testng.Assert;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Cargo_PrintAWB_StepDef{

	@Steps
	Cargo_PrintAWB cargo_PrintAWB;
	
	
	@Given("Navigate to Print AWB option")
	public void navigate_to_Print_AWB_option() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		cargo_PrintAWB.navigate_PrintAWBMenu();
	}

	@When("User enters {string} in the text field. User clicks on Validate AWB button")
	public void user_enters_in_the_text_field(String AWBnumber) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		cargo_PrintAWB.printAWB_EnterAWBnumber(AWBnumber);
	}

	@Then("Validate Details displayed. Also check entered {string} is validated")
	public void validate_Details_displayed_AWB_details(String AWBNumber) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		cargo_PrintAWB.validate_Details_Displayed(AWBNumber);
	}
	
	@When("User clicks on Print Button. User cliks escape button for printing to cancel")
	public void printAndCancel() throws InterruptedException, AWTException {
	    // Write code here that turns the phrase above into concrete actions
		cargo_PrintAWB.printAWBandCancel();
	}
	
	@When("User enters inavlid AWBs {string} in the text field. User clicks on Validate AWB button")
	public void user_enters_invalid_AWBs(String AWBNumber) throws InterruptedException {
		cargo_PrintAWB.printAWB_EnterAWBnumber(AWBNumber);
	}
	@Then("Validate Error message displayed {string} and check error message {string}")
	public void validate_blank_error_details(String AWBNumber, String errMsg) {
		cargo_PrintAWB.validate_Blank_Data_error_displayed(AWBNumber, errMsg);
	}
	
	@Then("Validate Details displayed for invalid AWBs {string}")
	public void validate_details_displayed_for_invlaid_AWBS(String Message) throws InterruptedException{
		cargo_PrintAWB.validate_Details_Displayed_for_Invalid_AWBs(Message);
	}
	
	@Then("Validate number of AWB Details displayed")
	public void validate_number_of_AWB_Details_displayed(){
		cargo_PrintAWB.validate_No_of_AWB_Details_Displayed();
		
	}

}


 