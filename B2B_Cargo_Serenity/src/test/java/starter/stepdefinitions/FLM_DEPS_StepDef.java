package starter.stepdefinitions;

import com.xb.cafe.ui.pages.FLM_DEPS;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class FLM_DEPS_StepDef {
	
	@Steps
	FLM_DEPS deps;
	
	@Given("User will navigate to Cargo Mark Shipments Damage")
	public void userwillnavigatetoCargoMarkShipmentsDamage() throws InterruptedException {
		deps.userwillnavigatetoCargoMarkShipmentsDamage();
	}
	
	@When("User will add the record for Damage MPS")
	public void userwilladdtherecordforDamageMPS() throws InterruptedException {
		deps.userwilladdtherecordforDamageMPS();
	}
	
	@Then("User enters the MPS details for Damage Products {string},{string}")
	public void userenterstheMPSdetailsforDamageProducts(String DamageReason,String Description) throws InterruptedException {
		deps.userenterstheMPSdetailsforDamageProducts(DamageReason,Description);
	}
	
	@Then("User enters the not shipped MPS details for Damage Products {string},{string},{string}")
	public void userentersthenotshippedMPSdetailsforDamageProducts(String DamageReason,String Description, String MPSStatus) throws InterruptedException {
		deps.userentersthenotshippedMPSdetailsforDamageProducts(DamageReason,Description,MPSStatus);
	}
	
	@Then("Capture the MPS No from cargo Booking")
	public void capturetheMPSNofromcargoBooking() throws InterruptedException {
		deps.capturetheMPSNofromcargoBooking();
	}
	
	@Given("If user wanted to update the Damage Condition {string}")
	public void ifuserwantedtoupdatetheDamageCondition(String DamageReason1) throws InterruptedException {
		deps.ifuserwantedtoupdatetheDamageCondition(DamageReason1);
	}
	
	@When("User wanted to View Damage Details {string},{string}")
	public void userwantedtoViewDamageDetails(String PreviousReason, String Description1) throws InterruptedException {
		deps.userwantedtoViewDamageDetails(PreviousReason, Description1);
	}
	
	@Then("User can view the uploaded image")
	public void usercanviewtheuploadedimage() throws InterruptedException {
		deps.usercanviewtheuploadedimage();
	}
	
	@Given("User clicks on Filter Option")
	public void userclicksonFilterOption() throws InterruptedException {
		deps.userclicksonFilterOption();
	}
	
	@When("User enters the invalid inputs {string},{string}")
	public void userenterstheinvalidinputs(String MPSNo, String DamageHubName) throws InterruptedException {
		deps.userenterstheinvalidinputs(MPSNo, DamageHubName);
	}
	
	@And("User searches for the provided input {string}")
	public void usersearchesfortheprovidedinput(String OutPut) throws InterruptedException {
		deps.usersearchesfortheprovidedinput(OutPut);
	}
	
	@Then("User enters the MPS details of other hubs for Damage Products {string},{string},{string},{string}")
	public void userenterstheMPSdetailsofotherhubsforDamageProducts(String DamageReason, String Desc, String MPSNo, String StatusValidation) throws InterruptedException {
		deps.userenterstheMPSdetailsofotherhubsforDamageProducts(DamageReason, Desc, MPSNo, StatusValidation);
	}
	
	@Then("User tries to upload image more then 6000 KB {string},{string},{string}")
	public void usertriestouploadimagemorethen6000KB(String DamageReason, String MPSNo, String ImageError) throws InterruptedException {
		deps.usertriestouploadimagemorethen6000KB(DamageReason, MPSNo, ImageError);
	}
	
	@Then("User enters the already marked MPS details again {string},{string},{string}")
	public void userentersthealreadymarkedMPSdetailsagain(String DamageReason, String Description, String MPSStatus) throws InterruptedException {
		deps.userentersthealreadymarkedMPSdetailsagain(DamageReason, Description, MPSStatus);
	}
}
