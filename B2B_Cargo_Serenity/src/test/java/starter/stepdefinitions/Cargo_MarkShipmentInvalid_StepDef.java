package starter.stepdefinitions;

import com.xb.cafe.ui.pages.Cargo_MarkShipmentInvalid;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Cargo_MarkShipmentInvalid_StepDef {

	@Steps
	Cargo_MarkShipmentInvalid mark_shipmentinvalid;

	@Given("Navigate  Support page")

	public void navigateSupport() throws InterruptedException {
		mark_shipmentinvalid.NavigateSupportLink();
	}

	@When("Clicked on Mark Shipment Invalid link")
	public void navigatetomarkshipmentinvalid() throws InterruptedException {
		mark_shipmentinvalid.NavigateToMarkShipmentInvalid();
	}

	@Then("User  navigate on {string} page")
	public void MSI(String MarkShipmentInvalid) throws InterruptedException {
		mark_shipmentinvalid.MarkSI(MarkShipmentInvalid);
	}

	@Then("Mark shipment invalid menu should be displayed")
	public void Markshipmentinvalidmenu() throws InterruptedException {
		mark_shipmentinvalid.MarksShipmentInvalidMenu();
	}

	@Then("User check elements present on page")
	public void pageelements() throws InterruptedException {
		mark_shipmentinvalid.PageElements();
	}

	@When("Entered {string}")
	public void shipmentidvalidation(String ShipmentId) throws InterruptedException {
		mark_shipmentinvalid.ShipmentIDValidation(ShipmentId);
	}

	@Then("It should show validation message {string},{string}")
	public void validationmsg(String ShipmentIdMsg, String commentsmsg) {
		mark_shipmentinvalid.ValidationMsg(ShipmentIdMsg, commentsmsg);
	}
	
	@When("Enter comment {string}")
	public void entercomment(String Comment) throws InterruptedException {
		mark_shipmentinvalid.EnterComment(Comment);
	
	}
	@Then("It should show sucess popup with heading {string} and message {string}")
	public void onSucess(String Popupheading,String SuccessMsg) throws InterruptedException
	{
		mark_shipmentinvalid.OnSuccess(Popupheading,SuccessMsg);
	}
	@When("Clicked update button")
	public void clickupdate() {
		mark_shipmentinvalid.ClickUpdatebtn();
	}
	@Then("It should show {string}")
	public void invalidAWB(String InvalidAWBMessage) throws InterruptedException {
		mark_shipmentinvalid.InvalidAWBNoMsg(InvalidAWBMessage);
	}
	 @When("Entered {string} more than Ten")
	 public void morethantenAWB(String ShipmentId) {
		 mark_shipmentinvalid.MoreThanTenAWB(ShipmentId);
	 }
	 @Then("It should show max allowed awb {string}")
	 public void msgmaxtenAWB(String MaxTenAWBMsg) {
		 mark_shipmentinvalid.MaxTenAWBAllowed(MaxTenAWBMsg);
	 }
	 @Then("Entered {string} and press enter key then it will not insert new line {string}")
	 public void PressEnterky(String ShipmentId,String Value) {
		 mark_shipmentinvalid.PressEnterKey(ShipmentId,Value);
	 }
	 
}
