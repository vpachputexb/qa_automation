package starter.stepdefinitions;

import com.xb.cafe.ui.pages.ShiftShipmentDestinationHub;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ShiftSHipmentDestinationHub_StepDef {
	
	@Steps
	ShiftShipmentDestinationHub shiftShipmentDestinationHub;
	
	@Given("Navigate to shift shipment destinationhub module")
	public void navigateToShiftShipmentDestinationHubModule() throws InterruptedException {
		shiftShipmentDestinationHub.navigateToShiftShipmentDestinationHubModule();
	}
	
	@When("User enters {string}")
	public void userEntersAWBNumber(String awbNumber) {
		shiftShipmentDestinationHub.userEntersAWBNumber(awbNumber);
	}

	@When("User selects change destination hub {string}")
	public void userSelectsChangeDestinationHub(String changedHub) throws InterruptedException {
		shiftShipmentDestinationHub.userSelectsChangeDestinationHub(changedHub);
	}
	@When("User enters comments {string}")
	public void userEntersComments(String comments) {
		shiftShipmentDestinationHub.userEntersComments(comments);
	}
	@Then("User Clicks on update button")
	public void userClicksOnUpdateButton() throws InterruptedException {
		shiftShipmentDestinationHub.userClicksOnUpdateButton();
	}
	
	@Then("Validate successmessage {string}")
	public void validateSuccessMessage(String successMessage) throws InterruptedException {
		shiftShipmentDestinationHub.validateSuccessMessage(successMessage);
	}
	@Then("Validate error messages {string}, {string}, {string}")
	public void validateErrorMessage(String errorAWBNumber, String errorFinalHub, String errorComments) throws InterruptedException {
		shiftShipmentDestinationHub.validateErrorMessage(errorAWBNumber, errorFinalHub, errorComments);
	}
	@Then("Validate successmessage samehub {string},{string}")
	public void validateSuccessmessageSamehub(String awbNumber, String errorMessage) throws InterruptedException {
		shiftShipmentDestinationHub.validateSuccessmessageSamehub(awbNumber, errorMessage);
	}
	@Then("Validate errorMessage format {string}")
	public void validateErrorMessageformat(String errorMessage) {
		shiftShipmentDestinationHub.validateErrorMessageformat(errorMessage);
	}
}
