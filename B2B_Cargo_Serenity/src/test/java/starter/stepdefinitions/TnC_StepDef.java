package starter.stepdefinitions;



import java.io.IOException;

import com.xb.cafe.ui.pages.TnC;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class TnC_StepDef {

//	@Given("I login in Cargo Web UI with following credentials")
//	public void cargo_Login(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
//		Cargo.cargoLogin(dataTable);
//	}
	@Steps
   TnC TnC;
	

	@Given("User Navigates to Master data1 option")
	public void userNavigatesToMasterData1Option() throws InterruptedException {
		 TnC.userNavigatesToMasterData1Option();

}
	@When("User Navigates to create new TnC option")
	public void userNavigatesToCreateNewTnCOption() throws InterruptedException {
		TnC.userNavigatesToCreateNewTnCOption();
	   }
	@Then ("User enters {string} in the text field")
	public void userEntersInTheTextField(String TermsandConditionsTemplateName) throws InterruptedException {
	TnC.userEntersInTheTextField(TermsandConditionsTemplateName);
	}
	@Then ("User enters {string} in the text field and clicks on Expand All button")
	public void userEntersInTheTextFieldAndClicksOnExpandAllButton(String TermsandConditionsTemplateDescription) throws InterruptedException {
	TnC.userEntersInTheTextFieldAndClicksOnExpandAllButton(TermsandConditionsTemplateDescription);
	}
	@Then ("User selects Billing Cycle {string} and select POD Hard Copy Required value and enter date {string}")
	public void userSelectsBillingCycleAndSelectPODHardCopyRequiredvalueAndEnterDate(String BillingCycle, String date) throws InterruptedException {
		TnC.userSelectsBillingCycleAndSelectPODHardCopyRequiredvalueAndEnterDate(BillingCycle, date);
	}
	@Then ("User enters Billing terms {string},{string},{string},{string} and user selects Payment mode Cheque")
	public void userEntersBillingTermsAndUserSelectsPaymentModeCheque(String CreditPeriod, String Creditlimit, String Advancepayment, String Creditlimitalert) throws InterruptedException {
     TnC.userEntersBillingTermsAndUserSelectsPaymentModeCheque(CreditPeriod, Creditlimit, Advancepayment, Creditlimitalert);
	}
	@Then ("User enters Other terms and conditions {string}")
	public void userEntersOtherTermsAndConditions(String Maxliabilityamount) throws InterruptedException {
	TnC.userEntersOtherTermsAndConditions(Maxliabilityamount);
	}
	@Then("User selects Billing Cycle {string} and select POD Hard Copy Required value and enter dates {string} {string}")
	public void userSelectsBillingCycleAndSelectPodHardCopyRequiredValueAndEnterDates(String BillingCycle, String date1, String date2) throws InterruptedException {
	TnC.userSelectsBillingCycleAndSelectPodHardCopyRequiredValueAndEnterDates(BillingCycle, date1, date2);
	
}
	@Then ("User selects Billing Cycle {string} and select POD Hard Copy Required value")
	public void userSelectsBillingCycleAndSelectPODHardCopyRequiredvalue(String BillingCycle) throws InterruptedException {
		TnC.userSelectsBillingCycleAndSelectPODHardCopyRequiredvalue(BillingCycle);
}
	@Then ("User validates error msgs {string},{string},{string},{string},{string},{string}")
	public void userValidatesErrorMsgs(String TermsandConditionsTemplateNameerrormsg, String TermsandConditionsTemplateDescriptionerrormsg, String Billingcycleerrormsg, String PODHardCopyrequirederrormsg, String Creditdayserrormsg, String Maxliabilityerrormsg) throws InterruptedException {
	TnC.userValidatesErrorMsgs(TermsandConditionsTemplateNameerrormsg, TermsandConditionsTemplateDescriptionerrormsg, Billingcycleerrormsg, PODHardCopyrequirederrormsg, Creditdayserrormsg, Maxliabilityerrormsg);
	}
	@Then ("User enters POD hard copy charges {string}")
	public void userEntersPODHardCopyCharges(String PODCharges) throws InterruptedException {
	TnC.userEntersPODHardCopyCharges(PODCharges);
	}
	@Then ("User selects Billing Cycle {string} and select POD Hard Copy Required value Yes")
	public void userSelectsBillingCycleAndSelectPODHardCopyRequiredvalueYes(String BillingCycle) throws InterruptedException {
	TnC.userSelectsBillingCycleAndSelectPODHardCopyRequiredvalueYes(BillingCycle);
	
	}

   @Then ("User validates successfulmsg and clicks on Ok {string}")
	public void userValidatesSuccessfulmsgAndClicksOnOk(String Successfulmsg) throws InterruptedException {
		TnC.userValidatesSuccessfulmsgAndClicksOnOk(Successfulmsg);
	}
	
	@Then ("User validates error msg and clicks on Ok {string}")
	public void userValidatesErrorMsgAndClicksOnOk(String Errormsg) throws InterruptedException {
		TnC.userValidatesErrorMsgAndClicksOnOk(Errormsg);
	}
	
	@Then ("User clicks on edit button")
	public void userClicksOnEditButton() throws InterruptedException {
		TnC.userClicksOnEditButton();
		
	}
	@Then ("User clicks on Expand All button")
	public void userClicksOnExpandAllButton() throws InterruptedException {
		TnC.userClicksOnExpandAllButton();
	}
	@Then ("User clears max liability amount")
	public void userClearsMaxLiabilityAmount() throws InterruptedException {
		TnC.userClearsMaxLiabilityAmount();
	}
	@Then ("User enters new max liability amount {string}")
	public void userEntersNewMaxLiabilityAmount(String Maxliabilityamount) throws InterruptedException {
		TnC.userEntersNewMaxLiabilityAmount(Maxliabilityamount);
	}
	
	@Then ("User validates listing elements {string},{string},{string},{string},{string},{string},{string}")	
		public void userValidatesListingElements(String TemplateName, String TemplateDescription, String CreatedBy, String CreatedDate, String LastModifiedBy, String LastModifiedDate, String Status) throws InterruptedException {
		TnC.userValidatesListingElements(TemplateName, TemplateDescription, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate, Status);
		
		
	}	
}	
	
	

	