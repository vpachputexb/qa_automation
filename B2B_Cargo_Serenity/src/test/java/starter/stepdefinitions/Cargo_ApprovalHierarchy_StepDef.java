package starter.stepdefinitions;

import com.xb.cafe.ui.pages.Cargo_ApprovalHierarchy;
import com.xb.cafe.ui.pages.Cargo_Booking;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Cargo_ApprovalHierarchy_StepDef {

	@Steps
	Cargo_ApprovalHierarchy approval_hierarchy;

	@Given("Navigate to Master data and click Approval hierarchy link")
	public void navigateToMaster() throws InterruptedException {
		approval_hierarchy.NavigateToMaster();
	}

	@When("Approval object is selected as Client")
	public void approvalObject() throws InterruptedException {
		approval_hierarchy.checkApprovalObjectSelection();
	}

	@When("User clicked on plus button")
	public void addrow() {
		approval_hierarchy.addRow();

	}

	@Then("it should add new row")
	public void addnewRow() throws InterruptedException {
		approval_hierarchy.addnewRow();

	}

	@When("user selects {string} and {string}")
	public void selectRoel(String Role, String User) throws InterruptedException {
		approval_hierarchy.selectRole(Role, User);

	}

	@When("user clicked on Save button")
	public void clickonSavebtn() throws InterruptedException {
		approval_hierarchy.clickOnSaveButton();

	}

	@Then("Success message {string} should displayed")
	public void successMessage(String Successmsg) throws InterruptedException {
		approval_hierarchy.checkSuccessMessage(Successmsg);
	}

	@Then("It should show {string},{string}")
	public void errorOnBlankinput(String ErrorOnBlankRole, String ErroronBlankUser) throws InterruptedException {
		approval_hierarchy.ErrorMessageonBlankInput(ErrorOnBlankRole, ErroronBlankUser);
	}

	@When("User clicked on Delete button")
	public void clickedDeletebutton() throws InterruptedException {
		approval_hierarchy.ClickOnDeleteButton();

	}

	@Then("It should show confirmation message {string} as well as Ok and cancel button")
	public void confirmationmsg(String DeleteConfirmationmsg) {
		approval_hierarchy.ConfirmationMessageOnDelete(DeleteConfirmationmsg);

	}

	@When("Confirmation message is displayed")
	public void showconfirmationmsg() throws InterruptedException {
		approval_hierarchy.ShowConfirmationMessageonclickedDelete();

	}

	@When("Clicked on OK button")
	public void clickOKbutton() throws InterruptedException {
		approval_hierarchy.ClickOKbutton();
	}

	@Then("Popup should get closed and record should get deleted")
	public void resultonOkclicked() {
		approval_hierarchy.ResultwhenClickedOK();
	}

	@When("User clicked on email checkbox")
	public void clickonemailCheckbox() throws InterruptedException {
		approval_hierarchy.ClickonEmailCheckbox();

	}

	@Then("Checkbox should be shown as selected")
	public void checkboxchecked() throws InterruptedException {
		approval_hierarchy.CheckboxSelected();
	}

}
