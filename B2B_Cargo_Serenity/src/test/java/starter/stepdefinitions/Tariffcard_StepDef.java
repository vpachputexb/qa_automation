package starter.stepdefinitions;



import java.io.IOException;

import com.xb.cafe.ui.pages.Tariffcard;
import com.xb.cafe.ui.pages.TnC;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Tariffcard_StepDef {

//	@Given("I login in Cargo Web UI with following credentials")
//	public void cargo_Login(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
//		Cargo.cargoLogin(dataTable);
//	}
	@Steps
	Tariffcard Tariffcard;
	
	@Given("User Navigates to Traiff card option")
	public void userNavigatesToTraiffCardOption() throws InterruptedException {
		Tariffcard.userNavigatesToTraiffCardOption();
	}
    @When("Click on add new Tariff card button")
    public void clickOnAddNewTariffCardButton() throws InterruptedException {
    	Tariffcard.clickOnAddNewTariffCardButton();
    	
    }
    @Then ("Provide Tariff card name {string} and select Route mode Air")
    public void provideTariffCardNameAndSelectRouteModeAir(String TemplateName) throws InterruptedException {
    	Tariffcard.provideTariffCardNameAndSelectRouteModeAir(TemplateName);
    }
    @Then ("select service level template {string} from drop down and select rate basis Quantity {string}")
    public void selectServiceLevelTemplateFromDropDownAndSelectRateBasisQuantity(String Serviceleveltemplate, String ratebasis) throws InterruptedException {
    	Tariffcard.selectServiceLevelTemplateFromDropDownAndSelectRateBasisQuantity(Serviceleveltemplate, ratebasis);
    }
    @Then ("Select Tariff card rate type flat rate {string}")
    public void selectTariffCardRateTypeFlatRate(String Tariffcardratetype) throws InterruptedException {
	   Tariffcard.selectTariffCardRateTypeFlatRate(Tariffcardratetype);
}
    
     @Then ("Upload file {string} and validate it")
     public void uploadFileAndValidateIt(String fileName) throws InterruptedException {
    	 Tariffcard.uploadFileAndValidateIt(fileName);
     }
     @Then ("click on Save button and validates successmsg {string}")
     public void clickOnSaveButtonAndValidatesSuccessmsg(String Successmsg) throws InterruptedException {
    	 Tariffcard.clickOnSaveButtonAndValidatesSuccessmsg(Successmsg);
     }

     
    @Then ("Select Tariff card rate type slab rate")
   public void selectTariffCardRateTypeSlabRate() throws InterruptedException {
	   Tariffcard.selectTariffCardRateTypeSlabRate();
   }
    
    @Then ("User selects slab type Quantity")
    public void userSelectsSlabTypeQuantity() throws InterruptedException {
    	Tariffcard.userSelectsSlabTypeQuantity();
    }
    @Then ("User selects slab template {string}")
    public void userSelectsSlabTemplate(String Slabtemplate) throws InterruptedException {
    	Tariffcard.userSelectsSlabTemplate(Slabtemplate);
    }
    
    @Then ("User clicks on Break 1")
    public void userClicksOnBreak1() throws InterruptedException {
    	Tariffcard.userClicksOnBreak1();
    }

   @Then ("Upload file 1 {string} and validate it")
    public void uploadFile1AndVvalidateIt(String fileName1) throws InterruptedException {
	Tariffcard.uploadFile1AndValidateIt(fileName1);
}
    @Then ("User clicks on Break 2")
    public void userClicksOnBreak2() throws InterruptedException {
    	Tariffcard.userClicksOnBreak2();
    }
    @Then ("Upload file 2 {string} and validate it")
    public void uploadFile2AndVvalidateIt(String fileName2) throws InterruptedException {
	Tariffcard.uploadFile2AndVvalidateIt(fileName2);

}
    @Then ("Upload file 2 {string}")
    public void uploadFile2(String fileName2) throws InterruptedException {
	Tariffcard.uploadFile2(fileName2);
    }
    
    @Then ("click on Save button and validates Errormsg {string}")
    public void clickOnSaveButtonAndValidatesErrormsg(String Errormsg) throws InterruptedException {
    	Tariffcard.clickOnSaveButtonAndValidatesErrormsg(Errormsg);
    }
    
    @Then ("Upload Invalid file {string} and validate it")
    public void uploadInvalidFileAndValidateIt(String InvalidfileName) throws InterruptedException {
    	Tariffcard.uploadInvalidFileAndValidateIt(InvalidfileName);
    }
    
    @Then ("User clicks on Download CSV button")
    public void userClicksOnDownloadCSVButton() throws InterruptedException {
    	Tariffcard.userClicksOnDownloadCSVButton();
    }
    @When ("User clicks on filter option1")
    public void  userClicksOnFilterOption1() throws InterruptedException {
    	Tariffcard.userClicksOnFilterOption1();
    	}
    
   @Then ("User enters tariffcard name {string} in Tariff card name filter")
    public void userEntersTariffcardNameInTariffCardNameFilter(String Tariffcardname) throws InterruptedException {
    	Tariffcard.userEntersTariffcardNameInTariffCardNameFilter(Tariffcardname);
    }
   @Then ("User clicks on Search button1")
   public void userClicksOnSearchButton1() throws InterruptedException {
	   Tariffcard.userClicksOnSearchButton1();
   }
    	
    
    @Then ("User clicks on Edit option")
    public void userClicksOnEditOption() throws InterruptedException {
    	Tariffcard.userClicksOnEditOption();
    	}
    @Then ("User validates the alert msg {string} and clicks on Ok")
    public void userValidatesTheAlertMsgAndClicksOnOk(String Alertmsg) throws InterruptedException {
    	Tariffcard.userValidatesTheAlertMsgAndClicksOnOk(Alertmsg);
    }
    @Then ("User clicks on save button")
    public void userClicksOnSaveButton() throws InterruptedException {
    	Tariffcard.userClicksOnSaveButton();
    }
    
    @Then ("download valid data")
    public void downloadValidData() throws InterruptedException {
    	Tariffcard.downloadValidData();
    	}
    @Then ("Upload file 3 {string}")
    public void uploadFile3(String fileName3) throws InterruptedException {
    	Tariffcard.uploadFile3(fileName3);
    }
    
    @Then ("verify Header mismatch error {string}")
    public void verifyHeaderMismatchError(String Headermismatcherror) throws InterruptedException {
    	Tariffcard.verifyHeaderMismatchError(Headermismatcherror);
    	}
    
    @Then ("select service level template {string} from drop down and select rate basis Weight {string}")
    public void selectServiceLevelTemplateFromDropDownAndSelectRateBasisWeight(String Serviceleveltemplate, String ratebasis) throws InterruptedException {
    	Tariffcard.selectServiceLevelTemplateFromDropDownAndSelectRateBasisWeight(Serviceleveltemplate, ratebasis);
    }
     @Then ("User tries to select disabled slab type Quantity")
     public void userTriesToSelectDisabledSlabTypeQuantity() throws InterruptedException {
      Tariffcard.userTriesToSelectDisabledSlabTypeQuantity();


}	
}	
	
	