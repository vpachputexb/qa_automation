package starter.stepdefinitions;
import com.xb.cafe.ui.pages.CargoBooking_FileUpload;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.webdriver.SupportedWebDriver;

public class Cargo_Fileupload_StepDef {
	
	@Steps
	CargoBooking_FileUpload cargofileupload;
	
	@Managed
	SupportedWebDriver browser;

	@Given("Navigate to Cargo Booking Menu and Clicks on Flipkart Soft Data Upload tab")
	public void navigateToCargoBookingMenuAndClicksOnFlipkartSoftDataUploadTab() throws InterruptedException {
		cargofileupload.NavigateBooking();
	}

	@When("User enters {string} information")
	public void userEntersClientFileTypeInformation(String clientfiletype) throws InterruptedException {
	   cargofileupload.userEntersClientFileTypeInformation(clientfiletype);
	}
	@When("User selectss client {string}")
	public void userSelectssClient(String ClientName) throws InterruptedException {
		cargofileupload.selectClient(ClientName);
	}

	@When("Select {string} which want to upload. User click on Submit button")
	public void selectOfFileWhichWantToUpload(String FileName) throws InterruptedException {
		cargofileupload.uploadFile(FileName);	
		}
	
	@Then("User clicks submit button. Validate success message as {string} Click on OK button")
	public void validateSuccessMessageAsClickOnOKButton(String Message_FileUpload) throws InterruptedException {
		cargofileupload.submitAndValidateSuccessMsg(Message_FileUpload);	
		}

	@Then("Validate upload details")
	public void validateUploadDetails() throws InterruptedException {
		cargofileupload.NavigateBooking();	
		}
	
	@When("User clicks on Submit button and validate the error message")
	public void userClicksOnSubmitButtonAndValidateTheErrorMessage() throws InterruptedException {
		cargofileupload.userClicksOnSubmitButtonAndValidateTheErrorMessage();
	}
	@Then("User searches client name and Validate Search {string}")
	public void userSearchesClientNameAndValidateSearch(String ClientName)throws InterruptedException {
		cargofileupload.userSearchesClientNameAndValidateSearch(ClientName);
	}
	@Then("User searches invalid client name and Validate Search {string}")
	public void userSearchesInvalidClientNameAndValidateSearch(String ClientName)throws InterruptedException {
		cargofileupload.userSearchesInvalidClientNameAndValidateSearch(ClientName);
	}
	@Then("User clicks on reset button")
	public void userClicksOnResetButton()throws InterruptedException {
		cargofileupload.userClicksOnResetButton();
	}
	@When("user select file {string} and clicks on submit button")
	public void userUploadsTheFile(String filename) throws InterruptedException {
		cargofileupload.userUploadsTheFile(filename);
	}
@Then("user clicks on submit button")
public void userClicksOnSubmitbuttonn() throws InterruptedException {
	cargofileupload.userClicksOnSubmitbuttonn();
}
	}
	

