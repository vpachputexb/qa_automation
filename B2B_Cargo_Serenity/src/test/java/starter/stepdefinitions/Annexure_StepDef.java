package starter.stepdefinitions;



import java.io.IOException;

import com.xb.cafe.ui.pages.Annexure;
import com.xb.cafe.ui.pages.Annexure;
import com.xb.cafe.ui.pages.Annexure;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Annexure_StepDef {

//	@Given("I login in Cargo Web UI with following credentials")
//	public void cargo_Login(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
//		Cargo.cargoLogin(dataTable);
//	}
	@Steps
	Annexure Annexure;
	
	@Given ("User Navigates to Annexure option")
	public void userNavigatesToAnnexureOption() throws InterruptedException {
		Annexure.userNavigatesToAnnexureOption();
	}
    @When ("Click on add new Annexure button")
    public void clickOnAddNewAnnexureButton() throws InterruptedException {
    	Annexure.clickOnAddNewAnnexureButton();
    }
   @Then ("User selects company name {string}") 
   public void userSelectsCompanyName(String Companyname) throws InterruptedException {
	   Annexure.userSelectsCompanyName(Companyname);
   }	
   
   @Then ("User selects contract id {string}")
   public void userSelectsContractId(String Contractid) throws InterruptedException {
	   Annexure.userSelectsContractId(Contractid);
   }
   
   @Then ("User provides Annexure name {string}")
   public void userProvidesAnnexureName(String Annexurename) throws InterruptedException {
	   Annexure.userProvidesAnnexureName(Annexurename);
	   
   }
   @Then ("User uploads Annexure file {string}")
   public void userUploadsAnnexureFile(String Annexurefilename) throws InterruptedException {
	   Annexure.userUploadsAnnexureFile(Annexurefilename);
	   
   }
  @Then ("User validates successful msg1 and clicks on Ok {string}")
  public void userValidatesSuccessfulMsg1AndClicksOnOk(String Successfulmsg) throws InterruptedException {
	  Annexure.userValidatesSuccessfulMsg1AndClicksOnOk(Successfulmsg);
  }
   
   @Then ("User clicks on save button1 validates successmsg and clicks on Ok {string}")
  public void userClicksOnSaveButton1ValidatesSuccessmsgAndClicksOnOk(String Successmsg) throws InterruptedException {
	   Annexure.userClicksOnSaveButton1ValidatesSuccessmsgAndClicksOnOk(Successmsg);
   }
   @Then ("User uploads jpeg Annexure file {string}")
   public void userUploadsJpegAnnexureFile(String Annexurefilename) throws InterruptedException {
	   Annexure.userUploadsJpegAnnexureFile(Annexurefilename);
	   

}
   @Then ("User uploads pdf Annexure file {string}")
   public void userUploadsPdfAnnexureFile(String Annexurefilename) throws InterruptedException {
	   Annexure.userUploadsPdfAnnexureFile(Annexurefilename);
   }
   @Then ("User uploads jpg Annexure file {string}")
   public void userUploadsJpgAnnexureFile(String Annexurefilename) throws InterruptedException {
	   Annexure.userUploadsJpgAnnexureFile(Annexurefilename);
   }
   @Then ("User uploads Annexure file of exceeded limit {string}")
   public void userUploadsAnnexureFileOfExceededLimit(String Annexurefilename) throws InterruptedException {
	   Annexure.userUploadsAnnexureFileOfExceededLimit(Annexurefilename);
   }
   @Then ("User validates error msg1 and clicks on Ok {string}")
   public void userValidatesErrorMsg1AndClicksOnOk(String Errormsg) throws InterruptedException {
	   Annexure.userValidatesErrorMsg1AndClicksOnOk(Errormsg);
   }
   
   @Then ("User clicks on download button")
   public void userClicksOnDownloadButton() throws InterruptedException {
	   Annexure.userClicksOnDownloadButton();
   }
   @Then ("User clicks on delete button")
   public void userClicksOnDeleteButton() throws InterruptedException {
	   Annexure.userClicksOnDeleteButton();
   
   }  
   
   @Then ("User validates confirmation msg and clicks on Ok {string}")
   public void userValidatesConfirmationMsgAndClicksOnOk(String Confirmationmsg) throws InterruptedException {
	   Annexure.userValidatesConfirmationMsgAndClicksOnOk(Confirmationmsg);
   }
  @Then ("User uploads non allowed file format {string}") 
  public void userUploadsNonAllowedFileFormat(String Annexurefilename) throws InterruptedException {
	  Annexure.userUploadsNonAllowedFileFormat(Annexurefilename);
  }
  @Then ("User validates error msg2 and clicks on Ok {string}")
  public void userValidatesErrorMsg2AndClicksOnOk(String Errormsg2) throws InterruptedException {
	   Annexure.userValidatesErrorMsg2AndClicksOnOk(Errormsg2);
   
}
  @Then ("User validates error msgs {string}, {string}, {string}, {string}")
  public void userValidatesErrorMsgs(String Companynameerrormsg, String Contractiderrormsg, String Annexurenameerrormsg, String Fileuploaderrormsg) throws InterruptedException {
	  Annexure.userValidatesErrorMsgs(Companynameerrormsg, Contractiderrormsg, Annexurenameerrormsg, Fileuploaderrormsg);
  }
  
//  @Then ("User clicks on Reset button")
//  public void userClicksOnResetButton() throws InterruptedException {
//	  Annexure.userClicksOnResetButton();
//	  }
  
  
  
}    	