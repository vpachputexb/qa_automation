package starter.stepdefinitions;

import com.xb.cafe.ui.pages.Cargo_UADConfi;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Cargo_UADConfiguration_StepDef {
	@Steps
	Cargo_UADConfi uadconfig;

	@Given("Navigate to FLM Menu")
	public void navigateFLM() throws InterruptedException {
		uadconfig.NavigateToFLM();
	}

	@Then("UAD menu should be displayed")
	public void uadmenuDisplayed() throws InterruptedException {
		uadconfig.UADMenuDisplayed();
	}

	@When("Navigate to UAD menu")
	public void navigateUADMenu() throws InterruptedException {
		uadconfig.NavigateUADMenu();
	}

	@Then("UAD configuration menu should displayed")
	public void uadconfigmenu() throws InterruptedException {
		uadconfig.UADConfigMenuNAvigation();
	}

	@When("Navigate to UAD Config tab")
	public void navigatetoUADconfigtab() throws InterruptedException {
		uadconfig.NavigateToUADConfigTab();
	}

	@Then("It should show sub tabs {string},{string},{string}")
	public void tabsonUADconfig(String Tab1, String Tab2, String Tab3) {
		uadconfig.TabsOnUADConfig(Tab1, Tab2, Tab3);
	}

	@Then("UAD Cut off tab should displayed")
	public void uadcutoffTabdisplayed() throws InterruptedException {
		uadconfig.UADCutOffTabDisplayed();
	}

	@Then("Click on UAD Cut off and check UAD Cut off log tab should displayed")
	public void uadCutofflogDisplayed() throws InterruptedException {
		uadconfig.UADCutOffLogDisplayed();
	}

	@Then("Click on other config atb and check Other configuration tab should displayed")
	public void UADotherconfig() throws InterruptedException {
		uadconfig.UADOtherConfigDisplayed();
	}

	@Then("Click on other config atb and check fields displayed")
	public void fieldsonOtherconfig() throws InterruptedException {
		uadconfig.FieldsOnOtherConfig();
	}

	@When("Click on Other config tab")
	public void clickonOtherconfig() throws InterruptedException {
		uadconfig.ClickOnOtherConfig();
	}

	@Then("{string} is and clicked save then it should display {string}")
	public void clicksave(String Maxdays, String ErrorMsg) throws InterruptedException {
		uadconfig.EnterMaxdaysAndclickSave(Maxdays, ErrorMsg);
	}

	@Then("{string} entered and clicked save then it should display {string}")
	public void blankmaxdaysandclickSave(String Maxdays1, String ErrorMsg1) throws InterruptedException {
		uadconfig.BlankMaxdaysAndclickSave(Maxdays1, ErrorMsg1);
	}

	@Then("{string} clicked save then it should display {string}")
	public void successonMaxdays(String Maxdays2, String SuccessMsg) throws InterruptedException {
		uadconfig.SuccessMsgOnMAxDays(Maxdays2, SuccessMsg);
	}

	@Then("Enter {string}  and click save then it should display {string}")
	public void enterconsigneeReason(String ConsigneeReason, String ConsigneeReasonErrorMsg)
			throws InterruptedException {
		uadconfig.EnterConsigneeReason(ConsigneeReason, ConsigneeReasonErrorMsg);
	}

	@Then("Enter valid {string} click save then it should show {string}")
	public void validConsigneeReason(String ConsigneeReasoninp, String SuccessConsigneeReason)
			throws InterruptedException {
		uadconfig.ValidConsigneeReason(ConsigneeReasoninp, SuccessConsigneeReason);
	}

	@Then("Enter blank {string} then click save then it should show {string}")
	public void blankConsigneereason(String ConsigneeReason1, String ConsigneeReasonErrorMsg1)
			throws InterruptedException {
		uadconfig.BlankConsigneeReason(ConsigneeReason1, ConsigneeReasonErrorMsg1);
	}

	@Then("Enter invalid {string}  and click save then it should display {string}")
	public void invalidoperationreason(String OperationReason, String OperationReasonErrorMsg)
			throws InterruptedException {
		uadconfig.InvalidOperationReason(OperationReason, OperationReasonErrorMsg);
	}

	@Then("Blank {string} then click save then it should show {string}")
	public void blankOperationReason(String OperationReason1, String OperationReasonErrorMsg1)
			throws InterruptedException {
		uadconfig.BlankOperationReason(OperationReason1, OperationReasonErrorMsg1);
	}

	@Then("Enter valid input {string} click save should show {string}")
	public void validoperationreason(String OperationReasoninp, String SuccessOperationReason)
			throws InterruptedException {
		uadconfig.ValidOperationReason(OperationReasoninp, SuccessOperationReason);
	}

	@When("Enter {string},{string},{string}")
	public void validOtherconfigvalues(String MAXdays, String CONSIGNEEReason, String OPERATIONReason)
			throws InterruptedException {
		uadconfig.EnterValidOtherConfigvalues(MAXdays, CONSIGNEEReason, OPERATIONReason);
	}

	@Then("click Save it should show {string}")
	public void successOtherConfig(String SUCMsg) throws InterruptedException {
		uadconfig.SuccessOtherConfig(SUCMsg);
	}

	@When("Navigate to UAD cut off log")
	public void navigateuadcutoffLog() throws InterruptedException {
		uadconfig.navigateUADCutoffLog();
	}

	@Then("It should show table with heading {string},{string},{string},{string},{string}")
	public void tablecolumns(String SrNo, String ModificationDate, String RADCutoff, String UADCutoff,
			String UserName) throws InterruptedException {
		uadconfig.LogTableColumns(SrNo, ModificationDate, RADCutoff, UADCutoff, UserName);
	}

	@Then("It should display table with heading {string},{string},{string},{string},{string}")
	public void uadcutofftable(String SrNo1, String ModificationDate1, String RADCutoff1, String UADCutoff1,
			String UserName1) {
		uadconfig.uadCutoffTable(SrNo1, ModificationDate1, RADCutoff1, UADCutoff1, UserName1);
	}

	@When("Clicked on Create cut off button")
	public void createcutoffbtn() throws InterruptedException {
		uadconfig.createCutoff();
	}

	@Then("It should show search and reset button")
	public void visibilityofbtn() throws InterruptedException {
		uadconfig.visibilityOfSearchAndCancelBtn();
	}

	@Then("It should show all expected fields")
	public void fieldsonCreatecutoff() throws InterruptedException {
		uadconfig.fieldsOnCreateCutOff();
	}

	@When("selected {string},{string},{string}")

	public void selectData(String Hubname, String RADInscanTime, String UADDialyCutOffTime)
			throws InterruptedException {
		uadconfig.selectDataIntoAllFields(Hubname, RADInscanTime, UADDialyCutOffTime);
	}

	@When("clicked Save button")
	public void clicksavebtn() throws InterruptedException {
		uadconfig.clickSaveBtn();
	}

	@Then("It should show sucess message {string}")
	public void sucessmessgaeoncutoffcreation(String Successmsg) throws InterruptedException {
		uadconfig.successMessageCutOffCreation(Successmsg);
	}

}
