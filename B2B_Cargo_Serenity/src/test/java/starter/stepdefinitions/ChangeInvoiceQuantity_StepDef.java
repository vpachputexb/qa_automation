package starter.stepdefinitions;

import com.xb.cafe.ui.pages.ChangeInvoiceQuantity;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ChangeInvoiceQuantity_StepDef {

//	@Given("I login in Cargo Web UI with following credentials")
//	public void cargo_Login(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
//		Cargo.cargoLogin(dataTable);
//	}
	@Steps
	ChangeInvoiceQuantity changeInvoiceQuantity;
	
	
	@Given("User Navigates to Support option")
	public void userNavigatesToSupportOption() throws InterruptedException {
		changeInvoiceQuantity.NavigatesToSupportOption();
	}
	
	@When("User enters {string} in the text field. User clicks on Search button")
	public void userEntersInTheTextFieldUserClicksOnSearchButton(String AWB) throws InterruptedException {
		changeInvoiceQuantity.userEntersInTheTextFieldUserClicksOnSearchButton(AWB);
	   
	}
	
	@Then("User clicks on Update button")
	public void userClicksOnUpdateButton()	throws InterruptedException {
		changeInvoiceQuantity.userClicksOnUpdateButton();
	}
	
	@Then ("User validates the successful msg and User clicks on Ok button {string}")
		public void userValidatesTheSuccessfulMsgAndUserClicksOnOkButton(String Successmsg) throws InterruptedException {
			changeInvoiceQuantity.userValidatesTheSuccessfulMsgAndUserClicksOnOkButton(Successmsg);
}
		
     @When("User enters invalid {string} in the text field and User clicks on Search button")
		public void userEntersInvalidInTheTextFieldAndUserClicksOnSearchButton(String AWB) throws InterruptedException {
			changeInvoiceQuantity.userEntersInvalidInTheTextFieldAndUserClicksOnSearchButton(AWB);
		   
		}
		
		@Then ("User validates the error msg and User clicks on Ok button {string}")
			public void userValidatesTheErrorMsgAndUserClicksOnOkButton(String errormsg) throws InterruptedException {
				changeInvoiceQuantity.userValidatesTheErrorMsgAndUserClicksOnOkButton(errormsg);
				
	}	
		
		
		@Then("User clicks on Reset button")
		public void UserClicksOnResetButton() throws InterruptedException {
			changeInvoiceQuantity.UserClicksOnResetButton();
		
		}
		
		@Then ("User enters in the text field. User clicks on Search button{string}")
		public void userEntersInTheTextFieldAndClicksOnSearchButton(String AWB) throws InterruptedException {
			changeInvoiceQuantity.userEntersInTheTextFieldAndClicksOnSearchButton(AWB);
		}
		
		@When("User enters shippingid of complete booking {string} in the text field and User clicks on Search button")
		public void userEntersShippingidOfCompleteBookingInTheTextFieldAndUserClicksOnSearchButton(String AWB) throws InterruptedException {
			changeInvoiceQuantity.userEntersShippingidOfCompleteBookingInTheTextFieldAndUserClicksOnSearchButton(AWB);
}
		@Then("User validates the Error msg and User clicks on Ok button {string}")
		public void userValidatesTheErrorMsgAndUserClicksOnOkButton1(String Errormsg) throws InterruptedException {
			changeInvoiceQuantity.userValidatesTheErrorMsgAndUserClicksOnOkButton1(Errormsg);

  
    	
    }
		@Then("User removes pre filled invoice quantity and enters a new quantity {string}")
		public void userRemovesPreFilledInvoiceQuantityAndEntersaNewQuantity(String ChangeInvoicevalue) throws InterruptedException {
			changeInvoiceQuantity.userRemovesPreFilledInvoiceQuantityAndEntersaNewQuantity(ChangeInvoicevalue);
		
		
		}


	  @When ("User clicks on Search button without entering shipping id")	
	public void userClicksOnSearchButtonWithoutEnteringShippingid() throws InterruptedException{
		  changeInvoiceQuantity.userClicksOnSearchButtonWithoutEnteringShippingid();  
	  }
	  
	 @Then ("User validates an error msg and User clicks on Ok button {string}")
	 public void userValidatesErrorMsgAndUserClicksOnOkButton(String errormessage) throws InterruptedException {
		 changeInvoiceQuantity.userValidatesErrorMsgAndUserClicksOnOkButton(errormessage);
		 
	 }
	 

}