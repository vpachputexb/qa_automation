package starter.stepdefinitions;



import java.io.IOException;

import com.xb.cafe.ui.pages.Dashboard;
import com.xb.cafe.ui.pages.Dashboard;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Dashboard_StepDef {

//	@Given("I login in Cargo Web UI with following credentials")
//	public void cargo_Login(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
//		Cargo.cargoLogin(dataTable);
//	}
	@Steps
	Dashboard Dashboard;
	
	@Given("User reads Total client value")
	public void userReadsTotalClientValue() throws InterruptedException {
		Dashboard.userReadsTotalClientValue();
	}
	
	@Then("User navigates to client listing")
	public void userNavigatesToClientListing() throws InterruptedException {
		Dashboard.userNavigatesToClientListing();
	}
	@Then("User reads Total records value")
	public void userReadsTotalRecordsValue() throws InterruptedException {
		Dashboard.userReadsTotalRecordsValue();
	}
	@Then("User compares Total client value and Total records value")
	public boolean userComparesTotalClientValueAndTotalRecordsValue() throws InterruptedException {
		Dashboard.userComparesTotalClientValueAndTotalRecordsValue();
		return false;
	}
	@When("User clicks on active chart")
	public void userClicksOnActiveChart() throws InterruptedException {
		Dashboard.userClicksOnActiveChart();
	}
	@Then("User Navigates to Client dashboard option")
	public void userNavigatesToClientDashboardOption() throws InterruptedException {
		Dashboard.userNavigatesToClientDashboardOption();
	}
	@Then("User compares Total active client value and Total active records value")
	public boolean userComparesTotalActiveClientValueAndTotalActiveRecordsValue() throws InterruptedException {
		Dashboard.userComparesTotalActiveClientValueAndTotalActiveRecordsValue();
		return false;
		
	}
	@When("User clicks on inactive chart")
	public void userClicksOnInactiveChart() throws InterruptedException {
		Dashboard.userClicksOnInactiveChart();
	}
	@Then("User compares Total inactive client value and Total inactive records value")
	public boolean userComparesTotalInactiveClientValueAndTotalInactiveRecordsValue() throws InterruptedException {
		Dashboard.userComparesTotalInactiveClientValueAndTotalInactiveRecordsValue();
		return false;
}
	@When("User clicks on active bar chart")
	public void userClicksOnActiveBarChart() throws InterruptedException {
		Dashboard.userClicksOnActiveBarChart();
	}
	@When("User clicks on inactive bar chart")
	public void userClicksOnInactiveBarChart() throws InterruptedException {
		Dashboard.userClicksOnInactiveBarChart();
	}
}	
	