package starter.stepdefinitions;



import com.xb.cafe.ui.pages.Cargo_UpdateInvoice;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.webdriver.SupportedWebDriver;

public class UpdateInvoiceDetails_Stepdef {
	
	@Steps
	Cargo_UpdateInvoice cargoupdateinvoicedetails;

	@Managed
	SupportedWebDriver browser;

	@Given("User Navigates to Update invoice details")
	public void userNavigatesToUpdateInvoiceDetails() throws InterruptedException {
		cargoupdateinvoicedetails.userNavigatesToUpdateInvoiceDetails();
	}
	
	@When("User Enters awb number {string}")
	public void userEntersAwbNumber(String awbsearch) throws InterruptedException {
		cargoupdateinvoicedetails.userEntersAwbNumber(awbsearch);
		
	}
	@Then("Click on search button and and Validate the button functionality")
	public void clickOnSearchButtonAndValidateTheButtonFunctionality()throws InterruptedException {
		cargoupdateinvoicedetails.clickOnSearchButtonAndValidateTheButtonFunctionality();
	}
	@Then("Click on reset button and validate the error message {string}")
	public void clickOnResetButtonAndValidateTheErrorMessage(String awbErrorMessage)throws InterruptedException {
		cargoupdateinvoicedetails.clickOnResetButtonAndValidateTheErrorMessage(awbErrorMessage);
		
	}
	@Then("User clicks on search button and validate the error message {string}")
	public void userClicksOnSearchButtonAndValidateTheErrorMessage(String awbErrorMessage)throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnSearchButtonAndValidateTheErrorMessage(awbErrorMessage);
	}
	@Then("User clicks on validate ewb button and validate the blank ewb input {string}")
	public void userClicksOnValidateEwbButtonAndValidateTheBlankEwbInput(String ewbErrorMessage)throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnValidateEwbButtonAndValidateTheBlankEwbInput(ewbErrorMessage);
	}
	@Then("User clicks on search button and validate the data error message {string}")
	public void userClicksOnSearchButtonAndValidateTheDataErrorMessage(String awbDataErrorMessage)throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnSearchButtonAndValidateTheDataErrorMessage(awbDataErrorMessage);
	}
	@When("User Clicks on validate ewb button and clicks on information icon")
	public void userClicksOnValidateEwbButtonAndClicksOnInformationIcon()throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnValidateEwbButtonAndClicksOnInformationIcon();
		
	}
	@Then("User Clicks on save button and validates the invalid invoice details {string}")
	public void userClicksOnSaveButtonAndValidatesTheInvalidInvoiceDetails(String invalidInvoiceErrorMessage)throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnSaveButtonAndValidatesTheInvalidInvoiceDetails(invalidInvoiceErrorMessage);
	}
	@When("User Clicks on plus button and enters the invoice details {string},{string},{string}")
	public void userClicksOnPlusButtonAndEntersTheInvoiceDetails(String invoiceNumber, String invoiceValue, String ewbNumber)throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnPlusButtonAndEntersTheInvoiceDetails(invoiceNumber,invoiceValue,ewbNumber);
	}
	@Then("validate the duplicate invoice details {string}")
	public void validateTheDuplicateInvoiceDetails(String duplicateInvoiceErrorMessage)throws InterruptedException {
		cargoupdateinvoicedetails.validateTheDuplicateInvoiceDetails(duplicateInvoiceErrorMessage);
	}
	@When("User clicks on plus button and delete button")
	public void userClicksOnPlusButtonAndDeleteButton()throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnPlusButtonAndDeleteButton();
	}
	@Then("validate the invoice value and Invoice number {string},{string}")
	public void validateTheInvoiceValueAndInvoiceNumber(String invoiceNumberErrorMessage, String invoiceValueErrorMessage)throws InterruptedException {
		cargoupdateinvoicedetails.validateTheInvoiceValueAndInvoiceNumber(invoiceNumberErrorMessage,invoiceValueErrorMessage);
		
	}
	@When("User clicks on validate ewb button")
	public void userClicksOnValidateEwbButton()throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnValidateEwbButton();
	}
	@Then("User clicks on validate ewb button and validate the click functionality")
	public void userClicksOnValidateEwbButtonAndValidateTheClickFunctionality()throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnValidateEwbButtonAndValidateTheClickFunctionality();
	}
	@Then("User enters the invalid ewb number and validates the ewb number {string},{string}")
	public void userEntersTheInvalidEwbNumberAndValidatesTheEwbNumber(String ewbNumberOne, String invalidEwbNumberMessage)throws InterruptedException {
		cargoupdateinvoicedetails.userEntersTheInvalidEwbNumberAndValidatesTheEwbNumber(ewbNumberOne,invalidEwbNumberMessage);		
	}
	@Then("User clicks on information icon and validate EWB expiration message {string}")
	public void userClicksOnInformationIconAndValidateEWBExpirationMessage(String ewbexpirationmessage)throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnInformationIconAndValidateEWBExpirationMessage(ewbexpirationmessage);
	}
	@When("User enters the invoice details {string},{string},{string}")
	public void userEntersTheInvoiceDetails(String invoiceNumber, String invoiceValue, String ewbNumber)throws InterruptedException {
		cargoupdateinvoicedetails.userEntersTheInvoiceDetails(invoiceNumber,invoiceValue,ewbNumber);
	}
	@When("User enters ewb number {string}")
	public void userEntersEwbNumber(String ewbNumber)throws InterruptedException {
		cargoupdateinvoicedetails.userEntersEwbNumber(ewbNumber);
	}
	@Then("User clicks on information icon and validate following error message {string},{string},{string}")
	public void userClicksOnInformationIconAndValidateFollowingErrorMessage(String errorMessageInvoiceNumber, String errorMessageInvoiceValue, String partberrormessage)throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnInformationIconAndValidateFollowingErrorMessage(errorMessageInvoiceNumber,errorMessageInvoiceValue,partberrormessage);
	}
	@Then("click on validate ewb button and click on save button {string}")
	public void clickOnValidateEwbButtonAndClickOnSaveButton(String successfulMessage)throws InterruptedException {
		cargoupdateinvoicedetails.clickOnValidateEwbButtonAndClickOnSaveButton(successfulMessage);
	}
	@When("User clicks on search button")
	public void userClicksOnSearchButton()throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnSearchButton();
	}
	@When("User clicks on delete button1")
	public void userClicksOnDeleteButton()throws InterruptedException {
		cargoupdateinvoicedetails.userClicksOnDeleteButton();
	}
	@Then("validate and click on save button {string}")
	public void validateAndClickOnSaveButton(String successfulMessage)throws InterruptedException {
		cargoupdateinvoicedetails.validateAndClickOnSaveButton(successfulMessage);
	}
}

