/*package starter;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)

@CucumberOptions(
        plugin = {"pretty"},
        features = {"src/test/resources/features"},
        glue = {"src/test/java"},
       // tags = {"@Cargo"},
        monochrome = true)

public class CucumberTestSuite {}

*/


package starter;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = { "pretty"}, 
	features = "src/test/resources/features/", 
	tags = "", 
	glue = {}, 
	monochrome = true, 
	publish = true, 
	stepNotifications = true)

public class CucumberTestSuite {}




//@RunWith(CucumberWithSerenity.class)
//@CucumberOptions(plugin = { "pretty" }, 
//features = "src/test/resources/features/",
//		tags = "@Login/LogoutTest or @SmokeTest or @RegDock or @ReportValidation", 
//		glue = {"stepdefinitions", "com.mobileTestCases" }, 
//		monochrome = true, 
//		publish = true, 
//		stepNotifications = true)