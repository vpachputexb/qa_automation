package com.payloads;

import com.apis.MobilePickupAPIs_MultipleAWBs;
import com.apis.MobilePickupApis;

public class MobilePickUpAPIs_MultipleAWB_Payload {

	MobilePickupAPIs_MultipleAWBs mobilePickupApisMultiple;
	
	public String logoutPayload() {
		return "{\"userid\":\""+mobilePickupApisMultiple.userID +"\",\"appkey\":\"$UNF#APP&MOB$\"}";
	}
	
	public  String mobileLogin() {
		return "{\"username\":\""+mobilePickupApisMultiple.username+"\",\"password\":\"Xpress@123\",\"appkey\":\"$UNF#APP&MOB$\",\"meta\":[{\"IMEINumber\":\"\",\"mac_address\":\"\",\"AdvertisingId\":\"cf79dd46-5380-4a80-af42-e683ab2f7ee0\",\"SecureId\":\"36021f201ed4dd93\",\"IsRooted\":false}]}";
	}
	
	public  String mobileLoginToken() {
		String a= mobilePickupApisMultiple.tokenLogin;
		return  "{\"token\":\""+ a +"\"}";
	}
	
	public String GetTripAllocationDetailsPayload() {
		String hubID=mobilePickupApisMultiple.hubID;
		return "{\"userid\":\""+mobilePickupApisMultiple.userID +"\",\"hubid\":\""+hubID+"\"}";
	}
	public String getAWBShippingDetailsPayload(String awb) {
		
		return "{\"parentawb\":\""+awb+"\",\"tripid\":\""+mobilePickupApisMultiple.tripID+"\"}" ;
		

	}
	
	public String saveMPSDetailsPayload(String awb) {
		return "{\"awbnumber\":\""+awb+"\",\"tripid\":\""+mobilePickupApisMultiple.tripID+"\",\"totalmps\":\"1\",\"goodmps\":\"1\",\"damagemps\":\"0\",\"appointmentdate\":\"\",\"malldelivery\":\"0\",\"pickuseremail\":\"13645\",\"pickuserid\":\"13645\",\"totalphysicalweight\":100}";
		
	}
	public String scanMPSDetailsPayload(String awb,String mps) {
		return "{\"parentawb\":\""+awb+"\",\"mps\":\""+mps+"\",\"userid\":\""+mobilePickupApisMultiple.tripID+"\",\"hubid\":\""+mobilePickupApisMultiple.hubID+"\",\"lastmodifiedby\":\""+mobilePickupApisMultiple.userID+"\",\"tripid\":\""+mobilePickupApisMultiple.tripID+"\",\"type\":\"FM\"}";
		
	}
	public String pickUpProofSubmitPayload(String awb) {
		return "{\"awb\":\""+awb+"\",\"tripid\":\""+mobilePickupApisMultiple.tripID+"\",\"mpsquantity\":\"1\",\"podurl\":{\"destinationpodimg\":\"[\\\"https:\\/\\/xbeestest.s3.amazonaws.com\\/CargoAppFiles\\/CargoMobile\\/Pickup\\/94185221112346.jpg\\\"]\"},\"lastmodifiedby\":\""+mobilePickupApisMultiple.tripID+"\"}";
		
	}
	
	public String webLoginDeliveryPayload() {
		return "{\"username\":\"admin.atul.deokar@xpressbees.com\",\"password\":\"Xpress@123\"}";
		
	}
	public String viewTripDetailsDelivery_Payload() {
		return "{\"tripid\":\""+mobilePickupApisMultiple.tripID+"\"}";
		
	}
	
	
	
	
	
}
