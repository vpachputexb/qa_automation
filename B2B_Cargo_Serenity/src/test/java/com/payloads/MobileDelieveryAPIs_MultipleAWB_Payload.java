package com.payloads;

import com.apis.MobileDeliveryAPI_Multiple;
import com.apis.MobileDeliveryAPIs_MultipleAWBs;


public class MobileDelieveryAPIs_MultipleAWB_Payload {

	MobileDeliveryAPIs_MultipleAWBs mobileDeliveryAPIMultiple=new MobileDeliveryAPIs_MultipleAWBs();
	
	public String GetTripAllocationDetailsPayloadDelivery() {
		String hubID=mobileDeliveryAPIMultiple.hubIDDelivery;
		return "{\"userid\":\""+mobileDeliveryAPIMultiple.userIDDelivery +"\",\"hubid\":\""+hubID+"\"}";
		}
	
	public String scanMPSDetailsPayloadDelivery(String awb,String mps) {
		return "{\"parentawb\":\""+awb+"\",\"mps\":\""+mps+"\",\"userid\":\"24422\",\"hubid\":117,\"lastmodifiedby\":\"24422\",\"tripid\":\""+mobileDeliveryAPIMultiple.tripIDDelivery+"\",\"type\":\"LM\"}";
		
	}
	public String lastMilePOD_Payload(String awb) {
		return "{\"awb\":\""+awb+"\",\"tripid\":\""+mobileDeliveryAPIMultiple.tripIDDelivery+"\",\"mpsquantity\":1,\"podidproof\":[{\"idname\":\"AadhaarCard\",\"image\":\"https:\\/\\/xbeestest.s3.amazonaws.com\\/CargoAppFiles\\/CargoMobile\\/IdProof\\/20220208_0718.jpg\",\"idnumber\":\"123456\"}],\"podimages\":\"[\\\"https:\\/\\/xbeestest.s3.amazonaws.com\\/CargoAppFiles\\/CargoMobile\\/Delivery\\/20220208_071825.jpg\\\"]\",\"lastmodifiedby\":\"13645\",\"receivername\":\"RahulThorat\",\"receivermobile\":\"7972355031\"}";
	}
	public String saveDelivery_Payload(String awb) {
		return "{\"parentawb\":\""+awb+"\",\"hubid\":\""+mobileDeliveryAPIMultiple.hubIDDelivery+"\",\"lastmodifiedby\":\"13645\",\"tripid\":\""+mobileDeliveryAPIMultiple.tripIDDelivery+"\",\"allocationtype\":\"LM\"}";
		
	}
	public String webLoginDeliveryPayload() {
		return "{\"username\":\"admin.atul.deokar@xpressbees.com\",\"password\":\"Xpress@123\"}";
		
	}
	public String viewTripDetailsDelivery_Payload() {
		return "{\"tripid\":\""+mobileDeliveryAPIMultiple.tripIDDelivery+"\"}";
		
	}
	public String awbverifyPayload(String awb,String mps) {
		
		return "{\n"
				+ "     \"ShippingID\":\""+awb+"\",\n"
				+ "     \"MeasurementMethod\": \"CM\",\n"
				+ "     \"MPSNo\": \""+mps+"\",\n"
				+ "     \"DimentionUOM\": \"M\",\n"
				+ "     \"Length\": \"10.6\",\n"
				+ "     \"Width\": \"6.1\",\n"
				+ "     \"Height\": \"10\",\n"
				+ "     \"CFT\":\"8\",\n"
				+ "     \"PhyWeight\": 11,\n"
				+ "     \"ChargeableWeight\": 5.055,\n"
				+ "     \"VolWeight\": \"15\",\n"
				+ "     \"MeasurementLocation\": \"876\",\n"
				+ "     \"MeasurementDoneBy\": \"user\",\n"
				+ "     \"MPSStatus\": \"pendding\"\n"
				+ "}";
				}
}
