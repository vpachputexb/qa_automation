package com.payloads;

import com.apis.Hubops_APIs;
import com.apis.MobileDeliveryAPI;
import com.xb.cafe.ui.pages.Cargo_FLM_TripManagement;

public class Hubops_payload {
	Cargo_FLM_TripManagement cargo_FLM_TripManagement=new Cargo_FLM_TripManagement();
	Hubops_APIs hubops_API=new Hubops_APIs();
	String mpsGenerated=cargo_FLM_TripManagement.mpsGenerated;
	 String awbGenerated=cargo_FLM_TripManagement.awbBookingGenerated;
	 
	 
	 public String markSchedule_Payload() {
		return "{\"ShippingID\":\""+mpsGenerated+"\",\"ShipmentStatus\":\"Scheduled\",\"UserProcess\":\"Scheduled\",\"LastModifiedBy\":\"USERtestt66\",\"CurrentHubId\":\"117\",\"ProcessCode\":\"P018\",\"Comments\":\"yy\",\"Parentawbno\":\""+awbGenerated+"\",\"ConnectionScheduledMasterId\":\"8620\",\"StatusMarkedFrom\":\"Web\"}";
		 
	 }
	 
	 public String markOutScan_Payload() {
		return "{\"ShippingID\":\""+mpsGenerated+"\",\"ShipmentStatus\":\"OutScan\",\"UserProcess\":\"ShipmentOutScan\",\"LastModifiedBy\":\"atul.deokar@xpressbees.com\",\"CurrentHubId\":\"117\",\"ProcessCode\":\"P018\",\"Comments\":\"testing\",\"Parentawbno\":\""+awbGenerated+"\",\"ConnectionScheduledMasterId\":\"8620\",\"StatusMarkedFrom\":\"Web\"}";
		 
	 }
	 public String markInScan_Payload() {
		return "{\"ShipmentStatus\":\"InScan\",\"ShippingID\":\""+mpsGenerated+"\",\"CurrentHubID\":\"5\",\"NextHubId\":0,\"LastModifiedBy\":\"testuser\",\"Comments\":\"Overage\",\"IsManualInScan\":\"\"}";
		 
	 }
	

}
