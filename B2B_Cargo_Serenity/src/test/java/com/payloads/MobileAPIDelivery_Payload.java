package com.payloads;

import com.apis.MobileDeliveryAPI;
import com.apis.MobileDeliveryAPI_Multiple;
import com.xb.cafe.ui.pages.Cargo_FLM_TripManagement;

public class MobileAPIDelivery_Payload {
	Cargo_FLM_TripManagement cargo_FLM_TripManagement=new Cargo_FLM_TripManagement();
	MobileDeliveryAPI_Multiple mobileDeliveryAPI=new MobileDeliveryAPI_Multiple();
	 String mpsGenerated=cargo_FLM_TripManagement.mpsGenerated;
	 String awbGenerated=cargo_FLM_TripManagement.awbBookingGenerated;
	 
	 
//	 String mpsGenerated="MPS372922230673";
//	 String awbGenerated="9422072101139";
//	
	public String awbverifyPayload(String mps) {
		
		return "{\n"
				+ "     \"ShippingID\":\""+awbGenerated+"\",\n"
				+ "     \"MeasurementMethod\": \"CM\",\n"
				+ "     \"MPSNo\": \""+mps+"\",\n"
				+ "     \"DimentionUOM\": \"M\",\n"
				+ "     \"Length\": \"10.6\",\n"
				+ "     \"Width\": \"6.1\",\n"
				+ "     \"Height\": \"10\",\n"
				+ "     \"CFT\":\"8\",\n"
				+ "     \"PhyWeight\": 11,\n"
				+ "     \"ChargeableWeight\": 5.055,\n"
				+ "     \"VolWeight\": \"15\",\n"
				+ "     \"MeasurementLocation\": \"876\",\n"
				+ "     \"MeasurementDoneBy\": \"user\",\n"
				+ "     \"MPSStatus\": \"pendding\"\n"
				+ "}";
				}
	
	
	public String GetTripAllocationDetailsPayloadDelivery() {
		String hubID=mobileDeliveryAPI.hubIDDelivery;
		return "{\"userid\":\""+mobileDeliveryAPI.userIDDelivery +"\",\"hubid\":\""+hubID+"\"}";
		}
	
	public String getPickUpLocation_DeliveryPayload() {
		
		return "{\"clientid\":\""+mobileDeliveryAPI.clientIDDelivery+"\",\"pincode\":\"411054\",\"ClientBusinessAccountId\":\""+mobileDeliveryAPI.clientBussinessIDDelivery+"\"}";
		
	}
	public String saveMPSDetailsPayloadDelivery() {
		return "{\"awbnumber\":\""+mobileDeliveryAPI.awbNumberDelivery+"\",\"tripid\":\""+mobileDeliveryAPI.tripIDDelivery+"\",\"totalmps\":\"2\",\"goodmps\":\"2\",\"damagemps\":\"0\",\"appointmentdate\":\"\",\"malldelivery\":\"0\",\"pickuseremail\":\"13645\",\"pickuserid\":\"13645\",\"totalphysicalweight\":100}";
		
	}
	public String scanMPSDetailsPayloadDelivery(String mps) {
		return "{\"parentawb\":\""+mobileDeliveryAPI.awbNumberDelivery+"\",\"mps\":\""+mps+"\",\"userid\":\"24422\",\"hubid\":117,\"lastmodifiedby\":\"24422\",\"tripid\":\""+mobileDeliveryAPI.tripIDDelivery+"\",\"type\":\"LM\"}";
		
	}
	public String lastMilePOD_Payload() {
		return "{\"awb\":\""+mobileDeliveryAPI.awbNumberDelivery+"\",\"tripid\":\""+mobileDeliveryAPI.tripIDDelivery+"\",\"mpsquantity\":2,\"podidproof\":[{\"idname\":\"AadhaarCard\",\"image\":\"https:\\/\\/xbeestest.s3.amazonaws.com\\/CargoAppFiles\\/CargoMobile\\/IdProof\\/20220208_0718.jpg\",\"idnumber\":\"123456\"}],\"podimages\":\"[\\\"https:\\/\\/xbeestest.s3.amazonaws.com\\/CargoAppFiles\\/CargoMobile\\/Delivery\\/20220208_071825.jpg\\\"]\",\"lastmodifiedby\":\"13645\",\"receivername\":\"RahulThorat\",\"receivermobile\":\"7972355031\"}";
	}
	
	public String saveDelivery_Payload() {
		return "{\"parentawb\":\""+mobileDeliveryAPI.awbNumberDelivery+"\",\"hubid\":\""+mobileDeliveryAPI.hubIDDelivery+"\",\"lastmodifiedby\":\"13645\",\"tripid\":\""+mobileDeliveryAPI.tripIDDelivery+"\",\"allocationtype\":\"LM\"}";
		
	}
	public String getAWBShippingDetailsPayload() {
		
		return "{\"parentawb\":\""+mobileDeliveryAPI.awbNumberDelivery+"\",\"tripid\":\""+mobileDeliveryAPI.tripIDDelivery+"\"}" ;
		

	}
	public String webLoginDeliveryPayload() {
		return "{\"username\":\"admin.atul.deokar@xpressbees.com\",\"password\":\"Xpress@123\"}";
		
	}
	public String viewTripDetailsDelivery_Payload() {
		return "{\"tripid\":\""+mobileDeliveryAPI.tripIDDelivery+"\"}";
		
	}
	
	
}

