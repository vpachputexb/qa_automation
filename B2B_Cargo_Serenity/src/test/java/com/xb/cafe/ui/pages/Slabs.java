package com.xb.cafe.ui.pages;

import java.util.List;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;



import net.serenitybdd.core.pages.PageObject;

public class Slabs extends PageObject {

	 public static final String UserValidatesTheErrorMsgAndUserClicksOnOkButton = null;
	private static final CharSequence HigherBreakLimitValue = null;
	String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	 String username;
	 String TagName1;
	 String TagComment1;
	 String Tag_MappedEnvironment;
	//  WebDriver driver = null;

	public  final By input_UN = By.xpath("//input[@name='email']");
	public  final By input_PW = By.xpath("//input[@id='typepass']");
	public  final By btn_Login = By.xpath("//button[contains(text(),'Login')]");
	public  final By Lnk_MasterData = By.xpath("//span[contains(text(),'Master Data')]");
	public  final By lnk_Slabs = By.xpath("//span[contains(text(),'Slabs')]");
	public  final By AddNewSlab_Bttn = By.xpath("//a[@title='Add New Slab']");
	public  final By SlabTxtinput = By.xpath("//input[@id='slabName']");
	public  final By BreakHigherLimitLocator = By.xpath("//select[@id='higherLimit0']");
	public  final By BreakHigherLimitValueLocator = By.xpath("//input[@id='higherValue0']");
	public final  By btn_Submit = By.xpath("//button[@class=\"-btn btn-success\"]");
	public final By Success_msg = By.xpath("//div[@id='swal2-content']");
	public final By btn_Ok = By.xpath("//button[contains(text(),'OK')]");
	public final By btn_Plus = By.xpath("//tbody/tr[1]/td[6]/i[1]");
	public final By BreakHigherLimitLocator1 = By.xpath("//select[@id='higherLimit1']");
	public final By BreakHigherLimitValueLocator1 = By.xpath("//input[@id='higherValue1']");
	public final By btn_Inactive = By.xpath("//button[contains(text(),'Inactive')]");
	public final By ActivationSuccess_msg = By.xpath("//div[@id='swal2-content']");
	public  final By filterLocator = By.xpath("//i[@title='Search Filter']");
	public final By btn_View = By.xpath("//tbody/tr[@id='70']/td[11]/span[2]/img[1]");
	public final By SlabNameLocator = By.xpath("//strong[contains(text(),'Slab Name :')]");
	public final By SlabDescriptionLocator = By.xpath("//strong[contains(text(),'Slab Description :')]");
	public final By SlabTypeLocator = By.xpath("//strong[contains(text(),'Slab Type :')]");
	public final By SlabSubTypeLocator = By.xpath("//strong[contains(text(),'Slab Sub Type :')]");
	public final By SlabBreaksLocator = By.xpath("//strong[contains(text(),'Slab Breaks :')]");
	public final By btn_Active = By.xpath("//button[contains(text(),'Active')]");		
	public final By InactivationSuccess_msg = By.xpath("//div[@id='swal2-content']"); 
	public final By Errormsg_Blankslab = By.xpath("//span[contains(text(),'Please enter Slab Name.')]");
	public final By Errormsg_Blankslabtype = By.xpath("//span[contains(text(),'Please select Slab Type.')]");
	public final By FilterStatus_Locator = By.xpath("//div[@class='item']");
	public final By FilterStatusActive_Locator = By.xpath("//div[contains(text(),'Active')]");
	public final By FilterStatusInactive_Locator = By.xpath("//div[contains(text(),'Inactive')]");
	public final By NoRecordsFound_Locator = By.xpath("//td[contains(text(),'No Records Found')]");
	public final By btn_delete = By.xpath("//tbody/tr[2]/td[6]/i[1]");
	public final By Deleteconfirm_Locator = By.xpath("//div[contains(text(),'Remove selected slab breakup.')]");
	public final By btn_Cancel = By.xpath("//button[contains(text(),'Cancel')]");
	
	SoftAssert softAssert = new SoftAssert();
	
	

	public  void LoginSelfServicepage(String URL) throws InterruptedException {
		getDriver().get(URL);
	}

	
	public void userNavigatesToMasterDataOption() throws InterruptedException {
		Thread.sleep(3000);
        getDriver().navigate().refresh();
        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
        try {
                getDriver().navigate().refresh();
                Thread.sleep(2000);
                wait.until(ExpectedConditions.elementToBeClickable(lnk_Slabs));
                getDriver().findElement(lnk_Slabs).click();
                Thread.sleep(2000);
        } catch (Exception e) {
                getDriver().navigate().refresh();
                Thread.sleep(2000);
                wait.until(ExpectedConditions.elementToBeClickable(Lnk_MasterData));
                getDriver().findElement(Lnk_MasterData).click();
                Thread.sleep(2000);
                wait.until(ExpectedConditions.elementToBeClickable(lnk_Slabs));
                getDriver().findElement(lnk_Slabs).click();
        }
	}
     public void userNavigatesToCreateNewSlabOption() throws InterruptedException {
    	 Thread.sleep(5000);
 		getDriver().findElement(AddNewSlab_Bttn).click();
 		
 	}
	
	public void userEntersInTheTextFieldAndUserSelectsSlabTypeInvoiceAmount(String SlabName) throws InterruptedException {
	
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated( SlabTxtinput));
		WebElement slabsearchTxt=getDriver().findElement( SlabTxtinput);
		slabsearchTxt.sendKeys(SlabName);
		Thread.sleep(1000);
		slabsearchTxt.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("//label[contains(text(),'Invoice Amount (INR)')]")).click();
		}


	public void userSelectsHigherBreakLimitTypeAndEnterHigherBreakLimitValue(String BreakHigherLimitType,String HigherBreakLimitValue) throws InterruptedException {
WebDriverWait wait = new WebDriverWait(getDriver(),10);
		
Thread.sleep(1000);
getDriver().findElement(BreakHigherLimitLocator).click();
List<WebElement> listLimitType=getDriver().findElements(By.xpath("//*[@name='higherLimit0']/option"));

for(int i=0;i<=listLimitType.size()-1;i++) {
	System.out.println(listLimitType.get(i).getText());
	if(listLimitType.get(i).getText().equals(BreakHigherLimitType)) {
		listLimitType.get(i).click();
		break;
	}
}
			getDriver().findElement(BreakHigherLimitValueLocator).sendKeys(HigherBreakLimitValue);
			
			}


public void userClicksOnSaveButton() {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(btn_Submit).click();
		
	}


	public void userValidatesSuccessfulMsgAndClicksOnOk(String successmsg) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		
	softAssert.assertEquals(getDriver().findElement(Success_msg).getText().trim(), successmsg,"Record created successfully.");
		wait.until(ExpectedConditions.elementToBeClickable(btn_Ok));
		getDriver().findElement(btn_Ok).click();
}
	public void userSelectsHigherBreakLimitTypeForSecondRowAndEnterHigherBreakLimitValue(String BreakHigherLimitType1,String HigherBreakLimitValue1) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
				
		Thread.sleep(1000);
		getDriver().findElement(BreakHigherLimitLocator1).click();
		List<WebElement> listLimitType=getDriver().findElements(By.xpath("//*[@name='higherLimit1']/option"));

		for(int i=0;i<=listLimitType.size()-1;i++) {
			System.out.println(listLimitType.get(i).getText());
			if(listLimitType.get(i).getText().equals(BreakHigherLimitType1)) {
				listLimitType.get(i).click();
				break;
			}
		}
		getDriver().findElement(BreakHigherLimitValueLocator1).sendKeys(HigherBreakLimitValue1);
		
		}
	

	public void userClicksOnPlusSign() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(btn_Plus).click();
	}


	public void userClicksOnInactiveStatusButtonAndClicksOnOkButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		wait.until(ExpectedConditions.elementToBeClickable(btn_Inactive));
		getDriver().findElement(btn_Inactive).click();
		
	}


	public void userValidatesActivationSuccessfulMsgAndClickOnOk(String ActivationSuccessmsg) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		
		softAssert.assertEquals(getDriver().findElement(ActivationSuccess_msg).getText().trim(), ActivationSuccessmsg,"Slab Activated Successfully!!");
			wait.until(ExpectedConditions.elementToBeClickable(btn_Ok));
			getDriver().findElement(btn_Ok).click();
	}


	public void userEntersInTheTextFieldAndUserSelectsSlabTypeWeightAndWeightTypeBillableWeight(String SlabName1) throws InterruptedException {
WebDriverWait wait = new WebDriverWait(getDriver(),10);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated( SlabTxtinput));
		WebElement slabsearchTxt=getDriver().findElement( SlabTxtinput);
		slabsearchTxt.sendKeys(SlabName1);
		Thread.sleep(1000);
		slabsearchTxt.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		
		getDriver().findElement(By.xpath("//label[contains(text(),'Weight (kg)')]")).click();
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("//label[contains(text(),'Billable Weight')]")).click();
		
	}


	public void userClicksOnFilterOption() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(filterLocator).click();
		
	}


	public void userEntersInTextFieldAndClicksOnSearchButton(String slabsname) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		wait.until(ExpectedConditions.visibilityOfElementLocated( SlabTxtinput));
		WebElement slabsearchTxt=getDriver().findElement( SlabTxtinput);
		slabsearchTxt.sendKeys(slabsname);
		slabsearchTxt.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		getDriver().findElement(btn_Submit).click();
	}


	public void userClicksOnViewButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(btn_View).click();
	}
	public void userClicksOnActiveStatusButtonAndClicksOnOkButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(btn_Active).click();
		getDriver().findElement(btn_Ok).click();
	}
	

public void userVerifiesBlankSlabErrorMsg(String blankSlabErrorMsg) throws InterruptedException {
WebDriverWait wait = new WebDriverWait(getDriver(),10);
	
	softAssert.assertEquals(getDriver().findElement(Errormsg_Blankslab).getText().trim(), blankSlabErrorMsg,"Please enter Slab Name.");
	
}


public void userVerifiesBlankSlabTypeErrorMsg(String blankSlabTypeErrorMsg) throws InterruptedException {
WebDriverWait wait = new WebDriverWait(getDriver(),10);
	
	softAssert.assertEquals(getDriver().findElement(Errormsg_Blankslabtype).getText().trim(), blankSlabTypeErrorMsg,"Please select Slab Type.");
	
	
}


public void userEntersInTextField(String slabsname1) throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(getDriver(),10);
	wait.until(ExpectedConditions.visibilityOfElementLocated( SlabTxtinput));
	WebElement slabsearchTxt=getDriver().findElement( SlabTxtinput);
	slabsearchTxt.sendKeys(slabsname1);
	slabsearchTxt.sendKeys(Keys.ENTER);
	
}


public void userClicksOnSearchButton() throws InterruptedException {
	getDriver().findElement(btn_Submit).click();
	
}


public void userVerifiesMsg(String msg) throws InterruptedException {
WebDriverWait wait = new WebDriverWait(getDriver(),10);
	
	softAssert.assertEquals(getDriver().findElement(NoRecordsFound_Locator).getText().trim(), msg,"No Records Found");
	
}


public void userEntersInTheTextFieldAndUserSelectsSlabTypeQuantity(String SlabName2) throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(getDriver(),10);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated( SlabTxtinput));
	WebElement slabsearchTxt=getDriver().findElement( SlabTxtinput);
	slabsearchTxt.sendKeys(SlabName2);
	Thread.sleep(1000);
	slabsearchTxt.sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	getDriver().findElement(By.xpath("//label[contains(text(),'Quantity (Number of Boxes)')]")).click();
	}


public void userClicksOnOkButton() throws InterruptedException {
	getDriver().findElement(btn_Ok).click();
	
}


public void userClicksOnSlabDeleteButtonAndVerifiesMsg(String Msg1) throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(getDriver(),10);
	getDriver().findElement(btn_delete).click();
	softAssert.assertEquals(getDriver().findElement(Deleteconfirm_Locator).getText().trim(), Msg1,"Remove selected slab breakup.");
}


public void userEntersInTheTextFieldAndUserSelectsSlabTypeDistance(String SlabName3) throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(getDriver(),10);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated( SlabTxtinput));
	WebElement slabsearchTxt=getDriver().findElement( SlabTxtinput);
	slabsearchTxt.sendKeys(SlabName3);
	Thread.sleep(1000);
	slabsearchTxt.sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	getDriver().findElement(By.xpath("//label[contains(text(),'Distance (km)')]")).click();
	}


public void userClicksOnCancelButton() throws InterruptedException {
	getDriver().findElement(btn_Cancel).click();
	
	
}



}


	

	


