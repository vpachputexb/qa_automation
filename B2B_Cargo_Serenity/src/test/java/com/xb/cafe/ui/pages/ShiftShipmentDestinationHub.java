package com.xb.cafe.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;

public class ShiftShipmentDestinationHub extends PageObject{
	public final By supportTabLocator=By.xpath("//span[contains(text(),'Support')]");
	public final By shiftShiomentDestinationHubLocator=By.xpath("//span[contains(text(),'Shift Shipment Destination Hub')]");
	public final By awbNumberInput=By.xpath("//textarea[@id='shipmentId']");
	public final By finalDestination=By.xpath("//div[contains(text(),'Select Destination Hub')]");
	public final By commentsInoutField=By.xpath("//textarea[@id='comments']");
	public final By restbutton=By.xpath("//button[@class='-btn btn-danger']");
	public final By updateButton =By.xpath("//button[@class='-btn btn-success']");
	public final By successmessagePopup=By.xpath("//div[contains(text(),'Destination Hub Successfully Updated')]");
	
	SoftAssert softAssert = new SoftAssert();
	
	public void navigateToShiftShipmentDestinationHubModule() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		try {
			getDriver().navigate().refresh();
			$(shiftShiomentDestinationHubLocator).waitUntilVisible().waitUntilClickable().click();
			$(restbutton).waitUntilVisible().waitUntilClickable().click();
			

		} catch (Exception e) {
			getDriver().navigate().refresh();
			$(supportTabLocator).waitUntilVisible().waitUntilClickable().click();
			$(shiftShiomentDestinationHubLocator).waitUntilVisible().waitUntilClickable().click();
			$(restbutton).waitUntilVisible().waitUntilClickable().click();
			Thread.sleep(3000);
		}
		
	}
	
	public void userEntersAWBNumber(String awbNumber) {
		$(awbNumberInput).waitUntilVisible().sendKeys(awbNumber);
	}
	public void userSelectsChangeDestinationHub(String changedHub) throws InterruptedException {
		getDriver().findElement(finalDestination).click();
		Thread.sleep(4000);
		List<WebElement> finaldestinationlist=getDriver().findElements(By.xpath("//div[@class='menu visible']/div"));
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		for(int i=0;i<=finaldestinationlist.size()-1;i++) {
			System.out.println(finaldestinationlist.get(i).getText());

			if(finaldestinationlist.get(i).getText().equalsIgnoreCase(changedHub)) {
				je.executeScript("arguments[0].scrollIntoView(true);",getDriver().findElement(By.xpath("//div[@class='menu visible']/div["+i+"]")));
				finaldestinationlist.get(i).click();
				break;
			}
		}
	}
	public void userEntersComments(String comments) {
		$(commentsInoutField).waitUntilVisible().sendKeys(comments);
		
	}
	public void userClicksOnUpdateButton() throws InterruptedException {
		Thread.sleep(1000);
		$(updateButton).waitUntilVisible().waitUntilClickable().click();
		
	}
	public void validateSuccessMessage(String successMessage) throws InterruptedException {
		Thread.sleep(1000);
		$(By.xpath("//div[contains(text(),'test')]".trim().replace("test", successMessage))).waitUntilVisible().waitUntilPresent().getText();
		softAssert.assertEquals(($(By.xpath("//div[contains(text(),'test')]".trim().replace("test", successMessage))).waitUntilVisible().waitUntilPresent().getText()),successMessage,"Success Message assertion failed");
		softAssert.assertAll();
	}
	public void validateErrorMessage(String errorAWBNumber, String errorFinalHub, String errorComments) throws InterruptedException {
		Thread.sleep(1000);
		softAssert.assertEquals(($(By.xpath("//span[contains(text(),'test')]".trim().replace("test", errorAWBNumber))).waitUntilVisible().waitUntilPresent().getText()),errorAWBNumber,"AWB Number error Message assertion failed");
		softAssert.assertEquals(($(By.xpath("//span[contains(text(),'test')]".trim().replace("test", errorFinalHub))).waitUntilVisible().waitUntilPresent().getText()),errorFinalHub,"Final Hub error Message assertion failed");
		softAssert.assertEquals(($(By.xpath("//span[contains(text(),'test')]".trim().replace("test", errorComments))).waitUntilVisible().waitUntilPresent().getText()),errorComments,"Comments error Message assertion failed");
		softAssert.assertAll();
		}
	public void validateSuccessmessageSamehub(String awbNumber, String errorMessage) throws InterruptedException {
		Thread.sleep(1000);
		softAssert.assertEquals(($(By.xpath("//div[contains(text(),'test')]".trim().replace("test", errorMessage+" : "+awbNumber))).waitUntilVisible().waitUntilPresent().getText()),errorMessage+" : "+awbNumber,"Error Message assertion failed");
		softAssert.assertAll();
	}
	public void validateErrorMessageformat(String errorMessage) {
		softAssert.assertEquals(($(By.xpath("//span[contains(text(),'test')]".trim().replace("test", errorMessage))).waitUntilVisible().getText()),errorMessage,"Format error Message assertion failed");
	
		softAssert.assertAll();
		}
	
	
	
	
	
		
}


