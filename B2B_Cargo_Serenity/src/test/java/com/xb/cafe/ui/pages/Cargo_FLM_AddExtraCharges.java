package com.xb.cafe.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.Assertion;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class Cargo_FLM_AddExtraCharges extends PageObject {
	
	public  final By firstMileLastMileLabelLocator=By.xpath("//span[contains(text(),'First & Last Mile')]");
	public  final By tripManagementLabelLocator=By.xpath("//span[contains(text(),'Trip Management')]");
	public final By  addExtraChargesLocator=By.xpath("//span[contains(text(),'Add Extra Charges')]");
	public final By  allocationTypeLocator=By.xpath("//div[@class='vue-treeselect__menu']//label");
	public final By shippingIdLocator=By.xpath("//input[@id='shippingid']");
	public final By fromDateLocator=By.xpath("//input[@name='fromdate']");
	public final By toDateLocator=By.xpath("//input[@name='todate']");
	public final By shipmentSizedChargesLocator=By.xpath("//input[@id='shippingsize']");
	public final By varaiChargesLocator=By.cssSelector("input[id=mathadi]");
	public final By portgageChargesLocator=By.xpath("//input[@id='portage']");
	public final By specialChargesLocator=By.xpath("//input[@id='specialcharge']");
	public final By loadChargesLocator=By.xpath("//input[@id='loadcharges']");
	public final By odaChargesLocator=By.xpath("//input[@id='oda']");
	public final By fullTruckLoadLocator=By.xpath("//input[@id='fulltruckload']");
	public final By saveButtonLocator=By.xpath("//button[contains(text(),'Save')]");
	public final By clearButtonOneLocator=By.xpath("(//button[contains(text(),'Clear')])[1]");
	public final By clearButtonTwoLocator=By.xpath("(//button[contains(text(),'Clear')])[2]");
	public final By SearchButtonLocator=By.xpath("//button[contains(text(),'Search')]");
	public final By exportButtonLocator=By.xpath("//button[contains(text(),'EXPORT')]");
	public final By btnOkLocator=By.xpath("//button[contains(text(),'OK')]");
	public final By shipmentsizedchargesuploadbuttonlocator=By.xpath("//input[@id='ShipmentDocument']");
	public final By mathadichargesuploadbuttonlocator=By.xpath("//input[@id='MathadiDocument']");
	public final By portgagesuploadbuttonlocator=By.xpath("//input[@id='PortgagesDocument']");
	public final By specialdeliverychargesuploadbuttonlocator=By.xpath("//input[@id='SpecialChargeDocument']");
	public final By loadingchargesuploadbuttonlocator=By.xpath("//input[@id='LoadingDocument']");
	public final By odachargesuploadbuttonlocator=By.xpath("//input[@id='ODADocument']");
	public final By fulltruckloaduploadbuttonlocator=By.xpath("//input[@id='FullTruckLoadDocument']");
	public final By uploadsuccessfullocator=By.xpath("//button[contains(text(),'OK')]");
	public final By successfulmessagelocator=By.xpath("//div[contains(text(),'successfull.')]");
	public final By errormessage1=By.xpath("//div[contains(text(),'Please Enter Values')]");
	public final By successfuluploadmessagelocator=By.xpath("//div[contains(text(),'Shipment document Upload successfully.')]");
	public final By invalidfileformatlocator=By.xpath("//div[contains(text(),'Please upload only Allowed file format')]");
	public final By largefilesizemessagelocator=By.xpath("//div[contains(text(),'Please Upload Document Less Than 5MB.')] ");
	public final By fromdatealertmessagelocator=By.xpath("//span[contains(text(),'Please select From date')]");
	public final By todatealertmessagelocator=By.xpath("//span[contains(text(),'Please enter To date')]");
	public final By norecordsfoundmessagelocator=By.xpath("//div[contains(text(),'No records found')]");
	public final By lowertodateerrormessagelocator=By.xpath("//span[contains(text(),'To date is Less than From date')]");
	public final By cancelbuttonlocator=By.xpath("//i[@class='fa fa-remove']");
	public final By failuremessagelocator=By.xpath("//div[contains(text(),'FAILURE.')]");
	public final By viewbuttonlocator=By.xpath("//i[@title='Open Image']");
	public final By viewmodeclosebuttonlocator=By.xpath("(//span[@class='close'])[1]");
	public final By filesizealertmessagelocator=By.xpath("//div[contains(text(),'Note: File Size Allowed Up to 5MB.')]");
	public final By docuploadmessagelocator=By.xpath("//div[contains(text(),'Note: Allowed File Format JPG,JPEG,PDF,PNG,DOC,DOCX.')]");
	public final By awberrormessagelocator=By.xpath("//div[contains(text(),'AWB Not Found.')]");
	
	SoftAssert softAssert = new SoftAssert();
	
	
	public  void userNavigaatesToTripManagement() throws InterruptedException  {
		Thread.sleep(3000);
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		
		
		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(addExtraChargesLocator));
			getDriver().findElement(addExtraChargesLocator).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(firstMileLastMileLabelLocator));
			getDriver().findElement(firstMileLastMileLabelLocator).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(addExtraChargesLocator));
			getDriver().findElement(addExtraChargesLocator).click();
			Thread.sleep(3000);
		}
		
	}

	public void userEntersTheAddExtraChargesDetails(String AllocationType2,String allocationType, String shippingId, String fromDate,
			String toDate, String shipmentSizedCharges, String varaiCharges, String portgagesCharges,
			String specialDeliveryCharges, String loadingUnloadingCharges, String odaCharges, String fullTruckLoad) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
	
		ListOfWebElementFacades extracharges = $$(allocationTypeLocator);
		
		
		for(int i=0;i<=extracharges.size()-1;i++) {
			softAssert.assertEquals(extracharges.get(i).getText(),AllocationType2);
			softAssert.assertEquals(extracharges.get(i).getText(),allocationType);
			if(extracharges.get(i).getText().equals(allocationType)) {
				extracharges.get(i).click();
			}
		}
		
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(shippingIdLocator));
		WebElement shippingIdFilterTxt=getDriver().findElement(shippingIdLocator);
		shippingIdFilterTxt.sendKeys(shippingId);
		shippingIdFilterTxt.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		getDriver().findElement(SearchButtonLocator).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(fromDateLocator));
		WebElement fromDateFilterTxt=getDriver().findElement(fromDateLocator);
		fromDateFilterTxt.sendKeys(fromDate);
		Thread.sleep(1000);
		fromDateFilterTxt.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(toDateLocator));
		WebElement toDateFilter=getDriver().findElement(toDateLocator);
		toDateFilter.sendKeys(toDate);
		Thread.sleep(3000);
		toDateFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(shipmentSizedChargesLocator));
		WebElement shipmentSizedChargesFilter=getDriver().findElement(shipmentSizedChargesLocator);
		shipmentSizedChargesFilter.sendKeys(shipmentSizedCharges);
		Thread.sleep(3000);
		shipmentSizedChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(varaiChargesLocator));
		WebElement varaiChargesFilter=getDriver().findElement(varaiChargesLocator);
		varaiChargesFilter.sendKeys(varaiCharges);
		Thread.sleep(3000);
		varaiChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(portgageChargesLocator));
		WebElement portgagesChargesFilter=getDriver().findElement(portgageChargesLocator);
		portgagesChargesFilter.sendKeys(portgagesCharges);
		Thread.sleep(3000);
		portgagesChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(specialChargesLocator));
		WebElement specialDeliveryChargesFilter=getDriver().findElement(specialChargesLocator);
		specialDeliveryChargesFilter.sendKeys(specialDeliveryCharges);
		Thread.sleep(3000);
		specialDeliveryChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(loadChargesLocator));
		WebElement loadingUnloadingChargesFilter=getDriver().findElement(loadChargesLocator);
		loadingUnloadingChargesFilter.sendKeys(loadingUnloadingCharges);
		Thread.sleep(3000);
		loadingUnloadingChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(odaChargesLocator));
		WebElement odaChargesFilter=getDriver().findElement(odaChargesLocator);
		odaChargesFilter.sendKeys(odaCharges);
		Thread.sleep(3000);
		odaChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(fullTruckLoadLocator));
		WebElement fullTruckLoadFilter=getDriver().findElement(fullTruckLoadLocator);
		fullTruckLoadFilter.sendKeys(fullTruckLoad);
		Thread.sleep(3000);
		fullTruckLoadFilter.sendKeys(Keys.ENTER);	
		
	}

	public void clickOnSaveButtonAndValidateTheResult(String successmessage) throws InterruptedException {
		getDriver().findElement(saveButtonLocator).click();
		softAssert.assertEquals(getDriver().findElement(successfulmessagelocator).getText().trim(), successmessage,"successfull.");
		softAssert.assertAll();
		
	}

	public void userEntersTheFollowingAddExtraChargesDetails( String AllocationType2,String allocationType, String shippingId, String fromDate,
			String toDate) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		
		ListOfWebElementFacades extracharges = $$(allocationTypeLocator);
		
		
		for(int i=0;i<=extracharges.size()-1;i++) {
			softAssert.assertEquals(extracharges.get(i).getText(),AllocationType2);
			softAssert.assertEquals(extracharges.get(i).getText(),allocationType);
			if(extracharges.get(i).getText().equals(allocationType)) {
				extracharges.get(i).click();
			}
		}
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(shippingIdLocator));
		WebElement shippingIdFilterTxt=getDriver().findElement(shippingIdLocator);
		shippingIdFilterTxt.sendKeys(shippingId);
		
		shippingIdFilterTxt.sendKeys(Keys.ENTER);
		
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(fromDateLocator));
		WebElement fromDateFilterTxt=getDriver().findElement(fromDateLocator);
		fromDateFilterTxt.sendKeys(fromDate);
		
		fromDateFilterTxt.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(toDateLocator));
		WebElement toDateFilter=getDriver().findElement(toDateLocator);
		toDateFilter.sendKeys(toDate);
		
		toDateFilter.sendKeys(Keys.ENTER);
		
	}

	public void clickOnClearButtonAndValidateTheButtonFunctionality() {
		
		getDriver().findElement(clearButtonOneLocator).click();
		
	}

	public void userEntersTheAddExtraChargesDetailsAsOfFollowing( String AllocationType2,String allocationType, String shippingId,
			String fromDate, String toDate, String shipmentSizedCharges, String varaiCharges, String portgagesCharges,
			String specialDeliveryCharges, String loadingUnloadingCharges, String odaCharges, String fullTruckLoad) throws InterruptedException {
WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		
		ListOfWebElementFacades extracharges = $$(allocationTypeLocator);
		
		
		for(int i=0;i<=extracharges.size()-1;i++) {
			softAssert.assertEquals(extracharges.get(i).getText(),AllocationType2);
			softAssert.assertEquals(extracharges.get(i).getText(),allocationType);
			if(extracharges.get(i).getText().equals(allocationType)) {
				extracharges.get(i).click();
			}
		}
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(shippingIdLocator));
		WebElement shippingIdFilterTxt=getDriver().findElement(shippingIdLocator);
		shippingIdFilterTxt.sendKeys(shippingId);
		
		shippingIdFilterTxt.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(fromDateLocator));
		WebElement fromDateFilterTxt=getDriver().findElement(fromDateLocator);
		fromDateFilterTxt.sendKeys(fromDate);
		
		fromDateFilterTxt.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(toDateLocator));
		WebElement toDateFilter=getDriver().findElement(toDateLocator);
		toDateFilter.sendKeys(toDate);
		
		toDateFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(shipmentSizedChargesLocator));
		WebElement shipmentSizedChargesFilter=getDriver().findElement(shipmentSizedChargesLocator);
		shipmentSizedChargesFilter.sendKeys(shipmentSizedCharges);
		
		shipmentSizedChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(varaiChargesLocator));
		WebElement varaiChargesFilter=getDriver().findElement(varaiChargesLocator);
		varaiChargesFilter.sendKeys(varaiCharges);
		
		varaiChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(portgageChargesLocator));
		WebElement portgagesChargesFilter=getDriver().findElement(portgageChargesLocator);
		portgagesChargesFilter.sendKeys(portgagesCharges);
		
		portgagesChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(specialChargesLocator));
		WebElement specialDeliveryChargesFilter=getDriver().findElement(specialChargesLocator);
		specialDeliveryChargesFilter.sendKeys(specialDeliveryCharges);
		
		specialDeliveryChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(loadChargesLocator));
		WebElement loadingUnloadingChargesFilter=getDriver().findElement(loadChargesLocator);
		loadingUnloadingChargesFilter.sendKeys(loadingUnloadingCharges);
		
		loadingUnloadingChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(odaChargesLocator));
		WebElement odaChargesFilter=getDriver().findElement(odaChargesLocator);
		odaChargesFilter.sendKeys(odaCharges);
		
		odaChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(fullTruckLoadLocator));
		WebElement fullTruckLoadFilter=getDriver().findElement(fullTruckLoadLocator);
		fullTruckLoadFilter.sendKeys(fullTruckLoad);
		
		fullTruckLoadFilter.sendKeys(Keys.ENTER);
		
	}

	public void clickOnFinalClearButtonAndValidateTheButtonFunctionality()throws InterruptedException  {
		getDriver().findElement(clearButtonTwoLocator).click();
		
	}

	public void clickOnExportButtonAndValidateTheButtonFunctionality()throws InterruptedException  {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		
		WebElement element = getDriver().findElement(By.xpath("//button[contains(text(),'EXPORT')]"));
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript("arguments[0].click();", element);
		//wait.until(ExpectedConditions.elementToBeClickable(exportButtonLocator));
		//getDriver().findElement(exportButtonLocator).click();
		
	}

	public void enterTheAddExtraChargesDetails(String AllocationType2, String allocationType, String shippingId, String fromDate, String toDate, String shipmentSizedCharges, String varaiCharges,String portgagesCharges, String specialDeliveryCharges, String loadingUnloadingCharges,String odaCharges, String fullTruckLoad) throws InterruptedException{
			WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		ListOfWebElementFacades extracharges = $$(allocationTypeLocator);
			for(int i=0;i<=extracharges.size()-1;i++) {
			softAssert.assertEquals(extracharges.get(i).getText(),AllocationType2);
			softAssert.assertEquals(extracharges.get(i).getText(),allocationType);
			 if(extracharges.get(i).getText().equals(allocationType)) {
			 extracharges.get(i).click();
			} }
			 wait.until(ExpectedConditions.visibilityOfElementLocated(shippingIdLocator));
			 WebElement shippingIdFilterTxt=getDriver().findElement(shippingIdLocator);
			 shippingIdFilterTxt.sendKeys(shippingId);
			 shippingIdFilterTxt.sendKeys(Keys.ENTER);
			Thread.sleep(1000);
		getDriver().findElement(SearchButtonLocator).click();
			Thread.sleep(1000);
			 wait.until(ExpectedConditions.elementToBeClickable(btnOkLocator));
			 getDriver().findElement(btnOkLocator).click();
		 wait.until(ExpectedConditions.visibilityOfElementLocated(fromDateLocator));
			 WebElement fromDateFilterTxt=getDriver().findElement(fromDateLocator);
			fromDateFilterTxt.sendKeys(fromDate);
			Thread.sleep(1000);
		fromDateFilterTxt.sendKeys(Keys.ENTER);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(toDateLocator));
			WebElement toDateFilter=getDriver().findElement(toDateLocator);
			 toDateFilter.sendKeys(toDate);
			 Thread.sleep(3000);
			 toDateFilter.sendKeys(Keys.ENTER);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(shipmentSizedChargesLocator));
			WebElement shipmentSizedChargesFilter=getDriver().findElement(shipmentSizedChargesLocator);
			 shipmentSizedChargesFilter.sendKeys(shipmentSizedCharges);
		 Thread.sleep(3000);
			 shipmentSizedChargesFilter.sendKeys(Keys.ENTER);
			wait.until(ExpectedConditions.visibilityOfElementLocated(varaiChargesLocator));
			 WebElement varaiChargesFilter=getDriver().findElement(varaiChargesLocator);
			 varaiChargesFilter.sendKeys(varaiCharges);
			 Thread.sleep(3000);
			 varaiChargesFilter.sendKeys(Keys.ENTER);
			
			 wait.until(ExpectedConditions.visibilityOfElementLocated(portgageChargesLocator));
			 WebElement portgagesChargesFilter=getDriver().findElement(portgageChargesLocator);
			portgagesChargesFilter.sendKeys(portgagesCharges);
			 Thread.sleep(3000);
			 portgagesChargesFilter.sendKeys(Keys.ENTER);
			
			 wait.until(ExpectedConditions.visibilityOfElementLocated(specialChargesLocator));
			 WebElement specialDeliveryChargesFilter=getDriver().findElement(specialChargesLocator);
			 specialDeliveryChargesFilter.sendKeys(specialDeliveryCharges);
			 Thread.sleep(3000);
			specialDeliveryChargesFilter.sendKeys(Keys.ENTER);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(loadChargesLocator));
			 WebElement loadingUnloadingChargesFilter=getDriver().findElement(loadChargesLocator);
			 loadingUnloadingChargesFilter.sendKeys(loadingUnloadingCharges);
		 Thread.sleep(3000);
			loadingUnloadingChargesFilter.sendKeys(Keys.ENTER);
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(odaChargesLocator));
			 WebElement odaChargesFilter=getDriver().findElement(odaChargesLocator);
			 odaChargesFilter.sendKeys(odaCharges);
			 Thread.sleep(3000);
			 odaChargesFilter.sendKeys(Keys.ENTER);
			
			 wait.until(ExpectedConditions.visibilityOfElementLocated(fullTruckLoadLocator));
			 WebElement fullTruckLoadFilter=getDriver().findElement(fullTruckLoadLocator);
			fullTruckLoadFilter.sendKeys(fullTruckLoad);
			Thread.sleep(3000);
			fullTruckLoadFilter.sendKeys(Keys.ENTER);
			}


			


	public void clickOnSaveButtonAndValidateTheErrorMessage( String errorMessageExtracharge) throws InterruptedException  {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.elementToBeClickable(saveButtonLocator));
		getDriver().findElement(saveButtonLocator).click();
		softAssert.assertEquals(getDriver().findElement(errormessage1).getText().trim(),errorMessageExtracharge,"Please Enter Values");
		softAssert.assertAll();
	}

		public void userClicksOnUploadButton(String shipmentSizedUpload,String mathadiChargesUpload,String portgageChargesUpload,String specialDeliveryChargesUpload,String loadingChargesUpload,String odaChargesUpload,String fullTruckLoadUpload,String successfulUploadMessage) throws InterruptedException  {
			WebDriverWait wait = new WebDriverWait(getDriver(),10);
			Thread.sleep(4000);
			WebElement shipmentSizedUpload1=getDriver().findElement(shipmentsizedchargesuploadbuttonlocator);
		shipmentSizedUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity1.png");
		Thread.sleep(4000);
			WebElement mathadiChargesUpload1 = getDriver().findElement(mathadichargesuploadbuttonlocator);
			mathadiChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity2.png");
			Thread.sleep(4000);
			WebElement portgageChargesUpload1 = getDriver().findElement(portgagesuploadbuttonlocator);
			portgageChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity3.png");
			Thread.sleep(4000);
			WebElement specialDeliveryChargesUpload1 = getDriver().findElement(specialdeliverychargesuploadbuttonlocator);
			specialDeliveryChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity4.png");
			Thread.sleep(4000);
			WebElement loadingChargesUpload1 = getDriver().findElement(loadingchargesuploadbuttonlocator);
			loadingChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity5.png");
			Thread.sleep(4000);
			WebElement odaChargesUpload1 = getDriver().findElement(odachargesuploadbuttonlocator);
			odaChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity6.png");
			Thread.sleep(4000);
			WebElement fullTruckLoadUpload1 = getDriver().findElement(fulltruckloaduploadbuttonlocator);
			fullTruckLoadUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/sample.docx");
			Thread.sleep(4000);
			Thread.sleep(1000);
			WebElement element2 = getDriver().findElement(By.xpath("//button[contains(text(),'Save')]"));
			JavascriptExecutor executor = (JavascriptExecutor)getDriver();
			executor.executeScript("arguments[0].click();", element2);
			Thread.sleep(2000);
//			softAssert.assertEquals(getDriver().findElement(successfuluploadmessagelocator).getText().trim(), successfulUploadMessage,"Shipment document Upload successfully.");
			softAssert.assertEquals(getDriver().findElement(successfulmessagelocator).getText().trim(), successfulUploadMessage,"successfull.");
			softAssert.assertAll();
		
	}

		public void userClicksOnUploadButtonAndUploadsTheInvalidFile(String shipmentSizedUpload,String mathadiChargesUpload,String portgageChargesUpload,String specialDeliveryChargesUpload,String loadingChargesUpload,String odaChargesUpload,String fullTruckLoadUpload) throws InterruptedException {
            WebDriverWait wait = new WebDriverWait(getDriver(),10);
				
			WebElement shipmentSizedUpload1=getDriver().findElement(shipmentsizedchargesuploadbuttonlocator);
		shipmentSizedUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity1.png");
		
			WebElement mathadiChargesUpload1 = getDriver().findElement(mathadichargesuploadbuttonlocator);
			mathadiChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity2.png");
			
			WebElement portgageChargesUpload1 = getDriver().findElement(portgagesuploadbuttonlocator);
			portgageChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity3.png");
			
			//wait.until(ExpectedConditions.refreshed(
			  //      ExpectedConditions.visibilityOfElementLocated(portgagesuploadbuttonlocator)));
			//WebElement portgageTxt=getDriver().findElement(portgagesuploadbuttonlocator);
			//portgageTxt.sendKeys("/home/rushikeshwadgaonkar/Pictures/serenity3.png");
		
			WebElement specialDeliveryChargesUpload1 = getDriver().findElement(specialdeliverychargesuploadbuttonlocator);
			specialDeliveryChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity4.png");
			
			WebElement loadingChargesUpload1 = getDriver().findElement(loadingchargesuploadbuttonlocator);
			loadingChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity5.png");
			
			WebElement odaChargesUpload1 = getDriver().findElement(odachargesuploadbuttonlocator);
			odaChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity6.png");
			
			WebElement fullTruckLoadUpload1 = getDriver().findElement(fulltruckloaduploadbuttonlocator);
			fullTruckLoadUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/awbExtraCharges.xlsx");	
			
		}

		public void validateTheInvalidUploadErrorMessage(String invalidUploadErrorMessage) throws InterruptedException{
			softAssert.assertEquals(getDriver().findElement(invalidfileformatlocator).getText().trim(),invalidUploadErrorMessage,"Please upload only Allowed file format");
			softAssert.assertAll();
			Thread.sleep(1000);
			WebElement element1 = getDriver().findElement(By.xpath("//button[contains(text(),'Save')]"));
			JavascriptExecutor executor = (JavascriptExecutor)getDriver();
			executor.executeScript("arguments[0].click();", element1);
			
		}

		public void userUploadsLargeFile(String shipmentSizedUpload, String mathadiChargesUpload,
				String portgageChargesUpload, String specialDeliveryChargesUpload, String loadingChargesUpload,
				String odaChargesUpload, String fullTruckLoadUpload) throws InterruptedException{
			WebDriverWait wait = new WebDriverWait(getDriver(),10);
			
			WebElement shipmentSizedUpload1=getDriver().findElement(shipmentsizedchargesuploadbuttonlocator);
		shipmentSizedUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity1.png");
		
		
			WebElement mathadiChargesUpload1 = getDriver().findElement(mathadichargesuploadbuttonlocator);
			mathadiChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity2.png");
			
			WebElement portgageChargesUpload1 = getDriver().findElement(portgagesuploadbuttonlocator);
			portgageChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity3.png");
			
			WebElement specialDeliveryChargesUpload1 = getDriver().findElement(specialdeliverychargesuploadbuttonlocator);
			specialDeliveryChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity4.png");
			
			WebElement loadingChargesUpload1 = getDriver().findElement(loadingchargesuploadbuttonlocator);
			loadingChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity5.png");
			
			WebElement odaChargesUpload1 = getDriver().findElement(odachargesuploadbuttonlocator);
			odaChargesUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity6.png");
			
			WebElement fullTruckLoadUpload1 = getDriver().findElement(fulltruckloaduploadbuttonlocator);
			fullTruckLoadUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/largefile.jpg");
			
		}

		public void validateTheLargeFileSizeErrorMessage(String largeFileSizeErrorMessage) throws InterruptedException{
			//WebDriverWait wait = new WebDriverWait(getDriver(),30);
			//wait.until(ExpectedConditions.elementToBeClickable(saveButtonLocator));
		
			softAssert.assertEquals(getDriver().findElement(largefilesizemessagelocator).getText().trim(), largeFileSizeErrorMessage,"Please Upload Document Less Than 5MB.");
			softAssert.assertAll();
			
		}

		public void validateTheFromDateAlertMessage(String fromDateAlertMessage) throws InterruptedException{
			softAssert.assertEquals(getDriver().findElement(fromdatealertmessagelocator).getText().trim(), fromDateAlertMessage,"Please select From date");
			softAssert.assertAll();
			
		}

		public void userEntersTheAllocationTypeAndfromDateDetails(String AllocationType2, String allocationType,
				String fromDate) throws InterruptedException{
			WebDriverWait wait = new WebDriverWait(getDriver(), 10);
			
			ListOfWebElementFacades extracharges = $$(allocationTypeLocator);
			
			
			for(int i=0;i<=extracharges.size()-1;i++) {
				softAssert.assertEquals(extracharges.get(i).getText(),AllocationType2);
				softAssert.assertEquals(extracharges.get(i).getText(),allocationType);
				if(extracharges.get(i).getText().equals(allocationType)) {
					extracharges.get(i).click();
				}
			}
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(fromDateLocator));
			WebElement fromDateFilterTxt=getDriver().findElement(fromDateLocator);
			fromDateFilterTxt.sendKeys(fromDate);
			Thread.sleep(1000);
			fromDateFilterTxt.sendKeys(Keys.ENTER);	
			
			
			
		}

		public void validateTheToDateAlertMessage(String toDateAlertMessage) throws InterruptedException{
			softAssert.assertEquals(getDriver().findElement(todatealertmessagelocator).getText().trim(), toDateAlertMessage,"Please enter To date");
			softAssert.assertAll();
			
		}

		public void userEntersTheAllocationTypeAndDateDetails(String AllocationType2, String allocationType,
				String fromDate, String toDate)  throws InterruptedException{
			WebDriverWait wait = new WebDriverWait(getDriver(), 10);
			
			ListOfWebElementFacades extracharges = $$(allocationTypeLocator);
			
			
			for(int i=0;i<=extracharges.size()-1;i++) {
				softAssert.assertEquals(extracharges.get(i).getText(),AllocationType2);
				softAssert.assertEquals(extracharges.get(i).getText(),allocationType);
				if(extracharges.get(i).getText().equals(allocationType)) {
					extracharges.get(i).click();
				}
			}
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(fromDateLocator));
			WebElement fromDateFilterTxt=getDriver().findElement(fromDateLocator);
			fromDateFilterTxt.sendKeys(fromDate);
			
			fromDateFilterTxt.sendKeys(Keys.ENTER);
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(toDateLocator));
			WebElement toDateFilter=getDriver().findElement(toDateLocator);
			toDateFilter.sendKeys(toDate);
			
			toDateFilter.sendKeys(Keys.ENTER);
			
		}

		public void validateTheNoRecordsFound(String noRecordsFoundAlertMessage) throws InterruptedException{
			softAssert.assertEquals(getDriver().findElement(norecordsfoundmessagelocator).getText().trim(), noRecordsFoundAlertMessage,"No records found");
			softAssert.assertAll();
			
		}

		public void validateTheLessToDateInputValue(String lowerToDateAlertMessage) throws InterruptedException{
			softAssert.assertEquals(getDriver().findElement(lowertodateerrormessagelocator).getText().trim(), lowerToDateAlertMessage,"To date is Less than From date");
			softAssert.assertAll();
			
		}

		public void userHitsTheUploadButton(String shipmentSizedUpload)throws InterruptedException {
WebDriverWait wait = new WebDriverWait(getDriver(),10);
			
			WebElement shipmentSizedUpload1=getDriver().findElement(shipmentsizedchargesuploadbuttonlocator);
		shipmentSizedUpload1.sendKeys(System.getProperty("user.dir")+"/Test Data/serenity1.png");
		wait.until(ExpectedConditions.elementToBeClickable(btnOkLocator));
		getDriver().findElement(btnOkLocator).click();
		
			
		}

		public void userClicksOnCancelButtonAndValidateTheUploadFileCancelButtonFunctionality(
				String cancelButtonAlertMessage)throws InterruptedException{
			WebDriverWait wait = new WebDriverWait(getDriver(), 10);
			wait.until(ExpectedConditions.elementToBeClickable(cancelbuttonlocator));
			getDriver().findElement(cancelbuttonlocator).click();
			Thread.sleep(1000);
			softAssert.assertEquals(getDriver().findElement(failuremessagelocator).getText().trim(), cancelButtonAlertMessage,"FAILURE.");
			softAssert.assertAll();
			wait.until(ExpectedConditions.elementToBeClickable(btnOkLocator));
			getDriver().findElement(btnOkLocator).click();
		}

		public void userEntersAllocationTypeAndShippingAndDateDetails(String AllocationType2, String allocationType,String shippingId) throws InterruptedException{
	WebDriverWait wait = new WebDriverWait(getDriver(), 10);
			
			ListOfWebElementFacades extracharges = $$(allocationTypeLocator);
			
			
			for(int i=0;i<=extracharges.size()-1;i++) {
				softAssert.assertEquals(extracharges.get(i).getText(),AllocationType2);
				softAssert.assertEquals(extracharges.get(i).getText(),allocationType);
				if(extracharges.get(i).getText().equals(allocationType)) {
					extracharges.get(i).click();
				}
			}
			wait.until(ExpectedConditions.visibilityOfElementLocated(shippingIdLocator));
			WebElement shippingIdFilterTxt=getDriver().findElement(shippingIdLocator);
			shippingIdFilterTxt.sendKeys(shippingId);
			shippingIdFilterTxt.sendKeys(Keys.ENTER);
			
		}

		public void userClicksOnViewButtonAndValidateTheUploadFileViewButtonFunctinality() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 10);
			wait.until(ExpectedConditions.elementToBeClickable(viewbuttonlocator));
			Thread.sleep(1000);
			getDriver().findElement(viewbuttonlocator).click();
			
		}

		public void userClicksOnCloseButtonOfTheFileOpenedAndValidateClickFunctionality() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 10);
			wait.until(ExpectedConditions.elementToBeClickable(viewbuttonlocator));
			Thread.sleep(1000);
			getDriver().findElement(viewbuttonlocator).click();
			wait.until(ExpectedConditions.elementToBeClickable(viewmodeclosebuttonlocator));
			Thread.sleep(1000);
			getDriver().findElement(viewmodeclosebuttonlocator).click();
			
			
		}

		public void userClicksOnSearchButtonAndValidateTheAwbErrorMessage(String AwbErrorMessage) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 10);
			getDriver().findElement(SearchButtonLocator).click();
			softAssert.assertEquals(getDriver().findElement(awberrormessagelocator).getText().trim(), AwbErrorMessage,"AWB Not Found.");
			$(btnOkLocator).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
			softAssert.assertAll();
			
		}

}
