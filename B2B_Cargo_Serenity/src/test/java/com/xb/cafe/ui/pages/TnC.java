package com.xb.cafe.ui.pages;

import java.util.List;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;



import net.serenitybdd.core.pages.PageObject;

public class TnC extends PageObject {

	 public static final String UserValidatesTheErrorMsgAndUserClicksOnOkButton = null;
	private static final CharSequence HigherBreakLimitValue = null;
	String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	 String username;
	 String TagName1;
	 String TagComment1;
	 String Tag_MappedEnvironment;
	//  WebDriver driver = null;
	 
	 public  final By input_UN = By.xpath("//input[@name='email']");
		public  final By input_PW = By.xpath("//input[@id='typepass']");
		public  final By btn_Login = By.xpath("//button[contains(text(),'Login')]");
		public  final By Lnk_MasterData = By.xpath("//span[contains(text(),'Master Data')]");
		public  final By lnk_TnC = By.xpath("//span[contains(text(),'T&C Template')]");
		public  final By AddNewTnC_Bttn = By.xpath("//a[@title='Add New TC']");
		public  final By TnCTxtinput = By.xpath("//input[@id='templateName']");
		public  final By TnCdescription = By.xpath("//textarea[@id='templateDescription']");
		public  final By ExpandAllbtn = By.xpath("//button[@title='Expand All']");
		public  final By Billingcycleinput = By.xpath("//input[@class='vue-treeselect__input']");
		public  final By PODLocator = By.xpath("//label[contains(text(),'No')]");
		public  final By Creditperiodlocator = By.xpath("//input[@id='creditPeriod']");
		public  final By Creditlimitlocator = By.xpath("//input[@id='creditLimit']");
		public  final By advancepaymentlocator = By.xpath("//input[@id='advancePayment']");
		public  final By Creditalertlocator = By.xpath("//input[@id='creditAlert']");
		public  final By Paymentmode = By.xpath("//label[contains(text(),'Cheque')]");
		public  final By Maxliabilitylocator = By.xpath("//input[@id='liabilityAmount']");
		public  final By Savebttn = By.xpath("//button[contains(text(),'Save')]");
		public  final By Okbttn = By.xpath("//button[contains(text(),'OK')]");
		public  final By Successmsg = By.xpath("//div[@id='swal2-content']");
        public final By EnterdateLocator = By.xpath("//input[@id='date1']");
		public  final By EnterdateLocator2 = By.xpath("//input[@id='date2']");
		public  final By TnCTemplateerrormsglocator = By.xpath("//span[contains(text(),'The TemplateName field is required')]");
		public  final By TnCTemplatedeserrormsglocator = By.xpath("//span[contains(text(),'The TemplateDescription field is required')]");
		public  final By Billingcycleerrormsglocator = By.xpath("//span[contains(text(),'Please Select Billing Cycle')]");
		public  final By PODhardcopyerrormsgloactor = By.xpath("//span[contains(text(),'Please Select POD Hard Copy')]");
		public  final By Creditperioderrormsglocator = By.xpath("//span[contains(text(),'Credit Period is required')]");
		public  final By Maxliabilityerrormsglocator = By.xpath("//span[contains(text(),'Max Liability Amount is required')]");
		public  final By PODChanrgeslocator = By.xpath("//input[@id='podCharge']");
		public  final By Inactive_btn = By.xpath("//button[contains(text(),'Inactive')]");
		public  final By Activationsuccessmsglocator = By.xpath("//div[contains(text(),'Are you sure to make Active/Inactive?')]");
		public  final By Successfulmsglocator = By.xpath("//div[@id='swal2-content']");
		public  final By filterLocator = By.xpath("//i[@title='Search Filter']");
		public  final By ErrormsgLocator = By.xpath("//div[contains(text(),'Terms Condition Template Name Already Exists.')]");
		public  final By Editlocator = By.xpath("//span[@title='Edit']");
		public  final By TemplateNameLocator = By.xpath("//td[contains(text(),'T&C37')]");
		public  final By TemplateDescriptionLocator = By.xpath("//td[contains(text(),'Test')]");
		public  final By CreatedByLocator = By.xpath("//tbody/tr[@id='31']/td[4]");
		public  final By CreatedDateLocator = By.xpath("//tbody/tr[@id='31']/td[5]");
		public  final By LastModifiedByLocator = By.xpath("//tbody/tr[@id='31']/td[6]");
		public  final By LasModifiedDateLocator = By.xpath("//tbody/tr[@id='31']/td[7]");
		public  final By StatusLocator = By.xpath("//button[contains(text(),'Active')]");
		
		
		
		SoftAssert softAssert = new SoftAssert();
		
		

		public  void LoginSelfServicepage(String URL) throws InterruptedException {
			getDriver().get(URL);
		}

		
		public void userNavigatesToMasterData1Option() throws InterruptedException {
			Thread.sleep(1000);
	        getDriver().navigate().refresh();
	        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
	        try {
	                getDriver().navigate().refresh();
	                Thread.sleep(1000);
	                wait.until(ExpectedConditions.elementToBeClickable(lnk_TnC));
	                getDriver().findElement(lnk_TnC).click();
	                Thread.sleep(1000);
	        } catch (Exception e) {
	                getDriver().navigate().refresh();
	                Thread.sleep(1000);
	                wait.until(ExpectedConditions.elementToBeClickable(Lnk_MasterData));
	                getDriver().findElement(Lnk_MasterData).click();
	                Thread.sleep(1000);
	                wait.until(ExpectedConditions.elementToBeClickable(lnk_TnC));
	                getDriver().findElement(lnk_TnC).click();
	        }
		}
	     public void userNavigatesToCreateNewTnCOption() throws InterruptedException {
	    	 Thread.sleep(5000);
	 		getDriver().findElement(AddNewTnC_Bttn).click();
	 		
	 	}


		public void userEntersInTheTextField(String TermsandConditionsTemplateName) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(),10);
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(TnCTxtinput));
			WebElement TnCsearchTxt=getDriver().findElement(TnCTxtinput);
			TnCsearchTxt.sendKeys(TermsandConditionsTemplateName);
			Thread.sleep(1000);
			TnCsearchTxt.sendKeys(Keys.ENTER);
			Thread.sleep(1000);
		}


		public void userEntersInTheTextFieldAndClicksOnExpandAllButton(String TermsandConditionsTemplateDescription)throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(),10);
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(TnCdescription));
			WebElement TnCdescriptionsearchTxt=getDriver().findElement(TnCdescription);
			TnCdescriptionsearchTxt.sendKeys(TermsandConditionsTemplateDescription);
			Thread.sleep(1000);
			TnCdescriptionsearchTxt.sendKeys(Keys.ENTER);
			Thread.sleep(1000);
			getDriver().findElement(ExpandAllbtn).click();
			
		}

		public void userSelectsBillingCycleAndSelectPODHardCopyRequiredvalueAndEnterDate(String BillingCycle,
				String date) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(),10);
//					
//			
//	        wait.until(ExpectedConditions.visibilityOfElementLocated(Billingcycleinput));
//	        WebElement BillingTxtbox=getDriver().findElement(Billingcycleinput);
//	        BillingTxtbox.sendKeys(BillingCycle);
//	        Thread.sleep(1000);
//	        BillingTxtbox.sendKeys(Keys.ENTER);
//			
			getDriver().findElement(Billingcycleinput).click();
			Thread.sleep(4000);
			List<WebElement> listBillingcycle=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
			for(int i=0;i<=listBillingcycle.size()-1;i++) {
			System.out.println(listBillingcycle.get(i).getText());
			if(listBillingcycle.get(i).getText().equalsIgnoreCase(BillingCycle)) {
				listBillingcycle.get(i).click();
			break;
			}
			}
		
			getDriver().findElement(By.xpath("//label[contains(text(),'No')]")).click();
			 Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(EnterdateLocator));
	        WebElement dateTxtbox=getDriver().findElement(EnterdateLocator);
	        dateTxtbox.sendKeys(date);
	        Thread.sleep(1000);
	        dateTxtbox.sendKeys(Keys.ENTER);
		
			}


		public void userEntersBillingTermsAndUserSelectsPaymentModeCheque(String CreditPeriod, String Creditlimit,
				String advancepayment, String creditlimitalert) throws InterruptedException {
			
         WebDriverWait wait = new WebDriverWait(getDriver(),20);
			
         wait.until(ExpectedConditions.visibilityOfElementLocated(Creditperiodlocator));
			getDriver().findElement(Creditperiodlocator).sendKeys(CreditPeriod);
			
			 wait.until(ExpectedConditions.visibilityOfElementLocated(Creditlimitlocator));
				getDriver().findElement(Creditlimitlocator).sendKeys(Creditlimit);
			
				wait.until(ExpectedConditions.visibilityOfElementLocated(advancepaymentlocator));
				getDriver().findElement(advancepaymentlocator).sendKeys(advancepayment);
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(Creditalertlocator));
				getDriver().findElement(Creditalertlocator).sendKeys(creditlimitalert);
				
				Thread.sleep(1000);	
				getDriver().findElement(By.xpath("//body/div[1]/div[1]/main[1]/div[2]/section[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[2]/label[1]/span[1]/i[1]")).click();
	
		}
		
          public void userEntersOtherTermsAndConditions(String maxliabilityamount) throws InterruptedException {
        	  WebDriverWait wait = new WebDriverWait(getDriver(),10);
          
        	  wait.until(ExpectedConditions.visibilityOfElementLocated(Maxliabilitylocator));
  			getDriver().findElement(Maxliabilitylocator).sendKeys(maxliabilityamount);
			
		}


		public void userSelectsBillingCycleAndSelectPodHardCopyRequiredValueAndEnterDates(String BillingCycle,String date1, String date2) throws InterruptedException {
WebDriverWait wait = new WebDriverWait(getDriver(),10);
					
			
	        wait.until(ExpectedConditions.visibilityOfElementLocated(Billingcycleinput));
	        WebElement BillingTxtbox=getDriver().findElement(Billingcycleinput);
	        BillingTxtbox.sendKeys(BillingCycle);
	        Thread.sleep(1000);
	        BillingTxtbox.sendKeys(Keys.ENTER);
			
			
		
			getDriver().findElement(By.xpath("//label[contains(text(),'No')]")).click();
			 Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(EnterdateLocator));
	        WebElement dateTxtbox=getDriver().findElement(EnterdateLocator);
	        dateTxtbox.sendKeys(date1);
	        Thread.sleep(1000);
	        dateTxtbox.sendKeys(Keys.ENTER);
	        wait.until(ExpectedConditions.visibilityOfElementLocated(EnterdateLocator2));
	        WebElement dateTxtbox2=getDriver().findElement(EnterdateLocator2);
	        dateTxtbox2.sendKeys(date2);
	        Thread.sleep(1000);
	        dateTxtbox2.sendKeys(Keys.ENTER);
		
			}


		public void userSelectsBillingCycleAndSelectPODHardCopyRequiredvalue(String BillingCycle) throws InterruptedException {
WebDriverWait wait = new WebDriverWait(getDriver(),10);
					
			
	        wait.until(ExpectedConditions.visibilityOfElementLocated(Billingcycleinput));
	        WebElement BillingTxtbox=getDriver().findElement(Billingcycleinput);
	        BillingTxtbox.sendKeys(BillingCycle);
	        Thread.sleep(1000);
	        BillingTxtbox.sendKeys(Keys.ENTER);
	        getDriver().findElement(By.xpath("//label[contains(text(),'No')]")).click();	
		}


		public void userValidatesErrorMsgs(String TermsandConditionsTemplateNameerrormsg,
				String TermsandConditionsTemplateDescriptionerrormsg, String Billingcycleerrormsg,
				String PODHardCopyrequirederrormsg, String Creditdayserrormsg, String Maxliabilityerrormsg) throws InterruptedException {
			
			softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'The TemplateName field is required')]".replace("The TemplateName field is required", TermsandConditionsTemplateNameerrormsg))).getText(), TermsandConditionsTemplateNameerrormsg,"The TemplateName field is required");
			 softAssert.assertAll();
			 softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Please Select POD Hard Copy')]".replace("Please Select POD Hard Copy", PODHardCopyrequirederrormsg))).getText(), PODHardCopyrequirederrormsg,"Please Select POD Hard Copy");
             softAssert.assertAll();
             softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'The TemplateDescription field is required')]".replace("The TemplateDescription field is required", TermsandConditionsTemplateDescriptionerrormsg))).getText(), TermsandConditionsTemplateDescriptionerrormsg,"The TemplateDescription field is required");
             softAssert.assertAll();
             softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Please Select Billing Cycle')]".replace("Please Select Billing Cycle", Billingcycleerrormsg))).getText(), Billingcycleerrormsg,"Please Select Billing Cycle");
             softAssert.assertAll();
             softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Credit Period is required')]".replace("Credit Period is required", Creditdayserrormsg))).getText(), Creditdayserrormsg,"Credit Period is required");
             softAssert.assertAll();
			 softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Max Liability Amount is required')]".replace("Max Liability Amount is required", Maxliabilityerrormsg))).getText(), Maxliabilityerrormsg,"Max Liability Amount is required");
             softAssert.assertAll();
             
             
             
             
            
             
}


		public void userEntersPODHardCopyCharges(String PODCharges) throws InterruptedException {
WebDriverWait wait = new WebDriverWait(getDriver(),10);
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(PODChanrgeslocator));
			WebElement TnCsearchTxt=getDriver().findElement(PODChanrgeslocator);
			TnCsearchTxt.sendKeys(PODCharges);
			Thread.sleep(1000);
			TnCsearchTxt.sendKeys(Keys.ENTER);
			Thread.sleep(1000);
			
		}


		public void userSelectsBillingCycleAndSelectPODHardCopyRequiredvalueYes(String BillingCycle) throws InterruptedException {
WebDriverWait wait = new WebDriverWait(getDriver(),10);
					
			
	        wait.until(ExpectedConditions.visibilityOfElementLocated(Billingcycleinput));
	        WebElement BillingTxtbox=getDriver().findElement(Billingcycleinput);
	        BillingTxtbox.sendKeys(BillingCycle);
	        Thread.sleep(1000);
	        BillingTxtbox.sendKeys(Keys.ENTER);
	        getDriver().findElement(By.xpath("//label[contains(text(),'Yes')]")).click();	
		}
		public void userEntersTextInInputField(String termsandConditionTemplateName) throws InterruptedException {

		
			
			
		
		}

		public void userValidatesSuccessfulmsgAndClicksOnOk(String ActivationSuccessmsg) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(),10);
			
			softAssert.assertEquals(getDriver().findElement(By.xpath("//div[@id='swal2-content']".replace("T&C Activated Successfully!!", ActivationSuccessmsg))).getText(), ActivationSuccessmsg,"T&C Activated Successfully!!");
            softAssert.assertAll();
			
			
//           softAssert.assertEquals(getDriver().findElement(Successfulmsglocator).getText().trim(), successfulmsg,"T&C Activated Successfully!!");
//			softAssert.assertAll();
			wait.until(ExpectedConditions.elementToBeClickable(Okbttn));
				getDriver().findElement(Okbttn).click();
			
		}


		public void userValidatesErrorMsgAndClicksOnOk(String Errormsg) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(),10);
			
			softAssert.assertEquals(getDriver().findElement(By.xpath("//div[contains(text(),'Terms Condition Template Name Already Exists.')]".replace("Terms Condition Template Name Already Exists.", Errormsg))).getText(), Errormsg,"Terms Condition Template Name Already Exists.");
            softAssert.assertAll();
			
			
//			softAssert.assertEquals(getDriver().findElement(ErrormsgLocator).getText().trim(), Errormsg,"Terms Condition Template Name Already Exists.");
//			softAssert.assertAll();
		}


		public void userClicksOnEditButton() throws InterruptedException {
	    	 Thread.sleep(5000);
	 		getDriver().findElement(Editlocator).click();
		}


		public void userClicksOnExpandAllButton() throws InterruptedException {
			Thread.sleep(5000);
	 		getDriver().findElement(ExpandAllbtn).click();
			
		}


		public void userClearsMaxLiabilityAmount() throws InterruptedException {
			Thread.sleep(5000);
	 		getDriver().findElement(Maxliabilitylocator).clear();
			
		}


		public void userEntersNewMaxLiabilityAmount(String Maxliabilityamount) throws InterruptedException {
			 WebDriverWait wait = new WebDriverWait(getDriver(),10);
	          
       	  wait.until(ExpectedConditions.visibilityOfElementLocated(Maxliabilitylocator));
 			getDriver().findElement(Maxliabilitylocator).sendKeys(Maxliabilityamount);
			
		}


		public void userValidatesListingElements(String TemplateName, String TemplateDescription, String CreatedBy,
				String CreatedDate, String LastModifiedBy, String LastModifiedDate, String Status) throws InterruptedException {
			softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'T&C37')]".replace("T&C37", TemplateName))).getText(), TemplateName,"T&C37");
			softAssert.assertAll();
			softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'Test')]".replace("Test", TemplateDescription))).getText(), TemplateDescription,"Test");
			softAssert.assertAll();
			softAssert.assertEquals(getDriver().findElement(By.xpath("//tbody/tr[@id='31']/td[4]".replace("admin.atul.deokar@xpressbees.com", CreatedBy))).getText(), CreatedBy,"admin.atul.deokar@xpressbees.com");
			softAssert.assertAll();
			softAssert.assertEquals(getDriver().findElement(By.xpath("//tbody/tr[@id='31']/td[5]".replace("05-05-2022 15:53", CreatedDate))).getText(), CreatedDate,"05-05-2022 15:53");
			softAssert.assertAll();
			softAssert.assertEquals(getDriver().findElement(By.xpath("//tbody/tr[@id='31']/td[6]".replace("admin.atul.deokar@xpressbees.com", LastModifiedBy))).getText(), LastModifiedBy,"admin.atul.deokar@xpressbees.com");
			softAssert.assertAll();
			softAssert.assertEquals(getDriver().findElement(By.xpath("//tbody/tr[@id='31']/td[7]".replace("06-05-2022 13:53", LastModifiedDate))).getText(), LastModifiedDate,"06-05-2022 13:53");
			softAssert.assertAll();
			softAssert.assertEquals(getDriver().findElement(By.xpath("//button[contains(text(),'Active')]".replace("Active", Status))).getText(), Status,"Active");
			softAssert.assertAll();
			
		}
}	
		
			
			
		
			

		
			
			
			
			
			
			
			
			
			
			
		