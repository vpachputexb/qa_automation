package com.xb.cafe.ui.pages;

import java.util.List;import org.apache.poi.poifs.crypt.dsig.KeyInfoKeySelector;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;

public class Cargo_MarkShipmentInvalid extends PageObject {

	public final By supportLink = By.xpath("//span[contains(text(),'Support')]");
	public final By markshpmntinvLink = By.xpath("//span[contains(text(),'Mark Shipment Invalid')]");
	public final By lbl = By.xpath("//label[contains(text(),'Mark Shipment Invalid')]");
	public final By enterShipmentidInp = By.xpath("//textarea[@id='shipmentId']");
	public final By commentInp = By.xpath("//textarea[@id='comments']");
	public final By updatebtn = By.xpath("//button[@class='-btn btn-success']");
	public final By resetbtn = By.xpath("//button[@class='-btn btn-danger']");
	public final By blnkawbmsg = By.xpath("//span[contains(text(),'Please enter Shipment ID(s)')]");
	public final By blnkcomment = By.xpath("//span[contains(text(),'The comments field is required')]");
	public final By successpopup = By.xpath("//span[contains(text(),'Mark shipment invalid')]");
	public final By successmsg = By.xpath("//td[contains(text(),'Record Updated Successfully')]");
	public final By closepopup = By.xpath("//button[contains(text(),'×')]");
	public final By successmsg1 = By.xpath("//td[contains(text(),'Shipment Status is Already Invalid')]");
	public final By invalidAWBmsg = By.xpath("//span[contains(text(),'Please follow the correct format')]");
	public final By maxtenAWB = By.xpath("//span[contains(text(),'Maximum 10 AWBs allowed')]");

	SoftAssert softAssert = new SoftAssert();

	public void NavigateSupportLink() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().navigate().refresh();
		getDriver().findElement(supportLink).click();
	}

	public void NavigateToMarkShipmentInvalid() throws InterruptedException {
		// TODO Auto-generated method stub
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(markshpmntinvLink));
			getDriver().findElement(markshpmntinvLink).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(supportLink).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(markshpmntinvLink));
			getDriver().findElement(markshpmntinvLink).click();
			Thread.sleep(3000);
		}

	}

	public void MarkSI(String MarkShipmentInvalid) throws InterruptedException {
		Thread.sleep(3000);
		String label = getDriver().findElement(lbl).getText();
		softAssert.assertEquals(label, MarkShipmentInvalid, "Page title is matched");
		softAssert.assertAll();
	}

	public void MarksShipmentInvalidMenu() throws InterruptedException {
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(markshpmntinvLink).isDisplayed(), "Menu displayed");
		softAssert.assertAll();
	}

	public void PageElements() throws InterruptedException {
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(enterShipmentidInp).isDisplayed(),
				"Shipment id field is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(commentInp).isDisplayed(), "Comments field is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(updatebtn).isDisplayed(), "Update button is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(resetbtn).isDisplayed(), "Reset button is displayed");
		softAssert.assertAll();
	}

	public void ShipmentIDValidation(String ShipmentId) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(enterShipmentidInp).sendKeys(ShipmentId);

	}

	public void ValidationMsg(String ShipmentIdMsg, String commentsmsg) {
		String msg = getDriver().findElement(blnkawbmsg).getText();
		softAssert.assertEquals(msg, ShipmentIdMsg, "Shipment id is blank");
		softAssert.assertAll();
		String msg1 = getDriver().findElement(blnkcomment).getText();
		softAssert.assertEquals(msg1, commentsmsg, "Comment is blank");
		softAssert.assertAll();
	}

	public void OnSuccess(String Popupheading, String SuccessMsg) throws InterruptedException {
		Thread.sleep(3000);
		String heading = $(successpopup).waitUntilVisible().waitUntilPresent().getText();
		softAssert.assertEquals(heading, Popupheading, "Popup heading is as expected");
		softAssert.assertAll();
		Thread.sleep(3000);
		try {
			String msg = getDriver().findElement(successmsg).getText();
			softAssert.assertEquals(msg, SuccessMsg, "Sucess message displayed successfully");
			softAssert.assertAll();
		} catch (Exception e) {
			String msg = getDriver().findElement(successmsg1).getText();
			softAssert.assertEquals(msg, "Shipment Status is Already Invalid");
			softAssert.assertAll();
		}
		getDriver().findElement(closepopup).click();
	}

	public void EnterComment(String Comment) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(commentInp).sendKeys(Comment);
	}

	public void ClickUpdatebtn() {
		getDriver().findElement(updatebtn).click();
	}

	public void InvalidAWBNoMsg(String InvalidAWBMessage) throws InterruptedException {
		Thread.sleep(3000);
		String msg = getDriver().findElement(invalidAWBmsg).getText();
		softAssert.assertEquals(msg, InvalidAWBMessage, "Invalid AWB number entered");
		softAssert.assertAll();
	}

	public void MoreThanTenAWB(String ShipmentId) {
		getDriver().findElement(enterShipmentidInp).sendKeys(ShipmentId);
	}

	public void MaxTenAWBAllowed(String MaxTenAWBMsg) {
		String msg = getDriver().findElement(maxtenAWB).getText();
		softAssert.assertEquals(msg, MaxTenAWBMsg, "Max 10 AWB allowed");
		softAssert.assertAll();
	}

	public void PressEnterKey(String ShipmentId,String Value) {
getDriver().findElement(enterShipmentidInp).sendKeys(ShipmentId);	
getDriver().findElement(enterShipmentidInp).sendKeys(Keys.ENTER);
String txtvalue=getDriver().findElement(enterShipmentidInp).getText();
softAssert.assertEquals(txtvalue, Value,"New line not inserted");
softAssert.assertAll();
	}

	

	

	
}
