package com.xb.cafe.ui.pages;

import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;
import net.serenitybdd.core.pages.PageObject;

public class CargoBooking_FileUpload extends PageObject{
	 String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	public  final By menu_CargoBooking = By.xpath("//span[contains(text(),'Cargo Booking')]");
	public  final By flipkarttab = By.xpath("//span[contains(text(),'Flipkart Soft Data Upload')]");
	public  final By clientfiletype=By.xpath("//label[contains(text(),'Client File Type')]");
	public  final By withselleridlocator=By.xpath("//label[contains(text(),'With Seller ID ')]");
	public  final By withoutselleridlocator=By.xpath("//label[contains(text(),'Without Seller ID ')]");
	public  final By clientTxtinput=By.xpath("//input[@class='vue-treeselect__input']");
	public  final By link_ChooseFile = By.xpath("//input[@id='FileUpload']");
	
	public  final By btn_Submit = By.xpath("//button[contains(text(),'Submit')]");
	public  final By btn_Reset = By.xpath("//button[contains(text(),'Reset')]");
	public  final By getMsg_FileUpload = By.xpath("//div[@id='swal2-content']");
	public  final By SuccMessage_FileUpload = By.xpath("//div[contains(text(),'File Upload Successful.')]");
	public  final By Btn_OK = By.xpath("//button[@class='swal2-confirm swal2-styled']");
	public  final By OKbtn= By.xpath("//button[contains(text(),'OK')]");
	public  final By uploadedDetails_TotalRecords = By.xpath("//section[@class='velmld-parent']/fieldset/table/tbody/tr[1]/td[1]");
	public  final By uploadedDetails_Uploaded = By.xpath("//section[@class='velmld-parent']/fieldset/table/tbody/tr[1]/td[2]");
	public  final By uploadedDetails_NotUploaded = By.xpath("//section[@class='velmld-parent']/fieldset/table/tbody/tr[1]/td[3]");
	
	
	 SoftAssert softAssert= new SoftAssert();
	public  void openApplication(String URL) throws InterruptedException {
		getDriver().get(URL);
	}
	
	public  void NavigateBooking() throws InterruptedException 
	{
		
		
		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			getDriver().findElement(flipkarttab).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(menu_CargoBooking).click();
			Thread.sleep(3000);
			getDriver().findElement(flipkarttab).click();
			Thread.sleep(3000);
		}
	}
	
	
	public  void userEntersClientFileTypeInformation(String clientfiletype) throws InterruptedException {
		Thread.sleep(12000);
		if(clientfiletype.equals("With Seller ID")) {
			Thread.sleep(12000);
			getDriver().findElement(withselleridlocator).click();
		}
		else {
			Thread.sleep(12000);
			getDriver().findElement(withoutselleridlocator).click();
		
	}
}
	
	
	public  void selectClient(String ClientName) throws InterruptedException 
	{
		Thread.sleep(12000);
		WebElement clientTxtbox=getDriver().findElement(clientTxtinput);
		clientTxtbox.sendKeys(ClientName);
		Thread.sleep(12000);
		clientTxtbox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);	

}
	public  void uploadFile(String FileName) throws InterruptedException 
	{
		Thread.sleep(1000);
		if(FileName.equals("SampleFileWithSeller.csv")) {
		WebElement uploadFile = getDriver().findElement(link_ChooseFile);
		Thread.sleep(1000);
		uploadFile.sendKeys("/home/rushikeshwadgaonkar/git/qa_automation/B2B_Cargo_Serenity/Test Data/SampleFileWithSeller.csv");
		}
		else if(FileName.equals("SampleFileWithoutSeller.csv")){
			WebElement uploadFile = getDriver().findElement(link_ChooseFile);
		Thread.sleep(1000);	
		uploadFile.sendKeys("/home/rushikeshwadgaonkar/git/qa_automation/B2B_Cargo_Serenity/Test Data/SampleFileWithoutSeller.csv");
		}
	}
	public  void submitAndValidateSuccessMsg(String Message_FileUpload) throws InterruptedException 
	{   
		getDriver().findElement(btn_Submit).click();
		Thread.sleep(2000);
		WebElement myDynamicElement = (new WebDriverWait(getDriver(), 200))
				.until(ExpectedConditions.presenceOfElementLocated(getMsg_FileUpload));	
		Thread.sleep(3000);
	     softAssert.assertEquals(getDriver().findElement(getMsg_FileUpload).getText().trim(), Message_FileUpload, "Expected success message not found");
	     getDriver().findElement(Btn_OK).click();
	     Thread.sleep(1000);
	     softAssert.assertAll();
	}

	public  void userClicksOnSubmitButtonAndValidateTheErrorMessage() throws InterruptedException {
		getDriver().findElement(btn_Submit).click();
		Thread.sleep(5000);
	}

	public  void userSearchesClientNameAndValidateSearch(String ClientName) throws InterruptedException {
		Thread.sleep(12000);
		WebElement clientTxtbox1=getDriver().findElement(clientTxtinput);
		clientTxtbox1.sendKeys(ClientName);
		Thread.sleep(3000);
		clientTxtbox1.sendKeys(Keys.ENTER);
		Thread.sleep(3000);
	}

	public  void userSearchesInvalidClientNameAndValidateSearch(String ClientName) throws InterruptedException {
		
		Thread.sleep(3000);
		WebElement clientTxtbox1=getDriver().findElement(clientTxtinput);
		clientTxtbox1.sendKeys(ClientName);
		Thread.sleep(3000);
		clientTxtbox1.sendKeys(Keys.ENTER);
		Thread.sleep(3000);
	}

	public  void userClicksOnResetButton() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(btn_Reset));
		getDriver().findElement(btn_Reset).click();
		
	}

	public  void userUploadsTheFile(String filename) throws InterruptedException {
		Thread.sleep(1000);
			WebElement newFile = getDriver().findElement(link_ChooseFile);
			Thread.sleep(2000);
			newFile.sendKeys("/home/rushikeshwadgaonkar/git/qa_automation/B2B_Cargo_Serenity/Test Data/Sample_Booking_Data.xlsx");
		}

	public  void userClicksOnSubmitbuttonn() throws InterruptedException {
		getDriver().findElement(OKbtn).click();
		Thread.sleep(5000);
		getDriver().findElement(btn_Submit).click();
		Thread.sleep(5000);
		
	}
}
