package com.xb.cafe.ui.pages;

import java.util.List;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;



import net.serenitybdd.core.pages.PageObject;

public class ChangeInvoiceQuantity extends PageObject {

	 public static final String UserValidatesTheErrorMsgAndUserClicksOnOkButton = null;
	String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	 String username;
	 String TagName1;
	 String TagComment1;
	 String Tag_MappedEnvironment;
	//  WebDriver driver = null;

	public  final By input_UN = By.xpath("//input[@name='email']");
	public  final By input_PW = By.xpath("//input[@id='typepass']");
	public  final By btn_Login = By.xpath("//button[contains(text(),'Login')]");
	public  final By Lnk_Support = By.xpath("//span[contains(text(),'Support')]");
	public  final By lnk_ChangeInvoiceQuantity = By.xpath("//span[contains(text(),'Change Invoice Quantity')]");
	public  final By input_awbNo = By.xpath("//input[@id='awbnumber']");
	public  final By btn_Search = By.xpath("//button[contains(text(),'Search')]");
	
	public  final By btn_Update = By.xpath("//button[contains(text(),'Update')]");;
	public  final By msg_Success = By.xpath("//div[contains(text(),'Invoice Count Updated Successfully.')]");
	public  final By btn_OK = By.xpath("//button[contains(text(),'OK')]");
	public  final By btn_Reset = By.xpath("//button[contains(text(),'Reset')]");
	public  final By msg_error = By.xpath("//div[contains(text(),'Data not found')]");
	public  final By lnk_Clientdashboard = By.xpath("//span[contains(text(),'Client Dashboard')]");
	public  final By input_InvoiceQuantity = By.xpath("//input[@id='invoicequantity']"); 
	public  final By Message_Error = By.xpath("//div[contains(text(),'Cannot Update Invoice For Complete Booking')]");
	public  final By msg_error1 = By.xpath("//div[contains(text(),'Shipment Id is required')]");		
	public  final By invoiceerror = By.xpath("//span[contains(text(),'Please enter invoice quantity')]");
	
	
	 SoftAssert softAssert = new SoftAssert();
	
	

	public  void LoginSelfServicepage(String URL) throws InterruptedException {
		getDriver().get(URL);
	}

	
	public void NavigatesToSupportOption() throws InterruptedException {
		Thread.sleep(3000);
        getDriver().navigate().refresh();
        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
        try {
                getDriver().navigate().refresh();
                Thread.sleep(2000);
                wait.until(ExpectedConditions.elementToBeClickable(lnk_ChangeInvoiceQuantity));
                getDriver().findElement(lnk_ChangeInvoiceQuantity).click();
                Thread.sleep(2000);
        } catch (Exception e) {
                getDriver().navigate().refresh();
                Thread.sleep(2000);
                wait.until(ExpectedConditions.elementToBeClickable(Lnk_Support));
                getDriver().findElement(Lnk_Support).click();
                Thread.sleep(2000);
                wait.until(ExpectedConditions.elementToBeClickable(lnk_ChangeInvoiceQuantity));
                getDriver().findElement(lnk_ChangeInvoiceQuantity).click();
        }
	}
	public void userEntersInTheTextFieldUserClicksOnSearchButton(String AWB) throws InterruptedException {
	
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(input_awbNo));
		WebElement awbsearchTxt=getDriver().findElement(input_awbNo);
		awbsearchTxt.sendKeys(AWB);
		Thread.sleep(1000);
		awbsearchTxt.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(btn_Search));
		getDriver().findElement(btn_Search).click();
		
		}


	public void userClicksOnUpdateButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
      
		wait.until(ExpectedConditions.elementToBeClickable(btn_Update));
		getDriver().findElement(btn_Update).click();
		
}


	public void userValidatesTheSuccessfulMsgAndUserClicksOnOkButton(String Successmsg) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		softAssert.assertEquals(getDriver().findElement(msg_Success).getText().trim(), Successmsg,"Invoice Count Updated Successfully.");
		wait.until(ExpectedConditions.elementToBeClickable(btn_OK));
		getDriver().findElement(btn_OK).click();
		
	}
	
   public void userEntersInvalidInTheTextFieldAndUserClicksOnSearchButton(String AWB) throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(getDriver(),10);

	wait.until(ExpectedConditions.visibilityOfElementLocated(input_awbNo));
	WebElement awbsearchTxt=getDriver().findElement(input_awbNo);
	awbsearchTxt.sendKeys(AWB);
	Thread.sleep(1000);
	awbsearchTxt.sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	wait.until(ExpectedConditions.elementToBeClickable(btn_Search));
	getDriver().findElement(btn_Search).click();
	
}
 public void userValidatesTheErrorMsgAndUserClicksOnOkButton(String errormsg) throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(getDriver(),10);
	Thread.sleep(1000);
	softAssert.assertEquals(getDriver().findElement(msg_error).getText().trim(), errormsg,"Data not found");
	softAssert.assertAll();

	wait.until(ExpectedConditions.elementToBeClickable(btn_OK));
	getDriver().findElement(btn_OK).click();
	
}

public void UserClicksOnResetButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
	  
		wait.until(ExpectedConditions.elementToBeClickable(btn_Reset));
		
		getDriver().findElement(btn_Reset).click();
		Thread.sleep(3000);
		
	}

public void userEntersInTheTextFieldAndClicksOnSearchButton(String AWB) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		

		wait.until(ExpectedConditions.visibilityOfElementLocated(input_awbNo));
		WebElement awbsearchTxt=getDriver().findElement(input_awbNo);
		awbsearchTxt.sendKeys(AWB);
		Thread.sleep(1000);
		awbsearchTxt.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(btn_Search));
		getDriver().findElement(btn_Search).click();
}

public void userEntersShippingidOfCompleteBookingInTheTextFieldAndUserClicksOnSearchButton(String AWB) throws InterruptedException{
	WebDriverWait wait = new WebDriverWait(getDriver(),10);
	
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(input_awbNo));
	WebElement awbsearchTxt=getDriver().findElement(input_awbNo);
	awbsearchTxt.sendKeys(AWB);
	Thread.sleep(1000);
	awbsearchTxt.sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	wait.until(ExpectedConditions.elementToBeClickable(btn_Search));
	getDriver().findElement(btn_Search).click();
}


public void userValidatesTheErrorMsgAndUserClicksOnOkButton1(String Errormsg) throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(getDriver(),10);
	Thread.sleep(3000);
	softAssert.assertEquals(getDriver().findElement(Message_Error).getText().trim(), Errormsg,"Cannot Update Invoice For Complete Booking");
	softAssert.assertAll();
	wait.until(ExpectedConditions.elementToBeClickable(btn_OK));
	getDriver().findElement(btn_OK).click();
	
}


public void userRemovesPreFilledInvoiceQuantityAndEntersaNewQuantity(String InvoiceQuantityvalue) throws InterruptedException{
	WebDriverWait wait = new WebDriverWait(getDriver(),10);
	wait.until(ExpectedConditions.visibilityOfElementLocated(input_InvoiceQuantity));
	WebElement InvoiceQuantityTxt=getDriver().findElement(input_InvoiceQuantity);
    InvoiceQuantityTxt.clear();
    Thread.sleep(1000);
    InvoiceQuantityTxt.sendKeys(InvoiceQuantityvalue);
	InvoiceQuantityTxt.sendKeys(Keys.ENTER);
	}


public void userEentersInTheTextFieldAndClicksOnSearchButton(String AWB) throws InterruptedException{
WebDriverWait wait = new WebDriverWait(getDriver(),10);
	
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(input_awbNo));
	WebElement awbsearchTxt=getDriver().findElement(input_awbNo);
	awbsearchTxt.sendKeys(AWB);
	Thread.sleep(1000);
	awbsearchTxt.sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	wait.until(ExpectedConditions.elementToBeClickable(btn_Search));
	getDriver().findElement(btn_Search).click();
}


public void userClicksOnSearchButtonWithoutEnteringShippingid() {
	WebDriverWait wait = new WebDriverWait(getDriver(),10);
	

	wait.until(ExpectedConditions.visibilityOfElementLocated(input_awbNo));
	wait.until(ExpectedConditions.elementToBeClickable(btn_Search));
	getDriver().findElement(btn_Search).click();
}




public void userValidatesErrorMsgAndUserClicksOnOkButton(String errormessage) throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(getDriver(),10);
	softAssert.assertEquals(getDriver().findElement(msg_error1).getText().trim(), errormessage,"Shipment Id is required");
	wait.until(ExpectedConditions.elementToBeClickable(btn_OK));
	getDriver().findElement(btn_OK).click();

	
}




	
}
	
	





	




	

	





		
	

	

	