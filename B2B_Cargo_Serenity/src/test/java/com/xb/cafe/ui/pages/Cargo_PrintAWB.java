package com.xb.cafe.ui.pages;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;


import net.serenitybdd.core.pages.PageObject;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

public class Cargo_PrintAWB extends PageObject {


	public  final By print_AWB = By.xpath("//span[contains(text(),'Print AWB')]");
	public  final By reset_bttn = By.xpath("//button[@title='Reset']");
	public  final By txt_box_AWBnos = By.xpath("//textarea[@id='AWBNumber']");
	public  final By btn_ValidateAWB = By.xpath("//button[@title='Validate AWB(s)']");

	public  final By btn_print = By.xpath("//button[@title='Print AWB(s)']");
	public  final By Msg_noValidAWBs = By.xpath("//div[@id='swal2-content']");
	public  final By Btn_OK = By.xpath("//button[contains(text(),'OK')]");

	public  final By blank_awb_error=By.xpath("//span[contains(text(),'The AWB Number field is required')]");
	public  final By Max_awb_error=By.xpath("//span[contains(text(),'Maximum of 20 AWB(s) you can enter')]");
	
	public  final By table_valid_AWBs = By.xpath("//table[@class='table-bordered tbl datatables']/tbody/tr");
	
	
	 SoftAssert softAssert = new SoftAssert();

	public  void navigate_PrintAWBMenu() throws InterruptedException {
		Thread.sleep(5000);
		getDriver().findElement(print_AWB).click();
		Thread.sleep(3000);
		getDriver().findElement(reset_bttn).click();

	}

	public  void printAWB_EnterAWBnumber(String AWBnumber) throws InterruptedException {

		getDriver().findElement(txt_box_AWBnos).sendKeys(AWBnumber);
		Thread.sleep(2000);
		getDriver().findElement(btn_ValidateAWB).click();
		Thread.sleep(1000);
	}

	public  void validate_Details_Displayed(String AWBNumber) throws InterruptedException {
		
		String[] awb_nos_array=AWBNumber.split(",");
		int awb_nos=awb_nos_array.length;
		String convo_awb=Integer.toString(awb_nos);
		String lbl_AWBDetails="//label[contains(text(),'Validated AWB(s): 1')]".replace("1", convo_awb);
		String awb_1st=awb_nos_array[0];
		Thread.sleep(2000);
		softAssert.assertTrue(getDriver().findElement(By.xpath(lbl_AWBDetails)).isDisplayed(),"Expected label is not displayed");
		String ValidatedAWB = "//td[contains(text(),'12')]".replace("12", awb_1st);
		softAssert.assertTrue(getDriver().findElement(By.xpath((ValidatedAWB))).isDisplayed());
		softAssert.assertAll();

	}
	

	public  void printAWBandCancel() throws InterruptedException, AWTException {
		System.out.print("Success");
	}
	
	public  void validate_Blank_Data_error_displayed(String AWBNumber, String errMsg) {
		if(AWBNumber.isEmpty()) {
		String errorMsg=getDriver().findElement(blank_awb_error).getText();
		softAssert.assertEquals(errorMsg, errMsg);
		softAssert.assertAll();
		}else {
			String MaxerrorMsg=getDriver().findElement(Max_awb_error).getText();
			softAssert.assertEquals(MaxerrorMsg, errMsg);
			softAssert.assertAll();
			
		}
	}
	
	
	public  void validate_Details_Displayed_for_Invalid_AWBs(String Message) throws InterruptedException {
		softAssert.assertEquals(getDriver().findElement(Msg_noValidAWBs).getText(), Message);
		Thread.sleep(3000);
		getDriver().findElement(Btn_OK).click();
		Thread.sleep(3000);
		softAssert.assertAll();
	}
	
	public  void validate_No_of_AWB_Details_Displayed() {
		int iCount = getDriver().findElements(table_valid_AWBs).size();
		String str1 ="//label[contains(text(),'Validated AWB(s): 6')]".replace("6", Integer.toString(iCount));
		softAssert.assertTrue(getDriver().findElement(By.xpath((str1))).isDisplayed());
		softAssert.assertAll();
		
	}
}
	