package com.xb.cafe.ui.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

public class FileUploadNew extends PageObject{
	private final By Upload_btn = null;
	public final By cargoBookingBttn = By.xpath("//span[contains(text(),'Cargo Booking')]");
	public final By cargoBookingArrow= By.xpath("//div[@class='vsm-arrow open-arrow']");
	public final By bookingBttn = By.xpath("//span[text()=\"Booking\"]");
	public final By createBookingBttn=By.xpath("//a[@title='Create Booking']");
	public final By uploadButtonLocator=By.xpath("//button[@title='Data Upload']");
	public final By clientTxtinput=By.xpath("//input[@class='vue-treeselect__input']");
	public final By fileUploadLocator=By.xpath("//input[@id='uploadBookingData']");
	public final By submitButtonLocator=By.xpath("//button[contains(text(),'Submit')]");
	public final By okButtonLocator=By.xpath("(//button[contains(text(),'OK')])[4]");
	public final By totalRecordsLocator=By.xpath("//th[contains(text(),'Total Records')]");
	public final By validRecordsLocator=By.xpath("//th[contains(text(),'Valid Records')]");
	public final By downloadTemplateLocator=By.xpath("//a[@title='Download Template']");
	public final By resetButtonLocator=By.xpath("//button[@title='Reset']");
	public final By clientTextLocator=By.xpath("//label[contains(text(),'Client')]");
	public final By chooseFileTextLocator=By.xpath("//a[contains(text(),'Choose File')]");
	public final By uploadFormatErrorMessageLocator=By.xpath("//div[contains(text(),'The upload format is incorrect. Please download template for correct format')]");
	public final By invalidRecordsLocator=By.xpath("//th[contains(text(),'Invalid Records')]");
	public final By clientErrorMessageLocator=By.xpath("//span[contains(text(),'The Client field is required')]");
	public final By fileErrorMessageLocator=By.xpath("//span[contains(text(),'Choose file to upload')]");
	public final By invalidFileLinkLocator=By.xpath("//span[@title='Invaild Records']");
	public final By validFileLinkLocator=By.xpath("//span[@title='Vaild Records']");
	public final By importValidRecordsLocator=By.xpath("//button[@title='Import Valid Records']");
	public final By messageLocator=By.xpath("//th[contains(text(),'Message')]");
	public final By fileHeaderMessageLocator=By.xpath("//div[contains(text(),'File headers not matched. Kindly use Download Template option for correct file format.')]");
	public final By poErrorMessageLocator=By.xpath("(//td[contains(text(),'Import Failed : PO Number already exists')])[1]");
	SoftAssert softAssert = new SoftAssert();
	

	public void navigateToCargoBookingMenu() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(bookingBttn));
			getDriver().findElement(bookingBttn).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(cargoBookingBttn).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(bookingBttn));
			getDriver().findElement(bookingBttn).click();
			Thread.sleep(3000);
		}
	}
	public void userNavigatesToCreateBoolingOptionAndClickOnDataUploadButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		getDriver().findElement(createBookingBttn).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(uploadButtonLocator));
		getDriver().findElement(uploadButtonLocator).click();
	}
	public void SelectTheClient(String clientName) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientTxtinput));
		WebElement clientTxtbox=getDriver().findElement(clientTxtinput);
		clientTxtbox.sendKeys(clientName);
		Thread.sleep(1000);
		clientTxtbox.sendKeys(Keys.ENTER);
		
	}
	public void SelectTheAndClickOnSubmitButton(String filename) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		if(filename.equals("WithoutSellerfileupload.csv")) {
			WebElement uploadFile = getDriver().findElement(fileUploadLocator);
			Thread.sleep(1000);
			getDriver().findElement(fileUploadLocator).sendKeys(System.getProperty("user.dir")+"/Test Data/WithoutSellerfileupload.csv");
		}
		else if(filename.equals("WithSellerfileupload.csv")){
			WebElement uploadFile2 = getDriver().findElement(fileUploadLocator);
		Thread.sleep(1000);	
		getDriver().findElement(fileUploadLocator).sendKeys(System.getProperty("user.dir")+"/Test Data/WithSellerfileupload.csv");
		}
			wait.until(ExpectedConditions.visibilityOfElementLocated(submitButtonLocator));
			getDriver().findElement(submitButtonLocator).click();	
		}



	public void validateTheUploadDetailsSection(String totalRecords, String validRecords) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(okButtonLocator));
		getDriver().findElement(okButtonLocator).click();
		Thread.sleep(3000);
		 softAssert.assertEquals(getDriver().findElement(totalRecordsLocator).getText().trim(), totalRecords, "Total Records");
		 softAssert.assertEquals(getDriver().findElement(validRecordsLocator).getText().trim(), validRecords, "Valid Records");
		 softAssert.assertAll();
		
	}
	public void clickOnDownloadTemplateButtonAndValidate() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(downloadTemplateLocator));
		getDriver().findElement(downloadTemplateLocator).click();
		
	}
	public void clickOnResetButtonAndValidate() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		WebElement element = getDriver().findElement(By.xpath("//button[@title='Reset']"));
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript("arguments[0].click();", element);
		
	}
	public void validateTheElementsPresent(String client, String chooseFile, String submit, String reset) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		 softAssert.assertEquals(getDriver().findElement(clientTextLocator).getText().trim(), client, "Client");
		 softAssert.assertEquals(getDriver().findElement(chooseFileTextLocator).getText().trim(), chooseFile, "Choose File");
		 softAssert.assertEquals(getDriver().findElement(submitButtonLocator).getText().trim(), submit, "Submit");
		 softAssert.assertEquals(getDriver().findElement(resetButtonLocator).getText().trim(), reset, "Reset");
         softAssert.assertAll();
	}
	public void userSelectsInvalidFileFormatAndClickOnSubmitButton(String file) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		if(file.equals("Pdffile.pdf")) {
			WebElement uploadFile = getDriver().findElement(fileUploadLocator);
			Thread.sleep(1000);
			getDriver().findElement(fileUploadLocator).sendKeys( System.getProperty("user.dir")+"/Test Data/Pdffile.pdf");
		}
			else if(file.equals("InvalidRecords.csv")){
				WebElement uploadFile = getDriver().findElement(fileUploadLocator);
			Thread.sleep(1000);	
			getDriver().findElement(fileUploadLocator).sendKeys( System.getProperty("user.dir")+"/Test Data/InvalidRecords.csv");
			}
	}

	public void validateTheErrorMessage(String uploadFormatErrorMessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		 softAssert.assertEquals(getDriver().findElement(uploadFormatErrorMessageLocator).getText().trim(), uploadFormatErrorMessage, "The upload format is incorrect. Please download template for correct format");
		 softAssert.assertAll();
		 wait.until(ExpectedConditions.visibilityOfElementLocated(okButtonLocator));
			getDriver().findElement(okButtonLocator).click();
	}
	public void validateTheUploadDetailsSection(String totalRecords, String validRecords, String invalidRecords) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(okButtonLocator));
		getDriver().findElement(okButtonLocator).click();
		Thread.sleep(3000);
		 softAssert.assertEquals(getDriver().findElement(totalRecordsLocator).getText().trim(), totalRecords, "Total Records");
		 softAssert.assertEquals(getDriver().findElement(validRecordsLocator).getText().trim(), validRecords, "Valid Records");
		 softAssert.assertEquals(getDriver().findElement(invalidRecordsLocator).getText().trim(), invalidRecords, "Invalid Records");
		 softAssert.assertAll();
	}
	public void clickOnSubmitButton() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
	wait.until(ExpectedConditions.visibilityOfElementLocated(submitButtonLocator));
		getDriver().findElement(submitButtonLocator).click();	
		
	}
	public void validateTheErrorMessages(String clientError, String fileError) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		softAssert.assertEquals(getDriver().findElement(clientErrorMessageLocator).getText().trim(), clientError, "The Client field is required");
		 softAssert.assertEquals(getDriver().findElement(fileErrorMessageLocator).getText().trim(), fileError, "Choose file to upload");
		 softAssert.assertAll();
	}
	public void userClicksOnInvalidFileLinkAndValidateTheMessage(String invalidFileErrorMessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(submitButtonLocator));
		getDriver().findElement(submitButtonLocator).click();	
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(okButtonLocator));
		getDriver().findElement(okButtonLocator).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invalidFileLinkLocator));
		getDriver().findElement(invalidFileLinkLocator).click();
		softAssert.assertEquals(getDriver().findElement(messageLocator).getText().trim(), invalidFileErrorMessage, "Message");
		 softAssert.assertAll();
		
	}
	public void selectTheExcelFileAndClickOnSubmitButton(String excelFile) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		if(excelFile.equals("WithSellerfileupload.xlsx")) {
			WebElement uploadFile3 = getDriver().findElement(fileUploadLocator);
			Thread.sleep(1000);
			getDriver().findElement(fileUploadLocator).sendKeys( System.getProperty("user.dir")+"/Test Data/WithSellerfileupload.xlsx");
			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(okButtonLocator));
			getDriver().findElement(okButtonLocator).click();
		}
		else if(excelFile.equals("WithSellerfileupload.csv")){
			WebElement uploadFile4 = getDriver().findElement(fileUploadLocator);
		Thread.sleep(1000);	
		getDriver().findElement(fileUploadLocator).sendKeys( System.getProperty("user.dir")+"/Test Data/WithSellerfileupload.csv");
		}
		
	}
	public void validateTheFileHeaderErrorMessage(String fileHeaderMessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(submitButtonLocator));
		getDriver().findElement(submitButtonLocator).click();
		Thread.sleep(1000);	
		softAssert.assertEquals(getDriver().findElement(fileHeaderMessageLocator).getText().trim(), fileHeaderMessage, "File headers not matched. Kindly use Download Template option for correct file format.");
		 softAssert.assertAll();
		 Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(okButtonLocator));
			getDriver().findElement(okButtonLocator).click();
	}
	public void selectTheWithoutSellerIdFileAndClickOnSubmitButton(String withoutSellerIdFile) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		if(withoutSellerIdFile.equals("WithoutSellerfileupload.csv")) {
			WebElement uploadFile5 = getDriver().findElement(fileUploadLocator);
			Thread.sleep(1000);
			getDriver().findElement(fileUploadLocator).sendKeys(System.getProperty("user.dir")+"/Test Data/WithoutSellerfileupload.csv");
		}
	}
	public void clickOnTheImportRecordsButtonAndClickOnOkButton() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(submitButtonLocator));
		getDriver().findElement(submitButtonLocator).click();	
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(okButtonLocator));
		getDriver().findElement(okButtonLocator).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(validFileLinkLocator));
		getDriver().findElement(validFileLinkLocator).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(importValidRecordsLocator));
		getDriver().findElement(importValidRecordsLocator).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(okButtonLocator));
		getDriver().findElement(okButtonLocator).click();
		
	}
	public void validateThePoErrorMessage(String poErrorMessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
	    js.executeScript("window.scrollBy(0,250)", "");
		softAssert.assertEquals(getDriver().findElement(poErrorMessageLocator).getText().trim(), poErrorMessage, "Import Failed : PO Number already exists");
		 softAssert.assertAll();
		
	}
}