package com.xb.cafe.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;

public class FTCcargo extends PageObject {
	
	
	public final By flmLocator=By.xpath("//span[contains(text(),'First & Last Mile')]");
	public  final By ftcLocator=By.xpath("//span[contains(text(),'FTC')]");
	public  final By ftcRadShipmentLocator=By.xpath("//span[contains(text(),'FTC RAD Shipment')]");
	public final By calculateFTCLocator=By.xpath("//a[contains(text(),'Calculate FTC ')]");
	public final By ftcMainPageLocator=By.xpath("//label[contains(text(),'Calculate FTC')]");
	public final By paymentLinkLocator=By.xpath("//button[contains(text(),'Send payment link')]");
	public final By sendButtonLocator=By.xpath("(//button[contains(text(),'Send')])[2]");
	public final By printMemoButtonLocator=By.xpath("//button[contains(text(),'View/Print Cash Memo')]");
	public final By cashMemoMessageLocator=By.xpath("(//span[contains(text(),'View/Print Cash Memo')])[1]");
	public final By printButtonLocator=By.xpath("(//button[contains(text(),'Print')])[2]");
	public final By awbLocator=By.xpath("//th[contains(text(),'AWB:')]");
	public final By totalNumberOfMpsLocator=By.xpath("//th[contains(text(),'Total number of MPS:')]");
	public final By originCityHubLocator=By.xpath("//th[contains(text(),'Origin City (Hub):')]");
	public final By destinationCityHubLocator=By.xpath("//th[contains(text(),'Destination City (Hub):')]");
	public final By pickupDateLocator=By.xpath("//th[contains(text(),'Pick Up Date :')]");
	public final By radDateLocator=By.xpath("//th[contains(text(),'RAD Date:')]");
	public final By consignorNameLocator=By.xpath("//legend[contains(text(),'Consignor Name and Address')]");
	public final By consigneeNameLocator=By.xpath("//legend[contains(text(),'Consignee Name and Address')]");
	public final By searchLinkLocator=By.xpath("//i[@title='Search Filter']");
	public final By awbNumberLocator=By.xpath("//input[@id='AWB']");
	public final By clientNameLocator=By.xpath("(//input[@class='vue-treeselect__input'])[1]");
	public final By radFromDateLocator=By.xpath("//input[@name='radFromDate']");
	public final By radToDateLocator=By.xpath("//input[@name='radToDate']");
	public final By cashMemoLocator=By.xpath("(//i[@class='cr-icon fa fa-check'])[1]");
	public final By searchLocator=By.xpath("//button[contains(text(),'Search')]");
	public final By reCalculateButtonLocator=By.xpath("//a[contains(text(),'Recalculate FTC')]");
	public final By actionConfirmationMessageLocator=By.xpath("//span[contains(text(),'Action Confirmation: Confirm FTC Amount')]");
	public final By confirmFtcButtonLocator=By.xpath("//button[contains(text(),'Confirm FTC Amount')]");
	public final By NoButtonLocator=By.xpath("//button[contains(text(),'No')]");
	public final By shipmentSizedChargesLocator=By.xpath("//input[@id='shippingsize']");
	public final By varaiChargesLocator=By.cssSelector("input[id=mathadi]");
	public final By portgageChargesLocator=By.xpath("//input[@id='portage']");
	public final By specialChargesLocator=By.xpath("//input[@id='specialcharge']");
	public final By loadChargesLocator=By.xpath("//input[@id='loadcharges']");
	public final By odaChargesLocator=By.xpath("//input[@id='oda']");
	public final By fullTruckLoadLocator=By.xpath("//input[@id='fulltruckload']");
	public final By saveButtonLocator=By.xpath("//button[contains(text(),'Save')]");
	public final By successfulmessagelocator=By.xpath("//div[contains(text(),'successfull.')]");
	public final By addExtraChargesButtonLocator=By.xpath("//button[contains(text(),'Add Extra Charges')]");
	public final By freightChargeTab=By.xpath("(//label[@class='tab-label'])[1]");
	public final By WeightLocator=By.xpath("//legend[contains(text(),'Weights')]");
	public final By chargeCalculationLocator=By.xpath("(//legend[contains(text(),'Charge Calculation')])[1]");
	public final By insuranceChargesTabLocator=By.xpath("(//label[@class='tab-label'])[2]");
	public final By applicableChargeLocator=By.xpath("//th[contains(text(),'Applicable charge:')]");
	public final By rovChargeTypeLocator=By.xpath("(//th[contains(text(),'ROV Charge Type:')])[1]");
	public final By rovPercentageTypeLocator=By.xpath("//th[contains(text(),'ROV % type')]");
	public final By rovRateLocator=By.xpath("//th[contains(text(),'ROV Rate (%):')]");
	public final By limitRovLocator=By.xpath("//th[contains(text(),'Limits (ROV Charges) (INR)')]");
	public final By invoiceValueLocator=By.xpath("//th[contains(text(),'Invoice Value (INR):')]");
	public final By rovChargesTextLocator=By.xpath("//th[contains(text(),'ROV Charges (INR) :')]");
	public final By rovChargesPayableLocator=By.xpath("//th[contains(text(),'ROV Charges payable (INR) :')]");
	public final By accessorialChargesTabLocator=By.xpath("(//label[@class='tab-label'])[3]");
	public final By wayBillChargesLocator=By.xpath("//th[contains(text(),'Waybill Charges:')]");
	public final By dcChargesLocator=By.xpath("//th[contains(text(),'DC Charges:')]");
	public final By ftcChargesLocator=By.xpath("//th[contains(text(),'FTC (To Pay) Charges:')]");
	public final By fuelSurchargeLocator=By.xpath("//th[contains(text(),'Fuel Surcharge:')]");
	public final By deliveryAttemptChargesLocator=By.xpath("//th[contains(text(),'Delivery Attempt Charges:')]");
	public final By demurrageChargesLocator=By.xpath("//th[contains(text(),'Demurrage Charges:')]");
	public final By appointmentDeliveryChargesLocator=By.xpath("//th[contains(text(),'Appointment/ Different Vehicle Charges:')]");
	public final By loadingChargesLocator=By.xpath("(//th[contains(text(),'Loading/Unloading Charges:')])[1]");
	public final By openDeliveryChargesLocator=By.xpath("//th[contains(text(),'Open pickup/delivery charges:')]");
	public final By handlingChargesLocator=By.xpath("//th[contains(text(),'Handling Charges:')]");
	public final By groundFloorChargesLocator=By.xpath("//th[contains(text(),'Delivery above ground floor charges:')]");
	public final By greenTaxLocator=By.xpath("//th[contains(text(),'Green Tax:')]");
	public final By rtoChargesLocator=By.xpath("//th[contains(text(),'RTO Charges:')]");
	public final By odaLocator=By.xpath("//th[contains(text(),'ODA charges:')]");
	public final By holidayDeliveryChargesLocator=By.xpath("//th[contains(text(),'Holiday delivery charges:')]");
	public final By ftcRadTextLocator=By.xpath("//h4[contains(text(),'FTC RAD Shipments')]");
	public final By srNoFieldLocator=By.xpath("//th[contains(text(),'Sr.No')]");
	public final By awbFieldLocator=By.xpath("//th[contains(text(),'AWB')]");
	public final By mpsCoFieldLocator=By.xpath("//th[contains(text(),'MPS Co')]");
	public final By clientNameFieldLocator=By.xpath("//th[contains(text(),'Client Name')]");
	public final By consigneeNameFieldLocator=By.xpath("//th[contains(text(),'Consignee Name')]");
	public final By consigneeAddressLocator=By.xpath("//th[contains(text(),'Consignee Address')]");
	public final By radDateFieldLocator=By.xpath("//th[contains(text(),'RAD Date')]");
	public final By cashMemoFieldLocator=By.xpath("//th[contains(text(),'Cash Memo Generate')]");
	public final By paymentStatusFieldLocator=By.xpath("//th[contains(text(),'Payment Status')]");
	public final By actionsFieldLocator=By.xpath("//th[contains(text(),'Actions')]");
	
	
	
	SoftAssert softAssert = new SoftAssert();

	public void userNavigatesToFTC()  throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(ftcRadShipmentLocator));
			getDriver().findElement(ftcRadShipmentLocator).click();
			Thread.sleep(3000);
		} catch (Exception e) {
		getDriver().navigate().refresh();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(flmLocator));
		getDriver().findElement(flmLocator).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(ftcLocator));
		getDriver().findElement(ftcLocator).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(ftcRadShipmentLocator));
		getDriver().findElement(ftcRadShipmentLocator).click();
		}
		
		
	}

	public void userClicksOnTheCalculateFTCAndValidateTheClickFunctionality(String calculateFTC) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.elementToBeClickable(calculateFTCLocator));
		Thread.sleep(3000);
		getDriver().findElement(calculateFTCLocator).click();
		softAssert.assertEquals(getDriver().findElement(ftcMainPageLocator).getText().trim(), calculateFTC,"Calculate FTC");
		softAssert.assertAll();
	}

	public void userClicksOnTheSendPaymentLinkButtonAndValidateTheSendButtonFunctionality() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		WebElement element1 = getDriver().findElement(By.xpath("//button[contains(text(),'Send payment link')]"));
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		Thread.sleep(3000);
		executor.executeScript("arguments[0].click();", element1);
		WebElement element3 = getDriver().findElement(By.xpath("(//button[contains(text(),'Send')])[2]"));
		JavascriptExecutor executor1 = (JavascriptExecutor)getDriver();
		Thread.sleep(3000);
		executor.executeScript("arguments[0].click();", element3);
	}

	public void userClicksOnThePrintCashMemoButtonAndValidateTheCashMemo(String cashMemoMessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.elementToBeClickable(printMemoButtonLocator));
		Thread.sleep(3000);
		getDriver().findElement(printMemoButtonLocator).click();
		Thread.sleep(3000);
		softAssert.assertEquals(getDriver().findElement(cashMemoMessageLocator).getText().trim(), cashMemoMessage,"View/Print Cash Memo");
		softAssert.assertAll();
		
	}

	public void userClicksOnCalculateFTC() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.elementToBeClickable(calculateFTCLocator));
		Thread.sleep(5000);
		getDriver().findElement(calculateFTCLocator).click();
		
	}

	public void userClicksOnThePrintCashMemoButtonAndClicksOnPrintButton() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.elementToBeClickable(printMemoButtonLocator));
		Thread.sleep(3000);
		getDriver().findElement(printMemoButtonLocator).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(printButtonLocator));

		
		
	}

	public void validateTheFields(String awbMessage, String totalNoOfMps, String originCity, String destinationCity,
			String pickupDate, String radDate, String consignorName, String consigneeName) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		softAssert.assertEquals(getDriver().findElement(awbLocator).getText().trim(), awbMessage,"AWB:");
		softAssert.assertEquals(getDriver().findElement(totalNumberOfMpsLocator).getText().trim(), totalNoOfMps,"Total number of MPS:");
		softAssert.assertEquals(getDriver().findElement(originCityHubLocator).getText().trim(), originCity,"Origin City (Hub):");
		softAssert.assertEquals(getDriver().findElement(destinationCityHubLocator).getText().trim(), destinationCity,"Destination City (Hub):");
		softAssert.assertEquals(getDriver().findElement(pickupDateLocator).getText().trim(), pickupDate,"Pick Up Date :");
		softAssert.assertEquals(getDriver().findElement(radDateLocator).getText().trim(), radDate,"RAD Date:");
		softAssert.assertEquals(getDriver().findElement(consignorNameLocator).getText().trim(), consignorName,"Consignor Name and Address");
		softAssert.assertEquals(getDriver().findElement(consigneeNameLocator).getText().trim(), consigneeName,"Consignee Name and Address");
		softAssert.assertAll();
	}

	public void userEnters(String awbNumber, String clientName, String radFromDate, String radToDate, String cashMemo) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		
		wait.until(ExpectedConditions.elementToBeClickable(searchLinkLocator));
		getDriver().findElement(searchLinkLocator).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(awbNumberLocator));
		WebElement awbNumberFilterTxt=getDriver().findElement(awbNumberLocator);
		awbNumberFilterTxt.sendKeys(awbNumber);
		awbNumberFilterTxt.sendKeys(Keys.ENTER);
		
		
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientNameLocator));
		WebElement clientNameTxt=getDriver().findElement(clientNameLocator);
		clientNameTxt.sendKeys(clientName);
		clientNameTxt.sendKeys(Keys.ENTER);
	
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(radFromDateLocator));
		WebElement radFromDateTxt=getDriver().findElement(radFromDateLocator);
		radFromDateTxt.sendKeys(radFromDate);
		radFromDateTxt.sendKeys(Keys.ENTER);
	
	
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(radToDateLocator));
		WebElement radToDateTxt=getDriver().findElement(radToDateLocator);
		radToDateTxt.sendKeys(radToDate);
		radToDateTxt.sendKeys(Keys.ENTER);
		
		getDriver().findElement(cashMemoLocator).click();
		
	}

	public void clickOnSearchAndValidateTheButtonFunctionality() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.elementToBeClickable(searchLocator));
		getDriver().findElement(searchLocator).click();
		
	}

	public void userClicksOnRecalculateButton() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.elementToBeClickable(reCalculateButtonLocator));
		getDriver().findElement(reCalculateButtonLocator).click();
		
	}

	public void clickOnConfirmFTCButtonAndValidateTheConfirmationMessage(String actionConfirmation) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.elementToBeClickable(confirmFtcButtonLocator));
		Thread.sleep(3000);
		getDriver().findElement(confirmFtcButtonLocator).click();
		
		softAssert.assertEquals(getDriver().findElement(actionConfirmationMessageLocator).getText().trim(), actionConfirmation,"Action Confirmation: Confirm FTC Amount");
		softAssert.assertAll();
		
		wait.until(ExpectedConditions.elementToBeClickable(NoButtonLocator));
		getDriver().findElement(NoButtonLocator).click();
		
	}

	public void userClicksOnAddExtraCharges(String shipmentSizedCharges, String varaiCharges, String portgagesCharges,
			String specialDeliveryCharges, String loadingUnloadingCharges, String odaCharges, String fullTruckLoad, String successmessage) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.elementToBeClickable(addExtraChargesButtonLocator));
		Thread.sleep(3000);
		getDriver().findElement(addExtraChargesButtonLocator).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(shipmentSizedChargesLocator));
		WebElement shipmentSizedChargesFilter=getDriver().findElement(shipmentSizedChargesLocator);
		shipmentSizedChargesFilter.sendKeys(shipmentSizedCharges);
		Thread.sleep(3000);
		shipmentSizedChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(varaiChargesLocator));
		WebElement varaiChargesFilter=getDriver().findElement(varaiChargesLocator);
		varaiChargesFilter.sendKeys(varaiCharges);
		Thread.sleep(3000);
		varaiChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(portgageChargesLocator));
		WebElement portgagesChargesFilter=getDriver().findElement(portgageChargesLocator);
		portgagesChargesFilter.sendKeys(portgagesCharges);
		Thread.sleep(3000);
		portgagesChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(specialChargesLocator));
		WebElement specialDeliveryChargesFilter=getDriver().findElement(specialChargesLocator);
		specialDeliveryChargesFilter.sendKeys(specialDeliveryCharges);
		Thread.sleep(3000);
		specialDeliveryChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(loadChargesLocator));
		WebElement loadingUnloadingChargesFilter=getDriver().findElement(loadChargesLocator);
		loadingUnloadingChargesFilter.sendKeys(loadingUnloadingCharges);
		Thread.sleep(3000);
		loadingUnloadingChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(odaChargesLocator));
		WebElement odaChargesFilter=getDriver().findElement(odaChargesLocator);
		odaChargesFilter.sendKeys(odaCharges);
		Thread.sleep(3000);
		odaChargesFilter.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(fullTruckLoadLocator));
		WebElement fullTruckLoadFilter=getDriver().findElement(fullTruckLoadLocator);
		fullTruckLoadFilter.sendKeys(fullTruckLoad);
		Thread.sleep(3000);
		fullTruckLoadFilter.sendKeys(Keys.ENTER);
		
		getDriver().findElement(saveButtonLocator).click();
		softAssert.assertEquals(getDriver().findElement(successfulmessagelocator).getText().trim(), successmessage,"successfull.");
		softAssert.assertAll();
		
	}

	public void userClicksOnFreightChargesTabAndValidate(String weightsMessage,String chargeCalculationMessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.elementToBeClickable(freightChargeTab));
		Thread.sleep(6000);
		getDriver().findElement(freightChargeTab).click();
		Thread.sleep(3000);
		softAssert.assertEquals(getDriver().findElement(WeightLocator).getText().trim(), weightsMessage,"Weights");
		softAssert.assertEquals(getDriver().findElement(chargeCalculationLocator).getText().trim(), chargeCalculationMessage,"Charge Calculation");
		softAssert.assertAll();
		
	}

	public void userClicksOnInsuranceChargesTabAndValidate(String applicableChargemessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.elementToBeClickable(insuranceChargesTabLocator));
		Thread.sleep(3000);
		getDriver().findElement(insuranceChargesTabLocator).click();
		Thread.sleep(3000);
		softAssert.assertEquals(getDriver().findElement(applicableChargeLocator).getText().trim(), applicableChargemessage,"Applicable charge:");
		softAssert.assertAll();
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		    js.executeScript("window.scrollBy(0,250)", "");
		    Thread.sleep(2000);
	}

	public void userClicksOnAccessorialChargesTabAndValidate(String waybillChargesMessage, String dcChargesMessage,
			String ftcChargesMessage, String fuelSurchargeMessage, String deliveryAttemptChargesMessage,
			String demurrageChargesMessage, String appointmentVehicleChargesMessage, String loadingChargesMessage,
			String pickupDeliveryChargesMessage, String handlingChargesMessage, String groundFloorChargesMessage,
			String greenTaxMessage, String rtoChargesMessage, String odaChargesMessage,
			String holidayDeliveryChargesMessage) throws InterruptedException{{
				WebDriverWait wait = new WebDriverWait(getDriver(), 30);
				wait.until(ExpectedConditions.elementToBeClickable(accessorialChargesTabLocator));
				Thread.sleep(3000);
				getDriver().findElement(accessorialChargesTabLocator).click();
				Thread.sleep(3000);
				softAssert.assertEquals(getDriver().findElement(wayBillChargesLocator).getText().trim(), waybillChargesMessage,"Waybill Charges:");
				softAssert.assertEquals(getDriver().findElement(dcChargesLocator).getText().trim(), dcChargesMessage,"DC Charges:");
				softAssert.assertEquals(getDriver().findElement(ftcChargesLocator).getText().trim(), ftcChargesMessage,"FTC (To Pay) Charges:");
				softAssert.assertEquals(getDriver().findElement(fuelSurchargeLocator).getText().trim(), fuelSurchargeMessage,"Fuel Surcharge:");
				softAssert.assertEquals(getDriver().findElement(deliveryAttemptChargesLocator).getText().trim(), deliveryAttemptChargesMessage,"Delivery Attempt Charges:");
				softAssert.assertEquals(getDriver().findElement(demurrageChargesLocator).getText().trim(), demurrageChargesMessage,"Demurrage Charges:");
				softAssert.assertEquals(getDriver().findElement(appointmentDeliveryChargesLocator).getText().trim(), appointmentVehicleChargesMessage,"Appointment/ Different Vehicle Charges:");
				softAssert.assertEquals(getDriver().findElement(loadingChargesLocator).getText().trim(), loadingChargesMessage,"Loading/Unloading Charges:");
				softAssert.assertEquals(getDriver().findElement(openDeliveryChargesLocator).getText().trim(), pickupDeliveryChargesMessage,"Open pickup/delivery charges:");
				softAssert.assertEquals(getDriver().findElement(handlingChargesLocator).getText().trim(), handlingChargesMessage,"Handling Charges:");
				softAssert.assertEquals(getDriver().findElement(groundFloorChargesLocator).getText().trim(), groundFloorChargesMessage,"Delivery above ground floor charges:");
				softAssert.assertEquals(getDriver().findElement(greenTaxLocator).getText().trim(), greenTaxMessage,"Green Tax:");
				softAssert.assertEquals(getDriver().findElement(rtoChargesLocator).getText().trim(), rtoChargesMessage,"RTO Charges:");
				softAssert.assertEquals(getDriver().findElement(odaLocator).getText().trim(), odaChargesMessage,"ODA charges:");
				softAssert.assertEquals(getDriver().findElement(holidayDeliveryChargesLocator).getText().trim(), holidayDeliveryChargesMessage,"Holiday delivery charges:");
				softAssert.assertAll();
				
				
		
	}

}

	public void validateFTCEntryUI(String ftcRadShipments) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(ftcRadTextLocator));
		softAssert.assertEquals(getDriver().findElement(ftcRadTextLocator).getText().trim(), ftcRadShipments);
		softAssert.assertAll();
	}

	public void validateTheTableEntryDetails(String srNo, String awb, String mpsCo, String clientName,
			String consigneeName, String consigneeAddress, String radDate, String cashMemoGenerate,
			String paymentStatus, String actions) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(srNoFieldLocator));
		
		softAssert.assertEquals(getDriver().findElement(srNoFieldLocator).getText(), srNo,"Sr.No");
		softAssert.assertEquals(getDriver().findElement(awbFieldLocator).getText(), awb,"AWB");
		softAssert.assertEquals(getDriver().findElement(mpsCoFieldLocator).getText(), mpsCo,"MPS Co");
		softAssert.assertEquals(getDriver().findElement(clientNameFieldLocator).getText(), clientName,"Client Name");
		softAssert.assertEquals(getDriver().findElement(consigneeNameFieldLocator).getText(), consigneeName,"Consignee Name");
		softAssert.assertEquals(getDriver().findElement(consigneeAddressLocator).getText(), consigneeAddress,"Consignee Address");
		softAssert.assertEquals(getDriver().findElement(radDateFieldLocator).getText(), radDate,"RAD Date");
		softAssert.assertEquals(getDriver().findElement(cashMemoFieldLocator).getText(), cashMemoGenerate,"Cash Memo Generate");
		softAssert.assertEquals(getDriver().findElement(paymentStatusFieldLocator).getText(), paymentStatus,"Payment Status");
		softAssert.assertEquals(getDriver().findElement(actionsFieldLocator).getText(), actions,"Actions");
		softAssert.assertAll();
		
	}
}
