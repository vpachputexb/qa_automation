package com.xb.cafe.ui.pages;

import java.util.List;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;



import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.actions.Upload;

public class Dashboard extends PageObject {

	 public static final String UserValidatesTheErrorMsgAndUserClicksOnOkButton = null;
	private static final CharSequence HigherBreakLimitValue = null;
	String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	 String username;
	 String TagName1;
	 String TagComment1;
	 String Tag_MappedEnvironment;
	 String TotalRecordsText;
	 String TotalClientText;
	 String activeText;
	 String InactiveText;
	 String Activebar;
	 //String Activeclientbarchart;
	 
	//  WebDriver driver = null;
	 
	 public  final By input_UN = By.xpath("//input[@name='email']");
		public  final By input_PW = By.xpath("//input[@id='typepass']");
		public  final By btn_Login = By.xpath("//button[contains(text(),'Login')]");
		public  final By Clientdashboardmenu_locator = By.xpath("//span[contains(text(),'Client Dashboard')]");
		public  final By Clientmanagementmenu_locator = By.xpath("//span[contains(text(),'Client Management')]");
		public  final By Clientmenu_locator = By.xpath("(//span[contains(text(),'Client')])[3]");
		public  final By TotalClient_locator = By.xpath("((//*[name()='svg'])[3]//*[name()='text'])[3]");
		public  final By TotalRecords_locator = By.xpath("//div[@class='col-md-3']//b");
		public  final By activetext_locator = By.xpath("(//*[name()='svg']//*[name()='g'][2]/*[name()='g']/*[name()='text'])[1]");
        public  final By Inactivetext_locator = By.xpath("(//*[name()='svg']//*[name()='g'][2]/*[name()='g']/*[name()='text'])[2]"); 
		public  final By Inactiveclick_locator = By.xpath("(//*[text()='Inactive'])[2]");
		public  final By Active_locator = By.xpath("(//*[text()='Active'])[2]");
		public  final By Activebarchart_locator = By.xpath("((//*[name()='svg'][1]//*[name()='text'])[5]//*[name()='tspan'])[1]");
		public final  By Inactivebarchartclick_locator = By.xpath("(//*[name()='svg'][1]//*[name()='text'])[6]");
		public  final By Activebarchartclick_locator = By.xpath("(//*[name()='svg'][1]//*[name()='text'])[5]");
		public  final By Inactivebarchart_locator = By.xpath("((//*[name()='svg'][1]//*[name()='text'])[6]//*[name()='tspan'])[1]");
		
		SoftAssert softAssert = new SoftAssert();
		
		

		public  void LoginSelfServicepage(String URL) throws InterruptedException {
			getDriver().get(URL);
		}

  public void userReadsTotalClientValue() throws InterruptedException {
		Thread.sleep(8000);
		TotalClientText = getDriver()
                .findElement(TotalClient_locator)
                .getText();
        System.out.println("Total Client: >> " + TotalClientText.split(" ")[3]);
		}
 
  public void userNavigatesToClientListing() throws InterruptedException {
		Thread.sleep(1000);
      getDriver().navigate().refresh();
      WebDriverWait wait = new WebDriverWait(getDriver(), 10);
      try {
              getDriver().navigate().refresh();
              Thread.sleep(2000);
              wait.until(ExpectedConditions.elementToBeClickable(Clientmenu_locator));
              getDriver().findElement(Clientmenu_locator).click();
              Thread.sleep(2000);
      } catch (Exception e) {
              getDriver().navigate().refresh();
              Thread.sleep(2000);
              wait.until(ExpectedConditions.elementToBeClickable(Clientmanagementmenu_locator));
              getDriver().findElement(Clientmanagementmenu_locator).click();
              Thread.sleep(2000);
              wait.until(ExpectedConditions.elementToBeClickable(Clientmenu_locator));
              getDriver().findElement(Clientmenu_locator).click();
      }
	
	}

public void userReadsTotalRecordsValue() throws InterruptedException {
			
			Thread.sleep(8000);
			TotalRecordsText = getDriver().findElement(TotalRecords_locator).getText();
	        System.out.println("Total Records: >> " + TotalRecordsText.split(" ")[2]);
			
		}

public boolean userComparesTotalClientValueAndTotalRecordsValue()throws InterruptedException {
				  try{
					  softAssert.assertEquals(TotalClientText, TotalRecordsText);
				   }catch(Throwable t){
		}
				  {
				  softAssert.assertNotEquals(TotalClientText, TotalRecordsText);
				  return false;
				  }
		 }


public void userNavigatesToClientDashboardOption() throws InterruptedException {
				Thread.sleep(1000);
		        getDriver().navigate().refresh();
		        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		        {
		                getDriver().navigate().refresh();
		                Thread.sleep(2000);
		                wait.until(ExpectedConditions.elementToBeClickable(Clientdashboardmenu_locator));
		                getDriver().findElement(Clientdashboardmenu_locator).click();
		                Thread.sleep(2000);
		       }
				
			}
			public void userClicksOnActiveChart() throws InterruptedException {
				Thread.sleep(8000);
				 activeText = getDriver().findElement(activetext_locator).getText();
		        System.out.println("Active Element: >> " + activeText.split(" ")[2]);
		       WebElement Active = getDriver().findElement(Active_locator);
		        Active.click();
		        Thread.sleep(8000);
				TotalRecordsText = getDriver().findElement(TotalRecords_locator).getText();
		        System.out.println("Total Records: >> " + TotalRecordsText.split(" ")[2]);
				
			}

			public boolean userComparesTotalActiveClientValueAndTotalActiveRecordsValue() throws InterruptedException {
				 try{
					  softAssert.assertEquals(activeText, TotalRecordsText);
				   }catch(Throwable t){
		}
				  {
				  softAssert.assertNotEquals(activeText, TotalRecordsText);
				  return false;
				  }
		 }

			public void userClicksOnInactiveChart() throws InterruptedException {
				Thread.sleep(8000);
			InactiveText = getDriver().findElement(Inactivetext_locator).getText();
		        System.out.println("InActive Element: >> " + InactiveText.split(" ")[2]);
		        WebElement InActive = getDriver().findElement(Inactiveclick_locator);
		        InActive.click();
		        Thread.sleep(8000);
				TotalRecordsText = getDriver().findElement(TotalRecords_locator).getText();
		        System.out.println("Total Records: >> " + TotalRecordsText.split(" ")[2]);
				
			}
			

			public boolean userComparesTotalInactiveClientValueAndTotalInactiveRecordsValue() throws InterruptedException {
				 try{
					  softAssert.assertEquals(InactiveText, TotalRecordsText);
				   }catch(Throwable t){
		}
				  {
				  softAssert.assertNotEquals(InactiveText, TotalRecordsText);
				  return false;
				  }
		 }

			public void userClicksOnActiveBarChart() throws InterruptedException {
				
				Thread.sleep(8000);
				String Activeclientbarchart = $(Activebarchart_locator).getText();
				 System.out.println("Active Clients: >> " +Activeclientbarchart);
			
		         $(Activebarchartclick_locator).click();
		         Thread.sleep(8000);
					TotalRecordsText = getDriver().findElement(TotalRecords_locator).getText();
			        System.out.println("Total Records: >> " + TotalRecordsText.split(" ")[2]);
			        
			        }
				
			public void userClicksOnInactiveBarChart() throws InterruptedException {
				Thread.sleep(8000);
				String Inactiveclientbarchart = $(Inactivebarchart_locator).getText();
				 System.out.println("Inactive Clients: >> " +Inactiveclientbarchart);
			
		         $(Inactivebarchartclick_locator).click();
		         Thread.sleep(8000);
					TotalRecordsText = getDriver().findElement(TotalRecords_locator).getText();
			        System.out.println("Total Records: >> " + TotalRecordsText.split(" ")[2]);
					
				}
				
}
				
			
			



		
		
		