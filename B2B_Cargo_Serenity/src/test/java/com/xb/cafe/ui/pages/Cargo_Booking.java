package com.xb.cafe.ui.pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.xb.cafe.ui.utils.Dateutils;

import io.cucumber.datatable.DataTable;
import net.serenitybdd.core.pages.PageObject;

public class Cargo_Booking extends PageObject{
	
	public  final By cargoBookingBttn = By.xpath("//span[contains(text(),'Cargo Booking')]");
	public  final By cargoBookingArrow= By.xpath("//div[@class='vsm-arrow open-arrow']");
	public  final By bookingBttn = By.xpath("//span[text()=\"Booking\"]");
	public  final By createBookingBttn=By.xpath("//a[@title='Create Booking']");
	
	public  final By clientTxtinput=By.xpath("//input[@class='vue-treeselect__input']");
	public  final By pickupPincodeinput=By.xpath("//input[@id='PickupPincode']");
	public  final By dropPincodeinput=By.xpath("//input[@id='DestinationPincode']");
	public  final By servicibilty=By.xpath("//label[contains(text(),'Serviceability')]");
	public  final By pickupPincodeDisplay=By.xpath("//label[contains(text(),'PickUp PinCode')]");
	public  final By servicibiltyBttn=By.xpath("//button[@title='Check Serviceability']");
	public  final By basicSavebttn=By.xpath("//button[@title='Save']");
	public  final By surfaceRouteModeLocator=By.xpath("//label[contains(text(),'Surface')]");
	public  final By airRouteModeLocator=By.xpath("//label[contains(text(),'Air')]");
	public  final By creditBookingModeLocator=By.xpath("//label[contains(text(),'Credit')]");
	public  final By ftcBookingModeLocator=By.xpath("//label[contains(text(),'FTC')]");
	
	public  final By locationNameLocator=By.xpath("//input[@class='vue-treeselect__input']");
	public  final By locationNameNewLocationAdditionLocator=By.xpath("//input[@id='LocationName']");
	public  final By mobileLocator=By.xpath("//input[@id='Mobile']");
	public  final By telephoneLocator=By.xpath("//input[@id='Telephone']");
	public  final By emailLocator=By.xpath("//input[@id='Email']");
	public  final By addressLocator=By.xpath("//textarea[@id='Address']");
	public  final By gstLocator=By.xpath("//input[@id='GstNo']");
	public  final By pickupSavebttn=By.xpath("(//button[@title='Save And Next'])[1]");
	public  final By addNewLocationBttn=By.xpath("//button[@title='Add New Location']");
	
	public  final By noOfInvoicesLocator= By.xpath("//input[@id='NumberOfInvoice']");
	public  final By totalShipmentDeclaredLocator= By.xpath("//input[@id='TotalShipmentValue']");
	public  final By numberOfMpsLocator=By.xpath("//input[@id='NumberOfMPS']");
	public  final By totalShipmentDeclaredWeightLocator=By.xpath("//input[@id='TotalShipmentWeight']");
	public  final By lengthLocator=By.xpath("//input[@id='length']");
	public  final By breadthLocator=By.xpath("//input[@id='breadth']");
	public  final By heightLocator=By.xpath("//input[@id='height']");
	public  final By noOfBoxesLocator=By.xpath("//input[@id='noofboxes0']");
	public  final By shipmentSavebttn=By.xpath("(//button[@title='Save And Next'])[2]");
	
	public  final By lengthLocator2=By.xpath("(//input[@id='length'])[2]");
	public  final By breadthLocator2=By.xpath("(//input[@id='breadth'])[2]");
	public  final By heightLocator2=By.xpath("(//input[@id='height'])[2]");
	public  final By noOfBoxesLocator2=By.xpath("(//input[@id='noofboxes1'])");
	public  final By plusSignLocator=By.xpath("//i[@title=\"Add New\"]");
	
	public  final By errorMismatchBoxesMPS=By.xpath("//div[@id='swal2-content']");
	public  final By errorMismatchOKBttn=By.xpath("(//button[contains(text(),'OK')])[4]");
	
	public  final By awbGeneratedBttn=By.xpath("(//button[contains(text(),'OK')])[5]");
	public  final By awbGeneratedTxt=By.xpath("//div[contains(text(),'AWB Generated')]");
	public  final By Btn_OK = By.xpath("(//button[contains(text(),'OK')])[5]");
	public  final By errorMessageBlankPincode=By.xpath("//div[contains(text(),'Please enter Pickup & Destination Pincodes')]");
	public  final By Btn_OK_blankPinCode = By.xpath("(//button[contains(text(),'OK')])[4]");
	
	public  final By pickupPincodeErrorMessage=By.xpath("//span[contains(text(),'The PickUp PinCode field is required')]");
	public  final By dropPincodeErrorMessage=By.xpath("//span[contains(text(),'The Destination PinCode field is required')]");
	
	public  final By pickupNonServicableLocator=By.xpath("(//span[contains(text(),'Non Serviceable')])[1]");
	public  final By dropNonServicableLocator=By.xpath("(//span[contains(text(),'Non Serviceable')])[2]");
	
	public  final By originHubLocator=By.xpath("//input[@id='OriginHub']");
	public  final By destHubLocator=By.xpath("//input[@id='DestinationHub']");
	
	public  final By serviceabilityLabelLocator=By.xpath("//label[contains(text(),'Serviceability')]");
	public  final By resetButtonLocator=By.xpath("//button[@title='Reset']");
	
	public  final By pickpupLocationLocator=By.xpath("//input[@id='PinCode']");
	public  final By cityLocator=By.xpath("//input[@id='City']");
	public  final By stateLocator=By.xpath("//input[@id='State']");
	
	public  final By errorLocationNameLocator=By.xpath("//span[contains(text(),'The Location Name field is required')]");
	public  final By errorMobileLocator=By.xpath("//span[contains(text(),'The Mobile field is required')]");
	public  final By errorEmailLocator=By.xpath("//span[contains(text(),'The Email field is required')]");
	public  final By errorAddressLocator=By.xpath("//span[contains(text(),'The Email field is required')]");
	public  final By errorGSTNOLocator=By.xpath("//span[contains(text(),'The GstNo field is required')]");
	
	public  final By errorEmailFormatLocator=By.xpath("//span[contains(text(),'The Email field must be a valid email')]");
	public  final By errorMobileFormatLocator=By.xpath("//span[contains(text(),'The Mobile field format is invalid')]");
	public  final By errorGSTnoFormatLocator=By.xpath("//span[contains(text(),'The GstNo field format is invalid')]");
	public  final By errorTelephoneFormatLocator=By.xpath("//span[contains(text(),'The Telephone field must be at least 6 characters')]");
	
	public  final By errorNoOfInvoicesLocator=By.xpath("//span[contains(text(),'The Number Of Invoice field is required')]");
	public  final By errorTotalShipmentDeclaredValueLocator=By.xpath("//span[contains(text(),'The Total Shipment Declared Value field is required')]");
	public  final By errorNumberOfMPsLocator=By.xpath("//span[contains(text(),'The Number Of MPS field is required')]");
	public  final By errorTotalShipmentDEclaredWeightLocator=By.xpath("//span[contains(text(),'The Total Shipment Declared Weight field is required ')]");
	public  final By errorLengthLocator=By.xpath("//span[contains(text(),'Please enter length')]");
	public  final By errorBreadthLocator=By.xpath("//span[contains(text(),'Please enter breadth')]");
	public  final By errorHeightLocator=By.xpath("//span[contains(text(),'Please enter height')]");
	public  final By errorNoOfBoxesLocator=By.xpath("//span[contains(text(),'Please enter no. of boxes')]");
	
	public  final By dateLocator=By.xpath("//input[@name='RequestedPickupDate']");
	
	public  final By bookingEntryLocator=By.xpath("//h4[contains(text(),'Booking Entry List')]");
	
	public  final By srNolocator=By.xpath("//th[contains(text(),'Sr.No')]");
	public  final By awblocator=By.xpath("//th[contains(text(),'AWB')]");
	public  final By mpslocator=By.xpath("//th[contains(text(),'#MPS')]");
	public  final By clientRefNolocator=By.xpath("//th[contains(text(),'Client Ref. No')]");
	public  final By tlTypelocator=By.xpath("//th[contains(text(),'TL Type')]");
	public  final By routeModelocator=By.xpath("//th[contains(text(),'Route Mode')]");
	public  final By bookingModelocator=By.xpath("//th[contains(text(),'Booking Mode')]");
	public  final By clientNamelocator=By.xpath("//th[contains(text(),'Client Name')]");
	public  final By originlocator=By.xpath("//th[contains(text(),'Origin')]");
	public  final By destinationlocator=By.xpath("//th[contains(text(),'Destination')]");
	public  final By invoiceValuelocator=By.xpath("//th[contains(text(),'Invoice Value (INR)')]");
	public  final By phyWtlocator=By.xpath("//th[contains(text(),'Phy Wt(KG)')]");
	public  final By volWtlocator=By.xpath("//th[contains(text(),'Vol Wt(KG)')]");
	public  final By pickupDatelocator=By.xpath("//th[contains(text(),'PickUp Date')]");
	public  final By bookingDatelocator=By.xpath("//th[contains(text(),'Booking Date')]");
	public  final By bookingMethodlocator=By.xpath("//th[contains(text(),'Booking Method')]");
	public  final By bookingStatuslocator=By.xpath("//th[contains(text(),'Booking Status')]");
	public  final By bookingTypelocator=By.xpath("//th[contains(text(),'Booking Type')]");
	public  final By actionlocator=By.xpath("//th[contains(text(),'Action')]");
	
	public  final By totalRecordsLocator=By.xpath("//b[contains(text(),'Total Records: 11383')]");
	public  final By navigate2ndPageLocator=By.xpath("//button[contains(text(),'2')]");
	public  final By navigateLastPageLocator=By.xpath("//button[contains(text(),'Last')]");
	
	public  final By viewBttnLocator=By.xpath("(//span[@title='View'])[1]");
	public  final By viewServiceabiltyLocator=By.xpath("//label[contains(text(),'Serviceability')]");
	public  final By viewPickupLocator=By.xpath("//label[contains(text(),'Pickup Location')]");
	public  final By viewShimentDetailsLocator=By.xpath("//label[contains(text(),'Shipment Details')]");
	
	public  final By createBookingLocator=By.xpath("//label[contains(text(),'Create Booking')]");
	public  final By basicInformationLocator=By.xpath("//button[@title='Basic Information']");
	public  final By deatiledInformationLocator=By.xpath("//button[@title='Detailed Information']");
	public  final By editButtonLocator=By.xpath("(//span[@title='Edit'])[1]");
	
	public  final By filterLocator=By.xpath("//i[@title='Search Filter']");
	public  final By resetFilterLocator=By.xpath("//button[@title='Reset']");
	
	public  final By companyNameFilterLocator=By.xpath("(//input[@class='vue-treeselect__input'])[1]");
	public  final By bookingStatusFilterLocator=By.xpath("(//input[@class='vue-treeselect__input'])[2]");
	public  final By truckLoadTypeFilterLocator=By.xpath("(//input[@class='vue-treeselect__input'])[3]");
	public  final By routeModeFilterLocator=By.xpath("(//input[@class='vue-treeselect__input'])[4]");
	public  final By bookingModeFilterLocator=By.xpath("(//input[@class='vue-treeselect__input'])[5]");
	public  final By bookingMethodFilterLocator=By.xpath("(//input[@class='vue-treeselect__input'])[6]");
	public  final By originFilterLocator=By.xpath("//div[contains(text(),'Select Origin')]");
	public  final By destinationFilterLocator=By.xpath("//div[contains(text(),'Select Destination')]");
	public  final By aWBNoFilterLocator=By.xpath("//input[@id='awbNo']");
	public  final By searchBttnFilterLocator=By.xpath("//button[@title='Search']");
	
	public  final By expandAllBttnLocator=By.xpath("//button[@title='Expand All']");
	public  final By routeModeViewLocator=By.xpath("//th[contains(text(),'Route Mode :')]/following-sibling::td[1]");
	public  final By pickupPincodeViewLocator=By.xpath("//th[contains(text(),'PickUp PinCode :')]/following-sibling::td[1]");
	public  final By dropPincodeViewLocator=By.xpath("//th[contains(text(),'Destination PinCode :')]/following-sibling::td[1]");
	public  final By bookingModeViewLocator=By.xpath("//th[contains(text(),'Booking Mode :')]/following-sibling::td[1]");
	public  final By locationNameViewLocator=By.xpath("//th[contains(text(),'Location Name :')]/following-sibling::td[1]");
	public  final By mobileViewLocator=By.xpath("//th[contains(text(),'Mobile :')]/following-sibling::td[1]");
	public  final By emailViewLocator=By.xpath("//th[contains(text(),'Email :')]/following-sibling::td[1]");
	public  final By addressViewLocator=By.xpath("//th[contains(text(),'Address :')]/following-sibling::td[1]");
	public  final By gSTNOViewLocator=By.xpath("//th[contains(text(),'GST No :')]/following-sibling::td[1]");
	public  final By noOfInvoicesViewLocator=By.xpath("//th[contains(text(),'Number of invoices :')]/following-sibling::td[1]");
	public  final By totalShipmentValueViewLocator=By.xpath("//th[contains(text(),'Total Shipment Declared Value (INR) :')]/following-sibling::td[1]");
	public  final By noOfMPSViewLocator=By.xpath("//th[contains(text(),'Number of MPS :')]/following-sibling::td[1]");
	public  final By totalShipmentWeightViewLocator=By.xpath("//th[contains(text(),'Total Shipment Declared Weight (kg) :')]/following-sibling::td[1]");
	public  final By pickuplocationTabLocator=By.xpath("//label[contains(text(),'Pickup Location')]");
	public  final By lengthValueViewLocator=By.xpath("//table[@class=\"table-bordered table-hover tbl datatables\"]/tbody/tr/td[2]");
	public  final By breadthValueViewLocator=By.xpath("//table[@class=\"table-bordered table-hover tbl datatables\"]/tbody/tr/td[3]");
	public  final By heightValueViewLocator=By.xpath("//table[@class=\"table-bordered table-hover tbl datatables\"]/tbody/tr/td[4]");
	public  final By noOfBoxesValueViewLocator=By.xpath("//table[@class=\"table-bordered table-hover tbl datatables\"]/tbody/tr/td[6]");
	
	public  final By refreshIconLocator=By.xpath("//i[@title='Refresh']");
	
	public  final By aWBNumberGeneratedLocator=By.xpath("//input[@id='awbNo']");
	public  final By convertInchesToCentimeterLink=By.xpath("(//span[contains(text(),'Convert Inches to Centimeter')])[1]");
	public  final By inchesInputFieldLocator=By.xpath("//input[@id='inches']");
	public  final By centimeterInputFieldLocator=By.xpath("//input[@id='cm']");
	public  final By closeBttnLocator=By.xpath("//button[@title='Close']");
	
	public  final By downloadXlsxButton=By.xpath("//button[contains(text(),'Download (XLSX)')]");
	
	public  final By detailedinformationBttn=By.xpath("//button[@title='Detailed Information']");
	public  final By consigneedetailsBttn=By.xpath("(//label[@class='tab-label'])[1]");
	public  final By invoiceandGSTdetailstab=By.xpath("(//label[@class='tab-label'])[3]");
	public  final By valueaddedServicestab=By.xpath("(//label[@class='tab-label'])[4]");
	
	public  final By addnewconsigneeBttn=By.xpath("//button[@title='Add New Consignee']");
	public  final By consigneecodeinput=By.xpath("//input[@id='ConsigneeCode']");
	public  final By companynamelocator=By.xpath("//input[@name='CompanyName']");
	public  final By mobiletxt2=By.xpath("//input[@id='Mobile']");
	public  final By emailtxt2=By.xpath("//input[@id='Email']");
	public  final By gstnotxt2=By.xpath("//input[@id='CSTTINNo']");
	
	public  final By addresstxt2=By.xpath("//textarea[@id='Address']");
	public  final By saveandnextbttn=By.xpath("(//button[@title='Save And Next'])[1]");
	public  final By savebttn=By.xpath("//button[@title='Save'][1]");
	public  final By saveandnxtbttn3=By.xpath("(//button[@title='Save And Next'])[3]");
	
	public  final By consigneepoclocator=By.xpath("//input[@id='ATTN']");

//	public  final By errorconsigneecodelocator=By.xpath("//span[contains(text(),'The Consignee Code field is required')]");
//	public  final By errorcompanynamelocator=By.xpath("//span[contains(text(),'The Company Name field is required')]");
//	public  final By errormobiletxtlocator=By.xpath("//span[contains(text(),'The Mobile field format is invalid')]");
//	public  final By errorconsigneepoclocator=By.xpath("//span[contains(text(),'The Consignee POC field is required')]");
//	public  final By erroremailtxtlocator=By.xpath("//span[contains(text(),'The Email field is required')]");
//	public  final By errorgstnolocator=By.xpath("//span[contains(text(),'Please enter valid format of GST Number')]");
//	public  final By erroraddresslocator=By.xpath("//span[contains(text(),'The Address field is required')]");
	public  final By aWBNumberGeneratedLocator1=By.xpath("//input[@id='awbNo']");
	public  final By errorproductcategorieslocator=By.xpath("//span[contains(text(),'The Product Categories field is required')]");
	public  final By hsninputlocator=By.xpath("//input[@id='HsnCode0']");

	public  final By errorinvoicenumberlocator1=By.xpath("//span[contains(text(),'Invoice Number is required')]");
	public  final By errorinvoicevaluelocator1=By.xpath("//span[contains(text(),'Invoice Value is required')]");

	public  final By pincodeLabelConsigneeLocator=By.xpath("//label[contains(text(),'PinCode')]");
	
	public  final By productcategoriesinput=By.xpath("(//div[@class='vue-treeselect__multi-value'])[1]");
	public  final By referencenoinputtxt=By.xpath("//input[@id='ReferenceNo']");
	public  final By clientreferencenumber=By.xpath("//input[@id='clientReferenceNo']");
	
	public  final By invoicenumberinput=By.cssSelector("input[id=InvoiceNumber0]");
	public  final By invoicevalueinput=By.cssSelector("input[id=InvoiceValue0]");
	public  final By hsnCheckbox=By.cssSelector("input[id=IsEwbnExempted0]");
	public  final By ewbnexemptedlocator=By.xpath("//input[@id='EWBNumber0']");

	public  final By ewbvalidatebttn=By.xpath("//button[@title='Validate EWB']");
	
	public  final By invoiceGeneratedTxt=By.xpath("//div[contains(text(),'Total Invoice Value should be equal to Total Shipment Value')]");
	public  final By RequestedDeliveryDate=By.xpath("//input[@id='RequestedDeliveryDate']");
	public  final By requestedFromTime=By.xpath("//input[@id='RequestedFromTime']");
	public  final By requestedToTime=By.xpath("//input[@id='RequestedToTime']");
	public  final By BtnoK = By.xpath("(//button[contains(text(),'OK')])[5]");
	public  final By informationbttn=By.xpath("//i[@class='fa fa-info-circle fa-2x'][2]");
	public  final By closebtn=By.xpath("//button[@title='Close'][1]");
	public  final By expandall=By.xpath("(//button[contains(text(),'Expand All')])[1]");
	public  final By collapseall=By.xpath("(//button[contains(text(),'Collapse All')])[1]");
	public  final By informationerrormessagelocator=By.xpath("//li[contains(text(),'EWB Number is not valid')]");
	public  final By Holddays1=By.xpath("//input[@id='HoldDays']");
	public  final By errorinvoicenumberlocator=By.xpath("//span[contains(text(),'Invoice Number is required')]");
	public  final By errorinvoicevaluelocator=By.xpath("//span[contains(text(),'Invoice Value is required')]");
	public  final By errorebnlocator=By.xpath("//span[contains(text(),'EWB Number is required and must be min 12 digits')]");
	public  final By ewberrormessage=By.xpath("//div[contains(text(),'Please enter EWB Number')]");

	
	public  final By invoicenumberinput1=By.cssSelector("input[id=InvoiceNumber1]");
	public  final By invoicevalueinput1=By.cssSelector("input[id=InvoiceValue1]");
	public  final By ewbvalidatebttn1=By.xpath("(//button[contains(text(),'Validate EWB')])[2]");
	public  final By ewbnexemptedlocator1=By.xpath("//input[@id='EWBNumber1']");
	public  final By invoiceerrorvalidation=By.xpath("//div[contains(text(),'Invoice Details are not valid')]");
	public final By okButtonODA=By.xpath("(//button[contains(text(),'OK')])[4]");
	public  final By okButtonLocator=By.xpath("(//button[contains(text(),'OK')])[5]");
	public  final By saveandnextbttn2=By.xpath("(//button[@title='Save And Next'])[2]");
	public  final By datepickerlocator=By.xpath("//input[@placeholder='Select Date']");
	public  String mobileDB;
	public  String emailDB;
	public  String gstDB;
	public  String addressDB;
	public String awbBookingGenerated;
	public  Boolean flag=false;

	 SoftAssert softAssert = new SoftAssert();

	
	
	public  void navigateToCargoBookingOption() throws InterruptedException{
		Thread.sleep(3000);
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		
		
		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
//			wait.until(ExpectedConditions.visibilityOfElementLocated(bookingBttn));
//			getDriver().findElement(bookingBttn).click();
			$(bookingBttn).waitUntilVisible().waitUntilClickable().click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
//			getDriver().findElement(cargoBookingBttn).click();
			Thread.sleep(3000);
			$(cargoBookingBttn).waitUntilVisible().waitUntilClickable().click();
//			wait.until(ExpectedConditions.visibilityOfElementLocated(bookingBttn));
//			getDriver().findElement(bookingBttn).click();
			$(bookingBttn).waitUntilVisible().waitUntilClickable().click();
			Thread.sleep(3000);
		}
	}
	
	public  void navigateToCreateBookingOption() throws InterruptedException {
		Thread.sleep(5000);
		getDriver().findElement(createBookingBttn).click();
		
	}
	
	public  void enterClientNameAndBasicInformation(String clientName, String route_mode, String booking_mode, String pickupPincode, String dropPincode,String serviceability) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientTxtinput));
//		WebElement clientTxtbox=getDriver().findElement(clientTxtinput);
//		clientTxtbox.sendKeys(clientName);
//		Thread.sleep(1000);
//		clientTxtbox.sendKeys(Keys.ENTER);
//		Thread.sleep(5000);
		getDriver().findElement(clientTxtinput).click();
		Thread.sleep(4000);
		List<WebElement> listClientName=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		
		for(int i=0;i<=listClientName.size()-1;i++) {
			System.out.println(listClientName.get(i).getText());
			if(listClientName.get(i).getText().equalsIgnoreCase(clientName)) {
				listClientName.get(i).click();
				break;
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[contains(text(),'test')]".trim().replace("test", route_mode))));
		Thread.sleep(6000);
		getDriver().findElement(By.xpath("//label[contains(text(),'test')]".trim().replace("test", route_mode))).click();
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("//label[contains(text(),'test')]".trim().replace("test", booking_mode))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(pickupPincodeinput));
		getDriver().findElement(pickupPincodeinput).sendKeys(pickupPincode);
		wait.until(ExpectedConditions.visibilityOfElementLocated(dropPincodeinput));
		getDriver().findElement(dropPincodeinput).sendKeys(dropPincode);
		wait.until(ExpectedConditions.visibilityOfElementLocated(servicibiltyBttn));
		getDriver().findElement(servicibiltyBttn).click();
		
		try {
			wait.until(ExpectedConditions.elementToBeClickable(basicSavebttn));
			softAssert.assertEquals((getDriver().findElement(By.xpath("(//span[contains(text(),'test')])[1]".trim().replace("test", serviceability))).getText()),serviceability,"Serviceability message not shown");
			softAssert.assertEquals((getDriver().findElement(By.xpath("(//span[contains(text(),'test')])[2]".trim().replace("test", serviceability))).getText()),serviceability,"Serviceability message not shown");
			softAssert.assertAll();
			if(getDriver().findElement(basicSavebttn).isEnabled()) {;
			wait.until(ExpectedConditions.elementToBeClickable(basicSavebttn));
			getDriver().findElement(basicSavebttn).click();
		}
		}
		catch(Exception e){
		if(getDriver().findElement(basicSavebttn).isEnabled()) {;
			wait.until(ExpectedConditions.elementToBeClickable(basicSavebttn));
			softAssert.assertEquals((getDriver().findElement(By.xpath("(//span[contains(text(),'test')])[1]".trim().replace("test", serviceability))).getText()),serviceability,"Serviceability message not shown");
			softAssert.assertEquals((getDriver().findElement(By.xpath("(//span[contains(text(),'test')])[2]".trim().replace("test", serviceability))).getText()),serviceability,"Serviceability message not shown");
			
			softAssert.assertAll();
			getDriver().findElement(basicSavebttn).click();
		}
		}
	}
	
public  void enterClientNameAndBasicInformationForODAValidation(String clientName, String route_mode, String booking_mode, String pickupPincode, String dropPincode,String serviceability,String message) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientTxtinput));
//		WebElement clientTxtbox=getDriver().findElement(clientTxtinput);
//		clientTxtbox.sendKeys(clientName);
//		Thread.sleep(1000);
//		clientTxtbox.sendKeys(Keys.ENTER);
//		Thread.sleep(5000);
		getDriver().findElement(clientTxtinput).click();
		Thread.sleep(4000);
		List<WebElement> listClientName=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		
		for(int i=0;i<=listClientName.size()-1;i++) {
			System.out.println(listClientName.get(i).getText());
			if(listClientName.get(i).getText().equalsIgnoreCase(clientName)) {
				listClientName.get(i).click();
				break;
			}
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[contains(text(),'test')]".trim().replace("test", route_mode))));
		Thread.sleep(5000);
		getDriver().findElement(By.xpath("//label[contains(text(),'test')]".trim().replace("test", route_mode))).click();
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("//label[contains(text(),'test')]".trim().replace("test", booking_mode))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(pickupPincodeinput));
		getDriver().findElement(pickupPincodeinput).sendKeys(pickupPincode);
		wait.until(ExpectedConditions.visibilityOfElementLocated(dropPincodeinput));
		getDriver().findElement(dropPincodeinput).sendKeys(dropPincode);
		wait.until(ExpectedConditions.visibilityOfElementLocated(servicibiltyBttn));
		getDriver().findElement(servicibiltyBttn).click();
		Thread.sleep(5000);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'test')]".trim().replace("test", message))));
//		softAssert.assertEquals((getDriver().findElement(By.xpath("//div[contains(text(),'test')]".trim().replace("test", message))).getText()),message,"ODA message not shown");
		wait.until(ExpectedConditions.visibilityOfElementLocated(okButtonODA));
		getDriver().findElement(okButtonODA).click();
		softAssert.assertAll();
		try {
			wait.until(ExpectedConditions.elementToBeClickable(basicSavebttn));
			softAssert.assertEquals((getDriver().findElement(By.xpath("(//span[contains(text(),'test')])[1]".trim().replace("test", serviceability))).getText()),serviceability,"Serviceability message not shown");
			softAssert.assertEquals((getDriver().findElement(By.xpath("(//span[contains(text(),'test')])[2]".trim().replace("test", serviceability))).getText()),serviceability,"Serviceability message not shown");
			softAssert.assertAll();
			if(getDriver().findElement(basicSavebttn).isEnabled()) {;
			wait.until(ExpectedConditions.elementToBeClickable(basicSavebttn));
			getDriver().findElement(basicSavebttn).click();
		}
		}
		catch(Exception e){
		if(getDriver().findElement(basicSavebttn).isEnabled()) {;
			wait.until(ExpectedConditions.elementToBeClickable(basicSavebttn));
			softAssert.assertEquals((getDriver().findElement(By.xpath("(//span[contains(text(),'test')])[1]".trim().replace("test", serviceability))).getText()),serviceability,"Serviceability message not shown");
			softAssert.assertEquals((getDriver().findElement(By.xpath("(//span[contains(text(),'test')])[2]".trim().replace("test", serviceability))).getText()),serviceability,"Serviceability message not shown");
			
			softAssert.assertAll();
			getDriver().findElement(basicSavebttn).click();
		}
		}
	}
	public  void userEntersPickupLocationDetails(String locationName, String mobile, String email, String address, String gstNo,String telephone) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locationNameLocator));
//		WebElement locationTxtbox=getDriver().findElement(locationNameLocator);
//		locationTxtbox.sendKeys(locationName);
//		Thread.sleep(1000);
//		locationTxtbox.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		getDriver().findElement(locationNameLocator).click();
		Thread.sleep(4000);
		List<WebElement> listLocationName=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		
		for(int i=0;i<=listLocationName.size()-1;i++) {
			System.out.println(listLocationName.get(i).getText());
			if(listLocationName.get(i).getText().equalsIgnoreCase(locationName)) {
				listLocationName.get(i).click();
				break;
			}
		}
		if(getDriver().findElement(mobileLocator).getAttribute("value").isEmpty())
		{
		
		wait.until(ExpectedConditions.elementToBeClickable(addNewLocationBttn));
			getDriver().findElement(addNewLocationBttn).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(locationNameNewLocationAdditionLocator));
			getDriver().findElement(locationNameNewLocationAdditionLocator).sendKeys(locationName);
			wait.until(ExpectedConditions.visibilityOfElementLocated(mobileLocator));
			getDriver().findElement(mobileLocator).sendKeys(mobile);
			wait.until(ExpectedConditions.visibilityOfElementLocated(telephoneLocator));
			getDriver().findElement(telephoneLocator).sendKeys(telephone);
			wait.until(ExpectedConditions.visibilityOfElementLocated(emailLocator));
			getDriver().findElement(emailLocator).sendKeys(email);
			wait.until(ExpectedConditions.visibilityOfElementLocated(addressLocator));
			getDriver().findElement(addressLocator).sendKeys(address);
			wait.until(ExpectedConditions.visibilityOfElementLocated(gstLocator));
			getDriver().findElement(gstLocator).sendKeys(gstNo);
			wait.until(ExpectedConditions.elementToBeClickable(pickupSavebttn));
			Thread.sleep(4000);
			getDriver().findElement(pickupSavebttn).click();
			Thread.sleep(2000);
		}
		else {
		flag=true;
		mobileDB=getDriver().findElement(mobileLocator).getAttribute("value");
		wait.until(ExpectedConditions.visibilityOfElementLocated(emailLocator));
		emailDB=getDriver().findElement(emailLocator).getAttribute("value");
		wait.until(ExpectedConditions.visibilityOfElementLocated(addressLocator));
		addressDB= getDriver().findElement(addressLocator).getAttribute("value");
		wait.until(ExpectedConditions.visibilityOfElementLocated(gstLocator));
		gstDB= getDriver().findElement(gstLocator).getAttribute("value");
		wait.until(ExpectedConditions.elementToBeClickable(pickupSavebttn));
		Thread.sleep(4000);
		getDriver().findElement(pickupSavebttn).click();
		Thread.sleep(2000);
		}
		}
	
	public  void userEntersShipmentDetails(String numberOfInvoices, String totalShipmentDeclaredValue, String numberMPS, String totalShipmentDeclaredWeight, String lenghth, String breadth, String height, String noOfBoxes) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(noOfInvoicesLocator));
		getDriver().findElement(noOfInvoicesLocator).sendKeys(numberOfInvoices);
		wait.until(ExpectedConditions.visibilityOfElementLocated(totalShipmentDeclaredLocator));
		getDriver().findElement(totalShipmentDeclaredLocator).sendKeys(totalShipmentDeclaredValue);
		wait.until(ExpectedConditions.visibilityOfElementLocated(numberOfMpsLocator));
		getDriver().findElement(numberOfMpsLocator).sendKeys(numberMPS);
		wait.until(ExpectedConditions.visibilityOfElementLocated(totalShipmentDeclaredWeightLocator));
		getDriver().findElement(totalShipmentDeclaredWeightLocator).sendKeys(totalShipmentDeclaredWeight);
		wait.until(ExpectedConditions.visibilityOfElementLocated(lengthLocator));
		getDriver().findElement(lengthLocator).sendKeys(lenghth);
		wait.until(ExpectedConditions.visibilityOfElementLocated(breadthLocator));
		getDriver().findElement(breadthLocator).sendKeys(breadth);
		wait.until(ExpectedConditions.visibilityOfElementLocated(heightLocator));
		getDriver().findElement(heightLocator).sendKeys(height);
		wait.until(ExpectedConditions.visibilityOfElementLocated(noOfBoxesLocator));
		getDriver().findElement(noOfBoxesLocator).sendKeys(noOfBoxes);
	}
	
	public  void userEntersAdditionalBoxDetails(String length1, String breadth1, String height1, String noOfBoxes1) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(plusSignLocator));
		getDriver().findElement(plusSignLocator).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(lengthLocator2));
		getDriver().findElement(lengthLocator2).sendKeys(length1);
		wait.until(ExpectedConditions.visibilityOfElementLocated(breadthLocator2));
		getDriver().findElement(breadthLocator2).sendKeys(breadth1);
		wait.until(ExpectedConditions.visibilityOfElementLocated(heightLocator2));
		getDriver().findElement(heightLocator2).sendKeys(height1);
		wait.until(ExpectedConditions.visibilityOfElementLocated(noOfBoxesLocator2));
		getDriver().findElement(noOfBoxesLocator2).sendKeys(noOfBoxes1);
	}
	
	public  void verifyAWBGeneratedAndStatus(String status) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.elementToBeClickable(shipmentSavebttn));
		getDriver().findElement(shipmentSavebttn).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(awbGeneratedTxt));
		String awbTxt=getDriver().findElement(awbGeneratedTxt).getText();
		softAssert.assertEquals(awbTxt, status);
		wait.until(ExpectedConditions.elementToBeClickable(Btn_OK));
		getDriver().findElement(Btn_OK).click();
		Thread.sleep(2000);
		String bookedTxt=getDriver().findElement(By.xpath("//input[@id='Status']")).getAttribute("value");
		softAssert.assertEquals(bookedTxt, "Booked","Booking assertion done");
		softAssert.assertAll();
		Thread.sleep(2000);
	}
	
	public void captureAWBGeneratedBooking() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(aWBNumberGeneratedLocator));
		awbBookingGenerated=getDriver().findElement(aWBNumberGeneratedLocator).getAttribute("value");
	}
	
	
	public  void verifyTheDetailsDisplayedWithAWBNumbersAndOtherDetails(String clientName, String route_mode, String booking_mode, String pickupPincode, String dropPincode,String locationName, String mobile, String email, String address, String gstNo,String numberOfInvoices, String totalShipmentDeclaredValue, String numberMPS, String totalShipmentDeclaredWeight, String lenghth, String breadth, String height, String noOfBoxes) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(aWBNumberGeneratedLocator));
		String aWBNumberGeneratedTxt=getDriver().findElement(aWBNumberGeneratedLocator).getAttribute("value");
		getDriver().navigate().refresh();
		wait.until(ExpectedConditions.visibilityOfElementLocated(filterLocator));
		getDriver().findElement(filterLocator).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(resetFilterLocator));
		getDriver().findElement(resetFilterLocator).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(aWBNoFilterLocator));
		getDriver().findElement(aWBNoFilterLocator).sendKeys(aWBNumberGeneratedTxt);
		getDriver().findElement(searchBttnFilterLocator).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(viewBttnLocator));
		Thread.sleep(1000);
		getDriver().findElement(viewBttnLocator).click();
		
		Thread.sleep(3000);
		getDriver().findElement(expandAllBttnLocator).click();
		Thread.sleep(1000);
		if(flag==true) {
			softAssert.assertEquals(getDriver().findElement(mobileViewLocator).getText(), mobileDB,"Mobile not correctly saved");
			softAssert.assertEquals(getDriver().findElement(emailViewLocator).getText(), emailDB,"Email not correctly saved");
			softAssert.assertEquals(getDriver().findElement(addressViewLocator).getText(), addressDB,"Address not correctly saved");
			softAssert.assertEquals(getDriver().findElement(gSTNOViewLocator).getText(), gstDB,"GSTNo not correctly saved");
			softAssert.assertAll();
		}
		else {
			softAssert.assertEquals(getDriver().findElement(locationNameViewLocator).getText(), locationName,"Location Name not correctly saved"); 
			softAssert.assertEquals(getDriver().findElement(mobileViewLocator).getText(), mobile,"Mobile not correctly saved");
			softAssert.assertEquals(getDriver().findElement(emailViewLocator).getText(), email,"Email not correctly saved");
			softAssert.assertEquals(getDriver().findElement(addressViewLocator).getText(), address,"Address not correctly saved");
			softAssert.assertEquals(getDriver().findElement(gSTNOViewLocator).getText(), gstNo,"GSTNo not correctly saved");
			softAssert.assertAll();
		}
		softAssert.assertEquals(getDriver().findElement(routeModeViewLocator).getText(), route_mode,"Route Mode not correctly saved");
		softAssert.assertEquals(getDriver().findElement(bookingModeViewLocator).getText(), booking_mode,"Booking Mode not correctly saved"); 
		softAssert.assertEquals(getDriver().findElement(pickupPincodeViewLocator).getText(), pickupPincode,"Pickup Pincode not correctly saved"); 
		softAssert.assertEquals(getDriver().findElement(dropPincodeViewLocator).getText(), dropPincode,"Drop Pincode not correctly saved"); 
		softAssert.assertEquals(getDriver().findElement(noOfInvoicesViewLocator).getText(), numberOfInvoices,"Number of Invoices not correctly saved");
		softAssert.assertEquals(getDriver().findElement(noOfMPSViewLocator).getText(), numberMPS,"Number Of MPS not correctly saved");
		softAssert.assertEquals(getDriver().findElement(totalShipmentValueViewLocator).getText(), totalShipmentDeclaredValue,"Total of Shipment declared Value not correctly saved");
		softAssert.assertEquals(getDriver().findElement(totalShipmentWeightViewLocator).getText(), totalShipmentDeclaredWeight,"Total of shipment declared weight not correctly saved");
		softAssert.assertEquals(getDriver().findElement(lengthValueViewLocator).getText(), lenghth,"length not correctly saved");
		softAssert.assertEquals(getDriver().findElement(breadthValueViewLocator).getText(), breadth,"breadth not correctly saved");
		softAssert.assertEquals(getDriver().findElement(heightValueViewLocator).getText(), height,"Height not correctly saved");
		softAssert.assertEquals(getDriver().findElement(noOfBoxesValueViewLocator).getText(), noOfBoxes,"No of Boxes not correctly saved");
		
		getDriver().findElement(By.xpath("//button[@title='Collapse All']")).click();
		softAssert.assertAll();
	
	}
	
	public  void userEntersClientName(String clientName) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientTxtinput));
//		WebElement clientTxtbox=getDriver().findElement(clientTxtinput);
//		clientTxtbox.sendKeys(clientName);
//		Thread.sleep(1000);
//		clientTxtbox.sendKeys(Keys.ENTER);
		getDriver().findElement(clientTxtinput).click();
		Thread.sleep(1000);
		List<WebElement> listClientTxt=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		
		for(int i=0;i<=listClientTxt.size()-1;i++) {
			System.out.println(listClientTxt.get(i).getText());
			if(listClientTxt.get(i).getText().equalsIgnoreCase(clientName)) {
				listClientTxt.get(i).click();
				break;
			}
		}
		
		
	}
	
	public  void userEntersServiceabilityInformation(String route_mode, String booking_mode, String pickupPincode, String dropPincode) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		getDriver().findElement(By.xpath("//label[contains(text(),'test')]".trim().replace("test", route_mode))).click();
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("//label[contains(text(),'test')]".trim().replace("test", booking_mode))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(pickupPincodeinput));
		getDriver().findElement(pickupPincodeinput).sendKeys(pickupPincode);
		wait.until(ExpectedConditions.visibilityOfElementLocated(dropPincodeinput));
		getDriver().findElement(dropPincodeinput).sendKeys(dropPincode);
		wait.until(ExpectedConditions.visibilityOfElementLocated(servicibiltyBttn));
		getDriver().findElement(servicibiltyBttn).click();
	}
	
	public  void userClicksOnRefreshButtonAndThenValidateErrorMessages(String errorMsgClientName, String messagePickupPincode, String messageDestinationPincode) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(basicSavebttn));
		getDriver().findElement(refreshIconLocator).click();
		Thread.sleep(3000);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", errorMsgClientName))).getText(), errorMsgClientName,"Client Name field error message incorrect");
		Thread.sleep(1000);
		getDriver().findElement(serviceabilityLabelLocator).click();
		Thread.sleep(2000);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", messagePickupPincode))).getText(), messagePickupPincode,"Pickup pincode error message incorrect");
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", messageDestinationPincode))).getText(), messageDestinationPincode,"Drop pincode error message incorrect");
		softAssert.assertAll();
	}
	public  void userClicksOnCheckServiceabiltyBttnAndValidateErrorMessage(String errorMessage) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(servicibiltyBttn));
		getDriver().findElement(servicibiltyBttn).click();
		softAssert.assertEquals(getDriver().findElement(errorMessageBlankPincode).getText(), errorMessage, "Error message not displyed correctly for blank and invalid format pincodes");
		softAssert.assertAll();
		wait.until(ExpectedConditions.visibilityOfElementLocated(Btn_OK_blankPinCode));
		getDriver().findElement(Btn_OK_blankPinCode).click();
	}
	
	
	public  void userClicksOnCheckServiceabiltyButtonAndValidatePincodeErrorMessage(String pickupError, String dropError) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Test')]".replace("Test", pickupError))));
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", pickupError))).getText(), pickupError,"Pickup pincode error message incorrect");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Test')]".replace("Test", dropError))));
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", dropError))).getText(), dropError,"Drop pincode error message incorrect");
		
		softAssert.assertAll();
	}
	
	
	
	public  void validateOriginHubAndDestinationHub(String origin_hub, String destination_hub) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(originHubLocator));
		softAssert.assertEquals(getDriver().findElement(originHubLocator).getAttribute("value"), origin_hub,"Origin Hub wrongly populated");
		wait.until(ExpectedConditions.visibilityOfElementLocated(originHubLocator));
		softAssert.assertEquals(getDriver().findElement(destHubLocator).getAttribute("value"), destination_hub,"Destination Hub Wrongly populated");
		softAssert.assertAll();
		
	}
	
	public  void functionalErrorValidationEnterClientNameAndBasicInformation(String clientName, String route_mode, String booking_mode, String pickupPincode, String dropPincode) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientTxtinput));
//		WebElement clientTxtbox=getDriver().findElement(clientTxtinput);
//		clientTxtbox.sendKeys(clientName);
//		Thread.sleep(1000);
//		clientTxtbox.sendKeys(Keys.ENTER);
		getDriver().findElement(clientTxtinput).click();
		Thread.sleep(1000);
		List<WebElement> listClientTxt=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		
		for(int i=0;i<=listClientTxt.size()-1;i++) {
			System.out.println(listClientTxt.get(i).getText());
			if(listClientTxt.get(i).getText().equalsIgnoreCase(clientName)) {
				listClientTxt.get(i).click();
				break;
			}
		}
		Thread.sleep(3000);
		getDriver().findElement(By.xpath("//label[contains(text(),'test')]".trim().replace("test", route_mode))).click();
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("//label[contains(text(),'test')]".trim().replace("test", booking_mode))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(pickupPincodeinput));
		getDriver().findElement(pickupPincodeinput).sendKeys(pickupPincode);
		wait.until(ExpectedConditions.visibilityOfElementLocated(dropPincodeinput));
		getDriver().findElement(dropPincodeinput).sendKeys(dropPincode);
		wait.until(ExpectedConditions.visibilityOfElementLocated(servicibiltyBttn));
		getDriver().findElement(servicibiltyBttn).click();
	}
	
	public  void validateTheErrorMessageDisplayedFunctionalError(String message) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'ABC')]".replace("ABC", message))));
		softAssert.assertEquals(getDriver().findElement(By.xpath("//div[contains(text(),'ABC')]".replace("ABC", message))).getText(), message,"Functional Error message not populated properly");
		softAssert.assertAll();
	}
	
	public  void validateErrorMessagesDisplayedAfterClickingOnReserButton(String messagePickpincode, String messageDestPincode, String messageOriginHub, String messageDestHub) throws InterruptedException {
		Thread.sleep(4000);
		getDriver().findElement(serviceabilityLabelLocator).click();
		Thread.sleep(1000);
		getDriver().findElement(resetButtonLocator).click();
		Thread.sleep(1000);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'ABC')]".replace("ABC", messagePickpincode))).getText(), messagePickpincode,"Pick up pincode Error message not populated properly");
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'ABC')]".replace("ABC", messageDestPincode))).getText(), messageDestPincode,"Destination pincode Error message not populated properly");	 
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'ABC')]".replace("ABC", messageOriginHub))).getText(), messageOriginHub,"Origin Hub Error message not populated properly");
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'ABC')]".replace("ABC", messageDestHub))).getText(), messageDestHub,"Destination Hub Error message not populated properly");
		softAssert.assertAll();
	}
	
	public  void validatePickupLocationDetails(String pickupPincode, String city, String state) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(pickpupLocationLocator));
		softAssert.assertEquals(getDriver().findElement(pickpupLocationLocator).getAttribute("value"), pickupPincode);
		wait.until(ExpectedConditions.visibilityOfElementLocated(cityLocator));
		softAssert.assertEquals(getDriver().findElement(cityLocator).getAttribute("value"), city);
		wait.until(ExpectedConditions.visibilityOfElementLocated(stateLocator));
		softAssert.assertEquals(getDriver().findElement(stateLocator).getAttribute("value"), state);
		softAssert.assertAll();
		
	}
	
	public  void validateBlankErrorMessagesPickupLocation(String ErrorLocationMessage, String ErrorMobileMessage, String ErrorEmailMessage, String ErrorAddressMessage, String ErrorGSTNumberMessage) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorLocationMessage))));
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorLocationMessage))).getText(), ErrorLocationMessage,"Error message not populated correctly");
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorMobileMessage))).getText(), ErrorMobileMessage,"Error message not populated correctly");
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorEmailMessage))).getText(), ErrorEmailMessage,"Error message not populated correctly");
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorAddressMessage))).getText(), ErrorAddressMessage,"Error message not populated correctly");
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorGSTNumberMessage))).getText(), ErrorGSTNumberMessage,"Error message not populated correctly");
		
		softAssert.assertAll();
	}
	public  void userEntersPickupLocationDetailsFormatValidation(String locationName, String mobile, String email, String address, String gstNo,String telephone) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(addNewLocationBttn));
		getDriver().findElement(addNewLocationBttn).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(locationNameNewLocationAdditionLocator));
		getDriver().findElement(locationNameNewLocationAdditionLocator).sendKeys(locationName);
		wait.until(ExpectedConditions.visibilityOfElementLocated(mobileLocator));
		getDriver().findElement(mobileLocator).sendKeys(mobile);
		wait.until(ExpectedConditions.visibilityOfElementLocated(telephoneLocator));
		getDriver().findElement(telephoneLocator).sendKeys(telephone);
		wait.until(ExpectedConditions.visibilityOfElementLocated(emailLocator));
		getDriver().findElement(emailLocator).sendKeys(email);
		wait.until(ExpectedConditions.visibilityOfElementLocated(addressLocator));
		getDriver().findElement(addressLocator).sendKeys(address);
		wait.until(ExpectedConditions.visibilityOfElementLocated(gstLocator));
		getDriver().findElement(gstLocator).sendKeys(gstNo);
		wait.until(ExpectedConditions.elementToBeClickable(pickupSavebttn));
		getDriver().findElement(pickupSavebttn).click();
		Thread.sleep(2000);
	}
	
	public  void validateFormatErrorMessagesOfPickupLocationDetails(String errorMobileFormat, String errorEmailFormat, String errorGSTFormat, String errorTelephoneFormat) throws InterruptedException {
		String txtMobile=getDriver().findElement(mobileLocator).getAttribute("value");
		Thread.sleep(3000);
		String txtEmail=getDriver().findElement(emailLocator).getAttribute("value");
		Thread.sleep(3000);
		String txtGst=getDriver().findElement(gstLocator).getAttribute("value");
		String txtTelephone=getDriver().findElement(telephoneLocator).getAttribute("value");
		String regexEmail = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		String regexGST="^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$"; 
		String regexMobile="^[6-9][0-9]{10}";
		if((txtEmail.matches(regexEmail)==false)){
			softAssert.assertEquals(getDriver().findElement(errorEmailFormatLocator).getText(), errorEmailFormat,"Email error message not displayed correctly");
			softAssert.assertAll();
			}
		if(txtMobile.matches(regexMobile)) {
			softAssert.assertEquals(getDriver().findElement(errorMobileFormatLocator).getText(), errorMobileFormat,"Mobile format error message not displayed correctly");
			softAssert.assertAll();
			}
		if(txtGst.matches(regexGST)==false) {
			softAssert.assertEquals(getDriver().findElement(errorGSTnoFormatLocator).getText(), errorGSTFormat, "GST format error message not displayed correctly");
			softAssert.assertAll();
			}
		if(txtTelephone.length()<6 && txtTelephone.isEmpty()==false) {
			softAssert.assertEquals(getDriver().findElement(errorTelephoneFormatLocator).getText(), errorTelephoneFormat, "Telephone format error message not displayed correctly");
			softAssert.assertAll();
			}
		softAssert.assertAll();
	}
	
	public  void userNavigatesBackToPickUpLocationTabAndValidateDetailsSaved(String locationName,String mobile,String email,String address,String gstNo) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(pickuplocationTabLocator));
		getDriver().findElement(pickuplocationTabLocator).click();
		Thread.sleep(2000);
		softAssert.assertEquals(getDriver().findElement(mobileLocator).getAttribute("value"), mobile,"Mobile error not displayed correctly");
		softAssert.assertEquals(getDriver().findElement(emailLocator).getAttribute("value"), email,"Email error not displayed correctly");
		softAssert.assertEquals(getDriver().findElement(addressLocator).getAttribute("value"), address,"address error not displayed correctly");
		softAssert.assertEquals(getDriver().findElement(gstLocator).getAttribute("value"), gstNo,"GST  error not displayed correctly");
		wait.until(ExpectedConditions.elementToBeClickable(addNewLocationBttn));
		getDriver().findElement(addNewLocationBttn).click();
		
		softAssert.assertAll();
	}
	
	public  void userClicksOnSaveButtonInShipmentDetailsTab() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(shipmentSavebttn));
		
		getDriver().findElement(shipmentSavebttn).click();
	}
	
	public  void validateErrorMessagesForBlankInputsOfShipmentDetails(String errorblankNumberofInvoices, String errorblankTotalDeclaredValue, String errorblankNumberMPS, String errorblankTotalDeclredWeight, String errorblanklength, String errorblankBreadth, String errorblankHeight, String errorblankNoBoxes) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(errorNoOfInvoicesLocator));
		softAssert.assertEquals(getDriver().findElement(errorNoOfInvoicesLocator).getText(), errorblankNumberofInvoices);
		softAssert.assertEquals(getDriver().findElement(errorTotalShipmentDeclaredValueLocator).getText(), errorblankTotalDeclaredValue);
		softAssert.assertEquals(getDriver().findElement(errorNumberOfMPsLocator).getText(), errorblankNumberMPS);
		softAssert.assertEquals(getDriver().findElement(errorTotalShipmentDEclaredWeightLocator).getText(), errorblankTotalDeclredWeight);
		softAssert.assertEquals(getDriver().findElement(errorLengthLocator).getText(), errorblanklength);
		softAssert.assertEquals(getDriver().findElement(errorBreadthLocator).getText(), errorblankBreadth);
		softAssert.assertEquals(getDriver().findElement(errorHeightLocator).getText(), errorblankHeight);
		softAssert.assertEquals(getDriver().findElement(errorNoOfBoxesLocator).getText(), errorblankNoBoxes);
			
		softAssert.assertAll();
		
	}
	
	public  void validateErrorMessageTotalShipmentValueErrorAndTotalShipmentWeightError(String messageValue, String messageWeight) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Test')]".replace("Test", messageValue))));
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", messageValue))).getText(), messageValue,"Error message not populated correctly");
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", messageWeight))).getText(), messageWeight,"Error message not populated correctly");
		softAssert.assertAll();
	}
	
	public  void ValidateTodaysDateIsAutoPopulatedInShipmentDetailsTab() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(dateLocator));
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu-MM-dd");
		LocalDate localDate = LocalDate.now();
		String formatedSTring=dtf.format(localDate);
		String dateTxt= getDriver().findElement(dateLocator).getAttribute("value");
		Thread.sleep(1000);
		softAssert.assertEquals(formatedSTring, dateTxt);
		softAssert.assertAll();
	}
	
	public  void userEntersShipmentDetailsForErrorValidationOfBoxAndMPSMismatch(String numberOfInvoices, String totalShipmentDeclaredValue, String numberMPS, String totalShipmentDeclaredWeight, String lenghth1, String breadth1, String height1, String noOfBoxes1,String lenghth2,String breadth2, String height2, String noOfBoxes2) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(noOfInvoicesLocator));
		getDriver().findElement(noOfInvoicesLocator).sendKeys(numberOfInvoices);
		wait.until(ExpectedConditions.visibilityOfElementLocated(totalShipmentDeclaredLocator));
		getDriver().findElement(totalShipmentDeclaredLocator).sendKeys(totalShipmentDeclaredValue);
		wait.until(ExpectedConditions.visibilityOfElementLocated(numberOfMpsLocator));
		getDriver().findElement(numberOfMpsLocator).sendKeys(numberMPS);
		wait.until(ExpectedConditions.visibilityOfElementLocated(totalShipmentDeclaredWeightLocator));
		getDriver().findElement(totalShipmentDeclaredWeightLocator).sendKeys(totalShipmentDeclaredWeight);
		Thread.sleep(1000);
		int noOfBoxes1Value=Integer.parseInt(noOfBoxes1);
		int noOfBoxes2Value=Integer.parseInt(noOfBoxes2);
		int noMPSValue=Integer.parseInt(numberMPS);
		int totalNoOfBoxes=noOfBoxes1Value+noOfBoxes2Value;
		
		if(noMPSValue < noOfBoxes1Value) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(lengthLocator));
			getDriver().findElement(lengthLocator).sendKeys(lenghth1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(breadthLocator));
			getDriver().findElement(breadthLocator).sendKeys(breadth1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(heightLocator));
			getDriver().findElement(heightLocator).sendKeys(height1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(noOfBoxesLocator));
			getDriver().findElement(noOfBoxesLocator).sendKeys(noOfBoxes1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(plusSignLocator));
			getDriver().findElement(plusSignLocator).click();
		}
		else {
			wait.until(ExpectedConditions.visibilityOfElementLocated(lengthLocator));
			getDriver().findElement(lengthLocator).sendKeys(lenghth1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(breadthLocator));
			getDriver().findElement(breadthLocator).sendKeys(breadth1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(heightLocator));
			getDriver().findElement(heightLocator).sendKeys(height1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(noOfBoxesLocator));
			getDriver().findElement(noOfBoxesLocator).sendKeys(noOfBoxes1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(plusSignLocator));
			getDriver().findElement(plusSignLocator).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(lengthLocator2));
			getDriver().findElement(lengthLocator2).sendKeys(lenghth2);
			wait.until(ExpectedConditions.visibilityOfElementLocated(breadthLocator2));
			getDriver().findElement(breadthLocator2).sendKeys(breadth2);
			wait.until(ExpectedConditions.visibilityOfElementLocated(heightLocator2));
			getDriver().findElement(heightLocator2).sendKeys(height2);
			wait.until(ExpectedConditions.visibilityOfElementLocated(noOfBoxesLocator2));
			getDriver().findElement(noOfBoxesLocator2).sendKeys(noOfBoxes2);
			wait.until(ExpectedConditions.visibilityOfElementLocated(shipmentSavebttn));
			getDriver().findElement(shipmentSavebttn).click();
			Thread.sleep(3000);
		}
			
	}
	
	public  void validateErrorMessageOfMPSAndBoxesMismatch(String errorMismatch) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(errorMismatchBoxesMPS));
		softAssert.assertEquals(getDriver().findElement(errorMismatchBoxesMPS).getText(), errorMismatch);
		wait.until(ExpectedConditions.elementToBeClickable(errorMismatchOKBttn));
		getDriver().findElement(errorMismatchOKBttn).click();
		softAssert.assertAll();
	}
	
	
	public  void validateBookingUIPresent(String bookingEntryList) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(bookingEntryLocator));
		softAssert.assertEquals(getDriver().findElement(bookingEntryLocator).getText().trim(), bookingEntryList);
	}
	
	public  void validateTableDetails(String srNo, String aWB, String mps, String clientRefNo, String tlType, String routeMode, String bookingMode, String clientName, String origin, String destination, String invoiceValue, String phyWt, String volWt, String pickupDate, String bookingDate,String bookingMethod, String bookingStatus, String bookingType, String Action) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(srNolocator));
		softAssert.assertEquals(getDriver().findElement(srNolocator).getText(), srNo,"Sr No. field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(awblocator).getText(), aWB,"AWb no field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(mpslocator).getText(), mps,"MPS field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(clientRefNolocator).getText(), clientRefNo,"Client Ref No field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(tlTypelocator).getText(), tlType,"TLtype field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(routeModelocator).getText(), routeMode,"Route Modefield wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(bookingModelocator).getText(), bookingMode,"Booking Modefield wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(clientNamelocator).getText(), clientName,"Client Name field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(originlocator).getText(), origin,"Originfield wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(destinationlocator).getText(), destination,"Destinationo field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(invoiceValuelocator).getText(), invoiceValue,"Invoice Value field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(phyWtlocator).getText(), phyWt,"PHYWt field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(volWtlocator).getText(), volWt,"VolWT field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(pickupDatelocator).getText(), pickupDate,"Pick up date field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(bookingDatelocator).getText(), bookingDate,"Booking Date field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(bookingMethodlocator).getText(), bookingMethod,"booking Methodfield wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(bookingStatuslocator).getText(), bookingStatus,"Booking Status field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(bookingTypelocator).getText(), bookingType,"Booking Type field wrongly displayed");
		softAssert.assertEquals(getDriver().findElement(actionlocator).getText(), Action,"Action field wrongly displayed");
	
		softAssert.assertAll();
	}
	
	public  void userNavigatesToConvertInchesToCentimeterTab() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(convertInchesToCentimeterLink));
		getDriver().findElement(convertInchesToCentimeterLink).click();
		
	}
	
	public  void enterInchesValueAndValidateCentimeterValues(String inches, String centimeter) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(convertInchesToCentimeterLink));
		WebElement inchesInputField=getDriver().findElement(inchesInputFieldLocator);
		inchesInputField.sendKeys(inches);
		inchesInputField.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		String centimeterConvertedTxt=getDriver().findElement(centimeterInputFieldLocator).getAttribute("value");
		softAssert.assertEquals(centimeter, centimeterConvertedTxt,"Inches to cengtimeter wrong conversion");
		softAssert.assertAll();
		wait.until(ExpectedConditions.elementToBeClickable(closeBttnLocator));
		getDriver().findElement(closeBttnLocator).click();
	}
	
	public  void userVerifiesNumberOfTotalRecordsAndRecordsDisplayed() throws InterruptedException {
		Thread.sleep(1000);
		List<WebElement> rows = getDriver().findElements(By.tagName("tr")); rows.size();
		int recordsSize=rows.size()-1;
		softAssert.assertEquals(Integer.toString(recordsSize), "10");
		softAssert.assertAll();
	}
	
	public  void validateNumberOfPagesDisplayedAndValidateRecordsDisplayedPerPage() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		String locatorLastEnrty="//b[contains(text(),'Total Records:')]";
		wait.until(ExpectedConditions.elementToBeClickable(navigate2ndPageLocator));
		getDriver().findElement(navigate2ndPageLocator).click();
		List<WebElement> rows = getDriver().findElements(By.tagName("tr")); rows.size();
		int recordsSize=rows.size()-1;
		softAssert.assertEquals(Integer.toString(recordsSize), "10");
		String recordTXt=getDriver().findElement(By.xpath((locatorLastEnrty))).getText();
		String numberOnly = recordTXt.replaceAll("\\D+", "");
		int totalrecordsNumerical=Integer.parseInt(numberOnly);
		int quotient=totalrecordsNumerical/10;
		int remainder=totalrecordsNumerical%10;
		
		if(remainder==0) {
			Thread.sleep(1000);
			getDriver().findElement(navigateLastPageLocator).click();
			Thread.sleep(1000);
			getDriver().findElement(By.xpath("//button[contains(text(),'Test')]".replace("Test", Integer.toString(quotient-1)))).click();
			List<WebElement> rows1 = getDriver().findElements(By.tagName("tr")); rows.size();
			int recordsSize1=rows1.size()-1;
			softAssert.assertEquals(Integer.toString(recordsSize1), "10","Pagination failed");
			}
		else {
			Thread.sleep(1000);
			getDriver().findElement(navigateLastPageLocator).click();
			Thread.sleep(1000);
			getDriver().findElement(By.xpath("//button[contains(text(),'Test')]".replace("Test", Integer.toString(quotient)))).click();
			Thread.sleep(1000);
			List<WebElement> rows2 = getDriver().findElements(By.tagName("tr"));
			rows.size();
			int recordsSize2=rows2.size()-1;
			softAssert.assertEquals(Integer.toString(recordsSize2), "10","Pagination Failed");	
		}
		softAssert.assertAll();
	}
	
	

	
	
	public  void userNavigatesToViewButton() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(viewBttnLocator).click();
	}
	
	public  void validateDetailsDisplayedInViewMode(String serviceability, String pickuplocationView, String shipmentDetails) {
		String viewServiciabilityTXT=getDriver().findElement(viewServiceabiltyLocator).getText();
		softAssert.assertEquals(viewServiciabilityTXT, serviceability);
		String viewPickupTXT=getDriver().findElement(viewPickupLocator).getText();
		softAssert.assertEquals(viewPickupTXT, pickuplocationView);
		
		String viewShipmentTXT=getDriver().findElement(viewShimentDetailsLocator).getText();
		softAssert.assertEquals(viewShipmentTXT, shipmentDetails);
		softAssert.assertAll();
	}
	
	public  void userNavigatesToFilterOption() throws InterruptedException {
		Thread.sleep(2000);
		getDriver().findElement(filterLocator).click();
		Thread.sleep(2000);
		getDriver().findElement(resetFilterLocator).click();
	}
	
	
	
	public  void validateDetailsDisplayedInEditMode(String createBooking, String basicInformation, String deatiledInformation) throws InterruptedException {
		getDriver().findElement(editButtonLocator).click();
		Thread.sleep(30000);
		String createBookingTxt=getDriver().findElement(createBookingLocator).getText();
		softAssert.assertEquals(createBookingTxt, createBooking);
		String basicInfoTxt=getDriver().findElement(basicInformationLocator).getText();
		softAssert.assertEquals(basicInformation, basicInfoTxt);
		String detailInfoTxt=getDriver().findElement(deatiledInformationLocator).getText();
		softAssert.assertEquals(deatiledInformation, detailInfoTxt);
		softAssert.assertAll();	
	}
	
	public  void userEntersFilterInputs(String companyName, String aWB, String bookingStatus, String truckLoadType, String routeMode, String bookingMode, String bookingMethod, String origin, String destination, String fromDateBooking, String fromDatePickup) throws InterruptedException {
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(companyNameFilterLocator));
//		WebElement companyNameFilterTxt=getDriver().findElement(companyNameFilterLocator);
//		companyNameFilterTxt.sendKeys(companyName);
//		Thread.sleep(1000);
//		companyNameFilterTxt.sendKeys(Keys.ENTER);
		getDriver().findElement(companyNameFilterLocator).click();
		Thread.sleep(1000);
		List<WebElement> listCompanyName=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		
		for(int i=0;i<=listCompanyName.size()-1;i++) {
			System.out.println(listCompanyName.get(i).getText());
			if(listCompanyName.get(i).getText().equalsIgnoreCase(companyName)) {
				listCompanyName.get(i).click();
				break;
			}
		}
		
//		WebElement bookingStatusFilterTxt=getDriver().findElement(bookingStatusFilterLocator);
//		bookingStatusFilterTxt.sendKeys(bookingStatus);
//		Thread.sleep(1000);
//		bookingStatusFilterTxt.sendKeys(Keys.ENTER);
		getDriver().findElement(bookingStatusFilterLocator).click();
		Thread.sleep(1000);
		List<WebElement> listbookingStatus=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		
		for(int i=0;i<=listbookingStatus.size()-1;i++) {
			System.out.println(listbookingStatus.get(i).getText());
			if(listbookingStatus.get(i).getText().equalsIgnoreCase(bookingStatus)) {
				listbookingStatus.get(i).click();
				break;
			}
		}
	
//		WebElement truckLoadTypeFilterTxt=getDriver().findElement(truckLoadTypeFilterLocator);
//		truckLoadTypeFilterTxt.sendKeys(truckLoadType);
//		Thread.sleep(1000);
//		truckLoadTypeFilterTxt.sendKeys(Keys.ENTER);
		getDriver().findElement(truckLoadTypeFilterLocator).click();
		Thread.sleep(1000);
		List<WebElement> listtruckLoadType=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		
		for(int i=0;i<=listtruckLoadType.size()-1;i++) {
			System.out.println(listtruckLoadType.get(i).getText());
			if(listtruckLoadType.get(i).getText().equalsIgnoreCase(truckLoadType)) {
				listtruckLoadType.get(i).click();
				break;
			}
		}
	
//		WebElement routeModeFilterTxt=getDriver().findElement(routeModeFilterLocator);
//		routeModeFilterTxt.sendKeys(routeMode);
//		Thread.sleep(1000);
//		routeModeFilterTxt.sendKeys(Keys.ENTER);
		getDriver().findElement(routeModeFilterLocator).click();
		Thread.sleep(1000);
		List<WebElement> listrouteMode=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		
		for(int i=0;i<=listrouteMode.size()-1;i++) {
			System.out.println(listrouteMode.get(i).getText());
			if(listrouteMode.get(i).getText().equalsIgnoreCase(routeMode)) {
				listrouteMode.get(i).click();
				break;
			}
		}
		
		
//		WebElement bookingModeFilterTxt=getDriver().findElement(bookingModeFilterLocator);
//		bookingModeFilterTxt.sendKeys(bookingMode);
//		Thread.sleep(1000);
//		bookingModeFilterTxt.sendKeys(Keys.ENTER);
		getDriver().findElement(bookingModeFilterLocator).click();
		Thread.sleep(1000);
		List<WebElement> listbookingMode=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		
		for(int i=0;i<=listbookingMode.size()-1;i++) {
			System.out.println(listbookingMode.get(i).getText());
			if(listbookingMode.get(i).getText().equalsIgnoreCase(bookingMode)) {
				listbookingMode.get(i).click();
				break;
			}
		}
		
		
//		WebElement bookingMethodFilterTxt=getDriver().findElement(bookingMethodFilterLocator);
//		bookingMethodFilterTxt.sendKeys(bookingMethod);
//		Thread.sleep(1000);
//		bookingMethodFilterTxt.sendKeys(Keys.ENTER);
//		
		getDriver().findElement(bookingMethodFilterLocator).click();
		Thread.sleep(1000);
		List<WebElement> listbookingMethod=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		
		for(int i=0;i<=listbookingMethod.size()-1;i++) {
			System.out.println(listbookingMethod.get(i).getText());
			if(listbookingMethod.get(i).getText().equalsIgnoreCase(bookingMethod)) {
				listbookingMethod.get(i).click();
				break;
			}
		}
//		getDriver().findElement(originFilterLocator).click();
//		Thread.sleep(1000);
//		List<WebElement> listorigin=getDriver().findElements(By.xpath("//div[@class='menu visible']//div"));
//		
//		for(int i=0;i<=listorigin.size()-1;i++) {
//			System.out.println(listorigin.get(i).getText());
//			if(listorigin.get(i).getText().equalsIgnoreCase(origin)) {
//				listorigin.get(i).click();
//				break;
//			}
//		}
//		getDriver().findElement(destinationFilterLocator).click();
//		Thread.sleep(1000);
//		List<WebElement> listdestination=getDriver().findElements(By.xpath("//div[@class='menu visible']//div"));
//		
//		for(int i=0;i<=listdestination.size()-1;i++) {
//			System.out.println(listdestination.get(i).getText());
//			if(listdestination.get(i).getText().equalsIgnoreCase(destination)) {
//				listdestination.get(i).click();
//				break;
//			}
//		}
		
		getDriver().findElement(aWBNoFilterLocator).sendKeys(aWB);
		Thread.sleep(3000);
		getDriver().findElement(By.xpath("//div[contains(text(),'Select Destination')]")).click();
		Thread.sleep(3000);
		getDriver().findElement(By.xpath("//input[@name='BookingFromDate']")).sendKeys(fromDateBooking);
		
		getDriver().findElement(By.xpath("//input[@name='PickUpBookingFromDate']")).sendKeys(fromDatePickup);
		Thread.sleep(1000);
		System.out.println(getDriver().findElement(companyNameFilterLocator).getAttribute("value")+"?????????");
		getDriver().findElement(searchBttnFilterLocator).click();
			
	}
	
	
	public  void validateDetailsDispalyedAfterSearchClickInFilterTab(String companyName, String aWB, String bookingStatus, String truckLoadType, String routeMode, String bookingMode, String bookingMethod, String origin, String destination, String fromDateBooking, String fromDatePickup) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", companyName))));
		softAssert.assertEquals(companyName.toLowerCase(), getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", companyName))).getText().toLowerCase(),"Wrongly displyed company Name");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", aWB))));
		softAssert.assertEquals(aWB, getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", aWB))).getText(), "AWB Number Wrongly displayed");
		softAssert.assertEquals(bookingStatus, getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", bookingStatus))).getText(),"Booking Status wrong;y displayed");
		softAssert.assertEquals(truckLoadType, getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", truckLoadType))).getText(), "TL type wrongly displayed");
		softAssert.assertEquals(routeMode, getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", routeMode))).getText(),"RouteMode wrongly displayed");
		softAssert.assertEquals(bookingMode, getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", bookingMode))).getText(),"BookingMode wrongly displayed");
		softAssert.assertEquals(bookingMethod, getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", bookingMethod))).getText(),"Booking Method wrongly displayed");
		softAssert.assertEquals(fromDateBooking, getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", fromDateBooking))).getText(),"From Date Booking wrongly displayed");
		softAssert.assertEquals( getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", fromDatePickup))).getText(),fromDatePickup,"From Date Pick up wrongly displayed");
		
		softAssert.assertAll();
		
		
	
		
	}
	
	public  void userEntersWrongAWBInput(String awbNumber) throws InterruptedException {
		Thread.sleep(2000);
		getDriver().findElement(aWBNoFilterLocator).sendKeys(awbNumber);
		
	}
	
	public  void validateNoRecordFoundMessage(String message) throws InterruptedException {
		getDriver().findElement(searchBttnFilterLocator).click();
		Thread.sleep(2000);
		softAssert.assertEquals(message,getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", message))).getText());
		softAssert.assertAll();
	}
	
	public  void userEntersFilterInputPartialInputs(String companyName, String aWB, String bookingStatus, String truckLoadType, String routeMode, String bookingMode, String bookingMethod) throws InterruptedException {
		Thread.sleep(5000);
		WebElement companyNameFilterTxt=getDriver().findElement(companyNameFilterLocator);
		companyNameFilterTxt.sendKeys(companyName);
		Thread.sleep(1000);
		companyNameFilterTxt.sendKeys(Keys.ENTER);
		WebElement bookingStatusFilterTxt=getDriver().findElement(bookingStatusFilterLocator);
		bookingStatusFilterTxt.sendKeys(bookingStatus);
		Thread.sleep(1000);
		bookingStatusFilterTxt.sendKeys(Keys.ENTER);
		WebElement truckLoadTypeFilterTxt=getDriver().findElement(truckLoadTypeFilterLocator);
		truckLoadTypeFilterTxt.sendKeys(truckLoadType);
		Thread.sleep(1000);
		truckLoadTypeFilterTxt.sendKeys(Keys.ENTER);
		WebElement routeModeFilterTxt=getDriver().findElement(routeModeFilterLocator);
		routeModeFilterTxt.sendKeys(routeMode);
		Thread.sleep(1000);
		routeModeFilterTxt.sendKeys(Keys.ENTER);
		WebElement bookingModeFilterTxt=getDriver().findElement(bookingModeFilterLocator);
		bookingModeFilterTxt.sendKeys(bookingMode);
		Thread.sleep(1000);
		bookingModeFilterTxt.sendKeys(Keys.ENTER);
		WebElement bookingMethodFilterTxt=getDriver().findElement(bookingMethodFilterLocator);
		bookingMethodFilterTxt.sendKeys(bookingMethod);
		Thread.sleep(1000);
		bookingMethodFilterTxt.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		getDriver().findElement(aWBNoFilterLocator).sendKeys(aWB);
		Thread.sleep(1000);
		getDriver().findElement(searchBttnFilterLocator).click();
	}
	
	
	public  void validateDetailsDisplayedAfterSearchClickinBookingEntryListPartialInputValidation(String companyNameDashboard, String aWBDashboard, String bookingStatusDashboard, String truckLoadTypeDashboard, String routeModeDashboard, String bookingModeDashboard, String bookingMethodDashboard) throws InterruptedException {
		Thread.sleep(3000);
		
		softAssert.assertEquals(companyNameDashboard, getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", companyNameDashboard))).getText(),"Wrongly displyed company Name");
		softAssert.assertEquals(aWBDashboard, getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", aWBDashboard))).getText(), "AWB Number Wrongly displayed");
		softAssert.assertEquals(bookingStatusDashboard, getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", bookingStatusDashboard))).getText(),"Booking Status wrong;y displayed");
		softAssert.assertEquals(routeModeDashboard, getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", routeModeDashboard))).getText(),"RouteMode wrongly displayed");
		softAssert.assertEquals(bookingModeDashboard, getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", bookingModeDashboard))).getText(),"BookingMode wrongly displayed");
		softAssert.assertEquals(bookingMethodDashboard, getDriver().findElement(By.xpath("(//td[contains(text(),'ABC')])[1]".replace("ABC", bookingMethodDashboard))).getText(),"Booking Method wrongly displayed");
		Thread.sleep(1000);
		softAssert.assertAll();
	}
	
	public  void userClicksOnDownloadXlsxButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.elementToBeClickable(downloadXlsxButton));
		getDriver().findElement(downloadXlsxButton).click();
		Thread.sleep(10000);
	}
	
	public  void verifyFileDownloadedAtLocation() {
		
		File f = new File("/home/rushikeshwadgaonkar/Downloads/BookingData.xlsx");
		softAssert.assertTrue(f.exists());
		softAssert.assertAll();
	}
	
	public  void verifyDetailsInTheFileDownloaded(String awb, String mps, String clientRefNo, String tlType, String routeMode, String bookingMode, String clientName, String origin, String destination) throws IOException {
		File f = new File("/home/rushikeshwadgaonkar/Downloads/BookingData.xlsx");
		FileInputStream inputStream = new FileInputStream(f);
		XSSFWorkbook wb=new XSSFWorkbook(inputStream);
		XSSFSheet sheet=wb.getSheet("data");
		XSSFRow row1=sheet.getRow(0);
		softAssert.assertEquals(row1.getCell(0).getStringCellValue(), awb,"");
		softAssert.assertEquals(row1.getCell(1).getStringCellValue(), mps,"");
		softAssert.assertEquals(row1.getCell(2).getStringCellValue(), clientRefNo,"");
		softAssert.assertEquals(row1.getCell(3).getStringCellValue(), tlType,"");
		softAssert.assertEquals(row1.getCell(4).getStringCellValue(), routeMode,"");
		softAssert.assertEquals(row1.getCell(5).getStringCellValue(), bookingMode,"");
		softAssert.assertEquals(row1.getCell(6).getStringCellValue(), clientName,"");
		softAssert.assertEquals(row1.getCell(7).getStringCellValue(), origin,"");
		softAssert.assertEquals(row1.getCell(8).getStringCellValue(), destination,"");
		softAssert.assertAll();
	}

	
	
	
	
	public  void userClicksOnConsigneeDetailsTab() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 60);
		softAssert.assertTrue(getDriver().findElement(consigneedetailsBttn).isEnabled());
	    wait.until(ExpectedConditions.elementToBeClickable(consigneedetailsBttn));
	    Thread.sleep(5000);
		getDriver().findElement(consigneedetailsBttn).click();
	}
	public  void userEntersConsigneeDetails(String Consigneecode, String companyname, String Mobile2, String consigneepoc,String Email2,String gstno2,String address2)throws InterruptedException {	
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(addnewconsigneeBttn));
			getDriver().findElement(addnewconsigneeBttn).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(consigneecodeinput));
			getDriver().findElement(consigneecodeinput).sendKeys(Consigneecode);
			wait.until(ExpectedConditions.visibilityOfElementLocated(companynamelocator));
			getDriver().findElement(companynamelocator).sendKeys(companyname);
			wait.until(ExpectedConditions.visibilityOfElementLocated(mobiletxt2));
			getDriver().findElement(mobiletxt2).sendKeys(Mobile2);
			wait.until(ExpectedConditions.visibilityOfElementLocated(consigneepoclocator));
			getDriver().findElement(consigneepoclocator).sendKeys(consigneepoc);
			wait.until(ExpectedConditions.visibilityOfElementLocated(emailtxt2));
			getDriver().findElement(emailtxt2).sendKeys(Email2);
			wait.until(ExpectedConditions.visibilityOfElementLocated(gstnotxt2));
			getDriver().findElement(gstnotxt2).sendKeys(gstno2);
			wait.until(ExpectedConditions.visibilityOfElementLocated(addresstxt2));
			getDriver().findElement(addresstxt2).sendKeys(address2);
		}
	
	public  void userClicksOnSaveAndNextButtonAndValidateRecordEnteredSuccessful() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(saveandnextbttn));
		getDriver().findElement(saveandnextbttn).click();
	}
	
	public  void validateTheErrorMessages(String ErrorConsigneeCodeMessage,String ErrorCompanyNameMessage,String ErrorMobileMessage,String ErrorConsigneePoc,String ErrorEmailMessage,String ErrorGSTNumberMessage,String ErrorAddressMessage)throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorConsigneeCodeMessage))));
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorConsigneeCodeMessage))).getText(),ErrorConsigneeCodeMessage);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorCompanyNameMessage))).getText(),ErrorCompanyNameMessage);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorMobileMessage))).getText(),ErrorMobileMessage);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorConsigneePoc))).getText(),ErrorConsigneePoc);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorEmailMessage))).getText(), ErrorEmailMessage); 
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorGSTNumberMessage))).getText(),ErrorGSTNumberMessage);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Test')]".replace("Test", ErrorAddressMessage))).getText(),ErrorAddressMessage);
		softAssert.assertAll();
	}
	
	public  void userClicksOnSaveAndNextButtonAndValidateForConsigneeDetailsTabFormatValidation() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
//		wait.until(ExpectedConditions.elementToBeClickable(saveandnextbttn));
		Thread.sleep(3000);
		Actions actions = new Actions(getDriver());
		actions.moveToElement(getDriver().findElement(saveandnextbttn)).click().perform();

//
//		getDriver().findElement(saveandnextbttn).click();
		
	}
	
	public  void userClicksOnSaveAndNextButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
//		wait.until(ExpectedConditions.elementToBeClickable(saveandnextbttn));
		Thread.sleep(3000);
		
		Actions actions = new Actions(getDriver());
        actions.moveToElement(getDriver().findElement(saveandnextbttn)).click().perform();
//		getDriver().findElement(saveandnextbttn).click();
		
	}
	
	public  void userClicksOnConsigneeDetailsTabAndValidateTheResult() throws InterruptedException{ 
		 WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		softAssert.assertTrue(getDriver().findElement(consigneedetailsBttn).isEnabled());
		Thread.sleep(3000);
		getDriver().findElement(consigneedetailsBttn).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(pincodeLabelConsigneeLocator));
		softAssert.assertTrue(getDriver().findElement(pincodeLabelConsigneeLocator).isDisplayed());
		softAssert.assertAll();
	}
	
	public  void userClicksOnAddNewConsigneeAndValidate() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.elementToBeClickable(addnewconsigneeBttn));
		getDriver().findElement(addnewconsigneeBttn).click();
		
	}
	public  void userEnttersFollowingShipmentDetails(String Refno, String Clientrefno) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(referencenoinputtxt));
		getDriver().findElement(referencenoinputtxt).sendKeys(Refno);
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientreferencenumber));
		getDriver().findElement(clientreferencenumber).sendKeys(Clientrefno);
		
	}
	
	public  void userClicksOnInvoiceAndGSTDetailsTab() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 40);
	    wait.until(ExpectedConditions.elementToBeClickable(invoiceandGSTdetailstab));
		getDriver().findElement(invoiceandGSTdetailstab).click();
		
	}
	public  void userEntersInvoiceAndGSTDetails(String invoiceNumber, String invoiceValue, String ewbNumber) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicenumberinput));
		getDriver().findElement(invoicenumberinput).sendKeys(invoiceNumber);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicevalueinput));
		getDriver().findElement(invoicevalueinput).sendKeys(invoiceValue);
		wait.until(ExpectedConditions.visibilityOfElementLocated(ewbnexemptedlocator));
		getDriver().findElement(ewbnexemptedlocator).sendKeys(ewbNumber);
		wait.until(ExpectedConditions.visibilityOfElementLocated(ewbvalidatebttn));
		getDriver().findElement(ewbvalidatebttn).click();
			
	}
	
	public  void usserEntersFollowingInvoiceAndGstDetails(String invoiceNumber, String invoiceValue,String ewbNumber) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicenumberinput));
		getDriver().findElement(invoicenumberinput).sendKeys(invoiceNumber);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicevalueinput));
		getDriver().findElement(invoicevalueinput).sendKeys(invoiceValue);
		wait.until(ExpectedConditions.visibilityOfElementLocated(ewbnexemptedlocator));
		getDriver().findElement(ewbnexemptedlocator).sendKeys(ewbNumber);		
	}
	
	public  void userClicksOnSaveAndNextButtonAndValidateTheInvoiceAndGSTDetailsResult() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(saveandnxtbttn3));
		Thread.sleep(3000);
//		getDriver().findElement(saveandnxtbttn3).click();
		Actions actions = new Actions(getDriver());
		actions.moveToElement(getDriver().findElement(saveandnxtbttn3)).click().perform();


		softAssert.assertAll();
	}
	public  void clickInformationButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 60);
		wait.until(ExpectedConditions.elementToBeClickable(informationbttn));
		getDriver().findElement(informationbttn).click();
		
	}
	
	public  void validateTheInformationErrorMessage(String informationerrormesssage) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		softAssert.assertEquals(getDriver().findElement(informationerrormessagelocator).getText(),informationerrormesssage);
		
	}
	public  void userClicksOnValueAddedServicesTab() throws InterruptedException  {
		WebDriverWait wait = new WebDriverWait(getDriver(), 40);
		wait.until(ExpectedConditions.elementToBeClickable(valueaddedServicestab));
		getDriver().findElement(valueaddedServicestab).click();
		
	}
	
	public  void holdDaysentry(String Holddays) throws InterruptedException  {
		WebDriverWait wait = new WebDriverWait(getDriver(), 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Holddays1));
		Thread.sleep(3000);
		getDriver().findElement(Holddays1).sendKeys(Holddays);
	}
	public  void userEntersDeliveryTime(String RequestedFromTime, String RequestedToTime) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(requestedFromTime));
		getDriver().findElement(requestedFromTime).sendKeys(RequestedFromTime);
		wait.until(ExpectedConditions.visibilityOfElementLocated(requestedToTime));
		getDriver().findElement(requestedToTime).sendKeys(RequestedToTime);
		
	}
	public void userEntersDeliveryDate()throws InterruptedException{
	
	        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
	        Dateutils date = new Dateutils();
	        wait.until(ExpectedConditions.visibilityOfElementLocated(datepickerlocator));
	        getDriver().findElement(datepickerlocator).sendKeys(date.futureDate(1));
	        
	    
    }

	public  void userClicksOnSaveButtonAndValidate() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 60);
		wait.until(ExpectedConditions.elementToBeClickable(savebttn));
		Thread.sleep(12000);
		getDriver().findElement(savebttn).click();
		Thread.sleep(7000);
		
	}
	public  void errorMessageValidation(String ErrorInvoiceNumber,String ErrorInvoiceValue,String ErrorEWBNumber)throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		
		softAssert.assertEquals(getDriver().findElement(errorinvoicenumberlocator).getText(),ErrorInvoiceNumber);
		softAssert.assertEquals(getDriver().findElement(errorinvoicevaluelocator).getText(),ErrorInvoiceValue);
		softAssert.assertEquals(getDriver().findElement(errorebnlocator).getText(),ErrorEWBNumber);	
		softAssert.assertAll();
	}
	
	public  void userClicksOnExpandAllButtonAndCollapseAllButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.elementToBeClickable(expandall));
		getDriver().findElement(expandall).click();	
		wait.until(ExpectedConditions.elementToBeClickable(collapseall));
		getDriver().findElement(collapseall).click();	
	}

	public  void userEnterInvoiceDetails(String invoiceNumber, String invoiceValue) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicenumberinput));
		getDriver().findElement(invoicenumberinput).sendKeys(invoiceNumber);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicevalueinput));
		getDriver().findElement(invoicevalueinput).sendKeys(invoiceValue);
		
		
	}
	public void userEntersInvoiceAndGSTDetailsBookingDynamic(String invoiceValue) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicenumberinput));
		getDriver().findElement(invoicenumberinput).sendKeys(awbBookingGenerated);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicevalueinput));
		getDriver().findElement(invoicevalueinput).sendKeys(invoiceValue);
		
	}
	public  void userValidatesInvoiceResult(String invoicegeneratedText) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		softAssert.assertTrue(getDriver().findElement(saveandnxtbttn3).isEnabled());
		getDriver().findElement(saveandnxtbttn3).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(okButtonLocator));
		getDriver().findElement(okButtonLocator).click();
		softAssert.assertEquals(getDriver().findElement(invoiceGeneratedTxt).getText(),invoicegeneratedText);
		softAssert.assertAll();
	}
	public  void validateEwbErrorMessage(String ewbErrorMessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		softAssert.assertEquals(getDriver().findElement(ewberrormessage).getText(),ewbErrorMessage);
		Thread.sleep(5000);
		$(okButtonLocator).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
		softAssert.assertAll();
	}

	public  void userEntersMultipleInvoiceandGSTDetails(String invoiceNumber, String invoiceValue,
			String ewbNumber, String invoiceNumber1, String invoiceValue1, String ewbNumber1) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicenumberinput));
		getDriver().findElement(invoicenumberinput).sendKeys(invoiceNumber);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicevalueinput));
		getDriver().findElement(invoicevalueinput).sendKeys(invoiceValue);
		wait.until(ExpectedConditions.visibilityOfElementLocated(ewbnexemptedlocator));
		getDriver().findElement(ewbnexemptedlocator).sendKeys(ewbNumber);
		wait.until(ExpectedConditions.elementToBeClickable(ewbvalidatebttn));
		getDriver().findElement(ewbvalidatebttn).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicenumberinput1));
		getDriver().findElement(invoicenumberinput1).sendKeys(invoiceNumber1);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicevalueinput1));
		getDriver().findElement(invoicevalueinput1).sendKeys(invoiceValue1);
		wait.until(ExpectedConditions.visibilityOfElementLocated(ewbnexemptedlocator1));
		getDriver().findElement(ewbnexemptedlocator1).sendKeys(ewbNumber1);
		
		$(ewbvalidatebttn1).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
		
	}

	public  void validateTheInvoiceMessage(String errormessageinvoice) {
	
		softAssert.assertEquals(getDriver().findElement(invoiceerrorvalidation).getText(),errormessageinvoice);
		softAssert.assertAll();
	}
	
	public String futureDate(int noDays) {
        Calendar c = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = new Date();
            String curretDate = df.format(date1);
            Date myDate = df.parse(curretDate);
            c.setTime(myDate);
            c.add(Calendar.DATE, noDays);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String toDate = df.format(c.getTime());
        return toDate;
    }
	
	public void userEnttersFollowingShipmentDetails(String Prodcat, String Refno, String Clientrefno,DataTable sectionDataTable) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
        
        wait.until(ExpectedConditions.visibilityOfElementLocated(productcategoriesinput));
        Thread.sleep(6000);
        getDriver().findElement(productcategoriesinput).click();
        Thread.sleep(6000);
        List<WebElement> prodcateg=getDriver().findElements(By.xpath("(//div[@class='vue-treeselect__menu']//div[@class='vue-treeselect__label-container']//label)"));
        List<WebElement> prodcateg2=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//div[@class='vue-treeselect__checkbox-container']"));
        List<String> multivalueselect=sectionDataTable.asList();
        for(int i=0;i<=prodcateg.size()-1;i++) {
        	for(int j=0;j<=multivalueselect.size()-1;j++) {
        		if(prodcateg.get(i).getText().equalsIgnoreCase(multivalueselect.get(j))) {

            prodcateg2.get(i).click();
        }
    }
    }
        wait.until(ExpectedConditions.visibilityOfElementLocated(referencenoinputtxt));
        getDriver().findElement(referencenoinputtxt).sendKeys(Refno);
        wait.until(ExpectedConditions.visibilityOfElementLocated(clientreferencenumber));
        getDriver().findElement(clientreferencenumber).sendKeys(Clientrefno);
        wait.until(ExpectedConditions.visibilityOfElementLocated(saveandnextbttn2));
        getDriver().findElement(saveandnextbttn2).click();
        
    }
}

	
	
	
	
	
	
	
	
	
	



