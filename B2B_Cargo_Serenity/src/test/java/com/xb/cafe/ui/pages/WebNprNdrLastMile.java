package com.xb.cafe.ui.pages;

import static org.mockito.ArgumentMatchers.isNotNull;

import org.hamcrest.core.IsEqual;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Managed;

public class WebNprNdrLastMile extends PageObject {

	public final By EditClick = By.xpath("(//i[@class='fa fa-edit'])[1]");
	public final By ExpandClick = By
			.xpath("//div[@class='row position']//div//button[@title='Expand All'][normalize-space()='Expand All']");
	public final By ExpandClick1 = By.xpath("//button[normalize-space()='Expand All']");
	public final By AwdDate = By.xpath("//th[normalize-space()='AWB RAD Date']");
	public final By LstMile = By.xpath("//button[normalize-space()='Last Mile Shipment']");
	public final By ValueAS = By.xpath("//th[normalize-space()='Value Added Services']");
	public final By DatnTim = By.xpath("//tr[@class='text-primary rowdisable']//td[11]");
	// public final By DatnTim=By.xpath("//td[normalize-space()='09-04-2022']");
	public final By FilterBtn = By.xpath("//i[@title='Search Filter']");
	public final By IpAwb = By.xpath("//input[@id='AWB']");
	public final By IpAwb1 = By.xpath("(//input[@id='awb'])[1]");
	public final By IpAwb2 = By.xpath("(//div[@id='collapse']//input[@id='AWB'])");
	public final By BtnSearch = By.xpath("//button[normalize-space()='Search']");
	public final By BtnView = By.xpath("//span[@title='View']//img");
	public final By BtnClose = By.xpath("//button[normalize-space()='Close Trip']");
	public final By BtnExpand = By.xpath(
			"//div[@class='tab-content1 tab-header1 tab-pane show fade active']//button[@title='Expand All'][normalize-space()='Expand All']");
	//public final By IpScan = By.xpath("//input[@id='scanAWB']");
	public final By IpScan = By.xpath("(//input[@id='ScanAWB'])[1])");
	public final By IpMps = By.xpath("//input[@id='scanMPS']");
	public final By IpInvoice = By.xpath("//input[@id='physicalInvoices']");
	public final By IpOdo = By.xpath("//input[@id='VehicleCloseODOreading']");
	public final By BtnCloseTrip = By.xpath("//button[normalize-space()='Close Trip']");
	public final By BtnUndn = By.xpath("//button[@class='swal2-confirm swal2-styled']");
	public final By BtnCloseOk = By.xpath("//button[@class='swal2-confirm swal2-styled']");
	public final By BtnEdit = By.xpath("//i[@class='fa fa-edit']");
	public final By BtnEx2 = By.xpath("//label[normalize-space()='Add Shipments']");
	public final By BtnNotAdded = By
			.xpath("//div[@class='col-md-12']//button[@title='Expand All'][normalize-space()='Expand All']");
	public final By ResdlClm = By.xpath("//th[normalize-space()='Re-schedule Date']");
	public final By TabLM = By.xpath("//button[normalize-space()='Last Mile Shipment']");
	public final By VasClm = By.xpath("//th[normalize-space()='Value Added Services']");
	public final By BtnCollap = By.xpath("//i[@data-toggle='collapse']");
	//public final By BtnCollapLM = By.xpath("//i[@class='fa fa-filter filter']");
	public final By BtnCollapLM = By.xpath("//i[@aria-controls='collapse']");
	public final By IpAwbStat = By.xpath("//div[@id='collapse']//div[@id='awbstatus']//input[@type='text']");
	public final By BtnSearch1 = By.xpath(
			"//div[@id='collapse']//div[@class='card']//div[@class='card-block']//div[@class='row']//div//button[@type='button'][normalize-space()='Search']");
	public final By PickupNot = By.xpath("//tr[@class='text-primary rowdisable']//td[3]");
	public final By AwbStatus = By.xpath("(//div[@id='awbstatus']");
	public final By BtnSearch2 = By.xpath(
			"//div[@id='collapse']//div[@class='card']//div[@class='card-block']//div[@class='row']//div//button[@type='button'][normalize-space()='Search']");
	public final By AwbStatus1 = By.xpath("(//td[contains(text(),'Undelivered')])[1]");
	public final By BtnBk = By.xpath("//i[@class='fa fa-arrow-left']");
	public final By ReDate = By.xpath("//tr[1]/td[11]");
	public final By Vdate = By.xpath("//span[@title='Appointment Delivery']");
	public final By Checkbox = By.xpath("//td[@class='text-center']//span[@class='cr enabled']");
	public final By BtnAssign = By.xpath("//button[normalize-space()='Assign To The Trip']");
	public final By BtnOk = By.xpath("//button[@class='swal2-confirm swal2-styled']");
	public final By ExpandClose = By.xpath("//input[@id='subchck1']");
	public final By StartTrip = By.xpath("//button[normalize-space()='Start Trip']");
	public final By OdoIn = By.xpath("//input[@id='vehicleODOReading']");
	public final By LmTab = By.xpath("//a[normalize-space()='Last Mile']");
	public final By ScanDoc = By.xpath("(//input[@name='Scandocument'])[2]");
	public final By AwbDoc = By.xpath("//label[normalize-space()='AWB Document']");
	public final By StartClose = By.xpath("//button[@class='-btn btn-primary1'][normalize-space()='Close']");
	public final By TabLM1 = By.xpath("//button[normalize-space()='Last Mile']");
	public final By TabUndlvrd = By.xpath("//label[normalize-space()='Undelivered AWBs']");
	public final By DocInScan = By.xpath("//label[@for='Docchck2']");
	public final By ScanAwb = By.xpath("//input[@id='scanUndeliveredAWB']");
	public final By Scaninvoice = By.xpath("//input[@id='lmPhysicalInvoices']");
	public final By SaveInvoice = By.xpath("//button[@class='-btn btn-success'][normalize-space()='Save']");
	public final By MpsScan = By.xpath("//label[@for='docchck']");
	public final By IpAwbScan = By.xpath("//input[@id='scanUndeliveredMPS']");
	public final By MpsTab = By.xpath("//label[@for='chck2']");
	public final By Redate = By.xpath("//tr[@class='text-primary']//td[11]");
	public final By VasDate = By.xpath("//tr[@class='text-primary']//td[12]");
	public final By MpsAwbScan = By.xpath("//input[@id='ScanAWB']");
	
	WebDriverWait wait = new WebDriverWait(getDriver(), 30);
	
	
	
	
	SoftAssert softAssert = new SoftAssert();
	public String awbBookingGenerated;

	public void userNavigatesToTripManagement() throws InterruptedException {

		getDriver().findElement(EditClick).click();
		Thread.sleep(1000);
		getDriver().findElement(ExpandClick).click();
		Thread.sleep(1000);
		getDriver().findElement(LstMile).click();
		Thread.sleep(5000);
		getDriver().findElement(ExpandClick1).click();
		Thread.sleep(2000);
		softAssert.assertTrue(getDriver().findElement(AwdDate).isDisplayed());
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(ValueAS).isDisplayed());
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(DatnTim).isDisplayed());

	}

	public void searchAndClickOnView(String aWB) throws InterruptedException {
		// Click on Filter
		getDriver().findElement(FilterBtn).click();
		// Send AWB
		getDriver().findElement(IpAwb).sendKeys(aWB);
		Thread.sleep(1000);
		getDriver().findElement(BtnSearch).click();
		// Click on View
		getDriver().findElement(BtnView).click();
		getDriver().findElement(BtnClose).click();

	}

	public void clickedExpandScanAnsSave(String aWB, String invoices) throws InterruptedException {
		Thread.sleep(1000);
		getDriver().findElement(BtnExpand).click();
		// Scan MPS-AWB
		getDriver().findElement(IpScan).sendKeys(aWB);
		getDriver().findElement(IpInvoice).sendKeys(invoices);
		getDriver().findElement(IpMps).sendKeys(aWB);

	}

	public void addOddAndCloseTrip(String odoReading) throws InterruptedException {
		getDriver().findElement(IpOdo).sendKeys(odoReading);
		getDriver().findElement(BtnCloseTrip).click();
		Thread.sleep(1000);
		getDriver().findElement(BtnUndn).click();
		Thread.sleep(1000);
		getDriver().findElement(BtnCloseOk).click();

	}

	public void searchRescheduleColumnIsThere(String aWB, String NprDate) throws InterruptedException {
		// Click on Back Button
		getDriver().findElement(BtnBk).click();
		// Click on Filter
		getDriver().findElement(FilterBtn).click();
		// Send AWB
		getDriver().findElement(IpAwb).sendKeys(aWB);
		Thread.sleep(3000);
		// Click on Edit
		getDriver().findElement(BtnEdit).click();
		Thread.sleep(3000);
		getDriver().findElement(BtnEx2).click();
		Thread.sleep(3000);
		getDriver().findElement(BtnNotAdded).click();
		Thread.sleep(3000);
		getDriver().findElement(BtnCollap).click();
		Thread.sleep(3000);
		getDriver().findElement(IpAwb1).sendKeys(aWB);
		Thread.sleep(3000);
		// getDriver().findElement(IpAwbStat).sendKeys("Pick Up Not Done");
		getDriver().findElement(BtnSearch1).click();
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(PickupNot).getText().contains("PickupNotDone"));
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(ResdlClm).isDisplayed());
		softAssert.assertAll();
		String ABC = getDriver().findElement(DatnTim).getText();
		System.out.println(ABC + ">>>>>>>>>>>>>>>>>>>>");

		softAssert.assertTrue(getDriver().findElement(DatnTim).getText().contains(NprDate));
		softAssert.assertAll();

		// softAssert.assertTrue(Actualdate[0], contains('"+nprDate+"');
		// softAssert.assertTrue(Actualdate[0], IsEqual('"+nprDate+"');
//		softAssert.assertEquals(Actualdate[0], isNotNull());
//		
		// softAssert.assertTrue(getDriver().findElement(DatnTim).isDisplayed());
		// softAssert.assertAll();

	}

	public void VasClmnIsPresent() throws InterruptedException {

		getDriver().findElement(TabLM).click();
		Thread.sleep(1000);
		getDriver().findElement(BtnEx2).click();
		getDriver().findElement(BtnCollapLM).click();
		getDriver().findElement(AwbStatus).sendKeys("Shipment Not Delivered");
		getDriver().findElement(BtnSearch2).click();
		softAssert.assertTrue(getDriver().findElement(AwbStatus1).getText().contains("Undelivered"));
		softAssert.assertTrue(getDriver().findElement(VasClm).isDisplayed());
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(DatnTim).isDisplayed());
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(ResdlClm).isDisplayed());
		softAssert.assertAll();

	}

	public void searchTripToValidateVas(String aWB, String vASdate) throws InterruptedException {
		// Click on Back Button
		//getDriver().findElement(BtnBk).click();
		// Click on Filter
		getDriver().findElement(FilterBtn).click();
		// Send AWB
		getDriver().findElement(IpAwb).sendKeys(aWB);
		Thread.sleep(3000);
		// Click on Edit
		getDriver().findElement(BtnEdit).click();
		Thread.sleep(3000);
		getDriver().findElement(BtnEx2).click();
		Thread.sleep(3000);
		getDriver().findElement(LstMile).click();
		Thread.sleep(3000);
		getDriver().findElement(BtnNotAdded).click();
		Thread.sleep(3000);
		getDriver().findElement(BtnCollap).click();
		Thread.sleep(3000);
		getDriver().findElement(IpAwb2).sendKeys(aWB);
		Thread.sleep(3000);
		getDriver().findElement(BtnSearch1).click();
		Thread.sleep(3000);
		String ABC = getDriver().findElement(Vdate).getText();
		System.out.println(ABC + ">>>>>>>>>>>>>>>>>>>>");

		softAssert.assertTrue(getDriver().findElement(Vdate).getText().contains(vASdate));
		softAssert.assertAll();

	}

	public void clickLastMile(String aWB) throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(LstMile));
		getDriver().findElement(LstMile).click();
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnNotAdded));
		getDriver().findElement(BtnNotAdded).click();
		System.out.println("Click on not added to trip");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnCollapLM));
		getDriver().findElement(BtnCollapLM).click();
		Thread.sleep(1500);
		System.out.println("Click on filter button");
		wait.until(ExpectedConditions.visibilityOfElementLocated(IpAwb2));
		Thread.sleep(1500);
		getDriver().findElement(IpAwb2).sendKeys(aWB);
		System.out.println("Enter AWB ");
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnSearch1));
		Thread.sleep(1500);
		getDriver().findElement(BtnSearch1).click();
		System.out.println("Clicked on search ");
		wait.until(ExpectedConditions.visibilityOfElementLocated(Checkbox));
		Thread.sleep(1500);
		getDriver().findElement(Checkbox).click();
		System.out.println("Check box checked");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnAssign));
		getDriver().findElement(BtnAssign).click();
		System.out.println("Assign to trip ");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnCollapLM));
		getDriver().findElement(BtnCollapLM).click();
		System.out.println("Clicked on Filter to close dropdown ");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(StartTrip));
		getDriver().findElement(StartTrip).click();
		System.out.println("Clicked on Start trip");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(LmTab));
		getDriver().findElement(LmTab).click();
		System.out.println("Clicked on Last mile tab");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(OdoIn));
		getDriver().findElement(OdoIn).sendKeys("1");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(AwbDoc));
		getDriver().findElement(AwbDoc).click();
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(ScanDoc));
		System.out.println("First drop down click ");
		getDriver().findElement(ScanDoc).sendKeys(aWB);
		System.out.println("Scan Document");
		Thread.sleep(5000);
	//////////////////////////////////////////////////////////////////////////////	
		getDriver().findElement(MpsTab).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(MpsAwbScan));
		System.out.println("Clicked on MPS scan tab ");
		Thread.sleep(1500);
		getDriver().findElement(MpsAwbScan).sendKeys(aWB); 
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(StartTrip));
		getDriver().findElement(StartTrip).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(StartClose));
		getDriver().findElement(StartClose).click();

	}

	public void ValidareNdrMarked(String awb, String ndrdate)  {
		
		getDriver().findElement(By.xpath("(//span[@title='View'])[1]")).click();
		getDriver().findElement(TabLM1).click();
		getDriver().findElement(BtnClose).click();
		getDriver().findElement(LmTab).click();
		getDriver().findElement(TabUndlvrd).click();
		getDriver().findElement(DocInScan).click();
		getDriver().findElement(ScanAwb).sendKeys(awb);
		getDriver().findElement(Scaninvoice).sendKeys("1");
		getDriver().findElement(SaveInvoice).click();
		getDriver().findElement(MpsScan).click();
		getDriver().findElement(IpAwbScan).sendKeys(awb);
		getDriver().findElement(MpsScan).click();
		getDriver().findElement(TabUndlvrd).click();
		getDriver().findElement(IpOdo).sendKeys("2");
		getDriver().findElement(BtnCloseTrip).click();
		getDriver().findElement(BtnUndn).click();
		getDriver().findElement(BtnOk).click();
		
	}
	
	public void ValidareNdrIsMarked(String awb, String ndrdate) throws InterruptedException  {
		
		getDriver().findElement(IpAwb).clear();
		getDriver().findElement(IpAwb).sendKeys(awb);
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnSearch));
		getDriver().findElement(BtnSearch).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(EditClick));
		getDriver().findElement(EditClick).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnEx2));
		Thread.sleep(1000);
		getDriver().findElement(BtnEx2).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(LstMile));
		getDriver().findElement(LstMile).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnNotAdded));
		getDriver().findElement(BtnNotAdded).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnCollapLM));
		getDriver().findElement(BtnCollapLM).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(IpAwb2));
		getDriver().findElement(IpAwb2).sendKeys(awb);
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnSearch1));
		getDriver().findElement(BtnSearch1).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(Redate));
		Thread.sleep(1000);
		System.out.println(getDriver().findElement(Redate).getText());
		softAssert.assertTrue(getDriver().findElement(Redate).getText().contains(ndrdate));
		softAssert.assertAll();
		Thread.sleep(3000);
		
		
		
			
	}

	public void searchForValueAddedServicesAwb(String vasawb, String vasdate) throws InterruptedException {
		
		getDriver().findElement(IpAwb).clear();
		getDriver().findElement(IpAwb).sendKeys(vasawb);
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnSearch));
		getDriver().findElement(BtnSearch).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(VasDate));
		softAssert.assertTrue(getDriver().findElement(VasDate).getText().contains(vasdate));
		softAssert.assertAll();
		
		
	}

	}