package com.xb.cafe.ui.pages;



import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;



import net.serenitybdd.core.pages.PageObject;


public class Cargo_PrintSticker extends PageObject {
	
	 String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	 String username ;
	 String TagName1;
	 String TagComment1;
	 String Tag_MappedEnvironment;
	
	/*public String getUserName() {
        return this.username;
    }
	
	 public void setUserName(String username) {
	        this.username = username;
	 }*/
	        
	
	public  final By input_UN = By.xpath("//input[@name='email']");
	public  final By input_PW = By.xpath("//input[@id='typepass']");
	public  final By btn_Login = By.xpath("//button[contains(text(),'Login')]");

	public  final By menu_CargoPrintSticker = By.xpath("//span[contains(text(),'Cargo Print Sticker')]");
	public  final By RadioBtn_AWB = By.xpath("//label[contains(text(),'Cargo AWB Number')]");
	public  final By RadioBtn_Carton = By.xpath("//label[contains(text(),'Cargo Carton No')]");
	public  final By Txt_AWB = By.xpath("//input[@id='AwbNo']");
	public  final By Txt_Catron = By.xpath("//input[@id='CartonNo']");
	public  final By Btn_Search = By.xpath("//button[@title='Search']");
	public  final By Btn_Reset = By.xpath("//button[@title='Reset']");
	public  final By Lbl_TotalMPS = By.xpath("//label[contains(text(),'Total MPS Count :')]");
	public  final By Btn_PrintMPS1 = By.xpath("//tbody/tr[1]/td[5]/button[1]");
	public  final By Btn_PrintAWB = By.xpath("//button[contains(text(),'Print AWB')]");
	public  final By Btn_PrintMPS = By.xpath("//button[contains(text(),'Print MPS')]");
	public  final By Msg_NoPrinter = By.xpath("//div[@id='swal2-content']");
	public  final By Btn_OK = By.xpath("//button[contains(text(),'OK')]");
	public  final By ErrMsg_BlankAWBorCartonNo = By.xpath("//span[contains(text(),'StickerType required')]");
	public  final By lbl_ClientName = By.xpath("//div[@class='form-group-row']/legend/div");
	
	
//	public  final By Test = By.name("email");
	public  final By errMsg_InvalidAWBorCartoonNo = By.id("swal2-content");
	public  String StickerOptionType=null;
	

	
	
			
	
	 SoftAssert softAssert= new SoftAssert();
	
	
	
	
	
	

	
	public  void Navigate_PrintAWBMenu() throws InterruptedException 
	{
		getDriver().navigate().refresh();
		Thread.sleep(5000);
		getDriver().findElement(menu_CargoPrintSticker).click();
		Thread.sleep(3000);
		getDriver().findElement(Btn_Reset).click();
		
		
	}
	
	public  void printAWBorCarton_Search(String StickerOption,String AWBorCartonNo) throws InterruptedException 
	{
		if (StickerOption.trim().equals("Cargo AWB Number"))
		{
			getDriver().findElement(RadioBtn_AWB).click();
			getDriver().findElement(Txt_AWB).sendKeys(AWBorCartonNo);
			Thread.sleep(2000);
		}
		else if (StickerOption.trim().equals("Cargo Carton No"))
		{
			getDriver().findElement(RadioBtn_Carton).click();
			getDriver().findElement(Txt_Catron).sendKeys(AWBorCartonNo);
			
		}
		getDriver().findElement(Btn_Search).click();
		Thread.sleep(5000);
		StickerOptionType=StickerOption;
		
	}
	
	public  void validateMPSDetailsShown() throws InterruptedException 
	{	Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(Lbl_TotalMPS).isDisplayed(), "Expected MPS details are not shown");
		softAssert.assertAll();
	}
	
	public  void printFirstMPSorCarton() throws InterruptedException 
	{
		getDriver().findElement(Btn_PrintMPS1).click();
		Thread.sleep(1000);
		
	}
	
	public  void printAllMPSorCarton() throws InterruptedException 
	{
	
		if (StickerOptionType.equals("Cargo AWB Number"))
		{
			getDriver().findElement(Btn_PrintAWB).click();
		}
		else if(StickerOptionType.equals("Cargo Carton No"))
		{
			getDriver().findElement(Btn_PrintMPS).click();
		}
		
		Thread.sleep(1000);
		
	}
	
	public  void errorMsg_Print(String ErrMsg_NoPrinterInstalled) throws InterruptedException 
	{
		softAssert.assertEquals(getDriver().findElement(Msg_NoPrinter).getText().trim(), ErrMsg_NoPrinterInstalled);
		Thread.sleep(1000);
		getDriver().findElement(Btn_OK).click();
		softAssert.assertAll();
		
	}
	
	public  void printSticker_ErrMsg_InvalidAWBorCartoonNo(String ErrMsg_WrongAWBorCartoonNo) throws InterruptedException 
	{
		softAssert.assertEquals(getDriver().findElement(errMsg_InvalidAWBorCartoonNo).getText().trim(), ErrMsg_WrongAWBorCartoonNo, "Expected error condition not found");
		Thread.sleep(1000);
		getDriver().findElement(Btn_OK).click();
		softAssert.assertAll();
		
	}
	
	public  void errorMsg_BlankAWBorCartonNo(String ErrMsg_BlankAWBorCartoonNo) throws InterruptedException 
	{
		Thread.sleep(2000);
		String str = "//span[contains(text(),'Test')]".replace("Test", ErrMsg_BlankAWBorCartoonNo);
		System.out.print("++++++++" +str);
		softAssert.assertTrue(getDriver().findElement(By.xpath((str))).isDisplayed(),"Expected error message is not shown");
		softAssert.assertAll();
		
	}
	
	public  void print_ResetBtn() throws InterruptedException 
	{	Thread.sleep(3000);
		getDriver().findElement(Btn_Reset).click();
		Thread.sleep(500);
	}
	
	public  void validate_blankAWBTOrCartonxtbox() throws InterruptedException 
	{
		if (StickerOptionType.equals("Cargo AWB Number"))
		{
			softAssert.assertTrue(getDriver().findElement(((Txt_AWB))).getAttribute("value").isEmpty(),"AWB Textbox is not empty");
		}
		else if(StickerOptionType.equals("Cargo Carton No"))
		{
			softAssert.assertTrue(getDriver().findElement(((Txt_Catron))).getAttribute("value").isEmpty(),"Carton Textbox is not empty");
		}
		softAssert.assertAll();
		
		
	}
	
	public  void validate_AWBOrCartonData(String ClientName) throws InterruptedException 
	{
		if (getDriver().findElement((lbl_ClientName)).getText().trim().endsWith(ClientName))
		{
			Thread.sleep(4000);
			softAssert.assertTrue(true,"Expected client name is there");
		}
		else 
			Thread.sleep(4000);
			softAssert.assertTrue(false,"Expected client name not fond");
			softAssert.assertAll();

		
	}
	
	
	
}
	

