package com.xb.cafe.ui.pages;

import java.util.List;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;



import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.actions.Upload;

public class ClientServices extends PageObject {

	 public static final String UserValidatesTheErrorMsgAndUserClicksOnOkButton = null;
	private static final CharSequence HigherBreakLimitValue = null;
	String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	 String username;
	 String TagName1;
	 String TagComment1;
	 String Tag_MappedEnvironment;
	//  WebDriver driver = null;
	 
	    public  final By input_UN = By.xpath("//input[@name='email']");
		public  final By input_PW = By.xpath("//input[@id='typepass']");
		public  final By btn_Login = By.xpath("//button[contains(text(),'Login')]");
		public  final By Lnk_Clientmanagement = By.xpath("//span[contains(text(),'Client Management')]");
		public  final By Lnk_Services = By.xpath("//span[contains(text(),'Services')]");
		public  final By Editbtn_locator = By.xpath("//span[@title='Edit']");
		public  final By Ok_btn = By.xpath("//button[contains(text(),'OK')]");
		public  final By Savebtn_loactor = By.xpath("//button[@class='-btn btn-success']");
		public  final By PTLtab_locator =  By.xpath("//a[contains(text(),'PTL (Part Truck Load) Services')]");
		public  final By SLdropdownlocator = By.xpath("//input[@class='vue-treeselect__input']");
		public  final By SLchoosefilelocator= By.xpath("//input[@id='FileUploadPTLAirForward']");
		public  final By SLvalidatefilelocator = By.xpath("//button[contains(text(),'Validate File')]");
		public  final By SLchoosefilewrong = By.xpath("//input[@id='FileUploadPTLSurfaceForward']");
		public  final By SurfaceFTLfileuploadlocator = By.xpath("//input[@id='FileUploadFTLSurfaceForward']");
		public  final By DownloadSalesTATlocator = By.xpath("//button[contains(text(),'Download Sales TAT')]");
		public  final By validationerrorlocator = By.xpath("//td[contains(text(),'Validation Error')]");
		
SoftAssert softAssert = new SoftAssert();
		
		

		public  void LoginSelfServicepage(String URL) throws InterruptedException {
			getDriver().get(URL);
		}



		public void userNavigatesToServicesOption() throws InterruptedException {
			Thread.sleep(1000);
	        getDriver().navigate().refresh();
	        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
	        try {
	                getDriver().navigate().refresh();
	                Thread.sleep(2000);
	                wait.until(ExpectedConditions.elementToBeClickable(Lnk_Services));
	                getDriver().findElement(Lnk_Services).click();
	                Thread.sleep(2000);
	        } catch (Exception e) {
	                getDriver().navigate().refresh();
	                Thread.sleep(2000);
	                wait.until(ExpectedConditions.elementToBeClickable(Lnk_Clientmanagement));
	                getDriver().findElement(Lnk_Clientmanagement).click();
	                Thread.sleep(2000);
	                wait.until(ExpectedConditions.elementToBeClickable(Lnk_Services));
	                getDriver().findElement(Lnk_Services).click();
	        }
		
		}



		public void clickOnEditServices() throws InterruptedException {
			Thread.sleep(1000);
			WebDriverWait wait = new WebDriverWait(getDriver(),10);
			wait.until(ExpectedConditions.elementToBeClickable(Editbtn_locator));
			getDriver().findElement(Editbtn_locator).click();
			Thread.sleep(2000);

			}



		public void userValidatesSuccessmsgServicesAndClicksOnOk(String successmsgServices) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			softAssert.assertEquals(getDriver().findElement(By.xpath("//div[@id='swal2-content']".replace("Record saved successfully.", successmsgServices))).getText(), successmsgServices,"Record saved successfully.");
			 softAssert.assertAll();
							 
				 wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
				getDriver().findElement(Ok_btn).click();
							
				}



		public void clickOnPTLTab() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			Thread.sleep(4000);
			wait.until(ExpectedConditions.elementToBeClickable(PTLtab_locator));
			Thread.sleep(1000);
			getDriver().findElement(PTLtab_locator).click();
						
			}



		public void selectServiceLevelTemplateFromDropDown(String SLTemplate) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(),10);
			Thread.sleep(1000);
		    getDriver().findElement(SLdropdownlocator).click();
		    Thread.sleep(4000);
		    List<WebElement> listtemplateName=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		    for(int i=0;i<=listtemplateName.size()-1;i++) {
		    System.out.println(listtemplateName.get(i).getText());
		    if(listtemplateName.get(i).getText().equalsIgnoreCase(SLTemplate)) {
		   	 listtemplateName.get(i).click();
		   	Thread.sleep(1000);
		    break;
		    
		    }
			
		}
			
		}



		public void selectUploadFileOption() throws InterruptedException {
			getDriver().findElement(By.xpath("//label[contains(text(),'Upload File')]")).click();
			
		}



		public void chooseASLFile(String SLfile) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			Thread.sleep(1000);
			System.out.println("My Path : "+System.getProperty("user.dir"));
			 String filePath = System.getProperty("user.dir")+"/Test Data/ClientSL.csv" ;
			 Thread.sleep(1000);
				System.out.println(filePath +"-------------------------------Filepathstarts frm here");
				 $(SLchoosefilelocator).sendKeys(filePath);
				Thread.sleep(1000);
			
		}



		public void clickOnValidateFileButton() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			Thread.sleep(1000);
			JavascriptExecutor js = (JavascriptExecutor) getDriver();
			js.executeScript("window.scrollBy(0,250)", "");
			 Thread.sleep(1000);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(SLvalidatefilelocator));
			 getDriver().findElement(SLvalidatefilelocator).click();
			 Thread.sleep(1000);
			
			
		}



		public void selectRouteModeAndBookingType() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			Thread.sleep(3000);
			getDriver().findElement(By.xpath("(//i[@class='cr-icon fa fa-check'])[1]")).click();
			getDriver().findElement(By.xpath("(//i[@class='cr-icon fa fa-check'])[3]")).click();
			
		}



		public void validateSLErrorMsgs(String Mandatoryfields, String Greaterthan0, String Integer, String TAT) throws InterruptedException {
         WebDriverWait wait = new WebDriverWait(getDriver(),10);
         Thread.sleep(1000);
         JavascriptExecutor js = (JavascriptExecutor) getDriver();
			js.executeScript("window.scrollBy(0,250)", "");
			
			
softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'Mandatory Fields Missing: E2E TAT')]".replace("Mandatory Fields Missing: E2E TAT", Mandatoryfields))).getText(), Mandatoryfields,"Mandatory Fields Missing: E2E TAT");
  softAssert.assertAll();
  
  softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'E2E TAT should be greater than 0')]".replace("E2E TAT should be greater than 0", Greaterthan0))).getText(), Greaterthan0,"E2E TAT should be greater than 0");
  softAssert.assertAll();
  
  softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'E2E TAT should be an integer value')]".replace("E2E TAT should be an integer value", Integer))).getText(), Integer,"E2E TAT should be an integer value");
  softAssert.assertAll();
  
  softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'E2E TAT should not be less than OPS TAT master')]".replace("E2E TAT should be an integer value", TAT))).getText(), TAT,"E2E TAT should be an integer value");
  softAssert.assertAll();
			
		}



		public void chooseAWrongSLFile(String WrongSLfile) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			Thread.sleep(1000);
			System.out.println("My Path : "+System.getProperty("user.dir"));
			 String filePath = System.getProperty("user.dir")+"/Test Data/SalesTAT.csv" ;
			 Thread.sleep(1000);
				System.out.println(filePath +"-------------------------------Filepathstarts frm here");
				 $(SLchoosefilewrong).sendKeys(filePath);
				Thread.sleep(1000);
				JavascriptExecutor js = (JavascriptExecutor) getDriver();
				js.executeScript("window.scrollBy(0,250)", "");
				 Thread.sleep(1000);
			
		}



		public void chooseASurfaceSLFile(String SurfaceSLfile) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			Thread.sleep(1000);
			System.out.println("My Path : "+System.getProperty("user.dir"));
			 String filePath = System.getProperty("user.dir")+"/Test Data/SalesTATSurface.csv" ;
			 Thread.sleep(1000);
				System.out.println(filePath +"-------------------------------Filepathstarts frm here");
				 $(SurfaceFTLfileuploadlocator).sendKeys(filePath);
				Thread.sleep(1000);
				JavascriptExecutor js = (JavascriptExecutor) getDriver();
				js.executeScript("window.scrollBy(0,250)", "");
				 Thread.sleep(1000);
			
		}



		public void selectRouteModeAndMultipleBookingTypes() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			Thread.sleep(1000);
			getDriver().findElement(By.xpath("(//i[@class='cr-icon fa fa-check'])[6]")).click();
			getDriver().findElement(By.xpath("(//i[@class='cr-icon fa fa-check'])[8]")).click();
			getDriver().findElement(By.xpath("(//i[@class='cr-icon fa fa-check'])[9]")).click();
			getDriver().findElement(By.xpath("(//i[@class='cr-icon fa fa-check'])[10]")).click();
		}



		public void clickOnDownloadSalesTAT() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			JavascriptExecutor js = (JavascriptExecutor) getDriver();
			js.executeScript("window.scrollBy(0,250)", "");
			 Thread.sleep(2000);
		
			 wait.until(ExpectedConditions.elementToBeClickable(DownloadSalesTATlocator));
				getDriver().findElement(DownloadSalesTATlocator).click();
		}



		public void validateValidationError(String Validationerror) throws InterruptedException {
			softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'Validation Error')]".replace("Validation Error", Validationerror))).getText(), Validationerror,"Validation Error");
			  softAssert.assertAll();
			  
			
		}
			
		}
			
		
			
		
			
		
		
		
		
		
		
		