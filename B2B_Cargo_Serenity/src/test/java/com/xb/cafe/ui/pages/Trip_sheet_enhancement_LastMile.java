package com.xb.cafe.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;
import com.xb.cafe.ui.pages.Cargo_FLM_TripManagement;

import net.serenitybdd.core.pages.PageObject;


public class Trip_sheet_enhancement_LastMile extends PageObject{
	
	public final By LastMileButtonLocator=By.xpath("//button[contains(text(), 'Last Mile Shipment')]");
	public final By LastMileConfirmTripButtonLocator=By.xpath("//a[contains(text(),'Last Mile')]");
	public final By expandAllBttn=By.xpath("//button[@title='Expand All']"); 
	public final By ScanAWBDocumentLocator=By.xpath("(//input[@name='Scandocument'])[2]");
	public final By ScanAWBorMPSDocumentLocator=By.xpath("//input[@name='ScanAWB']");
	public final By startTripButtonLocator=By.xpath("//button[contains(text(),'Start Trip')]");
	public final By Btn_Close = By.xpath("(//button[contains(text(), 'Close')])[1]");
	public final By filterButton=By.xpath("//i[@title='Search Filter']");
	public final By resetButton=By.xpath("//button[@type='reset']");
	public final By tripIDFilterInput=By.xpath("//input[@id='tripid']");
	public final By tripIDLocator=By.xpath("(//th[contains(text(),'Trip ID:')]/following-sibling::td[1])[1]");
	public final By searchButton=By.xpath("//button[@type='button']");
	public final By viewModeButton=By.xpath("//span[@title='View']");
	public final By viewModeLastMileButtonLocator=By.xpath("//button[contains(text(),'Last Mile')]");
	
	SoftAssert softAssert = new SoftAssert();
	Cargo_FLM_TripManagement AwbGenerated;
//	public  String awbBookingGenerated = "94192321420610";
	public String tripID;
	
	public void userclicksonLastMileTab() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(LastMileButtonLocator));
		getDriver().findElement(LastMileButtonLocator).click();
		Thread.sleep(2000);
	}

	public void userclicksonLastMileTabforTripconfirmation() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(LastMileConfirmTripButtonLocator));
		WebElement element = getDriver().findElement(LastMileConfirmTripButtonLocator);
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript("arguments[0].click();", element);
//		Actions actions = new Actions(getDriver());
//		Thread.sleep(2000);
//        actions.moveToElement(getDriver().findElement(LastMileConfirmTripButtonLocator)).click().perform();
		//getDriver().findElement(LastMileConfirmTripButtonLocator).click();
		Thread.sleep(2000);	
	}

	public void userclicksonExpandAll() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(expandAllBttn));
		getDriver().findElement(expandAllBttn).click();
	}

	public void userScansAWBNumberAndValidateSuccessMessageBookingWayforLastMile() throws InterruptedException {
		getDriver().findElement(ScanAWBDocumentLocator).sendKeys(AwbGenerated.awbBookingGenerated);
		Thread.sleep(5000);
		getDriver().findElement(ScanAWBorMPSDocumentLocator).sendKeys(AwbGenerated.awbBookingGenerated);
		Thread.sleep(5000);
		getDriver().findElement(startTripButtonLocator).click();
		Thread.sleep(15000);
//		softAssert.assertEquals(getDriver().findElement(By.xpath("//div[contains(text(),'ABC')]".replace("ABC", message))).getText(), message);
//		softAssert.assertAll();
		Thread.sleep(2000);
		getDriver().findElement(Btn_Close).click();
		Thread.sleep(2000);
	}
	
	public void validateTripCreatedDetailsBookingWayforLastMile(String assignedTo,String oDOReading) throws InterruptedException {
		tripID=getDriver().findElement(tripIDLocator).getText();
		getDriver().navigate().refresh();
		Thread.sleep(10000);
//		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
//		Thread.sleep(2000);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(filterButton));
//		getDriver().findElement(filterButton).click();
		$(filterButton).waitUntilVisible().waitUntilClickable().click();
		Thread.sleep(2000);
		getDriver().findElement(resetButton).click();
		Thread.sleep(4000);
		getDriver().findElement(tripIDFilterInput).sendKeys(tripID);
		Thread.sleep(3000);
//		getDriver().findElement(searchButton).click();
		$(searchButton).waitUntilVisible().waitUntilClickable().click();
		Thread.sleep(2000);
		Thread.sleep(2000);
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		je.executeScript("window.scrollBy(0,300)");
		getDriver().findElement(viewModeButton).click();
		Thread.sleep(2000);
		getDriver().findElement(viewModeLastMileButtonLocator).click();
		Thread.sleep(2000);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", AwbGenerated.awbBookingGenerated))).getText(), AwbGenerated.awbBookingGenerated);
		softAssert.assertAll();
		
	}

}
