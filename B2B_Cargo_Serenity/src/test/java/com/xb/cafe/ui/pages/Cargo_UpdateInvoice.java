package com.xb.cafe.ui.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;

public class Cargo_UpdateInvoice extends PageObject{
	
	public final By supportLabelLocator=By.xpath("//span[contains(text(),'Support')]");
	public final By updateInvoiceDetailsLocator=By.xpath("(//span[contains(text(),'Update Invoice Details')])[1]");
	public final By awbSearchLocator=By.xpath("//input[@id='awb']");
	public final By searchButtonLocator=By.xpath("//button[contains(text(),'Search')]");
	public final By srNoLocator=By.xpath("//th[contains(text(),'Sr.No')]");
	public final By invoiceNumberLocator=By.xpath("//th[contains(text(),'Invoice Number')]");
	public final By invoiceValueLocator=By.xpath("//th[contains(text(),'Invoice Value (INR)')]");
	public final By ewbNumberLocator=By.xpath("//th[contains(text(),'EWB Number')]");
	public final By ewbExpiryDateLocator=By.xpath("//th[contains(text(),'EWB Expiry Date')]");
	public final By actionLocator=By.xpath("//th[contains(text(),'Action')][1]");
	public final By resetButtonLocator=By.xpath("//button[contains(text(),'Reset')]");
	public final By awbErrorMessageLocator=By.xpath("//span[contains(text(),'Please enter AWB No')]");
	public final By validateewbbuttonlocator=By.xpath("(//button[contains(text(),'Validate EWB')])[1]");
	public final By validateewbbutton2locator=By.xpath("(//button[contains(text(),'Validate EWB')])[2]");
	public final By ebnErrorMessageLocator=By.xpath("//div[contains(text(),'Please enter EWB Number')]");
	public final By okButtonLocator=By.xpath("(//button[contains(text(),'OK')])[3]");
	public final By dataAwbErrorMessageLocator=By.xpath("//div[contains(text(),'Data not found')]");
	public final By informationIconLocator=By.xpath("(//i[@class='fa fa-info-circle fa-2x'])[2]");
	public final By saveButtonLocator=By.xpath("//button[@title='Save']");
	public final By invalidInvoiceDetailErrorMessageLocator=By.xpath("//div[contains(text(),'Invoice Details are not valid')]");
	public final By closeButtonLocator=By.xpath("(//button[@title='Close'])[2]");
	public final By plusButtonLocator=By.xpath("//i[@title='Add New']");
	public final By invoiceNumber1Locator=By.xpath("//input[@id='InvoiceNumber1']");
	public final By invoiceValue1Locator=By.xpath("//input[@id='InvoiceValue1']");
	public final By ewbNumber1Locator=By.xpath("//input[@id='EWBNumber1']");
	public final By duplicateInvoiceErrorMessageLocator=By.xpath("//div[contains(text(),'Duplicates Invoice Numbers are exists')]");
	public final By deleteButtonLocator=By.xpath("//i[@title='Delete']");
	public final By invoiceNumberErrorMessageLocator=By.xpath("//span[contains(text(),'Invoice Number is required')]");
	public final By	invoiceValueErrorMessageLocator=By.xpath("//span[contains(text(),'Invoice Value is required')]");
	public final By ewbNumber0Locator=By.xpath("//input[@id='EWBNumber0']");
	public final By ewbErrorMessageLocator=By.xpath("//span[contains(text(),'EWB Number is required and must be min 12 digits')]");
	public final By ewbExpirationMessageLocator=By.xpath("//li[contains(text(),'EWB Expired')]");
	public final By invalidInvoiceNumberLocator=By.xpath("//li[contains(text(),'Invalid Invoice Number')]");
	public final By invalidInvoiceValueLocator=By.xpath("//li[contains(text(),'Invoice Value not matched')]");
	public final By partBGeneratedLocator=By.xpath("//li[contains(text(),'Part B is not generated')]");
	public final By successfulMessageLocator=By.xpath("//div[contains(text(),'Updated successfully')]");
	
	
	
	SoftAssert softAssert = new SoftAssert();
	
	public void userNavigatesToUpdateInvoiceDetails() throws InterruptedException  {
		Thread.sleep(3000);
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(updateInvoiceDetailsLocator));
			getDriver().findElement(updateInvoiceDetailsLocator).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(supportLabelLocator));
			getDriver().findElement(supportLabelLocator).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(updateInvoiceDetailsLocator));
			getDriver().findElement(updateInvoiceDetailsLocator).click();
			Thread.sleep(3000);
		}
	}
	
	
	public void userEntersAwbNumber(String awbsearch) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(awbSearchLocator));
		WebElement awbsearchTxt=getDriver().findElement(awbSearchLocator);
		awbsearchTxt.sendKeys(awbsearch);
		Thread.sleep(1000);
		awbsearchTxt.sendKeys(Keys.ENTER);
	}
	public void clickOnSearchButtonAndValidateTheButtonFunctionality()throws InterruptedException {
		getDriver().findElement(searchButtonLocator).click();
	}


	public void clickOnResetButtonAndValidateTheErrorMessage(String awbErrorMessage)throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(awbSearchLocator));
		WebElement awbsearchTxt=getDriver().findElement(awbSearchLocator);
		Thread.sleep(3000);
		getDriver().findElement(searchButtonLocator).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(resetButtonLocator));
		Thread.sleep(1000);
		getDriver().findElement(resetButtonLocator).click();
		softAssert.assertEquals(getDriver().findElement(awbErrorMessageLocator).getText().trim(), awbErrorMessage,"Please enter AWB No");
		softAssert.assertAll();
		
	}


	public void userClicksOnSearchButtonAndValidateTheErrorMessage(String awbErrorMessage)throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(awbSearchLocator));
		WebElement awbsearchTxt=getDriver().findElement(awbSearchLocator);
		Thread.sleep(3000);
		getDriver().findElement(searchButtonLocator).click();
		softAssert.assertEquals(getDriver().findElement(awbErrorMessageLocator).getText().trim(), awbErrorMessage,"Please enter AWB No");
		softAssert.assertAll();
		
		
	}


	public void userClicksOnValidateEwbButtonAndValidateTheBlankEwbInput(String ewbErrorMessage) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(searchButtonLocator).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(validateewbbuttonlocator));
		getDriver().findElement(validateewbbuttonlocator).click();
		softAssert.assertEquals(getDriver().findElement(ebnErrorMessageLocator).getText().trim(), ewbErrorMessage,"Please enter EWB Number");
		softAssert.assertAll();
		getDriver().findElement(okButtonLocator).click();
	}


	public void userClicksOnSearchButtonAndValidateTheDataErrorMessage(String awbDataErrorMessage) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(searchButtonLocator).click();
		softAssert.assertEquals(getDriver().findElement(dataAwbErrorMessageLocator).getText().trim(), awbDataErrorMessage,"Data not found");
		softAssert.assertAll();
		getDriver().findElement(okButtonLocator).click();
	}


	public void userClicksOnValidateEwbButtonAndClicksOnInformationIcon() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(searchButtonLocator).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(validateewbbuttonlocator));
		getDriver().findElement(validateewbbuttonlocator).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(informationIconLocator));
		getDriver().findElement(informationIconLocator).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(closeButtonLocator));
		getDriver().findElement(closeButtonLocator).click();
	}


	public void userClicksOnSaveButtonAndValidatesTheInvalidInvoiceDetails(String invalidInvoiceErrorMessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		wait.until(ExpectedConditions.elementToBeClickable(saveButtonLocator));
		getDriver().findElement(saveButtonLocator).click();
		softAssert.assertEquals(getDriver().findElement(invalidInvoiceDetailErrorMessageLocator).getText().trim(), invalidInvoiceErrorMessage,"Invoice Details are not valid");
		softAssert.assertAll();
		getDriver().findElement(okButtonLocator).click();
	}


	public void userClicksOnPlusButtonAndEntersTheInvoiceDetails(String invoiceNumber, String invoiceValue,
			String ewbNumber) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(searchButtonLocator).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(plusButtonLocator));
		getDriver().findElement(plusButtonLocator).click();
		Thread.sleep(3000);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoiceNumber1Locator));
		WebElement invoicenumberTxt=getDriver().findElement(invoiceNumber1Locator);
		invoicenumberTxt.sendKeys(invoiceNumber);
		Thread.sleep(1000);
		invoicenumberTxt.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoiceValue1Locator));
		WebElement invoicevalueTxt=getDriver().findElement(invoiceValue1Locator);
		invoicevalueTxt.sendKeys(invoiceValue);
		Thread.sleep(1000);
		invoicevalueTxt.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(ewbNumber1Locator));
		WebElement ewbNumberTxt=getDriver().findElement(ewbNumber1Locator);
		ewbNumberTxt.sendKeys(ewbNumber);
		Thread.sleep(1000);
		ewbNumberTxt.sendKeys(Keys.ENTER);
		
		
	}


	public void validateTheDuplicateInvoiceDetails(String duplicateInvoiceErrorMessage) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		wait.until(ExpectedConditions.elementToBeClickable(saveButtonLocator));
		getDriver().findElement(saveButtonLocator).click();
		softAssert.assertEquals(getDriver().findElement(duplicateInvoiceErrorMessageLocator).getText().trim(), duplicateInvoiceErrorMessage,"Duplicates Invoice Numbers are exists");
		softAssert.assertAll();
		
	}


	public void userClicksOnPlusButtonAndDeleteButton() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(searchButtonLocator).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(plusButtonLocator));
		getDriver().findElement(plusButtonLocator).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(deleteButtonLocator));
		getDriver().findElement(deleteButtonLocator).click();
		Thread.sleep(1000);
		getDriver().findElement(okButtonLocator).click();
	}


	public void validateTheInvoiceValueAndInvoiceNumber(String invoiceNumberErrorMessage,
			String invoiceValueErrorMessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		wait.until(ExpectedConditions.elementToBeClickable(saveButtonLocator));
		getDriver().findElement(saveButtonLocator).click();
		softAssert.assertEquals(getDriver().findElement(invoiceNumberErrorMessageLocator).getText().trim(), invoiceNumberErrorMessage,"Invoice Number is required");
		softAssert.assertEquals(getDriver().findElement(invoiceValueErrorMessageLocator).getText().trim(), invoiceValueErrorMessage,"Invoice Value is required");
		softAssert.assertAll();
		
	}


	public void userClicksOnValidateEwbButton() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(searchButtonLocator).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(validateewbbuttonlocator));
		getDriver().findElement(validateewbbuttonlocator).click();
	}


	public void userClicksOnValidateEwbButtonAndValidateTheClickFunctionality() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		wait.until(ExpectedConditions.elementToBeClickable(validateewbbutton2locator));
		getDriver().findElement(validateewbbutton2locator).click();
		
	}


	public void userEntersTheInvalidEwbNumberAndValidatesTheEwbNumber(String ewbNumberOne, String invalidEwbNumberMessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(searchButtonLocator).click();
		Thread.sleep(1000);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(ewbNumber0Locator));
		WebElement ewbNoTxt=getDriver().findElement(ewbNumber0Locator);
		ewbNoTxt.sendKeys(ewbNumberOne);
		Thread.sleep(1000);
		ewbNoTxt.sendKeys(Keys.ENTER);
		
		softAssert.assertEquals(getDriver().findElement(ewbErrorMessageLocator).getText().trim(), invalidEwbNumberMessage,"EWB Number is required and must be min 12 digits");
		softAssert.assertAll();
		
	}


	public void userClicksOnInformationIconAndValidateEWBExpirationMessage(String ewbexpirationmessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(searchButtonLocator).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(validateewbbuttonlocator));
		getDriver().findElement(validateewbbuttonlocator).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(informationIconLocator));
		getDriver().findElement(informationIconLocator).click();
		softAssert.assertEquals(getDriver().findElement(ewbExpirationMessageLocator).getText().trim(), ewbexpirationmessage,"EWB Expired");
		softAssert.assertAll();
		
	}


	public void userEntersTheInvoiceDetails(String invoiceNumber, String invoiceValue, String ewbNumber) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		wait.until(ExpectedConditions.elementToBeClickable(plusButtonLocator));
		Thread.sleep(3000);
		getDriver().findElement(plusButtonLocator).click();
		
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoiceNumber1Locator));
		WebElement invoicenumberTxt=getDriver().findElement(invoiceNumber1Locator);
		invoicenumberTxt.sendKeys(invoiceNumber);
		Thread.sleep(1000);
		invoicenumberTxt.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoiceValue1Locator));
		WebElement invoicevalueTxt=getDriver().findElement(invoiceValue1Locator);
		invoicevalueTxt.sendKeys(invoiceValue);
		Thread.sleep(1000);
		invoicevalueTxt.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(ewbNumber1Locator));
		WebElement ewbNumberTxt=getDriver().findElement(ewbNumber1Locator);
		ewbNumberTxt.sendKeys(ewbNumber);
		Thread.sleep(1000);
		ewbNumberTxt.sendKeys(Keys.ENTER);
		
	}


	public void userEntersEwbNumber(String ewbNumber) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(searchButtonLocator).click();
		Thread.sleep(1000);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(ewbNumber0Locator));
		WebElement ewbTxt=getDriver().findElement(ewbNumber0Locator);
		ewbTxt.sendKeys(ewbNumber);
		Thread.sleep(1000);
		ewbTxt.sendKeys(Keys.ENTER);
		
	}


	public void userClicksOnInformationIconAndValidateFollowingErrorMessage(String errorMessageInvoiceNumber,
			String errorMessageInvoiceValue, String partberrormessage) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		
		wait.until(ExpectedConditions.elementToBeClickable(validateewbbuttonlocator));
		getDriver().findElement(validateewbbuttonlocator).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(informationIconLocator));
		getDriver().findElement(informationIconLocator).click();
		
		softAssert.assertEquals(getDriver().findElement(invalidInvoiceNumberLocator).getText().trim(), errorMessageInvoiceNumber,"Invalid Invoice Number");
		softAssert.assertEquals(getDriver().findElement(invalidInvoiceValueLocator).getText().trim(), errorMessageInvoiceValue,"Invoice Value not matched");
		softAssert.assertEquals(getDriver().findElement(partBGeneratedLocator).getText().trim(), partberrormessage,"Part B is not generated");
		
		softAssert.assertAll();
	}


	public void clickOnValidateEwbButtonAndClickOnSaveButton(String successfulMessage) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(searchButtonLocator).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(validateewbbuttonlocator));
		getDriver().findElement(validateewbbuttonlocator).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(saveButtonLocator));
		Thread.sleep(1000);
		getDriver().findElement(saveButtonLocator).click();
		Thread.sleep(1000);
		softAssert.assertEquals(getDriver().findElement(successfulMessageLocator).getText().trim(), successfulMessage,"Updated successfully");
		softAssert.assertAll();
	}


	public void userClicksOnSearchButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		getDriver().findElement(searchButtonLocator).click();
		
	}


	public void userClicksOnDeleteButton() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		wait.until(ExpectedConditions.elementToBeClickable(deleteButtonLocator));
		getDriver().findElement(deleteButtonLocator).click();
		Thread.sleep(1000);
		getDriver().findElement(okButtonLocator).click();
	}


	public void validateAndClickOnSaveButton(String successfulMessage) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(),10);
		wait.until(ExpectedConditions.elementToBeClickable(validateewbbuttonlocator));
		getDriver().findElement(validateewbbuttonlocator).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(saveButtonLocator));
		Thread.sleep(1000);
		getDriver().findElement(saveButtonLocator).click();	
		softAssert.assertEquals(getDriver().findElement(successfulMessageLocator).getText().trim(),successfulMessage,"Updated successfully");
		softAssert.assertAll();
		
	}
	
	
}
