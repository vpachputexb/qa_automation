package com.xb.cafe.ui.pages;

import java.util.List;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;



import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.actions.Upload;

public class Annexure extends PageObject {

	 public static final String UserValidatesTheErrorMsgAndUserClicksOnOkButton = null;
	private static final CharSequence HigherBreakLimitValue = null;
	String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	 String username;
	 String TagName1;
	 String TagComment1;
	 String Tag_MappedEnvironment;
	//  WebDriver driver = null;
	 
	    public  final By input_UN = By.xpath("//input[@name='email']");
		public  final By input_PW = By.xpath("//input[@id='typepass']");
		public  final By btn_Login = By.xpath("//button[contains(text(),'Login')]");
		public  final By Lnk_Clientmanagement = By.xpath("//span[contains(text(),'Client Management')]");
		public  final By Lnk_Annexure = By.xpath("//span[contains(text(),'Annexures')]");
		public  final By Addnewannexurebtn_locator = By.xpath("//a[@title='Add New Client Annexure']");
		public  final By Companynamedropdown_locator = By.xpath("(//div[@class='ui fluid search selection dropdown form-control'])[1]");
		public  final By Contractiddropdown_locator = By.xpath("(//div[@class='ui fluid search selection dropdown form-control'])[2]");
		public  final By Annexurenameinput_locator = By.xpath("//input[@id='annexurename']");
		public  final By Upload_locator = By.xpath("//input[@id='uploadAnnexure']");
		public  final By Savebtn_loactor = By.xpath("//button[@class='-btn btn-success']");
		public  final By Successfulmsg_locator = By.xpath("//div[@id='swal2-content']");
		public  final By Ok_btn = By.xpath("//button[contains(text(),'OK')]");
		public  final By download_locator = By.xpath("(//i[@title='Download Document'])[1]");
		public  final By Delete_locator = By.xpath("(//span[@title='Delete'])[1]");
		public  final By Filter_locator = By.xpath("//i[@title='Search Filter']");
		public  final By Searchbtn_loactor = By.xpath("//button[@class='-btn btn-success']");
        public  final By Resetbtn_locator = By.xpath("//button[@type='reset']");
		
		
		SoftAssert softAssert = new SoftAssert();
		
		

		public  void LoginSelfServicepage(String URL) throws InterruptedException {
			getDriver().get(URL);
		}



		public void userNavigatesToAnnexureOption() throws InterruptedException {
			Thread.sleep(1000);
	        getDriver().navigate().refresh();
	        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
	        try {
	                getDriver().navigate().refresh();
	                Thread.sleep(2000);
	                wait.until(ExpectedConditions.elementToBeClickable(Lnk_Annexure));
	                getDriver().findElement(Lnk_Annexure).click();
	                Thread.sleep(2000);
	        } catch (Exception e) {
	                getDriver().navigate().refresh();
	                Thread.sleep(2000);
	                wait.until(ExpectedConditions.elementToBeClickable(Lnk_Clientmanagement));
	                getDriver().findElement(Lnk_Clientmanagement).click();
	                Thread.sleep(2000);
	                wait.until(ExpectedConditions.elementToBeClickable(Lnk_Annexure));
	                getDriver().findElement(Lnk_Annexure).click();
	        }
		
		}



		public void clickOnAddNewAnnexureButton() throws InterruptedException {
			 Thread.sleep(5000);
		 		getDriver().findElement(Addnewannexurebtn_locator).click();
			
		}



		public void userSelectsCompanyName(String Companyname) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(),10);
			
            getDriver().findElement(Companynamedropdown_locator).click();
            Thread.sleep(4000);
            List<WebElement> listtemplateName=getDriver().findElements(By.xpath("//div[@class='menu visible']//div"));
            for(int i=0;i<=listtemplateName.size()-1;i++) {
            System.out.println(listtemplateName.get(i).getText());
            if(listtemplateName.get(i).getText().equalsIgnoreCase(Companyname)) {
           	 listtemplateName.get(i).click();
            break;
            }
            }
			
		}



		public void userSelectsContractId(String Contractid) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(),10);
			
            getDriver().findElement(Contractiddropdown_locator).click();
            Thread.sleep(4000);
            List<WebElement> listtemplateName=getDriver().findElements(By.xpath("//div[@class='menu visible']//div"));
            for(int i=0;i<=listtemplateName.size()-1;i++) {
            System.out.println(listtemplateName.get(i).getText());
            if(listtemplateName.get(i).getText().equalsIgnoreCase(Contractid)) {
           	 listtemplateName.get(i).click();
            break;
            }
            }
			
		}



		public void userProvidesAnnexureName(String Annexurename) throws InterruptedException {
	           WebDriverWait wait = new WebDriverWait(getDriver(),10);
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(Annexurenameinput_locator));
				WebElement AnnexurenameTxt=getDriver().findElement(Annexurenameinput_locator);
				Thread.sleep(1000);
				AnnexurenameTxt.sendKeys(Annexurename);
				Thread.sleep(1000);
				AnnexurenameTxt.sendKeys(Keys.ENTER);
				Thread.sleep(1000);
			
		}



		public void userUploadsAnnexureFile(String Annexurefilename) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);

			System.out.println("My Path : "+System.getProperty("user.dir"));
			 String filePath = System.getProperty("user.dir")+"/Test Data/Annexure.png" ;
			
				System.out.println(filePath +"-------------------------------Filepathstarts frm here");
				 $(Upload_locator).sendKeys(filePath);
				Thread.sleep(1000);
		}

		public void userValidatesSuccessfulMsg1AndClicksOnOk(String Successfulmsg) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			softAssert.assertEquals(getDriver().findElement(By.xpath("//div[@id='swal2-content']".replace("Document Upload successfully.", Successfulmsg))).getText(), Successfulmsg,"Document Upload successfully.");
			 softAssert.assertAll();
							 
				 wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
				getDriver().findElement(Ok_btn).click();
							
				}
		
		
			
			
			public void userClicksOnSaveButton1ValidatesSuccessmsgAndClicksOnOk(String Successmsg) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(Savebtn_loactor));
			 getDriver().findElement(Savebtn_loactor).click();
			 Thread.sleep(1000);
			 
			 softAssert.assertEquals(getDriver().findElement(By.xpath("//div[@id='swal2-content']".replace("Record created successfully.", Successmsg))).getText(), Successmsg,"Record created successfully.");
			 softAssert.assertAll();
							 
				 wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
				getDriver().findElement(Ok_btn).click();
							
				}



			public void userUploadsJpegAnnexureFile(String Annexurefilename) throws InterruptedException {
				WebDriverWait wait = new WebDriverWait(getDriver(), 30);

				System.out.println("My Path : "+System.getProperty("user.dir"));
				 String filePath = System.getProperty("user.dir")+"/Test Data/Annexure1.jpeg" ;
				
					System.out.println(filePath +"-------------------------------Filepathstarts frm here");
					 $(Upload_locator).sendKeys(filePath);
					Thread.sleep(1000);
			}



			public void userUploadsPdfAnnexureFile(String Annexurefilename) throws InterruptedException {
				WebDriverWait wait = new WebDriverWait(getDriver(), 30);

				System.out.println("My Path : "+System.getProperty("user.dir"));
				 String filePath = System.getProperty("user.dir")+"/Test Data/Annexure2.pdf" ;
				
					System.out.println(filePath +"-------------------------------Filepathstarts frm here");
					 $(Upload_locator).sendKeys(filePath);
					Thread.sleep(1000);
			}



			public void userUploadsJpgAnnexureFile(String Annexurefilename) throws InterruptedException {
				WebDriverWait wait = new WebDriverWait(getDriver(), 30);

				System.out.println("My Path : "+System.getProperty("user.dir"));
				 String filePath = System.getProperty("user.dir")+"/Test Data/Annexure3.jpg" ;
				
					System.out.println(filePath +"-------------------------------Filepathstarts frm here");
					 $(Upload_locator).sendKeys(filePath);
					Thread.sleep(1000);
			}



			public void userUploadsAnnexureFileOfExceededLimit(String Annexurefilename) throws InterruptedException {
				WebDriverWait wait = new WebDriverWait(getDriver(), 30);

				System.out.println("My Path : "+System.getProperty("user.dir"));
				 String filePath = System.getProperty("user.dir")+"/Test Data/Annexuregreaterthan5mb.png" ;
				
					System.out.println(filePath +"-------------------------------Filepathstarts frm here");
					 $(Upload_locator).sendKeys(filePath);
					Thread.sleep(1000);
			}



			public void userValidatesErrorMsg1AndClicksOnOk(String Errormsg) throws InterruptedException {
				WebDriverWait wait = new WebDriverWait(getDriver(), 30);
				softAssert.assertEquals(getDriver().findElement(By.xpath("//div[@id='swal2-content']".replace("Please upload document less than 5MB.", Errormsg))).getText(), Errormsg,"Please upload document less than 5MB.");
				 softAssert.assertAll();
								 
					 wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
					getDriver().findElement(Ok_btn).click();
								
					}



			public void userClicksOnDownloadButton() throws InterruptedException {
				WebDriverWait wait = new WebDriverWait(getDriver(), 30);
				 wait.until(ExpectedConditions.elementToBeClickable(download_locator));
					getDriver().findElement(download_locator).click();
								
					}



			public void userClicksOnDeleteButton() throws InterruptedException {
				WebDriverWait wait = new WebDriverWait(getDriver(), 30);
				 wait.until(ExpectedConditions.elementToBeClickable(Delete_locator));
					getDriver().findElement(Delete_locator).click();
								
					}



			public void userValidatesConfirmationMsgAndClicksOnOk(String Confirmationmsg) throws InterruptedException {
				WebDriverWait wait = new WebDriverWait(getDriver(), 30);
				softAssert.assertEquals(getDriver().findElement(By.xpath("//div[@id='swal2-content']".replace("Are you sure you want to delete?", Confirmationmsg))).getText(), Confirmationmsg,"Are you sure you want to delete?");
				 softAssert.assertAll();
								 
					 wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
					getDriver().findElement(Ok_btn).click();
								
					}



			public void userUploadsNonAllowedFileFormat(String Annexurefilename) throws InterruptedException {
				WebDriverWait wait = new WebDriverWait(getDriver(), 30);

				System.out.println("My Path : "+System.getProperty("user.dir"));
				 String filePath = System.getProperty("user.dir")+"/Test Data/Annexure4.docx" ;
				
					System.out.println(filePath +"-------------------------------Filepathstarts frm here");
					 $(Upload_locator).sendKeys(filePath);
					Thread.sleep(1000);
			}



			public void userValidatesErrorMsg2AndClicksOnOk(String Errormsg2) throws InterruptedException {
				WebDriverWait wait = new WebDriverWait(getDriver(), 30);
				softAssert.assertEquals(getDriver().findElement(By.xpath("//div[@id='swal2-content']".replace("Please upload only allowed file format", Errormsg2))).getText(), Errormsg2,"Please upload only allowed file format");
				 softAssert.assertAll();
								 
					 wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
					getDriver().findElement(Ok_btn).click();
								
					}



	public void userValidatesErrorMsgs(String Companynameerrormsg, String Contractiderrormsg, String Annexurenameerrormsg, String Fileuploaderrormsg) throws InterruptedException {		
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Please select company name.')]".replace("Please select company name.", Companynameerrormsg))).getText(), Companynameerrormsg,"Please select company name.");
		 softAssert.assertAll();			
			
		 softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Please select contract id.')]".replace("Please select contract id.", Contractiderrormsg))).getText(), Contractiderrormsg,"Please select contract id.");
		 softAssert.assertAll();
			
		 softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Please enter annexure name.')]".replace("Please enter annexure name.", Annexurenameerrormsg))).getText(), Annexurenameerrormsg,"Please enter annexure name.");
		 softAssert.assertAll();
			
		 softAssert.assertEquals(getDriver().findElement(By.xpath("//span[contains(text(),'Please upload file.')]".replace("Please upload file.", Fileuploaderrormsg))).getText(), Fileuploaderrormsg,"Please upload file.");
		 softAssert.assertAll();
	
			
			}

//	public void userClicksOnResetButton() throws InterruptedException {
//		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
//		 wait.until(ExpectedConditions.elementToBeClickable(Resetbtn_locator));
//			getDriver().findElement(Resetbtn_locator).click();
//						
			
	
			}

				
			
				
			
			
				
			
				
			
			
				
			
				
			
		



		
			
			
		



		
		
		