package com.xb.cafe.ui.pages;

import java.sql.SQLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.apis.BookingAPI;
import com.apis.DBConnectivityAndTripIDFetch;
import com.apis.MobileDeliveryAPI;
import com.apis.MobileDeliveryAPI_Multiple;
import com.apis.MobilePickupApis;
import com.apis.TripClosure;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Steps;

public class Cargo_FLM_TripManagement extends PageObject {

	public final By firstMileLastMileLabelLocator = By.xpath("//span[contains(text(),'First & Last Mile')]");
	public final By tripManagementLabelLocator = By.xpath("//span[contains(text(),'Trip Management')]");

	public final By createBookingBttn = By.xpath("//i[@class='fa fa-plus-square-o fa-2x']");
	public final By expandAllBttn = By.xpath("//button[@title='Expand All']");
	public final By selectUserLocator = By.xpath("//input[@class='vue-treeselect__input']");
	public final By vehicleModelLocator = By.xpath("(//input[@class='vue-treeselect__input'])[2]");
	public final By vehicleNumberLocator = By.xpath("//input[@id='adHocVehicleNumber']");
	public final By assignBAVehicleSavebttn = By.xpath("//button[@title='Save']");
	public final By Btn_OKBA = By.xpath("//button[@class='swal2-confirm swal2-styled']");
	public final By expandAllSecondButon = By.xpath("//label[contains(text(),'Not added to the trip')]");
	public final By filteredRecordsLabelLocator = By.xpath("(//b[contains(text(),'Filtered Records:')])[1]");
	public final By selectedRecordsLabelLocator = By.xpath("(//b[contains(text(),'Selected Records:')])[1]");
	public final By assignToTheTripLocator = By.xpath("//button[@title='Assign To The Trip']");
	public final By odoReadingInputLocator = By.xpath("//input[@id='vehicleODOReading']");
	public final By startTripButtonLocator = By.xpath("//button[contains(text(),'Start Trip')]");
	public final By scanDocumentLocator = By.xpath("(//input[@name='Scandocument'])[1]");
	public final By assignToTheTripButtonLocator = By.xpath("//button[@title='Assign To The Trip']");
	public final By tripIDLocator = By.xpath("(//th[contains(text(),'Trip ID:')]/following-sibling::td[1])[1]");
	public final By tripIDFilterInput = By.xpath("//input[@id='tripid']");
	public final By filterButton = By.xpath("//i[@title='Search Filter']");
	public final By resetButton = By.xpath("//button[@type='reset']");
	public final By searchButton = By.xpath("//button[@type='button']");
	public final By viewModeButton = By.xpath("//span[@title='View']");
	public final By contractedVehiclelLocator = By.xpath("(//input[@class='vue-treeselect__input'])[2]");
	public final By aWBNumberGeneratedLocator = By.xpath("//input[@id='awbNo']");

	public final By closeTripButton = By.xpath("//button[contains(text(),'Close Trip')]");
	public final By scanAWBInput = By.xpath("//input[@id='scanAWB']");
	public final By numberOfPhysicalInput = By.xpath("//input[@id='physicalInvoices']");
	public final By scanMPSInput = By.xpath("//input[@id='scanMPS']");
	public final By vehicleCloseODOReadingInput = By.xpath("//input[@id='VehicleCloseODOreading']");
	public final By saveButton = By.xpath("//button[contains(text(),'Save')]");
	public final By okButton = By.xpath("(//button[contains(text(),'OK')])[3]");

	public final By filtericonLocator = By.xpath("(//i[@title='Search Filter'])[1]");
	public final By searchAWBLocator = By.xpath("//input[@id='awb']");
	public final By searchButtonLocator = By.xpath("(//button[@class='-btn btn-success'])[3]");
	public final By selectIconLocator = By.xpath("//table[1]/tbody[1]/tr[\"+i+\"]/td[1]");

	public final By invoicenumberinput = By.cssSelector("input[id=InvoiceNumber0]");
	public final By invoicevalueinput = By.cssSelector("input[id=InvoiceValue0]");
	public final By basrDropdown = By.xpath("(//input[@class='vue-treeselect__input'])[3]");
	public final By tripStatusLocator = By.xpath("//table[@class=\"styled-table widthsize\"]//td[4]");
	public final By tripIDCosureLocator = By.xpath("//table[@class=\"styled-table widthsize\"]//td[2]");
	public final By removeAWBButton = By.xpath("//button[contains(text(),'Remove From The Trip')]");
	public final By mpsLocator = By.xpath("(//table[@class='styled-table widthsize'])[2]//td[3]");

	public final By LstMile = By.xpath("//button[normalize-space()='Last Mile Shipment']");
	public final By BtnExpand = By.xpath(
			"//div[@class='tab-content1 tab-header1 tab-pane show fade active']//button[@title='Expand All'][normalize-space()='Expand All']");
	public final By BtnNotAdded = By
			.xpath("//div[@class='col-md-12']//button[@title='Expand All'][normalize-space()='Expand All']");

	public final By BtnCollapLM = By.xpath("//i[@aria-controls='collapse']");
	public final By IpAwb2 = By.xpath("(//div[@id='collapse']//input[@id='AWB'])");
	public final By BtnSearch1 = By.xpath(
			"//div[@id='collapse']//div[@class='card']//div[@class='card-block']//div[@class='row']//div//button[@type='button'][normalize-space()='Search']");
	public final By Checkbox = By.xpath("//td[@class='text-center']//span[@class='cr enabled']");
	public final By BtnAssign = By.xpath("//button[normalize-space()='Assign To The Trip']");
	public final By BtnOk = By.xpath("//button[@class='swal2-confirm swal2-styled']");
	public final By ExpandClose = By.xpath("//input[@id='subchck1']");
	public final By StartTrip = By.xpath("//button[normalize-space()='Start Trip']");
	public final By OdoIn = By.xpath("//input[@id='vehicleODOReading']");
	public final By LmTab = By.xpath("//a[normalize-space()='Last Mile']");
	public final By ScanDoc = By.xpath("(//input[@name='Scandocument'])[2]");
	public final By AwbDoc = By.xpath("//label[normalize-space()='AWB Document']");
	public final By StartClose = By.xpath("//button[@class='-btn btn-primary1'][normalize-space()='Close']");
	public final By TabLM1 = By.xpath("//button[normalize-space()='Last Mile']");
	public final By TabUndlvrd = By.xpath("//label[normalize-space()='Undelivered AWBs']");
	public final By DocInScan = By.xpath("//label[@for='Docchck2']");
	public final By ScanAwb = By.xpath("//input[@id='scanUndeliveredAWB']");
	public final By Scaninvoice = By.xpath("//input[@id='lmPhysicalInvoices']");
	public final By SaveInvoice = By.xpath("//button[@class='-btn btn-success'][normalize-space()='Save']");
	public final By MpsScan = By.xpath("//label[@for='docchck']");
	public final By IpAwbScan = By.xpath("//input[@id='scanUndeliveredMPS']");
	public final By MpsTab = By.xpath("//label[@for='chck2']");
	public final By Redate = By.xpath("//tr[@class='text-primary']//td[11]");
	public final By VasDate = By.xpath("//tr[@class='text-primary']//td[12]");
	public final By MpsAwbScan = By.xpath("//input[@id='ScanAWB']");
	public final By FilterBtn = By.xpath("//i[@title='Search Filter']");
	public final By IpAwb = By.xpath("//input[@id='AWB']");
	public final By IpAwb1 = By.xpath("(//input[@id='awb'])[1]");
	public final By BtnSearch = By.xpath("//button[normalize-space()='Search']");
	public final By BtnView = By.xpath("//span[@title='View']//img");
	public final By BtnClose = By.xpath("//button[normalize-space()='Close Trip']");
	public final By IpScan = By.xpath("(//input[@id='ScanAWB'])[1])");
	public final By IpMps = By.xpath("//input[@id='scanMPS']");
	public final By IpInvoice = By.xpath("//input[@id='physicalInvoices']");
	public final By IpOdo = By.xpath("//input[@id='VehicleCloseODOreading']");
	public final By BtnCloseTrip = By.xpath("//button[normalize-space()='Close Trip']");
	public final By BtnUndn = By.xpath("//button[@class='swal2-confirm swal2-styled']");
	public final By BtnCloseOk = By.xpath("//button[@class='swal2-confirm swal2-styled']");
	public final By currentHubLocator = By.xpath("//span[@style='color: rgb(255, 255, 255); cursor: default;']");
	public final By changeHubDropDown = By.xpath("//a[@class='nav-link dropdown-toggle']");
	public final By inputHubID = By.xpath("//div[@class='multiselect__tags']");
	public final By saveButtonhubChange = By.xpath("//button[@class='btn btn-success']");
	public final By TripLocator=By.xpath("(//div[@class='col-md-2']//span)[1]");
	public final By BtnFilter = By.xpath("//i[@data-toggle='collapse']");
	public final By IpSearchAwb = By.xpath("//input[@id='awb']");
	public final By BtnSearchAwb = By.xpath("(//button[@type='button'][normalize-space()='Search'])[1]");
	public final By BtncheckAwb = By.xpath("//td[@class='text-center']//i[@class='cr-icon fa fa-check']");
	public final By IpTripId = By.xpath("//input[@id='tripid']");
	public final By ViewBtn = By.xpath("(//span[@title='View'])[1]");
	public final By ExpandAllBtn = By.xpath("(//button[@title='Expand All'][normalize-space()='Expand All'])[1]");
	public final By BtnInscanSave = By.xpath("//button[@class='-btn btn-primary'][normalize-space()='Save']");
	public final By BtnExpandAll = By.xpath("(//button[normalize-space()='Expand All'])[1]");
	public final By FlashMsg = By.xpath("//div[@class='ajs-message ajs-success ajs-visible']");
	public final By LmTab1 = By.xpath("//a[normalize-space()='Last Mile']");
	public final By UnDeliveAwb = By.xpath("//label[normalize-space()='Undelivered AWBs']");
	public final By SubTabExpandAll = By.xpath("//button[@title=' Sub Expand All']");
	public final By ScnDoc = By.xpath("//input[@id='scanUndeliveredAWB']");
	public final By InvcNo = By.xpath("//input[@id='lmPhysicalInvoices']");
	public final By Svbtn = By.xpath("//button[@class='-btn btn-success'][normalize-space()='Save']");
	public final By ScnAwb = By.xpath("//input[@id='scanUndeliveredMPS']");
	@Steps
	MobilePickupApis mobilePickupApiJava;
	
//	@Steps
//	MobileDeliveryAPI mobileDeliveryApiJava;
	
	@Steps
	MobileDeliveryAPI_Multiple mobileDeliveryApiJava;

	SoftAssert softAssert = new SoftAssert();
	public static String awbBookingGenerated;
	public String tripID;
	public static String closureTripID;
	public static String mpsGenerated;
	public static String tripIDNumberWeb;
	public static String tripIDNumberWebLM;
	DBConnectivityAndTripIDFetch dbConnectivityAndTripIDFetch = new DBConnectivityAndTripIDFetch();
	@Steps
	TripClosure tripClosure;

	public void userChangesHubToPickUpHub(String pickUpHub) throws InterruptedException {
		String currentHubName = $(currentHubLocator).waitUntilVisible().waitUntilPresent().getText();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		System.out.println(currentHubName + ">>>>>>>");
		System.out.println(pickUpHub + ">>>>>>>");
		if (!currentHubName.equals(pickUpHub)) {
			$(changeHubDropDown).waitUntilVisible().waitUntilClickable().click();
			Thread.sleep(2000);
			List<WebElement> dropdownList = getDriver()
					.findElements(By.xpath("//div[@class='dropdown-menu dropdown-menu-right']//a"));

			for (int i = 0; i <= dropdownList.size() - 1; i++) {
				System.out.println(dropdownList.get(i).getText());
				if (dropdownList.get(i).getText().trim().equalsIgnoreCase("Change Default Hub")) {
					dropdownList.get(i).click();
					break;
				}
			}
			Thread.sleep(4000);
			$(inputHubID).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();

			Thread.sleep(2000);
			List<WebElement> hubDropDownList = getDriver()
					.findElements(By.xpath("//div[@class='multiselect__content-wrapper']//span/span"));

			for (int i = 0; i <= hubDropDownList.size() - 1; i++) {
				System.out.println(hubDropDownList.get(i).getText());
				if (hubDropDownList.get(i).getText().equalsIgnoreCase(pickUpHub)) {
					hubDropDownList.get(i).click();
					Thread.sleep(4000);
					break;
				}
			}
			Thread.sleep(2000);
			$(saveButtonhubChange).waitUntilPresent().waitUntilVisible().waitUntilClickable().click();
			Thread.sleep(4000);
		}
	}

	public void userNavigatesToTripManagement() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);

		try {
			getDriver().navigate().refresh();
			Thread.sleep(4000);
			wait.until(ExpectedConditions.elementToBeClickable(tripManagementLabelLocator));
			getDriver().findElement(tripManagementLabelLocator).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			Thread.sleep(4000);
			wait.until(ExpectedConditions.elementToBeClickable(firstMileLastMileLabelLocator));
			getDriver().findElement(firstMileLastMileLabelLocator).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(tripManagementLabelLocator));
			getDriver().findElement(tripManagementLabelLocator).click();
			Thread.sleep(3000);
		}
	}

	public void userNavigatesToCreateBookingTripTab() throws InterruptedException {
		Thread.sleep(5000);
		$(createBookingBttn).waitUntilPresent().waitUntilVisible().waitUntilClickable().click();
		$(expandAllBttn).waitUntilPresent().waitUntilVisible().waitUntilClickable().click();
		Thread.sleep(3000);

	}

	public void userEntersAssignmentPersonAndAdHocVehicleDetails(String assignedTo, String adHocVehicleModel,
			String adHocVehicleNumber, String adHocVehicleType) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(3000);
		WebElement userTxtbox = getDriver().findElement(selectUserLocator);
		Thread.sleep(1000);
		userTxtbox.sendKeys(assignedTo);
		Thread.sleep(3000);
		userTxtbox.sendKeys(Keys.ENTER);
		WebElement vehicleTxtbox = getDriver().findElement(vehicleModelLocator);
		vehicleTxtbox.sendKeys(adHocVehicleModel);
		Thread.sleep(3000);
		vehicleTxtbox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("//label[contains(text(),'ABC')]".replace("ABC", adHocVehicleType))).click();
		getDriver().findElement(vehicleNumberLocator).sendKeys(adHocVehicleNumber);
		Thread.sleep(1000);
		getDriver().findElement(assignBAVehicleSavebttn).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(Btn_OKBA));
		Thread.sleep(3000);
		getDriver().findElement(Btn_OKBA).click();
		Thread.sleep(3000);
        String tripId =getDriver().findElement(TripLocator).getText();
        System.out.println(">>>>>>>>>>>>>>>>>tripId>>>>>>>>>>>>>>>>>>>>>>>>>>"+tripId);
        String[] Id = tripId.split(" ");
        tripIDNumberWeb = Id[3];
        System.out.println(">>>>>>>>>>>>>>>>>TripID>>>>>>>>>>>>>>>>>>>>>>>>>>"+tripIDNumberWeb);
        
        System.out.println("Trip Started with vehical");
	}
	public void userEntersAssignmentPersonAndAdHocVehicleDetailsLastMile(String assignedTo, String adHocVehicleModel,
			String adHocVehicleNumber, String adHocVehicleType) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(3000);
		WebElement userTxtbox = getDriver().findElement(selectUserLocator);
		Thread.sleep(1000);
		userTxtbox.sendKeys(assignedTo);
		Thread.sleep(3000);
		userTxtbox.sendKeys(Keys.ENTER);
		WebElement vehicleTxtbox = getDriver().findElement(vehicleModelLocator);
		vehicleTxtbox.sendKeys(adHocVehicleModel);
		Thread.sleep(3000);
		vehicleTxtbox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("//label[contains(text(),'ABC')]".replace("ABC", adHocVehicleType))).click();
		getDriver().findElement(vehicleNumberLocator).sendKeys(adHocVehicleNumber);
		Thread.sleep(1000);
		getDriver().findElement(assignBAVehicleSavebttn).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(Btn_OKBA));
		Thread.sleep(3000);
		getDriver().findElement(Btn_OKBA).click();
		Thread.sleep(3000);
        String tripId =getDriver().findElement(TripLocator).getText();
        System.out.println(">>>>>>>>>>>>>>>>>tripId>>>>>>>>>>>>>>>>>>>>>>>>>>"+tripId);
        String[] Id = tripId.split(" ");
        tripIDNumberWebLM = Id[3];
        System.out.println(">>>>>>>>>>>>>>>>>TripID>>>>>>>>>>>>>>>>>>>>>>>>>>"+tripIDNumberWeb);
        
        System.out.println("Trip Started with vehical");
	}

	public void userEntersDetailsInAddShipmentTabOneAWB(String aWBNumber) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
//		Thread.sleep(30000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(expandAllSecondButon));
//		Thread.sleep(7000);
		getDriver().findElement(expandAllSecondButon).click();
		Thread.sleep(2000);
		String filteredRecordsTxt = getDriver().findElement(filteredRecordsLabelLocator).getText();
		System.out.println(filteredRecordsTxt + "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		String numberOnly = filteredRecordsTxt.replaceAll("\\D+", "");
		int totalfilteredRecordrecordsNumerical = Integer.parseInt(numberOnly);
		System.out.println(getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr[1]/td[2]/span")).getText()
				+ ">>>>>>>>>>>>>>>>>>>>>>>>");
		awbSelectionForTrip(aWBNumber);

	}

	public void awbSelectionForTrip(String awbNumber) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		Thread.sleep(20000);
		Boolean flag = false;
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		do {
			for (int i = 1; i <= 10; i++) {
				if (getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr[" + i + "]/td[2]/span")).getText()
						.equals(awbNumber)) {
					flag = true;
					Thread.sleep(5000);
					System.out.println("11111111111111");
					je.executeScript("arguments[0].scrollIntoView(true);",
							getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr[" + i + "]/td[1]")));
					getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr[" + i + "]/td[1]")).click();
					System.out.println("22222");
					break;
				}
			}
			if (flag == false) {
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath("//button[@aria-label='Go to next page']")));
				System.out.println("33333");
				getDriver().findElement(By.xpath("//button[@aria-label='Go to next page']")).click();
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath("//button[@aria-label='Go to next page']")));
				System.out.println("44444");
			}

		} while (!flag);

		System.out.println("555555");
		Thread.sleep(5000);
	}

	public void userAssignsAWBNumberToTrip() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
//		Thread.sleep(30000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(expandAllSecondButon));
		Thread.sleep(2000);
		getDriver().findElement(assignToTheTripButtonLocator).click();

		Thread.sleep(10000);
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		Thread.sleep(2000);
		WebElement startButton = getDriver().findElement(startTripButtonLocator);
//		je.executeScript("arguments[0].scrollIntoView(true);",startButton);
		Actions actions = new Actions(getDriver());
		actions.moveToElement(startButton).click().perform();

		Thread.sleep(2000);
//		getDriver().findElement(startTripButtonLocator).click();
		Thread.sleep(5000);
	}

	public void userEntersODOReading(String oDOReading) throws InterruptedException {
		getDriver().findElement(odoReadingInputLocator).sendKeys(oDOReading);
		Thread.sleep(2000);
	}

	public void userScansAWBNumberAndValidateSucessMessage(String awbNumber, String message)
			throws InterruptedException {
		getDriver().findElement(scanDocumentLocator).sendKeys(awbNumber);
		Thread.sleep(5000);
		getDriver().findElement(startTripButtonLocator).click();
		Thread.sleep(5000);

		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//div[contains(text(),'ABC')]".replace("ABC", message))).getText(),
				message);
		softAssert.assertAll();
		Thread.sleep(2000);

		getDriver().findElement(Btn_OKBA).click();

		String tripID = getDriver().findElement(tripIDLocator).getText();

	}

	public void validateTripCreatedDetails(String aWBNumber, String assignedTo, String oDOReading)
			throws InterruptedException {
		String tripID = getDriver().findElement(tripIDLocator).getText();
		getDriver().navigate().refresh();
		Thread.sleep(2000);
		getDriver().findElement(filterButton).click();
		Thread.sleep(2000);
		getDriver().findElement(resetButton).click();
		Thread.sleep(2000);
		getDriver().findElement(tripIDFilterInput).sendKeys(tripID);
		Thread.sleep(2000);
		getDriver().findElement(searchButton).click();
		Thread.sleep(2000);
		Thread.sleep(2000);
		getDriver().findElement(viewModeButton).click();
		Thread.sleep(2000);
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", aWBNumber))).getText(),
				aWBNumber);
		softAssert.assertAll();

	}

	public void userEntersAssignementPersonAndContractedVehicleDetails(String assignedTo,
			String contractedVehicleNumber) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(6000);
//		getDriver().findElement(expandAllBttn).click();
//		Thread.sleep(3000);
		WebElement userTxtbox = getDriver().findElement(selectUserLocator);
		userTxtbox.sendKeys(assignedTo);
		Thread.sleep(3000);
		userTxtbox.sendKeys(Keys.ENTER);
		WebElement contractedVehicleTxtbox = getDriver().findElement(contractedVehiclelLocator);
		contractedVehicleTxtbox.sendKeys(contractedVehicleNumber);
		Thread.sleep(3000);
		contractedVehicleTxtbox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		getDriver().findElement(assignBAVehicleSavebttn).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(Btn_OKBA));
		Thread.sleep(3000);
		getDriver().findElement(Btn_OKBA).click();
		Thread.sleep(3000);
        String tripId =getDriver().findElement(TripLocator).getText();
        System.out.println(">>>>>>>>>>>>>>>>>tripId>>>>>>>>>>>>>>>>>>>>>>>>>>"+tripId);
        String[] Id = tripId.split(" ");
        tripIDNumberWeb = Id[3];
        System.out.println(">>>>>>>>>>>>>>>>>TripID>>>>>>>>>>>>>>>>>>>>>>>>>>"+tripIDNumberWeb);
        
        System.out.println("Trip Started with vehical");

	}

	public void userEntersDetailsInAddShipmentTabThreeAWB(String aWBNumber1, String aWBNumber2, String aWBNumber3)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(20000);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(expandAllSecondButon));
//		Thread.sleep(7000);
		getDriver().findElement(expandAllSecondButon).click();
		Thread.sleep(2000);
		String filteredRecordsTxt = getDriver().findElement(filteredRecordsLabelLocator).getText();
		System.out.println(filteredRecordsTxt + "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		String numberOnly = filteredRecordsTxt.replaceAll("\\D+", "");
		int totalfilteredRecordrecordsNumerical = Integer.parseInt(numberOnly);
		System.out.println(getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr[1]/td[2]/span")).getText()
				+ ">>>>>>>>>>>>>>>>>>>>>>>>");
		awbSelectionForTrip(aWBNumber1);
		Thread.sleep(3000);
		awbSelectionForTrip(aWBNumber2);
		Thread.sleep(3000);
		awbSelectionForTrip(aWBNumber3);
		Thread.sleep(3000);
	}

	public void userScansMultipleAWBNumberAndValidateSuccessMessage(String aWBNumber1, String aWBNumber2,
			String message) throws InterruptedException {
		getDriver().findElement(scanDocumentLocator).sendKeys(aWBNumber1);
		Thread.sleep(5000);
		getDriver().findElement(scanDocumentLocator).sendKeys(aWBNumber2);
		Thread.sleep(5000);
		getDriver().findElement(startTripButtonLocator).click();
		Thread.sleep(5000);

		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//div[contains(text(),'ABC')]".replace("ABC", message))).getText(),
				message);
		softAssert.assertAll();
		Thread.sleep(2000);

		getDriver().findElement(Btn_OKBA).click();

	}

	public void captureTheAWBNumberGenerated() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(aWBNumberGeneratedLocator));
		awbBookingGenerated = getDriver().findElement(aWBNumberGeneratedLocator).getAttribute("value");
	}

	public void userEntersInvoiceAndGSTDetailsBookingDynamic(String invoiceValue) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicenumberinput));
		getDriver().findElement(invoicenumberinput).sendKeys(awbBookingGenerated);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicevalueinput));
		getDriver().findElement(invoicevalueinput).sendKeys(invoiceValue);

	}

	public void userEntersDetailsInShipmentTabBookingWay() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(20000);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(expandAllSecondButon));
//		Thread.sleep(7000);
		$(expandAllSecondButon).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
//		getDriver().findElement(expandAllSecondButon).click();
		Thread.sleep(2000);
		String filteredRecordsTxt = getDriver().findElement(filteredRecordsLabelLocator).getText();
		System.out.println(filteredRecordsTxt + "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		String numberOnly = filteredRecordsTxt.replaceAll("\\D+", "");
		int totalfilteredRecordrecordsNumerical = Integer.parseInt(numberOnly);
		System.out.println(getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr[1]/td[2]/span")).getText()
				+ ">>>>>>>>>>>>>>>>>>>>>>>>");
		awbSelectionForTrip(awbBookingGenerated);

	}

	public void userScansAWBNumberAndValidateSuccessMessageBookingWay(String message) throws InterruptedException {
		getDriver().findElement(scanDocumentLocator).sendKeys(awbBookingGenerated);
		Thread.sleep(5000);
		getDriver().findElement(startTripButtonLocator).click();
		Thread.sleep(15000);

		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//div[contains(text(),'ABC')]".replace("ABC", message))).getText(),
				message);
		softAssert.assertAll();
		Thread.sleep(2000);

		getDriver().findElement(Btn_OKBA).click();
		Thread.sleep(2000);

	}

	public void validateTripCreatedDetailsBookingWay(String assignedTo, String oDOReading) throws InterruptedException {
		tripID = getDriver().findElement(tripIDLocator).getText();
		getDriver().navigate().refresh();
		Thread.sleep(15000);
//		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
//		Thread.sleep(2000);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(filterButton));
//		getDriver().findElement(filterButton).click();

		$(filterButton).waitUntilVisible().waitUntilClickable().click();
		Thread.sleep(2000);
		getDriver().findElement(resetButton).click();
		Thread.sleep(4000);
		getDriver().findElement(tripIDFilterInput).sendKeys(tripID);
		Thread.sleep(3000);
//		getDriver().findElement(searchButton).click();
		$(searchButton).waitUntilVisible().waitUntilClickable().click();
		Thread.sleep(2000);
		Thread.sleep(2000);
		getDriver().findElement(viewModeButton).click();
		Thread.sleep(2000);
		mpsGenerated = getDriver().findElement(mpsLocator).getText();
		System.out.println(mpsGenerated + "????????????????????????????????");
		softAssert.assertEquals(getDriver()
				.findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", awbBookingGenerated))).getText(),
				awbBookingGenerated);
		softAssert.assertAll();

	}

	public void validateTripStatus(String tripStatus) throws InterruptedException {
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(15000);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(filterButton));
		$(filterButton).waitUntilVisible().waitUntilClickable().click();
		Thread.sleep(2000);
		getDriver().findElement(resetButton).click();
		Thread.sleep(2000);
		getDriver().findElement(tripIDFilterInput).sendKeys(tripID);
		Thread.sleep(2000);
		getDriver().findElement(searchButton).click();
		Thread.sleep(2000);
		Thread.sleep(2000);
		getDriver().findElement(viewModeButton).click();
		Thread.sleep(2000);
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", tripStatus))).getText(),
				tripStatus);
		softAssert.assertAll();
	}

	public void validateTripCreatedDetailsMultiple(String aWBNumber1, String aWBNumber2, String assignedTo,
			String oDOReading) throws InterruptedException {
		String tripID = getDriver().findElement(tripIDLocator).getText();
		getDriver().navigate().refresh();
		Thread.sleep(15000);
		getDriver().findElement(filterButton).click();
		Thread.sleep(2000);
		getDriver().findElement(resetButton).click();
		Thread.sleep(2000);
		getDriver().findElement(tripIDFilterInput).sendKeys(tripID);
		Thread.sleep(2000);
		getDriver().findElement(searchButton).click();
		Thread.sleep(2000);
		Thread.sleep(2000);
		getDriver().findElement(viewModeButton).click();
		Thread.sleep(2000);
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", aWBNumber1))).getText(),
				aWBNumber1);
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", aWBNumber2))).getText(),
				aWBNumber2);

		softAssert.assertAll();

	}

	public void userOpensTheTripDetails(String tripStatus, String baSR) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Boolean flag = false;
		do {
			for (int i = 1; i <= 10; i++) {
				Thread.sleep(3000);
				if (getDriver().findElement(By.xpath("//table[@class='styled-table widthsize']//tr[" + i + "]//td[4]"))
						.getText().equals(tripStatus)
						&& getDriver()
								.findElement(By.xpath("//table[@class='styled-table widthsize']//tr[" + i + "]//td[6]"))
								.getText().equals(baSR)) {
					flag = true;
					Thread.sleep(1000);
					getDriver()
							.findElement(By.xpath(
									"//table[@class='styled-table widthsize']//tr[" + i + "]//td[8]/span[2]/img[1]"))
							.click();
					Thread.sleep(3000);
					mobilePickupApiJava.viewTrip();

					break;
				}
			}
			if (flag == false) {

				System.out.println("33333");
				getDriver().findElement(By.xpath("//button[contains(text(),'Next')]")).click();
			}

		} while (!flag);

	}

	public void userValidatesTripDetails(String AWBNumber, String mpsNumber) {
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", AWBNumber))).getText(),
				AWBNumber);
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", mpsNumber))).getText(),
				mpsNumber);
		softAssert.assertAll();
	}

	public void userNavigatesToCloseTripButtonAndScansAWB(String AWBNumber, String numberOfPhysicalINvoiceInput)
			throws InterruptedException {
		getDriver().findElement(closeTripButton).click();

		Thread.sleep(3000);

		getDriver().findElement(expandAllBttn).click();
		Thread.sleep(2000);
		getDriver().findElement(scanAWBInput).clear();
		Thread.sleep(2000);
		getDriver().findElement(scanAWBInput).sendKeys(AWBNumber);
		Thread.sleep(2000);

		getDriver().findElement(numberOfPhysicalInput).sendKeys(numberOfPhysicalINvoiceInput);
		Thread.sleep(2000);
		getDriver().findElement(saveButton).click();
		Thread.sleep(2000);

		getDriver().findElement(scanMPSInput).sendKeys(AWBNumber);
		Thread.sleep(500);

	}

	public void userEntersClosureODOReading(String vehicleCloseODOReading) throws InterruptedException {
		getDriver().findElement(vehicleCloseODOReadingInput).sendKeys(vehicleCloseODOReading);
		Thread.sleep(1000);
	}

	public void userClosesTripAndValidateTripClosureMessage(String message) throws InterruptedException {
		getDriver().findElement(closeTripButton).click();
		Thread.sleep(3000);
		getDriver().findElement(okButton).click();
		Thread.sleep(2000);
		softAssert.assertEquals(
				$(By.xpath("//div[contains(text(),'ABC')]".replace("ABC", message))).waitUntilVisible().waitUntilPresent().getText(),
				message);
		softAssert.assertAll();
		Thread.sleep(5000);
		getDriver().findElement(okButton).click();

	}

	public void userValidatesTripDetailsMultiple(String AWBNumber1, String mpsNumber1, String AWBNumber2,
			String mpsNumber2, String AWBNumber3, String mpsNumber3) {
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", AWBNumber1))).getText(),
				AWBNumber1);
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", mpsNumber1))).getText(),
				mpsNumber1);
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", AWBNumber2))).getText(),
				AWBNumber2);
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", mpsNumber2))).getText(),
				mpsNumber2);
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", AWBNumber3))).getText(),
				AWBNumber3);
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//td[contains(text(),'ABC')]".replace("ABC", mpsNumber3))).getText(),
				mpsNumber3);

		softAssert.assertAll();

	}

	public void userNavigatesToCloseTripButtonAndScansAWBMultiple(String AWBNumber1, String mpsNumber1,
			String numberOfPhysicalINvoiceInput1, String AWBNumber2, String mpsNumber2,
			String numberOfPhysicalINvoiceInput2, String AWBNumber3, String mpsNumber3,
			String numberOfPhysicalINvoiceInput3) throws InterruptedException {
		getDriver().findElement(closeTripButton).click();

		Thread.sleep(3000);

		getDriver().findElement(expandAllBttn).click();
		Thread.sleep(1200);
		getDriver().findElement(scanAWBInput).sendKeys(AWBNumber1);
		Thread.sleep(2000);
		getDriver().findElement(numberOfPhysicalInput).sendKeys(numberOfPhysicalINvoiceInput1);
		Thread.sleep(2000);
		getDriver().findElement(saveButton).click();
		Thread.sleep(3000);
		getDriver().findElement(scanAWBInput).clear();
		Thread.sleep(2000);
		getDriver().findElement(scanAWBInput).sendKeys(AWBNumber2);
		Thread.sleep(2000);
		getDriver().findElement(numberOfPhysicalInput).clear();
		Thread.sleep(2000);
		getDriver().findElement(numberOfPhysicalInput).sendKeys(numberOfPhysicalINvoiceInput2);
		Thread.sleep(3000);
		getDriver().findElement(saveButton).click();
		Thread.sleep(2000);
		getDriver().findElement(scanAWBInput).clear();
		Thread.sleep(2000);
		getDriver().findElement(scanAWBInput).sendKeys(AWBNumber3);
		Thread.sleep(2000);
		getDriver().findElement(numberOfPhysicalInput).clear();
		Thread.sleep(2000);
		getDriver().findElement(numberOfPhysicalInput).sendKeys(numberOfPhysicalINvoiceInput3);
		Thread.sleep(2000);
		getDriver().findElement(saveButton).click();
		Thread.sleep(2000);
		getDriver().findElement(scanMPSInput).sendKeys(mpsNumber1);
		Thread.sleep(2000);
		getDriver().findElement(scanMPSInput).clear();
		Thread.sleep(2000);
		getDriver().findElement(scanMPSInput).sendKeys(mpsNumber2);
		Thread.sleep(2000);
		getDriver().findElement(scanMPSInput).clear();
		Thread.sleep(2000);
		getDriver().findElement(scanMPSInput).sendKeys(mpsNumber3);
		Thread.sleep(4000);

	}

	public void userNavigatesToCloseTripButtonAndScansAWBBookingWay(String numberOfPhysicalINvoiceInput)
			throws InterruptedException {
		getDriver().findElement(closeTripButton).click();

		Thread.sleep(5000);

		getDriver().findElement(expandAllBttn).click();
		Thread.sleep(2000);
//			getDriver().findElement(scanAWBInput).clear();
		Thread.sleep(5000);
		String trimmedAWB = awbBookingGenerated.trim();
//			getDriver().findElement(scanAWBInput).sendKeys(trimmedAWB);
//			$(scanAWBInput).waitUntilVisible().waitUntilPresent().waitUntilClickable().clear();
//			Thread.sleep(5000);
//			$(scanAWBInput).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
//			Thread.sleep(5000);
//			$(scanAWBInput).waitUntilVisible().sendKeys(trimmedAWB.trim());
//			System.out.println("Script input"+$(scanAWBInput).getTextValue());
//			System.out.println("My Input"+trimmedAWB);
//			Thread.sleep(5000);
		WebElementFacade abwNumber = $(By.id("scanAWB")).waitUntilPresent().waitUntilVisible().waitUntilEnabled()
				.waitUntilClickable();
		for (int i = 0; i <= awbBookingGenerated.split("").length - 1; i++) {
			Thread.sleep(25);
			abwNumber.sendKeys(awbBookingGenerated.split("")[i]);
		}
		Thread.sleep(6000);
	
//		$(By.id("scanAWB")).waitUntilClickable().typeAndEnter(trimmedAWB);
//		getDriver().findElement(numberOfPhysicalInput).sendKeys(numberOfPhysicalINvoiceInput);
		WebElementFacade noofphyinvoice = $(numberOfPhysicalInput).waitUntilPresent().waitUntilVisible().waitUntilEnabled()
				.waitUntilClickable();
		for (int i = 0; i <= numberOfPhysicalINvoiceInput.split("").length - 1; i++) {
			Thread.sleep(25);
			noofphyinvoice.sendKeys(numberOfPhysicalINvoiceInput.split("")[i]);
		}
		Thread.sleep(6000);
		getDriver().findElement(saveButton).click();
		Thread.sleep(2000);

		getDriver().findElement(scanMPSInput).sendKeys(awbBookingGenerated);
		Thread.sleep(500);
		String trimmedAWB1 = awbBookingGenerated.trim();
//			getDriver().findElement(scanAWBInput).sendKeys(trimmedAWB);
//			$(scanAWBInput).waitUntilVisible().waitUntilPresent().waitUntilClickable().clear();
//			Thread.sleep(5000);
//			$(scanAWBInput).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
//			Thread.sleep(5000);
//			$(scanAWBInput).waitUntilVisible().sendKeys(trimmedAWB1.trim());
//			
	}

	public void userAddsShipmentDetailsFilter() throws InterruptedException {
		Thread.sleep(5000);
		getDriver().findElement(expandAllSecondButon).click();
		Thread.sleep(2000);
		String filteredRecordsTxt = getDriver().findElement(filteredRecordsLabelLocator).getText();
		System.out.println(filteredRecordsTxt + "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		String numberOnly = filteredRecordsTxt.replaceAll("\\D+", "");
		int totalfilteredRecordrecordsNumerical = Integer.parseInt(numberOnly);
		System.out.println(getDriver().findElement(By.xpath("//table[1]/tbody[1]/tr[1]/td[2]/span")).getText()
				+ ">>>>>>>>>>>>>>>>>>>>>>>>");
		Thread.sleep(3000);
		getDriver().findElement(filtericonLocator).click();
		Thread.sleep(3000);
		getDriver().findElement(searchAWBLocator).sendKeys(awbBookingGenerated);
		Thread.sleep(3000);
		getDriver().findElement(searchButtonLocator).click();
		Thread.sleep(7000);
		getDriver().findElement(selectIconLocator).click();
		Thread.sleep(3000);

	}

	public void userClicksOnSaveButtonCreateTripTab() throws InterruptedException {
		Thread.sleep(1000);
		getDriver().findElement(assignBAVehicleSavebttn).click();
	}

	public void validateErrorMessages(String errorAssignedTo, String errorContractedError) {
		softAssert.assertEquals(
				($(By.xpath("//span[contains(text(),'test')]".trim().replace("test", errorAssignedTo)))
						.waitUntilVisible().getText()),
				errorAssignedTo, "errorAssignedTo error Message assertion failed");
		softAssert.assertEquals(
				($(By.xpath("//span[contains(text(),'test')]".trim().replace("test", errorContractedError)))
						.waitUntilVisible().getText()),
				errorContractedError, "errorContractedError Message assertion failed");
		softAssert.assertAll();
	}

	public void userClosesTripFormDB(String baSR) throws InterruptedException, SQLException, ClassNotFoundException {
		Thread.sleep(10000);
		$(filterButton).waitUntilVisible().waitUntilClickable().click();
		$(resetButton).waitUntilVisible().waitUntilClickable().click();
		Thread.sleep(5000);
		$(basrDropdown).waitUntilVisible().waitUntilClickable().click();
		Thread.sleep(2000);
		List<WebElement> basrList = getDriver().findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));

		for (int i = 0; i <= basrList.size() - 1; i++) {
			System.out.println(basrList.get(i).getText());
			if (basrList.get(i).getText().equalsIgnoreCase(baSR)) {
				basrList.get(i).click();
				break;
			}
		}
		$(searchButton).waitUntilVisible().waitUntilClickable().click();

		String tripStatus = $(tripStatusLocator).waitUntilVisible().getText();
		if (tripStatus.equals("Created") || tripStatus.equals("Started")) {

			closureTripID = $(tripIDCosureLocator).waitUntilVisible().waitUntilPresent().getText();
			System.out.println(closureTripID + "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			dbConnectivityAndTripIDFetch.dbConnection(closureTripID);
			tripClosure.closeTripAPI();
		} else {
			getDriver().navigate().refresh();
		}

	}

	public void userClicksOnAssignAWBandRemoveAWBButton() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
//			Thread.sleep(30000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(expandAllSecondButon));
//			Thread.sleep(7000);
		getDriver().findElement(expandAllSecondButon).click();
		$(assignToTheTripButtonLocator).waitUntilVisible().waitUntilClickable().click();
		Actions actions = new Actions(getDriver());
		actions.moveToElement($(removeAWBButton)).click().perform();
//			$(removeAWBButton).waitUntilVisible().waitUntilClickable().click();
	}

	public void validateErrorDetailsAssignAndRemoveTrip(String errorMessage) {
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		softAssert
				.assertEquals(($(By.xpath("(//span[contains(text(),'test')])[1]".trim().replace("test", errorMessage)))
						.waitUntilVisible().getText()), errorMessage, "error Message assertion failed");
//			softAssert.assertEquals(($(By.xpath("(//span[contains(text(),'test')])[2]".trim().replace("test", errorMessage))).waitUntilVisible().getText()),errorMessage,"error Message assertion failed");
//			je.executeScript("arguments[0].scrollIntoView(true);",$(By.xpath("(//span[contains(text(),'test')])[2]".trim().replace("test", errorMessage))));

		softAssert.assertAll();
	}

	public void userClicksOnStartButtonWithoutEnteringODO() {
		$(startTripButtonLocator).waitUntilVisible().waitUntilClickable().click();
	}

	public void validateErrorMessage(String errorMessage) {
		softAssert
				.assertEquals(($(By.xpath("(//span[contains(text(),'test')])[1]".trim().replace("test", errorMessage)))
						.waitUntilVisible().getText()), errorMessage, "error Message assertion failed");
		softAssert.assertAll();
	}

	public void userNavigatesToCreateTripAndStartsTheTrip() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(LstMile));
		getDriver().findElement(LstMile).click();
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnNotAdded));
		getDriver().findElement(BtnNotAdded).click();
		System.out.println("Click on not added to trip");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnCollapLM));
		getDriver().findElement(BtnCollapLM).click();
		Thread.sleep(1500);
		System.out.println("Click on filter button");
		wait.until(ExpectedConditions.visibilityOfElementLocated(IpAwb2));
		Thread.sleep(1500);
		getDriver().findElement(IpAwb2).sendKeys(awbBookingGenerated);
		System.out.println("Enter AWB ");
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnSearch1));
		Thread.sleep(1500);
		getDriver().findElement(BtnSearch1).click();
		System.out.println("Clicked on search ");
		wait.until(ExpectedConditions.visibilityOfElementLocated(Checkbox));
		Thread.sleep(3000);
		getDriver().findElement(Checkbox).click();
		System.out.println("Check box checked");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnAssign));
		getDriver().findElement(BtnAssign).click();
		System.out.println("Assign to trip ");
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnCollapLM));
		getDriver().findElement(BtnCollapLM).click();
		System.out.println("Clicked on Filter to close dropdown ");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(StartTrip));
		getDriver().findElement(StartTrip).click();
		System.out.println("Clicked on Start trip");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(LmTab));
		getDriver().findElement(LmTab).click();
		System.out.println("Clicked on Last mile tab");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(OdoIn));
		getDriver().findElement(OdoIn).sendKeys("1");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(AwbDoc));
		getDriver().findElement(AwbDoc).click();
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(ScanDoc));
		System.out.println("First drop down click ");
		getDriver().findElement(ScanDoc).sendKeys(awbBookingGenerated);
		System.out.println("Scan Document");
		Thread.sleep(5000);
		//////////////////////////////////////////////////////////////////////////////
		getDriver().findElement(MpsTab).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(MpsAwbScan));
		System.out.println("Clicked on MPS scan tab ");
		Thread.sleep(1500);
		getDriver().findElement(MpsAwbScan).sendKeys(awbBookingGenerated);
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(StartTrip));
		getDriver().findElement(StartTrip).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(StartClose));
		getDriver().findElement(StartClose).click();
	}

	public void userNavigatesToCloseTrip() throws InterruptedException {
		getDriver().navigate().refresh();
		Thread.sleep(3000);
		mobileDeliveryApiJava.viewTripDelivery();
		Thread.sleep(3000);
		$(FilterBtn).waitUntilPresent().waitUntilVisible().waitUntilClickable().click();
		// Send AWB
		getDriver().findElement(By.xpath("//input[@id='tripid']")).sendKeys(tripIDNumberWeb);
		Thread.sleep(1000);
		getDriver().findElement(BtnSearch).click();
		// Click on View
		getDriver().findElement(BtnView).click();
		Thread.sleep(1000);
		$(By.xpath("//button[contains(text(),'Last Mile')]")).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
		Thread.sleep(1000);
		getDriver().findElement(BtnClose).click();
		Thread.sleep(1000);
		$(By.xpath("//a[contains(text(),'Last Mile')]")).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
		Thread.sleep(1000);
		getDriver().findElement(BtnExpand).click();
		// Scan MPS-AWB
		getDriver().findElement(By.xpath("//input[@id='scanDeliveredAWB']")).sendKeys(awbBookingGenerated);
		
		$(By.xpath("//label[contains(text(),'Good')]")).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
		$(By.xpath("//button[contains(text(),'Save and Next')]")).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
		getDriver().findElement(IpOdo).sendKeys("300");
		getDriver().findElement(BtnCloseTrip).click();
		Thread.sleep(1000);
		getDriver().findElement(BtnUndn).click();
		Thread.sleep(1000);
		getDriver().findElement(BtnCloseOk).click();
	}

	public void validateTheVehicleCapacity(String vehicleCapacity) {
		softAssert
				.assertEquals(
						($(By.xpath("//span[contains(text(),'test')]".trim().replace("test", vehicleCapacity)))
								.waitUntilVisible().getText()),
						vehicleCapacity, "Vehicle capacity  Message assertion failed");
		softAssert.assertAll();
	}

	public void userAssignsAWBNumberToTripOverload() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(expandAllSecondButon));
		Thread.sleep(2000);
		getDriver().findElement(assignToTheTripButtonLocator).click();
		Thread.sleep(2000);
	}

	public void validateErrorMessageOverload(String errorMessage) {
		softAssert
				.assertEquals(
						($(By.xpath("//div[contains(text(),'test')]".trim().replace("test", errorMessage)))
								.waitUntilVisible().getText()),
						errorMessage, "Vehicle capacity  Message assertion failed");
		softAssert.assertAll();
	}
	
	public void userEntersDetailsInAddShipmentTabAWB() throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(expandAllSecondButon));
		Thread.sleep(3000);
		getDriver().findElement(expandAllSecondButon).click();
		Thread.sleep(2000);
		$(BtnFilter).waitUntilEnabled().click();
		
		for(String awb: BookingAPI.listofAwb ) {
		$(IpSearchAwb).waitUntilPresent().waitUntilVisible().waitUntilClickable().clear();
		$(IpSearchAwb).waitUntilPresent().waitUntilVisible().waitUntilClickable().sendKeys(awb);
		$(BtnSearchAwb).waitUntilPresent().waitUntilVisible().waitUntilClickable().click();
		Thread.sleep(3000);
		$(BtncheckAwb).click();
	}
	}
	
public void userScansAllAWBNumberAndValidateSucessMessage(String message) throws InterruptedException {
		
		
		///////////////////////////////////////////////////////////////////
			for(String awbToscan: BookingAPI.listofAwb ) {
		$(scanDocumentLocator).typeAndEnter(awbToscan);
		Thread.sleep(5000);
		$(scanDocumentLocator).clear();
			}//ApiJasonUtils.listofAwb.clear();
		 Thread.sleep(5000);
		 getDriver().findElement(startTripButtonLocator).click();
		 Thread.sleep(5000);
		 
		 softAssert.assertEquals(getDriver().findElement(By.xpath("//div[contains(text(),'ABC')]".replace("ABC", message))).getText(), message);
		 softAssert.assertAll();
		 Thread.sleep(2000);
		 
		 getDriver().findElement(Btn_OKBA).click();
		
	}
public void CloseTheAssignedTrips() throws InterruptedException {
	getDriver().navigate().refresh();
	Thread.sleep(20000);
//	$(FilterBtn).waitUntilPresent().waitUntilVisible().waitUntilClickable().click();
	getDriver().findElement(FilterBtn).click();
	$(IpTripId).clear();
	$(IpTripId).typeAndEnter(Cargo_FLM_TripManagement.tripIDNumberWeb);
	//$(IpTripId).typeAndEnter("23867");
	$(IpTripId).clear();
	//$(IpTripId).typeAndEnter("23867");
	$(IpTripId).typeAndEnter(Cargo_FLM_TripManagement.tripIDNumberWeb);
	Thread.sleep(2000);
	getDriver().findElement(BtnSearch).click();
	Thread.sleep(2000);
	getDriver().findElement(ViewBtn).click();
	Thread.sleep(3000);
	$(BtnClose).waitUntilVisible().waitUntilClickable().click();
	Thread.sleep(2000);
	$(ExpandAllBtn).waitUntilVisible().waitUntilClickable().click();

		for(int j=0; j<=BookingAPI.listofAwb.size()-1; j++){
			$(scanAWBInput).clear();
			$(numberOfPhysicalInput).clear();
		WebElementFacade abwNumber = $(By.id("scanAWB")).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable();
		String[] awbNumber = BookingAPI.listofAwb.get(j).split("");
		for(int i =0; i<=awbNumber.length-1; i++) {
			Thread.sleep(35);
			abwNumber.sendKeys(awbNumber[i]);
		}$(numberOfPhysicalInput).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().sendKeys("1");
		$(BtnInscanSave).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
		//$(TabMpsInscan).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
		$(scanMPSInput).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().sendKeys(BookingAPI.listofAwb.get(j));
		Thread.sleep(5000);
		
	}
	$(vehicleCloseODOReadingInput).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().sendKeys("300");
	$(BtnClose).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
	$(Btn_OKBA).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
	Thread.sleep(2000);
	$(Btn_OKBA).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
	
	
	
}
public void userClickOnLastMileTabAndEnterMultipleAwbStartTheLastMileTrip() throws InterruptedException {
	try {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 100);
	Thread.sleep(3000);
	wait.until(ExpectedConditions.visibilityOfElementLocated(LstMile));
	getDriver().findElement(LstMile).click();
	Thread.sleep(6000);
	wait.until(ExpectedConditions.visibilityOfElementLocated(BtnNotAdded));
	getDriver().findElement(BtnNotAdded).click();
	System.out.println("Click on not added to trip");
	Thread.sleep(1500);
	wait.until(ExpectedConditions.visibilityOfElementLocated(BtnCollapLM));
	getDriver().findElement(BtnCollapLM).click();
	Thread.sleep(1500);
	System.out.println("Click on filter button");
	wait.until(ExpectedConditions.visibilityOfElementLocated(IpAwb2));
	Thread.sleep(1500);
	//getDriver().findElement(IpAwb2).sendKeys(awb);
	
	
	
	for(String awbToscan: BookingAPI.listofAwb ) {
		$(IpAwb2).clear();
		$(IpAwb2).typeAndEnter(awbToscan);
		Thread.sleep(5000);
		$(BtnSearch1).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
		$(Checkbox).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
			}
	
//	for(int j=0; j<=ApiJasonUtils.listofAwb.size()-1; j++){
//		
//		$(IpAwb2).clear();
//		WebElementFacade abwNumber = $(By.id("IpAwb2"));
//				//.waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable();
//		String[] awbNumber = ApiJasonUtils.listofAwb.get(j).split("");
//		for(int i =0; i<=awbNumber.length-1; i++) {
//			Thread.sleep(35);
//			abwNumber.sendKeys(awbNumber[i]);
//		}$(BtnSearch1).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
//		$(Checkbox).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
//
//	}
	wait.until(ExpectedConditions.visibilityOfElementLocated(BtnAssign));
	getDriver().findElement(BtnAssign).click();
	Thread.sleep(5000);
//	wait.until(ExpectedConditions.visibilityOfElementLocated(BtnCollapLM));
//	Thread.sleep(5000);
//	getDriver().findElement(BtnCollapLM).click();
//	System.out.println("Clicked on Filter to close dropdown ");
//	Thread.sleep(1500);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(StartTrip));
	getDriver().findElement(StartTrip).click();
	System.out.println("Clicked on Start trip");
	Thread.sleep(1500);
	wait.until(ExpectedConditions.visibilityOfElementLocated(LmTab));
	getDriver().findElement(LmTab).click();
	System.out.println("Clicked on Last mile tab");
	Thread.sleep(1500);
	wait.until(ExpectedConditions.visibilityOfElementLocated(OdoIn));
	getDriver().findElement(OdoIn).sendKeys("1");
	Thread.sleep(1500);
	wait.until(ExpectedConditions.visibilityOfElementLocated(BtnExpandAll));
	getDriver().findElement(BtnExpandAll).click();
	Thread.sleep(2000);
	try {
		
		for(int j=0; j<=BookingAPI.listofAwb.size()-1; j++){
			$(ScanDoc).clear();
			WebElementFacade abwNumber1 = $(ScanDoc);
					//$(By.id("ScanDoc"));
					//.waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable();
			String[] awbNumber = BookingAPI.listofAwb.get(j).split("");
			for(int i =0; i<=awbNumber.length-1; i++) {
				Thread.sleep(45);
				abwNumber1.waitUntilVisible().waitUntilPresent().waitUntilClickable().sendKeys(awbNumber[i]);
			}$(MpsAwbScan).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().sendKeys(BookingAPI.listofAwb.get(j));

		}
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	$(FlashMsg).waitUntilNotVisible();
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Start Trip')]")));
	getDriver().findElement(By.xpath("//button[contains(text(),'Start Trip')]")).click();
	Thread.sleep(2000);
	wait.until(ExpectedConditions.visibilityOfElementLocated(StartClose));
	getDriver().findElement(StartClose).click();

} catch (Exception e) {
	e.printStackTrace();
	e.printStackTrace();

}
	

}
public void searchForAssignedTripAndClickOnViewTripThenClickOnCloseTripButton() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(getDriver(), 100);
	getDriver().navigate().refresh();
	Thread.sleep(20000);
	getDriver().findElement(FilterBtn).click();
	// Send AWB
	Thread.sleep(3000);
	//wait.until(ExpectedConditions.visibilityOfElementLocated(IpTripId));
	$(IpTripId).clear();
	getDriver().findElement(IpTripId).sendKeys(Cargo_FLM_TripManagement.tripIDNumberWebLM);
//	$(IpTripId).click();
//	$(IpTripId).sendKeys("23723");
	Thread.sleep(2000);
	wait.until(ExpectedConditions.visibilityOfElementLocated(BtnSearch));
	$(BtnSearch).click();
	// Click on View 
	Thread.sleep(2000);
	wait.until(ExpectedConditions.visibilityOfElementLocated(ViewBtn));
	getDriver().findElement(ViewBtn).click();
	Thread.sleep(5000);
	$(By.xpath("//button[contains(text(),'Last Mile')]")).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
	Thread.sleep(1000);
	getDriver().findElement(BtnClose).click();
//	$(LmTab).waitUntilVisible().waitUntilClickable().click();
//	$(BtnCloseTrip).waitUntilVisible().waitUntilClickable().click();
	wait.until(ExpectedConditions.visibilityOfElementLocated(IpOdo));
	getDriver().findElement(IpOdo).sendKeys("332");
	$(LmTab1).waitUntilVisible().waitUntilClickable().click();
	$(UnDeliveAwb).waitUntilVisible().waitUntilClickable().click();
	$(SubTabExpandAll).waitUntilVisible().waitUntilClickable().click();
	
	for(int j=0; j<=BookingAPI.listofAwb.size()-1; j++){
		$(ScnDoc).clear();
		WebElementFacade abwNumber1 = $(ScnDoc);
		String[] awbNumber = BookingAPI.listofAwb.get(j).split("");
		for(int i =0; i<=awbNumber.length-1; i++) {
			Thread.sleep(25);
			abwNumber1.sendKeys(awbNumber[i]);
			//$(IpAwb2).clear();
			
		}$(InvcNo).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().sendKeys("1");
		$(Svbtn).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
		Thread.sleep(5000);
		$(ScnAwb).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().sendKeys(BookingAPI.listofAwb.get(j));
		Thread.sleep(5000);
		}
	$(BtnCloseTrip).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
	$(BtnUndn).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
	Thread.sleep(2000);
	$(BtnCloseOk).waitUntilPresent().waitUntilVisible().waitUntilEnabled().waitUntilClickable().click();
	
}


	
}
