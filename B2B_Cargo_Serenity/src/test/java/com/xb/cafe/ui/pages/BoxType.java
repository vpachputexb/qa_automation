package com.xb.cafe.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;

public class BoxType extends PageObject {

	public final By masterMenu = By.xpath("//span[contains(text(),'Master Data')]");
	public final By boxtypemenu = By.xpath("//span[contains(text(),'Box Types')]");
	public final By boxtypelisingheading = By.xpath("//h4[contains(text(),'Box Type List')]");
	public final By filtericon = By.xpath("//*[@title='Search Filter']");
	public final By createbtn = By.xpath("//*[@title='Add New Box Type']//i");
	public final By boxtypetb_hdr = By.xpath("//th[contains(text(),'Box Type')]");
	public final By boxtypeDescrtb_hdr = By.xpath("//th[contains(text(),'Box Type Description')]");
	public final By stackingleveltb_hdr = By.xpath("//th[contains(text(),'Stacking Level')]");
	public final By boxstrengthtb_hdr = By.xpath("//th[contains(text(),'Box Strength')]");
	public final By minwttb_hdr = By.xpath("//th[contains(text(),'Min Weight(kg)')]");
	public final By maxwttb_hdr = By.xpath("//th[contains(text(),'Max Weight(kg)')]");
	public final By createdbytb_hdr = By.xpath("//th[contains(text(),'Created By')]");
	public final By createddatetb_hdr = By.xpath("//th[contains(text(),'Created Date')]");
	public final By lastmodifiedbytb_hdr = By.xpath("//th[contains(text(),'Last Modified By')]");
	public final By lastmodifieddatetb_hdr = By.xpath("//th[contains(text(),'Last Modified Date')]");
	public final By statustb_hdr = By.xpath("//th[contains(text(),'Status')]");
	public final By actiontb_hdr = By.xpath("//th[contains(text(),'Action')]");
	public final By createboxtypepageheading = By.xpath("(//label[contains(text(),'Box Type')])[1]");

	public final By savebtn = By.xpath("//button[@type='submit']");
	public final By backbtn = By.xpath("//*[@title='Back to Box Types List']/i");
	public final By inp_boxtype = By.xpath("//input[@id='BoxType']");
	public final By inp_stackinglevel = By.xpath("//input[@id='StackingLevel']");
	public final By inp_BoxStrength = By.xpath("//input[@id='BoxStrength']");
	public final By inp_Description = By.xpath("//textarea[@id='Description']");
	public final By inp_MinimumWeight = By.xpath("//input[@id='MinimumWeight']");
	public final By inp_MaximumWeight = By.xpath("//input[@id='MaximumWeight']");

	public final By boxType_filterfield = By.xpath("//input[@name='BoxType']");
	public final By resetBtn = By.xpath("//button[@type='reset']");
	public final By bt = By.xpath("//input[@placeholder='Enter BoxType']");
	public final By totalrecords = By.xpath("//b[contains(text(),'Total Records: 1')]");
	public final By searchbtn = By.xpath("//button[@class='-btn btn-success']");
	public final By statusfilterfield = By.xpath("//input[@id='status']");

	public final By successmsg = By.xpath("//div[@id='swal2-content']");
	public final By okbtn = By.xpath("//button[contains(text(),'OK')]");
	public final By statusoncreation = By.xpath("//button[contains(text(),'Inactive')]");
	public final By editBoxtype = By.xpath("//span[@title='Edit']");
	public final By confirmationMsg = By.xpath("//div[@id='swal2-content']");
	public final By popupOkBtn = By.xpath("//button[contains(text(),'OK')]");
	public final By popupCancelBtn = By.xpath("//button[contains(text(),'Cancel')]");
	public final By boxtypeactivatedMsg = By.xpath("//div[@id='swal2-content']");
	public final By okbtnOnActivation = By.xpath("//button[contains(text(),'OK')]");
	public final By inactivebtn = By.xpath("(//button[@class='common-danger'])[1]");

	public final By boxtypeblnkValidationmsg = By.xpath("//span[contains(text(),'The BoxType field is required')]");
	public final By stackinglevelValidationmsg=By.xpath("//span[contains(text(),'The StackingLevel field is required')]");
	public final By boxStregthValidationmsg=By.xpath("//span[contains(text(),'The BoxStrength field is required')]");
	public final By descValidationmsg=By.xpath("//span[contains(text(),'The Description field is required')]");
	public final By minwtValidationmsg=By.xpath("//span[contains(text(),'The MinimumWeight field is required')]");
	public final By maxwtValidationmsg=By.xpath("//span[contains(text(),'The MaximumWeight field is required')]");
	
	SoftAssert softAssert = new SoftAssert();

	public void navigateToMasterMenu() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(masterMenu).click();

	}

	public void boxTypeMenuVisibility() throws InterruptedException {
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(boxtypemenu));
			getDriver().findElement(boxtypemenu).click();

		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(masterMenu).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(boxtypemenu));
			softAssert.assertTrue(getDriver().findElement(boxtypemenu).isDisplayed(), "Box type menu is displayed");
		}

	}

	public void clickBoxType() throws InterruptedException {
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(boxtypemenu));
			getDriver().findElement(boxtypemenu).click();

		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(masterMenu).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(boxtypemenu));
			getDriver().findElement(boxtypemenu).click();
		}
	}

	public void landedOnBoxType() throws InterruptedException {
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(boxtypelisingheading).isDisplayed(), "Landed on Box type page");
	}

	public void listingElements(String BoxType, String BoxTypeDescription, String StackingLevel, String BoxStrength,
			String MinWeight, String MaxWeight, String CreatedBy, String CreatedDate, String LastModifiedBy,
			String LastModifiedDate, String Status, String Action) throws InterruptedException {
		Thread.sleep(3000);
		String bt = getDriver().findElement(boxtypetb_hdr).getText();
		softAssert.assertEquals(bt, BoxType, "Box Type header is displayed");
		softAssert.assertAll();
		String btd = getDriver().findElement(boxtypeDescrtb_hdr).getText();
		softAssert.assertEquals(btd, BoxTypeDescription, "BoxType Description header is displayed");
		softAssert.assertAll();
		String sl = getDriver().findElement(stackingleveltb_hdr).getText();
		softAssert.assertEquals(sl, StackingLevel, "Stacking Level header is displayed");
		softAssert.assertAll();
		String bs = getDriver().findElement(boxstrengthtb_hdr).getText();
		softAssert.assertEquals(bs, BoxStrength, "Box Strength header is displayed");
		softAssert.assertAll();
		String mw = getDriver().findElement(minwttb_hdr).getText();
		softAssert.assertEquals(mw, MinWeight, "Min Weight header is displayed");
		softAssert.assertAll();
		String maxw = getDriver().findElement(maxwttb_hdr).getText();
		softAssert.assertEquals(maxw, MaxWeight, "Max Weight header is displayed");
		softAssert.assertAll();
		String cb = getDriver().findElement(createdbytb_hdr).getText();
		softAssert.assertEquals(cb, CreatedBy, "Created By header is displayed");
		softAssert.assertAll();
		String cd = getDriver().findElement(createddatetb_hdr).getText();
		softAssert.assertEquals(cd, CreatedDate, "Created Date header is displayed");
		softAssert.assertAll();
		String lmb = getDriver().findElement(lastmodifiedbytb_hdr).getText();
		softAssert.assertEquals(lmb, LastModifiedBy, "Last Modified By header is displayed");
		softAssert.assertAll();
		String lmd = getDriver().findElement(lastmodifieddatetb_hdr).getText();
		softAssert.assertEquals(lmd, LastModifiedDate, "LastModified Date header is displayed");
		softAssert.assertAll();
		String sts = getDriver().findElement(statustb_hdr).getText();
		softAssert.assertEquals(sts, Status, "Status header is displayed");
		softAssert.assertAll();
		String act = getDriver().findElement(actiontb_hdr).getText();
		softAssert.assertEquals(act, Action, "Action header is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(filtericon).isDisplayed(), "Search filter is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(createbtn).isDisplayed(), "Create button is displayed");
		softAssert.assertAll();

	}

	public void clickOnCreateBtn() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(createbtn).click();
	}

	public void createBoxTypePage(String CreateBoxtypeheading) throws InterruptedException {
		Thread.sleep(3000);
		String txt = getDriver().findElement(createboxtypepageheading).getText();
		softAssert.assertEquals(txt, CreateBoxtypeheading, "Page heading is as expected");
		softAssert.assertAll();
	}

	public void createBoxTypePageElements() throws InterruptedException {
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(inp_boxtype).isDisplayed(), "Box type input field is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(inp_stackinglevel).isDisplayed(),
				"Stacking level input field is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(inp_BoxStrength).isDisplayed(),
				"Box Strength input field is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(inp_MinimumWeight).isDisplayed(),
				"Minimum weigth input field is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(inp_MaximumWeight).isDisplayed(),
				"Maximum Weight input field is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(inp_Description).isDisplayed(),
				"Description input field is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(savebtn).isDisplayed(), "Save button is displayed");
		softAssert.assertAll();
	}

	public void clickonSearchFilterIcon() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(filtericon).click();
	}

	public void enterBoxTypeIntoSearchFilter(String BoxType) {
		getDriver().findElement(boxType_filterfield).sendKeys(BoxType);
	}

	public void clickOnResetBtn() {
		getDriver().findElement(resetBtn).click();
	}

	public void checkBoxTypeValue(String Value) {
		String txt = getDriver().findElement(boxType_filterfield).getText();
		softAssert.assertEquals(txt, Value, "Box type is reset");
		softAssert.assertAll();
	}

	public void clickOnSearchBtn() throws InterruptedException {
		getDriver().findElement(searchbtn).click();
		Thread.sleep(3000);
	}

	public void searchResult(String Result) {
		String txt = getDriver().findElement(totalrecords).getText();
		softAssert.assertEquals(txt, Result, "Result displayed as expected");
		softAssert.assertAll();
	}

	public void searchElements() throws InterruptedException {
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(boxType_filterfield).isDisplayed(),
				"Box type field is displayed in filter");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(statusfilterfield).isDisplayed(),
				"Status field is displayed in filter");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(searchbtn).isDisplayed(), "Search button is displayed in filter");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(resetBtn).isDisplayed(), "Reset button is displayed in filter");
		softAssert.assertAll();
	}

	public void enterDataToCreateBoxType(String BoxType, String StackingLevel, String BoxStrength, String Description,
			String Minimumwt, String Maxwt) throws InterruptedException {
		getDriver().findElement(inp_boxtype).sendKeys(BoxType);
		getDriver().findElement(inp_stackinglevel).click();
		List<WebElement> stackinglevel = getDriver().findElements(By.xpath("//div[@class='menu visible']//div"));
		Thread.sleep(3000);

		for (int i = 0; i <= stackinglevel.size() - 1; i++) {
			System.out.println(stackinglevel.get(i).getText());
			if (stackinglevel.get(i).getText().equalsIgnoreCase(StackingLevel)) {
				stackinglevel.get(i).click();
				break;
			}

		}
		Thread.sleep(3000);
		getDriver().findElement(inp_BoxStrength).click();
		List<WebElement> BoxSt = getDriver()
				.findElements(By.xpath("(//div[@class='col-md-4'])[3]//div//div//div//div"));
		Thread.sleep(3000);

		for (int i = 0; i <= BoxSt.size() - 1; i++) {
			System.out.println(BoxSt.get(i).getText());
			if (BoxSt.get(i).getText().equalsIgnoreCase(BoxStrength)) {
				BoxSt.get(i).click();
				break;
			}

		}
		getDriver().findElement(inp_Description).sendKeys(Description);
		getDriver().findElement(inp_MinimumWeight).sendKeys(Minimumwt);
		getDriver().findElement(inp_MaximumWeight).sendKeys(Maxwt);

	}

	public void clickOnSaveBtn() throws InterruptedException {
		getDriver().findElement(savebtn).click();
		Thread.sleep(3000);
	}

	public void successMSG(String SuccMsg) {
		String txt = getDriver().findElement(successmsg).getText();
		softAssert.assertEquals(txt, SuccMsg, "Success msg displayed");
		softAssert.assertAll();
		getDriver().findElement(okbtn).click();
	}

	public void enterBxType3(String Boxtype3) {

		getDriver().findElement(boxType_filterfield).sendKeys(Boxtype3);
	}

	public void checkStatus(String Status) {

		String txt = getDriver().findElement(statusoncreation).getText();
		softAssert.assertEquals(txt, Status, "status is inactive on creation");
		softAssert.assertAll();

	}

	public void clickOnEditBtn() throws InterruptedException {
		Thread.sleep(3000);

		getDriver().findElement(editBoxtype).click();
	}

	public void messageOnUpdatedBoxType(String UpdatedMsg) throws InterruptedException {

		Thread.sleep(3000);
		String txt = getDriver().findElement(successmsg).getText();
		softAssert.assertEquals(txt, UpdatedMsg, "Message displayed as expected");
		softAssert.assertAll();
		getDriver().findElement(okbtn).click();
	}

	public void selectInactiveBoxType(String Status2) throws InterruptedException {
		getDriver().findElement(statusfilterfield).click();

		List<WebElement> stats = getDriver().findElements(By.xpath("//div[@class='menu visible']/div"));
		Thread.sleep(3000);

		for (int i = 0; i <= stats.size() - 1; i++) {
			System.out.println(stats.get(i).getText());
			if (stats.get(i).getText().equalsIgnoreCase(Status2)) {
				stats.get(i).click();
				break;
			}

		}

	}

	public void clickOnInactiveStatusBoxType() {
		getDriver().findElement(inactivebtn).click();
	}

	public void confirmationMsgPopup(String ConfirmationMsg) throws InterruptedException {
		Thread.sleep(3000);
		String txt = getDriver().findElement(confirmationMsg).getText();
		softAssert.assertEquals(txt, ConfirmationMsg, "Confirmation popup is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(popupOkBtn).isDisplayed(), "Ok button is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(popupCancelBtn).isDisplayed(), "Cancel button is displayed");
		softAssert.assertAll();
		getDriver().findElement(popupOkBtn).click();
		Thread.sleep(3000);
	}

	public void statusChangedCheck(String MSG) {
		String txt = getDriver().findElement(boxtypeactivatedMsg).getText();
		softAssert.assertEquals(txt, MSG, "Box type activated");
		softAssert.assertAll();
		getDriver().findElement(okbtnOnActivation).click();
	}

	public void enterBlankBoxType(String Boxtype3) {
		getDriver().findElement(inp_boxtype).sendKeys(Boxtype3);
	}

	public void validationMsg(String validationmsg) {
		String txt = getDriver().findElement(boxtypeblnkValidationmsg).getText();
		softAssert.assertEquals(txt, validationmsg, "Validation message displayed on blank box type");
		softAssert.assertAll();
	}

	public void validationMsg(String ValidationMsgStackingLevel, String ValidationMsgBoxStrength,
			String ValidationMsgBoxTypeDesc, String ValidationMsgminwt, String ValidationMsgmaxwt) {
		String txt1 = getDriver().findElement(stackinglevelValidationmsg).getText();
		softAssert.assertEquals(txt1, ValidationMsgStackingLevel, "Stacking level validation message is displayed");
		softAssert.assertAll();
		String txt2 = getDriver().findElement(boxStregthValidationmsg).getText();
		softAssert.assertEquals(txt2, ValidationMsgBoxStrength, "Box strength validation message is displayed");
		softAssert.assertAll();
		String txt3 = getDriver().findElement(descValidationmsg).getText();
		softAssert.assertEquals(txt3, ValidationMsgBoxTypeDesc, "Validatio message on description is displayed");
		softAssert.assertAll();
		String txt4 = getDriver().findElement(minwtValidationmsg).getText();
		softAssert.assertEquals(txt4, ValidationMsgminwt, "Validation message on min wt is displayed");
		softAssert.assertAll();
		String txt5 = getDriver().findElement(maxwtValidationmsg).getText();
		softAssert.assertEquals(txt5, ValidationMsgmaxwt, "Validation message on max wt is displayed");
		softAssert.assertAll();
		
		
		
	}

}
