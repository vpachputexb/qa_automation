package com.xb.cafe.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;

public class Cargo_UADShipmentSearch extends PageObject {
	public final By FLMLnk = By.xpath("//span[contains(text(),'First & Last Mile')]");
	public final By UADLnk = By.xpath("(//span[contains(text(),'UAD')])[1]");
	public final By UadShipmentSearchLnk = By.xpath("//span[contains(text(),'UAD Shipment Search')]");
	public final By tb_shippingid = By.xpath("//th[contains(text(),'Shipping ID')]");

	public final By tb_consigneephone = By.xpath("//th[contains(text(),'Consignee phone')]");
	public final By tb_consigneeemail = By.xpath("//th[contains(text(),'Consignee e-mail')]");
	public final By tb_deliverypincode = By.xpath("//th[contains(text(),'Delivery Pincode')]");
	public final By tb_deliveryaddress = By.xpath("//th[contains(text(),'Delivery Address')]");
	public final By tb_deliverydate = By.xpath("//th[contains(text(),'Delivery Date')]");
	public final By tb_radinscan = By.xpath("//th[contains(text(),'RAD Inscan')]");
	public final By tb_reschdeuledate = By.xpath("//th[contains(text(),'Re-Schedule Date')]");
	public final By tb_uadmarkedstatus = By.xpath("//th[contains(text(),'UAD Marked Status')]");
	public final By tb_uadmarkedcount = By.xpath("//th[contains(text(),'UAD Marked Count')]");
	public final By tb_action = By.xpath("//th[contains(text(),'Action')]");
	public final By Markuadbtn = By.xpath("(//button[@title='Mark UAD'])[1]");
	public final By markuadpopupheading = By.xpath("//span[contains(text(),'Mark UAD')]");
	public final By ReasonLb = By.xpath("(//*[contains(text(),'Reason')])[1]");
	public final By subReasonLb = By.xpath("(//*[contains(text(),'Sub Reason')])[1]");
	public final By RescheduleDateLb = By.xpath("//*[contains(text(),'Re-schedule Date(If applicable)')]");
	public final By submitbtn = By.xpath("//button[contains(text(),'Submit')]");
	public final By cancelbt = By.xpath("//button[contains(text(),'Cancel')]");
	public final By closebtn = By.xpath("//button[contains(text(),'×')]");
	public final By blankReasonErr = By.xpath("//span[contains(text(),'Please select reason')]");
	public final By searchFilter = By.xpath("//*[@title='Search Filter']");
	public final By MarkUADbtnDisabled = By.xpath("//button[@title='Please perform complete booking.']");
	public final By msgOnReschduledatecurrntdate = By
			.xpath("//span[contains(text(),\"Re-Schedule date should be greater than today's date\")]");

	public final By reason = By.xpath("//*[@id='reason']");
	public final By erroronblankrescheduledate = By
			.xpath("//span[contains(text(),'Re-Schedule date is mandatory for this Reason/Sub-')]");
	public final By errblanksubreason = By.xpath("//span[contains(text(),'Please select Sub-Reason')]");
	public final By rescheduleDatefield = By.xpath("//input[@name='uadrescheduledate']");
	public final By uadmarkedsuccsmsg = By.xpath("//div[@class='alertify-notifier ajs-bottom ajs-right']/div");
	public final By subreason = By.xpath("(//input[@class='vue-treeselect__input'])[2]");
	public final By searchAWBinp = By.xpath("//input[@name='AWB']");
	public final By searchbtn = By.xpath("//button[@type='submit']");
	public final By txt = By.xpath("//b[contains(text(),'Total Records: 0')]");
	public final By maxlimitdatereached = By.xpath("//div[contains(text(),'Re-Schedule date limit reached')]");
	public final By btnMarkUAD = By.xpath("//button[contains(text(),'Mark UAD')]");

	public final By status = By.xpath("//td[contains(text(),'No')]");
	public final By count = By.xpath("//tbody/tr[1]/td[11]");

	SoftAssert softAssert = new SoftAssert();

	public void NavigateFLM() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(FLMLnk).click();
	}

	public void NavigateUAD() throws InterruptedException {
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADLnk));
			getDriver().findElement(UADLnk).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(FLMLnk).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADLnk));
			getDriver().findElement(UADLnk).click();
			Thread.sleep(3000);
		}
	}

	public void UADShipmentSearchMenu() throws InterruptedException {
		Thread.sleep(5000);
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADLnk));
			getDriver().findElement(UADLnk).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(FLMLnk).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADLnk));
			getDriver().findElement(UADLnk).click();
			softAssert.assertTrue(getDriver().findElement(UadShipmentSearchLnk).isDisplayed(),
					"UAD shipment Search menu is displayed");
			getDriver().findElement(UadShipmentSearchLnk).click();
			Thread.sleep(3000);
		}
	}

	public void NaviagteUADShipmentSearch() throws InterruptedException {
		Thread.sleep(5000);
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADLnk));
			getDriver().findElement(UADLnk).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(FLMLnk).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADLnk));
			getDriver().findElement(UADLnk).click();
			Thread.sleep(5000);
			getDriver().findElement(UadShipmentSearchLnk).click();
			Thread.sleep(9000);
		}
	}

	public void ShipmentSearchListing(String ShippingID, String ConsigneePhone, String ConsigneeEmail,
			String DeliveryPincode, String DeliveryAddress, String DeliveryDate, String RADInscan,
			String RescheduleDate, String UADMarkedStatus, String UADMarkedCount, String Action)
			throws InterruptedException {
		Thread.sleep(3000);
		String shpid = getDriver().findElement(tb_shippingid).getText();
		softAssert.assertEquals(shpid, ShippingID, "Column heading is Shipping ID");
		softAssert.assertAll();
		String conphone = getDriver().findElement(tb_consigneephone).getText();
		softAssert.assertEquals(conphone, ConsigneePhone, "Column heading is Consignee phone");
		softAssert.assertAll();
		String conemail = getDriver().findElement(tb_consigneeemail).getText();
		softAssert.assertEquals(conemail, ConsigneeEmail, "Column heading is Consignee Email");
		softAssert.assertAll();
		String delipincode = getDriver().findElement(tb_deliverypincode).getText();
		softAssert.assertEquals(delipincode, DeliveryPincode, "Column heading is Delivery Pincode");
		softAssert.assertAll();
		String deliadd = getDriver().findElement(tb_deliveryaddress).getText();
		softAssert.assertEquals(deliadd, DeliveryAddress, "Column heading is Delivery Address");
		softAssert.assertAll();
		String deldate = getDriver().findElement(tb_deliverydate).getText();
		softAssert.assertEquals(deldate, DeliveryDate, "Column heading is Delivery Date");
		softAssert.assertAll();
		String radinscan = getDriver().findElement(tb_radinscan).getText();
		softAssert.assertEquals(radinscan, RADInscan, "Column heading is RAD Inscan");
		softAssert.assertAll();
		String uadmarstatus = getDriver().findElement(tb_uadmarkedstatus).getText();
		softAssert.assertEquals(uadmarstatus, UADMarkedStatus, "Column heading is UAD Marked dStatus");
		softAssert.assertAll();
		String uadmarcnt = getDriver().findElement(tb_uadmarkedcount).getText();
		softAssert.assertEquals(uadmarcnt, UADMarkedCount, "Column heading is UAD Marked Count");
		softAssert.assertAll();
		String act = getDriver().findElement(tb_action).getText();
		softAssert.assertEquals(act, Action, "Column heading is Action");
		softAssert.assertAll();

	}

	public void markUADBtn() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Markuadbtn));
		softAssert.assertTrue(getDriver().findElement(Markuadbtn).isDisplayed(), "Mark UAD button is displayed");
		softAssert.assertAll();
		Thread.sleep(5000);
	}

	public void clickMarkUADBtn() throws InterruptedException {
		// Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(getDriver(), 3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Markuadbtn));
		getDriver().findElement(Markuadbtn).click();
		Thread.sleep(3000);
	}

	public void markUadPopupDispolayed(String PopupHEading) throws InterruptedException {
		Thread.sleep(5000);
		String txt = getDriver().findElement(markuadpopupheading).getText();
		softAssert.assertEquals(txt, PopupHEading, "Popup heading is as expected");
		softAssert.assertAll();

	}

	public void CheckMarkUADPopupElements(String Reason, String Subreason, String RescheduleDate)
			throws InterruptedException {
		Thread.sleep(3000);
		String rs = getDriver().findElement(ReasonLb).getText();
		softAssert.assertEquals(rs, Reason, "Reason field is displayed");
		softAssert.assertAll();
		String subrs = getDriver().findElement(subReasonLb).getText();
		softAssert.assertEquals(subrs, Subreason, "sub reason field is displayed");
		softAssert.assertAll();
		String rsd = getDriver().findElement(RescheduleDateLb).getText();
		softAssert.assertEquals(rsd, RescheduleDate, "Reschedule date field is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(submitbtn).isDisplayed(), "Submit button is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(cancelbt).isDisplayed(), "Cancel button is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(closebtn).isDisplayed(), "Close button is displayed");
		softAssert.assertAll();
		getDriver().findElement(closebtn).click();

	}

	public void clickOnSubmitBtn() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(submitbtn).click();
		Thread.sleep(3000);
	}

	public void errorMessageBlankReason(String ReasonValidationMsg) throws InterruptedException {
		Thread.sleep(3000);
		String err = getDriver().findElement(blankReasonErr).getText();
		softAssert.assertEquals(err, ReasonValidationMsg, "Reason is blank");
		softAssert.assertAll();
		getDriver().findElement(closebtn).click();
	}

	public void clickCancelBtn() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(cancelbt).click();
	}

	public void ClosebtnFunctionality() throws InterruptedException {
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(searchFilter).isDisplayed(), "Popup closed");
		softAssert.assertAll();

	}

	public void selectUADReason(String UADReason) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(reason).click();

		List<WebElement> resn = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu-container']/div/div/div/div/div"));

		for (int i = 0; i <= resn.size() - 1; i++) {
			if (resn.get(i).getText().equals(UADReason)) {
				resn.get(i).click();
				break;
			}
		}

	}

	public void errorOnBlankRescheduleDate(String RescheduleDateerrMsg) throws InterruptedException {
		Thread.sleep(3000);
		String err = getDriver().findElement(erroronblankrescheduledate).getText();
		softAssert.assertEquals(err, RescheduleDateerrMsg, "Error is displayed");
		softAssert.assertAll();
		getDriver().findElement(closebtn).click();
	}

	public void selectOperationalReason(String OperationReason) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(reason).click();

		List<WebElement> opsn = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu-container']/div/div/div/div/div"));

		for (int i = 0; i <= opsn.size() - 1; i++) {
			if (opsn.get(i).getText().equals(OperationReason)) {
				opsn.get(i).click();
				break;
			}
		}
	}

	public void validationMessageOnBlankdata(String Subreason, String RescheduleDateerrorMsg)
			throws InterruptedException {
		Thread.sleep(3000);
		String errsub = getDriver().findElement(errblanksubreason).getText();
		softAssert.assertEquals(errsub, Subreason, "Error displayed on subreason blank");
		softAssert.assertAll();
		String errresdt = getDriver().findElement(erroronblankrescheduledate).getText();
		softAssert.assertEquals(errresdt, RescheduleDateerrorMsg, "Error displayed on blank reschedule date");
		softAssert.assertAll();
		getDriver().findElement(closebtn).click();
	}

	public void selectConsigneeReason(String UADReason1) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(reason).click();

		List<WebElement> resn1 = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu-container']/div/div/div/div/div"));

		for (int i = 0; i <= resn1.size() - 1; i++) {
			if (resn1.get(i).getText().equals(UADReason1)) {
				resn1.get(i).click();
				break;
			}
		}

	}

	public void selectRescheduleDate(String ReschdeuleDate) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(rescheduleDatefield).sendKeys(ReschdeuleDate);
	}

	public void successOnMarkUAD(String UADMarkedSucessMsg) {
		String msg = getDriver().findElement(uadmarkedsuccsmsg).getText();
		softAssert.assertEquals(msg, UADMarkedSucessMsg, "UAD marked successfully");
		softAssert.assertAll();
	}

	public void clickOnReasonDropDown() throws InterruptedException {

		Thread.sleep(3000);
		getDriver().findElement(reason).click();

	}

	public void listOfUADReasons(String UADReasons) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(reason).click();

		List<WebElement> opsn = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu-container']/div/div/div/div/div"));

		for (int i = 0; i <= opsn.size() - 1; i++) {
			getDriver().findElement(reason).click();
			if (opsn.get(i).getText().equals(UADReasons)) {
				opsn.get(i).click();
				getDriver().findElement(closebtn).click();
//				System.out.println("Reason is:" + UADReasons);
//				softAssert.assertEquals(i, UADReasons, "Reason matched");
//				softAssert.assertAll();
				break;
				// getDriver().findElement(closebtn).click();
			}
		}
	}

	public void selectOpsReason(String OpsReason, String Subreason) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(reason).click();

		List<WebElement> resn1 = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu-container']/div/div/div/div/div"));

		for (int i = 0; i <= resn1.size() - 1; i++) {
			if (resn1.get(i).getText().equals(OpsReason)) {
				resn1.get(i).click();
				break;
			}
		}

		getDriver().findElement(subreason).click();

		List<WebElement> subrs = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu-container']/div/div/div/div/div"));

		for (int i = 0; i <= subrs.size() - 1; i++) {
			if (subrs.get(i).getText().equals(Subreason)) {
				subrs.get(i).click();
				break;
			}
		}

	}

	public void selectReshDate(String ReschdeuleDate1) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(rescheduleDatefield).sendKeys(ReschdeuleDate1);
	}

	public void showSuccessMSG(String UADMarkedSucessMsg1) throws InterruptedException {
		String msg = getDriver().findElement(uadmarkedsuccsmsg).getText();
		softAssert.assertEquals(msg, UADMarkedSucessMsg1, "UAD marked successfully");
		softAssert.assertAll();
		Thread.sleep(5000);
	}

	public void opsReasondisabled(String OpsReason1) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(reason).click();

		List<WebElement> resn1 = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu-container']/div/div/div/div/div"));

		for (int i = 0; i <= resn1.size() - 1; i++) {
			if (resn1.get(i).getText().equals(OpsReason1)) {
				softAssert.assertTrue(resn1.get(i).isDisplayed(), "Reason is disabled");
				softAssert.assertAll();
				getDriver().findElement(closebtn).click();
				break;

			}
		}
	}

	public void clickOnSearchFilter() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(searchFilter).click();
	}

	public void enterDeliveredAWB(String DeliveredAWB) throws InterruptedException {
		getDriver().findElement(searchAWBinp).sendKeys(DeliveredAWB);
		getDriver().findElement(searchbtn).click();
		//Thread.sleep(3000);
	}

	public void textDisplayed(String TotalRecords) {
		WebDriverWait wait=new WebDriverWait(getDriver(), 3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(txt));
		String text = getDriver().findElement(txt).getText();
		softAssert.assertEquals(text, TotalRecords, "Text is matched");
		softAssert.assertAll();
	}

	public void enterRapidBookingAWB(String RapidAWB) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(searchAWBinp).sendKeys(RapidAWB);
		getDriver().findElement(searchbtn).click();
		Thread.sleep(3000);
	}

	public void msgOnMouseHover(String RapidBookingMsg) throws InterruptedException {
		Thread.sleep(3000);
		WebElement ele = getDriver().findElement(MarkUADbtnDisabled);
		Actions action = new Actions(getDriver());
		action.moveToElement(ele).perform();
	}

	public void msgOncurrentDateselection(String UADMarkedSucessMsg2) throws InterruptedException {

		Thread.sleep(3000);
		String msg = getDriver().findElement(msgOnReschduledatecurrntdate).getText();
		softAssert.assertEquals(msg, UADMarkedSucessMsg2, "MEssage displayed for current date");
		softAssert.assertAll();
		getDriver().findElement(closebtn).click();
	}

	public void slctUADReason(String UADReason4) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(reason).click();

		List<WebElement> resn4 = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu-container']/div/div/div/div/div"));

		for (int i = 0; i <= resn4.size() - 1; i++) {
			if (resn4.get(i).getText().equals(UADReason4)) {
				resn4.get(i).click();
				break;
			}
		}
	}

	public void slctRESHdate(String ReschdeuleDate4) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(rescheduleDatefield).sendKeys(ReschdeuleDate4);
	}

	public void maxRESHDATELimitReached(String MaxRescheduledatereachedMsg) throws InterruptedException {
		String msg = getDriver().findElement(maxlimitdatereached).getText();
		softAssert.assertEquals(msg, MaxRescheduledatereachedMsg, "Message displayed as expected");
		softAssert.assertAll();
		getDriver().findElement(closebtn).click();
	}

	public void enterOFDAWB(String AWB) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(searchAWBinp).sendKeys(AWB);
		getDriver().findElement(searchbtn).click();
	}

	public void checkMEssageDisplayed(String AWBMSG) throws InterruptedException {
		Thread.sleep(3000);
		WebElement btn = getDriver().findElement(btnMarkUAD);
		String txt = btn.getAttribute("title");
		softAssert.assertEquals(txt, AWBMSG, "Message matched");
		softAssert.assertAll();
	}

	public void undeliveredAWBMarkUADBtnEnabled() {
		softAssert.assertTrue(getDriver().findElement(btnMarkUAD).isDisplayed(), "Mark UAD button displayed");
		softAssert.assertAll();
	}

	public void checkCount(String Count) {
		String txt = getDriver().findElement(count).getText();
		softAssert.assertEquals(txt, Count, "Count is 0");
		softAssert.assertAll();
	}

	public void checkStatus(String Status) {
		String ststuatxt = getDriver().findElement(status).getText();
		softAssert.assertEquals(ststuatxt, Status, "Status is as No");
		softAssert.assertAll();
	}
}
