package com.xb.cafe.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;

public class Trip_sheet_enhancement_FirstMile extends PageObject{
	
	public  final By aWBNumberGeneratedLocator=By.xpath("//input[@id='awbNo']");
	public  final By invoicenumberinput=By.cssSelector("input[id=InvoiceNumber0]");
	public  final By invoicevalueinput=By.cssSelector("input[id=InvoiceValue0]");
	public  final By saveandnextbttn4=By.xpath("(//button[@title='Save And Next'])[4]");
	public  final By okButtonInvoiceConfirmation=By.xpath("(//button[contains(text(),'OK')])[5]");
	public  final By TripSheetButtonLocator=By.xpath("//button[@class=\"btn1 btn-primary\"]");
	
	public  final By TripSheetTableVehicleLocator=By.xpath("//th[contains(text(),'Vehicle:')]/following-sibling::td[1]");
	public  final By TripSheetTableBAOrSRLocator=By.xpath("//th[contains(text(),'Trip ID')]/following-sibling::td[4]");
	public  final By TripSheetTableODOReadingLocator=By.xpath("//th[contains(text(),'Vehicle Start ODO reading:')]/following-sibling::td[1]");
	public  final By TripSheetTableClientNameLocator=By.xpath("(//tbody[@id='tbody'])[3]//tr//td[3]");
	public  final By TripSheetPickupAddressLocator=By.xpath("(//tbody[@id='tbody'])[3]//tr//td[4]");
	public  final By TripSheetAWBNoLocator=By.xpath("(//tbody[@id='tbody'])[3]//tr//td[5]");
	public  final By TripSheetNoOfMPSLocator=By.xpath("(//tbody[@id='tbody'])[3]//tr//td[6]");
	public  final By TripSheetPhysicalWtLocator=By.xpath("(//tbody[@id='tbody'])[3]//tr//td[7]");
	
	
	SoftAssert softAssert = new SoftAssert();
	public  String awbBookingGenerated;
	
	public void captureTheAWBNumberGenerated() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(aWBNumberGeneratedLocator));
		awbBookingGenerated=getDriver().findElement(aWBNumberGeneratedLocator).getAttribute("value");
	}

	public  void userentersInvoiceandGSTdetailslessthen50K(String invoiceValue) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicenumberinput));
		System.out.println(awbBookingGenerated);
		getDriver().findElement(invoicenumberinput).sendKeys(awbBookingGenerated);
		wait.until(ExpectedConditions.visibilityOfElementLocated(invoicevalueinput));
		getDriver().findElement(invoicevalueinput).sendKeys(invoiceValue);
	}
	
	public void userclicksonSaveandNextbuttontocompleteBooking() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		Thread.sleep(5000);
//		wait.until(ExpectedConditions.elementToBeClickable(saveandnextbttn4));
		Actions actions = new Actions(getDriver());
        actions.moveToElement(getDriver().findElement(saveandnextbttn4)).click().perform();
//		getDriver().findElement(saveandnextbttn4).click();
	}

	public void userclicksonTripSheetButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(TripSheetButtonLocator));
		getDriver().findElement(TripSheetButtonLocator).click();
	}

	public void UservalidatetheassignedrecordstoSR(String vehicleNumber,String Assignedto,String ODOReading,String clientName,String Address,String pickupPincode,String NumberMPS,String TotalShipmentDeclaredWeight) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(TripSheetTableVehicleLocator));
		String VehicleNo = getDriver().findElement(TripSheetTableVehicleLocator).getText();
		softAssert.assertEquals(VehicleNo, vehicleNumber);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(TripSheetTableBAOrSRLocator));
		String BAorSR = getDriver().findElement(TripSheetTableBAOrSRLocator).getText();
		softAssert.assertEquals(BAorSR, Assignedto);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(TripSheetTableODOReadingLocator));
		String TripSheetODO = getDriver().findElement(TripSheetTableODOReadingLocator).getText();
		softAssert.assertEquals(TripSheetODO, ODOReading+".00");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(TripSheetTableClientNameLocator));
		String ClientNameTripPage = getDriver().findElement(TripSheetTableClientNameLocator).getText();
		softAssert.assertEquals(ClientNameTripPage, clientName+" Pvt Ltd");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(TripSheetPickupAddressLocator));
		String TripSheetaddress = getDriver().findElement(TripSheetPickupAddressLocator).getText();
		softAssert.assertEquals(TripSheetaddress, Address+","+pickupPincode);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(TripSheetAWBNoLocator));
		String TripSheetAWBNo = getDriver().findElement(TripSheetAWBNoLocator).getText();
		softAssert.assertEquals(TripSheetAWBNo, awbBookingGenerated);
		wait.until(ExpectedConditions.visibilityOfElementLocated(TripSheetNoOfMPSLocator));
		String TripSheetMPS = getDriver().findElement(TripSheetNoOfMPSLocator).getText();
		softAssert.assertEquals(TripSheetMPS, NumberMPS);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(TripSheetPhysicalWtLocator));
		String TripSheetPhysicalWt = getDriver().findElement(TripSheetPhysicalWtLocator).getText();
		softAssert.assertEquals(TripSheetPhysicalWt, TotalShipmentDeclaredWeight);
		softAssert.assertAll();
		Thread.sleep(2000);
	}

	public void capturetheAWBNumberfromRapidBooking() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(aWBNumberGeneratedLocator));
		awbBookingGenerated=getDriver().findElement(aWBNumberGeneratedLocator).getAttribute("value");
		
	}
	

}
