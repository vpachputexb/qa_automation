package com.xb.cafe.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;

public class Cargo_UADConfi extends PageObject {

	public final By FLMMenu = By.xpath("//span[contains(text(),'First & Last Mile')]");
	public final By UADMenu = By.xpath("//span[contains(text(),'UAD')]");
	public final By UADConfigMenu = By.xpath("//span[contains(text(),'UAD Configuration')]");
	public final By UADCutofftab = By.xpath("//section/div/button[contains(text(),'UAD Cut Offs')][1]");
	public final By UADcutoffLogtab = By.xpath("//button[contains(text(),'UAD Cut Offs Log')]");
	public final By Otherconfigtab = By.xpath("//button[contains(text(),'Other Configuration')]");
	public final By maxdaysInp = By.xpath("//input[@id='UADrescheduledays']");
	public final By consigneeReasonInp = By.xpath("//input[@id='Consigneereason']");
	public final By operationreasonInp = By.xpath("//input[@id='Operationsreason']");
	public final By Savebtn = By.xpath("//button[@type='submit']");
	public final By ErroroninvalidMAxdays = By
			.xpath("//div[contains(text(),'Max days for UAD re-schedule should be a Number and greater than zero')]");
	public final By Erroronblankmaxdays = By.xpath("//div[contains(text(),'Please enter value in atleast one field')]");
	public final By sucessMsg = By.xpath("//div[contains(text(),'Configuration Uploaded Succesfully')]");
	public final By errorConsigneeReason = By
			.xpath("//div[contains(text(),'Max UAD Attempt should be a Number and greater than zero')]");
	public final By blnkConsigneeReasonMsg = By
			.xpath("//div[contains(text(),'Please enter value in atleast one field')]");
	public final By successConsigneeReasonmsg = By
			.xpath("//div[contains(text(),'Configuration Uploaded Succesfully')]");
	public final By invaliderrOperationReason = By
			.xpath("//div[@class='alertify-notifier ajs-bottom ajs-right']/div");
	public final By successOperationReason = By.xpath("//div[contains(text(),'Configuration Uploaded Succesfully')]");
	public final By blnkOperationReasonmsg = By
			.xpath("//div[contains(text(),'Please enter value in atleast one field')]");
	public final By tbSrNo = By.xpath("//th[contains(text(),'Sr.No')]");
	public final By tbModificationdate = By.xpath("//th[contains(text(),'Modification Date')]");
	public final By tbRADcutoff = By.xpath("//th[contains(text(),'RAD Cut-off')]");
	public final By tbUADcutoff = By.xpath("//th[contains(text(),'UAD Cut-off')]");
	public final By tbusern = By.xpath("//th[contains(text(),'User name')]");
	public final By createcutoffbtn = By.xpath("//*[@title='Create UAD configuration']");
	public final By svebtn = By.xpath("//button[@type='submit']");
	public final By cancelbtn = By.xpath("//button[@type='reset']");

	public final By hubnameInp = By.xpath("//input[@id='hubname']");
	public final By hublist = By.xpath("//*[@class='menu visible']/div");
	public final By RADinscanlbl = By.xpath("//label[contains(text(),'RAD: Inscan Cut-off Time')]");
	public final By Uaddailycutofflbl = By.xpath("//label[contains(text(),'UAD: Daily Cut-off Time')]");
	public final By RADinscancutofftime = By.xpath("(//div[@class='vue-treeselect__input-container'])[1]");
	public final By UAddailycutofftime = By.xpath("(//div[@class='vue-treeselect__input-container'])[3]");
	public final By selecthubname = By.xpath("//div[@class='menu visible']/div");
	public final By successmsg1 = By.xpath("//div[contains(text(),'Cut-Off Time Uploaded Successfully')]");

	SoftAssert softAssert = new SoftAssert();

	public void NavigateToFLM() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(FLMMenu).click();
	}

	public void UADMenuDisplayed() throws InterruptedException {
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADMenu));
			getDriver().findElement(UADMenu).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(FLMMenu).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADMenu));
			softAssert.assertTrue(getDriver().findElement(UADMenu).isDisplayed(), "UAD menu displayed");
			softAssert.assertAll();
			Thread.sleep(3000);
		}

	}

	public void NavigateUADMenu() throws InterruptedException {
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADMenu));
			getDriver().findElement(UADMenu).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(FLMMenu).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADMenu));
			getDriver().findElement(UADMenu).click();
			Thread.sleep(3000);
		}

	}

	public void UADConfigMenuNAvigation() throws InterruptedException {
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADMenu));
			getDriver().findElement(UADMenu).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(FLMMenu).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADMenu));
			getDriver().findElement(UADMenu).click();
			getDriver().findElement(UADConfigMenu).click();
			Thread.sleep(3000);
		}

	}

	public void NavigateToUADConfigTab() throws InterruptedException {
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADMenu));
			getDriver().findElement(UADMenu).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(FLMMenu).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(UADMenu));
			getDriver().findElement(UADMenu).click();
			getDriver().findElement(UADConfigMenu).click();
			Thread.sleep(3000);
		}

	}

	public void TabsOnUADConfig(String Tab1, String Tab2, String Tab3) {
		String tab = getDriver().findElement(UADCutofftab).getText();
		softAssert.assertEquals(tab, Tab1, "UAD Cut off tab displayed");
		softAssert.assertAll();
		String tab1 = getDriver().findElement(UADcutoffLogtab).getText();
		softAssert.assertEquals(tab1, Tab2, "UAD Cut off Log tab displayed");
		softAssert.assertAll();
		String tab2 = getDriver().findElement(Otherconfigtab).getText();
		softAssert.assertEquals(tab2, Tab3, "Other Configuration tab displayed");
		softAssert.assertAll();

	}

	public void UADCutOffTabDisplayed() throws InterruptedException {
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(UADCutofftab).isDisplayed(), "UAD cut off tab is selected");
		softAssert.assertAll();
	}

	public void UADCutOffLogDisplayed() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(UADcutoffLogtab).click();
		Thread.sleep(2000);
		softAssert.assertTrue(getDriver().findElement(UADcutoffLogtab).isDisplayed(),
				"UAD cut off log tab is selected");
		softAssert.assertAll();

	}

	public void UADOtherConfigDisplayed() throws InterruptedException {

		Thread.sleep(3000);
		getDriver().findElement(Otherconfigtab).click();
		Thread.sleep(2000);
		softAssert.assertTrue(getDriver().findElement(Otherconfigtab).isDisplayed(),
				"Other configuration tab is selected");
		softAssert.assertAll();
	}

	public void FieldsOnOtherConfig() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(Otherconfigtab).click();
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(maxdaysInp).isDisplayed(),
				"Max days for UAD re schedule is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(consigneeReasonInp).isDisplayed(),
				"Consignee Reason is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(operationreasonInp).isDisplayed(),
				"Operation Reason is displayed");
		softAssert.assertAll();

	}

	public void ClickOnOtherConfig() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(Otherconfigtab).click();
	}

	public void EnterMaxdaysAndclickSave(String Maxdays, String ErrorMsg) throws InterruptedException {
		Thread.sleep(4000);
		getDriver().findElement(maxdaysInp).sendKeys(Maxdays);
		Thread.sleep(3000);
		$(Savebtn).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
		Thread.sleep(3000);
//	WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		// wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(ErroroninvalidMAxdays));
		String msg = getDriver().findElement(ErroroninvalidMAxdays).getText();
		softAssert.assertEquals(msg, ErrorMsg, "Max days entered is invalid");
		softAssert.assertAll();
		getDriver().findElement(maxdaysInp).clear();
	}

	public void BlankMaxdaysAndclickSave(String Maxdays1, String ErrorMsg1) throws InterruptedException {
		Thread.sleep(2000);
		getDriver().findElement(maxdaysInp).sendKeys(Maxdays1);
		getDriver().findElement(Savebtn).click();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(Erroronblankmaxdays));
		String msg = getDriver().findElement(Erroronblankmaxdays).getText();
		softAssert.assertEquals(msg, ErrorMsg1, "Max days entered is blank");
		softAssert.assertAll();
	}

	public void SuccessMsgOnMAxDays(String Maxdays2, String SuccessMsg) throws InterruptedException {
		Thread.sleep(2000);
		getDriver().findElement(maxdaysInp).sendKeys(Maxdays2);
		getDriver().findElement(Savebtn).click();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(sucessMsg));
		String msg = getDriver().findElement(sucessMsg).getText();
		softAssert.assertEquals(msg, SuccessMsg, "Max days saved");
		softAssert.assertAll();
	}

	public void EnterConsigneeReason(String ConsigneeReason, String ConsigneeReasonErrorMsg)
			throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(consigneeReasonInp).sendKeys(ConsigneeReason);
		Thread.sleep(5000);
		getDriver().findElement(Savebtn).click();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(errorConsigneeReason));
		String msg = getDriver().findElement(errorConsigneeReason).getText();
		softAssert.assertEquals(msg, ConsigneeReasonErrorMsg, "Consignee reason error is displayed");
		softAssert.assertAll();
	}

	public void ValidConsigneeReason(String ConsigneeReasoninp, String SuccessConsigneeReason)
			throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(consigneeReasonInp).sendKeys(ConsigneeReasoninp);
		getDriver().findElement(Savebtn).click();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(successConsigneeReasonmsg));
		String msg = getDriver().findElement(successConsigneeReasonmsg).getText();
		softAssert.assertEquals(msg, SuccessConsigneeReason, "Consignee reason updated");
		softAssert.assertAll();
	}

	public void BlankConsigneeReason(String ConsigneeReason1, String ConsigneeReasonErrorMsg1)
			throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(consigneeReasonInp).sendKeys(ConsigneeReason1);
		Thread.sleep(3000);
		getDriver().findElement(Savebtn).click();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(blnkConsigneeReasonMsg));
		String msg = getDriver().findElement(blnkConsigneeReasonMsg).getText();
		softAssert.assertEquals(msg, ConsigneeReasonErrorMsg1, "Consignee reason is blank");
		softAssert.assertAll();
	}

	public void InvalidOperationReason(String OperationReason, String OperationReasonErrorMsg)
			throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(operationreasonInp).sendKeys(OperationReason);
		getDriver().findElement(Savebtn).click();
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(invaliderrOperationReason));
		String msg = getDriver().findElement(invaliderrOperationReason).getText();
		softAssert.assertEquals(msg, OperationReasonErrorMsg, "Operation reason is invalid");
		softAssert.assertAll();
	}

	public void BlankOperationReason(String OperationReason1, String OperationReasonErrorMsg1)
			throws InterruptedException {
		Thread.sleep(6000);
		getDriver().findElement(operationreasonInp).sendKeys(OperationReason1);
		$(Savebtn).waitUntilVisible().waitUntilPresent().waitUntilClickable().click();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(blnkOperationReasonmsg));
		String msg = getDriver().findElement(blnkOperationReasonmsg).getText();
		softAssert.assertEquals(msg, OperationReasonErrorMsg1, "Operation reason is blank");
		softAssert.assertAll();
	}

	public void ValidOperationReason(String OperationReasoninp, String SuccessOperationReason)
			throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(operationreasonInp).sendKeys(OperationReasoninp);
		getDriver().findElement(Savebtn).click();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(successOperationReason));
		String msg = getDriver().findElement(successOperationReason).getText();
		softAssert.assertEquals(msg, SuccessOperationReason, "Operation reason updated");
		softAssert.assertAll();
	}

	public void EnterValidOtherConfigvalues(String MAXdays, String CONSIGNEEReason, String OPERATIONReason)
			throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(maxdaysInp).sendKeys(MAXdays);
		getDriver().findElement(consigneeReasonInp).sendKeys(CONSIGNEEReason);
		getDriver().findElement(operationreasonInp).sendKeys(OPERATIONReason);

	}

	public void SuccessOtherConfig(String SUCMsg) throws InterruptedException {
		Thread.sleep(4000);
		getDriver().findElement(Savebtn).click();
		String msg = getDriver().findElement(sucessMsg).getText();
		softAssert.assertEquals(msg, SUCMsg, "Other configuration data updated successfully");
	}

	public void LogTableColumns(String SrNo, String ModificationDate, String RADCutoff, String UADCutoff,
			String UserName) throws InterruptedException {
		Thread.sleep(3000);
		String sr = getDriver().findElement(tbSrNo).getText();
		softAssert.assertEquals(sr, SrNo, "SR No column displayed");
		softAssert.assertAll();
		String md = getDriver().findElement(tbModificationdate).getText();
		softAssert.assertEquals(md, ModificationDate, "Modification date column displayed");
		softAssert.assertAll();
		String radct = getDriver().findElement(tbRADcutoff).getText();
		softAssert.assertEquals(radct, RADCutoff, "RAD cutoff  column displayed");
		softAssert.assertAll();
		String uadct = getDriver().findElement(tbUADcutoff).getText();
		softAssert.assertEquals(uadct, UADCutoff, "UAD cut off column displayed");
		softAssert.assertAll();
		String unme = getDriver().findElement(tbusern).getText();
		softAssert.assertEquals(unme, UserName, "USername column displayed");
		softAssert.assertAll();

	}

	public void navigateUADCutoffLog() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(UADcutoffLogtab).click();
	}

	public void uadCutoffTable(String SrNo1, String ModificationDate1, String RADCutoff1, String UADCutoff1,
			String UserName1) {
		String srn = getDriver().findElement(tbSrNo).getText();
		softAssert.assertEquals(srn, SrNo1, "SR No column displayed");
		softAssert.assertAll();
		String md1 = getDriver().findElement(tbModificationdate).getText();
		softAssert.assertEquals(md1, ModificationDate1, "Modification date column displayed");
		softAssert.assertAll();
		String radct1 = getDriver().findElement(tbRADcutoff).getText();
		softAssert.assertEquals(radct1, RADCutoff1, "RAD cutoff  column displayed");
		softAssert.assertAll();
		String uadct1 = getDriver().findElement(tbUADcutoff).getText();
		softAssert.assertEquals(uadct1, UADCutoff1, "UAD cut off column displayed");
		softAssert.assertAll();
		String unme1 = getDriver().findElement(tbusern).getText();
		softAssert.assertEquals(unme1, UserName1, "USername column displayed");
		softAssert.assertAll();
	}

	public void createCutoff() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(createcutoffbtn).click();
	}

	public void visibilityOfSearchAndCancelBtn() throws InterruptedException {
		Thread.sleep(3000);
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("window.scrollBy(0,250)", "");
		softAssert.assertTrue(getDriver().findElement(svebtn).isDisplayed(), "Save button is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(cancelbtn).isDisplayed(), "Cancel button is displayed");
		softAssert.assertAll();

	}

	public void fieldsOnCreateCutOff() throws InterruptedException {
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(hubnameInp).isDisplayed(), "hub name is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(RADinscanlbl).isDisplayed(), "RAD insacn field is displayed");
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(Uaddailycutofflbl).isDisplayed(),
				"UAD daily cut off field is displayed");
		softAssert.assertAll();

	}

	public void selectDataIntoAllFields(String Hubname, String RADInscanTime, String UADDialyCutOffTime)
			throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(hubnameInp).click();
		List<WebElement> listhub = getDriver().findElements(By.xpath("//*[@class='menu visible']/div"));

		for (int i = 0; i <= listhub.size() - 1; i++) {
			if (listhub.get(i).getText().equalsIgnoreCase(Hubname)) {
				listhub.get(i).click();
				break;
			}

		}
		getDriver().findElement(RADinscancutofftime).click();
		List<WebElement> radtime = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu-container']/div/div/div/div/div"));

		for (int i = 0; i <= radtime.size() - 1; i++) {
			if (radtime.get(i).getText().equals(RADInscanTime)) {
				radtime.get(i).click();
				break;
			}

		}
		getDriver().findElement(UAddailycutofftime).click();
		List<WebElement> uadtime = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu-container']/div/div/div/div/div"));

		for (int i = 0; i <= uadtime.size() - 1; i++) {
			if (uadtime.get(i).getText().equals(UADDialyCutOffTime)) {
				uadtime.get(i).click();
				break;
			}

		}

	}

	public void clickSaveBtn() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(svebtn).click();
		Thread.sleep(1000);
	}

	public void successMessageCutOffCreation(String Successmsg) throws InterruptedException {
		Thread.sleep(2000);
		String msg = getDriver().findElement(successmsg1).getText();
		softAssert.assertEquals(msg, Successmsg, "Cut off created");
		softAssert.assertAll();
	}

}
