package com.xb.cafe.ui.pages;

import java.util.List;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.actions.Upload;

public class Tariffcard extends PageObject {

	public static final String UserValidatesTheErrorMsgAndUserClicksOnOkButton = null;
	private static final CharSequence HigherBreakLimitValue = null;
	String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	String username;
	String TagName1;
	String TagComment1;
	String Tag_MappedEnvironment;
	// WebDriver driver = null;

	public final By input_UN = By.xpath("//input[@name='email']");
	public final By input_PW = By.xpath("//input[@id='typepass']");
	public final By btn_Login = By.xpath("//button[contains(text(),'Login')]");
	public final By Lnk_MasterData = By.xpath("//span[contains(text(),'Master Data')]");
	public final By lnk_Tariffcard = By.xpath("//span[contains(text(),'Tariff Card')]");
	public final By AddNewTariffCardlocator = By.xpath("//a[@title='Add New Tariff Card']");
	public final By TariffcardTxtinput = By.xpath("//input[@id='tariffcardName']");
	public final By RoutemodeAirlocator = By.xpath("//label[contains(text(),'Air')]");
	public final By Serviceleveltemplatenamelocator = By.xpath("//input[@class='vue-treeselect__input']");

	public final By downloadratetemplate_btn = By.xpath("//button[contains(text(),'Download Rate Template')]");
	public final By Choosefile_btn = By.xpath("//a[contains(text(),'Choose File')]");
	public final By Validatefile_btn = By.xpath("//button[contains(text(),'Validate File')]");
	public final By Save_btn = By.xpath("//button[@class='-btn btn-success']");
	public final By Ok_btn = By.xpath("//button[contains(text(),'OK')]");
	public final By Successmsglocator = By.xpath("//div[@id='swal2-content']");
	public final By Fileuploadlocator = By.xpath("//input[@id='FileUpload']");
	public final By Tariffcardfilterlocator = By.xpath("//input[@id='tariffcardName']");

	public final By Downloadratelocator = By.xpath("//button[contains(text(),'Download Rate')]");
	public final By Filterlocator = By.xpath("//i[@title='Search Filter']");
	public final By Searchbuttonlocator = By.xpath("//button[@class='-btn btn-success']");
	public final By Editbuttonlocator = By.xpath("//i[@class='fa fa-edit']");
	public final By Break1Locator = By
			.xpath("//label[contains(text(),'Break 1 (Quantity (Number of boxes)): >= 1,< 10)')]");
	public final By Break2locator = By
			.xpath("//label[contains(text(),'Break 2 (Quantity (Number of boxes)): >= 10,<= 20)')]");
	public final By slabtemplatelocator = By.xpath("(//input[@class='vue-treeselect__input'])[2]");

	public final By Tariffcardratetypelocator = By.xpath("(//i[@class='cr-icon fa fa-check'])[6]");
	public final By Slabtypelocator = By.xpath("(//i[@class='cr-icon fa fa-check'])[8]");
	public final By RatebasisQtylocator = By.xpath("(//i[@class='cr-icon fa fa-check'])[4]");
	public final By Choosefilelocator1 = By.xpath("//input[@id='FileUpload0']");
	public final By Choosefilelocator2 = By.xpath("//input[@id='FileUpload1']");
	public final By validatefilelocator1 = By.xpath("(//button[@title='Validate File'])[1]");
	public final By validatefilelocator2 = By.xpath("(//button[@title='Validate File'])[2]");
	public final By Invalidrecordslocator = By
			.xpath("//label[contains(text(),'Total Records: 23, Valid Records: 0, Invalid Records: 23')]");
	public final By Downloadcsvbtnlocator = By.xpath("//button[contains(text(),'Download (CSV)')]");
	public final By Tariffcardfilterinputlocator = By.xpath("//input[@id='tariffcardName']");
	public final By Searchbtnlocator = By.xpath("//button[@class='-btn btn-success']");
	public final By Editbtnlocator = By.xpath("//span[@title='Edit']");
	public final By Filterlocator1 = By.xpath("//i[@title='Search Filter']");
	public final By downloadratelocator = By.xpath("//button[contains(text(),'Download Rate')]");

	SoftAssert softAssert = new SoftAssert();

	public void LoginSelfServicepage(String URL) throws InterruptedException {
		getDriver().get(URL);
	}

	public void userNavigatesToTraiffCardOption() throws InterruptedException {
		Thread.sleep(1000);
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		try {
			getDriver().navigate().refresh();
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(lnk_Tariffcard));
			getDriver().findElement(lnk_Tariffcard).click();
			Thread.sleep(2000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(Lnk_MasterData));
			getDriver().findElement(Lnk_MasterData).click();
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(lnk_Tariffcard));
			getDriver().findElement(lnk_Tariffcard).click();
		}
	}

	public void clickOnAddNewTariffCardButton() throws InterruptedException {
		Thread.sleep(5000);
		getDriver().findElement(AddNewTariffCardlocator).click();

	}

	public void provideTariffCardNameAndSelectRouteModeAir(String TemplateName) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);

		wait.until(ExpectedConditions.visibilityOfElementLocated(TariffcardTxtinput));
		WebElement TariffcardsearchTxt = getDriver().findElement(TariffcardTxtinput);
		TariffcardsearchTxt.sendKeys(TemplateName);
		Thread.sleep(1000);
		TariffcardsearchTxt.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("//label[contains(text(),'Air')]")).click();
	}

	public void selectServiceLevelTemplateFromDropDownAndSelectRateBasisQuantity(String Serviceleveltemplate,
			String ratebasis) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);

		getDriver().findElement(Serviceleveltemplatenamelocator).click();
		Thread.sleep(4000);
		List<WebElement> listtemplateName = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		for (int i = 0; i <= listtemplateName.size() - 1; i++) {
			System.out.println(listtemplateName.get(i).getText());
			if (listtemplateName.get(i).getText().equalsIgnoreCase(Serviceleveltemplate)) {
				listtemplateName.get(i).click();
				break;
			}
		}

		getDriver().findElement(By.xpath("//label[contains(text(),'Quantity (Number of boxes)')]")).click();
	}

	public void selectTariffCardRateTypeFlatRate(String Tariffcardratetype) throws InterruptedException {
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("(//i[@class='cr-icon fa fa-check'])[5]")).click();

	}

	public void uploadFileAndValidateIt(String fileName) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		System.out.println("My Path : " + System.getProperty("user.dir"));
		String filePath = System.getProperty("user.dir") + "/Test Data/TariffAutomation.csv";

		System.out.println(filePath + "-------------------------------Filepathstarts frm here");
		$(Fileuploadlocator).sendKeys(filePath);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Validatefile_btn));
		getDriver().findElement(Validatefile_btn).click();
		Thread.sleep(1000);

	}

	public void clickOnSaveButtonAndValidatesSuccessmsg(String Successmsg) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(4000);
		getDriver().findElement(Save_btn).click();

		softAssert.assertEquals(getDriver()
				.findElement(By.xpath("//div[@id='swal2-content']".replace("Record created successfully.", Successmsg)))
				.getText(), Successmsg, "Record created successfully.");
		softAssert.assertAll();

		wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
		getDriver().findElement(Ok_btn).click();

	}

	public void selectTariffCardRateTypeSlabRate() throws InterruptedException {
		getDriver().findElement(By.xpath("(//i[@class='cr-icon fa fa-check'])[6]")).click();

	}

	public void userSelectsSlabTypeQuantity() throws InterruptedException {
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("(//i[@class='cr-icon fa fa-check'])[8]")).click();

	}

	public void userSelectsSlabTemplate(String Slabtemplate) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);

		getDriver().findElement(slabtemplatelocator).click();
		Thread.sleep(4000);
		List<WebElement> listSlabName = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		for (int i = 0; i <= listSlabName.size() - 1; i++) {
			System.out.println(listSlabName.get(i).getText());
			if (listSlabName.get(i).getText().equalsIgnoreCase(Slabtemplate)) {
				listSlabName.get(i).click();
				break;
			}

		}
	}

	public void userClicksOnBreak1() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);

		wait.until(ExpectedConditions.elementToBeClickable(Break1Locator));
		getDriver().findElement(Break1Locator).click();

	}

	public void uploadFile1AndValidateIt(String fileName1) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		System.out.println("My Path : " + System.getProperty("user.dir"));
		String filePath = System.getProperty("user.dir") + "/Test Data/SlabAutomation1.csv";

		System.out.println(filePath + "-------------------------------Filepathstarts frm here");
		$(Choosefilelocator1).sendKeys(filePath);
		Thread.sleep(1000);

		wait.until(ExpectedConditions.visibilityOfElementLocated(validatefilelocator1));
		getDriver().findElement(validatefilelocator1).click();
		Thread.sleep(1000);

	}

	public void userClicksOnBreak2() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("window.scrollBy(0,250)", "");

		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.elementToBeClickable(Break2locator));
		getDriver().findElement(Break2locator).click();

	}

	public void uploadFile2AndVvalidateIt(String fileName2) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("window.scrollBy(0,250)", "");

		System.out.println("My Path : " + System.getProperty("user.dir"));
		String filePath = System.getProperty("user.dir") + "/Test Data/SlabAutomation2.csv";

		System.out.println(filePath + "-------------------------------Filepathstarts frm here");
		$(Choosefilelocator2).sendKeys(filePath);
		Thread.sleep(1000);

		wait.until(ExpectedConditions.visibilityOfElementLocated(validatefilelocator2));
		getDriver().findElement(validatefilelocator2).click();
		Thread.sleep(1000);
	}

	public void uploadFile2(String fileName2) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		System.out.println("My Path : " + System.getProperty("user.dir"));
		String filePath = System.getProperty("user.dir") + "/Test Data/SlabAutomation2.csv";

		System.out.println(filePath + "-------------------------------Filepathstarts frm here");
		$(Choosefilelocator2).sendKeys(filePath);
		Thread.sleep(1000);

	}

	public void clickOnSaveButtonAndValidatesErrormsg(String Errormsg) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(4000);
		getDriver().findElement(Save_btn).click();

		softAssert.assertEquals(getDriver()
				.findElement(By
						.xpath("//div[@id='swal2-content']".replace("Please Validate Rate for Slab Break 2", Errormsg)))
				.getText(), Errormsg, "Please Validate Rate for Slab Break 2");
		softAssert.assertAll();

		wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
		getDriver().findElement(Ok_btn).click();

	}

	public void uploadInvalidFileAndValidateIt(String InvalidfileName) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		System.out.println("My Path : " + System.getProperty("user.dir"));
		String filePath = System.getProperty("user.dir") + "/Test Data/TariffAutomationInvaliddata.csv";

		System.out.println(filePath + "-------------------------------Filepathstarts frm here");
		$(Fileuploadlocator).sendKeys(filePath);

		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Validatefile_btn));
		getDriver().findElement(Validatefile_btn).click();
		Thread.sleep(1000);
	}

	public void userClicksOnDownloadCSVButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		Thread.sleep(4000);
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("window.scrollBy(0,250)", "");

		wait.until(ExpectedConditions.elementToBeClickable(Downloadcsvbtnlocator));
		getDriver().findElement(Downloadcsvbtnlocator).click();

	}

	public void userEntersTariffcardNameInTariffCardNameFilter(String Tariffcardname) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(4000);

		wait.until(ExpectedConditions.visibilityOfElementLocated(Tariffcardfilterinputlocator));
		WebElement Tariffcardfilterinput = getDriver().findElement(Tariffcardfilterinputlocator);
		Tariffcardfilterinput.sendKeys(Tariffcardname);
		Thread.sleep(1000);
		Tariffcardfilterinput.sendKeys(Keys.ENTER);
		Thread.sleep(1000);

	}

	public void userClicksOnEditOption() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(4000);
		wait.until(ExpectedConditions.elementToBeClickable(Editbtnlocator));
		getDriver().findElement(Editbtnlocator).click();

	}

	public void userValidatesTheAlertMsgAndClicksOnOk(String Alertmsg) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(1000);
		softAssert
				.assertEquals(getDriver()
						.findElement(By.xpath("//div[@id='swal2-content']"
								.replace("The old data will be overwritten by new data.Are you sure?", Alertmsg)))
						.getText(), Alertmsg, "The old data will be overwritten by new data.Are you sure?");
		softAssert.assertAll();

		wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
		getDriver().findElement(Ok_btn).click();

	}

	public void userClicksOnFilterOption1() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(4000);

		wait.until(ExpectedConditions.elementToBeClickable(Filterlocator1));
		getDriver().findElement(Filterlocator1).click();

	}

	public void userClicksOnSearchButton1() throws InterruptedException {
		getDriver().findElement(Searchbtnlocator).click();

	}

	public void userClicksOnSaveButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(4000);
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("window.scrollBy(0,250)", "");
		wait.until(ExpectedConditions.elementToBeClickable(Save_btn));
		getDriver().findElement(Save_btn).click();

	}

	public void downloadValidData() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(4000);
		wait.until(ExpectedConditions.elementToBeClickable(downloadratelocator));
		getDriver().findElement(downloadratelocator).click();

	}

	public void uploadFile3(String fileName3) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("window.scrollBy(0,250)", "");

		System.out.println("My Path : " + System.getProperty("user.dir"));
		String filePath = System.getProperty("user.dir") + "/Test Data/TariffAutomation.csv";

		System.out.println(filePath + "-------------------------------Filepathstarts frm here");
		$(Choosefilelocator1).sendKeys(filePath);
		Thread.sleep(1000);

	}

	public void verifyHeaderMismatchError(String Headermismatcherror) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(1000);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//div[@id='swal2-content']".replace(
				"File headers are not matched. Please download Tariff card csv file and use that file to upload data to avoid header mismatch issue.",
				Headermismatcherror))).getText(), Headermismatcherror,
				"File headers are not matched. Please download Tariff card csv file and use that file to upload data to avoid header mismatch issue.");
		softAssert.assertAll();

		wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
		getDriver().findElement(Ok_btn).click();

	}

	public void selectServiceLevelTemplateFromDropDownAndSelectRateBasisWeight(String Serviceleveltemplate,
			String ratebasis) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);

		getDriver().findElement(Serviceleveltemplatenamelocator).click();
		Thread.sleep(1000);
		List<WebElement> listtemplateName = getDriver()
				.findElements(By.xpath("//div[@class='vue-treeselect__menu']//label"));
		for (int i = 0; i <= listtemplateName.size() - 1; i++) {
			System.out.println(listtemplateName.get(i).getText());
			if (listtemplateName.get(i).getText().equalsIgnoreCase(Serviceleveltemplate)) {
				listtemplateName.get(i).click();
				break;
			}
		}

		getDriver().findElement(By.xpath("//label[contains(text(),'Weight (kg)')]")).click();
	}

	public void userTriesToSelectDisabledSlabTypeQuantity() throws InterruptedException {
		Thread.sleep(1000);
		getDriver().findElement(By.xpath("(//i[@class='cr-icon fa fa-check'])[8]"));
		if (getDriver().findElement(By.xpath("(//i[@class='cr-icon fa fa-check'])[8]")).isEnabled()) {
			getDriver().findElement(By.xpath("(//i[@class='cr-icon fa fa-check'])[8]")).click();
		}

	}

}
