package com.xb.cafe.ui.pages;

import java.util.List;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.actions.Upload;

public class TnCClient extends PageObject {

	public static final String UserValidatesTheErrorMsgAndUserClicksOnOkButton = null;
	private static final CharSequence HigherBreakLimitValue = null;
	String Timestamp = new DateTime().toString("yyyy-MM-dd-HH-mm-ss");
	String username;
	String TagName1;
	String TagComment1;
	String Tag_MappedEnvironment;
	// WebDriver driver = null;

	public final By input_UN = By.xpath("//input[@name='email']");
	public final By input_PW = By.xpath("//input[@id='typepass']");
	public final By btn_Login = By.xpath("//button[contains(text(),'Login')]");
	public final By Lnk_Clientmanagement = By.xpath("//span[contains(text(),'Client Management')]");
	public final By Lnk_TnC = By.xpath("//span[contains(text(),'T&C')]");
	public final By AddnewTnCbtn_locator = By.xpath("//a[@title='Add New Terms & Conditions']");
	public final By Companynamedropdown_locator = By
			.xpath("(//div[@class='ui fluid search selection dropdown form-control'])[1]");
	public final By Ok_btn = By.xpath("//button[contains(text(),'OK')]");
	public final By Savebtn_loactor = By.xpath("//button[@class='-btn btn-success']");
	public final By TnCtemplatedropdown_locator = By.xpath("//input[@id='template']");
	public final By TnCtemplatedropdown1_locator = By.xpath("//div[contains(text(),'Select T&C Template')]");
	public final By Editbtn_locator = By.xpath("//span[@title='Edit']");

	SoftAssert softAssert = new SoftAssert();

	public void LoginSelfServicepage(String URL) throws InterruptedException {
		getDriver().get(URL);
	}

	public void userNavigatesToClientManagementOptionTnCOption() throws InterruptedException {
		Thread.sleep(1000);
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		try {
			getDriver().navigate().refresh();
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(Lnk_TnC));
			getDriver().findElement(Lnk_TnC).click();
			Thread.sleep(2000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(Lnk_Clientmanagement));
			getDriver().findElement(Lnk_Clientmanagement).click();
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(Lnk_TnC));
			getDriver().findElement(Lnk_TnC).click();
		}

	}

	public void userClicksOnCreateNewTnCOption() throws InterruptedException {
		Thread.sleep(5000);
		getDriver().findElement(AddnewTnCbtn_locator).click();

	}

	public void userValidatesMsgAndClicksOnOk(String Msg) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//div[@id='swal2-content']".replace(
				"T&C Template already exists for selected contract id CR-2196-2022-1. You can update details for the same.",
				Msg))).getText(), Msg,
				"T&C Template already exists for selected contract id CR-2196-2022-1. You can update details for the same.");
		softAssert.assertAll();

		wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
		getDriver().findElement(Ok_btn).click();

	}

	public void userValidatesSuccessmsgTnCAndClicksOnOk(String SuccessmsgTnC) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		softAssert.assertEquals(getDriver()
				.findElement(
						By.xpath("//div[@id='swal2-content']".replace("Record saved successfully.", SuccessmsgTnC)))
				.getText(), SuccessmsgTnC, "Record saved successfully.");
		softAssert.assertAll();

		wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
		getDriver().findElement(Ok_btn).click();

	}

	public void userSelectsNewTnCTemplate(String TnCtemplate) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(4000);
		getDriver().findElement(TnCtemplatedropdown_locator).click();
		Thread.sleep(4000);
		List<WebElement> listtemplateName = getDriver().findElements(By.xpath("//div[@class='menu visible']//div"));
		for (int i = 0; i <= listtemplateName.size() - 1; i++) {
			System.out.println(listtemplateName.get(i).getText());
			if (listtemplateName.get(i).getText().equalsIgnoreCase(TnCtemplate)) {
				listtemplateName.get(i).click();
				break;
			}
		}

	}

	public void userValidatesMsg1AndClicksOnOk(String Msg1) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//div[@id='swal2-content']".replace(
				"T&C Template already exists for selected contract id CR-2196-2022-1. You can update details for the same.",
				Msg1))).getText(), Msg1,
				"T&C Template already exists for selected contract id CR-2196-2022-1. You can update details for the same.");
		softAssert.assertAll();

		wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
		getDriver().findElement(Ok_btn).click();

	}

	public void userValidatesMsg2AndClicksOnOk(String Msg2) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		softAssert.assertEquals(getDriver().findElement(By.xpath("//div[@id='swal2-content']".replace(
				"T&C Template already exists for selected contract id CR-1-2021-2. You can update details for the same.",
				Msg2))).getText(), Msg2,
				"T&C Template already exists for selected contract id CR-1-2021-2. You can update details for the same.");
		softAssert.assertAll();

		wait.until(ExpectedConditions.elementToBeClickable(Ok_btn));
		getDriver().findElement(Ok_btn).click();

	}

	public void userSelectsTnCTemplate(String NewTnCTemplate) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(4000);
		getDriver().findElement(TnCtemplatedropdown1_locator).click();
		Thread.sleep(4000);
		List<WebElement> listTnCtemplateName = getDriver().findElements(By.xpath("//div[@class='menu visible']//div"));
		for (int i = 0; i <= listTnCtemplateName.size() - 1; i++) {
			System.out.println(listTnCtemplateName.get(i).getText());
			if (listTnCtemplateName.get(i).getText().equalsIgnoreCase(NewTnCTemplate)) {
				listTnCtemplateName.get(i).click();
				break;
			}
		}

	}

	public void userClicksOnEditButton1() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.elementToBeClickable(Editbtn_locator));
		getDriver().findElement(Editbtn_locator).click();

	}

	public void userValidatesTnCErrorMsgs(String Companynameerrormsg1, String Contractiderrormsg1,
			String TnCTemplateerrormsg) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//span[contains(text(),'Please select company name.')]"
						.replace("Please select company name.", Companynameerrormsg1))).getText(),
				Companynameerrormsg1, "Please select company name.");
		softAssert.assertAll();

		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//span[contains(text(),'Please select contract id.')]"
						.replace("Please select contract id.", Contractiderrormsg1))).getText(),
				Contractiderrormsg1, "Please select contract id.");
		softAssert.assertAll();

		softAssert.assertEquals(
				getDriver().findElement(By.xpath("//span[contains(text(),'The template field is required')]"
						.replace("The template field is required", Contractiderrormsg1))).getText(),
				Contractiderrormsg1, "The template field is required");
		softAssert.assertAll();

	}

}
