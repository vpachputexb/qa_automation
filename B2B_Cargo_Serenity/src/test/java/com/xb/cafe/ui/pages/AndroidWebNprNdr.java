package com.xb.cafe.ui.pages;

import static org.mockito.ArgumentMatchers.isNotNull;

import org.hamcrest.core.IsEqual;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Managed;

public class AndroidWebNprNdr extends PageObject {

	public final By EditClick = By.xpath("(//i[@class='fa fa-edit'])[1]");
	public final By ExpandClick = By
			.xpath("//div[@class='row position']//div//button[@title='Expand All'][normalize-space()='Expand All']");
	public final By ExpandClick1 = By.xpath("//button[normalize-space()='Expand All']");
	public final By AwdDate = By.xpath("//th[normalize-space()='AWB RAD Date']");
	public final By LstMile = By.xpath("//button[normalize-space()='Last Mile Shipment']");
	public final By ValueAS = By.xpath("//th[normalize-space()='Value Added Services']");
	public final By DatnTim = By.xpath("//tr[@class='text-primary rowdisable']//td[11]");
	// public final By DatnTim=By.xpath("//td[normalize-space()='09-04-2022']");
	public final By FilterBtn = By.xpath("//i[@title='Search Filter']");
	public final By IpAwb = By.xpath("//input[@id='AWB']");
	public final By IpAwb1 = By.xpath("(//input[@id='awb'])[1]");
	public final By IpAwb2 = By.xpath("(//div[@id='collapse']//input[@id='AWB'])");
	public final By BtnSearch = By.xpath("//button[normalize-space()='Search']");
	public final By BtnView = By.xpath("//span[@title='View']//img");
	public final By BtnClose = By.xpath("//button[normalize-space()='Close Trip']");
	public final By BtnExpand = By.xpath(
			"//div[@class='tab-content1 tab-header1 tab-pane show fade active']//button[@title='Expand All'][normalize-space()='Expand All']");
	public final By IpScan = By.xpath("//input[@id='scanAWB']");
	public final By IpMps = By.xpath("//input[@id='scanMPS']");
	public final By IpInvoice = By.xpath("//input[@id='physicalInvoices']");
	public final By IpOdo = By.xpath("//input[@id='VehicleCloseODOreading']");
	public final By BtnCloseTrip = By.xpath("//button[normalize-space()='Close Trip']");
	public final By BtnUndn = By.xpath("//button[@class='swal2-confirm swal2-styled']");
	public final By BtnCloseOk = By.xpath("//button[@class='swal2-confirm swal2-styled']");
	public final By BtnEdit = By.xpath("//i[@class='fa fa-edit']");
	public final By BtnEx2 = By.xpath("//label[normalize-space()='Add Shipments']");
	public final By BtnNotAdded = By
			.xpath("//div[@class='col-md-12']//button[@title='Expand All'][normalize-space()='Expand All']");
	public final By ResdlClm = By.xpath("//th[normalize-space()='Re-schedule Date']");
	public final By TabLM = By.xpath("//button[normalize-space()='Last Mile Shipment']");
	public final By VasClm = By.xpath("//th[normalize-space()='Value Added Services']");
	public final By BtnCollap = By.xpath("//i[@data-toggle='collapse']");
	public final By BtnCollapLM = By.xpath("//i[@class='fa fa-filter filter']");
	public final By IpAwbStat = By.xpath("//div[@id='collapse']//div[@id='awbstatus']//input[@type='text']");
	public final By BtnSearch1 = By.xpath(
			"//div[@id='collapse']//div[@class='card']//div[@class='card-block']//div[@class='row']//div//button[@type='button'][normalize-space()='Search']");
	public final By PickupNot = By.xpath("//tr[@class='text-primary rowdisable']//td[3]");
	public final By AwbStatus = By.xpath("(//div[@id='awbstatus']");
	public final By BtnSearch2 = By.xpath(
			"//div[@id='collapse']//div[@class='card']//div[@class='card-block']//div[@class='row']//div//button[@type='button'][normalize-space()='Search']");
	public final By AwbStatus1 = By.xpath("(//td[contains(text(),'Undelivered')])[1]");
	public final By BtnBk = By.xpath("//i[@class='fa fa-arrow-left']");
	public final By ReDate = By.xpath("//tr[1]/td[11]");
	public final By Vdate = By.xpath("//span[@title='Appointment Delivery']");
	public final By Dashboard = By.xpath("//span[normalize-space()='Client Dashboard']");
	public final By DropClick = By.xpath("//span[@class='d-md-down-none']");
	public final By LogOut = By.xpath("//a[normalize-space()='Logout']");
	public final By TripIdBtn = By.xpath("//button[@class='swal2-confirm swal2-styled']");
	public final By ViewBtn = By.xpath("(//span[@title='View'])[1]");
	public final By LmTab = By.xpath("//button[normalize-space()='Last Mile']");
	public final By LmTab1 = By.xpath("//a[normalize-space()='Last Mile']");
	public final By UnDeliveAwb = By.xpath("//label[normalize-space()='Undelivered AWBs']");
	public final By DocInScn = By.xpath("//label[@for='Docchck2']");
	public final By ScnDoc = By.xpath("//input[@id='scanUndeliveredAWB']");
	public final By InvcNo = By.xpath("//input[@id='lmPhysicalInvoices']");
	public final By Svbtn = By.xpath("//button[@class='-btn btn-success'][normalize-space()='Save']");
	public final By MpsInsnTab = By.xpath("//label[@for='docchck']");
	public final By ScnAwb = By.xpath("//input[@id='scanUndeliveredMPS']");
	public final By FmTab = By.xpath("//button[normalize-space()='First Mile']");
	public final By DelivStas = By.xpath("(//div[@class='overflow'])[2]//td[4]");
	public final By IpTripId = By.xpath("//input[@id='tripid']");
	public final By BtnSave = By.xpath("//button[@class='-btn btn-primary'][normalize-space()='Save']");
	public final By ScanAwb = By.xpath("//input[@id='scanDeliveredAWB']");
	//public final By BtnGood = By.xpath("//input[@value='good']");
	public final By BtnGood = By.xpath("//label[normalize-space()='Good']");
	public final By BtnNxt = By.xpath("//button[normalize-space()='Save and Next']");

	WebDriverWait wait = new WebDriverWait(getDriver(), 30);
	
	SoftAssert softAssert = new SoftAssert();
	public String awbBookingGenerated;

	public void userNavigatesToTripManagement() throws InterruptedException {

		getDriver().findElement(EditClick).click();
		Thread.sleep(1000);
		getDriver().findElement(ExpandClick).click();
		Thread.sleep(1000);
		getDriver().findElement(LstMile).click();
		Thread.sleep(5000);
		getDriver().findElement(ExpandClick1).click();
		Thread.sleep(2000);
		softAssert.assertTrue(getDriver().findElement(AwdDate).isDisplayed());
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(ValueAS).isDisplayed());
		softAssert.assertAll();
		softAssert.assertTrue(getDriver().findElement(DatnTim).isDisplayed());

	}

	public void searchAndClickOnView(String awb2, String Awb) throws InterruptedException {
		
		// Click on Filter
		wait.until(ExpectedConditions.visibilityOfElementLocated(FilterBtn));
		getDriver().findElement(FilterBtn).click();
		
		// Send AWB
		wait.until(ExpectedConditions.visibilityOfElementLocated(IpTripId));
		
		
		//getDriver().findElement(IpTripId).sendKeys(WebFirstMile.tripIdNumber);
		//getDriver().findElement(IpTripId).sendKeys("22238");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnSearch));
		getDriver().findElement(BtnSearch).click();
		System.out.println("Clicked on search");
		// Click on View 
		//getDriver().findElement(BtnView).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(ViewBtn));
		getDriver().findElement(ViewBtn).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(LmTab));
		getDriver().findElement(LmTab).click();
		softAssert.assertTrue(getDriver().findElement(DelivStas).getText().contains("delivered"));
		Thread.sleep(1500);
		getDriver().findElement(BtnClose).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(LmTab1));
		getDriver().findElement(LmTab1).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnExpand));
		getDriver().findElement(BtnExpand).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(ScanAwb));
		getDriver().findElement(ScanAwb).sendKeys(Awb);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnGood));
		getDriver().findElement(BtnGood).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnNxt));
		getDriver().findElement(BtnNxt).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(IpOdo));
		getDriver().findElement(IpOdo).sendKeys("2");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnClose));
		getDriver().findElement(BtnClose).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnUndn));
		getDriver().findElement(BtnUndn).click();
		
		Thread.sleep(1000);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnUndn));
//		getDriver().findElement(BtnUndn).click();
//		
		
		
		
//		wait.until(ExpectedConditions.visibilityOfElementLocated(IpScan));
//		getDriver().findElement(IpScan).sendKeys(aWB);
//		
//		wait.until(ExpectedConditions.visibilityOfElementLocated(IpInvoice));
//		getDriver().findElement(IpInvoice).sendKeys("1");
//		
//		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnSave));
//		getDriver().findElement(BtnSave).click();
//				
//		wait.until(ExpectedConditions.visibilityOfElementLocated(IpMps));
//		getDriver().findElement(IpMps).sendKeys(aWB);
//				
//		wait.until(ExpectedConditions.visibilityOfElementLocated(IpOdo));
//		getDriver().findElement(IpOdo).sendKeys("2");
//		
//		
		
		
		//Unable to close trip on Bcz Close button is disabled 
		

	}
	
	public void searchForAwbAndCloseTheFirstMileTrip(String aWB) throws InterruptedException {
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

	public void clickedExpandScanAnsSave(String aWB, String invoices) throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnExpand));
		getDriver().findElement(BtnExpand).click();
		// Scan MPS-AWB
		getDriver().findElement(IpScan).sendKeys(aWB);
		getDriver().findElement(IpInvoice).sendKeys(invoices);
		getDriver().findElement(IpMps).sendKeys(aWB);

	}

	public void addOddAndCloseTrip(String odoReading) throws InterruptedException {
		getDriver().findElement(IpOdo).sendKeys(odoReading);
		getDriver().findElement(BtnCloseTrip).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnUndn));
		getDriver().findElement(BtnUndn).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnCloseOk));
		getDriver().findElement(BtnCloseOk).click();
		getDriver().findElement(BtnBk).click();
		Thread.sleep(3000);
		

	}

	public void searchRescheduleColumnIsThere(String aWB, String NprDate) throws InterruptedException {
		// Click on Back Button
		//getDriver().findElement(BtnBk).click();
		// Click on Filter
		//Thread.sleep(10000);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(FilterBtn));
		getDriver().findElement(FilterBtn).click();
		
		// Send AWB
		wait.until(ExpectedConditions.visibilityOfElementLocated(IpAwb));
		getDriver().findElement(IpAwb).sendKeys(aWB);
		
		// Click on Edit
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnEdit));
		getDriver().findElement(BtnEdit).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnEx2));
		getDriver().findElement(BtnEx2).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnNotAdded));
		getDriver().findElement(BtnNotAdded).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnCollap));
		getDriver().findElement(BtnCollap).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(IpAwb1));
		getDriver().findElement(IpAwb1).sendKeys(aWB);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnSearch1));
		getDriver().findElement(BtnSearch1).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(PickupNot));
		softAssert.assertTrue(getDriver().findElement(PickupNot).getText().contains("PickupNotDone"));
		softAssert.assertAll();
		
		softAssert.assertTrue(getDriver().findElement(ResdlClm).isDisplayed());
		softAssert.assertAll();

		softAssert.assertTrue(getDriver().findElement(DatnTim).getText().contains(NprDate));
		softAssert.assertAll();


	}

	public void VasClmnIsPresent() throws InterruptedException {

		getDriver().findElement(TabLM).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(BtnEx2));
		getDriver().findElement(BtnEx2).click();
		getDriver().findElement(BtnCollapLM).click();
		getDriver().findElement(AwbStatus).sendKeys("Shipment Not Delivered");
		getDriver().findElement(BtnSearch2).click();
		
		softAssert.assertTrue(getDriver().findElement(AwbStatus1).getText().contains("Undelivered"));
		softAssert.assertAll();
		
		softAssert.assertTrue(getDriver().findElement(VasClm).isDisplayed());
		softAssert.assertAll();
		
		softAssert.assertTrue(getDriver().findElement(DatnTim).isDisplayed());
		softAssert.assertAll();
		
		softAssert.assertTrue(getDriver().findElement(ResdlClm).isDisplayed());
		softAssert.assertAll();

	}

	public void searchTripToValidateVas(String aWB, String vASdate) throws InterruptedException {
		// Click on Back Button
		//getDriver().findElement(BtnBk).click();
		// Click on Filter
		getDriver().findElement(FilterBtn).click();
		// Send AWB
		getDriver().findElement(IpAwb).sendKeys(aWB);
		Thread.sleep(3000);
		// Click on Edit
		getDriver().findElement(BtnEdit).click();
		Thread.sleep(3000);
		getDriver().findElement(BtnEx2).click();
		Thread.sleep(3000);
		getDriver().findElement(LstMile).click();
		Thread.sleep(3000);
		getDriver().findElement(BtnNotAdded).click();
		Thread.sleep(3000);
		getDriver().findElement(BtnCollap).click();
		Thread.sleep(3000);
		getDriver().findElement(IpAwb2).sendKeys(aWB);
		Thread.sleep(3000);
		getDriver().findElement(BtnSearch1).click();
		Thread.sleep(3000);
		String ABC = getDriver().findElement(Vdate).getText();
		System.out.println(ABC + ">>>>>>>>>>>>>>>>>>>>");

		softAssert.assertTrue(getDriver().findElement(Vdate).getText().contains(vASdate));
		softAssert.assertAll();

	}

	public void clickLastMile() {
		getDriver().findElement(LstMile).click();
		
	}

	public void LogoutWeb() {
		getDriver().findElement(DropClick).click();
		getDriver().findElement(LogOut).click();
		
		
	}

	public void searchForUndeliveredSomethingAndClickOnViewTripThenClickOnCloseTripButton(String tripIdNumber,
			String awb) throws InterruptedException {

		// Click on Filter
				wait.until(ExpectedConditions.visibilityOfElementLocated(FilterBtn));
				getDriver().findElement(FilterBtn).click();
				
				// Send AWB
				wait.until(ExpectedConditions.visibilityOfElementLocated(IpTripId));
				
//				
//				getDriver().findElement(IpTripId).sendKeys(WebFirstMile.tripIdNumber);
//				//getDriver().findElement(IpTripId).sendKeys("22261");
//				
				wait.until(ExpectedConditions.visibilityOfElementLocated(BtnSearch));
				getDriver().findElement(BtnSearch).click();
				System.out.println("Clicked on search");
				// Click on View 
				//getDriver().findElement(BtnView).click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(ViewBtn));
				getDriver().findElement(ViewBtn).click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(LmTab));
				getDriver().findElement(LmTab).click();
				softAssert.assertTrue(getDriver().findElement(DelivStas).getText().contains("undelivered"));
				Thread.sleep(1500);
				getDriver().findElement(BtnClose).click();
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(LmTab1));
				getDriver().findElement(LmTab1).click();
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(UnDeliveAwb));
				getDriver().findElement(UnDeliveAwb).click();
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(DocInScn));
				getDriver().findElement(DocInScn).click();
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(ScnDoc));
				getDriver().findElement(ScnDoc).sendKeys(awb);
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(InvcNo));
				getDriver().findElement(InvcNo).sendKeys("1");
				Thread.sleep(5000);
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(Svbtn));
				getDriver().findElement(Svbtn).click();

				wait.until(ExpectedConditions.visibilityOfElementLocated(MpsInsnTab));
				getDriver().findElement(MpsInsnTab).click();
				Thread.sleep(5000);

				wait.until(ExpectedConditions.visibilityOfElementLocated(ScnAwb));
				getDriver().findElement(ScnAwb).sendKeys(awb);
				Thread.sleep(5000);
				wait.until(ExpectedConditions.visibilityOfElementLocated(IpOdo));
				getDriver().findElement(IpOdo).sendKeys("2");
				Thread.sleep(5000);

				wait.until(ExpectedConditions.visibilityOfElementLocated(BtnClose));
				getDriver().findElement(BtnClose).click();
				Thread.sleep(5000);

				wait.until(ExpectedConditions.visibilityOfElementLocated(BtnUndn));
				getDriver().findElement(BtnUndn).click();
				Thread.sleep(5000);

				wait.until(ExpectedConditions.visibilityOfElementLocated(BtnUndn));
				getDriver().findElement(BtnUndn).click();
				Thread.sleep(5000);

//				wait.until(ExpectedConditions.visibilityOfElementLocated(ScanAwb));
//				getDriver().findElement(ScanAwb).sendKeys(awb);
//				
//				wait.until(ExpectedConditions.visibilityOfElementLocated(BtnGood));
//				getDriver().findElement(BtnGood).click();
//				
//				wait.until(ExpectedConditions.visibilityOfElementLocated(BtnNxt));
//				getDriver().findElement(BtnNxt).click();
//				
//				wait.until(ExpectedConditions.visibilityOfElementLocated(IpOdo));
//				getDriver().findElement(IpOdo).sendKeys("2");
//				
//				wait.until(ExpectedConditions.visibilityOfElementLocated(BtnClose));
//				getDriver().findElement(BtnClose).click();
//				
//				wait.until(ExpectedConditions.visibilityOfElementLocated(BtnUndn));
//				getDriver().findElement(BtnUndn).click();

		
		
		
		
		
		
		
	}

}