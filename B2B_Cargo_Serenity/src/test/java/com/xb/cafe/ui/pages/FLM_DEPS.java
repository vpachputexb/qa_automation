package com.xb.cafe.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;

public class FLM_DEPS extends PageObject{
	
	public final By printMPSButton=By.xpath("//button[contains(text(),'Print MPS')]");
	public final By getMPStxt=By.xpath("//div[@id=\"printMPSData\"]//tr//td[2]");
	
	public final By tripManagementLabelLocator=By.xpath("//span[contains(text(),'Trip Management')]");
	public final By firstMileLastMileLabelLocator=By.xpath("//span[contains(text(),'First & Last Mile')]");
	public final By CargoMarkShipmentDamage=By.xpath("//span[contains(text(),'Cargo Mark Shipments Damage')]");
	public final By plusIcon=By.xpath("//a[@title=\"Mark Damage MPS\"]");
	public final By MPSNumber=By.xpath("//input[@id=\"MPS Number\"]");
	public final By GoButton=By.xpath("//button[contains(text(),'Go')]");
	public final By MarkCondition=By.xpath("(//input[@class=\"vue-treeselect__input\"])[2]");
	public final By UploadDamageImage=By.xpath("//input[@id=\"damagedoc\"]");
	public final By Description=By.xpath("//textarea[@id=\"description\"]");
	public final By SaveButton=By.xpath("//button[contains(text(),'Save')]");
	public final By BackArrow=By.xpath("//a[@title=\"Back to Damage MPS List\"]");
	public final By CurrentHubName=By.xpath("//a[@class=\"nav-link disabled\"]");
	public final By MPSstored=By.xpath("//div[@class=\"overflow\"]//tbody[@id=\"tbody\"]//tr//td[2]");
	public final By HubNameStored=By.xpath("//div[@class=\"overflow\"]//tbody[@id=\"tbody\"]//tr//td[3]");
	public final By ReasonStored=By.xpath("//div[@class=\"overflow\"]//tbody[@id=\"tbody\"]//tr//td[4]");
	public final By DescriptionStored=By.xpath("//div[@class=\"overflow\"]//tbody[@id=\"tbody\"]//tr//td[5]");
	public final By OpenUloadedImg=By.xpath("//img[@title=\"Open Image\"]");
	public final By CloseImage=By.xpath("//span[@title=\"Close Image\"]");
	public final By SearchFilter=By.xpath("//i[@title=\"Search Filter\"]");
	public final By FilterMPS=By.xpath("//textarea[@name=\"mpsnumber\"]");
	public final By HuborSVCName=By.xpath("//input[@id=\"hubname\"]");	
	public final By SearchButton=By.xpath("//button[contains(text(),'Search')]");
	public final By tableOutput=By.xpath("//tbody[@id=\"tbody\"]//tr//td");
	public final By ResetButton=By.xpath("//button[contains(text(),'Reset')]");
	public final By validfilterMPS=By.xpath("//tbody[@id=\"tbody\"]//tr//td[3]");
	public final By mpsStatusValidation=By.cssSelector("div[class*='ajs-message']");
	public final By currentRegionName=By.xpath("//input[@id=\"Region Name\"]");
	public final By originName=By.xpath("//input[@id=\"Origin Name\"]");
	public final By currentHubName=By.xpath("//input[@id=\"Current Hub Name\"]");
	public final By markDamageHubName=By.xpath("//input[@id=\"Mark Damage Hub Name\"]");
	public final By imageSizeError=By.xpath("//div[@id=\"swal2-content\"]");
	public final By okButton=By.xpath("//button[contains(text(),'OK')]");
	public final By viewImage=By.xpath("//tbody//tr//td[1]//img");
	public final By closeImage2=By.xpath("(//span[@class=\"closemodel\"])[2]");
	public final By removeImage=By.xpath("//i[@title=\"Remove Document\"]");
	
	public static String captureMPS;
	
	SoftAssert softAssert = new SoftAssert();
	

	public void userwillnavigatetoCargoMarkShipmentsDamage() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		try {
			getDriver().navigate().refresh();
			Thread.sleep(4000);
			wait.until(ExpectedConditions.elementToBeClickable(CargoMarkShipmentDamage));
			getDriver().findElement(CargoMarkShipmentDamage).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			Thread.sleep(4000);
			wait.until(ExpectedConditions.elementToBeClickable(firstMileLastMileLabelLocator));
			getDriver().findElement(firstMileLastMileLabelLocator).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(tripManagementLabelLocator));
			getDriver().findElement(tripManagementLabelLocator).click();
			Thread.sleep(3000);
			$(CargoMarkShipmentDamage).waitUntilClickable();
			$(CargoMarkShipmentDamage).click();
		}
	}

	public void userwilladdtherecordforDamageMPS() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(2000);
		$(plusIcon).waitUntilClickable();
		$(plusIcon).click();
	}

	public void userenterstheMPSdetailsforDamageProducts(String damageReason, String descriptiontxt) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(2000);
		$(MPSNumber).waitUntilVisible().sendKeys(captureMPS);	
		$(GoButton).waitUntilClickable();
		$(GoButton).click();
		Thread.sleep(2000);
		$(MarkCondition).waitUntilClickable();
		$(MarkCondition).click();
		Thread.sleep(4000);
		List<WebElement> damageCondition=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__list']//label"));
		
		for(int i=0;i<=damageCondition.size()-1;i++) {
			System.out.println(damageCondition.get(i).getText());
			if(damageCondition.get(i).getText().equalsIgnoreCase(damageReason)) {
				damageCondition.get(i).click();
				break;
			}
		}
		Thread.sleep(2000);
		WebElement uploadDamageimg = getDriver().findElement(UploadDamageImage);
		String projectPath=System.getProperty("user.dir");
		uploadDamageimg.sendKeys(projectPath+"/Test Data/DamageProduct.jpg");
		Thread.sleep(2000);
		$(Description).waitUntilVisible().sendKeys(descriptiontxt);
		WebElement element = $(SaveButton).waitUntilClickable();
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
		$(BackArrow).waitUntilClickable().click();
	}

	public void capturetheMPSNofromcargoBooking() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(2000);
		$(printMPSButton).waitUntilClickable().click();
		Thread.sleep(2000);
		captureMPS = $(getMPStxt).waitUntilVisible().getText();
		System.out.println("!!!!!!!!!!!MPS NO!!!!!!!!!!"+captureMPS);
		$("(//div[@class=\"card-header\"])[3]//div//button").click();
	}

	public void ifuserwantedtoupdatetheDamageCondition(String damageReason1) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		Thread.sleep(5000);
		Boolean flag=false;
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		do {
		for(int i=1;i<=10;i++) {
			if(getDriver().findElement(By.xpath("//table//tbody//tr["+i+"]//td[3]")).getText().equals(captureMPS)) 
					{
					flag=true;
					Thread.sleep(5000);
					Actions actions = new Actions(getDriver());
					Thread.sleep(2000);
					WebElement menuOption = getDriver().findElement(By.xpath("//span[@id=\"panRight\"]"));
					actions.moveToElement(menuOption).perform();
					Thread.sleep(2000);
			        actions.moveToElement(getDriver().findElement(By.xpath("//table//tbody//tr["+i+"]//td[13]//span[@title=\"Edit\"]"))).click().perform();
					break;
					}
			}
			if(flag==false) {
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@aria-label='Go to next page']")));
					getDriver().findElement(By.xpath("//button[@aria-label='Go to next page']")).click();
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@aria-label='Go to next page']")));
				}
			
	} while(!flag);
	
	Thread.sleep(5000);
	$(MarkCondition).waitUntilClickable().click();
	List<WebElement> damageCondition=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__list']//label"));
	for(int i=0;i<=damageCondition.size()-1;i++) {
		System.out.println(damageCondition.get(i).getText());
		if(damageCondition.get(i).getText().equalsIgnoreCase(damageReason1)) {
			damageCondition.get(i).click();
			break;
		}
	}
	Thread.sleep(2000);
	WebElement element = $(SaveButton).waitUntilClickable();
	JavascriptExecutor executor = (JavascriptExecutor)getDriver();
	executor.executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
	$(BackArrow).waitUntilClickable().click();
	}

	public void userwantedtoViewDamageDetails(String previousReason, String description1) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		Thread.sleep(5000);
		Boolean flag=false;
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		do {
		for(int i=1;i<=10;i++) {
			if(getDriver().findElement(By.xpath("//table//tbody//tr["+i+"]//td[3]")).getText().equals(captureMPS)) 
					{
					flag=true;
					Thread.sleep(5000);
					Actions actions = new Actions(getDriver());
					Thread.sleep(2000);
					WebElement menuOption = getDriver().findElement(By.xpath("//span[@id=\"panRight\"]"));
					actions.moveToElement(menuOption).perform();
					Thread.sleep(2000);
			        actions.moveToElement(getDriver().findElement(By.xpath("//table//tbody//tr["+i+"]//td[13]//span[@title=\"View Damage Details\"]"))).click().perform();
			        Thread.sleep(2000);
			        WebElement menuOptionLeft = getDriver().findElement(By.xpath("//span[@id=\"panLeft\"]"));
					actions.moveToElement(menuOptionLeft).perform();
					Thread.sleep(3000);
			        String getMPS = $(MPSstored).waitUntilVisible().getText();
					softAssert.assertEquals(captureMPS, getMPS);
					String getCurrHub = $(CurrentHubName).waitUntilVisible().getTextValue();
					String getHubName = $(HubNameStored).waitUntilVisible().getText();
					System.out.println(">>>>>Current Hub Name>>>>>"+getCurrHub+">>>>>Stored Hub Name>>>>>"+getHubName);
					softAssert.assertEquals(getCurrHub, getHubName);
					String getReason = $(ReasonStored).waitUntilVisible().getText();
					System.out.println("Feature file reason****"+previousReason+"****System Reason*****"+getReason);
					softAssert.assertEquals(previousReason, getReason);
					Thread.sleep(2000);
					actions.moveToElement(menuOption).perform();
					Thread.sleep(2000);
					String getDesc = $(DescriptionStored).waitUntilVisible().getText();
					System.out.println("Feature Description$$$$$"+description1+"$$$$System Desc$$$$"+getDesc);
					softAssert.assertEquals(description1, getDesc);
					actions.moveToElement(getDriver().findElement(By.xpath("//table//tbody//tr["+i+"]//td[13]//span[@title=\"View Damage Details\"]"))).click().perform();
					softAssert.assertAll();
					break;
					}
			}
			if(flag==false) {
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@aria-label='Go to next page']")));
					getDriver().findElement(By.xpath("//button[@aria-label='Go to next page']")).click();
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@aria-label='Go to next page']")));
				}
			
		} while(!flag);
		Thread.sleep(5000);
	}

	public void usercanviewtheuploadedimage() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		Thread.sleep(5000);
		Actions actions = new Actions(getDriver());
		WebElement menuOptionLeft = getDriver().findElement(By.xpath("//span[@id=\"panLeft\"]"));
		actions.moveToElement(menuOptionLeft).perform();
		Thread.sleep(2000);
		Boolean flag=false;
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		do {
		for(int i=1;i<=10;i++) {
			if(getDriver().findElement(By.xpath("//table//tbody//tr["+i+"]//td[3]")).getText().equals(captureMPS)) 
					{
					flag=true;
					Thread.sleep(5000);
					Thread.sleep(2000);
					WebElement menuOption = getDriver().findElement(By.xpath("//span[@id=\"panRight\"]"));
					actions.moveToElement(menuOption).perform();
					Thread.sleep(2000);
			        actions.moveToElement(getDriver().findElement(By.xpath("//table//tbody//tr[\"+i+\"]//td[13]//span[@title=\"View Images\"]"))).click().perform();
					break;
					}
			}
			if(flag==false) {
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@aria-label='Go to next page']")));
					getDriver().findElement(By.xpath("//button[@aria-label='Go to next page']")).click();
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@aria-label='Go to next page']")));
				}
			
		} while(!flag);
		Thread.sleep(5000);
		$(OpenUloadedImg).waitUntilVisible().waitUntilClickable().click();
		Thread.sleep(2000);
		$(CloseImage).waitUntilVisible().waitUntilClickable().click();
		Thread.sleep(1000);
		$("(//button[@class=\"close\"])[2]").waitUntilClickable().click();
	}

	public void userclicksonFilterOption() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		Thread.sleep(2000);
		$(SearchFilter).waitUntilClickable().click();
	}

	public void userenterstheinvalidinputs(String mPSNo, String damageHubName) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		Thread.sleep(1000);
		$(FilterMPS).waitUntilVisible().sendKeys(mPSNo);
		$(HuborSVCName).waitUntilClickable().sendKeys("PNQ/BAN");
		List<WebElement> damageCondition=getDriver().findElements(By.xpath("//div[@class='menu visible']//div"));
		for(int i=0;i<=damageCondition.size()-1;i++) {
			System.out.println(damageCondition.get(i).getText());
			if(damageCondition.get(i).getText().equalsIgnoreCase(damageHubName)) {
				damageCondition.get(i).click();
				break;
			}
		}
	}

	public void usersearchesfortheprovidedinput(String outPut) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		$(SearchButton).waitUntilClickable().click();
		Thread.sleep(1000);
		String getOutput = $(tableOutput).waitUntilVisible().getText();
		System.out.println("Feature Output!!!!!"+outPut+"!!!!!!System Output!!!!"+getOutput);
		softAssert.assertEquals(outPut, getOutput);
		$(ResetButton).waitUntilClickable().click();
		Thread.sleep(2000);
		$(FilterMPS).waitUntilVisible().sendKeys(captureMPS);
		$(SearchButton).waitUntilClickable().click();
		Thread.sleep(2000);
		Actions actions = new Actions(getDriver());
		WebElement menuOptionLeft = getDriver().findElement(By.xpath("//span[@id=\"panLeft\"]"));
		actions.moveToElement(menuOptionLeft).perform();
		Thread.sleep(2000);
		String getvalidfilterMPS = $(validfilterMPS).waitUntilVisible().getText();
		System.out.print("#######"+getvalidfilterMPS);
		softAssert.assertEquals(captureMPS, getvalidfilterMPS);
		softAssert.assertAll();
	}

	public void userentersthenotshippedMPSdetailsforDamageProducts(String damageReason, String description2, String mPSStatus) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(2000);
		$(MPSNumber).waitUntilVisible().sendKeys(captureMPS);	
		$(GoButton).waitUntilClickable();
		$(GoButton).click();
		Thread.sleep(2000);
		WebElement getCurrentRegionName = $(currentRegionName).waitUntilVisible();
		softAssert.assertEquals(false, getCurrentRegionName.isEnabled());
		WebElement getOriginHubName = $(originName).waitUntilVisible();
		softAssert.assertEquals(false, getOriginHubName.isEnabled());
		WebElement getCurrentHubName = $(currentHubName).waitUntilVisible();
		softAssert.assertEquals(false, getCurrentHubName.isEnabled());
		WebElement getMarkDamageHubName = $(markDamageHubName).and().waitUntilVisible();
		softAssert.assertEquals(false, getMarkDamageHubName.isEnabled());
		$(MarkCondition).waitUntilClickable();
		$(MarkCondition).click();
		Thread.sleep(4000);
		List<WebElement> damageCondition=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__list']//label"));
		
		for(int i=0;i<=damageCondition.size()-1;i++) {
			System.out.println(damageCondition.get(i).getText());
			if(damageCondition.get(i).getText().equalsIgnoreCase(damageReason)) {
				damageCondition.get(i).click();
				break;
			}
		}
		Thread.sleep(2000);
		WebElement uploadDamageimg = getDriver().findElement(UploadDamageImage);
		String projectPath=System.getProperty("user.dir");
		uploadDamageimg.sendKeys(projectPath+"/Test Data/DamageProduct.jpg");
		Thread.sleep(5000);
		$(Description).waitUntilVisible().sendKeys(description2);
		WebElement element = $(SaveButton).waitUntilClickable();
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
		String getmpsStatusValidation = $(mpsStatusValidation).waitUntilVisible().getText();
		System.out.println("getmpsStatusValidation>>>>>>>>>>>>>>>>>>>>>"+getmpsStatusValidation+"mPSStatus<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"+mPSStatus);
		Thread.sleep(2000);
		softAssert.assertEquals(mPSStatus.trim(), getmpsStatusValidation.trim());
		softAssert.assertAll();
	}

	public void userenterstheMPSdetailsofotherhubsforDamageProducts(String damageReason, String desc, String mPSNo, String statusValidation) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(2000);
		$(MPSNumber).waitUntilVisible().sendKeys(mPSNo);	
		$(GoButton).waitUntilClickable().click();
		Thread.sleep(3000);
		$(MarkCondition).waitUntilClickable().click();
		Thread.sleep(4000);
		List<WebElement> damageCondition=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__list']//label"));
		
		for(int i=0;i<=damageCondition.size()-1;i++) {
			System.out.println(damageCondition.get(i).getText());
			if(damageCondition.get(i).getText().equalsIgnoreCase(damageReason)) {
				damageCondition.get(i).click();
				break;
			}
		}
		Thread.sleep(2000);
		WebElement uploadDamageimg = getDriver().findElement(UploadDamageImage);
		String projectPath=System.getProperty("user.dir");
		uploadDamageimg.sendKeys(projectPath+"/Test Data/damage.jpg");
		Thread.sleep(1000);
		$(viewImage).waitUntilVisible().click();
		Thread.sleep(2000);
		$(closeImage2).waitUntilClickable().click();
		Thread.sleep(2000);
		Actions actions = new Actions(getDriver());
		Thread.sleep(2000);
        actions.moveToElement($(removeImage).waitUntilClickable()).click().perform();
        Thread.sleep(2000);
		uploadDamageimg.sendKeys(projectPath+"/Test Data/DamageProduct.jpg");
		Thread.sleep(5000);
		$(Description).waitUntilVisible().sendKeys(desc);
		WebElement element = $(SaveButton).waitUntilClickable();
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
		String getMPSstatusValidation = $(mpsStatusValidation).waitUntilVisible().getText();
		System.out.println("!!Feature error!!"+statusValidation+"!!System Error!!"+getMPSstatusValidation);
		softAssert.assertEquals(statusValidation, getMPSstatusValidation);
		softAssert.assertAll();
		$(BackArrow).waitUntilClickable().click();
	}

	public void usertriestouploadimagemorethen6000KB(String damageReason, String mPSNo, String imageError) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(2000);
		$(MPSNumber).waitUntilVisible().sendKeys(mPSNo);	
		$(GoButton).waitUntilClickable().click();
		Thread.sleep(3000);
		$(MarkCondition).waitUntilClickable().click();
		Thread.sleep(2000);
		List<WebElement> damageCondition=getDriver().findElements(By.xpath("//div[@class='vue-treeselect__list']//label"));
		
		for(int i=0;i<=damageCondition.size()-1;i++) {
			System.out.println(damageCondition.get(i).getText());
			if(damageCondition.get(i).getText().equalsIgnoreCase(damageReason)) {
				damageCondition.get(i).click();
				break;
			}
		}
		Thread.sleep(2000);
		WebElement uploadDamageimg = getDriver().findElement(UploadDamageImage);
		String projectPath=System.getProperty("user.dir");
		uploadDamageimg.sendKeys(projectPath+"/Test Data/damage.jpg");
		Thread.sleep(1000);
		uploadDamageimg.sendKeys(projectPath+"/Test Data/DamageProduct.jpg");
		Thread.sleep(2000);
		uploadDamageimg.sendKeys(projectPath+"/Test Data/HD.jpg");
		Thread.sleep(2000);
		String getImageSizeError = $(imageSizeError).waitUntilVisible().getText();
		System.out.println("!!Feature Error!!"+imageError+"!!System Error!!"+getImageSizeError);
		softAssert.assertEquals(imageError, getImageSizeError);
		$(okButton).waitUntilClickable().click();
		Thread.sleep(3000);
		softAssert.assertAll();
		$(BackArrow).waitUntilClickable().click();
	}

	public void userentersthealreadymarkedMPSdetailsagain(String damageReason, String description2, String mPSStatus) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		Thread.sleep(2000);
		$(MPSNumber).waitUntilVisible().sendKeys(captureMPS);	
		$(GoButton).waitUntilClickable().click();
		Thread.sleep(2000);
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		je.executeScript("window.scrollBy(0,300)");
		Thread.sleep(2000);
//		$(SaveButton).waitUntilClickable().click();
		WebElement element = $(SaveButton).waitUntilClickable();
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
		String getmpsStatusValidation = $(mpsStatusValidation).waitUntilVisible().getText();
		System.out.println("getmpsStatusValidation>>>>>>>>>>>>>>>>>>>>>"+getmpsStatusValidation+"mPSStatus<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"+mPSStatus);
		Thread.sleep(2000);
		softAssert.assertEquals(mPSStatus.trim(), getmpsStatusValidation.trim());
		softAssert.assertAll();
	}
}
