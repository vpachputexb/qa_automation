package com.xb.cafe.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import net.serenitybdd.core.pages.PageObject;

public class Cargo_ChangePhysicalWeight extends PageObject {

	public final By supportLink = By.xpath("//span[contains(text(),'Support')]");
	public final By changephysicalwtLnk = By.xpath("//span[contains(text(),'Change Physical Weight')]");
	public final By lbel = By.xpath("//label[contains(text(),'Change Physical Weight')]");
	public final By ShipmentID = By.xpath("//input[@id='shipmentId']");
	public final By blnkshipmenterrrmsg = By.xpath(" //span[contains(text(),'The Shipment Id field is required')]");
	public final By searchbtn = By.xpath("//button[@type='submit']");
	public final By resetbtn = By.xpath("//button[@type='button']");
	public final By errpopupinvalidid = By.xpath("//div[@id='swal2-content']");
	public final By okbtn = By.xpath("//button[contains(text(),'OK')]");

	public final By physicalwt = By.xpath("//input[@id='physicalWeight']");
	public final By updatebtn = By.xpath("//button[contains(text(),'Update')]");
	public final By deliveredAWBerr = By.xpath("//div[@id='swal2-content']");
	public final By deliveredAWBokbtn = By.xpath("//button[contains(text(),'OK')]");
	public final By success = By.xpath("//div[@id='swal2-content']");

	SoftAssert softAssert = new SoftAssert();

	public void NavigatetoSupportLink() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(supportLink).click();
	}

	public void NavigateToChangePhysicalWeight() throws InterruptedException {
		// TODO Auto-generated method stub
		getDriver().navigate().refresh();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		try {
			getDriver().navigate().refresh();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(changephysicalwtLnk));
			getDriver().findElement(changephysicalwtLnk).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			getDriver().navigate().refresh();
			getDriver().findElement(supportLink).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(changephysicalwtLnk));
			getDriver().findElement(changephysicalwtLnk).click();
			Thread.sleep(3000);
		}
	}

	public void LandedOnChangePhysicalWeightPage(String ChangePhysicalWeight) {
		String label = getDriver().findElement(lbel).getText();
		softAssert.assertEquals(label, ChangePhysicalWeight, "Landed on change  physical weight page ");
		softAssert.assertAll();

	}

	public void EnterShipmentIDAndSearch(String ShipemntID) throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(ShipmentID).sendKeys(ShipemntID);
		getDriver().findElement(searchbtn).click();

	}

	public void ErrorOnInvalidShipmentID(String ErrormsgonInvalidShipmentID) throws InterruptedException {
		Thread.sleep(3000);
		String errmsg = getDriver().findElement(blnkshipmenterrrmsg).getText();
		softAssert.assertEquals(errmsg, ErrormsgonInvalidShipmentID, "Shipemnt ID is invalid");
		softAssert.assertAll();

	}

	public void ClickOnResetButton() throws InterruptedException {
		Thread.sleep(3000);
		getDriver().findElement(resetbtn).click();

	}

	public void ResetShipmentID(String shipmentid) throws InterruptedException {
		Thread.sleep(3000);
		String si = getDriver().findElement(ShipmentID).getText();
		softAssert.assertEquals(si, shipmentid, "Shipment id is blank");
		softAssert.assertAll();

	}

	public void UpdatebtnDisplayed() throws InterruptedException {
		Thread.sleep(3000);
		softAssert.assertTrue(getDriver().findElement(updatebtn).isDisplayed(), "Update btn displayed");
		softAssert.assertAll();

	}

	public void EnterPhysicalWt(String physicalweight) throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(3000);
		getDriver().findElement(physicalwt).clear();
		getDriver().findElement(physicalwt).sendKeys(physicalweight);
		Thread.sleep(3000);
		getDriver().findElement(updatebtn).click();
		Thread.sleep(3000);

	}

	public void SuccessMsgOnUpdate(String Successmsg) throws InterruptedException {
		Thread.sleep(2000);
		String msg = $(success).waitUntilVisible().waitUntilPresent().getText();
		softAssert.assertEquals(msg, Successmsg, "Weight updated");
		softAssert.assertAll();
		getDriver().findElement(okbtn).click();
	}

	public void MsgonUpdatingWTDeliveredAWB(String DeliveredAWBMsg) throws InterruptedException {
		Thread.sleep(3000);
		String msg = getDriver().findElement(deliveredAWBerr).getText();
		softAssert.assertEquals(msg, DeliveredAWBMsg, "AWB is already delivered");
		softAssert.assertAll();
		getDriver().findElement(deliveredAWBokbtn).click();
		
	}

}
