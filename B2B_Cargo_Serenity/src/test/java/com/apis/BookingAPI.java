package com.apis;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.payloads.BookingAPI_Payload;
import com.xb.cafe.ui.utils.Dateutils;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class BookingAPI {
	
	
	public static String numberofAWB;
	public static RequestSpecification request_par;
	public static Response response;
	public static ValidatableResponse validate;
	public static String retunresponse;
	public static String baseUri;
	public String accessToken;
	RequestSpecification request_product;
	public static List<String> listofAwb = new ArrayList<String>();
	public static List<String> listOfMps=new ArrayList<String>();
	
	public int JsonValidationCode(String response, String address) {
		JsonPath js = new JsonPath(response);
		int actual = js.getInt(address);
		return actual;
	}

	public String JsonValidation(String response, String address) {
		JsonPath js = new JsonPath(response);
		String actual = js.getString(address);
		return actual;
	}

	public long generateRandomNumber(int length) {
	    Random random = new Random();
	    char[] digits = new char[length];
	    digits[0] = (char) (random.nextInt(9) + '1');
	    for (int i = 1; i < length; i++) {
	        digits[i] = (char) (random.nextInt(10) + '0');
	    }
	    return Long.parseLong(new String(digits));
	}

	public static boolean generateAwbNumbersOnStage(int noOfAWB,String pincode) {
		
		try {
			Dateutils dateutils=new Dateutils();
			BookingAPI key=new BookingAPI();
			BookingAPI_Payload payloads=new BookingAPI_Payload();
			RestAssured.baseURI ="http://192.168.50.42:803" ;
			for (int i = 0; i <70; i++) {
				String AWBNumber = "9" + String.valueOf(key.generateRandomNumber(14));
				String MPSNo="MPS"+String.valueOf(key.generateRandomNumber(10));
				String InvoiceNo = "INV" + AWBNumber;
				request_par = given().log().all().header("Content-Type", "application/json").header("version", "v1")
						.header("XBKey", "XB123")
						.body(payloads.AwbCreation(AWBNumber, MPSNo, InvoiceNo, dateutils.futureDate(2), pincode));
				response = request_par.when().post("/StandardForwardStagingService.svc/AddCargoManifestDetails");
				validate = response.then();
				retunresponse = validate.log().all().extract().response().asString();
				int actualCode = key.JsonValidationCode(retunresponse, "ReturnCode");
				if (actualCode == 100) {
					System.out.println("Created AWB is : "+AWBNumber);
					listofAwb.add(AWBNumber);
					listOfMps.add(MPSNo);
					int length =listofAwb.size();
					if (length == noOfAWB) {
						break;
					}
					

				}
			}System.out.println(listofAwb);
			System.out.println(listOfMps);
			return true;
		}

		catch (Exception e) { // TODO: handle exception

			System.out.println("Exception found>>>>" + e.getMessage());
			return false;
		}

	}

}
