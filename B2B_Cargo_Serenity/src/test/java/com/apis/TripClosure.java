package com.apis;

import static io.restassured.RestAssured.*;

import org.testng.annotations.Test;

import com.payloads.TripManagementTripClose;

import io.restassured.RestAssured;
import net.serenitybdd.core.pages.PageObject;

public class TripClosure extends PageObject {
	
	
	public  void  closeTripAPI() {
		TripManagementTripClose td = new TripManagementTripClose();
	   
		 String token = "token"; 
		 String tokenValue ="a61b26a70d2af9322570d4ac6078b8547555sd81dc191ceadbbe4eeccd1d588f2"; 
		 String baseURL = "http://cargoautoallocation-apistage.xbees.in:8081/"; 
		 String autorization = "Authorization"; 
		 String authorizationValue ="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjM5NjA3IiwiaWF0IjoxNjE3MjU0NjgxLCJleHAiOjE2MTcyOTA2ODF9.c0KHAo4aMhmFNF59ew1n0AZyH1kneA63vDoxuA8HRN0";
		 
		 RestAssured.baseURI = baseURL; 
		 String response= given().header(token,tokenValue).header(autorization, authorizationValue).body(td.closeTrip()).when()
		 .post("api/cargotripmanagement/external/mobile/closetrip").then().log().all().assertThat().statusCode(200).extract().response().asString();
		 System.out.print(response);
		
//		MobilePickupApis.mobileApis();
//		MobilePickupApis.loginWeb();
//		MobilePickupApis.viewTripDetails();
		
		
   		}
   }
