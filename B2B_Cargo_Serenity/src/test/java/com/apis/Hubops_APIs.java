package com.apis;

import static io.restassured.RestAssured.given;

import com.payloads.Hubops_payload;
import com.payloads.MobileAPIDelivery_Payload;

import io.restassured.RestAssured;

public class Hubops_APIs {
	public static String token = "token";
	public static String tokenValue = "c423ed60f34c0021016fc65cb53a9ad4";
	public static String baseURL = "http://api.staging.shipmentupdates.xbees.in/";
	public static String versionNumber = "versionnumber";
	public static String versionValue = "v1";
	public static String contentType="Content-Type";
	public static String contentTypeValue="application/json";
	
	public static void hubOpsAPIsHit() {
		markSchenduleAPI();
		markOutscan_API();
		markInscan_API();
	}
	
	public static void markSchenduleAPI() {
		Hubops_payload hubops_payload=new Hubops_payload();
		String postmanToken="Postman-Token";
		String postmanTokenValue="e74ebdb1-4ce5-43e1-b46c-3898612ef765				";
		RestAssured.baseURI = baseURL; 
		String response = given()
				.header(token, tokenValue)
				.header(versionNumber, versionValue)
				.header(contentType,contentTypeValue)
				.header(postmanToken,postmanTokenValue)
				.body(hubops_payload.markSchedule_Payload())
				.when().post("markscheduled")
				.then().log().all().assertThat().statusCode(200).extract().response().asString();
		System.out.println(response);
	}
	public static void markOutscan_API() {
		Hubops_payload hubops_payload=new Hubops_payload();
		String postmanToken="Postman-Token";
		String postmanTokenValue="ebd483be-34b4-4bbc-8918-b172afe966fa,01741ef5-2363-4d5e-9393-61a475eda8b4";
		RestAssured.baseURI = baseURL; 
		String response = given()
				.header(token, tokenValue)
				.header(versionNumber, versionValue)
				.header(contentType,contentTypeValue)
				.header(postmanToken,postmanTokenValue)
				.body(hubops_payload.markInScan_Payload())
				.when().post("markoutscan")
				.then().log().all().assertThat().statusCode(200).extract().response().asString();
		System.out.println(response);
	}
	public static void markInscan_API() {
		Hubops_payload hubops_payload=new Hubops_payload();
		String postmanToken="Postman-Token";
		String postmanTokenValue="29e32289-6f37-458a-8e18-84ff5cef2a06";
		String xbKey="xbkey";
		String xbKeyValue="xbkey47";
		
		RestAssured.baseURI = baseURL; 
		String response = given()
				.header(token, tokenValue)
				.header(versionNumber, versionValue)
				.header(contentType,contentTypeValue)
				.header(postmanToken,postmanTokenValue)
				.header(xbKey,xbKeyValue)
				.body(hubops_payload.markInScan_Payload())
				.when().post("updateinscancargoshipment")
				.then().log().all().assertThat().statusCode(200).extract().response().asString();
		System.out.println(response);
	}
	
	
}
