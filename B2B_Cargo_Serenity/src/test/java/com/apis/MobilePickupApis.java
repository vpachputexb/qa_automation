package com.apis;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.util.List;

import com.payloads.MobileApiPayload_PickUp;
import com.xb.cafe.ui.pages.Cargo_FLM_TripManagement;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class MobilePickupApis {
	public static String userID = "24422";
	public static String tokenLogin;
	public static String hubID;
	public static String awbNumber;
	public static String tripID;
	public static String mpsNumber1;
	public static String mpsNumber2;
	public static String mpsNumber3;
	public static String mpsQuantity;
	public static String tokenDynamicWeb;

	public static String token = "token";
	public static String tokenValue = "a61b26a70d2af9322570d4ac6078b8547555sd81dc191ceadbbe4eeccd1d588f2";

	public static String version = "versionnumber";
	public static String versionValue = "v1";
	public static String baseURL = "http://stageusermanagementapi.xbees.in/";
	public static String baseURLPicckup = "http://cargoautoallocation-apistage.xbees.in:8081/";

	DBConnectivityAndTripIDFetch dbConnectivityAndTripIDFetch = new DBConnectivityAndTripIDFetch();

	public static void main(String[] agrs) {
		mobileLogout();
		mobileLogin();
		mobileToken();
		getTripAllocation();
		getAWBdetails();
		savempsDetailsMultiple();
//		scanMpsDetails();

		submitpickupProof();

	}

	public static void mobileApis() {
		mobileLogout();
		mobileLogin();
		mobileToken();
		getTripAllocation();
		getAWBdetails();
		savempsDetailsMultiple();
		scanMpsDetails(mpsNumber1);
		scanMpsDetails(mpsNumber2);
		submitpickupProof();

	}

	public static void viewTrip() {
		loginWeb();
		viewTripDetails();
	}

	public static void mobileLogout() {
		MobileApiPayload_PickUp mobileToken = new MobileApiPayload_PickUp();

		String authorization = "Authorization";
		String authorizationValue = "Bearer vKyXQBGQCK";

		RestAssured.baseURI = baseURL;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.header(authorization, authorizationValue).body(mobileToken.logoutPayload()).when()
				.post("api/userauth/mobilelogout").then().log().all().assertThat().statusCode(200).extract().response()
				.asString();
		System.out.println(response);
	}

	public static void mobileLogin() {
		try {
			MobileApiPayload_PickUp td = new MobileApiPayload_PickUp();
			RestAssured.baseURI = baseURL;
			Response response = given().body(td.mobileLogin()).when().post("api/userauth/getmobileusertoken").then()
					.log().all().assertThat().statusCode(200).extract().response();

			tokenLogin = response.path("token");
			System.out.print(tokenLogin);
		} catch (Exception e) {
			System.out.println("Error Occured");
		}

	}

	public static void mobileToken() {
		MobileApiPayload_PickUp mobileToken = new MobileApiPayload_PickUp();

		RestAssured.baseURI = baseURL;
		String response = given().body(mobileToken.mobileLoginToken()).when().post("api/userauth/validmobileusertoken")
				.then().log().all().assertThat().statusCode(200).extract().response().asString();

		JsonPath js = new JsonPath(response);
		hubID = js.getString("userdetails[0].HubId[0]");
		System.out.println(hubID + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

	}

	public static void getTripAllocation() {
		MobileApiPayload_PickUp mobileToken = new MobileApiPayload_PickUp();

		RestAssured.baseURI = baseURLPicckup;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.body(mobileToken.GetTripAllocationDetailsPayload()).when()
				.post("api/cargotripmanagement/external/mobile/gettripallocation").then().log().all().assertThat()
				.statusCode(200).extract().response().asString();

		JsonPath js = new JsonPath(response);
		awbNumber = js.getString("data.clientShipments[0].shipments[0].awbno");
		tripID = js.getString("data.tripid");
		System.out.println(tripID + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println(awbNumber + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

	}

	public static void getAWBdetails() {
		MobileApiPayload_PickUp mobileToken = new MobileApiPayload_PickUp();

		RestAssured.baseURI = baseURLPicckup;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.body(mobileToken.getAWBShippingDetailsPayload()).when()
				.post("api/cargotripmanagement/external/mobile/getawbshippingdetails").then().log().all().assertThat()
				.statusCode(200).extract().response().asString();

	}

	public static void savempsDetails() {
		MobileApiPayload_PickUp mobileToken = new MobileApiPayload_PickUp();

		RestAssured.baseURI = baseURLPicckup;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.body(mobileToken.saveMPSDetailsPayload()).when()
				.post("api/cargotripmanagement/external/mobile/savempsdetails").then().log().all().assertThat()
				.statusCode(200).extract().response().asString();

		JsonPath js = new JsonPath(response);
		mpsNumber1 = js.getString("data[0].mps");
//		mpsNumber2 = js.getString("data[1].mps");

		System.out.println(mpsNumber1 + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}

	public static void savempsDetailsMultiple() {
		MobileApiPayload_PickUp mobileToken = new MobileApiPayload_PickUp();

		RestAssured.baseURI = baseURLPicckup;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.body(mobileToken.saveMPSDetailsPayload()).when()
				.post("api/cargotripmanagement/external/mobile/savempsdetails").then().log().all().assertThat()
				.statusCode(200).extract().response().asString();

		JsonPath js = new JsonPath(response);
		mpsNumber1 = js.getString("data[0].mps");
		mpsNumber2 = js.getString("data[1].mps");

		System.out.println(mpsNumber1 + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println(mpsNumber2 + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}

	public static void scanMpsDetails(String mps) {
		MobileApiPayload_PickUp mobileToken = new MobileApiPayload_PickUp();

		RestAssured.baseURI = baseURLPicckup;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.body(mobileToken.scanMPSDetailsPayload(mps)).when()
				.post("api/cargotripmanagement/external/mobile/scanmps").then().log().all().assertThat().statusCode(200)
				.extract().response().asString();

	}

	public static void submitpickupProof() {
		MobileApiPayload_PickUp mobileToken = new MobileApiPayload_PickUp();

		RestAssured.baseURI = baseURLPicckup;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.body(mobileToken.pickUpProofSubmitPayload()).when()
				.post("api/cargotripmanagement/external/mobile/firstmilepod").then().log().all().assertThat()
				.statusCode(200).extract().response().asString();

		System.out.println(response + ">>>>+++++++++======++++++++======>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}

	public static void loginWeb() {
		MobileApiPayload_PickUp mobileToken = new MobileApiPayload_PickUp();
		RestAssured.baseURI = baseURLPicckup;

		String response = given().body(mobileToken.webLoginPayload()).when().post("api/auth").then().log().all()
				.assertThat().statusCode(200).extract().response().asString();

		JsonPath js = new JsonPath(response);
		tokenDynamicWeb = js.getString("data.userToken");

		System.out.println(tokenDynamicWeb + ">>>>+++++++++======++++++++======>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}

	public static void viewTripDetails() {
		MobileApiPayload_PickUp mobileToken = new MobileApiPayload_PickUp();
		RestAssured.baseURI = baseURLPicckup;
		String authorizationWebLogin = "Authorization";
		String authorizationWebLoginValue = "Bearer " + tokenDynamicWeb;
		String tokenWebLogin = "token";
		String tokenWebLoginValue = "a61b26a70d2af9322570d4ac6078b8547555sd81dc191ceadbbe4eeccd1d588f2";
		String response = given().header(tokenWebLogin, tokenWebLoginValue)
				.header(authorizationWebLogin, authorizationWebLoginValue).body(mobileToken.viewTripDetailsPayload())
				.when().post("api/cargotripmanagement/gettripdata").then().log().all().assertThat().statusCode(200)
				.extract().response().asString();
		System.out.println(response + ">>>>+++++++++======++++++++======>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}
}
