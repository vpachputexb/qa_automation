package com.apis;

import static io.restassured.RestAssured.given;

import com.payloads.MobileAPIDelivery_Payload;
import com.payloads.MobilePickUpAPIs_MultipleAWB_Payload;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class MobilePickupAPIs_MultipleAWBs {
	public static String userID = "24422";
	public static String tokenLogin;
	public static String hubID;
	public static String tripID;
	public static String username="90398";
	public static String numberOfMps=BookingAPI.numberofAWB;
	public static String token = "token";
	public static String tokenValue = "a61b26a70d2af9322570d4ac6078b8547555sd81dc191ceadbbe4eeccd1d588f2";
	public static String tokenDynamicWeb;
	
	public static String version = "versionnumber";
	public static String versionValue = "v1";
	public static String baseURL = "http://stageusermanagementapi.xbees.in/";
	public static String baseURLPicckup = "http://cargoautoallocation-apistage.xbees.in:8081/";
	
	public static void mobileApis_Multiple() {
		BookingAPI bookingAPI=new BookingAPI();
		
		
		mobileLogout();
		mobileLogin();
		mobileToken();
		getTripAllocation();
		for(int i=0; i<=bookingAPI.listofAwb.size()-1; i++)  {
		getAWBdetails(bookingAPI.listofAwb.get(i));
		savempsDetails(bookingAPI.listofAwb.get(i));
		
		scanMpsDetails(bookingAPI.listofAwb.get(i),bookingAPI.listOfMps.get(i));
		submitpickupProof(bookingAPI.listofAwb.get(i));
		}
		loginWebDelivery();
		viewTripDetailsDelivery();
	}
	
	public static void mobileLogout() {
		MobilePickUpAPIs_MultipleAWB_Payload mobileToken = new MobilePickUpAPIs_MultipleAWB_Payload();

		String authorization = "Authorization";
		String authorizationValue = "Bearer vKyXQBGQCK";

		RestAssured.baseURI = baseURL;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.header(authorization, authorizationValue).body(mobileToken.logoutPayload()).when()
				.post("api/userauth/mobilelogout").then().log().all().assertThat().statusCode(200).extract().response()
				.asString();
		System.out.println(response);
	}
	
	public static void mobileLogin() {
		try {
			MobilePickUpAPIs_MultipleAWB_Payload td = new MobilePickUpAPIs_MultipleAWB_Payload();
			RestAssured.baseURI = baseURL;
			Response response = given().body(td.mobileLogin()).when().post("api/userauth/getmobileusertoken").then()
					.log().all().assertThat().statusCode(200).extract().response();

			tokenLogin = response.path("token");
			System.out.print(tokenLogin);
		} catch (Exception e) {
			System.out.println("Error Occured");
		}
	}
	
	
	public static void mobileToken() {
		MobilePickUpAPIs_MultipleAWB_Payload mobileToken = new MobilePickUpAPIs_MultipleAWB_Payload();

		RestAssured.baseURI = baseURL;
		String response = given().body(mobileToken.mobileLoginToken()).when().post("api/userauth/validmobileusertoken")
				.then().log().all().assertThat().statusCode(200).extract().response().asString();

		JsonPath js = new JsonPath(response);
		hubID = js.getString("userdetails[0].HubId[0]");
		System.out.println(hubID + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

	}
	public static void getTripAllocation() {
		MobilePickUpAPIs_MultipleAWB_Payload mobileToken = new MobilePickUpAPIs_MultipleAWB_Payload();

		RestAssured.baseURI = baseURLPicckup;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.body(mobileToken.GetTripAllocationDetailsPayload()).when()
				.post("api/cargotripmanagement/external/mobile/gettripallocation").then().log().all().assertThat()
				.statusCode(200).extract().response().asString();

		JsonPath js = new JsonPath(response);
//		awbNumber = js.getString("data.clientShipments[0].shipments[0].awbno");
		tripID = js.getString("data.tripid");
		System.out.println(tripID + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//		System.out.println(awbNumber + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

	}
	
	public static void getAWBdetails(String awb) {
		MobilePickUpAPIs_MultipleAWB_Payload mobileToken = new MobilePickUpAPIs_MultipleAWB_Payload();

		RestAssured.baseURI = baseURLPicckup;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.body(mobileToken.getAWBShippingDetailsPayload(awb)).when()
				.post("api/cargotripmanagement/external/mobile/getawbshippingdetails").then().log().all().assertThat()
				.statusCode(200).extract().response().asString();

	}

	public static void savempsDetails(String awb) {
		MobilePickUpAPIs_MultipleAWB_Payload mobileToken = new MobilePickUpAPIs_MultipleAWB_Payload();

		RestAssured.baseURI = baseURLPicckup;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.body(mobileToken.saveMPSDetailsPayload(awb)).when()
				.post("api/cargotripmanagement/external/mobile/savempsdetails").then().log().all().assertThat()
				.statusCode(200).extract().response().asString();

		JsonPath js = new JsonPath(response);
//		mpsNumber1 = js.getString("data[0].mps");
//		mpsNumber2 = js.getString("data[1].mps");

//		System.out.println(mpsNumber1 + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}
	
	public static void scanMpsDetails(String awb,String mps) {
		MobilePickUpAPIs_MultipleAWB_Payload mobileToken = new MobilePickUpAPIs_MultipleAWB_Payload();

		RestAssured.baseURI = baseURLPicckup;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.body(mobileToken.scanMPSDetailsPayload(awb,mps)).when()
				.post("api/cargotripmanagement/external/mobile/scanmps").then().log().all().assertThat().statusCode(200)
				.extract().response().asString();

	}

	public static void submitpickupProof(String awb) {
		MobilePickUpAPIs_MultipleAWB_Payload mobileToken = new MobilePickUpAPIs_MultipleAWB_Payload();

		RestAssured.baseURI = baseURLPicckup;
		String response = given().header(token, tokenValue).header(version, versionValue)
				.body(mobileToken.pickUpProofSubmitPayload(awb)).when()
				.post("api/cargotripmanagement/external/mobile/firstmilepod").then().log().all().assertThat()
				.statusCode(200).extract().response().asString();

		System.out.println(response + ">>>>+++++++++======++++++++======>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}
	public static void loginWebDelivery() {
		MobilePickUpAPIs_MultipleAWB_Payload mobileToken = new MobilePickUpAPIs_MultipleAWB_Payload();
		RestAssured.baseURI = baseURLPicckup;

		String response = given().body(mobileToken.webLoginDeliveryPayload()).when().post("api/auth").then().log().all()
				.assertThat().statusCode(200).extract().response().asString();

		JsonPath js = new JsonPath(response);
		tokenDynamicWeb= js.getString("data.userToken");

		System.out.println(tokenDynamicWeb + ">>>>+++++++++======++++++++======>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}
	
	public static void viewTripDetailsDelivery() {
		MobilePickUpAPIs_MultipleAWB_Payload mobileToken = new MobilePickUpAPIs_MultipleAWB_Payload();
		RestAssured.baseURI = baseURLPicckup;
		String authorizationWebLogin = "Authorization";
		String authorizationWebLoginValue = "Bearer " + tokenDynamicWeb;
		String tokenWebLogin = "token";
		String tokenWebLoginValue = "a61b26a70d2af9322570d4ac6078b8547555sd81dc191ceadbbe4eeccd1d588f2";
		String response = given().header(tokenWebLogin, tokenWebLoginValue)
				.header(authorizationWebLogin, authorizationWebLoginValue).body(mobileToken.viewTripDetailsDelivery_Payload())
				.when().post("api/cargotripmanagement/gettripdata").then().log().all().assertThat().statusCode(200)
				.extract().response().asString();
		System.out.println(response + ">>>>+++++++++======++++++++======>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}
}
