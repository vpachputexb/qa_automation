package com.apis;

import static io.restassured.RestAssured.given;

import com.payloads.MobileAPIDelivery_Payload;
import com.payloads.MobileDelieveryAPIs_MultipleAWB_Payload;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class MobileDeliveryAPIs_MultipleAWBs {

	public static String userIDDelivery = "24422";
	public static String hubIDDelivery="117";
	public static String tripIDDelivery;
	public static String clientIDDelivery;
	public static String pincodeDelivery;
	public static String clientBussinessIDDelivery;
	public static String tokenDynamicWebDelivery;
	public static String token = "token";
	public static String tokenValue = "a61b26a70d2af9322570d4ac6078b8547555sd81dc191ceadbbe4eeccd1d588f2";

	public static String version = "versionnumber";
	public static String versionValue = "v1";
	public static String contentType="Content-Type";
	public static String contentTypeValue="application/json";
	
	public static String baseURL = "http://stageusermanagementapi.xbees.in/";
	public static String baseURLDelivery = "http://cargoautoallocation-apistage.xbees.in:8081/";
	
	
	public void deliveryAPIshitMultiple() {
		
		BookingAPI bookingAPI=new BookingAPI();
		getTripAllocationDelivery();
		for(int i=0; i<=bookingAPI.listofAwb.size()-1; i++)  {
		scanMpsDetailsDelivery(bookingAPI.listofAwb.get(i),bookingAPI.listOfMps.get(i));
		lastMilePOD_API(bookingAPI.listofAwb.get(i));
		saveDelivery_API(bookingAPI.listofAwb.get(i));
		}
		loginWebDelivery();
		viewTripDetailsDelivery();
	}
	public void awbverifyAPIMultiple() {
		BookingAPI bookingAPI=new BookingAPI();
		for(int i=0; i<=bookingAPI.listofAwb.size()-1; i++) {
			awbVerifyAPI(bookingAPI.listofAwb.get(i),bookingAPI.listOfMps.get(i));
		}
	}
	
	
	public static void getTripAllocationDelivery() {

		MobileDelieveryAPIs_MultipleAWB_Payload mobileDelivery = new MobileDelieveryAPIs_MultipleAWB_Payload();

		RestAssured.baseURI = baseURLDelivery;
		String response = given().header(token, tokenValue).header(version, versionValue).header(contentType,contentTypeValue).header(contentType,contentTypeValue)
				.body(mobileDelivery.GetTripAllocationDetailsPayloadDelivery()).when()
				.post("api/cargotripmanagement/external/mobile/gettripallocation").then().log().all().assertThat()
				.statusCode(200).extract().response().asString();

		JsonPath js = new JsonPath(response);
//		awbNumberDelivery = js.getString("data.clientShipments[0].shipments[0].awbno");
		tripIDDelivery = js.getString("data.tripid");
		clientIDDelivery = js.getString("data.clientShipments[0].clientid");
		pincodeDelivery = js.getString("data.clientShipments[0].shipments[0].destinationpincode");
		clientBussinessIDDelivery = js.getString("data.clientShipments[0].clientbusinessaccountid");
//		mpsNumberDelivery=js.getString("data.clientShipments[0].shipments[0].pendingmps[0].mps");
//		System.out.println(mpsNumberDelivery + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//		
		System.out.println(tripIDDelivery + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//		System.out.println(awbNumberDelivery + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

	}
	
	public static void scanMpsDetailsDelivery(String awb, String mps) {
		MobileDelieveryAPIs_MultipleAWB_Payload mobileDelivery = new MobileDelieveryAPIs_MultipleAWB_Payload();

		RestAssured.baseURI = baseURLDelivery;
		String response = given().header(token, tokenValue).header(version, versionValue).header(contentType,contentTypeValue)
				.body(mobileDelivery.scanMPSDetailsPayloadDelivery(awb, mps)).when()
				.post("api/cargotripmanagement/external/mobile/scanmps").then().log().all().assertThat().statusCode(200)
				.extract().response().asString();

	}
	
	public static void lastMilePOD_API(String awb) {
		MobileDelieveryAPIs_MultipleAWB_Payload mobileDelivery = new MobileDelieveryAPIs_MultipleAWB_Payload();

		RestAssured.baseURI = baseURLDelivery;
		String response = given().header(token, tokenValue).header(version, versionValue).header(contentType,contentTypeValue)
				.body(mobileDelivery.lastMilePOD_Payload(awb)).when()
				.post("api/cargotripmanagement/external/mobile/lastmilepod").then().log().all().assertThat().statusCode(200)
				.extract().response().asString();
		
		System.out.println(response);
		}
	
	public static void saveDelivery_API(String awb) {
		MobileDelieveryAPIs_MultipleAWB_Payload mobileDelivery = new MobileDelieveryAPIs_MultipleAWB_Payload();

		RestAssured.baseURI = baseURLDelivery;
		String response = given().header(token, tokenValue).header(version, versionValue).header(contentType,contentTypeValue)
				.body(mobileDelivery.saveDelivery_Payload(awb)).when()
				.post("api/cargotripmanagement/external/mobile/savedelivery").then().log().all().assertThat().statusCode(200)
				.extract().response().asString();
		System.out.println(response+"<<<<<<<<<<Delivered>>>>>>>>>>>>>>");
	}
	
	public static void loginWebDelivery() {
		MobileDelieveryAPIs_MultipleAWB_Payload mobileDelivery = new MobileDelieveryAPIs_MultipleAWB_Payload();
		RestAssured.baseURI = baseURLDelivery;

		String response = given().body(mobileDelivery.webLoginDeliveryPayload()).when().post("api/auth").then().log().all()
				.assertThat().statusCode(200).extract().response().asString();

		JsonPath js = new JsonPath(response);
		tokenDynamicWebDelivery= js.getString("data.userToken");

		System.out.println(tokenDynamicWebDelivery + ">>>>+++++++++======++++++++======>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}

	public static void viewTripDetailsDelivery() {
		MobileDelieveryAPIs_MultipleAWB_Payload mobileDelivery = new MobileDelieveryAPIs_MultipleAWB_Payload();
		RestAssured.baseURI = baseURLDelivery;
		String authorizationWebLogin = "Authorization";
		String authorizationWebLoginValue = "Bearer " + tokenDynamicWebDelivery;
		String tokenWebLogin = "token";
		String tokenWebLoginValue = "a61b26a70d2af9322570d4ac6078b8547555sd81dc191ceadbbe4eeccd1d588f2";
		String response = given().header(tokenWebLogin, tokenWebLoginValue)
				.header(authorizationWebLogin, authorizationWebLoginValue).body(mobileDelivery.viewTripDetailsDelivery_Payload())
				.when().post("api/cargotripmanagement/gettripdata").then().log().all().assertThat().statusCode(200)
				.extract().response().asString();
		System.out.println(response + ">>>>+++++++++======++++++++======>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}
	public static void awbVerifyAPI(String awb,String mps) {
		MobileDelieveryAPIs_MultipleAWB_Payload mobileDelivery = new MobileDelieveryAPIs_MultipleAWB_Payload();
		String token = "token";
		String tokenValue = "c423ed60f34c0021016fc65cb53a9ad4";
		String baseURL = "http://api.staging.shipmentupdates.xbees.in/";
		String versionNumber = "versionnumber";
		String versionValue = "v1";
		String json1 = "Content-Type";
		String json2 = "application/json";

		RestAssured.baseURI = baseURL; 
		String response = given()
				.header(token, tokenValue)
				.header(versionNumber, versionValue)
				.header(contentType,contentTypeValue)
				.body(mobileDelivery.awbverifyPayload(awb,mps)).when()
				.post("updateawbverification").then().log().all().assertThat().statusCode(200).extract().response().asString();
		System.out.println(response);
	}
}
