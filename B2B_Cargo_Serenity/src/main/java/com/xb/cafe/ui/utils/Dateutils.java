package com.xb.cafe.ui.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Dateutils {

	public String futureDate(int noDays) {
		Calendar c = Calendar.getInstance();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date1 = new Date();
			String curretDate = df.format(date1);
			Date myDate = df.parse(curretDate);
			c.setTime(myDate);
			c.add(Calendar.DATE, noDays);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String toDate = df.format(c.getTime());
		return toDate;
	}

}
